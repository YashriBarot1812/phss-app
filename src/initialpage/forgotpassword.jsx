/**
 * Signin Firebase
 */

import React, { Component } from 'react';
import { Helmet } from "react-helmet";
import {Applogo} from '../Entryfile/imagepath.jsx'

import Loader from './Loader';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import SystemHelpers from './Helpers/SystemHelper';

class ForgotPassword extends Component {
  constructor(props) {
      super(props);

      this.state = {
          questions: [],
          password : '',
          errormsg :  '',

          
          step : 1,
          
          // step 1
          emailcheck : '',
          // step 1

          // step 2
          contactId : '',
          auth_token : '',
          num1 : '',
          num2 : '',
          num3 : '',
          num4 : '',
          otp : '',
          // step 2

          // step 3

          setQue1  : 0,
          setQue2 : 0,
          setQue3 : 0,
          setAns1 : '',
          setAns2 : '',
          setAns3 : '',
          // step 3

          // step 4
          NewPassword : '',
          ConfirmNewPassword : '',
          // step 4




      };
      this.handleChange = this.handleChange.bind(this);
      this.handleChangeOtp = this.handleChangeOtp.bind(this);
      
  }
   

  // Input box Type method
  handleChange = input => e => {
    this.setState({ [input]: e.target.value });

    if (this.state[input] != '') {
        delete this.state.errormsg[input];
    }
    
  }
  // Input box Type method

  // Input box Type method
  handleChangeOtp = input => e => {
    this.setState({ [input]: e.target.value });

    const { maxLength, value, name } = e.target;
    const [fieldName, fieldIndex] = name.split("-");

    // Check if they hit the max character length
    if (value.length >= maxLength) {
      // Check if it's not the last input field
      if (parseInt(fieldIndex, 10) < 4) {
        // Get the next input field
        const nextSibling = document.querySelector(
          `input[name=ssn-${parseInt(fieldIndex, 10) + 1}]`
        );

        // If found, focus the next field
        if (nextSibling !== null) {
          nextSibling.focus();
        }
      }
    }
  }

  // Page load time run
  componentDidMount(){
      if (location.pathname.includes("login") || location.pathname.includes("register") || location.pathname.includes("forgot-password")
      || location.pathname.includes("resetpassword") || location.pathname.includes("mobilesubmit") || location.pathname.includes("otp")|| location.pathname.includes("lockscreen") ) {
          $('body').addClass('account-page');
      }else if (location.pathname.includes("error-404") || location.pathname.includes("error-500") ) {
          $('body').addClass('error-page');
      }

      if (localStorage.getItem("token") != null) {
        this.props.history.push("/dashboard");
      }
      
  }
  // Page load time run

  // Loader hide show method
  hideLoader = () => {
    this.setState({ loading: false });
  }

  showLoader = () => {
    this.setState({ loading: true });
  }
  // Loader hide show method

 

  // 5 min call 
  push_func(){
    this.props.history.push("/login");
    this.hideLoader();
  }
  // 5 min call 

  // Check Email api (step - 1)
  VerifyEmail_API = () => e => {  
      e.preventDefault();  
      //debugger;
      let step1Errors = {};
      if (this.state["emailcheck"] === '') {
        step1Errors["emailcheck"] = "Please Enter Email"
      }
      if (typeof this.state["emailcheck"] !== "undefined") {
          let lastAtPos = this.state["emailcheck"].lastIndexOf('@');
          let lastDotPos = this.state["emailcheck"].lastIndexOf('.');

          if (!(lastAtPos < lastDotPos && lastAtPos > 0 && this.state["emailcheck"].indexOf('@@') == -1 && lastDotPos > 2 && (this.state["emailcheck"].length - lastDotPos) > 2)) {
              step1Errors["emailcheck"] = "Email is not valid";
          }
      }
      
      this.setState({ errormsg: step1Errors });
        
      if (Object.keys(step1Errors).length > 0) {
          return false;
      }

      this.showLoader();
     
      let bodyarray = {};
      bodyarray["userName"] = this.state["emailcheck"];
      
      var url=process.env.API_API_URL+'ResetPasswordCheckEmail';
      fetch(url, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(bodyarray)
      }).then((response) => response.json())
      .then(data => {

          if (data.responseType === "1") {
              if(data.redirectUrl == '/ResetOtpVerification'){
                this.setState({step: 2});
                this.setState({contactId: data.data.contactId});
                this.setState({auth_token: data.token});
                this.setState({emailcheck: ''});
                //this.setState({ questions: data.data.securityQuestionView })
              }else{
                SystemHelpers.ToastError(data.responseMessge);
              }
              
          }
          else{
              if(data.responseMessge == 'Data Not Found..!'){
                SystemHelpers.ToastError('Email address entered is not registered in PHSS portal.');
              }else{
                SystemHelpers.ToastError(data.responseMessge);  
              }
              
          }
          this.hideLoader();
          
      })
      .catch(error => {
        this.props.history.push("/error-500");
      });

  }
  // Check Email api (step - 1)

  // Check OTP api (step -2)
  VerifyForgotOTP_API = () => e => {  
      e.preventDefault();  
      //debugger;
      
      var otp =this.state["num1"]+this.state["num2"]+this.state["num3"]+this.state["num4"];
      
      let step1Errors = {};
      if (otp.length !=4) {
        step1Errors["otp"] = "Please Enter Valid OTP";
      }
      
      this.setState({ errormsg: step1Errors });
        
      if (Object.keys(step1Errors).length > 0) {
          return false;
      }

      this.showLoader();
     
      let bodyarray = {};
      bodyarray["otp"] = otp;
      bodyarray["contactId"] = this.state.contactId;
      
      var url=process.env.API_API_URL+'ResetPasswordCheckOTP';
      fetch(url, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'bearer '+this.state.auth_token
        },
        body: JSON.stringify(bodyarray)
      }).then((response) => response.json())
      .then(data => {
          //console.log("responseJson ResetPasswordCheckOTP");
          //console.log(data);
          //console.log(responseJson);
          // debugger;
          if (data.responseType === "1") {
              if(data.redirectUrl == '/ResetSecurityQuestion'){
                this.setState({step: 3});
                this.setState({ questions: data.data.securityQuestionView })
                //this.ToastSuccess(data.responseMessge);
              }else{
                SystemHelpers.ToastError(data.responseMessge);
              }
              
          }
          else {
              SystemHelpers.ToastError(data.responseMessge);
          }
          this.hideLoader();
          
      })
      .catch(error => {
        this.props.history.push("/error-500");
      });

  }
  // Check OTP api (step - 2)

  // Check Question Answer api (step -3)
  VerifyQuestionAnswer_API = () => e => {  
      e.preventDefault();  
      //debugger;
      
      let step1Errors = {};
      if (this.state["setQue1"] === 0) {
        step1Errors["setQue1"] = "Please Select Security Question#1";
      }

      var setAns1_strlen = this.state["setAns1"].length;
      if (this.state["setAns1"] === '') {
        step1Errors["setAns1"] = "Please Enter Answer #1";
      }else if (this.state["setAns1"] ===" "){
        step1Errors["setAns1"] = "Space are not allowed Answer #1";
      }

      if (this.state["setQue2"] === 0) {
        step1Errors["setQue2"] = "Please Select Security Question#2";
      }

      var setAns2_strlen = this.state["setAns2"].length;
      if (this.state["setAns2"] === '') {
        step1Errors["setAns2"] = "Please Enter Answer #2";
      }else if (this.state["setAns2"] ===" "){
        step1Errors["setAns2"] = "Space are not allowed Answer #2";
      }

      if (this.state["setQue3"] === 0) {
        step1Errors["setQue3"] = "Please Select Security Question#3";
      }

      var setAns3_strlen = this.state["setAns3"].length;
      if (this.state["setAns3"] === '') {
        step1Errors["setAns3"] = "Please Enter Answer #3";
      }else if (this.state["setAns3"] ===" "){
        step1Errors["setAns3"] = "Space are not allowed Answer #3";
      }

      if(this.state["setQue1"] == this.state["setQue2"])
      {
        step1Errors["setQue2"] = "You must select different a Security Question.";
      }else if(this.state["setQue2"] == this.state["setQue3"]){
        step1Errors["setQue3"] = "You must select different a Security Question.";
      }else if(this.state["setQue1"] == this.state["setQue3"]){
        step1Errors["setQue3"] = "You must select different a Security Question.";
      }
      
      this.setState({ errormsg: step1Errors });
        
      if (Object.keys(step1Errors).length > 0) {
          return false;
      }

      this.showLoader();
      
      let Que_Ans_Json = [
        {
            securityQuestionId: this.state["setQue1"],
            securityQuestion: this.state["setAns1"],
            securityQuestionAns: this.state["setAns1"]
        },
        {
            securityQuestionId: this.state["setQue2"],
            securityQuestion: this.state["setAns2"],
            securityQuestionAns: this.state["setAns2"]
        },
        {
            securityQuestionId: this.state["setQue3"],
            securityQuestion: this.state["setAns3"],
            securityQuestionAns: this.state["setAns3"]
        }
      ];

      let bodyarray = {};
      bodyarray["contactId"] = this.state.contactId;
      bodyarray["securityQuestionViews"] = Que_Ans_Json;
      
      //console.log('bodyarray ResetPasswordCheckQuestionAnswer');
      //console.log(bodyarray);

      var url=process.env.API_API_URL+'ResetPasswordCheckQuestionAnswer';
      fetch(url, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'bearer '+this.state.auth_token
        },
        body: JSON.stringify(bodyarray)
      }).then((response) => response.json())
      .then(data => {
          //console.log("responseJson ResetPasswordCheckQuestionAnswer");
          //console.log(data);
          //console.log(responseJson);
          // debugger;
          if (data.responseType === "1") {
              if(data.redirectUrl == '/ResetPassword'){
                this.setState({step: 4});
                //this.ToastSuccess(data.responseMessge);
              }else{
                SystemHelpers.ToastError(data.responseMessge);
              }
              
          }
          else {
              SystemHelpers.ToastError(data.responseMessge);
          }
          this.hideLoader();
          
      })
      .catch(error => {
        this.props.history.push("/error-500");
      });

  }
  // Check Question Answer api (step -3)

  // Reset New Password api (step -4)
  ResetPassword_API = () => e => {  
      e.preventDefault();  
      //debugger;
      
      let step1Errors = {};
      
      if (this.state["ConfirmNewPassword"] === '') {
        step1Errors["ConfirmNewPassword"] = "Please Enter Password";
      }else if (this.state["NewPassword"] !== this.state["ConfirmNewPassword"]) {
          step1Errors["ConfirmNewPassword"] = "Confirm Password doesn't match"
      }

      if (this.state["NewPassword"] == '') {
          step1Errors["NewPassword"] = "Password is mandatory"
      }else {
          //var regex = /^(?=.*\d)(?=.*[A-Z])(?=.*[a-z])(?=.*[a-zA-Z!#$%&?@ "])[a-zA-Z0-9!#$%&?@]{8,20}$/
          var regex = /(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/;
          if (!(regex.test(this.state["NewPassword"]))) {
            step1Errors["NewPassword"] = "Password should contain one upper, lower, special char and min 8 char";
          }
      }
      
      this.setState({ errormsg: step1Errors });
        
      if (Object.keys(step1Errors).length > 0) {
          return false;
      }

      this.showLoader();
     
      let bodyarray = {};
      bodyarray["newPassword"] = this.state["NewPassword"];
      bodyarray["contactId"] = this.state.contactId;
      
      var url=process.env.API_API_URL+'ResetPassword';
      fetch(url, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'bearer '+this.state.auth_token
        },
        body: JSON.stringify(bodyarray)
      }).then((response) => response.json())
      .then(data => {
          //console.log("responseJson ResetPassword");
          //console.log(data);
          //console.log(responseJson);
          // debugger;
          if (data.responseType === "1") {
              if(data.redirectUrl == '/login'){
                SystemHelpers.ToastSuccess(data.responseMessge);
                setTimeout( () => {this.push_func()}, 5000);
              }else{
                SystemHelpers.ToastError(data.responseMessge);
              }
              
          }
          else {
              SystemHelpers.ToastError(data.responseMessge);
          }
          this.hideLoader();
          
      })
      .catch(error => {
        this.props.history.push("/error-500");
      });

  }
  // Reset New Password api (step -4)

  // Resend OTP
  ResendForgotOTP = () => e => {
    
    this.showLoader();
     
    let bodyarray = {};
    bodyarray["ContactId"] = this.state.contactId;
    bodyarray["otpFor"] = "reset";

    var url=process.env.API_API_URL+'ResendOtP';
    fetch(url, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+this.state.auth_token
      },
      body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
        console.log("responseJson temp_login_token");
        console.log(data);
        //console.log(data.data.userRole);
        //console.log(JSON.parse(JSON.stringify(data.data.userRole)));
        // debugger;
        if (data.responseType === "1") {
            SystemHelpers.ToastSuccess(data.responseMessge);

            

            this.props.history.push('/forgot-password');
        }
        else if (data.responseType === "2") {
          SystemHelpers.ToastError(data.responseMessge);
        }else{
          SystemHelpers.ToastError(data.message);
        }
        this.hideLoader();
        
    })
    .catch(error => {
      this.props.history.push("/error-500");
    });
  }
  // Resend OTP

  step_function(){
    let div_return = [];
    const { step } = this.state;
    const {  setQue1 , setQue2, setQue3, setAns1, setAns2, setAns3 } = this.props;
    if(step == 1){
      div_return.push(
        <div className="account-content">
            
          <div className="container">
            {/* Account Logo */}
            <div className="account-logo">
              <a href="/login"><img src={Applogo} className="pk-logo" alt="PARTICIPATION HOUSE SUPPORT SERVICES" /></a>
            </div>
            {/* /Account Logo */}
            <div className="account-box">
              <div className="account-wrapper">
                <h3 className="account-title">Forgot Password?</h3>
                <p className="account-subtitle">Enter your email to get a password reset link</p>
                {/* Account Form */}
                <form action="/forgotsecurityquestions">
                  <div className="form-group">
                    <label>Email Address</label>
                    <input className="form-control" type="text" onChange={this.handleChange('emailcheck')} />
                    <span class="form-text error-font-color">{this.state.errormsg["emailcheck"]}</span>
                  </div>
                  <div className="form-group text-center">
                    <button className="btn btn-primary account-btn" type="submit" onClick={this.VerifyEmail_API({})}>Reset Password</button>
                  </div>
                  <div className="account-footer">
                    <p>Remember your password? <a href="/login">Login</a></p>
                  </div>
                </form>
                {/* /Account Form */}
              </div>
            </div>
          </div>
        </div>
        
      );
    }

    if(step == 2){
      div_return.push(
        <div className="account-content">
        <div className="container">
          {/* Account Logo */}
          <div className="account-logo">
            <a href="/login"><img src={Applogo} className="pk-logo" alt="PARTICIPATION HOUSE SUPPORT SERVICES" /></a>
          </div>
          {/* /Account Logo */}
          <div className="account-box">
            <div className="account-wrapper">
              <h3 className="account-title">OTP</h3>
              {/*<p className="account-subtitle">Verification your account</p>*/}
              <p className="account-subtitle">Please enter the 4 digit code sent to your email address</p>
              {/* Account Form */}
              
                <div className="otp-wrap">
                  <input type="text" name="ssn-1" placeholder={0} maxLength={1} className="otp-input" onChange={this.handleChangeOtp('num1')} />
                  <input type="text" name="ssn-2" placeholder={0} maxLength={1} className="otp-input" onChange={this.handleChangeOtp('num2')} />
                  <input type="text" name="ssn-3" placeholder={0} maxLength={1} className="otp-input" onChange={this.handleChangeOtp('num3')} />
                  <input type="text" name="ssn-4" placeholder={0} maxLength={1} className="otp-input" onChange={this.handleChangeOtp('num4')} />
                  <span class="form-text error-font-color">{this.state.errormsg["otp"]}</span> 
                </div>
                <div className="form-group text-center">
                  <button className="btn btn-primary account-btn" type="submit" onClick={this.VerifyForgotOTP_API({})} >Enter</button>
                </div>
                <div className="account-footer">
                  <p>Not yet received? <a onClick={this.ResendForgotOTP()} >Resend OTP</a></p>
                </div>
              
              {/* /Account Form */}
            </div>
          </div>
        </div>
      </div>
      );
    }

    if(step == 3){
      div_return.push(
        <div className="account-content account-content-pk">
          <div className="container">
            {/* Account Logo */}
            <div className="account-logo">
              <a href="/login"><img src={Applogo} className="pk-logo" alt="PARTICIPATION HOUSE SUPPORT SERVICES" /></a>
            </div>
            {/* /Account Logo */}
            <div className="account-box account-box-pk">
              <div className="account-wrapper">
                <h3 className="account-title">Forgot Password?</h3>
                <p className="account-subtitle">Select Question and Enter Answer</p>
                {/* Account Form */}
                <form action="/app/main/dashboard">
                  
                  <h4 class="card-title">Security Question</h4>
                  <div className="row">
                    <div className="col-sm-6">
                      <div className="form-group">
                        <label>Question #1 <span class="text-danger">*</span></label>
                        <select className="select" value={setQue1} onChange={this.handleChange('setQue1')}>
                          <option className="select_otp_pk">Select</option>
                          {this.state.questions.map((question) => <option className="select_otp_pk" value={question.securityQuestionId}  >{question.securityQuestion}</option>)}
                        </select>
                        <span class="form-text error-font-color">{this.state.errormsg["setQue1"]}</span>
                      </div>
                    </div>
                    <div className="col-sm-6">
                      <div className="form-group">
                        <label>Answer #1 <span class="text-danger">*</span></label>
                        <input type="text" className="form-control" value={setAns1} onChange={this.handleChange('setAns1')} />
                        <span class="form-text error-font-color">{this.state.errormsg["setAns1"]}</span>
                      </div>
                    </div>
                  </div>
                  

                  <div className="row">
                    
                    <div className="col-sm-6">
                        <div className="form-group">
                          <label>Question #2 <span class="text-danger">*</span></label>
                          <select className="select" value={setQue2} onChange={this.handleChange('setQue2')}>
                            <option>Select</option>
                           {this.state.questions.map((question) => <option value={question.securityQuestionId}  >{question.securityQuestion}</option>)}
                          </select>
                          <span class="form-text error-font-color">{this.state.errormsg["setQue2"]}</span>
                        </div>
                    </div>

                    <div className="col-sm-6">
                      <div className="form-group">
                        <label>Answer #2 <span class="text-danger">*</span></label>
                        <input type="text" className="form-control" value={setAns2} onChange={this.handleChange('setAns2')} />
                        <span class="form-text error-font-color">{this.state.errormsg["setAns2"]}</span>
                      </div>
                    </div>
                    
                    
                  </div>

                  <div className="row">
                    <div className="col-sm-6">
                      <div className="form-group">
                        <label>Question #3 <span class="text-danger">*</span></label>
                        <select className="select" value={setQue3} onChange={this.handleChange('setQue3')}>
                          <option>Select</option>
                          {this.state.questions.map((question) => <option value={question.securityQuestionId}  >{question.securityQuestion}</option>)}
                        </select>
                        <span class="form-text error-font-color">{this.state.errormsg["setQue3"]}</span>
                      </div>
                    </div>

                    <div className="col-sm-6">
                       <div className="form-group">
                          <label>Answer #3 <span class="text-danger">*</span></label>
                          <input type="text" className="form-control" value={setAns3} onChange={this.handleChange('setAns3')} />
                          <span class="form-text error-font-color">{this.state.errormsg["setAns3"]}</span>
                        </div>
                    </div>
                  </div>
                  
                  <div className="row">
                    
                    <div className="col-sm-12">
                      <div class="text-right">
                        <button type="submit" class="btn btn-primary" onClick={this.VerifyQuestionAnswer_API({})} >Submit</button>
                      </div>
                    </div>
                    
                    
                  </div>
                  
                  <div className="account-footer">
                    
                  </div>
                </form>
                {/* /Account Form */}
              </div>
            </div>
          </div>
        </div>
      );
    }

    if(step == 4){
      div_return.push(
        <div className="account-content">
          
          <div className="container">
            {/* Account Logo */}
            <div className="account-logo">
              <a href="/login"><img src={Applogo} className="pk-logo" alt="PARTICIPATION HOUSE SUPPORT SERVICES" /></a>
            </div>
            {/* /Account Logo */}
            <div className="account-box">
              <div className="account-wrapper">
                <h3 className="account-title">Reset Password</h3>
                <p className="account-subtitle"></p>
                {/* Account Form */}
                <form action="/app/main/dashboard">
                
                  <div className="form-group">
                    <label>New password</label>
                    <input className="form-control" type="password" onChange={this.handleChange('NewPassword')} />
                    <span class="form-text error-font-color">{this.state.errormsg["NewPassword"]}</span>
                  </div>
                  <div className="form-group">
                    <label>Confirm Password</label>
                    <input className="form-control" type="password" onChange={this.handleChange('ConfirmNewPassword')} />
                    <span class="form-text error-font-color">{this.state.errormsg["ConfirmNewPassword"]}</span>
                  </div>
                  <div className="form-group text-center">
                    <button className="btn btn-primary account-btn" type="submit"  onClick={this.ResetPassword_API({})}>Reset Password</button>
                  </div>
                  <div className="account-footer">
                    <p>Already have an account? <a href="/login">Login</a></p>
                  </div>
                </form>
                {/* /Account Form */}
              </div>
            </div>
          </div>
        </div>
      );
    }

    return div_return;
  }
   render() {
      return (
        <div>
          <ToastContainer position="top-right"
              autoClose={process.env.API_TOAST_TIMEOUT}
              hideProgressBar={false}
              newestOnTop={false}
              closeOnClick={false}
              rtl={false}
              pauseOnVisibilityChange={false}
              draggable={false}
              pauseOnHover={false} />
          {(this.state.loading) ? <Loader /> : null} {/* Loder method use */}
          <div className="main-wrapper">
            <Helmet>
                    <title>Forgot Password - PARTICIPATION HOUSE SUPPORT SERVICES</title>
                    <meta name="description" content="Login page"/>					
            </Helmet>
            {this.step_function()}
          </div>
        </div>
      );
   }
}


export default ForgotPassword;
