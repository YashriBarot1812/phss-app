
import React, { Component } from 'react';
import { Helmet } from "react-helmet";
import {  Avatar_01 ,Avatar_02,Avatar_03,Avatar_04, Avatar_05, Avatar_08, Avatar_09, Avatar_10,
    Avatar_11,Avatar_12,Avatar_13,Avatar_16   } from "../../Entryfile/imagepath"

import { Table } from 'antd';
import 'antd/dist/antd.css';
import {itemRender,onShowSizeChange} from "./pagination/paginationfunction"
import "../MainPage/antdstyle.css"
import { Multiselect } from 'multiselect-react-dropdown';

import Select from 'react-select-me';
import 'react-select-me/lib/ReactSelectMe.css';

import Header from '../Sidebar/header.jsx';
import SidebarContent from '../Sidebar/sidebar';

import Loader from '../Loader';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import SystemHelpers from '../Helpers/SystemHelper';
import FileUploadHelper from '../Helpers/FileUploadHelper';

import CryptoAES from 'crypto-js/aes';
import CryptoENC from 'crypto-js/enc-utf8';

import moment from 'moment';

import SelectDropdown from 'react-dropdown-select';

//table
import { MDBDataTable } from 'mdbreact';
import '@fortawesome/fontawesome-free/css/all.min.css';
import 'bootstrap-css-only/css/bootstrap.min.css';
import 'mdbreact/dist/css/mdb.css';
//table

import FileUploadPreview from '../FileUpload/FileUploadPreviewNotes'
import DocxImg from '../../assets/img/doc/docx.png'
import ExcelImg from '../../assets/img/doc/excel.png'
import PdfImg from '../../assets/img/doc/pdf.png'

import Datetime from "react-datetime";

class PayrollAdjustmentsAdd extends Component {
  constructor(props) {
    super(props);
    this.state = {

        locationID:localStorage.getItem("primaryLocationId"),
        staffContactID:localStorage.getItem("contactId"),
        fullName:localStorage.getItem("fullName"),

        role_payroll_adjustment_menu_can : {},
        role_hierarchy_can : {},

        errormsg :  '',
        isDelete : false,
        ListGrid : [],

        header_data : [],

        payPeriodListFilter : [],
        locationListFilter : [],
        RoleListFilter : [],

        FilterPayPeriod : '',
        FilterLocation : '',
        FilterEmploymentType : '',
        FilterRole : '',
        TempsearchText:'',

        ExportFilter : {},

        IsEditBtn: false,
        isTimeSheetApproved: false,
        isTimeSheetSubmittedToPayroll:false,
        isPayrollAdmin:false,

        // ============ TimeSheet ============ //
        // Drop down list
        serviceView:[],
        locationViews:[],
        AddPreference:[],
        // Drop down list

        // Add
        AddService:'',
        Addlocation:'',
        AddTimeIn:'',
        AddTimeOut:'',
        AddNoOfHours:'',
        AddPreferenceService:'',
        AddNotes:'',
        // Add

        // Edit
        EditPreference:[],
        // Edit
        // ============ TimeSheet ============ //

        // ============ Payroll Adjustment ============ //
        header_data_PayRollADJListGrid : [],
        ListGrid_PayRollADJ : [],

        // Drop down list
        PayrollADJAdjustmentCodeList : [],
        PayrollADJLocationList : [],
        PayrollADJEntitlementCategoryList : [],
        PayrollADJEntitlementSubCategoryList : [],
        // Drop down list

        // Add
        AddPayrollADJadjustmentCode: '',
        AddPayrollADJlocation: '',
        AddPayrollADJDate: '',
        AddPayrollADJQtyhours: '',
        AddPayrollADJQtyminutes: '',
        AddPayrollADJamount: '',
        AddPayrollADJdescription: '',
        AddPayrollADJsubstractFrom: false,
        AddPayrollADJEntCategory: '',
        AddPayrollADJEntSubCategory: '',
        AddPayrollADJEntQtyhours: '',
        AddPayrollADJEntQtyminutes: '',
        // Add

        // Edit
        EditPayrollADJId: '',
        EditPayrollADJadjustmentCode: '',
        EditPayrollADJlocation: '',
        EditPayrollADJDate: '',
        EditPayrollADJQtyhours: '',
        EditPayrollADJQtyminutes: '',
        EditPayrollADJamount: '',
        EditPayrollADJdescription: '',
        EditPayrollADJsubstractFrom: false,
        EditPayrollADJEntCategory: '',
        EditPayrollADJEntSubCategory: '',
        EditPayrollADJEntQtyhours: '',
        EditPayrollADJEntQtyminutes: '',
        // Edit
        // ============ Payroll Adjustment ============ //
    };

    this.setPropState = this.setPropState.bind(this);
    this.handleChange = this.handleChange.bind(this);
    
    this.onChangeSearch = this.onChangeSearch.bind(this);

    this.handleAddPayrollADJDate = this.handleAddPayrollADJDate.bind(this);
    this.handleEditPayrollADJDate = this.handleEditPayrollADJDate.bind(this);
  }

  handleAddPayrollADJDate = (date) =>{
    console.log('AddPayrollADJDate => '+ date);
    this.setState({ AddPayrollADJDate : date });
  };
  handleEditPayrollADJDate = (date) =>{
    console.log('EditPayrollADJDate => '+ date);
    this.setState({ EditPayrollADJDate : date });
  };

  onChangeSearch(value) {
    console.log(value);
    this.setState({ value });
  }

  setPropState(key, value) {
      this.setState({ [key]: value });
  }

  // Loader hide show method
  hideLoader = () => {
    this.setState({ loading: false });
  }

  showLoader = () => {
    this.setState({ loading: true });
  }
  // Loader hide show method

  handleChange = input => e => {
    this.setState({ [input]: e.target.value });
    //console.log('handleChange');
    //console.log(input);

    if (this.state[input] != '') {
      delete this.state.errormsg[input];
    }

    // Add time
    if([input]=="AddPayrollADJEntCategory")
    {
      this.setState({ PayrollADJEntitlementSubCategoryList: [] });
    
      this.GetEntitlementSubCategoryView(e.target.value);
      this.setState({ AddPayrollADJEntSubCategory: '' });
    }
    if([input]=="AddPayrollADJQtyhours")
    {
      this.setState({ AddPayrollADJEntQtyhours: e.target.value });
    }
    if([input]=="AddPayrollADJQtyminutes")
    {
      this.setState({ AddPayrollADJEntQtyminutes: e.target.value });
    }

    if([input]=="AddPayrollADJsubstractFrom" && $('#AddPayrollADJsubstractFrom').is(":checked"))
    {
      this.setState({ AddPayrollADJsubstractFrom: true });
    }
    else
    {
      this.setState({ AddPayrollADJsubstractFrom: false });
    }
    // Add time

    // Edit time
    if([input]=="EditPayrollADJEntSubCategory")
    {
      this.setState({ PayrollADJEntitlementSubCategoryList: [] });
    
      this.GetEntitlementSubCategoryView(e.target.value);
      this.setState({ EditPayrollADJEntSubCategory: '' });
    }
    if([input]=="EditPayrollADJQtyhours")
    {
      this.setState({ EditPayrollADJEntQtyhours: e.target.value });
    }
    if([input]=="EditPayrollADJQtyminutes")
    {
      this.setState({ EditPayrollADJEntQtyminutes: e.target.value });
    }

    if([input]=="EditPayrollADJsubstractFrom" && $('#EditPayrollADJsubstractFrom').is(":checked"))
    {
      this.setState({ EditPayrollADJsubstractFrom: true });
    }
    else
    {
      this.setState({ EditPayrollADJsubstractFrom: false });
    }
    // Edit time

  }

  ClearRecord = ()=> e => {
    e.preventDefault();

    this.setState({ errormsg: '' });
  }

  TableHeaderDesign(){
    
    
      if(this.state.FilterEmploymentType == "true")
      {
        var columns = [
          {
            label: 'Full Name',
            field: 'fullname',
            sort: 'asc',
            width: 150
          },
          {
            label: 'Primary Location',
            field: 'primarylocation',
            sort: 'asc',
            width: 150
          },
          {
            label: 'Reg Hours',
            field: 'reghours',
            sort: 'asc',
            width: 150
          },
          {
            label: 'Sick hours ',
            field: 'sickhours ',
            sort: 'asc',
            width: 150
          },
          {
            label: 'Sleep Hours',
            field: 'sleephours',
            sort: 'asc',
            width: 150
          },
          {
            label: 'OT Hours',
            field: 'OThours',
            sort: 'asc',
            width: 150
          },
          {
            label: 'TRIP Hours',
            field: 'TRIPhours',
            sort: 'asc',
            width: 150
          },
          {
            label: 'Vacation',
            field: 'vacation',
            sort: 'asc',
            width: 150
          },
          {
            label: 'STAT',
            field: 'stat',
            sort: 'asc',
            width: 150
          },
          {
            label: 'On-Call',
            field: 'oncall',
            sort: 'asc',
            width: 150
          },
          {
            label: 'BTT',
            field: 'btt',
            sort: 'asc',
            width: 150
          },
          {
            label: 'BRV',
            field: 'brv',
            sort: 'asc',
            width: 150
          },
          {
            label: 'Other Earnings Amount',
            field: 'otherearningsamount',
            sort: 'asc',
            width: 150
          },
          {
            label: 'Other Earnings Hours',
            field: 'otherearningshours',
            sort: 'asc',
            width: 150
          },
          {
            label: 'Actions',
            field: 'actions',
            sort: 'asc',
            width: 150
          }
        ];  

        return columns;
      }
      else
      {
        var columns = [
          {
            label: 'Full Name',
            field: 'fullname',
            sort: 'asc',
            width: 150
          },
          {
            label: 'Primary Location',
            field: 'primarylocation',
            sort: 'asc',
            width: 150
          },
          {
            label: 'Reg Hours',
            field: 'reghours',
            sort: 'asc',
            width: 150
          },
          {
            label: 'Sick hours ',
            field: 'sickhours ',
            sort: 'asc',
            width: 150
          },
          {
            label: 'Sleep Hours',
            field: 'sleephours',
            sort: 'asc',
            width: 150
          },
          {
            label: 'OT Hours',
            field: 'OThours',
            sort: 'asc',
            width: 150
          },
          {
            label: 'TRIP Hours',
            field: 'TRIPhours',
            sort: 'asc',
            width: 150
          },
          {
            label: 'Vacation',
            field: 'vacation',
            sort: 'asc',
            width: 150
          },
          {
            label: 'STAT',
            field: 'stat',
            sort: 'asc',
            width: 150
          },
          {
            label: 'On-Call',
            field: 'oncall',
            sort: 'asc',
            width: 150
          },
          {
            label: 'BTT',
            field: 'btt',
            sort: 'asc',
            width: 150
          },
          {
            label: 'BRV',
            field: 'brv',
            sort: 'asc',
            width: 150
          },
          {
            label: 'Other Earnings Amount',
            field: 'otherearningsamount',
            sort: 'asc',
            width: 150
          },
          {
            label: 'Other Earnings Hours',
            field: 'otherearningshours',
            sort: 'asc',
            width: 150
          },
          {
            label: 'Actions',
            field: 'action',
            sort: 'asc',
            width: 150
          }
        ];

        return columns; 
      }
    
  }

  componentDidMount() {
    console.log("Reports");

    /* Role Management */
    console.log('Role Store payroll_adjustment_menu_can');
    var getrole = SystemHelpers.GetRole();
    let payroll_adjustment_menu_can = getrole.payroll_adjustment_menu_can;
    this.setState({ role_payroll_adjustment_menu_can: payroll_adjustment_menu_can });
    console.log(payroll_adjustment_menu_can);

    console.log('Role Store hierarchy_can payroll_adjustment_menu_can');
    var getrole_hierarchy = SystemHelpers.GetRole();
    let hierarchy_can = getrole_hierarchy.hierarchy_can;
    this.setState({ role_hierarchy_can: hierarchy_can });
    console.log(hierarchy_can);
    /* Role Management */
    
    this.GetTimeSheetReportView();
    this.GetEntitlementCategoryView();
    this.GetPayrollAdjustmentCodeMaster();

    this.GetEmployeePayrollAdjustment();
    
  }

  // Drop down Api
  GetEntitlementCategoryView(){
    this.showLoader();
    var url=process.env.API_API_URL+'GetEntitlementCategoryView';
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
    }).then((response) => response.json())
    .then(data => {
        //console.log("responseJson GetEntitlementCategoryView");
        //console.log(data);
        if (data.responseType === "1") {
          this.setState({ PayrollADJEntitlementCategoryList: data.data});
        }else{
          SystemHelpers.ToastError(data.message);   
        }
        this.hideLoader();
    })
    .catch(error => {
      this.props.history.push("/error-500");
    });
  }

  GetEntitlementSubCategoryView(id){
    this.showLoader();
    var url=process.env.API_API_URL+'GetEntitlementSubCategoryView?categoryId='+id;
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
    }).then((response) => response.json())
    .then(data => {
        //console.log("responseJson GetEntitlementSubCategoryView");
        //console.log(data);
        if (data.responseType === "1") {
          this.setState({ PayrollADJEntitlementSubCategoryList: data.data});
        }else{
          SystemHelpers.ToastError(data.message);   
        }
        this.hideLoader();
        
    })
    .catch(error => {
      this.props.history.push("/error-500");
    });
  }

  GetTimeSheetReportView(){

    /* Role Management */
    var getrole = SystemHelpers.GetRole();
    let hierarchyId = getrole.hierarchy_can.hierarchy_can_id;
    /* Role Management */

    this.showLoader();
    var url=process.env.API_API_URL+'GetTimeSheetReportView?loggedInUserId='+this.state.staffContactID+'&rolePriorityId='+hierarchyId;
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
      //body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
      //console.log("responseJson Report GetTimeSheetReportView");
      //console.log(data);
      
      // debugger;
      if (data.responseType === "1") {

        this.setState({ PayrollADJLocationList: data.data.locationViews });
        
      

      }else{
        if(data.message == 'Authorization has been denied for this request.'){
          SystemHelpers.SessionOut();
          this.props.history.push("/login");
        }else{
          SystemHelpers.ToastError(data.message);
        }
      }
      this.hideLoader();
    })
    .catch(error => {
      console.log('GetUserWiseTimeSheetData error');
      this.props.history.push("/error-500");
    });
  }
  
  GetPayrollAdjustmentCodeMaster(){

    /* Role Management */
    var getrole = SystemHelpers.GetRole();
    let hierarchyId = getrole.hierarchy_can.hierarchy_can_id;
    /* Role Management */

    this.showLoader();
    var url=process.env.API_API_URL+'GetPayrollAdjustmentCodeMaster';
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
    }).then((response) => response.json())
    .then(data => {
      console.log("responseJson GetPayrollAdjustmentCodeMaster");
      console.log(data);
      
      // debugger;
      if (data.responseType === "1") {

        this.setState({ PayrollADJAdjustmentCodeList: data.data });
        
      

      }else{
        if(data.message == 'Authorization has been denied for this request.'){
          SystemHelpers.SessionOut();
          this.props.history.push("/login");
        }else{
          SystemHelpers.ToastError(data.message);
        }
      }
      this.hideLoader();
    })
    .catch(error => {
      console.log('GetPayrollAdjustmentCodeMaster error');
      this.props.history.push("/error-500");
    });
  }
  // Drop down Api

  GetPayrollAdjustmentListGrid = () => e => {
    //alert(moment().format('MM/DD/YYYY'));
    //return false;

      e.preventDefault();

      this.setState({ ListGrid : [] });
      this.setState({ ExportFilter: {} });

      /* Role Management */
      var getrole = SystemHelpers.GetRole();
      /* Role Management */

      let step1Errors = {};
    
      if (this.state["FilterEmploymentType"] =='') {
        step1Errors["FilterEmploymentType"] = "Please select Employment Type.";
      }

      this.setState({ errormsg: step1Errors });
        
      if (Object.keys(step1Errors).length > 0) {
          return false;
      }

      var FilterPayPeriod = this.state.FilterPayPeriod;
      var FilterLocation = this.state.FilterLocation;
      var FilterEmploymentType = this.state.FilterEmploymentType;
      var FilterRole = this.state.FilterRole;

      var paraPayPeriod = 'periodProfileId='+this.state.FilterPayPeriod;
      var paraLocation = '&locationId='+this.state.FilterLocation;
      var paraEmploymentType = '&employeementType='+this.state.FilterEmploymentType;
      var paraContactId = '&contactId=';
      var paraRoleId = '&roleId='+this.state.FilterRole;
      var paraneedToExport = '&needToExport=false';
      
      //ExportFilter
      let ExportFilterArray = {
        FilterPayPeriod: this.state.FilterPayPeriod,
        FilterLocation: this.state.FilterLocation,
        FilterEmploymentType: this.state.FilterEmploymentType,
        FilterEmployee: "",
        FilterRole: this.state.FilterRole
      };
      this.setState({ ExportFilter: ExportFilterArray });
      //ExportFilter

      if(this.state.FilterEmploymentType == "false"){
        var ApiName= 'GetPTBatchPayrollReport?'
      }else{
        var ApiName= 'GetFTBatchPayrollReport?'
      }

      var pass_url = ApiName+paraPayPeriod+paraLocation+paraEmploymentType+paraContactId+paraneedToExport;
      console.log(pass_url);
      //return false;

      this.showLoader();
      var url=process.env.API_API_URL+pass_url;
      fetch(url, {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'bearer '+localStorage.getItem("token")
        },
      }).then((response) => response.json())
      .then(data => {
          console.log("responseJson GetPTBatchPayrollReport GetFTBatchPayrollReport");
          console.log(data);
          if (data.responseType === "1") {
              if(data.data != null)
              {
                this.setState({ header_data: this.TableHeaderDesign() });
                this.setState({ ListGrid: this.rowData(data.data) });
              }
              this.hideLoader();
          }else{
            if(data.message == 'Authorization has been denied for this request.'){
              SystemHelpers.SessionOut();
              this.props.history.push("/login");
            }else{
              SystemHelpers.ToastError(data.message);
            }
            this.hideLoader();    
          }
          
          
      })
      .catch(error => {
        this.props.history.push("/error-500");
      });
  }
  // ============ TimeSheet ============ //
  // ============ TimeSheet ============ //

  // ============ PayRoll Adjustmenr ============ //
  TableHeaderDesignPayRollADJ(){
    var columnsPayRollADJ = [
      {
        label: 'Payroll Adjustment Code',
        field: 'payrollAdjustmentCode',
        sort: 'asc',
        width: 150
      },
      {
        label: 'Location',
        field: 'location',
        sort: 'asc',
        width: 150
      },
      {
        label: 'Date',
        field: 'date',
        sort: 'asc',
        width: 150
      },
      {
        label: 'Description',
        field: 'description',
        sort: 'asc',
        width: 150
      },
      {
        label: 'Hours',
        field: 'hours',
        sort: 'asc',
        width: 150
      },
      {
        label: 'Amount',
        field: 'amount',
        sort: 'asc',
        width: 150
      },
      {
        label: 'Entitlement Category',
        field: 'entitlementCategory',
        sort: 'asc',
        width: 150
      },
      {
        label: 'Reason',
        field: 'entitlementSubCatrgory',
        sort: 'asc',
        width: 150
      },
      {
        label: 'Hours',
        field: 'hours',
        sort: 'asc',
        width: 150
      },
      {
        label: 'Action',
        field: 'action',
        sort: 'asc',
        width: 150
      }
    ];  

    return columnsPayRollADJ;
  }

  GetEmployeePayrollAdjustment(){
    this.showLoader();

    /* Role Management */
    var getrole = SystemHelpers.GetRole();
    let canDelete = true;
    /* Role Management */
    var currentPage=1;
    var pageSize=5;
    var searchText="";
    var sort_Column="";
    var Sort_Type=false;
    var IsSortingEnabled=true;

    var url_paging_para = '&pageNumber='+currentPage+'&pageSize='+pageSize+'&searchText='+searchText+'&sortColumn='+sort_Column+'&SortType='+Sort_Type+'&IsSortingEnabled='+IsSortingEnabled;

    var url=process.env.API_API_URL+'GetEmployeePayrollAdjustment?contactId='+this.state.staffContactID+'&canDelete='+canDelete+url_paging_para;
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
    }).then((response) => response.json())
    .then(data => {
        console.log("responseJson GetEmployeePayrollAdjustment");
        console.log(data);
        if (data.responseType === "1") {
          this.setState({ header_data_PayRollADJListGrid: this.TableHeaderDesignPayRollADJ() });
          this.setState({ ListGrid_PayRollADJ: this.rowData_PayRollADJ(data.data) });
        }else{
          SystemHelpers.ToastError(data.message);   
        }
        this.hideLoader();
    })
    .catch(error => {
      this.props.history.push("/error-500");
    });
  }

  rowData_PayRollADJ(ListGridPayRollADJ) {
    console.log('rowData PayRollADJListGrid');
    console.log(ListGridPayRollADJ);

    var ListGridPayRollADJ_length = ListGridPayRollADJ.length;
    let dataArray = [];
    //console.log(ListGridPayRollADJ_length);
    //if(ListGridPayRollADJ_length > 0)
    //{
      //console.log(ListGridPayRollADJ_length);
      for (var x = 0; x < ListGridPayRollADJ_length; x++)
      {
        //console.log(ListGridPayRollADJ_length);
        //if(ListGridPayRollADJ != null)
        //{ 
            console.log(ListGridPayRollADJ);
            var tempdataArray = [];

            
              
            tempdataArray.payrollAdjustmentCode = ListGridPayRollADJ[x].payrollAdjustmentCodeName;
            tempdataArray.location = ListGridPayRollADJ[x].locationName;

            if(ListGridPayRollADJ[x].payrollAdjustmentDate !="" && ListGridPayRollADJ[x].payrollAdjustmentDate!=null){
              tempdataArray.date = moment(ListGridPayRollADJ[x].payrollAdjustmentDate,process.env.API_DATE_FORMAT).format(process.env.DATE_FORMAT);
            }else{
              tempdataArray.date = "";
            }
            
            tempdataArray.description = ListGridPayRollADJ[x].description;
            tempdataArray.hours = ListGridPayRollADJ[x].noOfHours;
            tempdataArray.amount = ListGridPayRollADJ[x].amount;
            tempdataArray.entitlementCategory = ListGridPayRollADJ[x].entitlementCatrgoryName;
            tempdataArray.entitlementSubCatrgory = ListGridPayRollADJ[x].entitlementSubCatrgoryName;
            tempdataArray.hours = ListGridPayRollADJ[x].noOfHours;
            
            tempdataArray.action = this.Edit_Update_Btn_Func_PayrollAdj(ListGridPayRollADJ[x]);

            dataArray.push(tempdataArray);
            
            
            
        //}
    
      }
    //}
    console.log('Return rowdata array');
    console.log(dataArray);
    return dataArray;
  }

  AddRecordPayrollAdj = () => e => {
      e.preventDefault();

      let step1Errors = {};
      
      if (this.state["AddPayrollADJadjustmentCode"] == '' || this.state["AddPayrollADJadjustmentCode"] == null) {
        step1Errors["AddPayrollADJadjustmentCode"] = "Adjustment Code is mandatory";
      }

      if (this.state["AddPayrollADJlocation"] == '' || this.state["AddPayrollADJlocation"] == null) {
        step1Errors["AddPayrollADJlocation"] = "Location is mandatory";
      } 
      
      if (this.state["AddPayrollADJDate"] == '' || this.state["AddPayrollADJDate"] == null) {
        step1Errors["AddPayrollADJDate"] = "Date is mandatory";
      }

      if (this.state["AddPayrollADJQtyhours"] == '' || this.state["AddPayrollADJQtyhours"] == null) {
        step1Errors["AddPayrollADJQtyhours"] = "Hours is mandatory";
      }
      if (this.state["AddPayrollADJQtyminutes"] == '' || this.state["AddPayrollADJQtyminutes"] == null) {
        step1Errors["AddPayrollADJQtyminutes"] = "Minutes is mandatory";
      }
      
      if (this.state["AddPayrollADJamount"] == '' || this.state["AddPayrollADJamount"] == null) {
        step1Errors["AddPayrollADJamount"] = "Amount is mandatory";
      }

      if (this.state["AddPayrollADJdescription"] == '' || this.state["AddPayrollADJdescription"] == null) {
        step1Errors["AddPayrollADJdescription"] = "Description is mandatory";
      }

      if (this.state["AddPayrollADJEntCategory"] == '' || this.state["AddPayrollADJEntCategory"] == null) {
        step1Errors["AddPayrollADJEntCategory"] = "Entitlement Category is mandatory";
      }

      if (this.state["AddPayrollADJEntSubCategory"] == '' || this.state["AddPayrollADJEntSubCategory"] == null) {
        step1Errors["AddPayrollADJEntSubCategory"] = "Reason is mandatory";
      }

      if (this.state["AddPayrollADJEntQtyhours"] == '' || this.state["AddPayrollADJEntQtyhours"] == null) {
        step1Errors["AddPayrollADJEntQtyhours"] = "Hours is mandatory";
      }

      if (this.state["AddPayrollADJEntQtyminutes"] == '' || this.state["AddPayrollADJEntQtyminutes"] == null) {
        step1Errors["AddPayrollADJEntQtyminutes"] = "Minutes is mandatory";
      }

      /*if(this.state.IsViewHistoryContactApplyAddRecord == false)
      {
        if(this.state.AddEmploymentHoursFieldCheckbox == false && this.state.AddLocationFieldCheckbox == false)
        {
          step1Errors["AddFieldErrorMsg"] = "Atleast one option from Applies to section should be selected.";
        }
      }*/

      console.log(step1Errors);

      this.setState({ errormsg: step1Errors });
          
      if (Object.keys(step1Errors).length > 0) {
        return false;
      }

      this.showLoader();

      var add_PayrollADJDate ='';
      alert(this.state["AddPayrollADJDate"]);
      if (this.state["AddPayrollADJDate"] != '' && this.state["AddPayrollADJDate"] != null)
      {
        var add_PayrollADJDate=moment(this.state["AddPayrollADJDate"]).format('MM-DD-YYYY');
      }
      
      let bodyarray = {};
      bodyarray["contactId"] = this.state.staffContactID;
      bodyarray["loggedInUserId"] = this.state.staffContactID;
      bodyarray["loggedInUserByName"] = this.state.fullName;

      bodyarray["adjustmentCodeId"] = this.state["AddPayrollADJadjustmentCode"];
      bodyarray["locationIdGuid"] = this.state["AddPayrollADJlocation"];
      bodyarray["payrollAdjustmentDate"] = add_PayrollADJDate;
      bodyarray["noOfHours"] = this.state["AddPayrollADJQtyhours"]+':'+this.state["AddPayrollADJQtyminutes"];
      bodyarray["amount"] = this.state["AddPayrollADJamount"];
      bodyarray["description"] = this.state["AddPayrollADJdescription"];
      bodyarray["substractFrom"] = this.state["AddPayrollADJsubstractFrom"];
      bodyarray["entitlementCategoryId"] = this.state["AddPayrollADJEntSubCategory"];
      bodyarray["entitlementSubCategoryId"] = this.state["AddPayrollADJEntSubCategory"];
      bodyarray["noOfHours"] = this.state["AddPayrollADJEntQtyhours"]+':'+this.state["AddPayrollADJEntQtyminutes"];
      
      console.log(bodyarray);
      //return false;
      var url=process.env.API_API_URL+'CreateUpdatePayrollAdjustment';
      fetch(url, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'bearer '+localStorage.getItem("token")
        },
        body: JSON.stringify(bodyarray)
      }).then((response) => response.json())
      .then(data => {
          console.log("responseJson CreateUpdatePayrollAdjustment");
          console.log(data);
          if (data.responseType === "1") {
            SystemHelpers.ToastSuccess(data.responseMessge);  
            $( ".close" ).trigger( "click" ); 
            this.ClearRecord();
            //this.GetUsersEntitlementBalanceView(this.state.currentPage,this.state.pageSize,this.state.searchText);
            this.GetEmployeePayrollAdjustment();
          }else if (data.responseType === "2" || data.responseType === "3" || data.responseType === "4") {
            SystemHelpers.ToastError(data.responseMessge);  
          } else{
            SystemHelpers.ToastError(data.message);
          }
          this.hideLoader();
      })
      .catch(error => {
        this.props.history.push("/error-500");
      });
  }

  Edit_Update_Btn_Func_PayrollAdj(record){
    let return_push = [];

    if(this.state.role_payroll_adjustment_menu_can.payroll_adjustment_menu_can_view == true){
        let Edit_push = [];
        if(this.state.role_payroll_adjustment_menu_can.payroll_adjustment_menu_can_view == true){
          Edit_push.push(
          <a href="#" onClick={this.EditRecord_PayrollAdj(record)}  className="dropdown-item" data-toggle="modal" data-target="#edit_location"><i className="fa fa-pencil m-r-5"></i>Edit</a>
          );
        }
        let Delete_push = [];
        if(this.state.role_emergency_contact_can.emergency_contact_can_delete == true){
          if(emgContactId.isDelete == false)
          {  
            Delete_push.push(
              <a href="#" onClick={this.EditRecord_PayrollAdj(emgContactId)} data-toggle="modal" data-target="#delete_emergency_contact" className="dropdown-item"><i className="fa fa-trash-o m-r-5" ></i> Inactive</a>
            );
          }
          else
          {
            Delete_push.push(
              <a href="#" onClick={this.EditRecord_PayrollAdj(emgContactId)} data-toggle="modal" data-target="#delete_emergency_contact" className="dropdown-item"><i className="fa fa-trash-o m-r-5" ></i> Active</a>
            );
          }
        }
        
        return_push.push(
          <div className="dropdown dropdown-action">
            <a aria-expanded="false" data-toggle="dropdown" className="action-icon dropdown-toggle" href="#"><i className="material-icons">more_vert</i></a>
            <div className="dropdown-menu dropdown-menu-right">
              {Edit_push}
              {Delete_push}
            </div>
          </div>
        );
      }
      return return_push;
  }

  EditRecord_PayrollAdj = (record) => e => {
    e.preventDefault();
    //console.log('Holiday EditRecord');
    //console.log(record);

    this.setState({ EditPayrollADJId: record.addressId });
    this.setState({ EditPayrollADJadjustmentCode: record.addressType });
    this.setState({ EditPayrollADJlocation: record.street });

    if(record.endDate != ""){
      this.setState({ EditPayrollADJDate: record.unit });
    }else{
      this.setState({ EditPayrollADJDate: "" });
    }

    this.setState({ EditPayrollADJQtyhours: record.streetName });
    this.setState({ EditPayrollADJQtyminutes: record.countryId });
    this.setState({ EditPayrollADJamount: record.provinceId });
    this.setState({ EditPayrollADJdescription: record.cityId });
    this.setState({ EditPayrollADJsubstractFrom: record.postalCode });
    this.setState({ EditPayrollADJEntCategory: record.streetName2 });
    this.setState({ EditPayrollADJEntSubCategory: record.streetName2 });
    this.setState({ EditPayrollADJEntQtyhours: record.streetName2 });
    this.setState({ EditPayrollADJEntQtyminutes: record.streetName2 });
    
    this.GetEntitlementSubCategoryView(record.countryId);
  }

  UpdateRecordPayrollAdj = () => e => {
      e.preventDefault();

      let step1Errors = {};
      
      if (this.state["EditPayrollADJadjustmentCode"] == '' || this.state["EditPayrollADJadjustmentCode"] == null) {
        step1Errors["EditPayrollADJadjustmentCode"] = "Adjustment Code is mandatory";
      }

      if (this.state["EditPayrollADJlocation"] == '' || this.state["EditPayrollADJlocation"] == null) {
        step1Errors["EditPayrollADJlocation"] = "Location is mandatory";
      } 
      
      if (this.state["EditPayrollADJDate"] == '' || this.state["EditPayrollADJDate"] == null) {
        step1Errors["EditPayrollADJDate"] = "Date is mandatory";
      }

      if (this.state["EditPayrollADJQtyhours"] == '' || this.state["EditPayrollADJQtyhours"] == null) {
        step1Errors["EditPayrollADJQtyhours"] = "Hours is mandatory";
      }
      if (this.state["EditPayrollADJQtyminutes"] == '' || this.state["EditPayrollADJQtyminutes"] == null) {
        step1Errors["EditPayrollADJQtyminutes"] = "Minutes is mandatory";
      }
      
      if (this.state["EditPayrollADJamount"] == '' || this.state["EditPayrollADJamount"] == null) {
        step1Errors["EditPayrollADJamount"] = "Amount is mandatory";
      }

      if (this.state["EditPayrollADJdescription"] == '' || this.state["EditPayrollADJdescription"] == null) {
        step1Errors["EditPayrollADJdescription"] = "Description is mandatory";
      }

      if (this.state["EditPayrollADJEntCategory"] == '' || this.state["EditPayrollADJEntCategory"] == null) {
        step1Errors["EditPayrollADJEntCategory"] = "Entitlement Category is mandatory";
      }

      if (this.state["EditPayrollADJEntSubCategory"] == '' || this.state["EditPayrollADJEntSubCategory"] == null) {
        step1Errors["EditPayrollADJEntSubCategory"] = "Reason is mandatory";
      }

      if (this.state["EditPayrollADJEntQtyhours"] == '' || this.state["EditPayrollADJEntQtyhours"] == null) {
        step1Errors["EditPayrollADJEntQtyhours"] = "Hours is mandatory";
      }

      if (this.state["EditPayrollADJEntQtyminutes"] == '' || this.state["EditPayrollADJEntQtyminutes"] == null) {
        step1Errors["EditPayrollADJEntQtyminutes"] = "Minutes is mandatory";
      }

      console.log(step1Errors);

      this.setState({ errormsg: step1Errors });
          
      if (Object.keys(step1Errors).length > 0) {
        return false;
      }

      this.showLoader();

      var Edit_PayrollADJDate ='';
      alert(this.state["EditPayrollADJDate"]);
      if (this.state["EditPayrollADJDate"] != '' && this.state["EditPayrollADJDate"] != null)
      {
        var Edit_PayrollADJDate=moment(this.state["EditPayrollADJDate"]).format('MM-DD-YYYY');
      }
      
      let bodyarray = {};
      bodyarray["contactId"] = this.state.staffContactID;
      bodyarray["loggedInUserId"] = this.state.staffContactID;
      bodyarray["loggedInUserByName"] = this.state.fullName;

      bodyarray["payrollAdjustMentId"] = this.state["EditPayrollADJId"];
      bodyarray["adjustmentCodeId"] = this.state["EditPayrollADJadjustmentCode"];
      bodyarray["locationIdGuid"] = this.state["EditPayrollADJlocation"];
      bodyarray["payrollAdjustmentDate"] = Edit_PayrollADJDate;
      bodyarray["noOfHours"] = this.state["EditPayrollADJQtyhours"]+':'+this.state["EditPayrollADJQtyminutes"];
      bodyarray["amount"] = this.state["EditPayrollADJamount"];
      bodyarray["description"] = this.state["EditPayrollADJdescription"];
      bodyarray["substractFrom"] = this.state["EditPayrollADJsubstractFrom"];
      bodyarray["entitlementCategoryId"] = this.state["EditPayrollADJEntSubCategory"];
      bodyarray["entitlementSubCategoryId"] = this.state["EditPayrollADJEntSubCategory"];
      bodyarray["noOfHours"] = this.state["EditPayrollADJEntQtyhours"]+':'+this.state["EditPayrollADJEntQtyminutes"];
      
      console.log(bodyarray);
      //return false;
      var url=process.env.API_API_URL+'CreateUpdatePayrollAdjustment';
      fetch(url, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'bearer '+localStorage.getItem("token")
        },
        body: JSON.stringify(bodyarray)
      }).then((response) => response.json())
      .then(data => {
          console.log("responseJson CreateUpdatePayrollAdjustment");
          console.log(data);
          if (data.responseType === "1") {
            SystemHelpers.ToastSuccess(data.responseMessge);  
            $( ".close" ).trigger( "click" ); 
            this.ClearRecord();
            //this.GetUsersEntitlementBalanceView(this.state.currentPage,this.state.pageSize,this.state.searchText);
            this.GetEmployeePayrollAdjustment();
          }else if (data.responseType === "2" || data.responseType === "3" || data.responseType === "4") {
            SystemHelpers.ToastError(data.responseMessge);  
          } else{
            SystemHelpers.ToastError(data.message);
          }
          this.hideLoader();
      })
      .catch(error => {
        this.props.history.push("/error-500");
      });
  }
  // ============ PayRoll Adjustmenr ============ //
  render() {

    const data_PayRollADJ = {
      columns: this.state.header_data_PayRollADJListGrid,
      rows: this.state.ListGrid_PayRollADJ
    };

    return ( 
      <div className="main-wrapper">
     
        {/* Toast & Loder method use */}
          
        {(this.state.loading) ? <Loader /> : null} 
        {/* Toast & Loder method use */}
        <Header/>

        
          <div className="page-wrapper">  
             

              

              {/* Add Reports Modal */}
                {/* Current Time Staff Modal */}
                  <div id="View_PayrollAdjustments" data-backdrop="static" className="modal custom-modal fade" role="dialog">
                    <div className="modal-dialog modal-dialog-centered modal-xl" role="document">
                      <div className="modal-content">

                        <div className="modal-header">
                          <h3 className="card-title">Time Sheet Entries</h3>
                          <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                          </button>
                        </div>

                        <div className="modal-body modal-body-past-details">
                          <div className=""> {/* className="card" */}
                            <div className="card-body">

                              {/*Time Sheet Entries Grid*/}
                              <div className="row">
                                  <div className="card col-lg-12">
                                    {( this.state.IsEditBtn == true ||this.state.isTimeSheetApproved == false || (this.state.isTimeSheetSubmittedToPayroll == false && this.state.isPayrollAdmin == true)) ?
                                      <h3 className="card-title"><a href="#" className="edit-icon" data-toggle="modal" data-target="#CurrentTimeStaff_Add_modal"><i className="fa fa-plus" /></a></h3>
                                    : null }
                                   
                                    <div className="table-responsive">
                                      <table className="table table-nowrap">
                                        <thead>
                                         <tr>
                                            <th>Date</th>
                                            <th>Service Name</th>
                                            <th>Time In</th>
                                            <th>Time Out</th>
                                            <th>Location</th>
                                            <th>Note</th>
                                            <th>No of hours</th>
                                            <th>Record Type</th>
                                            <th>CreatedOn</th>
                                            <th>Action</th>
                                            
                                            
                                          </tr>
                                        </thead>
                                        <tbody>
                                          
                                          <tr>
                                            <td ></td>
                                            <td ></td>
                                            <td ></td>
                                            <td ></td>
                                            <td ></td>
                                            <td ></td>
                                            <td ></td>
                                            <td ></td>
                                            <td ></td>
                                            <td></td>
                                          </tr>
                                                
                                          <tr><td className="pk-td-border" colSpan="6"> Total hours </td>
                                              <td></td>
                                              <td></td>
                                              <td></td>
                                              <td></td>
                                              
                                              
                                          </tr>
                                        </tbody>
                                      </table>
                                      
                                    </div>
                                  </div>
                              </div>
                              {/*Time Sheet Entries Grid*/}
                              <br/><br/>

                              {/*Summary Of Entries*/}
                              <div className="row">
                                <div className="col-md-6 col-lg-6 col-xl-6 d-flex">
                                  <div className="card flex-fill dash-statistics">
                                    <div className="card-body">
                                      <h5 className="card-title">Summary of Entries</h5>
                                        <div className="col-md-12">
                                          <div className="col-md-6 float-left">
                                            <div className="title">Total Regular Hours: 00:00</div>
                                            <div className="title">Total Sick Hours: 00:00</div>
                                            <div className="title">Total OT Hours: 00:00</div>
                                            <div className="title">Total Sleep Hours: 00:00</div>
                                            <div className="title">Total TRIP Hours: 00:00</div>
                                          </div>
                                          <div className="col-md-6 float-right">
                                            <div className="title">Total Brv Hours: 00:00</div>
                                            <div className="title">Total Leave Hours: 00:00</div>
                                            <div className="title">Total On-call: 00:00</div>
                                            <div className="title">Total STAT Hours: 00:00</div>
                                            <div className="title"></div>
                                          </div>
                                        </div>
                                    </div>
                                  </div>
                                </div>

                                <div className="col-md-6 col-lg-6 col-xl-6 d-flex">
                                  <div className="card flex-fill dash-statistics">
                                    <div className="card-body">
                                      <h5 className="card-title">Overtime Summary</h5>
                                        <div className="col-md-12">
                                          <div className="col-md-6 float-left">
                                            <div className="title">Banked Time Used: 00:00</div>
                                            <div className="title">Banked Time Earned: 00:00</div>
                                            <div className="title">Special Banked Time: 00:00</div>
                                            <div className="title">OT Paid: 00:00</div>
                                            <div className="title"></div>
                                            <div className="title"></div>
                                            <div className="title">Total banked time hours: 00:00</div>
                                          </div>
                                          <div className="col-md-6 float-right">
                                            <div className="title"> </div>
                                            <div className="title">Straight time Hours: 00:00</div>
                                            <div className="title"> </div>
                                            <div className="title"> </div>
                                            <div className="title"> </div>
                                            <div className="title"> </div>
                                            <div className="title"> </div>
                                          </div>
                                        </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              {/*Summary Of Entries*/}
                              <br/><br/>

                              {/*Payroll Adjustments Grid*/}
                              <div className="row">
                                  <div className="card col-lg-12">
                                    <div className="card-body">
                                      <h3 className="card-title">Payroll Adjustments</h3>
                                      <h3 className="card-title"><a href="#" className="edit-icon" data-toggle="modal" data-target="#PayrollAdjustments_Add_modal"><i className="fa fa-plus" /></a></h3>
                                      <div className="table-responsive">
                                        
                                        <MDBDataTable
                                          striped
                                          bordered
                                          small
                                          data={data_PayRollADJ}
                                          entriesOptions={[5, 10, 20, 50, 100]} entries={5} pagesAmount={4}
                                          className="table table-striped custom-table mb-0 datatable"
                                        />
                                        
                                      </div>
                                    </div>
                                  </div>
                              </div>
                              {/*Payroll Adjustments Grid*/}
                              <br/><br/>

                              {/*Notes*/}
                                <div>
                                  <div className="row">
                                    <div className="card col-lg-8">
                                      <div className="card-body">
                                        <h3 class="card-title">Notes</h3>
                                        {/* Chat Main Row */}
                                        <div className="chat-main-row">
                                          {/* Chat Main Wrapper */}
                                          <div className="chat-main-wrapper">
                                            {/* Chats View */}
                                            <div className="col-lg-9 message-view task-view">
                                              <div className="chat-window">
                                              
                                                <div className="chat-contents">
                                                  <div className="chat-content-wrap">
                                                    <div className="chat-wrap-inner">
                                                      <div className="chat-box">
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>
                                                <div className="chat-footer chat-footer-pk">
                                                  <div className="message-bar">
                                                    <div className="message-inner">
                                                      {/*<a className="link attach-icon" href="#" data-toggle="modal" data-target="#drag_files"><img src={Attachment} alt="" /></a>*/}
                                                      <div className="message-area">
                                                        <div className="input-group">
                                                          <textarea className="form-control" placeholder="Type Notes..." value={this.state.AddMsg} onChange={this.handleChange('AddMsg')} />
                                                        </div>
                                                        <span className="form-text error-font-color">{this.state.errormsg["AddMsg"]}</span>
                                                      </div>
                                                    </div>
                                                  </div>
                                                  <div className="project-members">
                                                    <div className="col-md-12 notes-padding-left">
                                                      <div className="form-group">
                                                        <div className="checkbox">
                                                          <label>
                                                            <input type="checkbox" name="allow_emp" value="true" onChange={this.handleChange('allow_emp')}  checked={this.state.allow_emp == true ? "true" : null} /> Allow the employees to see the notes
                                                          </label>
                                                        </div>
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                            {/* /Chats View */}
                                          </div>
                                          {/* /Chat Main Wrapper */}
                                        </div>
                                        {/* /Chat Main Row */}
                                      </div>
                                    </div>
                                    <div className="card col-md-4 notes-padding-left">
                                      <div className="form-group">
                                        <label>Attachment</label>
                                        <FileUploadPreview
                                          ResetFileMethod={this.state.AddNotesResetflag}
                                          className="form-control" 
                                          setPropState={this.setPropState} />
                                          <input type="hidden" name="filePreviewsFinalEditActive" id="filePreviewsFinalEditActive" value={this.state.filePreviewsFinalEditActive}/>
                                        <span className="form-text success-font-color Guidelines_Doc">{process.env.ATTACHMENT_GUIDELINES}</span>
                                        <span className="form-text error-font-color">{this.state.errormsg["Addattachment"]}</span>
                                      </div>
                                    </div>
                                  </div>
                                  {/*<div className="row">
                                    <div className="col-md-12 notes-padding-left">
                                      <div className="form-group">
                                        <label>Attachment</label>
                                        <FileUploadPreview
                                          ResetFileMethod={this.state.AddNotesResetflag}
                                          className="form-control" 
                                          setPropState={this.setPropState} />
                                          <input type="hidden" name="filePreviewsFinalEditActive" id="filePreviewsFinalEditActive" value={this.state.filePreviewsFinalEditActive}/>
                                        <span className="form-text success-font-color Guidelines_Doc">{process.env.ATTACHMENT_GUIDELINES}</span>
                                        <span className="form-text error-font-color">{this.state.errormsg["Addattachment"]}</span>
                                      </div>
                                    </div>
                                  </div>*/}
                                </div>
                                <div className="submit-section">
                                  <button className="btn btn-primary submit-btn" >Save</button>
                                </div>
                              {/*Notes*/}

                            </div>
                          </div>   
                        </div>

                      </div>
                    </div>
                  </div>
                {/* //Current Time Staff Modal */}
              {/* /Add Reports Modal */}

              {/* ADD Reports Modal */}
              {/* /ADD Reports Modal */}

              {/* ===================================== Modal ===================================== */}
                {/* /ADD Current Time Staff Tab Modals */}
                  <div id="CurrentTimeStaff_Add_modal" data-backdrop="static" className="modal custom-modal" role="dialog">
                    <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
                      <div className="modal-content">
                        <div className="modal-header">
                          <h5 className="modal-title">Time Sheet Entry</h5>
                          <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={this.ClearRecord()}>
                            <span aria-hidden="true">×</span>
                          </button>
                        </div>
                        <div className="modal-body">
                         
                            <div className="card">
                              <div className="card-body">
                                <div className="row">
                                  <div className="col-md-6">
                                    <div className="form-group">
                                      <label>Service Name <span className="text-danger">*</span></label>
                                      <select className="form-control" id="AddService" value={this.state.AddService} onChange={this.handleChange('AddService')}>
                                        <option value="">-</option>
                                        {/*{this.state.serviceView.map(( listValue, index ) => {
                                          return (
                                            <option key={index} data-basserivce={listValue.isBasedOnSerivceBase} value={listValue.serviceId}>{listValue.serviceName}</option>
                                          );
                                        })}*/}
                                      </select>
                                      <span className="form-text error-font-color">{this.state.errormsg["AddService"]}</span>
                                    </div>
                                  </div>
                                  <div className="col-md-6">
                                    <div className="form-group">
                                      <label>Location <span className="text-danger">*</span></label>
                                      <select className="form-control" id="Addlocation" value={this.state.Addlocation} onChange={this.handleChange('Addlocation')}>
                                        <option value="">-</option>
                                        {/*{this.state.locationViews.map(( listValue, index ) => {
                                          return (
                                            <option key={index} value={listValue.locationGuid}>{listValue.locationName}</option>
                                          );
                                        })}*/}
                                      </select>
                                      <span className="form-text error-font-color">{this.state.errormsg["Addlocation"]}</span>
                                    </div>
                                  </div>

                                  <div className="col-md-6">
                                    <div className="form-group">
                                      <label>Time in <span className="text-danger">*</span></label>
                                      <div className="row">
                                        {/*<div className="col-md-6">
                                          <input className="form-control" type="datetime-local" min={this.state.AddTimeIn_Min} max={this.state.AddTimeIn_Max} value={this.state.AddTimeIn} onChange={this.handleChange('AddTimeIn')}  /> 
                                        </div> */ }
                                        <div className="col-md-12">
                                          <Datetime
                                            
                                            isValidDate={this.validationDateAddIn}
                                            inputProps={{readOnly: true}}
                                            closeOnTab={true}
                                            input={true}
                                            value={(this.state.AddTimeIn) ? this.state.AddTimeIn : ''}
                                            onChange={this.handleDateAddIn}
                                            dateFormat={process.env.DATE_FORMAT}
                                            timeFormat={process.env.TIME_FORMAT}
                                            timeConstraints={{
                                              hours: { min: 0, max: 23 },
                                              minutes: { min: 0, max: 59, step: 15 }
                                            }}
                                            renderInput={(props) => {
                                               return <input {...props} value={(this.state.AddTimeIn) ? props.value : ''} />
                                            }}
                                          />
                                        </div>
                                      </div>
                                      <span className="form-text error-font-color">{this.state.errormsg["AddTimeIn"]}</span>
                                    </div>
                                  </div>
                                  <div className="col-md-6">
                                    <div className="form-group">
                                      <label>Time out <span className="text-danger">*</span></label>
                                      {/* <input className="form-control" type="datetime-local" step="900" id="AddTimeOut" min={this.state.AddTimeOut_Min} max={this.state.AddTimeOut_Max} value={this.state.AddTimeOut} onChange={this.handleChange('AddTimeOut')} /> */}
                                      <div className="row">
                                        <div className="col-md-12">
                                            <Datetime
                                              inputProps={{readOnly: true,disabled: this.state.AddTimeOutDisabled}}
                                              className="readonly-cls AddTimeOut"
                                              isValidDate={this.validationDateAddOut}
                                              onClose={this.AddTimeOutonClose}
                                              closeOnTab={true}
                                              input={true}
                                              value={(this.state.AddTimeOut) ? this.state.AddTimeOut : ''} 
                                              defaultValue={this.state.AddTimeOut}
                                              onChange={this.handleDateAddOut}
                                              dateFormat={process.env.DATE_FORMAT}
                                              timeFormat={process.env.TIME_FORMAT}
                                              timeConstraints={{
                                                hours: { min: 0, max: 23 },
                                                minutes: { min: 0, max: 59, step: 15 }
                                              }}
                                              renderInput={(props) => {
                                                   return <input {...props} value={(this.state.AddTimeOut) ? props.value : ''} />
                                               }}
                                            /> 
                                        </div>
                                      </div>
                                      <span className="form-text error-font-color">{this.state.errormsg["AddTimeOut"]}</span>
                                    </div>
                                  </div>
                                  <div className="col-md-6">
                                    { this.state.AddService != process.env.API_ON_CALL ?
                                      <div className="form-group">
                                        <label># of hrs</label>
                                        <input className="form-control" type="text"  value={this.state.AddNoOfHours} onChange={this.handleChange('AddNoOfHours')} disabled/>
                                        <span className="form-text error-font-color">{this.state.errormsg["AddNoOfHours"]}</span>
                                      </div>
                                      : null
                                    }
                                  </div>

                                  {/*{ this.state.AddPreference.length > 0 ? */}
                                    <div className="col-md-6">
                                      <div className="form-group">
                                        <label>Payout Preference <span className="text-danger">*</span></label>
                                        <select className="form-control" id="AddPreferenceService" value={this.state.AddPreferenceService} onChange={this.handleChange('AddPreferenceService')}>
                                          <option value="">-</option>
                                          {this.state.AddPreference.map(( listValue, index ) => {
                                            return (
                                              <option key={index} value={listValue.guidId}>{listValue.name}</option>
                                            );
                                          })}
                                        </select>
                                        <span className="form-text error-font-color">{this.state.errormsg["AddPreferenceService"]}</span>
                                      </div>
                                    </div>
                                    {/*: null
                                  } */}
                                  
                                  <div className="col-md-12">
                                    <div className="form-group">
                                      <label>Notes</label>
                                      <textarea className="form-control" type="text" value={this.state.AddNotes} onChange={this.handleChange('AddNotes')} ></textarea>
                                      <span className="form-text error-font-color">{this.state.errormsg["AddNotes"]}</span>
                                    </div>
                                  </div>
                                  
                                </div>
                                
                                
                                <div className="submit-section">
                                  <button className="btn btn-primary submit-btn" >Save</button>
                                </div>
                              </div>
                            </div>
                          
                        </div>
                      </div>
                    </div>
                  </div>
                {/* /ADD Current Time Staff Tab Modals */}

                {/* /Edit Current Time Staff Tab Modals */}
                  <div id="CurrentTimeStaff_Edit_modal" data-backdrop="static" className="modal custom-modal" role="dialog">
                    <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
                      <div className="modal-content">
                        <div className="modal-header">
                          <h5 className="modal-title">Time Sheet Entry</h5>
                          <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                          </button>
                        </div>
                        <div className="modal-body">
                          <div className="card">
                            <div className="card-body">
                              <div className="row">
                                <div className="col-md-6">
                                  <div className="form-group">
                                    <label>Service Name <span className="text-danger">*</span></label>
                                    <select className="form-control" id="EditService" value={this.state.EditService} onChange={this.handleChange('EditService')}>
                                      <option value="">-</option>
                                      {this.state.serviceView.map(( listValue, index ) => {
                                        if(this.state.EditService == process.env.API_ON_CALL || listValue.serviceId != process.env.API_ON_CALL){
                                          return (
                                            <option key={index} data-basserivce={listValue.isBasedOnSerivceBase} value={listValue.serviceId}>{listValue.serviceName}</option>
                                          );
                                        }
                                      })}
                                    </select>
                                  </div>
                                </div>
                                <div className="col-md-6">
                                  <div className="form-group">
                                    <label>Location <span className="text-danger">*</span></label>
                                    <select className="form-control" id="Editlocation" value={this.state.Editlocation} onChange={this.handleChange('Editlocation')}>
                                      <option value="">-</option>
                                      {this.state.locationViews.map(( listValue, index ) => {
                                        return (
                                          <option key={index} value={listValue.locationGuid}>{listValue.locationName}</option>
                                        );
                                      })}
                                    </select>
                                  </div>
                                </div>
                                <div className="col-md-6">
                                  <div className="form-group">
                                    <label>Time in <span className="text-danger">*</span></label>
                                    <div className="row">
                                      <div className="col-md-12">
                                        <Datetime
                                          inputProps={{readOnly: true,disabled: this.state.EditTimeInDisabled}}
                                          isValidDate={this.validationDateEditIn}
                                          closeOnTab={true}
                                          input={true}
                                          value={(this.state.EditTimeIn) ? this.state.EditTimeIn : ''}
                                          onChange={this.handleDateEditIn}
                                          dateFormat={process.env.DATE_FORMAT}
                                          timeFormat={process.env.TIME_FORMAT}
                                          timeConstraints={{
                                            hours: { min: 0, max: 23 },
                                            minutes: { min: 0, max: 59, step: 15 }
                                          }}
                                          renderInput={(props) => {
                                               return <input {...props} value={(this.state.EditTimeIn) ? props.value : ''} />
                                          }}
                                        />
                                      </div>
                                    </div>
                                    <span className="form-text error-font-color">{this.state.errormsg["EditTimeIn"]}</span>
                                  </div>
                                </div>
                                <div className="col-md-6">
                                  <div className="form-group">
                                    <label>Time out <span className="text-danger">*</span></label>
                                    <div className="row">
                                      <div className="col-md-12">
                                        <Datetime
                                          inputProps={{readOnly: true,disabled: this.state.EditTimeOutDisabled}}
                                          isValidDate={this.validationDateEditOut}
                                          onClose={this.EditTimeOutonClose}
                                          closeOnTab={true}
                                          input={true}
                                          value={(this.state.EditTimeOut) ? this.state.EditTimeOut : ''}
                                          onChange={this.handleDateEditOut}
                                          dateFormat={process.env.DATE_FORMAT}
                                          timeFormat={process.env.TIME_FORMAT}
                                          timeConstraints={{
                                            hours: { min: 0, max: 23 },
                                            minutes: { min: 0, max: 59, step: 15 }
                                          }}
                                          renderInput={(props) => {
                                               return <input {...props} value={(this.state.EditTimeOut) ? props.value : ''} />
                                          }}
                                        />
                                      </div>
                                    </div>
                                    <span className="form-text error-font-color">{this.state.errormsg["EditTimeOut"]}</span>
                                  </div>
                                </div>
                                <div className="col-md-6">
                                  { this.state.EditService != process.env.API_ON_CALL ?
                                    <div className="form-group">
                                      <label>No of hours</label>
                                      <input className="form-control" type="text"  value={this.state.EditNoOfHours} onChange={this.handleChange('EditNoOfHours')} disabled/>
                                      <span className="form-text error-font-color">{this.state.errormsg["EditNoOfHours"]}</span>
                                    </div>
                                    : null
                                  }
                                </div>

                                { this.state.EditPreference.length > 0 ?
                                  <div className="col-md-6">
                                    <div className="form-group">
                                      <label>Payout Preference <span className="text-danger">*</span></label>
                                      <select className="form-control" id="EditPreferenceService" value={this.state.EditPreferenceService} onChange={this.handleChange('EditPreferenceService')}>
                                        <option value="">-</option>
                                        {this.state.EditPreference.map(( listValue, index ) => {
                                          return (
                                            <option key={index} value={listValue.guidId}>{listValue.name}</option>
                                          );
                                        })}
                                      </select>
                                      <span className="form-text error-font-color">{this.state.errormsg["EditPreferenceService"]}</span>
                                    </div>
                                  </div>
                                  : null
                                }
                                  
                                <div className="col-md-12">
                                  <div className="form-group">
                                    <label>Notes</label>
                                    <textarea className="form-control" type="text" value={this.state.EditNotes} onChange={this.handleChange('EditNotes')} ></textarea>
                                  </div>
                                </div>
                              </div>
                              
                              <div className="submit-section">
                                <button className="btn btn-primary submit-btn">Save</button>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                {/* /Edit Current Time Staff Tab Modals */}

                {/* ADD Payroll Adjustments Modal */}
                  <div id="PayrollAdjustments_Add_modal" data-backdrop="static" className="modal custom-modal" role="dialog">
                    <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
                      <div className="modal-content">
                        <div className="modal-header">
                          <h5 className="modal-title">Payroll Adjustments</h5>
                          <button type="button" className="close" data-dismiss="modal" aria-label="Close" >
                            <span aria-hidden="true">×</span>
                          </button>
                        </div>
                        <div className="modal-body">
                         
                            <div className="card">
                              <div className="card-body">

                                <div className="row">
                                  <div className="col-md-12">
                                    <div className="form-group">
                                      <label>Employee Name : Support Worker FT 03</label>
                                    </div>
                                  </div>

                                  <div className="col-md-6">
                                    <div className="form-group row">
                                      <label className="col-lg-5 col-form-label">Adjustment Code </label>
                                      <div className="col-lg-7">
                                        <select className="form-control" value={this.state.AddPayrollADJadjustmentCode} onChange={this.handleChange('AddPayrollADJadjustmentCode')} >
                                          <option value="">-</option>
                                          {this.state.PayrollADJAdjustmentCodeList.map(( listValue, index ) => {
                                            return (
                                              <option key={index} value={listValue.guidId}>{listValue.name}</option>
                                            );
                                          })}
                                        </select>
                                        <span className="form-text error-font-color">{this.state.errormsg["AddPayrollADJadjustmentCode"]}</span>
                                      </div>
                                    </div>
                                  </div>
                                  <div className="col-md-6">
                                    <div className="form-group row">
                                      <label className="col-lg-5 col-form-label">Location</label>
                                      <div className="col-lg-7">
                                        <select className="form-control" value={this.state.AddPayrollADJlocation} onChange={this.handleChange('AddPayrollADJlocation')} >
                                          <option value="">-</option>
                                          {this.state.PayrollADJLocationList.map(( listValue, index ) => {
                                            return (
                                              <option key={index} value={listValue.locationGuid}>{listValue.locationName}</option>
                                            );
                                          })}
                                        </select>
                                        <span className="form-text error-font-color">{this.state.errormsg["AddPayrollADJlocation"]}</span>
                                      </div>
                                    </div>
                                  </div>

                                  <div className="col-md-4">
                                    <div className="form-group row">
                                      <label className="col-lg-5 col-form-label">Date</label>
                                      <div className="col-lg-7">
                                        <Datetime
                                          inputProps={{readOnly: true}}
                                          closeOnTab={true}
                                          input={true}
                                          value={(this.state.AddPayrollADJDate) ? this.state.AddPayrollADJDate : ''}
                                          onChange={this.handleAddPayrollADJDate}
                                          dateFormat={process.env.DATE_FORMAT}
                                          timeFormat={false}
                                          renderInput={(props) => {
                                             return <input {...props} value={(this.state.AddPayrollADJDate) ? props.value : ''} />
                                          }}
                                        />
                                        <span className="form-text error-font-color">{this.state.errormsg["AddPayrollADJDate"]}</span>
                                      </div>
                                    </div>
                                  </div>
                                  
                                  <div className="col-md-4" id="id_hours_div">
                                    <div className="form-group row">
                                      <label className="col-lg-5 col-form-label">Hours<span className="text-danger">*</span></label>
                                      <div className="col-lg-7">
                                        <input type="number" className="form-control" value={this.state.AddPayrollADJQtyhours} onChange={this.handleChange('AddPayrollADJQtyhours')} />
                                        <span className="form-text error-font-color">{this.state.errormsg["AddPayrollADJQtyhours"]}</span>
                                      </div>
                                    </div>
                                  </div>
                                  <div className="col-md-4" id="id_minutes_div">
                                    <div className="form-group row">
                                      <label className="col-lg-5 col-form-label">Minutes<span className="text-danger">*</span></label>
                                      <div className="col-lg-7">  
                                        <select className="form-control floating" value={this.state.AddPayrollADJQtyminutes} onChange={this.handleChange('AddPayrollADJQtyminutes')}> 
                                          <option value="">-</option>
                                          <option value="00">00</option>
                                          <option value="15">15</option>
                                          <option value="30">30</option>
                                          <option value="45">45</option>
                                        </select>
                                        <span className="form-text error-font-color">{this.state.errormsg["AddPayrollADJQtyminutes"]}</span>
                                      </div>
                                    </div>
                                  </div>
                                  <div className="col-md-4">
                                    <div className="form-group row">
                                      <label className="col-lg-5 col-form-label">Amount</label>
                                      <div className="col-lg-7">
                                        <input type="text" className="form-control" value={this.state.AddPayrollADJamount} onChange={this.handleChange('AddPayrollADJamount')} />
                                        <span className="form-text error-font-color">{this.state.errormsg["AddPayrollADJamount"]}</span>
                                      </div>
                                    </div>
                                  </div>

                                  <div className="col-md-12">
                                    <div className="form-group row">
                                      <label className="col-lg-2 col-form-label">Description</label>
                                      <div className="col-lg-10">
                                        <input type="text" className="form-control" value={this.state.AddPayrollADJdescription} onChange={this.handleChange('AddPayrollADJdescription')} />
                                        <span className="form-text error-font-color">{this.state.errormsg["AddPayrollADJdescription"]}</span>
                                      </div>
                                    </div>
                                  </div>

                                  <div className="col-md-12 pk-margin-top">
                                    <div className="form-group row">
                                      {/*<div className="col-lg-1">
                                        <input type="checkbox" className="form-control" value="" />
                                      </div>*/}
                                      <input type="checkbox" className="form-control col-lg-1 pk-payrolladjust-checkbox" id="AddPayrollADJsubstractFrom" value={this.state.AddPayrollADJsubstractFrom} onChange={this.handleChange('AddPayrollADJsubstractFrom')} />
                                      <label className="col-lg-4 col-form-label">Subtract from</label>
                                    </div>
                                  </div>

                                  <div className="col-md-6">
                                    <div className="form-group row">
                                      <label className="col-lg-6 col-form-label">Entitlement Category</label>
                                      <div className="col-lg-6">
                                        <select className="form-control" value={this.state.AddPayrollADJEntCategory}  onChange={this.handleChange('AddPayrollADJEntCategory')} >
                                          <option value="">-</option>
                                            {this.state.PayrollADJEntitlementCategoryList.map(( listValue, index ) => {
                                              return (
                                                <option key={index} value={listValue.entitlementCategoryId}>{listValue.entitlementCategoryName}</option>
                                              );
                                            })}
                                        </select>
                                        <span className="form-text error-font-color">{this.state.errormsg["AddPayrollADJEntCategory"]}</span>
                                      </div>
                                    </div>
                                  </div>
                                  <div className="col-md-6">
                                    <div className="form-group row">
                                      <label className="col-lg-6 col-form-label">Reason</label>
                                      <div className="col-lg-6">
                                        <select className="form-control" id="EditService" value={this.state.AddPayrollADJEntSubCategory}  onChange={this.handleChange('AddPayrollADJEntSubCategory')}  >
                                          <option value="">-</option>
                                          
                                            {this.state.PayrollADJEntitlementSubCategoryList.map(( listValue, index ) => {
                                              return (
                                                <option key={index} value={listValue.entitlementSubCategoryId}>{listValue.entitlementSubCategoryName}</option>
                                              );
                                            })}
                                        </select>
                                        <span className="form-text error-font-color">{this.state.errormsg["AddPayrollADJEntSubCategory"]}</span>
                                      </div>
                                    </div>
                                  </div>

                                  
                                  <div className="col-md-4" id="id_hours_div">
                                    <div className="form-group row">
                                      <label className="col-lg-5 col-form-label">Hours<span className="text-danger">*</span></label>
                                      <div className="col-lg-7">
                                        <input type="number" className="form-control" value={this.state.AddPayrollADJEntQtyhours} onChange={this.handleChange('AddPayrollADJEntQtyhours')} />
                                        <span className="form-text error-font-color">{this.state.errormsg["AddPayrollADJEntQtyhours"]}</span>
                                      </div>
                                    </div>
                                  </div>
                                  <div className="col-md-4" id="id_minutes_div">
                                    <div className="form-group row">
                                      <label className="col-lg-5 col-form-label">Minutes<span className="text-danger">*</span></label>
                                      <div className="col-lg-7">  
                                        <select className="form-control floating" value={this.state.AddPayrollADJEntQtyminutes} onChange={this.handleChange('AddPayrollADJEntQtyminutes')}> 
                                          <option value="">-</option>
                                          <option value="00">00</option>
                                          <option value="15">15</option>
                                          <option value="30">30</option>
                                          <option value="45">45</option>
                                        </select>
                                        <span className="form-text error-font-color">{this.state.errormsg["AddPayrollADJEntQtyminutes"]}</span>
                                      </div>
                                    </div>
                                  </div>

                                </div>
                                
                                <div className="submit-section">
                                  <button className="btn btn-primary submit-btn" onClick={this.AddRecordPayrollAdj()} >Save</button>
                                </div>
                              </div>
                            </div>
                          
                        </div>
                      </div>
                    </div>
                  </div>
                {/* /ADD Payroll Adjustments Modal */}

                {/* /Edit Payroll Adjustments Modal */}
                  <div id="PayrollAdjustments_Edit_modal" data-backdrop="static" className="modal custom-modal" role="dialog">
                    <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
                      <div className="modal-content">
                        <div className="modal-header">
                          <h5 className="modal-title">Payroll Adjustments</h5>
                          <button type="button" className="close" data-dismiss="modal" aria-label="Close" >
                            <span aria-hidden="true">×</span>
                          </button>
                        </div>
                        <div className="modal-body">
                         
                            <div className="card">
                              <div className="card-body">

                                <div className="row">
                                  <div className="col-md-12">
                                    <div className="form-group">
                                      <label>Employee Name : Support Worker FT 03</label>
                                    </div>
                                  </div>

                                  <div className="col-md-6">
                                    <div className="form-group row">
                                      <label className="col-lg-5 col-form-label">Adjustment Code </label>
                                      <div className="col-lg-7">
                                        <select className="form-control" value={this.state.EditPayrollADJadjustmentCode} onChange={this.handleChange('EditPayrollADJadjustmentCode')} >
                                          <option value="">-</option>
                                          {this.state.PayrollADJAdjustmentCodeList.map(( listValue, index ) => {
                                            return (
                                              <option key={index} value={listValue.guidId}>{listValue.name}</option>
                                            );
                                          })}
                                        </select>
                                        <span className="form-text error-font-color">{this.state.errormsg["EditPayrollADJadjustmentCode"]}</span>
                                      </div>
                                    </div>
                                  </div>
                                  <div className="col-md-6">
                                    <div className="form-group row">
                                      <label className="col-lg-5 col-form-label">Location</label>
                                      <div className="col-lg-7">
                                        <select className="form-control" value={this.state.EditPayrollADJlocation} onChange={this.handleChange('EditPayrollADJlocation')} >
                                          <option value="">-</option>
                                          {this.state.PayrollADJLocationList.map(( listValue, index ) => {
                                            return (
                                              <option key={index} value={listValue.locationGuid}>{listValue.locationName}</option>
                                            );
                                          })}
                                        </select>
                                        <span className="form-text error-font-color">{this.state.errormsg["EditPayrollADJlocation"]}</span>
                                      </div>
                                    </div>
                                  </div>

                                  <div className="col-md-4">
                                    <div className="form-group row">
                                      <label className="col-lg-5 col-form-label">Date</label>
                                      <div className="col-lg-7">
                                        <Datetime
                                          inputProps={{readOnly: true}}
                                          closeOnTab={true}
                                          input={true}
                                          value={(this.state.EditPayrollADJDate) ? this.state.EditPayrollADJDate : ''}
                                          onChange={this.handleEditPayrollADJDate}
                                          dateFormat={process.env.DATE_FORMAT}
                                          timeFormat={false}
                                          renderInput={(props) => {
                                             return <input {...props} value={(this.state.EditPayrollADJDate) ? props.value : ''} />
                                          }}
                                        />
                                        <span className="form-text error-font-color">{this.state.errormsg["EditPayrollADJDate"]}</span>
                                      </div>
                                    </div>
                                  </div>
                                  
                                  <div className="col-md-4" id="id_hours_div">
                                    <div className="form-group row">
                                      <label className="col-lg-5 col-form-label">Hours<span className="text-danger">*</span></label>
                                      <div className="col-lg-7">
                                        <input type="number" className="form-control" value={this.state.EditPayrollADJQtyhours} onChange={this.handleChange('EditPayrollADJQtyhours')} />
                                        <span className="form-text error-font-color">{this.state.errormsg["EditPayrollADJQtyhours"]}</span>
                                      </div>
                                    </div>
                                  </div>
                                  <div className="col-md-4" id="id_minutes_div">
                                    <div className="form-group row">
                                      <label className="col-lg-5 col-form-label">Minutes<span className="text-danger">*</span></label>
                                      <div className="col-lg-7">  
                                        <select className="form-control floating" value={this.state.EditPayrollADJQtyminutes} onChange={this.handleChange('EditPayrollADJQtyminutes')}> 
                                          <option value="">-</option>
                                          <option value="00">00</option>
                                          <option value="15">15</option>
                                          <option value="30">30</option>
                                          <option value="45">45</option>
                                        </select>
                                        <span className="form-text error-font-color">{this.state.errormsg["EditPayrollADJQtyminutes"]}</span>
                                      </div>
                                    </div>
                                  </div>
                                  <div className="col-md-4">
                                    <div className="form-group row">
                                      <label className="col-lg-5 col-form-label">Amount</label>
                                      <div className="col-lg-7">
                                        <input type="text" className="form-control" value={this.state.EditPayrollADJamount} onChange={this.handleChange('EditPayrollADJamount')} />
                                        <span className="form-text error-font-color">{this.state.errormsg["EditPayrollADJamount"]}</span>
                                      </div>
                                    </div>
                                  </div>

                                  <div className="col-md-12">
                                    <div className="form-group row">
                                      <label className="col-lg-2 col-form-label">Description</label>
                                      <div className="col-lg-10">
                                        <input type="text" className="form-control" value={this.state.EditPayrollADJdescription} onChange={this.handleChange('EditPayrollADJdescription')} />
                                        <span className="form-text error-font-color">{this.state.errormsg["EditPayrollADJdescription"]}</span>
                                      </div>
                                    </div>
                                  </div>

                                  <div className="col-md-12 pk-margin-top">
                                    <div className="form-group row">
                                      {/*<div className="col-lg-1">
                                        <input type="checkbox" className="form-control" value="" />
                                      </div>*/}
                                      <input type="checkbox" className="form-control col-lg-1 pk-payrolladjust-checkbox" id="EditPayrollADJsubstractFrom" value={this.state.EditPayrollADJsubstractFrom} onChange={this.handleChange('EditPayrollADJsubstractFrom')} />
                                      <label className="col-lg-4 col-form-label">Subtract from</label>
                                    </div>
                                  </div>

                                  <div className="col-md-6">
                                    <div className="form-group row">
                                      <label className="col-lg-6 col-form-label">Entitlement Category</label>
                                      <div className="col-lg-6">
                                        <select className="form-control" value={this.state.EditPayrollADJEntCategory}  onChange={this.handleChange('EditPayrollADJEntCategory')} >
                                          <option value="">-</option>
                                            {this.state.PayrollADJEntitlementCategoryList.map(( listValue, index ) => {
                                              return (
                                                <option key={index} value={listValue.entitlementCategoryId}>{listValue.entitlementCategoryName}</option>
                                              );
                                            })}
                                        </select>
                                        <span className="form-text error-font-color">{this.state.errormsg["EditPayrollADJEntCategory"]}</span>
                                      </div>
                                    </div>
                                  </div>
                                  <div className="col-md-6">
                                    <div className="form-group row">
                                      <label className="col-lg-6 col-form-label">Reason</label>
                                      <div className="col-lg-6">
                                        <select className="form-control" id="EditService" value={this.state.EditPayrollADJEntSubCategory}  onChange={this.handleChange('EditPayrollADJEntSubCategory')}  >
                                          <option value="">-</option>
                                          
                                            {this.state.PayrollADJEntitlementSubCategoryList.map(( listValue, index ) => {
                                              return (
                                                <option key={index} value={listValue.entitlementSubCategoryId}>{listValue.entitlementSubCategoryName}</option>
                                              );
                                            })}
                                        </select>
                                        <span className="form-text error-font-color">{this.state.errormsg["EditPayrollADJEntSubCategory"]}</span>
                                      </div>
                                    </div>
                                  </div>

                                  
                                  <div className="col-md-4" id="id_hours_div">
                                    <div className="form-group row">
                                      <label className="col-lg-5 col-form-label">Hours<span className="text-danger">*</span></label>
                                      <div className="col-lg-7">
                                        <input type="number" className="form-control" value={this.state.EditPayrollADJEntQtyhours} onChange={this.handleChange('EditPayrollADJEntQtyhours')} />
                                        <span className="form-text error-font-color">{this.state.errormsg["EditPayrollADJEntQtyhours"]}</span>
                                      </div>
                                    </div>
                                  </div>
                                  <div className="col-md-4" id="id_minutes_div">
                                    <div className="form-group row">
                                      <label className="col-lg-5 col-form-label">Minutes<span className="text-danger">*</span></label>
                                      <div className="col-lg-7">  
                                        <select className="form-control floating" value={this.state.EditPayrollADJEntQtyminutes} onChange={this.handleChange('EditPayrollADJEntQtyminutes')}> 
                                          <option value="">-</option>
                                          <option value="00">00</option>
                                          <option value="15">15</option>
                                          <option value="30">30</option>
                                          <option value="45">45</option>
                                        </select>
                                        <span className="form-text error-font-color">{this.state.errormsg["EditPayrollADJEntQtyminutes"]}</span>
                                      </div>
                                    </div>
                                  </div>

                                </div>
                                
                                <div className="submit-section">
                                  <button className="btn btn-primary submit-btn" onClick={this.UpdateRecordPayrollAdj()} >Save</button>
                                </div>
                              </div>
                            </div>
                          
                        </div>
                      </div>
                    </div>
                  </div>
                {/* /Edit Payroll Adjustments Modal */}
              {/* ===================================== Modal ===================================== */}  

              {/* Delete Today Work Modal */}
              {/* Delete Today Work Modal */}
          </div>
          <SidebarContent/>
      </div>
        );
      
   }
}

export default PayrollAdjustmentsAdd;
