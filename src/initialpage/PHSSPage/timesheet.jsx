
import React, { Component } from 'react';
import { Helmet } from "react-helmet";
import {Reportto,ProfileImage,Avatar_02,Avatar_05,Avatar_09,Avatar_10,Avatar_16 } from '../../Entryfile/imagepath'

import { Table } from 'antd';
import 'antd/dist/antd.css';
import {itemRender,onShowSizeChange} from "./pagination/paginationfunction"
import "../MainPage/antdstyle.css"

import Header from '../Sidebar/header.jsx';
import SidebarContent from '../Sidebar/sidebar';

import moment from 'moment';
import Loader from '../Loader';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import SystemHelpers from '../Helpers/SystemHelper';
import Datetime from "react-datetime";




//table
import { MDBDataTable } from 'mdbreact';
import '@fortawesome/fontawesome-free/css/all.min.css';
import 'bootstrap-css-only/css/bootstrap.min.css';
import 'mdbreact/dist/css/mdb.css';
//table

import CryptoAES from 'crypto-js/aes';
import CryptoENC from 'crypto-js/enc-utf8';
class Timesheet extends Component {
  
  constructor(props) {
    super(props);
    this.state = {
      ListGrid:[],
      ListGridPast:[],
      errormsg : '',
      serviceView:[],
      locationViews:[],
      ListGridPastDetails:[],
      timeSheetContactMasterId:'',
      timeSheetPeriodViews:[],
      CurrenttimeSheetPeriodId:'',
      CurrentDate_Min:'',
      CurrentDate_Max:'',
      isTimeSheetSubmitted:'',
      isCurrent: false,
      workedHour: '',
      locationIdGuid:'',
      //add model 
      AddDate:'',
      AddTimeIn:'',
      AddTimeOut:'',
      AddTimeOutFlage:false,
      Addlocation:'',
      AddService:'',
      AddNotes:'',
      AddNoOfHours:'',
      timeSheetPeriodId:'',
      IsNoOfHoursvalid: false,
      AddTimeOutDisabled : false,
      //Edit model
      timeSheetTransactionId: '',
      EditService: '',
      Editlocation: '',
      EditTimeIn: '',
      EditTimeOut: '',
      EditNoOfHours: '',
      EditDate: '',
      IsNoOfHoursvalidEdit:true,
      DeletetimesheetTransactionId:'',
      staffContactID:localStorage.getItem("contactId"),
      fullName:localStorage.getItem("fullName"),
      role_timesheet_can: {},
      selectedValue: '',
      DueDate:'',
      EditTimeInDisabled : false,
      EditTimeOutDisabled : false,

      PasttimeSheetStartDate : '',
      PasttimeSheetEndDate : '',

      PreviewTimesheetDate : '',
      PreviewtimeSheetPeriodId: '',

      isAmendmends: false,
      recordType:'',
      isAmendmendsSubmitted:'',
      IsAmendmendsEnabled: false,

      isTimeSheetApproved: false,
      isTimeSheetSubmitedToPayroll: false,
      isamendmentFlag : false,
      ListNotes:[],
      allow_emp: false,

      AddPreference:[],
      EditPreference:[],
      AddPreferenceService:'',
      EditPreferenceService:'',

      // ===== Entitlement Summary ===== //
      EntitleSickTimeTotalYear : '0',
      EntitleSickTimeAdditionalEntitled : '0',
      EntitleSickTimeUsedPeriod : '0',
      EntitleSickTimeUsedDate : '0',
      EntitleSickTimeCurrentHrsRemaining : '0',
      EntitleSickTimeCOVID19RemainingDays : '0',

      EntitleVacationPreviousHrsRemaining : '0',
      EntitleVacationHrsEntitledDate : '0',
      EntitleVacationHrsTakenPayPeriod : '0',
      EntitleVacationHrsUsedDate : '0',
      EntitleVacationHrsRemaining : '0',

      EntitleOvertimePreviousPayPeriodRemaining : '0',
      EntitleOvertimeGainedPayPeriod : '0',
      EntitleOvertimeTakenPayPeriod : '0',
      EntitleOvertimeCurrentHrsRemaining : '0',
      EntitleOvertimeSpecialBankTimeBalance : '0'
      // ===== Entitlement Summary ===== //

    };
    this.setPropState = this.setPropState.bind(this);
    this.handleChange = this.handleChange.bind(this);

    this.handleDateAddIn = this.handleDateAddIn.bind(this);
    this.handleDateAddOut = this.handleDateAddOut.bind(this);
    this.handleDateEditIn = this.handleDateEditIn.bind(this);
    this.handleDateEditOut = this.handleDateEditOut.bind(this);

  }

 

  handleDateAddIn = (date) =>{
    //alert(date);

    // validation check
      //alert(date.isBefore(moment(this.state.AddTimeIn_Max)) && date.isAfter(moment(this.state.AddTimeIn_Min).subtract(1, 'day')));

    // validation check
    //var date_temp = moment(moment(date).add(2, 'day').format('YYYY/MM/DD HH:mm A'));
    this.setState({ AddTimeOut_Min: moment(date).subtract(1, 'day')});
    this.setState({ AddTimeOut_Max: moment(date).add(2, 'day')});
    this.setState({ AddTimeIn: date});

    

  //  console.log('date' + date);
    //console.log('AddTimeOut_Min' + moment(date).subtract(1, 'day'));
    //console.log('AddTimeOut_Max' + moment(date).add(2, 'day'));

    var AddTimeOut = this.state.AddTimeOut;

    if(this.state.AddService == process.env.API_24HOURS_SERVICE ){
       this.setState({ AddTimeOutDisabled: true});
      
      var AddTimeOut = moment(moment(date).add(24, 'hours'));
    }

    if(this.state.AddService != process.env.API_24HOURS_SERVICE ){
      this.setState({ AddTimeOutDisabled: false});
    }
    //this.setState({AddTimeOut: ''});
    //$('.AddTimeOut .form-control').val('null'); 
    
    //this.props.setPropState('AddTimeOut', null);
    this.setState({ AddTimeOut: AddTimeOut});

    if(date !='' && AddTimeOut != ''){
      var Get_Hour = this.Get_Hour(date,AddTimeOut);
    //  console.log(Get_Hour);
      this.setState({ AddNoOfHours: Get_Hour });
    }
    

    //console.log('AddTimeOut_Min => '+ date);
    //console.log('AddTimeOut_Max => '+ NewDate);
  };

  handleDateAddOut = (date) =>{
    //alert(date);
    this.setState({ AddTimeOut: date});

    var StarTime = this.state.AddTimeIn;
    var EndTime = date;


  //  console.log(StarTime);
  //  console.log(EndTime);

    if(StarTime != '' && EndTime !=''){
      var Get_Hour = this.Get_Hour(StarTime,EndTime);
    //  console.log(Get_Hour);
      this.setState({ AddNoOfHours: Get_Hour });
    }

  };

  handleDateEditIn = (date) =>{
    //alert(date);
    this.setState({ EditTimeOut_Min: moment(date).subtract(1, 'day')});
    this.setState({ EditTimeOut_Max: moment(date).add(2, 'day')});
    this.setState({ EditTimeIn: date});


    var EditTimeOut = this.state.EditTimeOut;
    if(this.state.EditService == process.env.API_24HOURS_SERVICE ){
      var EditTimeOut = moment(moment(date).add(24, 'hours'));
    }
    
    this.setState({ EditTimeOut: EditTimeOut});
    

    if(date !='' && EditTimeOut != ''){
      var Get_Hour = this.Get_Hour_Edit(date,EditTimeOut);
    //  console.log(Get_Hour);
      this.setState({ EditNoOfHours: Get_Hour });
    }
    

    //console.log('EditTimeOut_Min => '+ date);
    //console.log('EditTimeOut_Max => '+ NewDate);
  };

  handleDateEditOut = (date) =>{
    //alert(date);
    this.setState({ EditTimeOut: date});

    var StarTime = this.state.EditTimeIn;
    var EndTime = date;


  //  console.log(StarTime);
  //  console.log(EndTime);

    if(StarTime != '' && EndTime !=''){
      var Get_Hour = this.Get_Hour_Edit(StarTime,EndTime);
    //  console.log(Get_Hour);
      this.setState({ EditNoOfHours: Get_Hour });
    }

  };

  

  validationDateAddIn = (currentDate) => {
    //min={this.state.AddTimeIn_Min} max={this.state.AddTimeIn_Max}
    return currentDate.isBefore(moment(this.state.AddTimeIn_Max)) && currentDate.isAfter(moment(this.state.AddTimeIn_Min).subtract(1, 'day'));
  };


  validationDateAddOut = (currentDate) => {
    //min={this.state.AddTimeIn_Min} max={this.state.AddTimeIn_Max}
    if(this.state.AddService == process.env.API_ON_CALL || this.state.AddService == process.env.API_ZERO_HOURS){
      return currentDate.isBefore(moment(this.state.AddTempTimeOut_Max)) && currentDate.isAfter(moment(this.state.AddTimeOut_Min));
    }else{
      return currentDate.isBefore(moment(this.state.AddTimeOut_Max)) && currentDate.isAfter(moment(this.state.AddTimeOut_Min));
    }
  };

  AddTimeOutonClose = (date) =>{
    /* Payout Preference Start */

    if(this.state.AddService !="" && this.state.Addlocation !=""){
      var service = $("#AddService").find(':selected').data('basserivce');
      this.GetPayOffDetailsForUser(this.state.AddService,date,service,this.state.Addlocation,'Add');
    }

    /* Payout Preference End */
  }

  EditTimeOutonClose = (date) =>{
    /* Payout Preference Start */

    if(this.state.EditService !="" && this.state.Editlocation !=""){
      var service = $("#EditService").find(':selected').data('basserivce');
      
      this.GetPayOffDetailsForUser(this.state.EditService,date,service,this.state.Editlocation,'Edit');
      
    }

    /* Payout Preference End */
  }

  validationDateEditIn = (currentDate) => {
    // console.log("EditTimeIn_Max => "+this.state.EditTimeIn_Max);
    // console.log("EditTimeIn_Min => "+this.state.EditTimeIn_Min);
    //min={this.state.EditTimeIn_Min} max={this.state.EditTimeIn_Max}
    //return currentDate.isBefore(this.state.EditTimeOut_Min) && currentDate.isAfter(this.state.EditTimeOut_Min);
    return currentDate.isBefore(moment(this.state.EditTimeIn_Max)) && currentDate.isAfter(moment(this.state.EditTimeIn_Min));
  };

  validationDateEditOut = (currentDate) => {
    //min={this.state.AddTimeIn_Min} max={this.state.AddTimeIn_Max}
    // min={this.state.EditTimeOut_Min} max={this.state.EditTimeOut_Max}
    if(this.state.EditService == process.env.API_ON_CALL || this.state.EditService == process.env.API_ZERO_HOURS){
      return currentDate.isBefore(moment(this.state.EditTempTimeOut_Max)) && currentDate.isAfter(moment(this.state.EditTimeOut_Min));
    }else{
      return currentDate.isBefore(moment(this.state.EditTimeOut_Max)) && currentDate.isAfter(moment(this.state.EditTimeOut_Min));
    }
    
  };



  handleChange = input => e => {
    this.setState({ [input]: e.target.value });
    console.log(input);
    console.log(e.target);
    //console.log(e.target.data-basserivce);
    console.log(e.target.value);
    if (e.target.value != '') {
        delete this.state.errormsg[input];
    }

    /* Payout Preference Start */
    if(([input]=="AddService" && e.target.value !='') &&  this.state.Addlocation !="" && this.state.AddTimeIn !=""){
      var service = $("#AddService").find(':selected').data('basserivce');
      this.GetPayOffDetailsForUser(e.target.value,this.state.AddTimeOut,service,this.state.Addlocation,'Add');
    }

    if([input]=="AddService" && this.state.AddTimeIn !='' && this.state.AddTimeOut != ''){
        var Get_Hour = this.Get_Hour(this.state.AddTimeIn,this.state.AddTimeOut);
        this.setState({ AddNoOfHours: Get_Hour });
    }

    if([input]=="EditService" && this.state.EditTimeIn !='' && this.state.EditTimeOut != ''){
        var Get_Hour = this.Get_Hour_Edit(this.state.EditTimeIn,this.state.EditTimeOut);
        this.setState({ EditNoOfHours: Get_Hour });
    }

    if(([input]=="Addlocation" && e.target.value !='') &&  this.state.AddService !="" && this.state.AddTimeIn !=""){
      var service = $("#AddService").find(':selected').data('basserivce');
      
      this.GetPayOffDetailsForUser(this.state.AddService,this.state.AddTimeOut,service,e.target.value,'Add');
      
    }

    if(([input]=="EditService" && e.target.value !='') &&  this.state.Editlocation !="" && this.state.EditTimeIn !=""){
      var service = $("#EditService").find(':selected').data('basserivce');
      
      this.GetPayOffDetailsForUser(e.target.value,this.state.EditTimeOut,service,this.state.Editlocation,'Edit');
      
    }

    if(([input]=="Editlocation" && e.target.value !='') &&  this.state.EditService !="" && this.state.EditTimeIn !=""){
      var service = $("#EditService").find(':selected').data('basserivce');
      
      this.GetPayOffDetailsForUser(this.state.EditService,this.state.EditTimeOut,service,e.target.value,'Edit');
      
    }

    

    /* Payout Preference End */

    if([input]=="EditService"){
      /* Payout Preference Start */
      var service = $("#EditService").find(':selected').data('basserivce');
      //alert(service);
      /* Payout Preference End */
    }

    if([input]=="AddService" && this.state.AddTimeIn !='' && e.target.value == process.env.API_24HOURS_SERVICE){
      this.setState({ AddTimeOutDisabled: true});
      var AddTimeOut = moment(moment(this.state.AddTimeIn).add(24, 'hours'));
      //alert(NewDate);
      this.setState({ AddTimeOut: AddTimeOut});
      
      var StarTime = this.state.AddTimeIn;
      var EndTime = AddTimeOut;

    //  console.log(StarTime);
    //  console.log(EndTime);

      if(StarTime != '' && EndTime !=''){
        var Get_Hour = this.Get_Hour(StarTime,EndTime);
      //  console.log(Get_Hour);
        this.setState({ AddNoOfHours: Get_Hour });
      }  

      
    }

    if([input]=="AddService" && e.target.value != process.env.API_24HOURS_SERVICE ){
      this.setState({ AddTimeOutDisabled: false});
    }


    if([input]=="EditService" && e.target.value == process.env.API_24HOURS_SERVICE  && this.state.EditTimeIn !=''){
      this.setState({ EditTimeOutDisabled: true});
      $("#EditTimeOut").prop( "disabled", true );

      var NewDate = moment(moment(this.state.EditTimeIn).add(24, 'hours'));
      //var NewDate = moment(this.state.EditTimeIn).add(24, 'hours').format('YYYY/MM/DD HH:mm A');
      //alert(NewDate);

      
      this.setState({ EditTimeOut: NewDate});
      var StarTime = this.state.EditTimeIn;
      var EndTime = NewDate;

    //  console.log(StarTime);
    //  console.log(EndTime);

      if(StarTime != '' && EndTime !=''){
        var Get_Hour = this.Get_Hour_Edit(StarTime,EndTime);
      //  console.log(Get_Hour);
        this.setState({ EditNoOfHours: Get_Hour });
      }  
    }else if([input]=="EditService" && e.target.value != process.env.API_24HOURS_SERVICE ){
      $("#EditTimeOut").prop( "disabled", false );
      this.setState({ EditTimeOutDisabled: false});
    }
  }

  GetPayOffDetailsForUser(AddService,date,baseservice,Addlocation,calltype){

    // if(baseservice == false){

    //   this.setState({ AddPreference: [] });
    //   this.setState({ EditPreference: [] });
    //   return false;
    // }

    this.setState({ AddPreferenceService : '' });
    //this.setState({ EditPreferenceService : '' });

    if(calltype == 'Add'){
     this.setState({ AddPreference: [] });
    }else{
      this.setState({ EditPreference: [] });
    }
    
    var Servicedate=moment(date).format('MM/DD/YYYY');

    //console.log(Servicedate);

    this.showLoader();
    var url=process.env.API_API_URL+'GetPayOffDetailsForUser?serviceCodeId='+AddService+"&inTimeDate="+Servicedate+"&basedOnServiceCode="+baseservice+"&locationId="+Addlocation;
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
      //body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
      console.log("responseJson GetPayOffDetailsForUser");
      console.log(data);
      
      // debugger;
      if (data.responseType === "1") {
        // Profile & Contact

        if(data != null){

          if(calltype == 'Add'){
           this.setState({ AddPreference: data.data });
          }else{
            this.setState({ EditPreference: data.data });
          }
        }
        
      }else{
        if(data.message == 'Authorization has been denied for this request.'){
          SystemHelpers.SessionOut();
          this.props.history.push("/login");
        }else{
          SystemHelpers.ToastError(data.message);
        }
      }
      this.hideLoader();
    })
    .catch(error => {
      this.props.history.push("/error-500");
    });
  }

  Get_Hour(StarTime,EndTime){
  //  console.log('StarTime =>'+StarTime);
  //  console.log('EndTime =>'+EndTime);
    var StarTime =moment(StarTime).format("DD/MM/YYYY HH:mm:ss");
    var EndTime =moment(EndTime).format("DD/MM/YYYY HH:mm:ss");

    var ms = moment(EndTime,"DD/MM/YYYY HH:mm:ss").diff(moment(StarTime,"DD/MM/YYYY HH:mm:ss"));
    var d = moment.duration(ms);
    var totalHours = Math.floor(d.asHours()) + moment.utc(ms).format(":mm");
    //alert(d.asHours());

    let step1Errors = {};
    if ((d.asHours() < 0 || totalHours == '0:00')) {
      step1Errors["AddNoOfHours"] = "Enter Valid TimeIn and TimeOut.";
      this.setState({ IsNoOfHoursvalid: false });
    }else if(d.asHours() > 24 && this.state.AddService != process.env.API_ZERO_HOURS){
      step1Errors["AddNoOfHours"] = "you can add max 24 hours.";
      this.setState({ IsNoOfHoursvalid: false });
    }
    else{
      delete this.state.errormsg['AddNoOfHours'];
      this.setState({ IsNoOfHoursvalid: true });
    }

    //console.log(this.state.AddService);
    
    if(this.state.AddService == process.env.API_ZERO_HOURS){
      delete this.state.errormsg['AddNoOfHours'];
      this.setState({ IsNoOfHoursvalid: true });
      totalHours = '00:00';
      let step1Errors = {};
    }

    this.setState({ errormsg: step1Errors });

    //var totalHours = moment.utc(moment(StarTime,"DD/MM/YYYY HH:mm:ss").diff(moment(EndTime,"DD/MM/YYYY HH:mm:ss"))).format("HH:mm:ss");
    return totalHours;
  }


  Get_Hour_Edit(StarTime,EndTime){
  //  console.log(StarTime);
  //  console.log(EndTime);
    var StarTime =moment(StarTime).format("DD/MM/YYYY HH:mm:ss");
    var EndTime =moment(EndTime).format("DD/MM/YYYY HH:mm:ss");
  //  console.log(StarTime);
  //  console.log(EndTime);

    var ms = moment(EndTime,"DD/MM/YYYY HH:mm:ss").diff(moment(StarTime,"DD/MM/YYYY HH:mm:ss"));
    var d = moment.duration(ms);
    var totalHours = Math.floor(d.asHours()) + moment.utc(ms).format(":mm");
    //alert(d.asHours());

    let step1Errors = {};
    if (d.asHours() < 0 && (this.state.EditService != process.env.API_ON_CALL || this.state.EditService != process.env.API_ZERO_HOURS)) {
      step1Errors["EditNoOfHours"] = "Enter Valid TimeIn and TimeOut.";
      this.setState({ IsNoOfHoursvalidEdit: false });
    }else if(d.asHours() > 24 && (this.state.EditService != process.env.API_ON_CALL || this.state.EditService != process.env.API_ZERO_HOURS)){
      step1Errors["EditNoOfHours"] = "you can add max 24 hours.";
      this.setState({ IsNoOfHoursvalidEdit: false });
    }
    else{
      delete this.state.errormsg['EditNoOfHours'];
      this.setState({ IsNoOfHoursvalidEdit: true });
    }

    if(this.state.EditService == process.env.API_ZERO_HOURS){
      delete this.state.errormsg['EditNoOfHours'];
      this.setState({ IsNoOfHoursvalidEdit: true });
      totalHours = '00:00';
      let step1Errors = {};
    }

    this.setState({ errormsg: step1Errors });

    //var totalHours = moment.utc(moment(StarTime,"DD/MM/YYYY HH:mm:ss").diff(moment(EndTime,"DD/MM/YYYY HH:mm:ss"))).format("HH:mm:ss");
    return totalHours;
  }

  setPropState(key, value) {
    this.setState({ [key]: value });
  }

  // Loader hide show method
  hideLoader = () => {
    this.setState({ loading: false });
  }

  showLoader = () => {
    this.setState({ loading: true });
  }
  // Loader hide show method

  componentDidMount() {

    // User type session decode
      var pwd = localStorage.getItem("contactId")+"Phss@123";
      var UserType_session = localStorage.getItem("usertypesession");

      var _ciphertext = CryptoAES.decrypt(UserType_session, pwd);
      var UserType_JsonCreate = JSON.parse(_ciphertext.toString(CryptoENC));

      console.log('UserType_JsonCreate');
      console.log(UserType_JsonCreate);
      
      this.setState({ isCordinatorSession : UserType_JsonCreate.isCordinator });
      this.setState({ isPayrollAdminSession : UserType_JsonCreate.isPayrollAdmin });
      this.setState({ isSeniorCordinatorSession : UserType_JsonCreate.isSeniorCordinator });
      this.setState({ isServiceCoordinatorLeadSession : UserType_JsonCreate.isServiceCoordinatorLead });
    // User type session decode


    /* Role Management */
    // var pwd = localStorage.getItem("contactId")+"Phss@123";
    // var Role_session = localStorage.getItem('sessiontoken');

    // var _ciphertext = CryptoAES.decrypt(Role_session, pwd);
    // console.log('Role Store timesheet_can');
    // //console.log(_ciphertext.toString(CryptoENC));
    // var JsonCreate = JSON.parse(_ciphertext.toString(CryptoENC));
    // this.setState({ role_timesheet_can: JsonCreate.timesheet_can });
    
    // console.log(JsonCreate.timesheet_can);
    /* Role Management */

    /* Role Management */
  //  console.log('Role Store timesheet_can');
    var getrole = SystemHelpers.GetRole();
    let timesheet_can = getrole.timesheet_can;
    this.setState({ role_timesheet_can: timesheet_can });
  //  console.log(timesheet_can);
    /* Role Management */


    this.GetProfile();
    this.GetUserWiseTimeSheetData();
    //this.ChangePeriod2();
    this.GetUserPastTimeSheetData();
    
    $( "#AddTimeOut" ).prop( "disabled", true );

    this.GetUserEntitlementSummary(localStorage.getItem("CurrenttimeSheetPeriodId"));
  }

  GetUserWiseTimeSheetData(){

    this.setState({ isTimeSheetApproved: false });
    this.setState({ isTimeSheetSubmitedToPayroll: false });
    this.setState({ isamendmentFlag: false });

    this.showLoader();
    var url=process.env.API_API_URL+'GetUserWiseTimeSheetData?contactIdFillFor='+this.state.staffContactID;
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
      //body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
      console.log("responseJson GetUserWiseTimeSheetData");
      console.log(data);
      
      // debugger;
      if (data.responseType === "1") {
        // Profile & Contact

        if(data != null){

          if(data.data.timesheetContactView != null){
            //  console.log(data.data.timeSheetConatctSheetViews);
              this.setState({ ListGrid: data.data.timesheetContactView.timeSheetConatctSheetViews });
              this.setState({ timeSheetContactMasterId: data.data.timesheetContactView.timeSheetContactMasterId });
              this.setState({ isTimeSheetSubmitted: data.data.timesheetContactView.isTimeSheetSubmitted });
              this.setState({ isAmendmendsSubmitted: data.data.timesheetContactView.isAmendmendsSubmitted });
              this.setState({ IsAmendmendsEnabled : data.data.timesheetContactView.isAmendmendsEnabled });
              this.setState({ workedHour: data.data.timesheetContactView.workedHour });
              this.setState({ DueDate: data.data.timesheetContactView.timeSheetEndDate });

              //
              var ContactViewtimesheet= data.data.timesheetContactView.timeSheetConatctSheetViews;
              for (var z = 0; z < ContactViewtimesheet.length; z++)
              {
                if(ContactViewtimesheet[z].recordType == "Amendment")
                {
                  this.setState({ isamendmentFlag: true });
                }
                
              }
              //console.log("check IsAmendmendsEnabled");
              //console.log(data.data.timesheetContactView.isAmendmendsEnabled);

              this.setState({ isTimeSheetApproved: data.data.timesheetContactView.isTimeSheetApproved });
              this.setState({ isTimeSheetSubmitedToPayroll: data.data.timesheetContactView.isTimeSheetSubmitedToPayroll });
          }
          
          if(data.data.timeSheetPeriodViews != null){
            this.setState({ timeSheetPeriodViews: this.Preview_TimeSheet(data.data.timeSheetPeriodViews) });
            this.Default_TimeSheet(data.data.timeSheetPeriodViews);
            console.log('timeSheetPeriodViews');
            console.log(this.Preview_TimeSheet(data.data.timeSheetPeriodViews));

            //this.Preview_TimeSheet(data.data.timeSheetPeriodViews);
          }

          
          
          this.setState({ serviceView: data.data.serviceView });
          this.setState({ locationViews: data.data.locationViews });
        }
        
        //console.log(data.data.userSkillInfo);
        //this.setState({ ListGrid: this.rowData(data.data.userSkillInfo) })
      }else{
        if(data.message == 'Authorization has been denied for this request.'){
          SystemHelpers.SessionOut();
          this.props.history.push("/login");
        }else{
          SystemHelpers.ToastError(data.message);
        }
      }
      this.hideLoader();
    })
    .catch(error => {
      this.props.history.push("/error-500");
    });
  }

  Preview_TimeSheet(timeSheetPeriod){
    var length = timeSheetPeriod.length;

    console.log('Preview_TimeSheet 123');
    console.log(timeSheetPeriod);

    if (length > 0) {
      let return_push = [];
      for (var zz = 0; zz < length; zz++) {
        if(timeSheetPeriod[zz].isCurrentTimeSheet == true){
          var isCurrentTimeSheet = timeSheetPeriod[zz];
          var PreviewTimesheetDate = moment(isCurrentTimeSheet.timeSheetPeriodStartDate,process.env.API_DATE_FORMAT).subtract(1, 'day').format('MM-DD-YYYY');
        //  console.log(timeSheetPeriod);
        //  console.log("PreviewTimesheetDate => "+PreviewTimesheetDate);
          this.setState({ PreviewTimesheetDate: PreviewTimesheetDate});

          for (var jj = 0; jj < length; jj++) {
            var LoopTimesheetDate = moment(timeSheetPeriod[jj].timeSheetPeriodStartDate,process.env.API_DATE_FORMAT).format('MM-DD-YYYY');
            var LoopTimesheetDateX =LoopTimesheetDate;
            //var diffDate = LoopTimesheetDate.diff(PreviewTimesheetDate, 'days');
            //console.log('diffDate 1 ' + LoopTimesheetDate + " " + PreviewTimesheetDate);
            //console.log(diffDate);
            var TempLoopTimesheetDate=moment(LoopTimesheetDate,'MM-DD-YYYY').format('YYYY-MM-DD');
            var TempPreviewTimesheetDate=moment(PreviewTimesheetDate,'MM-DD-YYYY').format('YYYY-MM-DD');


            let x = moment(TempLoopTimesheetDate,'YYYY-MM-DD');
            let y = moment(TempPreviewTimesheetDate,'YYYY-MM-DD');

            var CheckDays = x.diff(y, 'days');
            //console.log('diffDate '+CheckDays+" "+ + LoopTimesheetDateX + " " + TempPreviewTimesheetDate);

            var timeSheetPeriodEndDate = moment(timeSheetPeriod[jj].timeSheetPeriodEndDate,process.env.API_DATE_FORMAT).format('MM-DD-YYYY');
            if(timeSheetPeriodEndDate == PreviewTimesheetDate){
              this.setState({ PreviewtimeSheetPeriodId:  timeSheetPeriod[jj].timeSheetPeriodId});
            }
            
            if(timeSheetPeriod[jj].isCurrentTimeSheet == true || CheckDays > 0   || timeSheetPeriodEndDate == PreviewTimesheetDate ){
              
              console.log('timeSheetPeriod[jj] => '+ jj);
              console.log(timeSheetPeriod[jj].isCurrentTimeSheet);
              console.log(CheckDays);
              console.log(timeSheetPeriodEndDate);
              console.log(PreviewTimesheetDate);

              return_push.push(timeSheetPeriod[jj]); 
            }
          }
        }
      }

    //  console.log('return_push');
    //  console.log(return_push);

      return return_push;
    }
  }

  Default_TimeSheet(timeSheetPeriod){
      var length = timeSheetPeriod.length;
      var MinDate = moment(new Date()).format("YYYY-MM-DD");
      var MaxDate = moment(new Date()).format("YYYY-MM-DD");
      //alert(MaxDate);
      if (length > 0) {
        var i = 1;
        for (var zz = 0; zz < length; zz++) {

          if(timeSheetPeriod[zz].isCurrentTimeSheet == true && this.state.timeSheetPeriodId == ""){
            var AddDate_Max =  moment(timeSheetPeriod[zz].timeSheetPeriodEndDate,process.env.API_DATE_FORMAT).format('YYYY-MM-DD');
            this.setState({ AddTimeIn_Max: AddDate_Max+"T00:01"});
            this.setState({ EditTimeIn_Max: AddDate_Max+"T00:01"});

            this.setState({ AddTempTimeOut_Max: AddDate_Max+"T00:01"});
            this.setState({ EditTempTimeOut_Max: AddDate_Max+"T00:01"});

            
            var AddDate_Min = moment(timeSheetPeriod[zz].timeSheetPeriodStartDate,process.env.API_DATE_FORMAT).subtract(1, "days").format('YYYY-MM-DD');
            this.setState({ AddTimeIn_Min: AddDate_Min+"T00:01"});
            var EditDate_Min = moment(timeSheetPeriod[zz].timeSheetPeriodStartDate,process.env.API_DATE_FORMAT).subtract(2, "days").format('YYYY-MM-DD');
            this.setState({ EditTimeIn_Min: EditDate_Min+"T00:01"});

            this.setState({ CurrentDate_Min: AddDate_Min});
            this.setState({ CurrentDate_Max: AddDate_Max});
            this.setState({ isCurrent: timeSheetPeriod[zz].isCurrentTimeSheet});
            

            
            $('#timeSheetPeriod').val(timeSheetPeriod[zz].timeSheetPeriodId).trigger('change');

            this.setState({ timeSheetPeriodId: timeSheetPeriod[zz].timeSheetPeriodId});

            this.setState({ CurrenttimeSheetPeriodId: timeSheetPeriod[zz].timeSheetPeriodId});

            this.setState({ DueDate: timeSheetPeriod[zz].dueDate });
            this.setState({ dueDateAmendment: timeSheetPeriod[zz].dueDateAmendment });

          }else if(this.state.timeSheetPeriodId == timeSheetPeriod[zz].timeSheetPeriodId){
            //alert(timeSheetPeriod[zz].isCurrentTimeSheet);
            //alert(timeSheetPeriod[zz].isCurrentTimeSheet);
            var AddDate_Max =  moment(timeSheetPeriod[zz].timeSheetPeriodEndDate,process.env.API_DATE_FORMAT).format('YYYY-MM-DD');
            this.setState({ AddTimeIn_Max: AddDate_Max+"T00:01"});
            this.setState({ EditTimeIn_Max: AddDate_Max+"T00:01"});

            this.setState({ AddTempTimeOut_Max: AddDate_Max+"T00:01"});
            this.setState({ EditTempTimeOut_Max: AddDate_Max+"T00:01"});

            
            var AddDate_Min = moment(timeSheetPeriod[zz].timeSheetPeriodStartDate,process.env.API_DATE_FORMAT).subtract(1, "days").format('YYYY-MM-DD');
            this.setState({ AddTimeIn_Min: AddDate_Min+"T00:01"});
            var EditDate_Min = moment(timeSheetPeriod[zz].timeSheetPeriodStartDate,process.env.API_DATE_FORMAT).subtract(2, "days").format('YYYY-MM-DD');
            this.setState({ EditTimeIn_Min: EditDate_Min+"T00:01"});

            this.setState({ CurrentDate_Min: AddDate_Min});
            this.setState({ CurrentDate_Max: AddDate_Max});
            this.setState({ isCurrent: timeSheetPeriod[zz].isCurrentTimeSheet});
            

            
            $('#timeSheetPeriod').val(timeSheetPeriod[zz].timeSheetPeriodId).trigger('change');

            this.setState({ timeSheetPeriodId: timeSheetPeriod[zz].timeSheetPeriodId});

            this.setState({ CurrenttimeSheetPeriodId: timeSheetPeriod[zz].timeSheetPeriodId});

            this.setState({ DueDate: timeSheetPeriod[zz].dueDate });
            this.setState({ dueDateAmendment: timeSheetPeriod[zz].dueDateAmendment });

          }
          i++;
        }
      }
  }

  Default_TimeSheet2(timesheetPeriodId){
      var timeSheetPeriod = this.state.timeSheetPeriodViews;
      var length = timeSheetPeriod.length;
      var MinDate = moment(new Date()).format("YYYY-MM-DD");
      var MaxDate = moment(new Date()).format("YYYY-MM-DD");

     console.log('timeSheetPeriod');
     console.log(timeSheetPeriod);
     console.log(timesheetPeriodId);
      if (length > 0) {
        var i = 1;
        for (var zz = 0; zz < length; zz++) {

          if(timeSheetPeriod[zz].isCurrentTimeSheet == true && timesheetPeriodId == ""){
            var AddDate_Max =  moment(timeSheetPeriod[zz].timeSheetPeriodEndDate,process.env.API_DATE_FORMAT).format('YYYY-MM-DD');
            this.setState({ AddTimeIn_Max: AddDate_Max+"T00:01"});
            this.setState({ EditTimeIn_Max: AddDate_Max+"T00:01"});

            this.setState({ AddTempTimeOut_Max: AddDate_Max+"T00:01"});
            this.setState({ EditTempTimeOut_Max: AddDate_Max+"T00:01"});

            
            var AddDate_Min = moment(timeSheetPeriod[zz].timeSheetPeriodStartDate,process.env.API_DATE_FORMAT).subtract(1, "days").format('YYYY-MM-DD')
            this.setState({ AddTimeIn_Min: AddDate_Min+"T00:01"});
            var EditDate_Min = moment(timeSheetPeriod[zz].timeSheetPeriodStartDate,process.env.API_DATE_FORMAT).subtract(2, "days").format('YYYY-MM-DD');
            this.setState({ EditTimeIn_Min: EditDate_Min+"T00:01"});

            this.setState({ CurrentDate_Min: AddDate_Min});
            this.setState({ CurrentDate_Max: AddDate_Max});
            this.setState({ isCurrent: timeSheetPeriod[zz].isCurrentTimeSheet});
            

            
            $('#timeSheetPeriod').val(timeSheetPeriod[zz].timeSheetPeriodId).trigger('change');

            this.setState({ timeSheetPeriodId: timeSheetPeriod[zz].timeSheetPeriodId});

            this.setState({ CurrenttimeSheetPeriodId: timeSheetPeriod[zz].timeSheetPeriodId});

            this.setState({ DueDate: timeSheetPeriod[zz].dueDate });
            this.setState({ dueDateAmendment: timeSheetPeriod[zz].dueDateAmendment });

          }else if(timesheetPeriodId == timeSheetPeriod[zz].timeSheetPeriodId){
            //alert(timeSheetPeriod[zz].isCurrentTimeSheet);
            //alert(this.state.timeSheetPeriodId);
            var AddDate_Max =  moment(timeSheetPeriod[zz].timeSheetPeriodEndDate,process.env.API_DATE_FORMAT).format('YYYY-MM-DD');
            this.setState({ AddTimeIn_Max: AddDate_Max+"T00:01"});
            this.setState({ EditTimeIn_Max: AddDate_Max+"T00:01"});

            this.setState({ AddTempTimeOut_Max: AddDate_Max+"T00:01"});
            this.setState({ EditTempTimeOut_Max: AddDate_Max+"T00:01"});

            
            var AddDate_Min = moment(timeSheetPeriod[zz].timeSheetPeriodStartDate,process.env.API_DATE_FORMAT).subtract(1, "days").format('YYYY-MM-DD')
            this.setState({ AddTimeIn_Min: AddDate_Min+"T00:01"});
            var EditDate_Min = moment(timeSheetPeriod[zz].timeSheetPeriodStartDate,process.env.API_DATE_FORMAT).subtract(2, "days").format('YYYY-MM-DD');
            this.setState({ EditTimeIn_Min: EditDate_Min+"T00:01"});

            this.setState({ CurrentDate_Min: AddDate_Min});
            this.setState({ CurrentDate_Max: AddDate_Max});
            this.setState({ isCurrent: timeSheetPeriod[zz].isCurrentTimeSheet});
            

            
            $('#timeSheetPeriod').val(timeSheetPeriod[zz].timeSheetPeriodId).trigger('change');

            this.setState({ timeSheetPeriodId: timeSheetPeriod[zz].timeSheetPeriodId});

            this.setState({ CurrenttimeSheetPeriodId: timeSheetPeriod[zz].timeSheetPeriodId});

            this.setState({ DueDate: timeSheetPeriod[zz].dueDate });
            this.setState({ dueDateAmendment: timeSheetPeriod[zz].dueDateAmendment });
          }
          i++;
        }
      }
  }

  GetUserPastTimeSheetData(){
    this.showLoader();
    var url=process.env.API_API_URL+'GetUserPastTimeSheetData?contactIdFillFor='+this.state.staffContactID;
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
      //body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
    //  console.log("responseJson GetUserPastTimeSheetData");
    //  console.log(data);
    //  console.log(data.data);
      // debugger;
      if (data.responseType === "1") {
        // Profile & Contact
        if(data.data != null){
          this.setState({ ListGridPast: data.data });
        }
        
        //console.log(data.data.userSkillInfo);
        //this.setState({ ListGrid: this.rowData(data.data.userSkillInfo) })
      }else{
        if(data.message == 'Authorization has been denied for this request.'){
          SystemHelpers.SessionOut();
          this.props.history.push("/login");
        }else{
          SystemHelpers.ToastError(data.message);
        }
      }
      this.hideLoader();
    })
    .catch(error => {
      this.props.history.push("/error-500");
    });
  }


  Edit_Update_Btn_Func(record){
    let return_push = [];
    //console.log(record);
    if(this.state.role_timesheet_can.timesheet_can_update == true || this.state.role_timesheet_can.timesheet_can_delete == true){
      let Edit_push = [];
      if(this.state.role_timesheet_can.timesheet_can_update == true  ){
        Edit_push.push(
          <a href="#" onClick={this.EditRecord(record)} className="dropdown-item" data-toggle="modal" data-target="#CurrentTimeStaff_Edit_modal"><i className="fa fa-pencil m-r-5" /> Edit</a>
        );
      }
      let Delete_push = [];
      if(this.state.role_timesheet_can.timesheet_can_delete == true){
        Delete_push.push(
          <a href="#" onClick={this.DeleteInfo(record)}  className="dropdown-item" data-toggle="modal" data-target="#timesheet_Delete_modal"><i className="fa fa-trash-o m-r-5" /> Delete</a>
        );
      }
      
      return_push.push(
        <div className="dropdown dropdown-action">
          <a aria-expanded="false" data-toggle="dropdown" className="action-icon dropdown-toggle" href="#"><i className="material-icons">more_vert</i></a>
          <div className="dropdown-menu dropdown-menu-right">
            {Edit_push}
            {Delete_push}
          </div>
        </div>
      );
    }
    return return_push;
  }

  DeleteInfo = (record) => e =>{
    e.preventDefault();
    //console.log(record);

    var isAmendment = false;
    if(this.state.isTimeSheetSubmitted == true){
      isAmendment = true;
    }else{
      isAmendment = false;
    }
    this.setState({ isAmedmends: isAmendment });
    
    var temparray = '';

    var ListGrid = this.state.ListGrid;

    for (var z = 0; z < ListGrid.length; z++)
    {
      if(ListGrid[z].tempGuid == record.tempGuid)
      {
        temparray=temparray+ListGrid[z].timeSheetTransactionId+",";
      }  
    }
    temparray=temparray.substring(0, temparray.length - 1);
    //this.setState({ DeletetimesheetTransactionId: record.timeSheetTransactionId });
    this.setState({ DeletetimesheetTransactionId: temparray });

    console.log('temparray');
    console.log(temparray);
  }

  EditRecord = (record) => e => {
  //  console.log('Edit Current Time Staff');
  //  console.log(record);

    
    $('#EditNotes').val('');
    


    this.setState({ EditPreference: [] });
    this.setState({ AddPreference: [] });

    this.setState({ timeSheetTransactionId: record.timeSheetTransactionId });

    this.setState({ recordType:  record.recordType });
    this.setState({ isAmendmends:  record.isAmendmends });

    this.setState({ EditDate: moment(record.timeSheetDate).format('YYYY-MM-DD') });
    this.setState({ EditTimeIn: moment(record.timeIN) });
    this.setState({ EditTimeOut:  moment(record.timeOUT) });
    
    //this.setState({ EditTimeOut_Min: moment(moment(record.timeIN).subtract(1, 'day').format('YYYY/MM/DD HH:mm A'))});
    //this.setState({ EditTimeOut_Max: moment(moment(record.timeIN).add(1, 'day').format('YYYY/MM/DD HH:mm A'))});

    this.setState({ EditTimeOut_Min: moment(record.timeIN).subtract(1, 'day')});
    this.setState({ EditTimeOut_Max: moment(record.timeIN).add(2, 'day')});

    if(record.serviceId ==  process.env.API_24HOURS_SERVICE ){
      this.setState({ EditTimeOutDisabled: true});
    }else{
      this.setState({ EditTimeOutDisabled: false});
    }
    
    this.setState({ Editlocation: record.timeLocationId });
    this.setState({ EditService: record.serviceId });
    this.setState({ EditNotes: record.note });
    this.setState({ EditNoOfHours: record.noOfHours });

    this.setState({ EditPreferenceService: record.payTimeOffId });

    if(record.payTimeOffId !='' && record.payTimeOffId != null){
      this.GetPayOffDetailsForUser(record.serviceId,record.timeOUT,true,record.timeLocationId,'Edit');
    }

    if(record.serviceId == process.env.API_ON_CALL){
      $('#EditService').prop('disabled', true);
      $('#Editlocation').prop('disabled', true);
      this.setState({ EditTimeInDisabled: true });
      this.setState({ EditTimeOutDisabled: true });
    }else{
      $('#EditService').prop('disabled', false);
      $('#Editlocation').prop('disabled', false);
      this.setState({ EditTimeInDisabled: false });
      this.setState({ EditTimeOutDisabled: false });
    }

    
  }

  GetProfile(){
  //  console.log(localStorage.getItem("token"));
    this.showLoader();
    var url=process.env.API_API_URL+'GetUserBasicInfoById?contactId='+this.state.staffContactID;
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
      //body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
        console.log("responseJson GetUserBasicInfoById");
        console.log(data);
        //console.log(data.data.userRole);
        // debugger;
        
         
        if (data.responseType === "1") {
            this.setState({ all_data: data.data});
            
            this.setState({ cellPhone: data.data.cellPhone});
            this.setState({ phssEmail: data.data.phssEmail});
            this.setState({ Preferred_Name: data.data.preferredName});
            this.setState({ get_profile_call: true});
            this.setState({ staffNumberId: data.data.staffNumberId});
            this.setState({ primaryLocationId: data.data.primaryLocationId}); 
            this.setState({ primarylocation: data.data.primaryLocation}); 
            this.setState({ reportToUserName: data.data.reportToUserName});  
            this.setState({ userRoleDisplay: data.data.userRoleDisplay}); 
            this.setState({ locationIdGuid: data.data.locationIdGuid}); 

            this.setState({ TimeSheetApproverName: data.data.timeSheetApproverName}); 

          //  console.log('primaryLocationId' + data.data.primaryLocationId);

            if(data.data.locationIdGuid != '' && data.data.locationIdGuid != null){
              this.setState({ Addlocation: data.data.locationIdGuid});
            }
             
            this.setState({ UserHoursType: data.data.employmentHours}); 

            //locationIdGuid
        }else{
              if(data.message == 'Authorization has been denied for this request.'){
                SystemHelpers.SessionOut();
                this.props.history.push("/login");
              }else{
                SystemHelpers.ToastError(data.message);
              }
              
        }
        this.hideLoader();
        
    })
    .catch(error => {
      this.props.history.push("/error-500");
    });
  }

  GetTimeSheetId(TimeSheetStartDate){
  //  console.log(TimeSheetStartDate);
      var timeSheetPeriod = this.state.timeSheetPeriodViews;
      var length = timeSheetPeriod.length;

      if (length > 0) {
        var i = 1;
        for (var zz = 0; zz < length; zz++) {

          var StartDate = moment(timeSheetPeriod[zz].timeSheetPeriodStartDate).format('YYYY-MM-DD');
          var EndDate = moment(timeSheetPeriod[zz].timeSheetPeriodEndDate).format('YYYY-MM-DD');

          if(StartDate<=TimeSheetStartDate &&  TimeSheetStartDate<= EndDate){
            return timeSheetPeriod[zz];
          }

          if(i == length){
            return false;
          }

          i++;
        }
      } 
  }

  ClearRecord = ()=> e => {
    e.preventDefault();

    this.setState({ AddTimeIn : '' });
    this.setState({ AddTimeOut : '' });
    //this.setState({ Addlocation : '' });
    this.setState({ Addlocation: this.state.locationIdGuid}); 
    this.setState({ AddNotes : '' });
    this.setState({ AddNoOfHours : '' });
    this.setState({ AddService : '' });

    this.setState({ errormsg: '' });
  }

  AddRecord = () => e => {
    //debugger;

    e.preventDefault();

    let step1Errors = {};

    var timeSheetStartDate = '';
    var timeSheetEndDate = '';
    var timeSheetContactMasterId = '';
    var timeSheetPeriodId = '';
    var dueDate ='';
    
    var TimeSheet_PeriodViews = this.state.timeSheetPeriodViews;

  //  console.log('timeSheetPeriodViews');
    //console.log(TimeSheet_PeriodViews);
    var IsEntitlemntBasedOnStartDate = true;
    if(this.state["AddTimeIn"] != "")
    {
      

      for (var i = 0; i < TimeSheet_PeriodViews.length; i++) {
        // this.state["AddTimeOut"]

        var checkStartDate = moment(this.state["AddTimeIn"]).format('YYYY-MM-DD');
        var checkEndDate = moment(this.state["AddTimeOut"]).format('YYYY-MM-DD');


        if(checkStartDate != checkEndDate){
          

          var FirstDay1 = moment(this.state["AddTimeIn"]).format('YYYY-MM-DD HH:mm');
          var FirstDay2 = checkStartDate+" 24:00";

          var SecondDay1 = checkEndDate+" 00:00";
          var SecondDay2 = moment(this.state["AddTimeOut"]).format('YYYY-MM-DD HH:mm');

          console.log("FirstDay1 => "+FirstDay1);
          console.log("FirstDay2 => "+FirstDay2);

          //console.log("SecondDay1 => "+SecondDay1);
          //console.log("SecondDay2 => "+SecondDay2);

          var duration1 = moment.duration(moment(FirstDay2).diff(moment(FirstDay1)));
          var FirstDayMinutes = duration1.asMinutes();

          var duration2 = moment.duration(moment(SecondDay2).diff(moment(SecondDay1)));
          var SecondDayMinutes = duration2.asMinutes();

          //console.log(FirstDayMinutes);
          //console.log(SecondDayMinutes);

          if(FirstDayMinutes > SecondDayMinutes && FirstDayMinutes != SecondDayMinutes){
            var checkEndDate = moment(this.state["AddTimeIn"]).format('YYYY-MM-DD');
            IsEntitlemntBasedOnStartDate = true;
          }else{
            var checkEndDate = moment(this.state["AddTimeOut"]).format('YYYY-MM-DD');
            IsEntitlemntBasedOnStartDate = false;
          }

        }else{
          var checkEndDate = moment(this.state["AddTimeOut"]).format('YYYY-MM-DD');
          IsEntitlemntBasedOnStartDate = false;
        }

        console.log(checkEndDate);
        //return false;
        //var checkEndDate = moment(this.state["AddTimeIn"]).format('YYYY-MM-DD')
        var start_date = moment(TimeSheet_PeriodViews[i].timeSheetPeriodStartDate,process.env.API_DATE_FORMAT).format('YYYY-MM-DD');
        var end_date = moment(TimeSheet_PeriodViews[i].timeSheetPeriodEndDate,process.env.API_DATE_FORMAT).format('YYYY-MM-DD');
        //var check_is_exists = moment(checkEndDate).isBetween(start_date, end_date);
        var check_is_exists = moment(checkEndDate).isBetween(start_date, end_date, null, '[]');
      //  console.log('start_date => '+ start_date + ' end_date => '+ end_date + ' checkEndDate => '+ checkEndDate);
      //  console.log('time sheet check end date');
      //  console.log(check_is_exists);
        if(check_is_exists == true || end_date == checkEndDate)
        {

          timeSheetStartDate = start_date;
          timeSheetEndDate = end_date;
          timeSheetContactMasterId = TimeSheet_PeriodViews[i].timeSheetContactMasterId;
          timeSheetPeriodId = TimeSheet_PeriodViews[i].timeSheetPeriodId;
          dueDate = TimeSheet_PeriodViews[i].dueDate;
        }
      }
    }
    //return false;

    // if (this.state["AddDate"] =='') {
    //   step1Errors["AddDate"] = "Date is mandatory";
    // }
    

    var AddTimeInDate = moment(this.state["AddTimeIn"]).format("MM-DD-YYYY");
    // var sheetinfo = this.GetTimeSheetId(AddTimeInDate);
    // console.log('GetTimeSheetId');
    // console.log(sheetinfo);

    // if(sheetinfo  == false){
    //   step1Errors["AddTimeIn"] = "Please Check Your Timesheet Date.";
    // }

    if (this.state["AddTimeIn"] == '') {
      step1Errors["AddTimeIn"] = "TimeIn is mandatory";
    }

    if (this.state["AddTimeOut"] == '') {
      step1Errors["AddTimeOut"] = "TimeOut is mandatory";
    }

    var AddTimeInDateTemp = moment(this.state["AddTimeIn"]).format("YYYY-MM-DD");
    var AddTimeOutDateTemp = moment(this.state["AddTimeOut"]).format("YYYY-MM-DD");
    //console.log("AddTimeInDateTemp =>"+AddTimeInDateTemp+" CurrentDate_Min =>"+this.state.CurrentDate_Min);
    if(AddTimeInDateTemp == this.state.CurrentDate_Min && AddTimeOutDateTemp == this.state.CurrentDate_Min){
      step1Errors["AddTimeOut"] = "Time IN and Time OUT values should not belong to Previous pay period dates.";
    }

    if (this.state["Addlocation"] == '') {
      step1Errors["Addlocation"] = "Location is mandatory";
    }

    if (this.state["AddService"] == '') {
      step1Errors["AddService"] = "Service is mandatory";
    }

    if(this.state.IsNoOfHoursvalid == false && this.state.AddService != process.env.API_ON_CALL){
      step1Errors["AddNoOfHours"] = "Enter Valid TimeIn and TimeOut.";
    }

    if(this.state.AddPreference.length > 0 && this.state.AddPreferenceService == ''){
      step1Errors["AddPreferenceService"] = "Payout Preference is mandatory.";
    }

    if(timeSheetPeriodId == "")
    {
      SystemHelpers.ToastError("Please contact system administrator");
      return false;
    }




  //  console.log(step1Errors);

    this.setState({ errormsg: step1Errors });
        
    if (Object.keys(step1Errors).length > 0) {
        return false;
    }

    //return false;

    // find CategoryId, SubCategoryId, OperationType
    var CategoryId = '';
    var SubCategoryId = '';
    var OperationType = '';

    var serviceView= this.state.serviceView;
    // console.log("All Service View Drop Down");
    // console.log(this.state.serviceView);

    for (var z = 0; z < serviceView.length; z++)
    {
      // console.log("For if 1");
      // console.log(serviceView[z].serviceId);
      // console.log(this.state["AddService"]);
      if(serviceView[z].serviceId == this.state["AddService"])
      {
        // console.log("For if 2");
        // console.log(this.state.AddPreferenceService);
        // console.log(process.env.API_BANK_HOURS);
        //if(this.state.AddPreferenceService == process.env.API_BANK_HOURS){
        //if(serviceView[z].operationType != "Bank Hours"){
          //console.log("For if 3");
          //console.log(serviceView[z].operationType);
          OperationType = serviceView[z].operationType;
        //}
        // HR
        /*else if(serviceView[z].operationType !=""){
          console.log("For if 4");
          console.log(serviceView[z].operationType);
          OperationType = serviceView[z].operationType;
        }*/

        // HR

        CategoryId = serviceView[z].categoryId;
        SubCategoryId = serviceView[z].subCategoryId;
        
      }
      
    }


    // find CategoryId, SubCategoryId, OperationType

    // console.log("AddPreferenceService");
    // console.log(this.state.AddPreferenceService);
    // console.log("API_BANK_HOURS");
    // console.log(process.env.API_BANK_HOURS);

    this.showLoader();
    var AddTimeIn=moment(this.state["AddTimeIn"]).format('YYYY-MM-DD hh:mm A');
    var AddTimeOut=moment(this.state["AddTimeOut"]).format('YYYY-MM-DD hh:mm A');
    
    //return false;

    var isAmendment = false;
    if(this.state.isTimeSheetSubmitted == true){
      isAmendment = true;
    }else{
      isAmendment = false;
    }

    let currentDate = moment();
    //let currentDate = moment('2021-08-15');
    let DueDate = moment(dueDate);

    var CheckisAmendment = DueDate.diff(currentDate, 'days');

    if(CheckisAmendment < 0){
      isAmendment = true;
    }
    
    //console.log(DueDate.diff(currentDate, 'days'));// => 1
    //console.log(moment());
    //return false;

    let ArrayJson = [];

    if(this.state.AddService == process.env.API_ON_CALL){

      var today = moment();
      let datesCollection = [];
      //console.log(this.state["AddTimeIn"]);
      //console.log(this.state["AddTimeOut"]);
      var startdate = moment(this.state["AddTimeIn"], "DD/MM/YYYY");

      if(this.state["AddTimeOut"] != ''){
        var enddate = moment(this.state["AddTimeOut"], "DD/MM/YYYY");
        var diff_day_count = enddate.diff(startdate, 'days') // 1
      }else{
        var diff_day_count = 1;
      }
      

      //console.log(diff_day_count);
      var z = 0;
      var zx = 1;

      var TempTimeIn=moment(this.state["AddTimeIn"]).format('hh:mm A');
      var TempTimeOut=moment(this.state["AddTimeOut"]).format('hh:mm A');
      var checkEndDateOnCall = moment(this.state["AddTimeOut"]).format('YYYY-MM-DD');
      ArrayJson.push({
            serviceId: this.state["AddService"],
            timeSheetDate : checkEndDateOnCall,
            PayTimeOffId: this.state["AddPreferenceService"],
            timeIN: AddTimeIn,
            timeOUT: AddTimeOut,
            timeLocationId: this.state["Addlocation"],
            note: this.state["AddNotes"],
            noOfHours: '00:00',
            timeSheetTransactionId:'',
            IsAmendmends:isAmendment,
            RecordType:'',
            WantToDisplayInTimesheet:true,
            categoryId:CategoryId,
            subCategoryId:SubCategoryId,
            operationType:OperationType,
            IsEntitlemntBasedOnStartDate : false
      });

      for (var i = 0; i <= diff_day_count; i++) {
        var AddTimeIn = moment(this.state["AddTimeIn"]).add(z, 'days').format('YYYY-MM-DD');
        var AddTimeOut = moment(this.state["AddTimeIn"]).add(zx, 'days').format('YYYY-MM-DD');

        ArrayJson.push({
            serviceId: this.state["AddService"],
            timeSheetDate : AddTimeIn,
            PayTimeOffId: this.state["AddPreferenceService"],
            timeIN: AddTimeIn+" "+TempTimeIn,
            timeOUT: AddTimeIn+" "+TempTimeOut,
            timeLocationId: this.state["Addlocation"],
            note: this.state["AddNotes"],
            noOfHours: '00:00',
            timeSheetTransactionId:'',
            IsAmendmends:isAmendment,
            RecordType:'',
            WantToDisplayInTimesheet:false,
            categoryId:CategoryId,
            subCategoryId:SubCategoryId,
            operationType:OperationType,
            IsEntitlemntBasedOnStartDate : IsEntitlemntBasedOnStartDate
        });

        z++;
        zx++;
      }

    }else{
      var AddTimeInDate = moment(checkEndDate,'YYYY-MM-DD').format("MM-DD-YYYY");

      ArrayJson.push({
            serviceId: this.state["AddService"],
            timeSheetDate : AddTimeInDate,
            PayTimeOffId: this.state["AddPreferenceService"],
            timeIN: AddTimeIn,
            timeOUT: AddTimeOut,
            timeLocationId: this.state["Addlocation"],
            note: this.state["AddNotes"],
            noOfHours: this.state["AddNoOfHours"],
            timeSheetTransactionId:'',
            IsAmendmends:isAmendment,
            RecordType:'',
            WantToDisplayInTimesheet:true,
            categoryId:CategoryId,
            subCategoryId:SubCategoryId,
            operationType:OperationType,
            IsEntitlemntBasedOnStartDate : IsEntitlemntBasedOnStartDate
      });
    }


    //console.log(ArrayJson);
    //console.log("=======================Timesheet Entry=======================");
    //return false;
    

    //var timeSheetStartDate = moment(sheetinfo.timeSheetPeriodStartDate).format("YYYY-MM-DD");
    //var timeSheetEndDate = moment(sheetinfo.timeSheetPeriodEndDate).format("YYYY-MM-DD");

    //var timeSheetStartDate = moment(this.state.CurrentDate_Min).format("YYYY-MM-DD");
    //var timeSheetEndDate = moment(this.state.CurrentDate_Max).format("YYYY-MM-DD");
    
    //var timeSheetPeriodId = this.state.timeSheetPeriodId;
    //var timeSheetContactMasterId=this.state.timeSheetContactMasterId;

    var IsOnCall = false;
    if(this.state.AddService == process.env.API_ON_CALL){
      var IsOnCall = true;
    }
    

    let bodyarray = {};
    bodyarray["timeSheetContactMasterId"] = timeSheetContactMasterId;
    bodyarray["isTimeSheetSubmitted"] = false;
    bodyarray["TimeSheetPeriodId"] = timeSheetPeriodId;
    bodyarray["timeSheetStartDate"] = timeSheetStartDate;
    bodyarray["timeSheetEndDate"] = timeSheetEndDate;
    bodyarray["timeSheetContactId"] = this.state.staffContactID;
    bodyarray["timeSheetCreateBy"] = this.state.staffContactID;
    bodyarray["IsOnCall"] = IsOnCall;
    bodyarray["UpdateByName"] = this.state.fullName;
    bodyarray["timeSheetConatctSheetViews"] = ArrayJson;

    //console.log(bodyarray);
    //return false;
    var url=process.env.API_API_URL+'InsertUpdateUserTimeSheetData';
    fetch(url, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
      body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
      //  console.log("responseJson userSkillInfo");
      //  console.log(data);
        //console.log(responseJson);
        // debugger;
        if (data.responseType === "1") {
            //this.props.history.push('/dashboard');
            this.setState({ AddTimeIn : '' });
            this.setState({ AddTimeOut : '' });
            //this.setState({ Addlocation : '' });
            this.setState({ Addlocation: this.state.locationIdGuid}); 
            this.setState({ AddNotes : '' });
            this.setState({ AddNoOfHours : '' });
            this.setState({ AddService : '' });
            this.setState({ AddPreferenceService : '' });
            
            //this.setState({ AllSubCategory : [] });
            //AllSubCategory userSkillCategories

            SystemHelpers.ToastSuccess(data.responseMessge);
            $( ".close" ).trigger( "click" );
            
            this.setState({ timeSheetPeriodViews: this.Preview_TimeSheet(data.data)});
            this.ChangePeriod2();
            //this.GetUserWiseTimeSheetData();  
        }
        else if(data.responseType === "2"){
           SystemHelpers.ToastError(data.responseMessge);
          // $( ".close" ).trigger( "click" );
        }
        else{
            SystemHelpers.ToastError(data.responseMessge);
        }
        this.hideLoader();
        
    })
    .catch(error => {
      this.props.history.push("/error-500");
    });
    return false;
  }

  UpdateRecord = () => e => {
    //debugger;
    e.preventDefault();

    let step1Errors = {};


    var timeSheetStartDate = '';
    var timeSheetEndDate = '';
    var timeSheetContactMasterId = '';
    var timeSheetPeriodId = '';
    var dueDate = '';
    var TimeSheet_PeriodViews = this.state.timeSheetPeriodViews;

  //  console.log('timeSheetPeriodViews');
  //  console.log(TimeSheet_PeriodViews);
    var IsEntitlemntBasedOnStartDate = true;
   
    if(this.state["EditTimeIn"] != "")
    {
      

      for (var i = 0; i < TimeSheet_PeriodViews.length; i++) {

        var checkStartDate = moment(this.state["EditTimeIn"]).format('YYYY-MM-DD');
        var checkEndDate = moment(this.state["EditTimeOut"]).format('YYYY-MM-DD');

        if(checkStartDate != checkEndDate){
          

          var FirstDay1 = moment(this.state["EditTimeIn"]).format('YYYY-MM-DD HH:mm');
          var FirstDay2 = checkStartDate+" 24:00";

          var SecondDay1 = checkEndDate+" 00:00";
          var SecondDay2 = moment(this.state["EditTimeOut"]).format('YYYY-MM-DD HH:mm');

          console.log("FirstDay1 => "+FirstDay1);
          console.log("FirstDay2 => "+FirstDay2);

          console.log("SecondDay1 => "+SecondDay1);
          console.log("SecondDay2 => "+SecondDay2);

          var duration1 = moment.duration(moment(FirstDay2).diff(moment(FirstDay1)));
          var FirstDayMinutes = duration1.asMinutes();

          var duration2 = moment.duration(moment(SecondDay2).diff(moment(SecondDay1)));
          var SecondDayMinutes = duration2.asMinutes();

          console.log(FirstDayMinutes);
          console.log(SecondDayMinutes);

          if(FirstDayMinutes > SecondDayMinutes && FirstDayMinutes != SecondDayMinutes){
            var checkEndDate = moment(this.state["EditTimeIn"]).format('YYYY-MM-DD');
            IsEntitlemntBasedOnStartDate = true;
          }else{
            var checkEndDate = moment(this.state["EditTimeOut"]).format('YYYY-MM-DD');
            IsEntitlemntBasedOnStartDate = false;
          }

        }else{
          var checkEndDate = moment(this.state["EditTimeOut"]).format('YYYY-MM-DD');
          IsEntitlemntBasedOnStartDate = false;
        }

        console.log(checkEndDate);

        // this.state["AddTimeOut"]
        //var checkEndDate = moment(this.state["EditTimeOut"]).format('YYYY-MM-DD');
        //var checkEndDate = moment(this.state["EditTimeIn"]).format('YYYY-MM-DD')
        var start_date = moment(TimeSheet_PeriodViews[i].timeSheetPeriodStartDate,process.env.API_DATE_FORMAT).format('YYYY-MM-DD');
        var end_date = moment(TimeSheet_PeriodViews[i].timeSheetPeriodEndDate,process.env.API_DATE_FORMAT).format('YYYY-MM-DD');
        //var check_is_exists = moment(checkEndDate).isBetween(start_date, end_date);
        var check_is_exists = moment(checkEndDate).isBetween(start_date, end_date, null, '[)');
       console.log('start_date => '+ start_date + ' end_date => '+ end_date + ' checkEndDate => '+ checkEndDate);
       console.log('time sheet check end date');
       console.log(check_is_exists);
        if(check_is_exists == true || end_date == checkEndDate)
        {
          timeSheetStartDate = start_date;
          timeSheetEndDate = end_date;
          timeSheetContactMasterId = TimeSheet_PeriodViews[i].timeSheetContactMasterId;
          timeSheetPeriodId = TimeSheet_PeriodViews[i].timeSheetPeriodId;
          dueDate = TimeSheet_PeriodViews[i].dueDate;
        }
      }
    }

    console.log('timeSheetPeriodId =>'+timeSheetPeriodId);

    //return false;
    
    // if (this.state["EditDate"] =='') {
    //   step1Errors["EditDate"] = "Date is mandatory";
    // }

 
    if (this.state["EditTimeIn"] == '') {
      step1Errors["EditTimeIn"] = "TimeIn is mandatory";
    }

    if (this.state["EditTimeOut"] == '') {
      step1Errors["EditTimeOut"] = "TimeOut is mandatory";
    }

    var EditTimeInDateTemp = moment(this.state["EditTimeIn"]).format("YYYY-MM-DD");
    var EditTimeOutDateTemp = moment(this.state["EditTimeOut"]).format("YYYY-MM-DD");
    if(EditTimeInDateTemp == this.state.CurrentDate_Min && EditTimeOutDateTemp == this.state.CurrentDate_Min){
      step1Errors["EditTimeOut"] = "Time IN and Time OUT values should not belong to Previous pay period dates.";
    }

    if (this.state["Editlocation"] == '') {
      step1Errors["Editlocation"] = "Location is mandatory";
    }

    if (this.state["EditService"] == '') {
      step1Errors["EditService"] = "Service is mandatory";
    }

    if(this.state.IsNoOfHoursvalidEdit == false){
      step1Errors["EditNoOfHours"] = "Enter Valid TimeIn and TimeOut.";
    }

    if(timeSheetPeriodId == "")
    {
      SystemHelpers.ToastError("Please contact system administrator");
      return false;
    }


  //  console.log(step1Errors);

    this.setState({ errormsg: step1Errors });
        
    if (Object.keys(step1Errors).length > 0) {
        return false;
    }

    // find CategoryId, SubCategoryId, OperationType
    var CategoryId = '';
    var SubCategoryId = '';
    var OperationType = '';

    var serviceView= this.state.serviceView;
    for (var z = 0; z < serviceView.length; z++)
    {
      if(serviceView[z].serviceId == this.state["EditService"])
      {
        //if(this.state.EditPreferenceService == process.env.API_BANK_HOURS){
          OperationType = serviceView[z].operationType;
        //}

        CategoryId = serviceView[z].categoryId;
        SubCategoryId = serviceView[z].subCategoryId;
        //OperationType = serviceView[z].operationType;
      }
      
    }


    // find CategoryId, SubCategoryId, OperationType

    

    this.showLoader();
    // var EditTimeIn=moment(this.state["EditDate"]+"T"+this.state["EditTimeIn"]).format('hh:mm A');
    // var EditTimeOut=moment(this.state["EditDate"]+"T"+this.state["EditTimeOut"]).format('hh:mm A');
    
    var EditTimeIn=moment(this.state["EditTimeIn"]).format('YYYY-MM-DD hh:mm A');
    var EditTimeOut=moment(this.state["EditTimeOut"]).format('YYYY-MM-DD hh:mm A');

    var isAmendment = false;
    if(this.state.isTimeSheetSubmitted == true){
      isAmendment = true;
    }else{
      isAmendment = false;
    }

    let currentDate = moment();
    //let currentDate = moment('2021-08-15');
    let DueDate = moment(dueDate);

    var CheckisAmendment = DueDate.diff(currentDate, 'days');

    if(CheckisAmendment < 0){
      isAmendment = true;
    }

    var EditTimeInDate = moment(checkEndDate,'YYYY-MM-DD').format("MM-DD-YYYY");

    let ArrayJson = [{
          serviceId: this.state["EditService"],
          timeSheetDate: EditTimeInDate,
          PayTimeOffId: this.state["EditPreferenceService"],
          timeIN: EditTimeIn,
          timeOUT: EditTimeOut,
          timeLocationId: this.state["Editlocation"],
          note: this.state["EditNotes"],
          noOfHours: this.state["EditNoOfHours"],
          timeSheetTransactionId:this.state.timeSheetTransactionId,
          isAmendmends:isAmendment,
          recordType:this.state.recordType,
          WantToDisplayInTimesheet:true,
          categoryId:CategoryId,
          subCategoryId:SubCategoryId,
          operationType:OperationType,
          IsEntitlemntBasedOnStartDate : IsEntitlemntBasedOnStartDate
    }];


    var IsOnCall = false;
    if(this.state.EditService == process.env.API_ON_CALL){
      var IsOnCall = true;
    }
     
    let bodyarray = {};
    
    bodyarray["timeSheetContactMasterId"] = timeSheetContactMasterId;
    bodyarray["isTimeSheetSubmitted"] = false;
    bodyarray["TimeSheetPeriodId"] = timeSheetPeriodId;
    bodyarray["timeSheetStartDate"] = timeSheetStartDate;
    bodyarray["timeSheetEndDate"] = timeSheetEndDate;
    bodyarray["timeSheetContactId"] = this.state.staffContactID;
    bodyarray["timeSheetCreateBy"] = this.state.staffContactID;
    bodyarray["IsOnCall"] = IsOnCall;
    bodyarray["UpdateByName"] = this.state.fullName;
    bodyarray["timeSheetConatctSheetViews"] = ArrayJson;

  //  console.log(bodyarray);
    //return false;
    var url=process.env.API_API_URL+'InsertUpdateUserTimeSheetData';
    fetch(url, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
      body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
      //  console.log("responseJson InsertUpdateUserTimeSheetData");
      //  console.log(data);
        //console.log(responseJson);
        // debugger;
        if (data.responseType === "1") {
            //this.props.history.push('/dashboard');

            
            //this.setState({ AllSubCategory : [] });
            //AllSubCategory userSkillCategories
            
            this.setState({ EditNotes : '' });

            SystemHelpers.ToastSuccess(data.responseMessge);
            $( ".close" ).trigger( "click" );
            //this.GetUserWiseTimeSheetData();  
            //this.ChangePeriod();  
            this.ChangePeriod2();
        }
        else{
            if(data.message == 'Authorization has been denied for this request.'){
                SystemHelpers.SessionOut();
                this.props.history.push("/login");
              }else{
                SystemHelpers.ToastError(data.responseMessge);
              }
        }
        this.hideLoader();
        
    })
    .catch(error => {
      this.props.history.push("/error-500");
    });
    return false;
  }

 

  PastDetails = (timesheetPeriodId, PastDetails) => e => {
    e.preventDefault();
    this.showLoader();
    // locationIdGuid 
    this.setState({ EdittimeSheetContactMasterId: ''});
    this.setState({ ListGridPastDetails: [] });
    var url=process.env.API_API_URL+'GetUserPastTimeSheetDataById?contactIdFillFor='+this.state.staffContactID+"&timesheetPeriodId="+timesheetPeriodId+"&locationId="+PastDetails.locationId+"&dedicatedApprover=false";
    //var url=process.env.API_API_URL+'GetUserWiseTimeSheetData?contactIdFillFor='+this.state.staffContactID+"&timesheetPeriodId="+timesheetPeriodId;
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
      //body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
    //  console.log("responseJson GetUserPastTimeSheetDataById");
    //  console.log(data);
      //console.log(data.data.timeSheetConatctSheetViews);
      // debugger;
      if (data.responseType === "1") {
        // Profile & Contact

        if(data != null){

          if(data.data.timesheetContactView != null){

              this.setState({ isTimeSheetSubmitted: data.data.isTimeSheetSubmitted });
              this.setState({ isAmendmendsSubmitted: data.data.isAmendmendsSubmitted });
              this.setState({ IsAmendmendsEnabled : data.data.isAmendmendsEnabled });

              this.setState({ ListGridPastDetails: data.data.timesheetContactView.timeSheetConatctSheetViews });

              //this.setState({ PasttimeSheetStartDate: data.data.timesheetContactView.timeSheetStartDate });
              //this.setState({ PasttimeSheetEndDate: data.data.timesheetContactView.timeSheetEndDate });
              var timeSheetPeriod = data.data.timeSheetPeriodViews;
              for (var zz = 0; zz < timeSheetPeriod.length; zz++) {
                if(timesheetPeriodId == timeSheetPeriod[zz].timeSheetPeriodId){
                  this.setState({ PasttimeSheetStartDate: timeSheetPeriod[zz].timeSheetPeriodStartDate});
                  this.setState({ PasttimeSheetEndDate: timeSheetPeriod[zz].timeSheetPeriodEndDate });
                }
              }
              

              this.setState({ EdittimeSheetContactMasterId: data.data.timesheetContactView.timeSheetContactMasterId });

              var locationViews = this.state.locationViews;
              var locationGuid  = '';
              for (var xx = 0; xx < locationViews.length; xx++) {
                if(locationViews[xx].locationName == PastDetails.locationName){
                  locationGuid = locationViews[xx].locationId;
                  //alert(locationGuid);
                  this.GetTimeSheetNotes(data.data.timesheetContactView.timeSheetContactMasterId,locationGuid);
                }
              } 


              //this.GetTimeSheetNotes(data.data.timesheetContactView.timeSheetContactMasterId,locationGuid);
          }
        }
        
        //console.log(data.data.userSkillInfo);
        //this.setState({ ListGrid: this.rowData(data.data.userSkillInfo) })
      }else{
        if(data.message == 'Authorization has been denied for this request.'){
          SystemHelpers.SessionOut();
          this.props.history.push("/login");
        }else{
          SystemHelpers.ToastError(data.message);
        }
      }
      this.hideLoader();
    })
    .catch(error => {
      this.props.history.push("/error-500");
    });
  }

  GetFileName(file) {
   return file.substr( (file.lastIndexOf('/') +1) );
  }

  openFile(file) {
    var extension = file.substr( (file.lastIndexOf('.') +1) );
    switch(extension) {
      case 'jpg':
      case 'png':
      case 'jpeg':
          return 'jpg';  // There's was a typo in the example where
      break;                         // the alert ended with pdf instead of gif.
      case 'xls':
      case 'xlsx':
      case 'csv':
          return 'xls';
      break;
      case 'docx':
      case 'doc':
          return 'doc';
      break;
      case 'pdf':
          return 'pdf';
      break;
      default:
        return '';
    }
  };

  GetChatList(){

    let final_list_push =[];
    let list_push =[];
    var list = this.state.ListNotes;
    for (var i = 0; i<list.length; i++) {

      let file_type_push =[];

      var isFileAttached = list[i].isFileAttached;
      var timeSheetNotesFile = list[i].timeSheetNotesFile;

      let att_img_push =[];
      let att_doc_push =[];

      if(isFileAttached == true){
        for (var j = 0; j<timeSheetNotesFile.length; j++) {
            var filetype=this.openFile(timeSheetNotesFile[j].base64String);

            if(filetype == 'jpg'){
              att_img_push.push(
                <a className="chat-img-attach" href={timeSheetNotesFile[j].base64String} target="_blank">
                  <img width={182} height={137} alt="" src={timeSheetNotesFile[j].base64String} />
                  <div className="chat-placeholder">
                    {/*<div className="chat-img-name">placeholder.jpg</div>
                    <div className="chat-file-desc">Download</div>*/}
                  </div>
                </a>
              );
            }else if(filetype == 'xls' || filetype == 'doc' || filetype == 'pdf' ){
                var filenm="Notes."+filetype;
                att_doc_push.push(
                  <ul class="attach-list">
                    <li><i class="fa fa-file"></i> <a href={timeSheetNotesFile[j].base64String} target="_blank" download="Notes" >{this.GetFileName(timeSheetNotesFile[j].base64String)}</a></li>
                  </ul>
                );
            }
            
        }

        file_type_push.push(
          <div className="chat-img-group clearfix">
            <p>Uploaded Files</p>
            {att_img_push}
          </div>
        );

      }

      

      
      

      let is_delete_push =[];
      let close_btn = [];
      let edit_btn = [];
      if(list[i].contactId == this.state.staffContactID){
        if(this.state.EditNoteId != '' && this.state.EditNoteId == list[i].noteId){
          close_btn.push(
            <li><a href="#" className="del-msg" onClick={this.ClearRecord()}><i className="fa fa-close" /></a></li>
          );
        }else{
          edit_btn.push(
            <li><a href="#" className="edit-msg" onClick={this.EditMsg(list[i])}><i className="fa fa-pencil" /></a></li>
          );
        }
        is_delete_push.push(
          <ul>
            {edit_btn}
            <li><a href="#" className="del-msg" onClick={this.DeleteMsg(list[i])}><i className="fa fa-trash-o" /></a></li>
            {close_btn}
          </ul>
        );
      }
      
      let msg_push =[];
      if((list[i].note != '' && list[i].note != null) && (isFileAttached == true && isFileAttached != null)){
        msg_push.push(
          <div className="chat-bubble 1">
            <div className="chat-content img-content">
              <span class="task-chat-user">{list[i].contactName}</span>
              <span class="chat-time chat-time-pk">{moment(list[i].createdOn).format(process.env.DATETIME_FORMAT)}</span>
              <p>{list[i].note}</p>
              <br/>
              {file_type_push}
              {att_doc_push}
            </div>
            <div className="chat-action-btns">
              {is_delete_push}
            </div>
          </div>
        );
      }else if((list[i].note != '' && list[i].note != null) && (isFileAttached == '' || isFileAttached == null)){
        msg_push.push(
          <div className="chat-bubble 2">
            <div class="chat-content">
              <span class="task-chat-user">{list[i].contactName}</span> 
              <span class="chat-time chat-time-pk">{moment(list[i].createdOn).format(process.env.DATETIME_FORMAT)}</span>
              <p>{list[i].note}</p>
            </div>

            <div className="chat-action-btns">
              {is_delete_push}
            </div>
          </div>
        );
      }else if((isFileAttached == true && isFileAttached != null) && (list[i].note == '' || list[i].note == null) ){
        msg_push.push(
          <div className="chat-bubble 3">
            <div className="chat-content img-content">
              <span class="task-chat-user">{list[i].contactName}</span>
              <span class="chat-time chat-time-pk">{moment(list[i].createdOn).format(process.env.DATETIME_FORMAT)}</span>
              
              {file_type_push}
            </div>
            <div className="chat-action-btns">
              {is_delete_push}
            </div>
          </div>
        );
      }

      list_push.push(
        <div className="chat chat-left">
          <div className="chat-avatar">
            <a href="/blue/app/profile/employee-profile" className="avatar">
              <img alt="" src={list[i].profileLink} />
            </a>
          </div>
          <div className="chat-body">
            {msg_push}
          </div>
        </div>
      );
    }

    final_list_push.push(
      <div className="chats">
      {list_push}
      </div>
    );
    

    return final_list_push;
  }

  GetTimeSheetNotes(timesheetcontactmasterId,locationGuid){
    this.setState({ ListNotes: [] });
    /* Role Management */
    var getrole = SystemHelpers.GetRole();
    //let canDelete = getrole.documents_can.documents_can_delete;
    let canDelete = false;
    /* Role Management */

    this.showLoader();
    var url=process.env.API_API_URL+'GetTimesheetNotesDetails?timesheetcontactmasterId='+timesheetcontactmasterId+"&locationId="+locationGuid;
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
      //body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
        console.log("responseJson GetTimesheetNotesDetails");
        console.log(data);
        //console.log(data.data.userRole);
        
        if (data.responseType === "1") {
            // Profile & Contact
            var list = data.data;
            let list_push =[];
            for (var i = 0; i<list.length; i++) {
              if(list[i].allowUserToSeeNotes == true){
                 list_push.push(list[i]);
              }
            }
            this.setState({ ListNotes: list_push });
            
        }else{
              if(data.message == 'Authorization has been denied for this request.'){
                SystemHelpers.SessionOut();
                this.props.history.push("/login");
              }else{
                SystemHelpers.ToastError(data.message);
              }
              
        }
        this.hideLoader();
        
    })
    .catch(error => {
      this.props.history.push("/error-500");
    });
  }

  ChangePeriod = e => {
    e.preventDefault();
    //console.log('ChangePeriod');
    this.ChangePeriod2();
    //alert($("#timeSheetPeriod").val());
    this.GetUserEntitlementSummary($("#timeSheetPeriod").val());
  }

  ChangePeriod2 () {
    //alert();
    this.showLoader();
    this.setState({ isamendmentFlag: false });
    //this.setState({ ListGrid: [] });
    var timesheetPeriodId = $("#timeSheetPeriod").val();
    

    if(timesheetPeriodId == null || timesheetPeriodId == ''){
      var timesheetPeriodId = localStorage.getItem("CurrenttimeSheetPeriodId");
    }

    //alert(timesheetPeriodId);

    this.setState({ timeSheetPeriodId: timesheetPeriodId});
    var url=process.env.API_API_URL+'GetUserWiseTimeSheetDataByPeriodProfileId?contactIdFillFor='+this.state.staffContactID+"&periodProfileId="+timesheetPeriodId;
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
      //body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
      console.log("responseJson GetUserWiseTimeSheetDataByPeriodProfileId 2");
      console.log(data);
      //console.log(data.data.timeSheetConatctSheetViews);
      // debugger;
      
      if (data.responseType === "1") {
        // Profile & Contact
       
        if(data.data != null){
            
          if(data.data.timeSheetConatctSheetViews != null){
            //  console.log(data.data.timeSheetConatctSheetViews);
              this.setState({ ListGrid: data.data.timeSheetConatctSheetViews });
              this.setState({ timeSheetContactMasterId: data.data.timeSheetContactMasterId });
              this.setState({ isTimeSheetSubmitted: data.data.isTimeSheetSubmitted });
              this.setState({ isAmendmendsSubmitted: data.data.isAmendmendsSubmitted });
              this.setState({ IsAmendmendsEnabled : data.data.isAmendmendsEnabled });

              this.setState({ isTimeSheetApproved : data.data.isTimeSheetApproved });
              this.setState({ isTimeSheetSubmitedToPayroll : data.data.isTimeSheetSubmitedToPayroll });
              
              this.setState({ workedHour: data.data.workedHour });
              //this.setState({ DueDate: data.data.timeSheetEndDate });

              var ContactViewtimesheet= data.data.timeSheetConatctSheetViews;
              for (var z = 0; z < ContactViewtimesheet.length; z++)
              {
                if(ContactViewtimesheet[z].recordType == "Amendment")
                {
                  this.setState({ isamendmentFlag: true });
                }
                
              }
              

              this.Default_TimeSheet2(timesheetPeriodId);
              this.GetUserEntitlementSummary(timesheetPeriodId);

          }else{
            this.setState({ ListGrid: [] });
            this.setState({ timeSheetContactMasterId: '' });
            this.setState({ isTimeSheetSubmitted: false });
            this.setState({ workedHour: '00.00' });
            this.setState({ DueDate: '' });

            this.setState({ isTimeSheetApproved: false });
            this.setState({ isTimeSheetSubmitedToPayroll: false });
            this.setState({ isamendmentFlag: false });

            this.Default_TimeSheet2(timesheetPeriodId);
            this.GetUserEntitlementSummary(timesheetPeriodId);
          }

          if(data.data.timeSheetPeriodViews != null){
            this.setState({ timeSheetPeriodViews: this.Preview_TimeSheet(data.data.timeSheetPeriodViews) });
            //this.Default_TimeSheet(data.data.timeSheetPeriodViews);
          }
          
          //this.setState({ serviceView: data.data.serviceView });
          //this.setState({ locationViews: data.data.locationViews });
      }else{
        console.log('ChangePeriod2');
        this.setState({ ListGrid: [] });
        this.setState({ timeSheetContactMasterId: '' });
        this.setState({ isTimeSheetSubmitted: false });
        this.setState({ workedHour: '00.00' });
        this.setState({ DueDate: '' });

        this.setState({ isTimeSheetApproved: false });
        this.setState({ isTimeSheetSubmitedToPayroll: false });
        this.setState({ isamendmentFlag: false });

        this.Default_TimeSheet2(timesheetPeriodId);
        this.GetUserEntitlementSummary(timesheetPeriodId);
      }
        
        //this.Default_TimeSheet(this.state.timeSheetPeriodViews);
        //console.log(data.data.userSkillInfo);
        //this.setState({ ListGrid: this.rowData(data.data.userSkillInfo) })
      }else{
        this.setState({ ListGrid: [] });
        this.setState({ timeSheetContactMasterId: '' });
        this.setState({ isTimeSheetSubmitted: false });
        this.setState({ workedHour: '00.00' });
        this.setState({ DueDate: '' });

        this.setState({ isTimeSheetApproved: false });
        this.setState({ isTimeSheetSubmitedToPayroll: false });
        this.setState({ isamendmentFlag: false });


        this.Default_TimeSheet2(timesheetPeriodId);
        this.GetUserEntitlementSummary(timesheetPeriodId);
        
        if(data.message == 'Authorization has been denied for this request.'){
          SystemHelpers.SessionOut();
          this.props.history.push("/login");
        }else{
          SystemHelpers.ToastError(data.message);
        }
      }
      this.hideLoader();
    })
    .catch(error => {
      this.props.history.push("/error-500");
    });
  }
  

  SubmitUserTimeSheet = () => e => {
    e.preventDefault();
    this.showLoader();
    let bodyarray = {};
    
    bodyarray["submitByContactId"] = this.state.staffContactID;
    bodyarray["timesheetContactMasterId"] = this.state.timeSheetContactMasterId;
    bodyarray["IsAmendmendSubmit"] = false;
    bodyarray["SubmitByName"] = this.state.fullName;

    var url=process.env.API_API_URL+'SubmitUserTimeSheet';
    fetch(url, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
      body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
      //  console.log("responseJson SubmitUserTimeSheet");
      //  console.log(data);
        //console.log(responseJson);
        // debugger;
        if (data.responseType === "1") {
            //this.props.history.push('/dashboard');

            
            //this.setState({ AllSubCategory : [] });
            //AllSubCategory userSkillCategories

            
            $( ".cancel-btn" ).trigger( "click" );
            SystemHelpers.ToastSuccess(data.responseMessge);
            this.GetUserWiseTimeSheetData();    
            this.GetUserPastTimeSheetData();
        }
        else{
            if(data.message == 'Authorization has been denied for this request.'){
                SystemHelpers.SessionOut();
                this.props.history.push("/login");
              }else{
                //SystemHelpers.ToastError(data.message);
              }
        }
        this.hideLoader();
        
    })
    .catch(error => {
      this.props.history.push("/error-500");
    });
    return false;
  }

  

  DeleteRecord = () => e => {
    e.preventDefault();
    this.showLoader();
    //isAmedmends
  //  console.log(this.state.DeletetimesheetTransactionId);
    var url=process.env.API_API_URL+'DeleteUserTimeSheetByTransactionId?timesheetTransactionId='+this.state.DeletetimesheetTransactionId+'&userName='+this.state.fullName+'&isAmedmends='+this.state.isAmedmends;
    fetch(url, {
      method: 'PUT',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
      //body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
      //  console.log("responseJson DeleteUserSkillSet");
      //  console.log(data);
        //console.log(data.data.userRole);
        // debugger;
        if (data.responseType === "1") {
            // Profile & Contact
            SystemHelpers.ToastSuccess(data.responseMessge);
            $( ".cancel-btn" ).trigger( "click" );
            this.ChangePeriod2();
            this.hideLoader();
        }else if (data.responseType == "2" || data.responseType == "3") {
            SystemHelpers.ToastError(data.responseMessge);
            $( ".cancel-btn" ).trigger( "click" );
            this.hideLoader();
        }else{
              if(data.message == 'Authorization has been denied for this request.'){
                SystemHelpers.SessionOut();
                this.props.history.push("/login");
              }else{
                SystemHelpers.ToastError(data.message);
              }
              this.hideLoader();
              $( ".cancel-btn" ).trigger( "click" );
        }
        
        
    })
    .catch(error => {
      this.props.history.push("/error-500");
    });
  }

  CheckStatus(isTimeSheetSubmitted,isTimeSheetApproved,isTimeSheetSubmitedToPayroll)
  {
    var returnstatus = '';
    if(isTimeSheetSubmitedToPayroll == true){
      return 'TimeSheetSubmitedToPayroll';
    }

    if(isTimeSheetApproved == true){
      return 'TimeSheetApproved';
    }

    if(isTimeSheetSubmitted == true){
      return 'TimeSheetSubmitted';
    }

    return returnstatus;
  }

  HeaderTitleFunc(){
    //console.log('HeaderTitleFunc '+ this.state.PreviewtimeSheetPeriodId +" "+this.state.timeSheetPeriodId);
    // if(this.state.PreviewtimeSheetPeriodId == this.state.timeSheetPeriodId){
    //   return <h3 className="card-title">({this.state.CurrentDate_Min != '' ? moment(this.state.CurrentDate_Min).format(process.env.DATE_FORMAT) : null} to {this.state.CurrentDate_Max != '' ? moment(this.state.CurrentDate_Max).format(process.env.DATE_FORMAT) : null })</h3>
    // }else if(this.state.role_timesheet_can.timesheet_can_create == false){
    //   return <h3 className="card-title"> <a href="#" className="phss-lock"><i className="fa fa-lock" /></a></h3>
    // }
    // else if(this.state.isCurrent == true && this.state.isTimeSheetSubmitted != true && this.state.role_timesheet_can.timesheet_can_create == true){
    //   return <h3 className="card-title">Current Time Sheet ({this.state.CurrentDate_Min != '' ? moment(this.state.CurrentDate_Min).format(process.env.DATE_FORMAT) : null} to {this.state.CurrentDate_Max != '' ? moment(this.state.CurrentDate_Max).format(process.env.DATE_FORMAT) : null })<a href="#" className="edit-icon" data-toggle="modal" data-target="#CurrentTimeStaff_Add_modal"><i className="fa fa-plus" /></a></h3>
    // }else if(this.state.isCurrent == false && this.state.role_timesheet_can.timesheet_can_create == true){
    //   return <h3 className="card-title">{this.state.CurrentDate_Min != '' ? moment(this.state.CurrentDate_Min).format(process.env.DATE_FORMAT) : null} to {this.state.CurrentDate_Max != '' ? moment(this.state.CurrentDate_Max).format(process.env.DATE_FORMAT) : null }<a href="#" className="edit-icon" data-toggle="modal" data-target="#CurrentTimeStaff_Add_modal"><i className="fa fa-plus" /></a></h3>
    // }else if(this.state.isCurrent == true && this.state.isTimeSheetSubmitted != false && this.state.role_timesheet_can.timesheet_can_create == true){
    //   return <h3 className="card-title">Current Time Sheet ({this.state.CurrentDate_Min != '' ? moment(this.state.CurrentDate_Min).format(process.env.DATE_FORMAT) : null} to {this.state.CurrentDate_Max != '' ? moment(this.state.CurrentDate_Max).format(process.env.DATE_FORMAT) : null })</h3>
    // }

    var isCheckCurrent = '';
    var DateStr = '';
    var PlusBtn = '';
    if(this.state.isCurrent == true){
      isCheckCurrent = 'Current Time Sheet';
      DateStr = "( "+moment(this.state.CurrentDate_Min,'YYYY-MM-DD').add(1, 'day').format(process.env.DATE_FORMAT)+" to "+moment(this.state.CurrentDate_Max,'YYYY-MM-DD').format(process.env.DATE_FORMAT)+" )";
    }else{
      DateStr = moment(this.state.CurrentDate_Min,'YYYY-MM-DD').add(1, 'day').format(process.env.DATE_FORMAT)+" to "+moment(this.state.CurrentDate_Max,'YYYY-MM-DD').format(process.env.DATE_FORMAT);
    }


    let currentDate = moment();
    //let currentDate = moment('2021-08-15');
    let DueDate = moment(this.state.dueDateAmendment);

    var CheckisAmendment = DueDate.diff(currentDate, 'days');
    var display_plus = true;
    if(CheckisAmendment < 0){
      display_plus = false;
    }

    // console.log('***************************************');
    // console.log('CheckisAmendment => '+CheckisAmendment);
    // console.log('display_plus => '+display_plus);
    // console.log('isTimeSheetApproved => '+this.state.isTimeSheetApproved);
    // console.log('isTimeSheetSubmitedToPayroll => '+this.state.isTimeSheetSubmitedToPayroll);


    if( display_plus == true && this.state.isTimeSheetApproved == false &&  this.state.isTimeSheetSubmitedToPayroll == false){
      PlusBtn = <a href="#" className="edit-icon" data-toggle="modal" data-target="#CurrentTimeStaff_Add_modal"><i className="fa fa-plus" /></a>;
    }

    if(this.state.CurrentDate_Min != '' && this.state.CurrentDate_Max != '' && this.state.role_timesheet_can.timesheet_can_create == true && (this.state.isTimeSheetSubmitted == false || this.state.isAmendmendsSubmitted == false)){
      return <h3 className="card-title">{isCheckCurrent} {DateStr}{PlusBtn}</h3>
    }else if(this.state.CurrentDate_Min != '' && this.state.CurrentDate_Max != ''){
      return <h3 className="card-title">{isCheckCurrent} {DateStr}</h3>
    }
       
  }

  past_display_grid(){
    var ListGridPast = this.state.ListGridPast;
    var row_count = ListGridPast.length;
    // console.log('location_wise_display_grid 1');
    // console.log(locationWiseTimeSheeetViews);
    let row = [];
    let grid_row = [];
    for (var zz = 0; zz < row_count; zz++) {
      grid_row.push(
        <tr className="past_grid_background">
        <td>{ListGridPast[zz].periodProfileName}</td>
        <td></td>
        <td>{ListGridPast[zz].totalNoOfHours}</td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        </tr>
      );

      var timeSheetSubmitLocationView = ListGridPast[zz].timeSheetSubmitLocationView;
      var row_count_location = timeSheetSubmitLocationView.length;
      for (var jj = 0; jj < row_count_location; jj++) {
        grid_row.push(
          <tr>
          <td></td>
          <td>{timeSheetSubmitLocationView[jj].locationName}</td>
          <td>{timeSheetSubmitLocationView[jj].totalHours}</td>
          <td>{timeSheetSubmitLocationView[jj].submitedByName}</td>
          <td>{moment(timeSheetSubmitLocationView[jj].submitedDate).format(process.env.DATE_FORMAT)}</td>
          <td>{timeSheetSubmitLocationView[jj].timesheetStatus}</td>
          <td>{timeSheetSubmitLocationView[jj].approvalByName}</td>
          <td>{timeSheetSubmitLocationView[jj].approvalDate != null && timeSheetSubmitLocationView[jj].approvalDate !='' ? moment(timeSheetSubmitLocationView[jj].approvalDate).format(process.env.DATE_FORMAT) : null }</td>
          <td><button type="button" class="btn btn-danger btn-sm mr-1" data-toggle="modal" data-target="#past_details_modal" onClick={this.PastDetails(ListGridPast[zz].periodProfileId,timeSheetSubmitLocationView[jj])}>View</button></td>
          </tr>
        );

      }
    }
    return grid_row;
  }

  SubmitUserAmendmendSubmitTimeSheet = () => e => {
    e.preventDefault();
    this.showLoader();
    let bodyarray = {};
    
    bodyarray["submitByContactId"] = this.state.staffContactID;
    bodyarray["timesheetContactMasterId"] = this.state.timeSheetContactMasterId;
    bodyarray["IsAmendmendSubmit"] = true;
    bodyarray["SubmitByName"] = this.state.fullName;


    var url=process.env.API_API_URL+'SubmitUserTimeSheet';
    fetch(url, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
      body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
      //  console.log("responseJson SubmitUserTimeSheet");
      //  console.log(data);
        //console.log(responseJson);
        // debugger;
        if (data.responseType === "1") {
            //this.props.history.push('/dashboard');

            
            //this.setState({ AllSubCategory : [] });
            //AllSubCategory userSkillCategories

            
            $( ".cancel-btn" ).trigger( "click" );
            SystemHelpers.ToastSuccess(data.responseMessge);
            this.GetUserWiseTimeSheetData();    
            this.GetUserPastTimeSheetData();
        }
        else{
            if(data.message == 'Authorization has been denied for this request.'){
                SystemHelpers.SessionOut();
                this.props.history.push("/login");
              }else{
                //SystemHelpers.ToastError(data.message);
              }
        }
        this.hideLoader();
        
    })
    .catch(error => {
      this.props.history.push("/error-500");
    });
    return false;
  }

  /// Entitlement Summary
  GetUserEntitlementSummary (timeSheetPeriod) {
    
      this.showLoader();
      //alert(timeSheetPeriod);
      var timeSheet_Period = '&periodProfileId='+timeSheetPeriod;

      var url=process.env.API_API_URL+'GetUserEntitlementSummary?contactId='+this.state.staffContactID+timeSheet_Period;
      fetch(url, {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'bearer '+localStorage.getItem("token")
        },
      }).then((response) => response.json())
      .then(data => {
          console.log("responseJson GetUserEntitlementSummary");
          console.log(data);
          //console.log(data.data);
          //console.log((data.data).length);

          if (data.responseType == "1")
          {
            var data_length = (data.data).length;
            
            if(data_length > 0)
            {
              
              var data_res = data.data;
              var i = 1;
              for (var zz = 0; zz < data_length; zz++) 
              {
                
                      if(data_res[zz].entitlementCategoryName == "Sick Leave" && data_res[zz].summary != null)
                      {
                        //console.log(data_res[zz].entitlementCategoryName);
                        //console.log(data_res[zz].summary.totalYearlySickTimeAllowed);
                        this.setState({ EntitleSickTimeTotalYear: data_res[zz].summary.totalYearlySickTimeAllowed });
                        this.setState({ EntitleSickTimeAdditionalEntitled: data_res[zz].summary.additionalSickTimeEntitled });
                        this.setState({ EntitleSickTimeUsedPeriod: data_res[zz].summary.sickTimeUsedInThisPayPeriod });
                        this.setState({ EntitleSickTimeUsedDate: data_res[zz].summary.sickTimeUsedToThisDate });
                        this.setState({ EntitleSickTimeCurrentHrsRemaining: data_res[zz].summary.currentSickTimeHoursRemained });
                        this.setState({ EntitleSickTimeCOVID19RemainingDays: data_res[zz].summary.covid19RemainingDays });                      
                      }
                      
                      if(data_res[zz].entitlementCategoryName == "Vacation" && data_res[zz].summary != null)
                      {
                        //this.setState({ EntitleVacationPreviousHrsRemaining: data_res[zz].summary.previousVacationHoursRemaning });
                        //this.setState({ EntitleVacationHrsEntitledDate: data_res[zz].summary.vacationHoursEntitledToDate });
                        this.setState({ EntitleVacationPreviousHrsRemaining: data_res[zz].summary.vacationHoursEntitledToDate });
                        this.setState({ EntitleVacationHrsEntitledDate: data_res[zz].summary.previousVacationHoursRemaning });
                        this.setState({ EntitleVacationHrsTakenPayPeriod: data_res[zz].summary.vacationHoursTakenToThisPayPeriod });
                        this.setState({ EntitleVacationHrsUsedDate: data_res[zz].summary.vacationHoursUsedToDate });
                        this.setState({ EntitleVacationHrsRemaining: data_res[zz].summary.vacationHoursRemanining });
                      }

                      if(data_res[zz].entitlementCategoryName == "Overtime" && data_res[zz].summary != null)
                      {
                        this.setState({ EntitleOvertimePreviousPayPeriodRemaining: data_res[zz].summary.overTimePreviousPayPeriodRemaining });
                        this.setState({ EntitleOvertimeGainedPayPeriod: data_res[zz].summary.overTimeGainedInThisPayPeriods });
                        this.setState({ EntitleOvertimeTakenPayPeriod: data_res[zz].summary.overTimeTakenInThisPayPeriod });
                        this.setState({ EntitleOvertimeCurrentHrsRemaining: data_res[zz].summary.currentoverTimeRemaining });

                        if(data_res[zz].summary.specialBankTimeBalance && data_res[zz].summary != null){
                          this.setState({ EntitleOvertimeSpecialBankTimeBalance: data_res[zz].summary.specialBankTimeBalance });
                        }
                      }
                i++;
              }
            }  

          }else{
            SystemHelpers.ToastError(data.message); 
          }
          this.hideLoader();
      })
      .catch(error => {
        console.log("catch");
        console.log(error);
        this.props.history.push("/error-500");
      });
  }
  // Entitlement Summary

  render() {    
  const timeConstraints = {
    minutes: {
      step: 5
    }
  }               
  return ( 
    <div className="main-wrapper">
        {/* Toast & Loder method use */}
            
        {(this.state.loading) ? <Loader /> : null} 
        {/* Toast & Loder method use */}
      <Header/>
        <div className="page-wrapper">
            <Helmet>
                <title>{process.env.WEB_TITLE}</title>
                <meta name="description" content="Login page"/>					
            </Helmet>
              {/* Page Content */}
            <div className="content container-fluid">
              {/* Page Header */}

              

              <div className="page-header">
                <div className="row">
                  <div className="col-sm-12">
                    <h3 className="page-title">Time Sheet</h3>
                    <ul className="breadcrumb">
                      <li className="breadcrumb-item"><a href="/dashboard">Time Sheet</a></li>
                      <li className="breadcrumb-item active">Time Sheet</li>
                    </ul>
                  </div>
                </div>
              </div>
              {/* /Page Header */}
              <div className="card tab-box">
                <div className="row user-tabs">
                  <div className="col-lg-12 col-md-12 col-sm-12 line-tabs">
                    <ul className="nav nav-tabs nav-tabs-bottom">
                      <li className="nav-item"><a href="#current_time_staff_tab" data-toggle="tab" className="nav-link active">Current Time Sheets</a></li>
                      <li className="nav-item"><a href="#past_time_staff_tab" data-toggle="tab" className="nav-link">Submitted Time Sheets</a></li>
                      
                    </ul>
                  </div>
                </div>
              </div>
              
              

              <div className="tab-content">
                {/* //============ Current Time Staff Tab ============ */}
                <div id="current_time_staff_tab" className="pro-overview tab-pane fade show active">
                  
                  <div className="row">
                    <div className="col-md-12 d-flex">
                      <div className="card flex-fill">
                        <div className="card mb-0">
                          <div className="card-body">
                            <div className="row">
                              <div className="col-md-12">
                                <div className="profile-view">
                                  <div className="profile-img-wrap">
                                    <div className="profile-img">
                                      <a href="#"><img alt="" src={ProfileImage} /></a>
                                    </div>
                                  </div>
                                  <div className="profile-basic">
                                    <div className="row">
                                      <div className="col-lg-5">
                                        <div className="profile-info-left">
                                          <h3 className="user-name m-t-0 mb-0">{this.state.Preferred_Name == '' || this.state.Preferred_Name == null? <div className="text hide-font">None</div> : <div className="text">{this.state.Preferred_Name}</div> }</h3>
                                          <h6 className="text-muted"></h6>
                                          <small className="text-muted"></small>
                                          <div className="staff-id">Employee ID : {this.state.staffNumberId}</div>
                                          <div className="staff-id">Phone : {this.state.cellPhone}</div>
                                          <div className="staff-id">Email : {this.state.phssEmail}</div>
                                          <div className="staff-id">Primary location : {this.state.primarylocation}</div>
                                          {/*<div className="staff-id">Reports to : <a href="#">{this.state.reportToUserName}</a></div>*/}
                                        </div>
                                      </div>
                                      <div className="col-lg-7">
                                        <ul className="personal-info">
                                          {/*<li>
                                            <div className="title">Scheduled hours:</div>
                                            <div className="text">00h 00 min</div>
                                          </li> */}
                                          <li>
                                            <div className="title">Worked Hours:</div>
                                            {this.state.workedHour == '' || this.state.workedHour == null? <div className="text hide-font">None</div> : <div className="text">{this.state.workedHour}</div> }
                                          </li>
                                          <li>
                                            <div className="title">Period :</div>
                                            <div className="text">{this.state.CurrentDate_Min != '' ? moment(this.state.CurrentDate_Min).add(1, 'day').format(process.env.DATE_FORMAT) : null } to { this.state.CurrentDate_Max != '' ? moment(this.state.CurrentDate_Max).format(process.env.DATE_FORMAT) : null}</div>
                                          </li>
                                          <li>
                                            <div className="title">Due Date (Submission) :</div>
                                            {this.state.DueDate == '' || this.state.DueDate == null? <div className="text hide-font">None</div> : <div className="text">{moment(this.state.DueDate,process.env.API_DATE_FORMAT).format(process.env.DATE_FORMAT)}</div> }
                                          </li>

                                          <li>
                                            <div className="title">Due Date (Amendment) :</div>
                                            {this.state.dueDateAmendment == '' || this.state.dueDateAmendment == null? <div className="text hide-font">None</div> : <div className="text">{moment(this.state.dueDateAmendment,'MM-DD-YYYY HH:mm').format(process.env.DATETIME_FORMAT)}</div> }
                                          </li>

                                          <li>
                                            <div className="title">Reports to:</div>
                                            <div className="text">
                                              <div className="avatar-box">
                                                <div className="avatar avatar-xs">
                                                  <img src={Reportto} alt="" />
                                                </div>
                                              </div>
                                              <a href="#" className="capitalize-title">
                                                {this.state.reportToUserName}
                                              </a>
                                            </div>
                                          </li>
                                          <li>
                                            <div className="title">Timesheet Approver:</div>
                                            {this.state.TimeSheetApproverName == '' || this.state.TimeSheetApproverName == null? <div className="text hide-font">None</div> : <div className="text">{this.state.TimeSheetApproverName}</div> }
                                          </li>

                                        </ul>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  
                  {/* Search Filter */}
                  { this.state.timeSheetPeriodViews.length > 0 ?

                    <div className="row filter-row">

                      <div className="col-lg-6 col-sm-12 col-xs-12">  
                        <div className="form-group row">
                          <label className="col-md-5 col-form-label">Time Sheet Period Selection :</label>
                          <div className="col-md-7"> 
                            
                            <select className="select floating" id="timeSheetPeriod" value={this.state.timeSheetPeriod} onChange={this.handleChange('timeSheetPeriod')}>
                              {this.state.timeSheetPeriodViews.map(( listValue, index ) => {
                                return (
                                  <option key={index} value={listValue.timeSheetPeriodId}>{moment(listValue.timeSheetPeriodStartDate,process.env.API_DATE_FORMAT).format(process.env.DATE_FORMAT)} to {moment(listValue.timeSheetPeriodEndDate,process.env.API_DATE_FORMAT).format(process.env.DATE_FORMAT)}</option>
                                );
                              })}
                            </select>
                          </div>
                        </div>
                      </div>
                      <div className="col-lg-6 col-sm-12 col-xs-12 margin-bottom">  
                        <a href="#" className="col-lg-6 col-sm-12 col-xs-12 float-right  btn btn-success btn-block" id="ChangePeriod" onClick={this.ChangePeriod}> Change Time Sheet Period </a>  
                      </div>     
                    </div>
                  :null
                  }
                  {/* /Search Filter */}    

                  {/* Entitlement Summary */}  
                  <div className="row">
                    <div className="col-md-12 d-flex">
                      <div className="card flex-fill">
                        <div className="card-body">
                          <div className="faq-card">
                            <div className="card">

                              <div className="card-header">
                                <h4 className="card-title">
                                  <a className="collapsed" data-toggle="collapse" href="#entitlementSummary">Entitlement Summary</a>
                                </h4>
                              </div>

                              <div id="entitlementSummary" className="card-collapse collapse">
                                <div className="card-body">

                                  <div className="row">

                                    <div className="col-lg-4">
                                      <div className="form-group">
                                        
                                          <table className="table table-bordered table-striped custom-table mb-0 datatable pk-timesheetEntitlement-table">
                                            <tbody>
                                              <tr>
                                                <td className="pk-timesheetEntitlement-table-tr-td" colSpan="2">Sick Time Taken/Remaining</td>
                                              </tr>
                                              <tr>
                                                <td>Total yearly sick time allotted </td>
                                                <td>{this.state.EntitleSickTimeTotalYear}</td>
                                              </tr>
                                              <tr>
                                                <td>Additional Sick time Entitled </td>
                                                <td>{this.state.EntitleSickTimeAdditionalEntitled}</td>
                                              </tr>  
                                              <tr>
                                                <td>Sick time used this period </td>
                                                <td>{this.state.EntitleSickTimeUsedPeriod}</td>
                                              </tr>
                                              <tr>
                                                <td>Sick Time Used to date </td>
                                                <td>{this.state.EntitleSickTimeUsedDate}</td>
                                              </tr>
                                              <tr>
                                                <td>Current Sick Hours Remaining </td>
                                                <td>{this.state.EntitleSickTimeCurrentHrsRemaining}</td>
                                              </tr>
                                              <tr>
                                                <td>COVID-19 remaining (days) </td>
                                                <td>{this.state.EntitleSickTimeCOVID19RemainingDays}</td>
                                              </tr>
                                            </tbody>
                                          </table> 
                                        
                                      </div>
                                    </div>

                                    {this.state.UserHoursType == "Full-Time" ?
                                      <div className="col-lg-4">
                                        <div className="form-group">
                                          
                                            <table className="table table-bordered table-striped custom-table mb-0 datatable pk-timesheetEntitlement-table">
                                              <tbody>
                                                <tr>
                                                  <td className="pk-timesheetEntitlement-table-tr-td" colSpan="2">Vacation Time Taken/Remaining </td>
                                                </tr>
                                                <tr>
                                                  <td>Previous Vacation Hours remaining </td>
                                                  <td>{this.state.EntitleVacationPreviousHrsRemaining}</td>
                                                </tr>
                                                <tr>
                                                  <td>Vacations Hours Entitled To Date </td>
                                                  <td>{this.state.EntitleVacationHrsEntitledDate}</td>
                                                </tr>  
                                                <tr>
                                                  <td>Vacation Hours Taken This Pay Period </td>
                                                  <td>{this.state.EntitleVacationHrsTakenPayPeriod}</td>
                                                </tr>
                                                <tr>
                                                  <td>Vacation Hours Used To Date </td>
                                                  <td>{this.state.EntitleVacationHrsUsedDate}</td>
                                                </tr>
                                                <tr>
                                                  <td>Vacation Hours Remaining </td>
                                                  <td>{this.state.EntitleVacationHrsRemaining}</td>
                                                </tr>
                                              </tbody>
                                            </table>
                                          
                                        </div>
                                      </div>
                                    : null
                                    }
                                      
                                    {this.state.UserHoursType == "Full-Time" ?
                                      <div className="col-lg-4">
                                        <div className="form-group">
                                          
                                            <table className="table table-bordered table-striped custom-table mb-0 datatable pk-timesheetEntitlement-table">
                                              <tbody>
                                                <tr>
                                                  <td className="pk-timesheetEntitlement-table-tr-td" colSpan="2">Bank Time Balance </td>
                                                </tr>
                                                <tr>
                                                  <td>Bank Time Previous pay period remaining </td>
                                                  <td>{this.state.EntitleOvertimePreviousPayPeriodRemaining}</td>
                                                </tr>
                                                <tr>
                                                  <td>Bank Time gained in this pay period </td>
                                                  <td>{this.state.EntitleOvertimeGainedPayPeriod}</td>
                                                </tr>  
                                                <tr>
                                                  <td>Bank Time Taken in this pay period </td>
                                                  <td>{this.state.EntitleOvertimeTakenPayPeriod}</td>
                                                </tr>
                                                <tr>
                                                  <td>Current Bank Time hours remaining </td>
                                                  <td>{this.state.EntitleOvertimeCurrentHrsRemaining}</td>
                                                </tr>
                                                <tr>
                                                  <td>Miscellaneous Bank Time balance</td>
                                                  <td>{this.state.EntitleOvertimeSpecialBankTimeBalance}</td>
                                                </tr>
                                              </tbody>
                                            </table> 
                                          
                                        </div>
                                      </div>
                                    : null
                                    }

                                  </div>

                                </div>
                              </div>

                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  {/* Entitlement Summary */}  

                  <div className="row">
                    <div className="col-md-12 d-flex">
                      <div className="card profile-box flex-fill">
                        <div className="card-body">
                          {this.HeaderTitleFunc()}
                          
                          <div className="table-responsive">
                            <table className="table table-nowrap">
                              <thead>
                               <tr>
                                  <th>Date</th>
                                  <th>Service Name</th>
                                  <th>Time In</th>
                                  <th>Time Out</th>
                                  <th>Location</th>
                                  <th>Note</th>
                                  <th>No of Hours</th>
                                  <th>Record Type</th>
                                  <th>CreatedOn</th>
                                  {this.state.isAmendmendsSubmitted == false ? <th>Action</th> : null }
                                </tr>
                              </thead>
                              <tbody>
                                {this.state.ListGrid.map(( listValue, index ) => {
                                    if(listValue.isAmendmends == true && listValue.wantToDisplayInTimeSheet == true){
                                      var action_btn = '';
                                      var cls_nm = 'pk-strikethrough';
                                      

                                      return (
                                        <tr key={index}>
                                          <td className={cls_nm}>{moment(listValue.timeSheetDate,process.env.API_DATE_FORMAT).format(process.env.DATE_FORMAT)}</td>
                                          <td className={cls_nm}>{listValue.serviceName}</td>
                                          <td className={cls_nm}>{moment(listValue.timeIN).format(process.env.DATETIME_FORMAT)}</td>
                                          <td className={cls_nm}>{moment(listValue.timeOUT).format(process.env.DATETIME_FORMAT)}</td>
                                          <td className={cls_nm}>{listValue.locationName}</td>
                                          <td className={cls_nm}>{listValue.note}</td>
                                          <td className={cls_nm}>{listValue.noOfHours}</td>
                                          <td className={cls_nm}>{listValue.recordType}</td>
                                          <td className={cls_nm}>{SystemHelpers.TimeZone_DateTime(listValue.createdOn)}</td>
                                          <td></td>
                                        </tr>
                                      );
                                    }else{
                                      var action_btn = '';
                                      if(this.state.isAmendmendsSubmitted == false && this.state.isTimeSheetApproved == false &&  this.state.isTimeSheetSubmitedToPayroll == false){
                                        action_btn = this.Edit_Update_Btn_Func(listValue);
                                      }
                                      if(listValue.wantToDisplayInTimeSheet == true){
                                        return (
                                          <tr key={index}>
                                            <td>{moment(listValue.timeSheetDate,process.env.API_DATE_FORMAT).format(process.env.DATE_FORMAT)}</td>
                                            <td>{listValue.serviceName}</td>
                                            <td>{moment(listValue.timeIN).format(process.env.DATETIME_FORMAT)}</td>
                                            <td>{moment(listValue.timeOUT).format(process.env.DATETIME_FORMAT)}</td>
                                            <td>{listValue.locationName}</td>
                                            <td>{listValue.note}</td>
                                            <td>{listValue.noOfHours}</td>
                                            <td>{listValue.recordType}</td>
                                            <td>{SystemHelpers.TimeZone_DateTime(listValue.createdOn)}</td>
                                            <td>{action_btn}</td>
                                          </tr>
                                        );
                                      }
                                    }
                                    
                                })}
                                
                              </tbody>
                            </table>
                          </div>
                          { this.state.ListGrid.length > 0 && this.state.isCurrent == true && this.state.isTimeSheetSubmitted == false && this.state.isTimeSheetApproved == false &&  this.state.isTimeSheetSubmitedToPayroll == false ?
                          <div className="row justify-content">
                            <button className="btn btn-success mr-1"  data-toggle="modal" data-target="#submit_for_approval">Submit for approval</button>
                          </div>
                          : null
                          }

                          { this.state.IsAmendmendsEnabled == true  && this.state.isTimeSheetSubmitted == true && this.state.isTimeSheetApproved == false &&  this.state.isTimeSheetSubmitedToPayroll == false &&  this.state.isamendmentFlag == true ?
                            <div className="row justify-content">
                              <button className="btn btn-success mr-1"  data-toggle="modal" data-target="#submit_for_amendmend">SUBMIT FOR AMENDMENT</button>
                            </div>
                            : null
                          }

                          
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                {/* //============ Current Time Staff Tab ============ */}

                {/* //============ Past Time Staff Tab ============ */}
                <div id="past_time_staff_tab" className="tab-pane fade">
                  <div className="row">
                    <div className="col-md-12 d-flex">
                      <div className="card profile-box flex-fill">
                        <div className="card-body">
                          <h3 className="card-title">Submitted Time Sheets</h3>
                          <div className="table-responsive">
                            <table className="table table-nowrap">
                              <thead>
                                <tr>
                                  <th>Pay Period</th>
                                  <th>Location</th>
                                  <th>Total hours</th>
                                  <th>Submitted By</th>
                                  <th>Submitted Date</th>
                                  <th>Status</th>
                                  <th>Approval By</th>
                                  <th>Approved Date</th>
                                  <th>Action</th>
                                  <th />
                                </tr>
                              </thead>
                              <tbody>
                                {this.past_display_grid()}
                                {/*{this.state.ListGridPast.map(( listValue, index ) => {
                                    return (
                                      <tr key={index}>
                                        <td>{listValue.timeSheetStartDate} to {listValue.timeSheetEndDate} </td>
                                        <td>{listValue.totalHours}</td>
                                        <td>{listValue.approveByName}</td>
                                        <td>{listValue.submitByName}</td>
                                        <td>{listValue.submitByDate}</td>
                                        <td>{this.CheckStatus(listValue.isTimeSheetSubmitted,listValue.isTimeSheetApproved,listValue.isTimeSheetSubmitedToPayroll)}</td>
                                        <td>{listValue.approvedDate}</td>
                                        <td><button type="button" class="btn btn-danger btn-sm mr-1" data-toggle="modal" data-target="#past_details_modal" onClick={this.PastDetails(listValue.timeSheetPeriodId)}>View</button></td>
                                      </tr>
                                    );
                                })}*/}
                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                {/* //============ Past Time Staff Tab ============ */}
              </div>
            </div>
            {/* //Page Content */}
             
               {/* ------------------------- Modes ------------------------- */}

            {/* ****************** Current Time Staff Tab Modals ****************** */}
            {/* Current Time Staff Modal */}
            <div id="CurrentTimeStaff_Add_modal" className="modal custom-modal fade" role="dialog">
              <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div className="modal-content">
                  <div className="modal-header">
                    <h5 className="modal-title">Time Sheet Entry</h5>
                    <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={this.ClearRecord()}>
                      <span aria-hidden="true">×</span>
                    </button>
                  </div>
                  <div className="modal-body">
                   
                      <div className="card">
                        <div className="card-body">
                          <div className="row">
                        {/*    <div className="col-md-4">
                              <div className="form-group">
                                <label>Date <span className="text-danger">*</span></label>
                                <input className="form-control" type="date" min={this.state.AddDate_Min} max={this.state.AddDate_Max} value={this.state.AddDate} onChange={this.handleChange('AddDate')} readonly/>
                                <span className="form-text error-font-color">{this.state.errormsg["AddDate"]}</span>
                              </div>
                            </div> */}

                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Service Name <span className="text-danger">*</span></label>
                                <select className="form-control" id="AddService" value={this.state.AddService} onChange={this.handleChange('AddService')}>
                                  <option value="">-</option>
                                  {this.state.serviceView.map(( listValue, index ) => { 
                                    if((this.state.isCordinatorSession == true || this.state.isSeniorCordinatorSession) && listValue.serviceId == process.env.API_ON_CALL ){
                                      return (
                                        <option key={index} data-basserivce={listValue.isBasedOnSerivceBase} value={listValue.serviceId}>{listValue.serviceName}</option>
                                      );
                                    }else if(listValue.serviceId != process.env.API_ON_CALL){
                                      return (
                                        <option key={index} data-basserivce={listValue.isBasedOnSerivceBase} value={listValue.serviceId}>{listValue.serviceName}</option>
                                      );
                                    }
                                    
                                  })}
                                </select>
                                <span className="form-text error-font-color">{this.state.errormsg["AddService"]}</span>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Location <span className="text-danger">*</span></label>
                                <select className="form-control" value={this.state.Addlocation} onChange={this.handleChange('Addlocation')}>
                                  <option value="">-</option>
                                  {this.state.locationViews.map(( listValue, index ) => {
                                    return (
                                      <option key={index} value={listValue.locationId}>{listValue.locationName}</option>
                                    );
                                  })}
                                </select>
                                <span className="form-text error-font-color">{this.state.errormsg["Addlocation"]}</span>
                              </div>
                            </div>

                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Time in <span className="text-danger">*</span></label>
                                <div className="row">
                                  {/*<div className="col-md-6">
                                    <input className="form-control" type="datetime-local" min={this.state.AddTimeIn_Min} max={this.state.AddTimeIn_Max} value={this.state.AddTimeIn} onChange={this.handleChange('AddTimeIn')}  /> 
                                  </div> */ }
                                  <div className="col-md-12">
                                    <Datetime
                                      
                                      isValidDate={this.validationDateAddIn}
                                      inputProps={{readOnly: true}}
                                      closeOnTab={true}
                                      input={true}
                                      value={(this.state.AddTimeIn) ? this.state.AddTimeIn : ''}
                                      onChange={this.handleDateAddIn}
                                      dateFormat={process.env.DATE_FORMAT}
                                      timeFormat={process.env.TIME_FORMAT}
                                      timeConstraints={{
                                        hours: { min: 0, max: 23 },
                                        minutes: { min: 0, max: 59, step: 15 }
                                      }}
                                      renderInput={(props) => {
                                         return <input {...props} value={(this.state.AddTimeIn) ? props.value : ''} />
                                      }}
                                    />
                                  </div>
                                </div>
                                <span className="form-text error-font-color">{this.state.errormsg["AddTimeIn"]}</span>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Time out <span className="text-danger">*</span></label>
                                {/* <input className="form-control" type="datetime-local" step="900" id="AddTimeOut" min={this.state.AddTimeOut_Min} max={this.state.AddTimeOut_Max} value={this.state.AddTimeOut} onChange={this.handleChange('AddTimeOut')} /> */}
                                <div className="row">
                                  <div className="col-md-12">
                                    
                                      <Datetime
                                        inputProps={{readOnly: true,disabled: this.state.AddTimeOutDisabled}}
                                        className="readonly-cls AddTimeOut"
                                        isValidDate={this.validationDateAddOut}
                                        onClose={this.AddTimeOutonClose}
                                        closeOnTab={true}
                                        input={true}
                                        value={(this.state.AddTimeOut) ? this.state.AddTimeOut : ''} 
                                        defaultValue={this.state.AddTimeOut}
                                        onChange={this.handleDateAddOut}
                                        dateFormat={process.env.DATE_FORMAT}
                                        timeFormat={process.env.TIME_FORMAT}
                                        timeConstraints={{
                                          hours: { min: 0, max: 23 },
                                          minutes: { min: 0, max: 59, step: 15 }
                                        }}
                                        renderInput={(props) => {
                                             return <input {...props} value={(this.state.AddTimeOut) ? props.value : ''} />
                                         }}
                                      /> 
                                  </div>
                                </div>
                                <span className="form-text error-font-color">{this.state.errormsg["AddTimeOut"]}</span>
                              </div>
                            </div>
                           

                            
                            
                            <div className="col-md-6">
                              { this.state.AddService != process.env.API_ON_CALL ?
                                <div className="form-group">
                                  <label># of hrs</label>
                                  <input className="form-control" type="text"  value={this.state.AddNoOfHours} onChange={this.handleChange('AddNoOfHours')} disabled/>
                                  <span className="form-text error-font-color">{this.state.errormsg["AddNoOfHours"]}</span>
                                </div>
                                : null
                              }
                              
                            </div>

                            { this.state.AddPreference.length > 0 ?
                              <div className="col-md-6">
                                <div className="form-group">
                                  <label>Payout Preference <span className="text-danger">*</span></label>
                                  <select className="form-control" id="AddPreferenceService" value={this.state.AddPreferenceService} onChange={this.handleChange('AddPreferenceService')}>
                                    <option value="">-</option>
                                    {this.state.AddPreference.map(( listValue, index ) => {
                                      return (
                                        <option key={index} value={listValue.guidId}>{listValue.name}</option>
                                      );
                                    })}
                                  </select>
                                  <span className="form-text error-font-color">{this.state.errormsg["AddPreferenceService"]}</span>
                                </div>
                              </div>
                              : null
                            }
                            
                            
                            <div className="col-md-12">
                              <div className="form-group">
                                <label>Notes</label>
                                <textarea className="form-control" type="text" value={this.state.AddNotes} onChange={this.handleChange('AddNotes')} ></textarea>
                                <span className="form-text error-font-color">{this.state.errormsg["AddNotes"]}</span>
                              </div>
                            </div>
                            
                          </div>
                          
                          
                          <div className="submit-section">
                            <button className="btn btn-primary submit-btn" onClick={this.AddRecord()}>Save</button>
                          </div>
                        </div>
                      </div>
                    
                  </div>
                </div>
              </div>
            </div>
            {/* //Current Time Staff Modal */}

            {/* Current Time Staff Modal */}
            <div id="CurrentTimeStaff_Edit_modal" className="modal custom-modal fade" role="dialog">
              <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div className="modal-content">
                  <div className="modal-header">
                    <h5 className="modal-title">Time Sheet Entry</h5>
                    <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">×</span>
                    </button>
                  </div>
                  <div className="modal-body">
                   
                      <div className="card">
                        <div className="card-body">
                          <div className="row">
                            {/*
                              <div className="col-md-6">
                              <div className="form-group">
                                <label>Date</label>
                                <input className="form-control" type="text"  value={this.state.EditDate} onChange={this.handleChange('EditDate')} disabled/>
                              </div>
                            </div>
                            */}

                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Service Name <span className="text-danger">*</span></label>
                                <select className="form-control" id="EditService" value={this.state.EditService} onChange={this.handleChange('EditService')}>
                                  <option value="">-</option>
                                  {this.state.serviceView.map(( listValue, index ) => {
                                    if(this.state.EditService == process.env.API_ON_CALL || listValue.serviceId != process.env.API_ON_CALL){
                                      return (
                                      <option key={index} data-basserivce={listValue.isBasedOnSerivceBase} value={listValue.serviceId}>{listValue.serviceName}</option>
                                    );
                                    }
                                  })}
                                </select>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Location <span className="text-danger">*</span></label>
                                <select className="form-control" id="Editlocation" value={this.state.Editlocation} onChange={this.handleChange('Editlocation')}>
                                  <option value="">-</option>
                                  {this.state.locationViews.map(( listValue, index ) => {
                                    return (
                                      <option key={index} value={listValue.locationId}>{listValue.locationName}</option>
                                    );
                                  })}
                                </select>
                              </div>
                            </div>
                            
                            
                            
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Time in <span className="text-danger">*</span></label>
                                {/*<input className="form-control" type="time"  value={this.state.EditTimeIn} onChange={this.handleChange('EditTimeIn')} /> */}
                                <div className="row">
                                  <div className="col-md-12">
                                      <Datetime
                                        inputProps={{readOnly: true,disabled: this.state.EditTimeInDisabled}}
                                        isValidDate={this.validationDateEditIn}
                                        closeOnTab={true}
                                        input={true}
                                        value={(this.state.EditTimeIn) ? this.state.EditTimeIn : ''}
                                        onChange={this.handleDateEditIn}
                                        dateFormat={process.env.DATE_FORMAT}
                                        timeFormat={process.env.TIME_FORMAT}
                                        timeConstraints={{
                                          hours: { min: 0, max: 23 },
                                          minutes: { min: 0, max: 59, step: 15 }
                                        }}
                                        renderInput={(props) => {
                                             return <input {...props} value={(this.state.EditTimeIn) ? props.value : ''} />
                                        }}
                                      />
                                  </div>
                                </div>
                                <span className="form-text error-font-color">{this.state.errormsg["EditTimeIn"]}</span>
                              </div>
                            </div>
                            {/*<div className="col-md-6">
                              <div className="form-group">
                                <label>Time out <span className="text-danger">*</span></label>
                                <input className="form-control" id="EditTimeOut" type="datetime-local" min={this.state.EditTimeOut_Min} max={this.state.EditTimeOut_Max}   value={this.state.EditTimeOut} onChange={this.handleChange('EditTimeOut')} />
                                <span className="form-text error-font-color">{this.state.errormsg["EditTimeOut"]}</span>
                              </div>
                            </div> */}

                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Time out <span className="text-danger">*</span></label>
                                {/* <input className="form-control" type="datetime-local" step="900" id="AddTimeOut" min={this.state.AddTimeOut_Min} max={this.state.AddTimeOut_Max} value={this.state.AddTimeOut} onChange={this.handleChange('AddTimeOut')} /> */}
                                <div className="row">
                                  <div className="col-md-12">
                                      <Datetime
                                        inputProps={{readOnly: true,disabled: this.state.EditTimeOutDisabled}}
                                        isValidDate={this.validationDateEditOut}
                                        onClose={this.EditTimeOutonClose}
                                        closeOnTab={true}
                                        input={true}
                                        value={(this.state.EditTimeOut) ? this.state.EditTimeOut : ''}
                                        onChange={this.handleDateEditOut}
                                        dateFormat={process.env.DATE_FORMAT}
                                        timeFormat={process.env.TIME_FORMAT}
                                        timeConstraints={{
                                          hours: { min: 0, max: 23 },
                                          minutes: { min: 0, max: 59, step: 15 }
                                        }}
                                        renderInput={(props) => {
                                             return <input {...props} value={(this.state.EditTimeOut) ? props.value : ''} />
                                        }}
                                      />
                                  </div>
                                </div>
                                <span className="form-text error-font-color">{this.state.errormsg["EditTimeOut"]}</span>
                              </div>
                            </div>
                            


                            <div className="col-md-6">
                              { this.state.EditService != process.env.API_ON_CALL ?
                                <div className="form-group">
                                  <label>No of hours</label>
                                  <input className="form-control" type="text"  value={this.state.EditNoOfHours} onChange={this.handleChange('EditNoOfHours')} disabled/>
                                  <span className="form-text error-font-color">{this.state.errormsg["EditNoOfHours"]}</span>
                                </div>
                                : null
                              }
                            </div>

                            { this.state.EditPreference.length > 0 ?
                              <div className="col-md-6">
                                <div className="form-group">
                                  <label>Payout Preference <span className="text-danger">*</span></label>
                                  <select className="form-control" id="EditPreferenceService" value={this.state.EditPreferenceService} onChange={this.handleChange('EditPreferenceService')}>
                                    <option value="">-</option>
                                    {this.state.EditPreference.map(( listValue, index ) => {
                                      return (
                                        <option key={index} value={listValue.guidId}>{listValue.name}</option>
                                      );
                                    })}
                                  </select>
                                  <span className="form-text error-font-color">{this.state.errormsg["EditPreferenceService"]}</span>
                                </div>
                              </div>
                              : null
                            }
                            
                            <div className="col-md-12">
                              <div className="form-group">
                                <label>Notes</label>
                                <textarea className="form-control" type="text" id="EditNotes" value={this.state.EditNotes} onChange={this.handleChange('EditNotes')} ></textarea>
                              </div>
                            </div>
                            
                          </div>
                            
                          
  
                          <div className="submit-section">
                            <button className="btn btn-primary submit-btn" onClick={this.UpdateRecord()}>Save</button>
                          </div>
                        </div>
                      </div>
                    
                  </div>
                </div>
              </div>
            </div>
            {/* //Current Time Staff Modal */}
            {/* ****************** Current Time Staff Tab Modals ****************** */}

            {/* Current Time Staff Modal */}
            <div id="past_details_modal" className="modal custom-modal fade" role="dialog">
              <div className="modal-dialog modal-dialog-centered modal-xl" role="document">
                <div className="modal-content">
                  <div className="modal-header">
                    <h5 className="modal-title">Time Entries for {this.state.PasttimeSheetStartDate != '' ? moment(this.state.PasttimeSheetStartDate).format(process.env.DATE_FORMAT) : null} to {this.state.PasttimeSheetEndDate != '' ? moment(this.state.PasttimeSheetEndDate).format(process.env.DATE_FORMAT) : null } </h5>
                    <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">×</span>
                    </button>
                  </div>
                  <div className="modal-body">
                   
                      <div className="card">
                        <div className="card-body">
                          <div className="row">
                            <div className="col-lg-12">
                              <div className="table-responsive">
                                <table className="table table-nowrap">
                                  <thead>
                                   <tr>
                                      <th>Date</th>
                                      <th>Service Name</th>
                                      <th>Time In</th>
                                      <th>Time Out</th>
                                      <th>Location</th>
                                      <th>Note</th>
                                      <th>No of Hours</th>
                                      <th>Record Type</th>
                                      <th>CreatedOn</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    {this.state.ListGridPastDetails.map(( listValue, index ) => {
                                        if(listValue.isAmendmends == true && listValue.wantToDisplayInTimeSheet == true){
                                          
                                          var cls_nm = 'pk-strikethrough';
                                          

                                          return (
                                            <tr key={index}>
                                              <td className={cls_nm}>{moment(listValue.timeSheetDate).format(process.env.DATE_FORMAT)}</td>
                                              <td className={cls_nm}>{listValue.serviceName}</td>
                                              <td className={cls_nm}>{moment(listValue.timeIN).format(process.env.DATETIME_FORMAT)}</td>
                                              <td className={cls_nm}>{moment(listValue.timeOUT).format(process.env.DATETIME_FORMAT)}</td>
                                              <td className={cls_nm}>{listValue.locationName}</td>
                                              <td className={cls_nm}>{listValue.note}</td>
                                              <td className={cls_nm}>{listValue.noOfHours}</td>
                                              <td className={cls_nm}>{listValue.recordType}</td>
                                              <td className={cls_nm}>{SystemHelpers.TimeZone_DateTime(listValue.createdOn)}</td>
                                             
                                            </tr>
                                          );
                                        }else{
                                          if(listValue.wantToDisplayInTimeSheet == true){
                                            return (
                                              <tr key={index}>
                                                <td>{moment(listValue.timeSheetDate).format(process.env.DATE_FORMAT)}</td>
                                                <td>{listValue.serviceName}</td>
                                                <td>{moment(listValue.timeIN).format(process.env.DATETIME_FORMAT)}</td>
                                                <td>{moment(listValue.timeOUT).format(process.env.DATETIME_FORMAT)}</td>
                                                <td>{listValue.locationName}</td>
                                                <td>{listValue.note}</td>
                                                <td>{listValue.noOfHours}</td>
                                                <td>{listValue.recordType}</td>
                                                <td>{SystemHelpers.TimeZone_DateTime(listValue.createdOn)}</td>
                                                
                                              </tr>
                                            );
                                          }
                                        }
                                    })}
                                    
                                  </tbody>
                                </table>
                              </div>
                            </div>
                          </div>

                          <br/>
                          <br/>
                          {this.state.EdittimeSheetContactMasterId != '' && this.state.ListNotes.length > 0 ?
                          <div className="row">
                             <div className="col-lg-12">
                             <h3 class="card-title">Notes</h3>
                              {/* Chat Main Row */}
                              <div className="chat-main-row">
                                {/* Chat Main Wrapper */}
                                <div className="chat-main-wrapper">
                                  {/* Chats View */}
                                  <div className="col-lg-9 message-view task-view">
                                    <div className="chat-window">
                                      
                                      <div className="chat-contents">
                                        <div className="chat-content-wrap">
                                          <div className="chat-wrap-inner">
                                            <div className="chat-box">
                                              {this.GetChatList()}
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      
                                    </div>
                                  </div>
                                  {/* /Chats View */}

                                  
                                </div>
                                {/* /Chat Main Wrapper */}
                              </div>
                              {/* /Chat Main Row */}
                              
                             </div>
                          </div>
                          : null }

                        </div>
                      </div>
                    
                  </div>
                </div>
              </div>
            </div>
            {/* //Current Time Staff Modal */}

            {/* Delete timesheet  Modal */}
            <div className="modal custom-modal fade" id="timesheet_Delete_modal" role="dialog">
              <div className="modal-dialog modal-dialog-centered">
                <div className="modal-content">
                  <div className="modal-body">
                    <div className="form-header">
                      <h3>Time Sheet</h3>
                      <p>Are you sure want to delete selected record?</p>
                    </div>
                    <div className="modal-btn delete-action">
                      <div className="row">
                        <div className="col-6">
                          <a  onClick={this.DeleteRecord()} className="btn btn-primary continue-btn">Delete</a>
                        </div>
                        <div className="col-6">
                          <a href="" data-dismiss="modal" className="btn btn-primary cancel-btn">Cancel</a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          {/* /Delete Human Resources Modal */}

          {/* submit For Approval timesheet  Modal */}
            <div className="modal custom-modal fade" id="submit_for_approval" role="dialog">
              <div className="modal-dialog modal-dialog-centered">
                <div className="modal-content">
                  <div className="modal-body">
                    <div className="form-header">
                      <h3>Time Sheet</h3>
                      <p>Are you sure you want to submit time Sheet for approval, after submission time sheet would be read only</p>
                    </div>
                    <div className="modal-btn delete-action">
                      <div className="row">
                        <div className="col-6">
                          <a  onClick={this.SubmitUserTimeSheet()} className="btn btn-primary continue-btn">Yes</a>
                        </div>
                        <div className="col-6">
                          <a href="" data-dismiss="modal" className="btn btn-primary cancel-btn">No</a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          {/* submit For Approval timesheet  Modal */}


          {/* submit For Approval timesheet  Modal */}
            <div className="modal custom-modal fade" id="submit_for_amendmend" role="dialog">
              <div className="modal-dialog modal-dialog-centered">
                <div className="modal-content">
                  <div className="modal-body">
                    <div className="form-header">
                      <h3>Time Sheet</h3>
                      <p>Are you sure you want to submit time Sheet for approval, after submission time sheet would be read only</p>
                    </div>
                    <div className="modal-btn delete-action">
                      <div className="row">
                        <div className="col-6">
                          <a  onClick={this.SubmitUserAmendmendSubmitTimeSheet()} className="btn btn-primary continue-btn">Yes</a>
                        </div>
                        <div className="col-6">
                          <a href="" data-dismiss="modal" className="btn btn-primary cancel-btn">No</a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          {/* submit For Approval timesheet  Modal */}
         
            {/* ------------------------- Modes ------------------------- */}
            </div>
          <SidebarContent/>
      </div>
    );
  }
}

export default Timesheet;
