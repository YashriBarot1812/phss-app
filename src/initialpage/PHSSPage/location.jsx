
import React, { Component } from 'react';
import { Helmet } from "react-helmet";
import {  Avatar_01 ,Avatar_02,Avatar_03,Avatar_04, Avatar_05, Avatar_08, Avatar_09, Avatar_10,
    Avatar_11,Avatar_12,Avatar_13,Avatar_16   } from "../../Entryfile/imagepath"

import { Table } from 'antd';
import 'antd/dist/antd.css';
import {itemRender,onShowSizeChange} from "./pagination/paginationfunction"
import "../MainPage/antdstyle.css"
import { Multiselect } from 'multiselect-react-dropdown';

import Select from 'react-select-me';
import 'react-select-me/lib/ReactSelectMe.css';

import Header from '../Sidebar/header.jsx';
import SidebarContent from '../Sidebar/sidebar';


import Loader from '../Loader';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import SystemHelpers from '../Helpers/SystemHelper';

//table
import { MDBDataTable } from 'mdbreact';
import '@fortawesome/fontawesome-free/css/all.min.css';
import 'bootstrap-css-only/css/bootstrap.min.css';
import 'mdbreact/dist/css/mdb.css';
//table

import CryptoAES from 'crypto-js/aes';
import CryptoENC from 'crypto-js/enc-utf8';

import PhoneInput from 'react-phone-input-2';

//import moment from 'moment';

import MomentTimezone from 'moment-timezone';

class Location extends Component {
  constructor(props) {
    super(props);
    this.state = {
        errormsg :  '',
        userTypeList : [],
        roleList : [],
       
        ListGrid: [],
        locationId:'',

        ListCountry: [],
        ListProvince:[],
        ListCity:[],
        
        // Pagination 
        totalCount : 0,
        pageSize : 5,
        currentPage : 1,
        totalPages : 0,
        previousPage : false,
        nextPage : false,
        searchText : '',
        pagingData : {},
        TempsearchText:'',

        sortColumn : '',
        SortType : false,
        IsSortingEnabled : true,
        // Pagination

        // Add
        AddlocationName : '',
        AddlocationType : '',
        AddlocationCode : '',
        AddlocationAddress : '',
        AddPostalCode:'',
        AddCountry:'',
        AddProvince:'',
        AddCity:'',
        AddlocationPhone : '',
        AddlocationPhoneCountryCode : '',
        Addstate : '',
        AddDueDaysToTimesheetSubmit : '',
        AddDueDaysToTimesheetAmendment : '',
        AddDueDaysToTimesheetAmendmentTime : '',
        
        AddStaffDueDaysToTimesheetAmendment : '',
        AddStaffDueDaysToTimesheetAmendmentTime : '',
        // Add

        // Edit
        EditlocationName:'',
        EditlocationType : '',
        EditlocationCode : '',
        EditlocationAddress : '',
        EditPostalCode:'',
        EditCountry:'',
        EditProvince:'',
        EditCity:'',
        EditlocationPhone : '',
        EditlocationPhoneCountryCode : '',
        Editstate : '',
        EditDueDaysToTimesheetSubmit : '',
        EditDueDaysToTimesheetAmendment : '',
        EditDueDaysToTimesheetAmendmentTime : '',

        EditStaffDueDaysToTimesheetAmendment : '',
        EditStaffDueDaysToTimesheetAmendmentTime : '',
        // Edit
        role_locations_can : {},

        isDelete : false,

        //header_data : [],

        hierarchyId:'',
        page_wrapper_cls:'',
        unauthorized_cls:''
    };
    this.setPropState = this.setPropState.bind(this);
    this.handleChange = this.handleChange.bind(this);
       
  }

  setPropState(key, value) {
      this.setState({ [key]: value });
  }

  handleChange = input => e => {
    this.setState({ [input]: e.target.value });
    //console.log(input);
    if (this.state[input] != '') {
        delete this.state.errormsg[input];
    }

    // Pagination
    if([input]=="pageSize")
    {
      this.setState({ current_page: 1 });
      this.GetLocations(1,e.target.value,this.state.searchText)
    }
    // Pagination

    // Add time
    if([input]=="AddCountry")
    {
      this.setState({ ListProvince: [] });
      this.setState({ ListCity: [] });

      this.GetProvince(e.target.value);
      this.setState({ AddProvince: '' });
      this.setState({ AddCity: '' });
    }

    if([input]=="AddProvince")
    {
      this.setState({ ListCity: [] });

      this.GetCity(e.target.value);
      this.setState({ AddCity: '' });
    }
    // Add time

    // Edit time
    if([input]=="EditCountry")
    {
      this.setState({ ListProvince: [] });
      this.setState({ ListCity: [] });

      this.GetProvince(e.target.value);
      this.setState({ EditProvince: '' });
      this.setState({ EditCity: '' });
    }

    if([input]=="EditProvince")
    {
      this.setState({ ListCity: [] });

      this.GetCity(e.target.value);
      this.setState({ EditCity: '' });
    } 
    // Edit time

  }

  // Loader hide show method
  hideLoader = () => {
    this.setState({ loading: false });
  }

  showLoader = () => {
    this.setState({ loading: true });
  }
  // Loader hide show method
  
  // Phone number change event
  handleOnChangeLocationPhone(value, data, event, formattedValue)
  {
    var phone = value;
    var str2 = "+";
    var country_code=data.dialCode;
    if(phone.indexOf(str2) != -1){
        //console.log(str2 + " found");
    }else{
        value='+'+value;
        country_code="+"+data.dialCode;
    }
    //console.log('locationPhone value ' + value);
    //console.log('locationPhone formattedValue ' + formattedValue);
    //console.log('locationPhone country_code ' + country_code);
    this.setState({ AddlocationPhone: value});
    this.setState({ AddlocationPhoneCountryCode : country_code });   
  }
  handleOnChangeLocationPhoneEdit(value, data, event, formattedValue)
  {
    var phone = value;
    var str2 = "+";
    var country_code=data.dialCode;
    if(phone.indexOf(str2) != -1){
        //console.log(str2 + " found");
    }else{
        value='+'+value;
        country_code="+"+data.dialCode;
    }
    //console.log('Edit locationPhone value ' + value);
    //console.log('Edit locationPhone formattedValue ' + formattedValue);
    //console.log('Edit locationPhone country_code ' + country_code);
    this.setState({ EditlocationPhone: value});
    this.setState({ EditlocationPhoneCountryCode : country_code });   
  }
  // Phone number change event

  componentDidMount() {
   /* var timedifference =  MomentTimezone.tz.guess();
    console.log(timedifference);
    alert(timedifference);*/
    /* Role Management */
     //console.log('Role Store locations_can');
     var getrole = SystemHelpers.GetRole();
     let locations_can = getrole.locations_can;
     this.setState({ role_locations_can: locations_can });
     //console.log(locations_can);
    /* Role Management */

    /* Role Management */
    let hierarchyId = getrole.hierarchy_can.hierarchy_can_id;
    this.setState({ hierarchyId: hierarchyId });

    if(hierarchyId == 1){
      this.setState({ page_wrapper_cls: 'page-wrapper pk-page-wrapper-remove hide-div' });
      this.setState({ unauthorized_cls: 'error-box pk-margin-top' });

      setTimeout(() => {
        $('.pk-page-wrapper-remove').remove()
      }, 5000);
      
    }else{
      this.setState({ page_wrapper_cls: 'page-wrapper' });
      this.setState({ unauthorized_cls: 'error-box hide-div' });
    }
    /* Role Management */

    //console.log("Employees");

    //console.log(localStorage.getItem("token"));
    this.GetLocations(this.state.currentPage,this.state.pageSize,this.state.searchText);
    this.GetCountry();

    // Delete Permison
    // Delete Permison
  }

  // country-province-city
  GetCountry(){
    this.showLoader();
    var url=process.env.API_API_URL+'GetCountry';
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        //'Authorization': 'bearer '+localStorage.getItem("token")
      },
      //body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
        //console.log("responseJson GetCountry");
        //console.log(data);
        //console.log(data.data.userRole);
        // debugger;
        if (data.responseType === "1") {
            // Profile & Contact
            if(data.data != null){
               this.setState({ ListCountry: data.data});
            } 
        }else{
          SystemHelpers.ToastError(data.message);   
        }
        this.hideLoader();
        
    })
    .catch(error => {
      this.props.history.push("/error-500");
    });
  }

  GetProvince(id){
    this.showLoader();
    var url=process.env.API_API_URL+'GetProvince?id='+id;
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        //'Authorization': 'bearer '+localStorage.getItem("token")
      },
      //body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
        //console.log("responseJson GetProvince");
        //console.log(data);
        //console.log(data.data.userRole);
        // debugger;
        if (data.responseType === "1") {
            // Profile & Contact
            if(data.data != null){
               this.setState({ ListProvince: data.data});
            } 
        }else{
          SystemHelpers.ToastError(data.message);   
        }
        this.hideLoader();
        
    })
    .catch(error => {
      this.props.history.push("/error-500");
    });
  }

  GetCity(id){
    this.showLoader();
    var url=process.env.API_API_URL+'GetCity?id='+id;
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        //'Authorization': 'bearer '+localStorage.getItem("token")
      },
      //body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
        //console.log("responseJson GetCity");
        //console.log(data);
        //console.log(data.data.userRole);
        // debugger;
        if (data.responseType === "1") {
            // Profile & Contact
            if(data.data != null){
               this.setState({ ListCity: data.data});
            } 
        }else{
          SystemHelpers.ToastError(data.message);   
        }
        this.hideLoader();
        
    })
    .catch(error => {
      this.props.history.push("/error-500");
    });
  }
  // country-province-city

  ClearRecord = ()=> e => {
    e.preventDefault();

    this.setState({ AddlocationName: '' });
    this.setState({ AddlocationType: '' });
    this.setState({ AddlocationCode: '' });
    this.setState({ AddlocationAddress: '' });
    this.setState({ AddPostalCode: '' });
    this.setState({ AddCountry: '' });
    this.setState({ AddProvince: '' });
    this.setState({ AddCity: '' });
    this.setState({ AddlocationPhone: '' });
    this.setState({ AddlocationPhoneCountryCode: '' });
    this.setState({ AddDueDaysToTimesheetSubmit: 0 });
    this.setState({ AddDueDaysToTimesheetAmendment: 0 });
    this.setState({ AddDueDaysToTimesheetAmendmentTime: '' });
    
    this.setState({ AddStaffDueDaysToTimesheetAmendment: 0 });
    this.setState({ AddStaffDueDaysToTimesheetAmendmentTime: '' });

    this.setState({ errormsg: '' });

    this.setState({ EditlocationName: '' });
    this.setState({ EditlocationType: '' });
    this.setState({ EditlocationCode: '' });
    this.setState({ EditlocationAddress: '' });
    this.setState({ EditPostalCode: '' });
    this.setState({ EditCountry: '' });
    this.setState({ EditProvince: '' });
    this.setState({ EditCity: '' });
    this.setState({ EditlocationPhone: '' });
    this.setState({ EditlocationPhoneCountryCode: '' });
    this.setState({ EditDueDaysToTimesheetSubmit: 0 });
    this.setState({ EditDueDaysToTimesheetAmendment: 0 });
    this.setState({ EditDueDaysToTimesheetAmendmentTime: '' });
    
    this.setState({ EditStaffDueDaysToTimesheetAmendment: 0 });
    this.setState({ EditStaffDueDaysToTimesheetAmendmentTime: '' });

  }

  AddRecord = () => e => {
      //debugger;
      e.preventDefault();

      let step1Errors = {};
      
      var AddlocationName =this.state["AddlocationName"];
      if (this.state["AddlocationName"] == '' || this.state["AddlocationName"] == null) {
        step1Errors["AddlocationName"] = "Location Name is mandatory";
      }
      
      if (this.state["AddlocationType"] == "-" || this.state["AddlocationType"] == '' || this.state["AddlocationName"] == null) {
        step1Errors["AddlocationType"] = "Location Type is mandatory";
      }

      if (this.state["AddlocationCode"] == '' || this.state["AddlocationCode"] == null) {
        step1Errors["AddlocationCode"] = "Location Code is mandatory";
      }

      if (this.state["AddlocationAddress"] == '' || this.state["AddlocationAddress"] == null) {
        step1Errors["AddlocationAddress"] = "Location Address is mandatory";
      }

      if (this.state["AddPostalCode"] == '' || this.state["AddPostalCode"] == null) {
        step1Errors["AddPostalCode"] = "Postal Code is mandatory";
      }

      if (this.state["AddCountry"] == '' || this.state["AddCountry"] == null) {
        step1Errors["AddCountry"] = "Country is mandatory";
      }

      if (this.state["AddProvince"] == '' || this.state["AddProvince"] == null) {
        step1Errors["AddProvince"] = "Province is mandatory";
      }

      if (this.state["AddCity"] == '' || this.state["AddCity"] == null) {
        step1Errors["AddCity"] = "City is mandatory";
      }

      var AddlocationPhone =this.state["AddlocationPhone"];
      var AddlocationPhoneCountryCode =this.state["AddlocationPhoneCountryCode"];
      if (this.state["AddlocationPhone"] == '' || this.state["AddlocationPhone"] == null) {
        step1Errors["AddlocationPhone"] = "Location Phone is mandatory";
      }
      else {
        var totalPhoneAdd = AddlocationPhone.length - AddlocationPhoneCountryCode.length;
        if(totalPhoneAdd!=10) {
          step1Errors["AddlocationPhone"] = "Please Enter a Valid Location Phone.";
        }
      }

      // if (this.state["AddDueDaysToTimesheetSubmit"] == '' || this.state["AddDueDaysToTimesheetSubmit"] == null) {
      //   step1Errors["AddDueDaysToTimesheetSubmit"] = "Due Days to Timesheet Submit is mandatory";
      // }

      // if (this.state["AddDueDaysToTimesheetAmendment"] == '' || this.state["AddDueDaysToTimesheetAmendment"] == null) {
      //   step1Errors["AddDueDaysToTimesheetAmendment"] = "Due Days to Timesheet Amendment is mandatory";
      // }

      if (this.state["AddDueDaysToTimesheetAmendmentTime"] == '' || this.state["AddDueDaysToTimesheetAmendmentTime"] == null) {
        step1Errors["AddDueDaysToTimesheetAmendmentTime"] = "Amendment Time is mandatory";
      }

      if (this.state["AddStaffDueDaysToTimesheetAmendmentTime"] == '' || this.state["AddStaffDueDaysToTimesheetAmendmentTime"] == null) {
        step1Errors["AddStaffDueDaysToTimesheetAmendmentTime"] = "Staff Amendment Time is mandatory";
      }
      //console.log(step1Errors);

      this.setState({ errormsg: step1Errors });
          
      if (Object.keys(step1Errors).length > 0) {
          return false;
      }


      this.showLoader();

      
       
      let bodyarray = {};
      bodyarray["locationName"] = this.state["AddlocationName"];

      bodyarray["locationType"] = this.state["AddlocationType"];
      bodyarray["locationCode"] = this.state["AddlocationCode"];
      bodyarray["locationAddress"] = this.state["AddlocationAddress"];
      bodyarray["postalCode"] = this.state["AddPostalCode"];
      bodyarray["countryId"] = this.state["AddCountry"];
      bodyarray["provinceId"] = this.state["AddProvince"];
      bodyarray["cityId"] = this.state["AddCity"];
      bodyarray["locationPhone"] = this.state["AddlocationPhone"];
      bodyarray["dueDateTimeSheet"] = this.state["AddDueDaysToTimesheetSubmit"];

      bodyarray["amendmentsDays"] = this.state["AddDueDaysToTimesheetAmendment"];
      bodyarray["timeForAmendments"] = this.state["AddDueDaysToTimesheetAmendmentTime"];

      bodyarray["amendmentsDaysForStaff"] = this.state["AddStaffDueDaysToTimesheetAmendment"];
      bodyarray["timeForAmendmentsforStaff"] = this.state["AddStaffDueDaysToTimesheetAmendmentTime"];

      console.log(bodyarray);
      //return false;
      var url=process.env.API_API_URL+'CreateUpdateLocation';
      fetch(url, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'bearer '+localStorage.getItem("token")
        },
        body: JSON.stringify(bodyarray)
      }).then((response) => response.json())
      .then(data => {
          console.log("responseJson CreateUpdateLocation");
          console.log(data);
          //console.log(responseJson);
          // debugger;
          if (data.responseType === "1") {
              //this.props.history.push('/dashboard');
              SystemHelpers.ToastSuccess(data.responseMessge);  
              $( ".close" ).trigger( "click" ); 
              this.ClearRecord();
              //this.GetLocations();
              this.GetLocations(this.state.currentPage,this.state.pageSize,this.state.searchText);
          }else if (data.responseType === "2" || data.responseType === "3" || data.responseType === "4") {
              //this.props.history.push('/dashboard');
              SystemHelpers.ToastError(data.responseMessge);  
              //this.GetUserRegistrationDetail();
          }
          else{
              SystemHelpers.ToastError(data.message);
          }
          this.hideLoader();
          
      })
      .catch(error => {
        this.props.history.push("/error-500");
      });
  }

  GetLocations(currentPage,pageSize,searchText){

      /* Role Management */
      var getrole = SystemHelpers.GetRole();
      console.log('Location Get role');
      console.log(getrole.locations_can);
      let canDelete = getrole.locations_can.locations_can_delete;
      //let canViewall = getrole.locations_can.locations_can_viewall;
      let hierarchyId = getrole.hierarchy_can.hierarchy_can_id;
      /* Role Management */

      this.setState({ ListGrid : [] });

      // Pagination
      let bodyarray = {};
      bodyarray["currentPage"] = 1;
      bodyarray["nextPage"] = false;
      bodyarray["pageSize"] = 5;
      bodyarray["previousPage"] = false;
      bodyarray["totalCount"] = 0;
      bodyarray["totalPages"] = 0;
      
      this.setState({ pagingData : bodyarray });

      this.setState({ currentPage: currentPage });
      this.setState({ pageSize: pageSize });

      var sort_Column = this.state.sortColumn;
      var Sort_Type = this.state.SortType;
      
      var IsSortingEnabled = true;

      var url_paging_para = '&pageNumber='+currentPage+'&pageSize='+pageSize+'&searchText='+searchText+'&sortColumn='+sort_Column+'&SortType='+Sort_Type+'&IsSortingEnabled='+IsSortingEnabled;
      // Pagination

      this.showLoader();
      var url=process.env.API_API_URL+'GetLocations'+'?canDelete='+canDelete+'&rolePriorityId='+hierarchyId+"&contactId="+localStorage.getItem("contactId")+url_paging_para;
      fetch(url, {
        //method: 'POST',
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'bearer '+localStorage.getItem("token")
        },
      }).then((response) => response.json())
      .then(data => {
          console.log("responseJson GetLocations");
          console.log(data);
          if (data.responseType === "1") {
              //his.setState({ ListGrid: data.data });
              //this.setState({ ListGrid: this.rowData(data.data) })
              this.setState({ ListGrid: data.data });
              this.setState({ pagingData: data.pagingData });

              //this.setState({ TempsearchText: '' });
              this.hideLoader();
          }else{
            if(data.message == 'Authorization has been denied for this request.'){
              SystemHelpers.SessionOut();
              this.props.history.push("/login");
            }else{
              SystemHelpers.ToastError(data.message);
            }
            this.hideLoader();    
          }
          
          
      })
      .catch(error => {
        this.props.history.push("/error-500");
      });
  }

  

  Edit_Update_Btn_Func(record){
    let return_push = [];

    if(this.state.role_locations_can.locations_can_update == true || this.state.role_locations_can.locations_can_delete == true){
        let Edit_push = [];
        if(this.state.role_locations_can.locations_can_update == true){
          var url ="/employee-profile/"+record.contactId;
          Edit_push.push(
          <a href={url} onClick={this.EditRecord(record)}  className="dropdown-item" data-toggle="modal" data-target="#edit_location"><i className="fa fa-pencil m-r-5" ></i> Edit</a>
          );
        }
        let Delete_push = [];
        if(this.state.role_locations_can.locations_can_delete == true){

          if(record.isDelete == false)
          {
            Delete_push.push(
              <a href="#" onClick={this.EditRecord(record)} className="dropdown-item" data-toggle="modal" data-target="#delete_location"><i className="fa fa-trash-o m-r-5" ></i> Inactive</a>
            );
          }
          else
          {
            Delete_push.push(
              <a href="#" onClick={this.EditRecord(record)} className="dropdown-item" data-toggle="modal" data-target="#delete_location"><i className="fa fa-trash-o m-r-5" ></i> Active</a>
            );
          }
        }
        
        return_push.push(
          <div className="dropdown dropdown-action">
            <a aria-expanded="false" data-toggle="dropdown" className="action-icon dropdown-toggle" href="#"><i className="material-icons">more_vert</i></a>
            <div className="dropdown-menu dropdown-menu-right">
              {Edit_push}
              {Delete_push}
            </div>
          </div>
        );
      }
      return return_push;
  }

  EditRecord = (record) => e => {
    e.preventDefault();
    console.log('Edit Location master');
    console.log(record);

    this.setState({ locationId: record.locationId });
    this.setState({ EditlocationName: record.locationName });

    this.setState({ EditlocationType: record.locationType });
    this.setState({ EditlocationCode: record.locationCode });
    this.setState({ EditlocationAddress: record.locationAddress });
    this.setState({ EditPostalCode: record.postalCode });
    this.setState({ EditCountry: record.countryId });
    this.setState({ EditProvince: record.provinceId });
    this.setState({ EditCity: record.cityId });
    this.setState({ EditlocationPhone: record.locationPhone });
    this.setState({ EditDueDaysToTimesheetSubmit: record.dueDateTimeSheet });

    this.setState({ EditDueDaysToTimesheetAmendment: record.amendmentsDays });
    this.setState({ EditDueDaysToTimesheetAmendmentTime: record.timeForAmendments });

    this.GetProvince(record.countryId);
    this.GetCity(record.provinceId);

    var locationPhone = record.locationPhone;
    if(locationPhone != null && locationPhone != ''){
      var CountrylocationPhone = locationPhone.substring(0, locationPhone.length-10)
      this.setState({ EditlocationPhoneCountryCode: CountrylocationPhone });
    }

    this.setState({ EditStaffDueDaysToTimesheetAmendment : record.amendmentsDaysForStaff });
    this.setState({ EditStaffDueDaysToTimesheetAmendmentTime : record.timeForAmendmentsforStaff });

    this.setState({ isDelete: record.isDelete });
  }

  UpdateRecord = () => e => {
      //debugger;
      e.preventDefault();

      let step1Errors = {};
      
      if (this.state["EditlocationName"] == '' || this.state["EditlocationName"] == null) {
        step1Errors["EditlocationName"] = "Location Name is mandatory";
      }
      
      if (this.state["EditlocationType"] == "-" || this.state["EditlocationType"] == '' || this.state["EditlocationName"] == null) {
        step1Errors["EditlocationType"] = "Location Type is mandatory";
      }

      if (this.state["EditlocationCode"] == '' || this.state["EditlocationCode"] == null) {
        step1Errors["EditlocationCode"] = "Location Code is mandatory";
      }

      if (this.state["EditlocationAddress"] == '' || this.state["EditlocationAddress"] == null) {
        step1Errors["EditlocationAddress"] = "Location Address is mandatory";
      }

      if (this.state["EditPostalCode"] == '' || this.state["EditPostalCode"] == null) {
        step1Errors["EditPostalCode"] = "Postal Code is mandatory";
      }

      if (this.state["EditCountry"] == '' || this.state["EditCountry"] == null) {
        step1Errors["EditCountry"] = "Country is mandatory";
      }

      if (this.state["EditProvince"] == '' || this.state["EditProvince"] == null) {
        step1Errors["EditProvince"] = "Province is mandatory";
      }

      if (this.state["EditCity"] == '' || this.state["EditCity"] == null) {
        step1Errors["EditCity"] = "City is mandatory";
      }

      var EditlocationPhone =this.state["EditlocationPhone"];
      var EditlocationPhoneCountryCode = this.state["EditlocationPhoneCountryCode"];
      
      //alert(this.state["EditlocationPhone"]);
      //alert(this.state["EditlocationPhoneCountryCode"]);

      if (this.state["EditlocationPhone"] == '' || this.state["EditlocationPhone"] == null) {
        step1Errors["EditlocationPhone"] = "Location Phone is mandatory";
      }
      else {
        var totalPhoneAdd = EditlocationPhone.length - EditlocationPhoneCountryCode.length;
        if(totalPhoneAdd!=10) {
          step1Errors["EditlocationPhone"] = "Please Enter a Valid Location Phone.";
        }
      }

      // if (this.state["EditDueDaysToTimesheetSubmit"] == '' || this.state["EditDueDaysToTimesheetSubmit"] == null) {
      //   step1Errors["EditDueDaysToTimesheetSubmit"] = "Due Days to Timesheet Submit is mandatory";
      // }

      // if (this.state["EditDueDaysToTimesheetAmendment"] == '' || this.state["EditDueDaysToTimesheetAmendment"] == null) {
      //   step1Errors["EditDueDaysToTimesheetAmendment"] = "Due Days to Timesheet Amendment is mandatory";
      // }

      if (this.state["EditDueDaysToTimesheetAmendmentTime"] == '' || this.state["EditDueDaysToTimesheetAmendmentTime"] == null) {
        step1Errors["EditDueDaysToTimesheetAmendmentTime"] = "Amendment Time is mandatory";
      }

      if (this.state["EditStaffDueDaysToTimesheetAmendmentTime"] == '' || this.state["EditStaffDueDaysToTimesheetAmendmentTime"] == null) {
        step1Errors["EditStaffDueDaysToTimesheetAmendmentTime"] = "Staff Amendment Time is mandatory";
      }

      console.log(step1Errors);

      this.setState({ errormsg: step1Errors });
          
      if (Object.keys(step1Errors).length > 0) {
          return false;
      }

      this.showLoader();

      let bodyarray = {};

      bodyarray["locationName"] = this.state["EditlocationName"];
      bodyarray["locationId"] = this.state["locationId"];

      bodyarray["locationType"] = this.state["EditlocationType"];
      bodyarray["locationCode"] = this.state["EditlocationCode"];
      bodyarray["locationAddress"] = this.state["EditlocationAddress"];
      bodyarray["postalCode"] = this.state["EditPostalCode"];
      bodyarray["countryId"] = this.state["EditCountry"];
      bodyarray["provinceId"] = this.state["EditProvince"];
      bodyarray["cityId"] = this.state["EditCity"];
      bodyarray["locationPhone"] = this.state["EditlocationPhone"];
      bodyarray["dueDateTimeSheet"] = this.state["EditDueDaysToTimesheetSubmit"];

      bodyarray["amendmentsDays"] = this.state["EditDueDaysToTimesheetAmendment"];
      bodyarray["timeForAmendments"] = this.state["EditDueDaysToTimesheetAmendmentTime"];

      bodyarray["amendmentsDaysForStaff"] = this.state["EditStaffDueDaysToTimesheetAmendment"];
      bodyarray["timeForAmendmentsforStaff"] = this.state["EditStaffDueDaysToTimesheetAmendmentTime"];

      console.log(bodyarray);
      //return false;
      var url=process.env.API_API_URL+'CreateUpdateLocation';
      fetch(url, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'bearer '+localStorage.getItem("token")
        },
        body: JSON.stringify(bodyarray)
      }).then((response) => response.json())
      .then(data => {
          console.log("responseJson CreateUpdateLocation");
          console.log(data);
          //console.log(responseJson);
          // debugger;
          if (data.responseType === "1") {
              //this.props.history.push('/dashboard');
              SystemHelpers.ToastSuccess(data.responseMessge);  
              $( ".close" ).trigger( "click" ); 
              this.ClearRecord();
              //this.GetLocations();
              this.GetLocations(this.state.currentPage,this.state.pageSize,this.state.searchText);
          }else if (data.responseType === "2" || data.responseType === "3" || data.responseType === "4") {
              //this.props.history.push('/dashboard');
              SystemHelpers.ToastError(data.responseMessge);  
          }
          else{
              SystemHelpers.ToastError(data.message);
          }
          this.hideLoader();
          
      })
      .catch(error => {
        this.props.history.push("/error-500");
      });
  }

  DeleteRecord = () => e => {
      e.preventDefault();

      var isdelete = '';
      if(this.state.isDelete== true)
      {
        isdelete = false;
      }
      else
      {
        isdelete = true;
      }

      this.showLoader();
      console.log(this.state.skillId);
      var url=process.env.API_API_URL+'DeleteLocation?locationId='+this.state.locationId+'&isDelete='+isdelete+'&userName='+localStorage.getItem('fullName');
      fetch(url, {
        method: 'PUT',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'bearer '+localStorage.getItem("token")
        },
        //body: JSON.stringify(bodyarray)
      }).then((response) => response.json())
      .then(data => {
          console.log("responseJson DeleteLocation");
          console.log(data);
          //console.log(data.data.userRole);
          // debugger;
          if (data.responseType === "1") {
              // Profile & Contact
              SystemHelpers.ToastSuccess(data.responseMessge);
              $( ".cancel-btn" ).trigger( "click" );
              this.GetLocations(this.state.currentPage,this.state.pageSize,this.state.searchText);
              this.hideLoader();
          }else if (data.responseType == "2" || data.responseType == "3") {
              SystemHelpers.ToastError(data.responseMessge);
              $( ".cancel-btn" ).trigger( "click" );
              this.hideLoader();
          }else{
                if(data.message == 'Authorization has been denied for this request.'){
                  SystemHelpers.SessionOut();
                  this.props.history.push("/login");
                }else{
                  SystemHelpers.ToastError(data.message);
                }
                this.hideLoader();
                $( ".cancel-btn" ).trigger( "click" );
          }
          
          
      })
      .catch(error => {
        this.props.history.push("/error-500");
      });
  }

  // Pagination Design
  PaginationDesign ()
  {
    let PageOutput = [];
    //console.log('pagination');
    //console.log(this.state.pagingData);
    
    if(this.state.pagingData !="" && this.state.pagingData !="undefined")
    {
      var Page_Count = this.state.pagingData.totalPages;
      //alert(this.state.pagingData.currentPage);
    //console.log('page count = ' + Page_Count);
    /* pagination count */


        var Page_Start=1;
        var Page_End=1;

        if(this.state.pagingData.currentPage == 1){
            Page_Start=1;

            if(Page_Count <= 10){
                Page_End=Page_Count;
            }else{
                Page_End=10;
            }
            
        }else{

            if(this.state.pagingData.currentPage < 5){
                Page_Start=1;
                Page_End=Page_Count;
                //Page_End=parseInt(this.state.pagingData.currentPage) + parseInt(6);
                //console.log("Page_End 1 "+ Page_End);
            }else{
                Page_Start=parseInt(this.state.pagingData.currentPage) - parseInt(4);
                Page_End=parseInt(this.state.pagingData.currentPage) + parseInt(6);
                //console.log("Page_End 2 "+ Page_End);
                if(Page_End > Page_Count){
                    Page_End=Page_Count;
                    //console.log("Page_End 3 "+ Page_End);
                }
            }

        }
      let Page = [];
      var i = 1;
      for (var z=Page_Start; z <= Page_End ; z++)
      {
        if(z==this.state.pagingData.currentPage)
        {
          Page.push(<li className="page-item active pk-active">
            <a className="page-link pk-active" id={z} href="#" onClick={this.PageGetGridData}>{z}<span className="sr-only">(current)</span></a>
          </li>);
        }
        else
        {
          Page.push(<li className="page-item"><a className="page-link" id={z} href="#" onClick={this.PageGetGridData} >{z}</a></li>);
        }
        i++;
      }

      let PagePrev = [];

      if(this.state.pagingData.currentPage == 1){
        PagePrev.push(<li className="page-item disabled">
          <a className="page-link" href="#">Previous</a>
        </li>);
      }else{
        PagePrev.push(<li className="page-item">
          <a className="page-link" href="#" id={parseInt(this.state.pagingData.currentPage)-parseInt(1)} tabIndex={-1} onClick={this.PageGetGridData}>Previous</a>
        </li>);
      }

      let PageNext = [];

      if(this.state.pagingData.currentPage == Page_Count){
        PageNext.push(<li className="page-item disabled">
          <a className="page-link" href="#">Next</a>
        </li>);
      }else{
        PageNext.push(
          <li className="page-item">
            <a className="page-link" href="#" id={parseInt(this.state.pagingData.currentPage)+parseInt(1)} onClick={this.PageGetGridData}>Next</a>
          </li>
        );
      }

      let PageLast = [];

      if(this.state.pagingData.currentPage == Page_Count){
        PageLast.push(<li className="page-item disabled">
          <a className="page-link" href="#">Last</a>
        </li>);
      }else{
        PageLast.push(
          <li className="page-item">
            <a className="page-link" href="#" id={parseInt(Page_Count)} onClick={this.PageGetGridData}>Last</a>
          </li>
        );
      }



      PageOutput.push(<section className="comp-section" id="comp_pagination">
                        <div className="pagination-box">
                          <div>
                            <ul className="pagination">
                              
                              {PagePrev}
                              {Page}
                              {PageNext}
                              {PageLast}
                              
                            </ul>
                          </div>
                        </div>
                      </section>);
    }
    
    return PageOutput;
  }

  PageGetGridData = e => {

    e.preventDefault();
    let current_page= e.target.id;
    this.GetLocations(current_page,this.state.pageSize,this.state.searchText)
  }

  SearchGridData = e => {
    this.setState({ pageSize: this.state.TempsearchText });
    this.GetLocations(1,this.state.pageSize,this.state.TempsearchText);
  }
  // Pagination Design

  render() {
      
                        
      return ( 
      <div className="main-wrapper">
          {/* Toast & Loder method use */}
            
          {(this.state.loading) ? <Loader /> : null} 
          {/* Toast & Loder method use */}
      <Header/>
        <div className={this.state.unauthorized_cls}>
          {/*<h1>403</h1>*/}
          <h4><i className="fa fa-warning"></i> Sorry, you are not authorized to access this page.</h4>
          {/*<p>Sorry, you are not authorized to access this page.</p>*/}
        </div>
        <div className={this.state.page_wrapper_cls}>
            <Helmet>
                <title>{process.env.WEB_TITLE}</title>
                <meta name="description" content="Login page"/>         
            </Helmet>
              {/* Page Content */}
              <div className="content container-fluid">
                {/* Page Header */}
                <div className="page-header">
                  <div className="row align-items-center">
                    <div className="col">
                      <h3 className="page-title">Location Management</h3>
                      <ul className="breadcrumb">
                        <li className="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                        <li className="breadcrumb-item active">Location Management</li>
                      </ul>
                    </div>
                    <div className="col-auto float-right ml-auto">

                      { this.state.role_locations_can.locations_can_create == true  ?

                        <a href="#" className="btn add-btn" data-toggle="modal" data-target="#add_location"><i className="fa fa-plus" /> Add Location</a>
                      :<a href="#" className="phss-lock"><i className="fa fa-lock" /></a>
                      }
                    </div>
                  </div>
                </div>
                {/* /Page Header */}

                {/* Page Per Record and serach design*/}
                <div className="row filter-row">
                  <div className="col-sm-6 col-md-2"> 
                    <div className="form-group form-focus select-focus">
                      <select className="form-control floating" value={this.state.pageSize}  onChange={this.handleChange('pageSize')}> 
                        <option value="5">5/Page</option>
                        <option value="10">10/Page</option>
                        <option value="50">50/Page</option>
                        <option value="100">100/Page</option>
                      </select>
                      <label className="focus-label">Per Page</label>
                    </div>
                  </div>
                  
                  <div className="col-sm-6 col-md-3">
                    <div className="form-group form-focus">
                      <label className="focus-label">Sorting</label>
                      <select className="form-control floating" id="sortColumn" value={this.state.sortColumn} onChange={this.handleChange('sortColumn')}> 
                        <option value="">-</option>
                        <option value="LocationName">Location</option>
                      </select>
                    </div>
                  </div> 

                  <div className="col-sm-6 col-md-2">
                    <div className="form-group form-focus">
                      <label className="focus-label">Sorting Order</label>
                      <select className="form-control floating" id="SortTypeId" value={this.state.SortType} onChange={this.handleChange('SortType')}> 
                        <option value="">-</option>
                        <option value="false">Ascending</option>
                        <option value="true">Descending</option>
                      </select>
                    </div>
                  </div> 

                  <div className="col-sm-6 col-md-3">  
                    <div className="form-group form-focus">
                      <input className="form-control floating" type="text" value={this.state.TempsearchText}  onChange={this.handleChange('TempsearchText')}/>
                      <label className="focus-label">Search</label>
                    </div>
                  </div>

                  <div className="col-sm-6 col-md-2">  
                    <a href="#" className="btn btn-success btn-block" onClick={this.SearchGridData}> Search </a>  
                  </div> 
                </div>
                {/* Page Per Record and serach design*/}

                <div className="row">
                  <div className="col-md-12">
                    <div className="table-responsive pk-overflow-hide">
                      
                      {/*<MDBDataTable
                        striped
                        bordered
                        small
                        data={data}
                        entriesOptions={[5, 10, 20, 50, 100]} entries={5} pagesAmount={4}
                        className="table table-striped custom-table mb-0 datatable"
                      />*/}

                      <table className="table table-striped custom-table mb-0 datatable">
                          <thead>
                            <tr>
                              <th>Location</th>
                              <th>Status</th>
                              <th>Action</th>
                            </tr>
                          </thead>
                          <tbody>
                            {this.state.ListGrid.map(( listValue, index ) => {
                                var status = "";
                                if(listValue.isDelete == false){
                                  status = <div><span class="badge bg-inverse-success">Active</span></div>;
                                }else if(listValue.isDelete == "Inactive"){
                                  status = <div><span class="badge bg-inverse-warning">Inactive</span></div>;
                                }
                                return (
                                  <tr key={index}>
                                    <td>{listValue.locationName}</td>
                                    <td>{status}</td>
                                    <td>{this.Edit_Update_Btn_Func(listValue)}</td>
                                  </tr>
                                );
                              
                            })}
                            
                          </tbody>
                        </table> 

                        {/* Pagination */}
                        {this.PaginationDesign()}
                        {/* /Pagination */}
                       
                    </div>
                  </div>
                </div>
              </div>
              {/* /Page Content */}
              {/* Add Employees Modal */}
              <div id="add_location" className="modal custom-modal fade" role="dialog">
                <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
                  <div className="modal-content">
                    <div className="modal-header">
                      <h5 className="modal-title">Add Location Details</h5>
                      <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={this.ClearRecord()}>
                        <span aria-hidden="true">×</span>
                      </button>
                    </div>
                    <div className="modal-body">
                      <form>
                        <div className="row">
                          <div className="col-md-6">
                            <div className="form-group">
                              <label>Location Name <span className="text-danger">*</span></label>
                              <input className="form-control" type="text" value={this.state.AddlocationName}  onChange={this.handleChange('AddlocationName')} />
                              <span className="form-text error-font-color">{this.state.errormsg["AddlocationName"]}</span>
                            </div>
                          </div>

                          <div className="col-md-6">
                            <div className="form-group">
                              <label>Location Type <span className="text-danger">*</span></label>
                              <select className="form-control" value={this.state.AddlocationType} onChange={this.handleChange('AddlocationType')}>
                                <option value="">-</option>
                                <option value="Family">Family</option>
                                <option value="Residential and Administration">Residential and Administration</option>
                                <option value="Administrative Department">Administrative Department</option>
                              </select>
                              <span className="form-text error-font-color">{this.state.errormsg["AddlocationType"]}</span>
                            </div>
                          </div>
                          <div className="col-md-6">
                            <div className="form-group">
                              <label>Location Code <span className="text-danger">*</span></label>
                              <input className="form-control" type="text" value={this.state.AddlocationCode}  onChange={this.handleChange('AddlocationCode')} />
                              <span className="form-text error-font-color">{this.state.errormsg["AddlocationCode"]}</span>
                            </div>
                          </div>
                          <div className="col-md-6">
                            <div className="form-group">
                              <label>Location Address <span className="text-danger">*</span></label>
                              <input className="form-control" type="text" value={this.state.AddlocationAddress}  onChange={this.handleChange('AddlocationAddress')} />
                              <span className="form-text error-font-color">{this.state.errormsg["AddlocationAddress"]}</span>
                            </div>
                          </div>
                          <div className=" col-md-6">
                            <div className="form-group">
                              <label>Postal Code <span className="text-danger">*</span></label>
                              <input className="form-control" type="text" value="123-456-7890" value={this.state.AddPostalCode} onChange={this.handleChange('AddPostalCode')}/>
                              <span className="form-text error-font-color">{this.state.errormsg["AddPostalCode"]}</span>
                            </div>
                          </div>
                          <div className="col-md-6">
                            <div className="form-group">
                              <label>Country <span className="text-danger">*</span></label>
                              <select className="form-control" value={this.state.AddCountry} onChange={this.handleChange('AddCountry')}>
                                <option value="">-</option>
                                {this.state.ListCountry.map(( listValue, index ) => {
                                  return (
                                    <option key={index} value={listValue.countryId}>{listValue.countryName}</option>
                                  );
                                })}
                              </select>
                              <span className="form-text error-font-color">{this.state.errormsg["AddCountry"]}</span>
                            </div>
                          </div>
                          <div className="col-md-6">
                            <div className="form-group">
                              <label>Province <span className="text-danger">*</span></label>
                              <select className="form-control" value={this.state.AddProvince} onChange={this.handleChange('AddProvince')}>
                                <option value="">-</option>
                                {this.state.ListProvince.map(( listValue, index ) => {
                                  return (
                                    <option key={index} value={listValue.provinceId}>{listValue.provinceName}</option>
                                  );
                                })}
                              </select>
                              <span className="form-text error-font-color">{this.state.errormsg["AddProvince"]}</span>
                            </div>
                          </div>
                          <div className="col-md-6">
                            <div className="form-group">
                              <label>City <span className="text-danger">*</span></label>
                              <select className="form-control" value={this.state.AddCity} onChange={this.handleChange('AddCity')}>
                                <option value="">-</option>
                                {this.state.ListCity.map(( listValue, index ) => {
                                  return (
                                    <option key={index} value={listValue.cityId}>{listValue.cityName}</option>
                                  );
                                })}
                              </select>
                              <span className="form-text error-font-color">{this.state.errormsg["AddCity"]}</span>
                            </div>
                          </div>
                          <div className="col-md-6">
                            <div className="form-group">
                              <label>Location Phone <span className="text-danger">*</span></label>
                              <PhoneInput
                                    inputClass='form-control'
                                    inputStyle= {{'width': '100%'}}
                                    country={"ca"}
                                    onlyCountries={['ca', 'in' ,'us']}
                                    value={this.state.AddlocationPhone}
                                    onChange={this.handleOnChangeLocationPhone.bind(this)}
                                  />
                              <span className="form-text error-font-color">{this.state.errormsg["AddlocationPhone"]}</span>
                            </div>
                          </div>
                          <div className="col-lg-6">
                            <div className="form-group">
                              <label>Due Days to Timesheet Submit</label>
                              <select className="form-control" value={this.state.AddDueDaysToTimesheetSubmit} onChange={this.handleChange('AddDueDaysToTimesheetSubmit')}>
                                <option value="">0</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                              </select>
                              <span className="form-text error-font-color">{this.state.errormsg["AddDueDaysToTimesheetSubmit"]}</span>
                            </div>
                          </div>
                          <div className="col-lg-6">
                            <div className="form-group">
                              <label>Due Days to Timesheet Amendment </label>
                              <select className="form-control" value={this.state.AddDueDaysToTimesheetAmendment} onChange={this.handleChange('AddDueDaysToTimesheetAmendment')}>
                                <option value="">0</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                              </select>
                              <span className="form-text error-font-color">{this.state.errormsg["AddDueDaysToTimesheetAmendment"]}</span>
                            </div>
                          </div>
                          <div className=" col-lg-6">
                            <div className="form-group">
                              <label>Amendment Time<span className="text-danger">*</span></label>
                              <input className="form-control" type="time" value={this.state.AddDueDaysToTimesheetAmendmentTime} onChange={this.handleChange('AddDueDaysToTimesheetAmendmentTime')}/>
                              <span className="form-text error-font-color">{this.state.errormsg["AddDueDaysToTimesheetAmendmentTime"]}</span>
                            </div>
                          </div>

                          <div className="col-lg-6">
                            <div className="form-group">
                              <label>Approver Due Days to Timesheet Amendment </label>
                              <select className="form-control" value={this.state.AddStaffDueDaysToTimesheetAmendment} onChange={this.handleChange('AddStaffDueDaysToTimesheetAmendment')}>
                                <option value="">0</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                              </select>
                              <span className="form-text error-font-color">{this.state.errormsg["AddStaffDueDaysToTimesheetAmendment"]}</span>
                            </div>
                          </div>
                          <div className=" col-lg-6">
                            <div className="form-group">
                              <label>Approver Amendment Time<span className="text-danger">*</span></label>
                              <input className="form-control" type="time" value={this.state.AddStaffDueDaysToTimesheetAmendmentTime} onChange={this.handleChange('AddStaffDueDaysToTimesheetAmendmentTime')}/>
                              <span className="form-text error-font-color">{this.state.errormsg["AddStaffDueDaysToTimesheetAmendmentTime"]}</span>
                            </div>
                          </div>

                        </div>
                        <div className="submit-section">
                          <button className="btn btn-primary submit-btn" onClick={this.AddRecord()}>Submit</button>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
              {/* /Add Employees Modal */}
              {/* Edit Employees Modal */}
              <div id="edit_location" className="modal custom-modal fade" role="dialog">
                <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
                  <div className="modal-content">
                    <div className="modal-header">
                      <h5 className="modal-title">Edit Location Details</h5>
                      <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                      </button>
                    </div>
                    <div className="modal-body">
                      <form>
                        <div className="row">
                          
                          <div className="col-md-6">
                            <div className="form-group">
                              <label>Location Name <span className="text-danger">*</span></label>
                              <input className="form-control" type="text" value={this.state.EditlocationName}  onChange={this.handleChange('EditlocationName')} readonly="readonly" />
                              <span className="form-text error-font-color">{this.state.errormsg["EditlocationName"]}</span>
                            </div>
                          </div>

                          <div className="col-md-6">
                            <div className="form-group">
                              <label>Location Type <span className="text-danger">*</span></label>
                              <select className="form-control" value={this.state.EditlocationType} onChange={this.handleChange('EditlocationType')}>
                                <option value="">-</option>
                                <option value="Family">Family</option>
                                <option value="Residential and Administration">Residential and Administration</option>
                                <option value="Administrative Department">Administrative Department</option>
                              </select>
                              <span className="form-text error-font-color">{this.state.errormsg["EditlocationType"]}</span>
                            </div>
                          </div>
                          <div className="col-md-6">
                            <div className="form-group">
                              <label>Location Code <span className="text-danger">*</span></label>
                              <input className="form-control" type="text" value={this.state.EditlocationCode}  onChange={this.handleChange('EditlocationCode')} readonly="readonly" />
                              <span className="form-text error-font-color">{this.state.errormsg["EditlocationCode"]}</span>
                            </div>
                          </div>
                          <div className="col-md-6">
                            <div className="form-group">
                              <label>Location Address <span className="text-danger">*</span></label>
                              <input className="form-control" type="text" value={this.state.EditlocationAddress}  onChange={this.handleChange('EditlocationAddress')} />
                              <span className="form-text error-font-color">{this.state.errormsg["EditlocationAddress"]}</span>
                            </div>
                          </div>
                          <div className="col-md-6">
                            <div className="form-group">
                              <label>Postal Code <span className="text-danger">*</span></label>
                              <input className="form-control" type="text" value="123-456-7890" value={this.state.EditPostalCode} onChange={this.handleChange('EditPostalCode')}/>
                              <span className="form-text error-font-color">{this.state.errormsg["EditPostalCode"]}</span>
                            </div>
                          </div>
                          <div className="col-md-6">
                            <div className="form-group">
                              <label>Country <span className="text-danger">*</span></label>
                              <select className="form-control" value={this.state.EditCountry} onChange={this.handleChange('EditCountry')}>
                                <option value="">-</option>
                                {this.state.ListCountry.map(( listValue, index ) => {
                                  return (
                                    <option key={index} value={listValue.countryId}>{listValue.countryName}</option>
                                  );
                                })}
                              </select>
                              <span className="form-text error-font-color">{this.state.errormsg["EditCountry"]}</span>
                            </div>
                          </div>
                          <div className="col-md-6">
                            <div className="form-group">
                              <label>Province <span className="text-danger">*</span></label>
                              <select className="form-control" value={this.state.EditProvince} onChange={this.handleChange('EditProvince')}>
                                <option value="">-</option>
                                {this.state.ListProvince.map(( listValue, index ) => {
                                  return (
                                    <option key={index} value={listValue.provinceId}>{listValue.provinceName}</option>
                                  );
                                })}
                              </select>
                              <span className="form-text error-font-color">{this.state.errormsg["EditProvince"]}</span>
                            </div>
                          </div>
                          <div className="col-md-6">
                            <div className="form-group">
                              <label>City <span className="text-danger">*</span></label>
                              <select className="form-control" value={this.state.EditCity} onChange={this.handleChange('EditCity')}>
                                <option value="">-</option>
                                {this.state.ListCity.map(( listValue, index ) => {
                                  return (
                                    <option key={index} value={listValue.cityId}>{listValue.cityName}</option>
                                  );
                                })}
                              </select>
                              <span className="form-text error-font-color">{this.state.errormsg["EditCity"]}</span>
                            </div>
                          </div>
                          <div className="col-md-6">
                            <div className="form-group">
                              <label>Location Phone<span className="text-danger">*</span></label>
                              <PhoneInput
                                    inputClass='form-control'
                                    inputStyle= {{'width': '100%'}}
                                    country={"ca"}
                                    onlyCountries={['ca', 'in' ,'us']}
                                    value={this.state.EditlocationPhone}
                                    onChange={this.handleOnChangeLocationPhoneEdit.bind(this)}
                                  />
                              <span className="form-text error-font-color">{this.state.errormsg["EditlocationPhone"]}</span>
                            </div>
                          </div>
                          <div className="col-lg-6">
                            <div className="form-group">
                              <label>Due Days to Timesheet Submit </label>
                              <select className="form-control" value={this.state.EditDueDaysToTimesheetSubmit} onChange={this.handleChange('EditDueDaysToTimesheetSubmit')}>
                                <option value="">0</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                              </select>
                              <span className="form-text error-font-color">{this.state.errormsg["EditDueDaysToTimesheetSubmit"]}</span>
                            </div>
                          </div>
                          <div className="col-lg-6">
                            <div className="form-group">
                              <label>Due Days to Timesheet Amendment Submit </label>
                              <select className="form-control" value={this.state.EditDueDaysToTimesheetAmendment} onChange={this.handleChange('EditDueDaysToTimesheetAmendment')}>
                                <option value="">0</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                              </select>
                              <span className="form-text error-font-color">{this.state.errormsg["EditDueDaysToTimesheetAmendment"]}</span>
                            </div>
                          </div>
                          <div className=" col-lg-6">
                            <div className="form-group">
                              <label>Amendment Time<span className="text-danger">*</span></label>
                              <input className="form-control" type="time" value={this.state.EditDueDaysToTimesheetAmendmentTime} onChange={this.handleChange('EditDueDaysToTimesheetAmendmentTime')}/>
                              <span className="form-text error-font-color">{this.state.errormsg["EditDueDaysToTimesheetAmendmentTime"]}</span>
                            </div>
                          </div>

                          <div className="col-lg-6">
                            <div className="form-group">
                              <label>Approver Due Days to Timesheet Amendment </label>
                              <select className="form-control" value={this.state.EditStaffDueDaysToTimesheetAmendment} onChange={this.handleChange('EditStaffDueDaysToTimesheetAmendment')}>
                                <option value="">0</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                              </select>
                              <span className="form-text error-font-color">{this.state.errormsg["EditStaffDueDaysToTimesheetAmendment"]}</span>
                            </div>
                          </div>
                          <div className=" col-lg-6">
                            <div className="form-group">
                              <label>Approver Amendment Time<span className="text-danger">*</span></label>
                              <input className="form-control" type="time" value={this.state.EditStaffDueDaysToTimesheetAmendmentTime} onChange={this.handleChange('EditStaffDueDaysToTimesheetAmendmentTime')}/>
                              <span className="form-text error-font-color">{this.state.errormsg["EditStaffDueDaysToTimesheetAmendmentTime"]}</span>
                            </div>
                          </div>

                        </div>
                        <div className="submit-section">
                          <button className="btn btn-primary submit-btn" onClick={this.UpdateRecord()}>Update</button>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
              {/* /Edit Employees Modal */}
              {/* Delete Skills Locations  Modal */}
                <div className="modal custom-modal fade" id="delete_location" role="dialog">
                  <div className="modal-dialog modal-dialog-centered">
                    <div className="modal-content">
                      <div className="modal-body">
                        <div className="form-header">
                          <h3>Location</h3>
                          <p>Are you sure you want to mark location as {this.state.isDelete == true ? 'Active' : 'Inactive' } ?</p>
                        </div>
                        <div className="modal-btn delete-action">
                          <div className="row">
                            <div className="col-6">
                              <a  onClick={this.DeleteRecord()} className="btn btn-primary continue-btn">{this.state.isDelete == true ? 'Active' : 'Inactive' }</a>
                            </div>
                            <div className="col-6">
                              <a href="" data-dismiss="modal" className="btn btn-primary cancel-btn">Cancel</a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
            {/* /Delete Trained Locations Modal */}
            </div>
          <SidebarContent/>
      </div>
        );
      
   }
}

export default Location;
