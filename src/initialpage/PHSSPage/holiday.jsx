
import React, { Component } from 'react';
import { Helmet } from "react-helmet";
import {  Avatar_01 ,Avatar_02,Avatar_03,Avatar_04, Avatar_05, Avatar_08, Avatar_09, Avatar_10,
    Avatar_11,Avatar_12,Avatar_13,Avatar_16   } from "../../Entryfile/imagepath"

import { Table } from 'antd';
import 'antd/dist/antd.css';
import {itemRender,onShowSizeChange} from "./pagination/paginationfunction"
import "../MainPage/antdstyle.css"
import { Multiselect } from 'multiselect-react-dropdown';

import Select from 'react-select-me';
import 'react-select-me/lib/ReactSelectMe.css';

import Header from '../Sidebar/header.jsx';
import SidebarContent from '../Sidebar/sidebar';


import Loader from '../Loader';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import SystemHelpers from '../Helpers/SystemHelper';

//table
import { MDBDataTable } from 'mdbreact';
import '@fortawesome/fontawesome-free/css/all.min.css';
import 'bootstrap-css-only/css/bootstrap.min.css';
import 'mdbreact/dist/css/mdb.css';
//table

import CryptoAES from 'crypto-js/aes';
import CryptoENC from 'crypto-js/enc-utf8';

import Datetime from "react-datetime";

import moment from 'moment';

class Holiday extends Component {
  constructor(props) {
    super(props);
    this.state = {
        errormsg :  '',
        userTypeList : [],
        roleList : [],
       
        ListGrid: [],
        locationId:'',

        ListCountry: [],
        ListProvince:[],
        ListCity:[],
        EditListProvince : [],

        // Pagination 
        totalCount : 0,
        pageSize : 5,
        currentPage : 1,
        totalPages : 0,
        previousPage : false,
        nextPage : false,
        searchText : '',
        pagingData : {},
        TempsearchText:'',

        sortColumn : '',
        SortType : false,
        IsSortingEnabled : true,
        // Pagination
       
        // Add
        AddholidayName : '',
        AddStartDate:'',
        AddCountry:'',
        AddProvince:[],
        // Add

        // Edit
        EditholidayName : '',
        EditStartDate:'',
        EditCountry:'',
        EditProvince:[],
        // Edit

        role_holidays_can : {},

        isDelete : false,

        header_data : [],

        staffContactFullname : localStorage.getItem('fullName')
    };
    this.setPropState = this.setPropState.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.onSelectProvince = this.onSelectProvince.bind(this);
    this.onRemoveProvince = this.onRemoveProvince.bind(this);  
    this.ProvinceListRef = React.createRef();

    this.handleAddStartDate = this.handleAddStartDate.bind(this);
    this.handleEditStartDate = this.handleEditStartDate.bind(this);
    
  }


  handleAddStartDate = (date) =>{
    //alert(date);
    console.log('AddStartDate => '+ date);
    //console.log('AddTimeOut_Max => '+ NewDate);
    this.setState({ AddStartDate : date });
  };

  handleEditStartDate = (date) =>{
    //alert(date);
    console.log('EditStartDate => '+ date);
    //console.log('AddTimeOut_Max => '+ NewDate);
    this.setState({ EditStartDate : date });
  };

  setPropState(key, value) {
      this.setState({ [key]: value });
  }

  handleChange = input => e => {
    this.setState({ [input]: e.target.value });
    //console.log(input);
    if (this.state[input] != '') {
        delete this.state.errormsg[input];
    }

    // Pagination
    if([input]=="pageSize")
    {
      this.setState({ current_page: 1 });
      this.GetHolidays(1,e.target.value,this.state.searchText);
    }
    // Pagination

    // Add time
    if([input]=="AddCountry")
    {
      this.setState({ ListProvince: [] });
      this.setState({ ListCity: [] });

      this.GetProvince(e.target.value);
      this.setState({ AddProvince: '' });
    }

    // Add time

    // Edit time
    if([input]=="EditCountry")
    {
      this.setState({ ListProvince: [] });
      this.setState({ ListCity: [] });
      this.setState({ EditListProvince: [] });

      this.GetProvince(e.target.value);
      this.setState({ EditProvince: '' });
    }

    // Edit time

  }

  // Loader hide show method
  hideLoader = () => {
    this.setState({ loading: false });
  }

  showLoader = () => {
    this.setState({ loading: true });
  }
  // Loader hide show method
  
  
  componentDidMount() {

    // var id_of_div = "Test_637672201071063094"; // Test_09/14/2021 12:39:44 PM
    // var id_number = id_of_div.replace(/[^0-9.]/g, "");
    // console.log('Holiday get time');
    // console.log(id_number);
    // //console.log(STR_TO_DATE(id_number));
    // //console.log(Date.parse(id_number.toString()));
    // console.log(moment(id_number));


    /* Role Management */
     console.log('Role Store holidays_can');
     var getrole = SystemHelpers.GetRole();
     let holidays_can = getrole.holidays_can;
     this.setState({ role_holidays_can: holidays_can });
     console.log(holidays_can);
    /* Role Management */

    console.log("Employees");

    console.log(localStorage.getItem("token"));

    this.GetCountry();
    this.GetHolidays(this.state.currentPage,this.state.pageSize,this.state.searchText);
    

    // Delete Permison
    // Delete Permison
  }

  // Province Multe select
  onSelectProvince(selectedList, selectedItem) {
    let lang =[];
    // provinceId provinceName
    for (var i = 0; i<selectedList.length; i++) {
      var number1 = selectedList[i].id;
      var number =  number1.toString();
      lang.push(number);
      console.log('holiday Province select ');
      console.log(selectedList);
    }
    this.setState({ AddProvince: lang });
    delete this.state.errormsg['AddProvince'];
  }

  onRemoveProvince(selectedList, removedItem) {
    let lang =[];
    
    for (var i = 0; i<selectedList.length; i++) {
      var number1 = selectedList[i].id;
      var number =  number1.toString();
      lang.push(number);

    }
    if(lang.length != 0){
      this.setState({ AddProvince: lang });
    }
    delete this.state.errormsg['AddProvince'];
  }

  resetValues() {
    // By calling the belowe method will reset the selected values programatically
    this.ProvinceListRef.current.resetSelectedValues();
  }
  // Province multe select

  // country-province-city
  GetCountry(){
    this.showLoader();
    var url=process.env.API_API_URL+'GetCountry';
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        //'Authorization': 'bearer '+localStorage.getItem("token")
      },
      //body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
        console.log("responseJson GetCountry");
        console.log(data);
        //console.log(data.data.userRole);
        // debugger;
        if (data.responseType === "1") {
            // Profile & Contact
            if(data.data != null){
               this.setState({ ListCountry: data.data});
            } 
        }else{
          SystemHelpers.ToastError(data.message);   
        }
        this.hideLoader();
        
    })
    .catch(error => {
      this.props.history.push("/error-500");
    });
  }

  GetProvince(id){
    this.showLoader();
    this.resetValues();
    var url=process.env.API_API_URL+'GetProvince?id='+id;
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        //'Authorization': 'bearer '+localStorage.getItem("token")
      },
      //body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
        console.log("responseJson GetProvince");
        console.log(data);
        //console.log(data.data.userRole);
        // debugger;
        if (data.responseType === "1") {
            // Profile & Contact
            if(data.data != null){
              this.setState({ ListProvince: this.FuncProvinceList(data.data)});
              this.setState({ EditListProvince: data.data });
              //console.log(this.FuncProvinceList(data.data));
            } 
        }else{
          SystemHelpers.ToastError(data.message);   
        }
        this.hideLoader();
        
    })
    .catch(error => {
      this.props.history.push("/error-500");
    });
  }

  FuncProvinceList(Province)
  {
    console.log('fun FuncProvinceList');
    //console.log(Location);
    var Province_lenght = Province.length;
    let dataArray = [];
    var i=1;
    if(Province_lenght > 0)
    {
      for (var z = 0; z < Province_lenght; z++) {
        var tempdataArray = [];
        //SelectFLocationList  
        tempdataArray.name = Province[z].provinceName;
        tempdataArray.id = Province[z].provinceId;

        dataArray.push(tempdataArray);
        z++;
      }
    }
    console.log(dataArray);
    return dataArray;
    
  }
  // country-province-city

  ClearRecord = ()=> e => {
    e.preventDefault();

    this.setState({ AddholidayName: '' });
    this.setState({ AddStartDate: '' });
    this.setState({ AddCountry: 0 });
    
    this.resetValues();

    
    this.setState({ errormsg: '' });
  }

  AddRecord = () => e => {
    //alert();
      //debugger;
      e.preventDefault();

      let step1Errors = {};
      
      var AddholidayName =this.state["AddholidayName"];
      if (this.state["AddholidayName"] == '' || this.state["AddholidayName"] == null) {
        step1Errors["AddholidayName"] = "Holiday Name is mandatory";
      }

      if (this.state["AddStartDate"] == '') {
        step1Errors["AddStartDate"] = "Date is mandatory";
      }

      if (this.state["AddCountry"] == '' || this.state["AddCountry"] == null) {
        step1Errors["AddCountry"] = "Country is mandatory";
      }

      if (this.state["AddProvince"].length == 0 || this.state["AddProvince"] == '' || this.state["AddProvince"] == null) {
        step1Errors["AddProvince"] = "Province is mandatory";
      }

      console.log(step1Errors);

      this.setState({ errormsg: step1Errors });
          
      if (Object.keys(step1Errors).length > 0) {
          return false;
      }


      this.showLoader();

      var AddStartDate=moment(this.state["AddStartDate"]).format('MM-DD-YYYY');
      

      let bodyarray = {};
      bodyarray["holidayName"] = this.state["AddholidayName"];

      bodyarray["holidayDate"] = AddStartDate;
      bodyarray["countryId"] = this.state["AddCountry"];
      bodyarray["provinceId"] = this.state["AddProvince"];

      bodyarray["loggedInUserName"] = localStorage.getItem('fullName');
      bodyarray["loggedInId"] = localStorage.getItem('contactId');
      
      console.log(bodyarray);
      //return false;
      var url=process.env.API_API_URL+'CreateUpdateHolidayList';
      fetch(url, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'bearer '+localStorage.getItem("token")
        },
        body: JSON.stringify(bodyarray)
      }).then((response) => response.json())
      .then(data => {
          console.log("responseJson CreateUpdateHolidayList");
          console.log(data);
          //console.log(responseJson);
          // debugger;
          if (data.responseType === "1") {

              this.setState({ AddholidayName: '' });
              this.setState({ AddStartDate: '' });
              this.setState({ AddProvince: 0 });
              this.setState({ AddCountry: 0 });

              this.resetValues();
              
              SystemHelpers.ToastSuccess(data.responseMessge);  
              $( ".close" ).trigger( "click" ); 
              this.GetHolidays(this.state.currentPage,this.state.pageSize,this.state.searchText);
          }else if (data.responseType === "2" || data.responseType === "3" || data.responseType === "4") {
              SystemHelpers.ToastError(data.responseMessge);  
          }
          else{
              SystemHelpers.ToastError(data.message);
          }
          this.hideLoader();
          
      })
      .catch(error => {
        this.props.history.push("/error-500");
      });
  }

  GetHolidays(currentPage,pageSize,searchText){

      /* Role Management */
      var getrole = SystemHelpers.GetRole();
      let canDelete = getrole.holidays_can.holidays_can_delete;
      /* Role Management */

      this.setState({ ListGrid : [] });

      // Pagination
      let bodyarray = {};
      bodyarray["currentPage"] = 1;
      bodyarray["nextPage"] = false;
      bodyarray["pageSize"] = 5;
      bodyarray["previousPage"] = false;
      bodyarray["totalCount"] = 0;
      bodyarray["totalPages"] = 0;
      
      this.setState({ pagingData : bodyarray });

      this.setState({ currentPage: currentPage });
      this.setState({ pageSize: pageSize });

      var sort_Column = this.state.sortColumn;
      var Sort_Type = this.state.SortType;
      
      var IsSortingEnabled = true;

      var url_paging_para = '&pageNumber='+currentPage+'&pageSize='+pageSize+'&searchText='+searchText+'&sortColumn='+sort_Column+'&SortType='+Sort_Type+'&IsSortingEnabled='+IsSortingEnabled;
      // Pagination

      this.showLoader();
      var url=process.env.API_API_URL+'GetHolidayList'+'?canDelete='+canDelete+url_paging_para;
      console.log("api GetHolidays");
      console.log(url);
      fetch(url, {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'bearer '+localStorage.getItem("token")
        },
      }).then((response) => response.json())
      .then(data => {
          console.log("responseJson GetHolidayList");
          console.log(data);
          if (data.responseType === "1") {
              this.setState({ ListGrid: data.data });
              //this.setState({ ListGrid: this.rowData(data.data) });
              this.setState({ pagingData: data.pagingData });

              this.hideLoader();
          }else{
            if(data.message == 'Authorization has been denied for this request.'){
              SystemHelpers.SessionOut();
              this.props.history.push("/login");
            }else{
              SystemHelpers.ToastError(data.message);
            }
            this.hideLoader();    
          }
          
          
      })
      .catch(error => {
        this.props.history.push("/error-500");
      });
  }

  rowData(ListGrid) {
      console.log('rowdata holiday');
      console.log(ListGrid);

      /* Role Management */
      var getrole = SystemHelpers.GetRole();
      let canDelete = getrole.holidays_can.holidays_can_delete;
      /* Role Management */

      var ListGrid_length = ListGrid.length;
      let dataArray = [];
      var i=1;
      for (var z = 0; z < ListGrid_length; z++) {
        var tempdataArray = [];
        //tempdataArray.rownum = i;
        tempdataArray.holidayname = ListGrid[z].holidayName;
        tempdataArray.date = moment(ListGrid[z].date,process.env.API_DATE_FORMAT).format(process.env.DATE_FORMAT);
        tempdataArray.country = ListGrid[z].countryName;
        tempdataArray.province = ListGrid[z].provienceName;
        
        if(canDelete == true)
        {
          var status = "";
          if(ListGrid[z].isDelete == true){
            tempdataArray.status = <div><span class="badge bg-inverse-warning">Inactive</span></div>;
          }else{
            tempdataArray.status = <div><span class="badge bg-inverse-success">Active</span></div>;
          }
        }

        tempdataArray.action = this.Edit_Update_Btn_Func(ListGrid[z]);

        dataArray.push(tempdataArray);
        i++;
      }

      return dataArray;
  }

  Edit_Update_Btn_Func(record){
    let return_push = [];

    if(this.state.role_holidays_can.holidays_can_update == true || this.state.role_holidays_can.holidays_can_delete == true){
        let Edit_push = [];
        if(this.state.role_holidays_can.holidays_can_update == true){
          Edit_push.push(
          <a href="#" onClick={this.EditRecord(record)}  className="dropdown-item" data-toggle="modal" data-target="#edit_location"><i className="fa fa-pencil m-r-5"></i>Edit</a>
          );
        }
        let Delete_push = [];
        if(this.state.role_holidays_can.holidays_can_delete == true){

          if(record.isDelete == false)
          {
            Delete_push.push(
              <a href="#" onClick={this.EditRecord(record)} className="dropdown-item" data-toggle="modal" data-target="#delete_location"><i className="fa fa-lock m-r-5"></i> Inactive</a>
            );
          }
          else
          {
            Delete_push.push(
              <a href="#" onClick={this.EditRecord(record)} className="dropdown-item" data-toggle="modal" data-target="#delete_location"><i className="fa fa-unlock m-r-5"></i> Active</a>
            );
          }
        }
        
        return_push.push(
          <div className="dropdown dropdown-action">
            <a aria-expanded="false" data-toggle="dropdown" className="action-icon dropdown-toggle" href="#"><i className="material-icons">more_vert</i></a>
            <div className="dropdown-menu dropdown-menu-right">
              {Edit_push}
              {Delete_push}
            </div>
          </div>
        );
      }
      return return_push;
  }

  EditRecord = (record) => e => {
    e.preventDefault();
    //console.log('Holiday EditRecord');
    //console.log(record);

    this.setState({ holidayId: record.holidayId });
    this.setState({ EditholidayName: record.holidayName });

    //this.setState({ EditStartDate: moment(record.date).format('YYYY-MM-DD') });
    this.setState({ EditStartDate: moment(record.date,process.env.API_DATE_FORMAT) });
    this.setState({ EditCountry: record.countryId });
    this.setState({ EditProvince: record.provienceId });
    
    this.GetProvince(record.countryId);
    
    this.setState({ isDelete: record.isDelete });
  }

  UpdateRecord = () => e => {
      //debugger;
      e.preventDefault();

      let step1Errors = {};
      
      if (this.state["EditholidayName"] == '' || this.state["EditholidayName"] == null) {
        step1Errors["EditholidayName"] = "Holiday Name is mandatory";
      }
      
      if (this.state["EditStartDate"] == "-" || this.state["EditStartDate"] == '') {
        step1Errors["EditStartDate"] = "Date is mandatory";
      }

      if (this.state["EditCountry"] == '' || this.state["EditCountry"] == null) {
        step1Errors["EditCountry"] = "Country is mandatory";
      }

      if (this.state["EditProvince"] == '' || this.state["EditProvince"] == null) {
        step1Errors["EditProvince"] = "Province is mandatory";
      }

      console.log(step1Errors);

      this.setState({ errormsg: step1Errors });
          
      if (Object.keys(step1Errors).length > 0) {
          return false;
      }

      this.showLoader();

      let EditProvinceArray = [];
      EditProvinceArray.push(this.state["EditProvince"]);

      //var EditStartDate=moment(this.state["EditStartDate"]).format(process.env.DATE_FORMAT);
      var EditStartDate=moment(this.state["EditStartDate"]).format("MM-DD-YYYY");
      //var NewEditStartDate = moment(EditStartDate, "MM-DD-YYYY").add(1, 'days');
       
      let bodyarray = {};
      bodyarray["holidayName"] = this.state["EditholidayName"];
      bodyarray["holidayDate"] = EditStartDate;
      bodyarray["countryId"] = this.state["EditCountry"];
      //bodyarray["provinceId"] = this.state["EditProvince"];
      bodyarray["provinceId"] = EditProvinceArray;
      bodyarray["holidayId"] = this.state["holidayId"];

      bodyarray["loggedInUserName"] = localStorage.getItem('fullName');
      bodyarray["loggedInId"] = localStorage.getItem('contactId');
      bodyarray["userName"] = this.state.staffContactFullname;

      console.log(bodyarray);
      //return false;
      var url=process.env.API_API_URL+'CreateUpdateHolidayList';
      fetch(url, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'bearer '+localStorage.getItem("token")
        },
        body: JSON.stringify(bodyarray)
      }).then((response) => response.json())
      .then(data => {
          console.log("responseJson CreateUpdateHolidayList");
          console.log(data);
          //console.log(responseJson);
          // debugger;
          if (data.responseType === "1") {
              //this.props.history.push('/dashboard');
              SystemHelpers.ToastSuccess(data.responseMessge);  
              $( ".close" ).trigger( "click" ); 
              this.GetHolidays(this.state.currentPage,this.state.pageSize,this.state.searchText);
          }else if (data.responseType === "2" || data.responseType === "3" || data.responseType === "4") {
              //this.props.history.push('/dashboard');
              SystemHelpers.ToastError(data.responseMessge);  
          }
          else{
              SystemHelpers.ToastError(data.message);
          }
          this.hideLoader();
          
      })
      .catch(error => {
        this.props.history.push("/error-500");
      });
  }

  DeleteRecord = () => e => {
      e.preventDefault();

      var isdelete = '';
      if(this.state.isDelete== true)
      {
        isdelete = false;
      }
      else
      {
        isdelete = true;
      }

      this.showLoader();
      console.log(this.state.holidayId);
      var url=process.env.API_API_URL+'DeleteHolidayList?holidayId='+this.state.holidayId+'&isDelete='+isdelete+'&userName='+this.state.staffContactFullname;
      fetch(url, {
        method: 'PUT',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'bearer '+localStorage.getItem("token")
        },
        //body: JSON.stringify(bodyarray)
      }).then((response) => response.json())
      .then(data => {
          console.log("responseJson DeleteHolidayList");
          console.log(data);
          //console.log(data.data.userRole);
          // debugger;
          if (data.responseType === "1") {
              // Profile & Contact
              SystemHelpers.ToastSuccess(data.responseMessge);
              $( ".cancel-btn" ).trigger( "click" );
              this.GetHolidays(this.state.currentPage,this.state.pageSize,this.state.searchText);
              this.hideLoader();
          }else if (data.responseType == "2" || data.responseType == "3") {
              SystemHelpers.ToastError(data.responseMessge);
              $( ".cancel-btn" ).trigger( "click" );
              this.hideLoader();
          }else{
                if(data.message == 'Authorization has been denied for this request.'){
                  SystemHelpers.SessionOut();
                  this.props.history.push("/login");
                }else{
                  SystemHelpers.ToastError(data.message);
                }
                this.hideLoader();
                $( ".cancel-btn" ).trigger( "click" );
          }
          
          
      })
      .catch(error => {
        this.props.history.push("/error-500");
      });
  }

  // Pagination Design
  PaginationDesign ()
  {
    let PageOutput = [];
    //console.log('pagination');
    //console.log(this.state.pagingData);
    
    if(this.state.pagingData !="" && this.state.pagingData !="undefined")
    {
      var Page_Count = this.state.pagingData.totalPages;
      //alert(this.state.pagingData.currentPage);
    //console.log('page count = ' + Page_Count);
    /* pagination count */


        var Page_Start=1;
        var Page_End=1;

        if(this.state.pagingData.currentPage == 1){
            Page_Start=1;

            if(Page_Count <= 10){
                Page_End=Page_Count;
            }else{
                Page_End=10;
            }
            
        }else{

            if(this.state.pagingData.currentPage < 5){
                Page_Start=1;
                Page_End=Page_Count;
                //Page_End=parseInt(this.state.pagingData.currentPage) + parseInt(6);
                //console.log("Page_End 1 "+ Page_End);
            }else{
                Page_Start=parseInt(this.state.pagingData.currentPage) - parseInt(4);
                Page_End=parseInt(this.state.pagingData.currentPage) + parseInt(6);
                //console.log("Page_End 2 "+ Page_End);
                if(Page_End > Page_Count){
                    Page_End=Page_Count;
                    //console.log("Page_End 3 "+ Page_End);
                }
            }

        }
      let Page = [];
      var i = 1;
      for (var z=Page_Start; z <= Page_End ; z++)
      {
        if(z==this.state.pagingData.currentPage)
        {
          Page.push(<li className="page-item active pk-active">
            <a className="page-link pk-active" id={z} href="#" onClick={this.PageGetGridData}>{z}<span className="sr-only">(current)</span></a>
          </li>);
        }
        else
        {
          Page.push(<li className="page-item"><a className="page-link" id={z} href="#" onClick={this.PageGetGridData} >{z}</a></li>);
        }
        i++;
      }

      let PagePrev = [];

      if(this.state.pagingData.currentPage == 1){
        PagePrev.push(<li className="page-item disabled">
          <a className="page-link" href="#">Previous</a>
        </li>);
      }else{
        PagePrev.push(<li className="page-item">
          <a className="page-link" href="#" id={parseInt(this.state.pagingData.currentPage)-parseInt(1)} tabIndex={-1} onClick={this.PageGetGridData}>Previous</a>
        </li>);
      }

      let PageNext = [];

      if(this.state.pagingData.currentPage == Page_Count){
        PageNext.push(<li className="page-item disabled">
          <a className="page-link" href="#">Next</a>
        </li>);
      }else{
        PageNext.push(
          <li className="page-item">
            <a className="page-link" href="#" id={parseInt(this.state.pagingData.currentPage)+parseInt(1)} onClick={this.PageGetGridData}>Next</a>
          </li>
        );
      }

      let PageLast = [];

      if(this.state.pagingData.currentPage == Page_Count){
        PageLast.push(<li className="page-item disabled">
          <a className="page-link" href="#">Last</a>
        </li>);
      }else{
        PageLast.push(
          <li className="page-item">
            <a className="page-link" href="#" id={parseInt(Page_Count)} onClick={this.PageGetGridData}>Last</a>
          </li>
        );
      }



      PageOutput.push(<section className="comp-section" id="comp_pagination">
                        <div className="pagination-box">
                          <div>
                            <ul className="pagination">
                              
                              {PagePrev}
                              {Page}
                              {PageNext}
                              {PageLast}
                              
                            </ul>
                          </div>
                        </div>
                      </section>);
    }
    
    return PageOutput;
  }

  PageGetGridData = e => {

    e.preventDefault();
    let current_page= e.target.id;
    this.GetHolidays(current_page,this.state.pageSize,this.state.searchText);
  }

  SearchGridData = e => {
    this.setState({ pageSize: this.state.TempsearchText });
    this.GetHolidays(1,this.state.pageSize,this.state.TempsearchText);
  }
  // Pagination Design

  render() {
      
                        
      return ( 
      <div className="main-wrapper">
          {/* Toast & Loder method use */}
            
          {(this.state.loading) ? <Loader /> : null} 
          {/* Toast & Loder method use */}
      <Header/>
        <div className="page-wrapper">
            <Helmet>
                <title>{process.env.WEB_TITLE}</title>
                <meta name="description" content="Login page"/>         
            </Helmet>
              {/* Page Content */}
              <div className="content container-fluid">
                {/* Page Header */}
                <div className="page-header">
                  <div className="row align-items-center">
                    <div className="col">
                      <h3 className="page-title">Holiday Management</h3>
                      <ul className="breadcrumb">
                        <li className="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                        <li className="breadcrumb-item active">Holiday Management</li>
                      </ul>
                    </div>
                    <div className="col-auto float-right ml-auto">

                      { this.state.role_holidays_can.holidays_can_create == true  ?

                        <a href="#" className="btn add-btn" data-toggle="modal" data-target="#add_location"><i className="fa fa-plus" />Add Holiday</a>
                      :<a href="#" className="phss-lock"><i className="fa fa-lock" /></a>
                      }
                    </div>
                  </div>
                </div>
                {/* /Page Header */}

                {/* Page Per Record and serach design*/}
                <div className="row filter-row">
                  <div className="col-sm-6 col-md-2"> 
                    <div className="form-group form-focus select-focus">
                      <select className="form-control floating" value={this.state.pageSize}  onChange={this.handleChange('pageSize')}> 
                        <option value="5">5/Page</option>
                        <option value="10">10/Page</option>
                        <option value="50">50/Page</option>
                        <option value="100">100/Page</option>
                      </select>
                      <label className="focus-label">Per Page</label>
                    </div>
                  </div>

                  <div className="col-sm-6 col-md-3">
                    <div className="form-group form-focus">
                      <label className="focus-label">Sorting</label>
                      <select className="form-control floating" id="sortColumn" value={this.state.sortColumn} onChange={this.handleChange('sortColumn')}> 
                        <option value="">-</option>
                        <option value="HolidayName">Holiday Name</option>
                        <option value="Date">Date</option>
                        {/*<option value="Country">Country</option>
                        <option value="Province">Province</option>*/}
                      </select>
                    </div>
                  </div> 
                  <div className="col-sm-6 col-md-2">
                    <div className="form-group form-focus">
                      <label className="focus-label">Sorting Order</label>
                      <select className="form-control floating" id="SortTypeId" value={this.state.SortType} onChange={this.handleChange('SortType')}> 
                        <option value="">-</option>
                        <option value="false">Ascending</option>
                        <option value="true">Descending</option>
                      </select>
                    </div>
                  </div>
                  <div className="col-sm-6 col-md-3">  
                    <div className="form-group form-focus">
                      <input className="form-control floating" type="text" value={this.state.TempsearchText}  onChange={this.handleChange('TempsearchText')}/>
                      <label className="focus-label">Search</label>
                    </div>
                  </div>  

                  <div className="col-sm-6 col-md-2">  
                    <a href="#" className="btn btn-success btn-block" onClick={this.SearchGridData}> Search </a>  
                  </div> 
                </div>
                {/* Page Per Record and serach design*/}

                <div className="row">
                  <div className="col-md-12">
                    <div className="table-responsive pk-overflow-hide">
                      
                      {/*<MDBDataTable
                        striped
                        bordered
                        small
                        data={data}
                        entriesOptions={[5, 10, 20, 50, 100]} entries={5} pagesAmount={4}
                        className="table table-striped custom-table mb-0 datatable"
                      />*/}
                       
                      <table className="table table-striped custom-table mb-0 datatable">
                        <thead>
                          <tr>
                            <th>Holiday Name</th>
                            <th>Date</th>
                            <th>Country</th>
                            <th>Province</th>
                            <th>Status</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          {this.state.ListGrid.map(( listValue, index ) => {
                              var status = "";
                              if(listValue.isDelete == false){
                                status = <div><span class="badge bg-inverse-success">Active</span></div>;
                              }else if(listValue.isDelete == "Inactive"){
                                status = <div><span class="badge bg-inverse-warning">Inactive</span></div>;
                              }
                              return (
                                <tr key={index}>
                                  <td>{listValue.holidayName}</td>
                                  <td>{moment(listValue.date,process.env.API_DATE_FORMAT).format(process.env.DATE_FORMAT)}</td>
                                  <td>{listValue.countryName}</td>
                                  <td>{listValue.provienceName}</td>
                                  <td>{status}</td>
                                  <td>{this.Edit_Update_Btn_Func(listValue)}</td>
                                </tr>
                              );
                            
                          })}
                          
                        </tbody>
                      </table> 

                      {/* Pagination */}
                      {this.PaginationDesign()}
                      {/* /Pagination */}

                    </div>
                  </div>
                </div>
              </div>
              {/* /Page Content */}
              {/* Add Employees Modal */}
              <div id="add_location" className="modal custom-modal fade" role="dialog">
                <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
                  <div className="modal-content">
                    <div className="modal-header">
                      <h5 className="modal-title">Add Holiday</h5>
                      <button type="button" className="close" data-dismiss="modal" aria-label="Close"  onClick={this.ClearRecord()}>
                        <span aria-hidden="true">×</span>
                      </button>
                    </div>
                    <div className="modal-body">
                      <form>
                        <div className="row">
                          <div className="col-md-6">
                            <div className="form-group">
                              <label>Holiday Name<span className="text-danger">*</span></label>
                              <input className="form-control" type="text" value={this.state.AddholidayName}  onChange={this.handleChange('AddholidayName')} />
                              <span className="form-text error-font-color">{this.state.errormsg["AddholidayName"]}</span>
                            </div>
                          </div>
                          <div className="col-md-6">
                            <div className="form-group">
                              <label>Date<span className="text-danger">*</span></label>
                              <Datetime
                                inputProps={{readOnly: true}}
                                closeOnTab={true}
                                input={true}
                                value={(this.state.AddStartDate) ? this.state.AddStartDate : ''}
                                onChange={this.handleAddStartDate}
                                dateFormat={process.env.DATE_FORMAT}
                                timeFormat={false}
                                renderInput={(props) => {
                                   return <input {...props} value={(this.state.AddStartDate) ? props.value : ''} />
                                }}
                              />
                              {/*<input className="form-control" type="date" value={this.state.AddStartDate} onChange={this.handleChange('AddStartDate')}  />*/}
                              <span className="form-text error-font-color">{this.state.errormsg["AddStartDate"]}</span>
                            </div>
                          </div>
                          <div className="col-md-6">
                            <div className="form-group">
                              <label>Country <span className="text-danger">*</span></label>
                              <select className="form-control" value={this.state.AddCountry} onChange={this.handleChange('AddCountry')}>
                                <option value="">-</option>
                                {this.state.ListCountry.map(( listValue, index ) => {
                                  return (
                                    <option key={index} value={listValue.countryId}>{listValue.countryName}</option>
                                  );
                                })}
                              </select>
                              <span className="form-text error-font-color">{this.state.errormsg["AddCountry"]}</span>
                            </div>
                          </div>
                          <div className="col-md-6">
                            <div className="form-group">
                              <label>Province <span className="text-danger">*</span></label>
                              {/*<select className="form-control" value={this.state.AddProvince} onChange={this.handleChange('AddProvince')}>
                                <option value="">-</option>
                                {this.state.ListProvince.map(( listValue, index ) => {
                                  return (
                                    <option key={index} value={listValue.provinceId}>{listValue.provinceName}</option>
                                  );
                                })}
                              </select>*/}

                              <Multiselect
                                options={this.state.ListProvince} // Options to display in the dropdown
                                selectedValues={this.state.selectedValue} // Preselected value to persist in dropdown
                                onSelect={this.onSelectProvince} // Function will trigger on select event
                                onRemove={this.onRemoveProvince} // Function will trigger on remove event
                                displayValue="name" // Property name to display in the dropdown options
                                ref={this.ProvinceListRef}
                              />


                              <span className="form-text error-font-color">{this.state.errormsg["AddProvince"]}</span>
                            </div>
                          </div>
                        </div>
                        <div className="submit-section">
                          <button className="btn btn-primary submit-btn" onClick={this.AddRecord()}>Submit</button>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
              {/* /Add Employees Modal */}
              {/* Edit Employees Modal */}
              <div id="edit_location" className="modal custom-modal fade" role="dialog">
                <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
                  <div className="modal-content">
                    <div className="modal-header">
                      <h5 className="modal-title">Edit Holiday</h5>
                      <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                      </button>
                    </div>
                    <div className="modal-body">
                      <form>
                        <div className="row">
                          <div className="col-md-6">
                            <div className="form-group">
                              <label>Holiday Name<span className="text-danger">*</span></label>
                              <input className="form-control" type="text" value={this.state.EditholidayName}  onChange={this.handleChange('EditholidayName')} />
                              <span className="form-text error-font-color">{this.state.errormsg["EditholidayName"]}</span>
                            </div>
                          </div>
                          <div className="col-md-6">
                            <div className="form-group">
                              <label>Date<span className="text-danger">*</span></label>
                              <Datetime
                                inputProps={{readOnly: true}}
                                closeOnTab={true}
                                input={true}
                                value={(this.state.EditStartDate) ? this.state.EditStartDate : ''}
                                onChange={this.handleEditStartDate}
                                dateFormat={process.env.DATE_FORMAT}
                                timeFormat={false}
                                enderInput={(props) => {
                                   return <input {...props} value={(this.state.EditStartDate) ? props.value : ''} />
                                }}
                              />
                              {/*<input className="form-control" type="date" value={this.state.EditStartDate} onChange={this.handleChange('EditStartDate')}  />*/}
                              <span className="form-text error-font-color">{this.state.errormsg["EditStartDate"]}</span>
                            </div>
                          </div>
                          <div className="col-md-6">
                            <div className="form-group">
                              <label>Country <span className="text-danger">*</span></label>
                              <select className="form-control" value={this.state.EditCountry} onChange={this.handleChange('EditCountry')}>
                                <option value="">-</option>
                                {this.state.ListCountry.map(( listValue, index ) => {
                                  return (
                                    <option key={index} value={listValue.countryId}>{listValue.countryName}</option>
                                  );
                                })}
                              </select>
                              <span className="form-text error-font-color">{this.state.errormsg["EditCountry"]}</span>
                            </div>
                          </div>
                          <div className="col-md-6">
                            <div className="form-group">
                              <label>Province <span className="text-danger">*</span></label>
                              <select className="form-control" value={this.state.EditProvince} onChange={this.handleChange('EditProvince')}>
                                <option value="">-</option>
                                {this.state.EditListProvince.map(( listValue, index ) => {
                                  return (
                                    <option key={index} value={listValue.provinceId}>{listValue.provinceName}</option>
                                  );
                                })}
                              </select>
                              <span className="form-text error-font-color">{this.state.errormsg["EditProvince"]}</span>
                            </div>
                          </div>
                        </div>
                        <div className="submit-section">
                          <button className="btn btn-primary submit-btn" onClick={this.UpdateRecord()}>Update</button>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
              {/* /Edit Employees Modal */}
              {/* Delete Skills Locations  Modal */}
                <div className="modal custom-modal fade" id="delete_location" role="dialog">
                  <div className="modal-dialog modal-dialog-centered">
                    <div className="modal-content">
                      <div className="modal-body">
                        <div className="form-header">
                          <h3>Holiday</h3>
                          <p>Are you sure you want to mark holiday as {this.state.isDelete == true ? 'Active' : 'Inactive' } ?</p>
                        </div>
                        <div className="modal-btn delete-action">
                          <div className="row">
                            <div className="col-6">
                              <a  onClick={this.DeleteRecord()} className="btn btn-primary continue-btn">{this.state.isDelete == true ? 'Active' : 'Inactive' }</a>
                            </div>
                            <div className="col-6">
                              <a href="" data-dismiss="modal" className="btn btn-primary cancel-btn">Cancel</a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
            {/* /Delete Trained Locations Modal */}
            </div>
          <SidebarContent/>
      </div>
        );
      
   }
}

export default Holiday;
