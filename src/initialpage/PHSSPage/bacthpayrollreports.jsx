
import React, { Component } from 'react';
import { Helmet } from "react-helmet";
import {  Avatar_01 ,Avatar_02,Avatar_03,Avatar_04, Avatar_05, Avatar_08, Avatar_09, Avatar_10,
    Avatar_11,Avatar_12,Avatar_13,Avatar_16   } from "../../Entryfile/imagepath"

import { Table } from 'antd';
import 'antd/dist/antd.css';
import {itemRender,onShowSizeChange} from "./pagination/paginationfunction"
import "../MainPage/antdstyle.css"
import { Multiselect } from 'multiselect-react-dropdown';


import 'react-select-me/lib/ReactSelectMe.css';

import Header from '../Sidebar/header.jsx';
import SidebarContent from '../Sidebar/sidebar';

import Loader from '../Loader';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import SystemHelpers from '../Helpers/SystemHelper';
import FileUploadHelper from '../Helpers/FileUploadHelper';

import CryptoAES from 'crypto-js/aes';
import CryptoENC from 'crypto-js/enc-utf8';

import moment from 'moment';

import SelectDropdown from 'react-dropdown-select';

//import Select from 'react-select-me';
import Select from 'react-select';

//table
import { MDBDataTable } from 'mdbreact';
import '@fortawesome/fontawesome-free/css/all.min.css';
import 'bootstrap-css-only/css/bootstrap.min.css';
import 'mdbreact/dist/css/mdb.css';
//table

class BacthPayrollReports extends Component {
  constructor(props) {
    super(props);
    this.state = {

        locationID:localStorage.getItem("primaryLocationId"),
        staffContactID:localStorage.getItem("contactId"),
        fullName:localStorage.getItem("fullName"),

        role_reports_can : {},
        role_employees_can : {},
        role_hierarchy_can : {},

        errormsg :  '',
        isDelete : false,
        ListGrid : [],

        header_data : [],

        payPeriodListFilter : [],
        locationListFilter : [],
        
        employmentTypeListFilter :[],
        
        EmployeeNameListFilter : [],

        FilterPayPeriod : '',
        FilterLocation : '',
        
        FilterEmploymentType : '',
        
       
        
        FilterEmployee : '',

        ExportFilter : {},
       
        GetCRMUserList:[],
        selectedOptionLocation: null,
        selectedOptionEmployees: null
    };

    this.setPropState = this.setPropState.bind(this);
    this.handleChange = this.handleChange.bind(this);
    
    this.onChangeSearch = this.onChangeSearch.bind(this);
  }

  onChangeSearch(value) {
    console.log(value);
    this.setState({ value });
  }

  setPropState(key, value) {
      this.setState({ [key]: value });
  }

  // Loader hide show method
  hideLoader = () => {
    this.setState({ loading: false });
  }

  showLoader = () => {
    this.setState({ loading: true });
  }
  // Loader hide show method

  handleChange = input => e => {
    this.setState({ [input]: e.target.value });
    //console.log('handleChange');
    //console.log(input);

    if (this.state[input] != '') {
      delete this.state.errormsg[input];
    }

    
    
    // ========== Filter conditions ========== //
    // 1) When pay period select then startdate and enddate disabled
    

    
    // 1) When pay period select then startdate and enddate disabled

    // 2) When start date select then pay period disabled
    
    // 2) When start date select then pay period disabled
    // ========== Filter conditions ========== //
  }

  TableHeaderDesign(){
      // PT false, FT true
    
      if(this.state.FilterEmploymentType == "true")
      {
          var columns = [
            {
              label: 'ID Employee',
              field: 'idEmployee',
              sort: 'asc',
              width: 150
            },
            {
              label: 'Name Employee',
              field: 'nameEmployee',
              sort: 'asc',
              width: 150
            },
            {
              label: 'ID Location',
              field: 'idLocation',
              sort: 'asc',
              width: 150
            },
            {
              label: 'Name Location',
              field: 'nameLocation',
              sort: 'asc',
              width: 150
            },
            {
              label: 'Reg Hours',
              field: 'regHours',
              sort: 'asc',
              width: 150
            },
            /*{
              label: 'Sleep Hours',
              field: 'sleepHours',
              sort: 'asc',
              width: 150
            },*/
            {
              label: 'Straight time',
              field: 'straightTime',
              sort: 'asc',
              width: 150
            },
            {
              label: 'Overtime',
              field: 'overtime',
              sort: 'asc',
              width: 150
            },
            {
              label: 'TRIP',
              field: 'trip',
              sort: 'asc',
              width: 150
            },
            {
              label: 'FTV',
              field: 'ftv',
              sort: 'asc',
              width: 150
            },
            {
              label: 'BTT',
              field: 'btt',
              sort: 'asc',
              width: 150
            },
            {
              label: 'SCK',
              field: 'sck',
              sort: 'asc',
              width: 150
            },
            {
              label: 'ESP',
              field: 'esp',
              sort: 'asc',
              width: 150
            },
            {
              label: 'UNP',
              field: 'unp',
              sort: 'asc',
              width: 150
            },
            {
              label: 'BRV',
              field: 'brv',
              sort: 'asc',
              width: 150
            },
            {
              label: 'On-Call',
              field: 'onCall',
              sort: 'asc',
              width: 150
            },
            /*{
              label: 'On-Call Pay Pref.',
              field: 'onCallPayPref',
              sort: 'asc',
              width: 150
            },*/
            {
              label: 'On-Call STAT',
              field: 'onCallstat',
              sort: 'asc',
              width: 150
            },
            {
              label: 'STAT 2.5',
              field: 'stat25',
              sort: 'asc',
              width: 150
            },
            {
              label: 'STAT 1.5',
              field: 'stat15',
              sort: 'asc',
              width: 150
            },
            {
              label: 'STAT 1.0',
              field: 'stat10',
              sort: 'asc',
              width: 150
            },
            {
              label: 'STAT Pay Pref.',
              field: 'statPayPref',
              sort: 'asc',
              width: 150
            },
            {
              label: 'Other Earnings',
              field: 'otherEarnings',
              sort: 'asc',
              width: 150
            },
            {
              label: 'Other Earnings Value',
              field: 'otherEarningsValue',
              sort: 'asc',
              width: 150
            },
            {
              label: 'On Call -Bank Hours',
              field: 'onCallBankHours',
              sort: 'asc',
              width: 150
            },
            {
              label: 'On Call - Payout',
              field: 'onCallPayout',
              sort: 'asc',
              width: 150
            },
            {
              label: 'On Call - RRSP',
              field: 'onCallRRSP',
              sort: 'asc',
              width: 150
            },
            {
              label: 'STAT - Bank Hours',
              field: 'statBankHours',
              sort: 'asc',
              width: 150
            },
            {
              label: 'STAT - Payout',
              field: 'statPayout',
              sort: 'asc',
              width: 150
            },
            {
              label: 'STAT - RRSP',
              field: 'statRRSP',
              sort: 'asc',
              width: 150
            },
            {
              label: 'COVID-19',
              field: 'covid19',
              sort: 'asc',
              width: 150
            }
          ];

        //this.setState({ header_data: columns });
        return columns;
      }
      else
      {
          var columns = [
            {
              label: 'ID Employee',
              field: 'idEmployee',
              sort: 'asc',
              width: 150
            },
            {
              label: 'Name Employee',
              field: 'nameEmployee',
              sort: 'asc',
              width: 150
            },
            {
              label: 'ID Location',
              field: 'idLocation',
              sort: 'asc',
              width: 150
            },
            {
              label: 'Name Location',
              field: 'nameLocation',
              sort: 'asc',
              width: 150
            },
            {
              label: 'Reg Hours',
              field: 'regHours',
              sort: 'asc',
              width: 150
            },
            {
              label: 'Sleep Hours',
              field: 'sleepHours',
              sort: 'asc',
              width: 150
            },
            {
              label: 'CRIT ILL PAY',
              field: 'CritIllPay',
              sort: 'asc',
              width: 150
            },
            {
              label: 'SCK',
              field: 'sck',
              sort: 'asc',
              width: 150
            },
            {
              label: 'PPT Date',
              field: 'PPTDate',
              sort: 'asc',
              width: 150
            },
            {
              label: 'BRV',
              field: 'brv',
              sort: 'asc',
              width: 150
            },
            {
              label: 'OT HOURS',
              field: 'OTHours',
              sort: 'asc',
              width: 150
            },
            {
              label: 'COVID-19',
              field: 'covid19',
              sort: 'asc',
              width: 150
            },
            {
              label: 'STAT 2.5',
              field: 'stat25',
              sort: 'asc',
              width: 150
            },
            {
              label: 'STAT - RRSP',
              field: 'statRRSP',
              sort: 'asc',
              width: 150
            },
            {
              label: 'STAT - Bank Hours',
              field: 'statBankHours',
              sort: 'asc',
              width: 150
            },
            {
              label: 'STAT - Payout',
              field: 'statPayout',
              sort: 'asc',
              width: 150
            },
            {
              label: 'Public holiday',
              field: 'publicHoliday',
              sort: 'asc',
              width: 150
            },
            {
              label: 'Other Earnings',
              field: 'otherEarnings',
              sort: 'asc',
              width: 150
            },
            {
              label: 'Other Earnings Value',
              field: 'otherEarningsValue',
              sort: 'asc',
              width: 150
            },
            {
              label: 'Total',
              field: 'total',
              sort: 'asc',
              width: 150
            }
            
            
          ];
        //this.setState({ header_data: columns }); 
        return columns; 
      }
    
  }

  componentDidMount() {
    console.log("Reports");

    /* Role Management */
    console.log('Role Store reports_can');
    var getrole = SystemHelpers.GetRole();
    let report_can = getrole.report_can;
    this.setState({ role_reports_can: report_can });
    console.log(report_can);

    console.log('Role Store hierarchy_can');
    var getrole_hierarchy = SystemHelpers.GetRole();
    let hierarchy_can = getrole_hierarchy.hierarchy_can;
    this.setState({ role_hierarchy_can: hierarchy_can });
    console.log(hierarchy_can);
    /* Role Management */
    console.log(localStorage.getItem("token"));
    
    this.GetTimeSheetReportView();
    
    // Table header
    // Table header
  }

  GetTimeSheetReportView(){

    /* Role Management */
      var getrole = SystemHelpers.GetRole();
      console.log('Location Get role');
      console.log(getrole.locations_can);
      //let canDelete = getrole.locations_can.locations_can_delete;
      //let locationscanViewall = getrole.locations_can.locations_can_viewall;
      let hierarchyId = getrole.hierarchy_can.hierarchy_can_id;
    /* Role Management */
    this.showLoader();
    var url=process.env.API_API_URL+'GetTimeSheetReportView?loggedInUserId='+this.state.staffContactID+'&rolePriorityId='+hierarchyId;
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
      //body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
      //console.log("responseJson Report GetTimeSheetReportView");
      //console.log(data);
      
      // debugger;
      if (data.responseType === "1") {

        this.setState({ payPeriodListFilter: data.data.timeSheetPeriodViews});

        if(this.state.role_hierarchy_can.hierarchy_can_id == "1")
        {
          $('#FilterLocation').prop('disabled', true);

          $('#FilterEmployee').prop('disabled', true);
          this.setState({ FilterEmployee: this.state.staffContactID });

          $('#FilterEmploymentType').prop('disabled', true);
          this.setState({ FilterEmploymentType: localStorage.getItem("employmentHoursName") });
          
          
        }
        
        
        
        this.setState({ employmentTypeListFilter: data.data.employeementTypes });
        
        /* ************************************************************ */
        let locationViews_push = [];
        var locationViews = data.data.locationViews;

        let locationtemp = {};
        locationtemp["value"] = "";
        locationtemp["label"] = "All";
        locationViews_push.push(locationtemp);

        for (var zz = 0; zz < locationViews.length; zz++) {
            let locationtemp = {};
            locationtemp["value"] = locationViews[zz].locationId;
            locationtemp["label"] = locationViews[zz].locationName;
            locationViews_push.push(locationtemp);
        }

        this.setState({ LocationListFilter: locationViews_push });
        //this.setState({ locationListFilter: data.data.locationViews });
        /* ************************************************************ */

        /* ************************************************************ */
        let employeeViews_push = [];
        var employeeViews = data.data.employees;

        let employeetemp = {};
        employeetemp["value"] = "";
        employeetemp["label"] = "All";
        employeeViews_push.push(employeetemp);

        for (var zz = 0; zz < employeeViews.length; zz++) {
            let employeetemp = {};
            employeetemp["value"] = employeeViews[zz].contactId;
            employeetemp["label"] = employeeViews[zz].employeeName;
            employeeViews_push.push(employeetemp);
        }

        this.setState({ EmployeeNameListFilter: employeeViews_push });
        //this.setState({ EmployeeNameListFilter: data.data.employees });
        /* ************************************************************ */
        
        
        this.Default_TimeSheet(data.data.timeSheetPeriodViews);
      }else{
        if(data.message == 'Authorization has been denied for this request.'){
          SystemHelpers.SessionOut();
          this.props.history.push("/login");
        }else{
          SystemHelpers.ToastError(data.message);
        }
      }
      this.hideLoader();
    })
    .catch(error => {
      console.log('GetUserWiseTimeSheetData error');
      this.props.history.push("/error-500");
    });
  }
  
  Default_TimeSheet(timeSheetPeriod){
      var length = timeSheetPeriod.length;
      
      if (length > 0) {
        var i = 1;
        for (var zz = 0; zz < length; zz++) {
          if(timeSheetPeriod[zz].isCurrentTimeSheet == true){
            this.setState({ FilterPayPeriod: timeSheetPeriod[zz].timeSheetPeriodId});
          }
          i++;
        }
      }
  }

  
  GetReportListGrid = () => e => {
    
      e.preventDefault();

      this.setState({ ListGrid : [] });
      this.setState({ ExportFilter: {} });
      
      /* Role Management */
      var getrole = SystemHelpers.GetRole();
      /* Role Management */


      let step1Errors = {};
    
      if (this.state["FilterEmploymentType"] =='') {
        step1Errors["FilterEmploymentType"] = "Please select Report Type.";
      }

      this.setState({ errormsg: step1Errors });
        
      if (Object.keys(step1Errors).length > 0) {
          return false;
      }

      var paraPayPeriod = 'periodProfileId='+this.state.FilterPayPeriod;
      var paraLocation = '&locationId='+this.state.FilterLocation;
      var paraEmploymentType = '&employeementType='+this.state.FilterEmploymentType;
      var paraContactId = '&contactId='+this.state.FilterEmployee;
      var paraneedToExport = '&needToExport=false';
      
      //ExportFilter
      let ExportFilterArray = {
        FilterPayPeriod: this.state.FilterPayPeriod,
        FilterLocation: this.state.FilterLocation,
        FilterEmploymentType: this.state.FilterEmploymentType,
        FilterEmployee: this.state.FilterEmployee,
      };
      this.setState({ ExportFilter: ExportFilterArray });
      //ExportFilter

      if(this.state.FilterEmploymentType == "false"){
        var ApiName= 'GetPTBatchPayrollReport?'
      }else{
        var ApiName= 'GetFTBatchPayrollReport?'
      }

      var pass_url = ApiName+paraPayPeriod+paraLocation+paraEmploymentType+paraContactId+paraneedToExport;
      console.log(pass_url);
      //return false;

      this.showLoader();
      var url=process.env.API_API_URL+pass_url;
      fetch(url, {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'bearer '+localStorage.getItem("token")
        },
      }).then((response) => response.json())
      .then(data => {
          console.log("responseJson GetPTBatchPayrollReport ");
          console.log(data);
          if (data.responseType === "1") {
              if(data.data != null)
              {
                //console.log("TableHeaderDesign");
                //console.log(this.TableHeaderDesign());
                this.setState({ header_data: this.TableHeaderDesign() });
                this.setState({ ListGrid: this.rowData(data.data) });
              }
              this.hideLoader();
          }else{
            if(data.message == 'Authorization has been denied for this request.'){
              SystemHelpers.SessionOut();
              this.props.history.push("/login");
            }else{
              SystemHelpers.ToastError(data.message);
            }
            this.hideLoader();    
          }
      })
      .catch(error => {
        console.log(error);
        this.props.history.push("/error-500");
      });
  }

  ExportReportData = () => e => {

      e.preventDefault();
      
      /* Role Management */
      var getrole = SystemHelpers.GetRole();
      let canDelete = getrole.employees_can.employees_can_delete;
      /* Role Management */

      var FilterPara = this.state.ExportFilter;
      console.log('payload GetTimesheetReportDataExport');
      console.log(FilterPara);
      //console.log(FilterPara.FilterPayPeriod);
      //return false;
      
      var paraPayPeriod = 'periodProfileId='+FilterPara.FilterPayPeriod;
      var paraLocation = '&locationId='+FilterPara.FilterLocation;
      var paraEmploymentType = '&employeementType='+FilterPara.FilterEmploymentType;
      var paraContactId = '&contactId='+FilterPara.FilterEmployee;
      var paraneedToExport = '&needToExport=true';

      if(this.state.FilterEmploymentType == "false"){
        var ApiName= 'GetPTBatchPayrollReport?'
      }else{
        var ApiName= 'GetFTBatchPayrollReport?'
      }

      var pass_url = ApiName+paraPayPeriod+paraLocation+paraEmploymentType+paraContactId+paraneedToExport;
      console.log(pass_url);
      //return false;

      this.showLoader();
      var url=process.env.API_API_URL+pass_url;
      fetch(url, {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'bearer '+localStorage.getItem("token")
        },
      }).then((response) => response.json())
      .then(data => {
          //console.log("responseJson ExportReportData Batch payroll");
          //console.log(data);
          if (data.responseType === "1") {

              if(data.data != null)
              {
                //alert();
                var today = new Date();
                var y = today.getFullYear();
                var m = today.getMonth() + 1;
                var d = today.getDate();
                var h = today.getHours();
                var mi = today.getMinutes();
                var s = today.getSeconds();
                var ms = today.getMilliseconds();
                var time = "PHSS000"+y  + m  + d  + h  + mi  + s + ms+FileUploadHelper.randomFun();

                // File name
                var payperiodname = $('#FilterPayPeriod').find(':selected').data('payperiodname');
                
                let firstChar = $.trim(payperiodname.split(" ")[0]);
                let lastChar = $.trim(payperiodname.split(" ").pop());
                
                if(this.state.FilterEmploymentType == "false"){
                  var File_Name= 'PT Payroll Batch '+payperiodname;
                }else{
                  var name_ptfile = moment(firstChar,'DD/MM/YYYY').format(process.env.FTPAYROLLBACTH_DATEFORMAT)+ " to " +moment(lastChar,'DD/MM/YYYY').format(process.env.FTPAYROLLBACTH_DATEFORMAT);
                  var File_Name= 'FT Payroll Batch '+name_ptfile;
                }
                // File name
                
                var createBase64 = "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,"+data.data;
                var ext=File_Name+'.xlsx';
                
                const linkSource = createBase64;
                const downloadLink = document.createElement("a");
                const FileName = ext;

                downloadLink.href = linkSource;
                downloadLink.download = FileName;
                downloadLink.click();

              }
              
              this.hideLoader();
          }else{
            if(data.message == 'Authorization has been denied for this request.'){
              SystemHelpers.SessionOut();
              this.props.history.push("/login");
            }else{
              SystemHelpers.ToastError(data.message);
            }
            this.hideLoader();    
          }
          
          
      })
      .catch(error => {
        this.props.history.push("/error-500");
      });
  }

  ExportReportData_1 = () => e => {
    

      e.preventDefault();

      
      //var payperiodname = $('#FilterPayPeriod').find(':selected').data('payperiodname');
      //let firstChar = payperiodname.split(" ")[0];
      //let lastChar = payperiodname.split(" ").pop();
      //alert(firstChar);
      //alert(lastChar);
      //return false;
      /* Role Management */
      var getrole = SystemHelpers.GetRole();
      let canDelete = getrole.employees_can.employees_can_delete;
      let canViewall = getrole.employees_can.employees_can_viewall
      //console.log('employee getrole');
      //console.log(getrole.employees_can.employees_can_viewall);
      //console.log(getrole);
      /* Role Management */

      var FilterPayPeriod = this.state.FilterPayPeriod;
      var payperiodname = $('#FilterPayPeriod').find(':selected').data('payperiodname');
      
      var FilterLocation = this.state.FilterLocation;
      var locationname = $('#FilterLocation').find(':selected').data('locationname');

      var FilterEmploymentType = this.state.FilterEmploymentType;

      var FilterEmployee = this.state.FilterEmployee;
      var employeename = $('#FilterEmployee').find(':selected').data('employeename');

      console.log('FilterPayPeriod = '+FilterPayPeriod+' FilterPayPeriodName = '+payperiodname);
      console.log('FilterLocation = '+FilterLocation+' FilterLocationName = '+locationname);
      console.log('FilterEmploymentType = '+FilterEmploymentType);
      console.log('FilterEmployeeId = '+FilterEmployee+' FilterEmployeeName = '+employeename);

      var paraPayPeriod = 'periodProfileId='+this.state.FilterPayPeriod;
      var paraLocation = '&locationId='+this.state.FilterLocation;
      var paraEmploymentType = '&employeementType='+this.state.FilterEmploymentType;
      var paraContactId = '&contactId='+this.state.FilterEmployee;
      var paraneedToExport = '&needToExport=true';
      
      //ExportFilter
      let ExportFilterArray = {
        FilterPayPeriod: this.state.FilterPayPeriod,
        FilterLocation: this.state.FilterLocation,
        FilterEmploymentType: this.state.FilterEmploymentType,
        FilterEmployee: this.state.FilterEmployee,
      };
      this.setState({ ExportFilter: ExportFilterArray });
      //ExportFilter

      if(this.state.FilterEmploymentType == "false"){
        var ApiName= 'GetPTBatchPayrollReport?'
      }else{
        var ApiName= 'GetFTBatchPayrollReport?'
      }



      var pass_url = ApiName+paraPayPeriod+paraLocation+paraEmploymentType+paraContactId+paraneedToExport;
      console.log(pass_url);
      //return false;

      this.showLoader();
      var url=process.env.API_API_URL+pass_url;
      fetch(url, {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'bearer '+localStorage.getItem("token")
        },
      }).then((response) => response.json())
      .then(data => {
          console.log("responseJson Excel GetPTBatchPayrollReport");
          //console.log(data);
          if (data.responseType === "1") {
              if(data.data != null)
              {
                //alert();
                var today = new Date();
                var y = today.getFullYear();
                var m = today.getMonth() + 1;
                var d = today.getDate();
                var h = today.getHours();
                var mi = today.getMinutes();
                var s = today.getSeconds();
                var ms = today.getMilliseconds();
                var time = "PHSS000"+y  + m  + d  + h  + mi  + s + ms+FileUploadHelper.randomFun();

                // File name
                var payperiodname = $('#FilterPayPeriod').find(':selected').data('payperiodname');
                
                let firstChar = $.trim(payperiodname.split(" ")[0]);
                let lastChar = $.trim(payperiodname.split(" ").pop());
                
                if(this.state.FilterEmploymentType == "false"){
                  var File_Name= 'PT Payroll Batch '+payperiodname;
                }else{
                  var name_ptfile = moment(firstChar,'DD/MM/YYYY').format(process.env.FTPAYROLLBACTH_DATEFORMAT)+ " to " +moment(lastChar,'DD/MM/YYYY').format(process.env.FTPAYROLLBACTH_DATEFORMAT);
                  var File_Name= 'FT Payroll Batch '+name_ptfile;
                }
                // File name
                
                var createBase64 = "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,"+data.data;
                var ext=File_Name+'.xlsx';
                
                const linkSource = createBase64;
                const downloadLink = document.createElement("a");
                const FileName = ext;

                downloadLink.href = linkSource;
                downloadLink.download = FileName;
                downloadLink.click();

              }
              
              
              this.hideLoader();
          }else{
            if(data.message == 'Authorization has been denied for this request.'){
              SystemHelpers.SessionOut();
              this.props.history.push("/login");
            }else{
              SystemHelpers.ToastError(data.message);
            }
            this.hideLoader();    
          }
          
          
      })
      .catch(error => {
        console.log(error);
        this.props.history.push("/error-500");
      });
  }

  rowData(ListGrid) {
    console.log('rowData Bacth Payroll');
    console.log(ListGrid);

    /* Role Management */
    var getrole = SystemHelpers.GetRole();
    /* Role Management */
    
    var ListGrid_length = ListGrid.length;
    let dataArray = [];
    
    for (var x = 0; x < ListGrid_length; x++)
    {
      if(ListGrid != null)
      {
        
          var tempdataArray = [];

          if(this.state.FilterEmploymentType == "true")
          {
            tempdataArray.idEmployee = ListGrid[x].staffNumberId;
            tempdataArray.nameEmployee = ListGrid[x].employeeName;
            tempdataArray.idLocation = ListGrid[x].locationCode;
            tempdataArray.nameLocation = ListGrid[x].locationName;
            tempdataArray.regHours = ListGrid[x].regHours;
            //tempdataArray.sleepHours = ListGrid[x].sleepHours;
            tempdataArray.straightTime = ListGrid[x].straightTime;
            tempdataArray.overtime = ListGrid[x].overTime;
            tempdataArray.trip = ListGrid[x].trip;
            tempdataArray.ftv = ListGrid[x].ftv;
            tempdataArray.btt = ListGrid[x].btt;
            tempdataArray.sck = ListGrid[x].sck;
            tempdataArray.esp = ListGrid[x].esp;
            tempdataArray.unp = ListGrid[x].unp;
            tempdataArray.brv = ListGrid[x].brv;
            tempdataArray.onCall = ListGrid[x].onCall;
            //tempdataArray.onCallPayPref = ListGrid[x].onCallPayRef;
            tempdataArray.onCallstat = ListGrid[x].onCallStat;
            tempdataArray.stat25 = ListGrid[x].stat25;
            tempdataArray.stat15 = ListGrid[x].stat15;
            tempdataArray.stat10 = ListGrid[x].stat1;
            tempdataArray.statPayPref = ListGrid[x].statPayRef;
            tempdataArray.otherEarnings = ListGrid[x].otherEarnings;
            tempdataArray.otherEarningsValue = ListGrid[x].otherEarningsValue;

            tempdataArray.onCallBankHours = ListGrid[x].onCallBankHoursPayPref;
            tempdataArray.onCallPayout = ListGrid[x].onCallPayOutPayPref;
            tempdataArray.onCallRRSP = ListGrid[x].onCallRRSPPayPref;
            tempdataArray.statBankHours = ListGrid[x].statBankHoursPayPref;
            tempdataArray.statPayout = ListGrid[x].statPayOutPayPref;
            tempdataArray.statRRSP = ListGrid[x].statRRSPPayPref;
            tempdataArray.covid19 = ListGrid[x].covid19Hours;

            dataArray.push(tempdataArray);
          }
          else if(this.state.FilterEmploymentType == "false")
          {
            tempdataArray.idEmployee = ListGrid[x].staffNumberId;
            tempdataArray.nameEmployee = ListGrid[x].employeeName;
            tempdataArray.idLocation = ListGrid[x].locationCode;
            tempdataArray.nameLocation = ListGrid[x].locationName;
            tempdataArray.regHours = ListGrid[x].regHours;
            tempdataArray.sleepHours = ListGrid[x].sleepHours;
            tempdataArray.CritIllPay = ListGrid[x].critIllPay;
            tempdataArray.sck = ListGrid[x].sck;
            tempdataArray.PPTDate = ListGrid[x].pptDate;
            tempdataArray.brv = ListGrid[x].brv;
            tempdataArray.OTHours = ListGrid[x].otHours;
            tempdataArray.covid19 = ListGrid[x].covid19Hours;
            tempdataArray.stat25 = ListGrid[x].stat25;
            tempdataArray.statRRSP = ListGrid[x].statRRSPPayPref;
            tempdataArray.statBankHours = ListGrid[x].statBankHoursPayPref;
            tempdataArray.statPayout = ListGrid[x].statPayOutPayPref;
            
            tempdataArray.publicHoliday = ListGrid[x].publicHolidayPay;
            tempdataArray.otherEarnings = ListGrid[x].otherEarnings;
            tempdataArray.otherEarningsValue = ListGrid[x].otherEarningsValue;
            tempdataArray.total = ListGrid[x].total;

           
            
            

            dataArray.push(tempdataArray);
          }

          
        
      }
    
    }
    //console.log('Return rowdata array');
    //console.log(dataArray);
    return dataArray;
  }

  handleChangeSelectLocation = (selectedOptionLocation) => {
    this.setState({ selectedOptionLocation });
    //console.log(`Option selected:`, selectedOptionLocation.value);
    this.setState({ FilterLocation: selectedOptionLocation.value });
    //LocationFilterID
  };

  handleChangeSelectEmployees = (selectedOptionEmployees) => {
    this.setState({ selectedOptionEmployees });
    //console.log(`Option selected:`, selectedOptionLocation.value);
    this.setState({ FilterEmployee: selectedOptionEmployees.value });
    //LocationFilterID
  };

  render() {

    const data = {
      columns: this.state.header_data,
      rows: this.state.ListGrid
    };

    const { selectedOptionLocation,selectedOptionEmployees  } = this.state;

    return ( 
      <div className="main-wrapper">
     
        {/* Toast & Loder method use */}
          
        {(this.state.loading) ? <Loader /> : null} 
        {/* Toast & Loder method use */}
        <Header/>

        <div className="page-wrapper">
            <Helmet>
                <title>{process.env.WEB_TITLE}</title>
                <meta name="description" content="Login page"/>         
            </Helmet>
              {/* Page Content */}
              <div className="content container-fluid">
                {/* Page Header */}
                <div className="page-header">
                  <div className="row align-items-center">
                    <div className="col">
                      <h3 className="page-title">Reports</h3>
                      <ul className="breadcrumb">
                        <li className="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                        <li className="breadcrumb-item active">Batch Payroll Reports</li>
                      </ul>
                    </div>
                    <div className="col-auto float-right ml-auto">
                      
                    </div>
                  </div>
                </div>

                {/* Search Filter */}
                  <div className="row filter-row">

                    <div className="col-lg-2 col-sm-3 col-xs-12"> 
                      <div className="form-group form-focus select-focus">
                        <select className="form-control floating" id="FilterEmploymentType" value={this.state.FilterEmploymentType} onChange={this.handleChange('FilterEmploymentType')}> 
                          <option value="">-</option>
                          <option value="false">PT</option>
                          <option value="true">FT</option>
                        </select>
                        <label className="focus-label">Report Type</label>
                        <span className="form-text error-font-color">{this.state.errormsg["FilterEmploymentType"]}</span>
                      </div>
                    </div>
                    
                    <div className="col-lg-3 col-sm-3 col-xs-12"> 
                      <div className="form-group form-focus select-focus">
                        <select className="form-control floating" id="FilterPayPeriod" value={this.state.FilterPayPeriod} onChange={this.handleChange('FilterPayPeriod')}> 
                          <option value="" data-payperiodname="" >-</option>
                          {this.state.payPeriodListFilter.map(( listValue, index ) => {
                              var payPeriodName = moment(listValue.timeSheetPeriodStartDate,process.env.API_DATE_FORMAT).format(process.env.DATE_FORMAT)+ " to " +moment(listValue.timeSheetPeriodEndDate,process.env.API_DATE_FORMAT).format(process.env.DATE_FORMAT);
                              return (
                                <option key={index} value={listValue.timeSheetPeriodId} data-payperiodname={payPeriodName} >{moment(listValue.timeSheetPeriodStartDate,process.env.API_DATE_FORMAT).format(process.env.DATE_FORMAT)} to {moment(listValue.timeSheetPeriodEndDate,process.env.API_DATE_FORMAT).format(process.env.DATE_FORMAT)}</option>
                              );
                           
                          })}
                        </select>
                        <label className="focus-label">Pay period</label>
                      </div>
                    </div>
                    <div className="col-lg-3 col-sm-3 col-xs-12"> 
                      <div className="form-group form-focus select-focus">
                        {/*<select className="form-control floating" id="FilterLocation" value={this.state.FilterLocation} onChange={this.handleChange('FilterLocation')}> 
                          <option value="" data-locationname="">All</option>
                          {this.state.locationListFilter.map(( listValue, index ) => {
                           
                              return (
                                <option key={index} value={listValue.locationId} data-locationname={listValue.locationName} >{listValue.locationName}</option>
                              );
                           
                          })}
                        </select>*/}
                        <Select
                          value={selectedOptionLocation}
                          onChange={this.handleChangeSelectLocation}
                          options={this.state.LocationListFilter}
                          defaultValue={""}
                          isSearchable={true}
                          placeholder="Search Location"
                        />
                        {/*<label className="focus-label">Location</label>*/}
                      </div>
                    </div>
                    
                    {/*<div className="col-sm-6 col-md-2"> 
                      <div className="form-group form-focus select-focus">
                        <select className="form-control floating" id="FilterEmploymentType" value={this.state.FilterEmploymentType} onChange={this.handleChange('FilterEmploymentType')}> 
                          <option value="">All</option>
                          {this.state.employmentTypeListFilter.map(( listValue, index ) => {
                           
                              return (
                                <option key={index} value={listValue.name} >{listValue.name}</option>
                              );
                           
                          })}
                        </select>
                        <label className="focus-label">Employment Type</label>
                      </div>
                    </div>*/}

                    <div className="col-lg-2 col-sm-3 col-xs-12"> 
                      <div className="form-group form-focus select-focus">
                        {/*<input className="form-control" type="text" id="FilterEmployeeName" value={this.state.FilterEmployeeName}  onChange={this.handleChange('FilterEmployeeName')} />*/}
                        {/*<select className="form-control" id="FilterEmployee" value={this.state.FilterEmployee}  onChange={this.handleChange('FilterEmployee')} >
                          <option value='' data-employeename="" >-</option>
                          {this.state.EmployeeNameListFilter.map(( listValue, index ) => {
                            return (
                              <option key={index}  value={listValue.contactId} data-employeename={listValue.employeeName} >{listValue.employeeName}</option>
                            );
                          })}
                        </select>*/}
                        <Select
                          value={selectedOptionEmployees}
                          onChange={this.handleChangeSelectEmployees}
                          options={this.state.EmployeeNameListFilter}
                          defaultValue={""}
                          isSearchable={true}
                          placeholder="Search Employee"
                        />
                        {/*<label className="focus-label">Employee name</label>*/}
                      </div>
                    </div>
                    
                    <div className="col-lg-2 col-sm-12 col-xs-12">  
                      <a href="#" className="col-lg-12 col-sm-12 col-xs-12 float-right btn btn-success btn-block" onClick={this.GetReportListGrid()}> Search </a>  
                    </div> 

                  </div>
                {/* /Search Filter */}    
                {/* /Page Header */}

                {this.state.ListGrid.length > 0 ?
                  <div className="row">
                      <div className="col-sm-12">  
                        <a href="#" className="btn btn-danger mr-1 float-right" onClick={this.ExportReportData()}> Export EXCEL </a>  
                        {/*<a href="#" className="btn btn-danger mr-1 float-right" onClick={this.ExportReportData('PDF')}> Export PDF </a>  */}
                      </div>
                  </div>
                  : null
                }
                <div className="row">
                  <div className="col-md-12">
                      <div className="table-responsive">
                      
               
                        {this.state.ListGrid.length > 0 ?
                          <MDBDataTable
                            striped
                            bordered
                            small
                            data={data}
                            entriesOptions={[10, 20, 50, 100]} entries={10} pagesAmount={10}
                            className="table table-striped custom-table mb-0 datatable"
                          />
                          : null
                        }
                     

                    </div>
                  </div>
                </div>
              </div>
              {/* /Page Content */}

              {/* Add Reports Modal */}
              {/* /Add Reports Modal */}

              {/* Edit Reports Modal */}
              {/* /Edit Reports Modal */}

              {/* Delete Today Work Modal */}
              {/* Delete Today Work Modal */}
            </div>
          <SidebarContent/>
      </div>
        );
      
   }
}

export default BacthPayrollReports;
