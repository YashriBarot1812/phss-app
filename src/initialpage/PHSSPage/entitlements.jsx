
import React, { Component } from 'react';
import { Helmet } from "react-helmet";
import {  Avatar_01 ,Avatar_02,Avatar_03,Avatar_04, Avatar_05, Avatar_08, Avatar_09, Avatar_10,
    Avatar_11,Avatar_12,Avatar_13,Avatar_16   } from "../../Entryfile/imagepath"

import { Table } from 'antd';
import 'antd/dist/antd.css';
import {itemRender,onShowSizeChange} from "./pagination/paginationfunction"
import "../MainPage/antdstyle.css"
import { Multiselect } from 'multiselect-react-dropdown';

import Select from 'react-select-me';
import 'react-select-me/lib/ReactSelectMe.css';

import Header from '../Sidebar/header.jsx';
import SidebarContent from '../Sidebar/sidebar';


import Loader from '../Loader';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import SystemHelpers from '../Helpers/SystemHelper';

//table
import { MDBDataTable } from 'mdbreact';
import '@fortawesome/fontawesome-free/css/all.min.css';
import 'bootstrap-css-only/css/bootstrap.min.css';
import 'mdbreact/dist/css/mdb.css';
//table

import CryptoAES from 'crypto-js/aes';
import CryptoENC from 'crypto-js/enc-utf8';

import PhoneInput from 'react-phone-input-2';

import Datetime from "react-datetime";
import moment from 'moment';

class Entitlements extends Component {
  constructor(props) {
    super(props);
    this.state = {

        // Pagination
        totalCount : 0,
        pageSize : 5,
        currentPage : 1,
        totalPages : 0,
        previousPage : false,
        nextPage : false,
        searchText : '',
        pagingData : {},
        TempsearchText:'',

        sortColumn : '',
        SortType : false,
        IsSortingEnabled : true,
        // Pagination

        // Search Entitlement View
        sortColumnEntHistory : 'transactionDate',
        SortTypeIdEntHistory : false,
        // Search Entitlement View

        errormsg :  '',
        
        role_entitlement_menu_can : {},

        ListGrid: [],
        HistoryListGrid: [],

        staffContactID:localStorage.getItem("contactId"),
        staffContactName:localStorage.getItem("fullName"),

        DesignEqual : "=",
        DesignNotequal : "!=",
        
        SearchEntitlementYearList : [],
        SearchEntitlementLocationList : [],
        SearchEntitlementRoleList : [],
        SearchEntitlementEmployeementHoursList : [],

        EntitlementYearList : [],
        EntitlementCategoryList : [],
        EntitlementSubCategoryList : [],
        EntitlementEmployeementHoursList : [],
        EntitlementLocationList : [],

        HistoryYearList : [],
        
        // Current Year
        //PastCarryForwardYearEntitlement :moment().subtract(1, 'year').format(process.env.ENTITLEMENTS_DATE_FORMAT),
        // Current Year

        // Search
        //SearchentitlementYear : 'd7d41b63-5df4-eb11-94ef-000d3a3e39d6',
        SearchentitlementYear : localStorage.getItem('LocalStorageCurrentYearGuid'),
        SearchentitlementLocation : '',
        SearchentitlementRole : '',
        SearchentitlementEmploymenthours : '',
        // Search

        hierarchyId:'',
        page_wrapper_cls:'',
        unauthorized_cls:'',

        entitlementId:'',

        // Add
        AddentitlementYear : '',
        AddentitlementCategory : '',
        AddentitlementSubCategory : '',
        AddentitlementAction : '',
        AddentitlementQty : '',
        AddentitlementQtydays : '',
        AddentitlementQtyhours : '',
        AddentitlementQtyminutes : '',
        AddentitlementUnit : '',
        AddentitlementDescription : '',

        AddentitlementAppliesTos : [],


        AddFieldErrorMsg : '',

        AddEmploymentHoursFieldCheckbox : false,
        AddApplyEmploymentHoursField : 'Employment hours',
        AddApplyEmploymentHoursOperator : '',
        AddApplyEmploymentHoursValue : '',

        AddLocationFieldCheckbox : false,
        AddApplyLocationField : 'Primary Location',
        AddApplyLocationOperator : '',
        AddApplyLocationValue : localStorage.getItem("primaryLocationGuid"),

        AddContactFieldCheckbox : false,
        AddApplyContactField : 'Contact',
        AddApplyContactOperator : '',
        AddApplyContactValue : '',
        // Add

        // History 
        HistoryemployeeName : '',
        HistoryemployeementHours : '',
        HistoryhireDate : '',
        HistorynoOfHours : '',

        HistoryYear : '',

        HistoryEntitlementsCurrentYear : '',
        HistoryEntitlementsCurrentVacation : '',
        HistoryEntitlementsCurrentSickTime : '',

        HistoryEntitlementsCarryYear : '',
        HistoryEntitlementsCarryVacation : '',
        HistoryEntitlementsCarryOverTime : '',

        HistoryContactId : '',
        // History

        isDelete : false,

        IsViewHistoryContactApplyAddRecord : false,

    };
    this.setPropState = this.setPropState.bind(this);
    this.handleChange = this.handleChange.bind(this);
  
    this.handleEntitlementQtyAdd = this.handleEntitlementQtyAdd.bind(this);      
  }

  setPropState(key, value) {
      this.setState({ [key]: value });
  }

  handleChange = input => e => {
    this.setState({ [input]: e.target.value });
    if (this.state[input] != '') {
      delete this.state.errormsg[input];
    }

    // Pagination
    if([input]=="pageSize")
    {
      this.setState({ current_page: 1 });
      this.GetUsersEntitlementBalanceView(1,e.target.value,this.state.searchText)
    }
    // Pagination

    // Add time
    if([input]=="AddentitlementCategory")
    {
      this.setState({ EntitlementSubCategoryList: [] });
    
      this.GetEntitlementSubCategoryView(e.target.value);
      this.setState({ AddentitlementSubCategory: '' });
    }

    if([input]=="AddEmploymentHoursFieldCheckbox" && $('#AddEmploymentHoursFieldCheckbox').is(":checked"))
    {
      this.setState({ AddEmploymentHoursFieldCheckbox: true });
    }
    else if([input]=="AddEmploymentHoursFieldCheckbox")
    {
      this.setState({ AddEmploymentHoursFieldCheckbox: false });
    }

    if([input]=="AddLocationFieldCheckbox" && $('#AddLocationFieldCheckbox').is(":checked"))
    {
      this.setState({ AddLocationFieldCheckbox: true });
    }
    else if([input]=="AddLocationFieldCheckbox" )
    {
      this.setState({ AddLocationFieldCheckbox: false });
    }


    if([input]=="AddApplyEmploymentHoursValue")
    {
      if(e.target.value == "All"){
        this.setState({ AddApplyEmploymentHoursOperator: "Equal" });
        $('#AddApplyEmploymentHoursOperator').prop('disabled', true);
        $('#AddApplyEmploymentHoursOperator').css('background-color', '#e3e3e3');
      }else{
        //this.setState({ AddApplyEmploymentHoursOperator: "" });
        $('#AddApplyEmploymentHoursOperator').prop('disabled', false);
        $('#AddApplyEmploymentHoursOperator').css('background-color', '#fff');
      }
    }
    
    // Add time

    // History time Year Change event
    if([input]=="HistoryYear")
    {
      this.setState({ HistoryYear : e.target.value });
      //console.log("HistoryYear 123="+e.target.value);
      var year = e.target.value;
      //alert(e.target.value);
      this.HistoryChangeViewRecord(year);
    }
    // History time Year Change event

    // Add Time COVID 19 Hours and Minutes Hide
    if([input]=="AddentitlementSubCategory")
    {
      if(e.target.value == process.env.API_ENTITLEMENT_IDEL_COVID19)
      {
        $("#id_days_div").show();

        $("#id_hours_div").hide();
        $("#id_minutes_div").hide();
        this.setState({ AddentitlementQtyhours: '' });
        this.setState({ AddentitlementQtyminutes: '' });
        this.setState({ AddentitlementUnit: 'days' });
      }
      else
      {
        $("#id_hours_div").show();
        $("#id_minutes_div").show();

        $("#id_days_div").hide();
        this.setState({ AddentitlementQtyhours: '' });
        this.setState({ AddentitlementUnit: 'hours' });
      }
    }
    // Add Time COVID 19 Hours and Minutes Hide
  }

  // Loader hide show method
  hideLoader = () => {
    this.setState({ loading: false });
  }

  showLoader = () => {
    this.setState({ loading: true });
  }
  // Loader hide show method
  
  handleEntitlementQtyAdd = (date) =>{
    //alert(date);
    //console.log('date');
    //console.log(date);
    //console.log(moment(new Date(date)).format('YYYY-MM-DD HH:mm:ss a'));
    this.setState({ AddentitlementQty: moment(new Date(date)).format('HH:mm') });
  };

  componentDidMount() {
    
    
    /* Role Management */
    console.log('Role Store entitlement_menu_can');
    var getrole = SystemHelpers.GetRole();
    let entitlement_menu_can = getrole.entitlement_menu_can;
    this.setState({ role_entitlement_menu_can: entitlement_menu_can });
    console.log(entitlement_menu_can);
    /* Role Management */

    // Check hierarchy
    let hierarchyId = getrole.hierarchy_can.hierarchy_can_id;
    console.log("GetUsersEntitlementBalanceView Entitlement = " + hierarchyId);
    this.setState({ hierarchyId: hierarchyId });

    if(hierarchyId == 1){
      this.setState({ page_wrapper_cls: 'page-wrapper pk-page-wrapper-remove hide-div' });
      this.setState({ unauthorized_cls: 'error-box pk-margin-top' });

      setTimeout(() => {
        $('.pk-page-wrapper-remove').remove()
      }, 5000);
      
    }else{
      this.setState({ page_wrapper_cls: 'page-wrapper' });
      this.setState({ unauthorized_cls: 'error-box hide-div' });
    }
    // Check hierarchy

    //console.log("Getentitlement");
    
    this.GetEntitlementCategoryView();
    this.GetEmployeementHours();
    this.GetPhssYears();
    this.GetRoleWiseAssignedLocations();
    this.GetTimeSheetReportView();

    //this.GetUsersEntitlementBalanceView();
    this.GetUsersEntitlementBalanceView(this.state.currentPage,this.state.pageSize,this.state.searchText);

    $("#id_days_div").hide();
  }

  // Drop down Api
  GetEntitlementCategoryView(){
    this.showLoader();
    var url=process.env.API_API_URL+'GetEntitlementCategoryView';
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
    }).then((response) => response.json())
    .then(data => {
        //console.log("responseJson GetEntitlementCategoryView");
        //console.log(data);
        if (data.responseType === "1") {
          this.setState({ EntitlementCategoryList: data.data});
        }else{
          SystemHelpers.ToastError(data.message);   
        }
        this.hideLoader();
    })
    .catch(error => {
      this.props.history.push("/error-500");
    });
  }

  GetEntitlementSubCategoryView(id){
    this.showLoader();
    var url=process.env.API_API_URL+'GetEntitlementSubCategoryView?categoryId='+id;
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
    }).then((response) => response.json())
    .then(data => {
        //console.log("responseJson GetEntitlementSubCategoryView");
        //console.log(data);
        if (data.responseType === "1") {
          this.setState({ EntitlementSubCategoryList: data.data});
        }else{
          SystemHelpers.ToastError(data.message);   
        }
        this.hideLoader();
        
    })
    .catch(error => {
      this.props.history.push("/error-500");
    });
  }

  GetEmployeementHours(id){
    this.showLoader();
    var url=process.env.API_API_URL+'GetEmployeementHours?categoryId='+id;
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
    }).then((response) => response.json())
    .then(data => {
        //console.log("responseJson GetEmployeementHours");
        //console.log(data);
        if (data.responseType === "1") {
          this.setState({ EntitlementEmployeementHoursList: data.data});
        }else{
          SystemHelpers.ToastError(data.message);   
        }
        this.hideLoader();
        
    })
    .catch(error => {
      this.props.history.push("/error-500");
    });
  }

  GetPhssYears(){
    this.showLoader();
    var url=process.env.API_API_URL+'GetPhssYears';
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
    }).then((response) => response.json())
    .then(data => {
        //console.log("responseJson GetPhssYears");
        //console.log(data);
        if (data.responseType === "1") {
          this.setState({ EntitlementYearList: data.data});
          this.setState({ SearchEntitlementYearList: data.data});
          this.setState({ HistoryYearList: data.data});
          
        }else{
          SystemHelpers.ToastError(data.message);   
        }
        this.hideLoader();
    })
    .catch(error => {
      this.props.history.push("/error-500");
    });
  }

  GetRoleWiseAssignedLocations(){
    this.showLoader();

     /* Role Management */
    var getrole = SystemHelpers.GetRole();
    //console.log('Location Get role Entitlement');
    //console.log(getrole.entitlement_can);
    //let canDelete = getrole.locations_can.locations_can_delete;
    //let locationscanViewall = getrole.locations_can.locations_can_viewall;
    let hierarchyId = getrole.hierarchy_can.hierarchy_can_id;
    /* Role Management */

    var url=process.env.API_API_URL+'GetRoleWiseAssignedLocations?rolePriorityId='+hierarchyId+'&loggedInUserId='+this.state.staffContactID;
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
    }).then((response) => response.json())
    .then(data => {
        //console.log("responseJson GetRoleWiseAssignedLocations");
        //console.log(data);
        if (data.responseType === "1") {
          this.setState({ SearchEntitlementLocationList: data.data});
          this.setState({ EntitlementLocationList: data.data});
        }else{
          SystemHelpers.ToastError(data.message);   
        }
        this.hideLoader();
    })
    .catch(error => {
      this.props.history.push("/error-500");
    });
  }

  GetTimeSheetReportView(){

    /* Role Management */
    var getrole = SystemHelpers.GetRole();
    //console.log('Location Get role');
    //console.log(getrole.locations_can);
    //let canDelete = getrole.locations_can.locations_can_delete;
    //let locationscanViewall = getrole.locations_can.locations_can_viewall;
    let hierarchyId = getrole.hierarchy_can.hierarchy_can_id;
    /* Role Management */

    this.showLoader();
    var url=process.env.API_API_URL+'GetTimeSheetReportView?loggedInUserId='+this.state.staffContactID+'&rolePriorityId='+hierarchyId;
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
    }).then((response) => response.json())
    .then(data => {
      //console.log("responseJson Entitlement GetTimeSheetReportView");
      //console.log(data);
      
      // debugger;
      if (data.responseType === "1") {
        this.setState({ SearchEntitlementRoleList: data.data.roleList });
        this.setState({ SearchEntitlementEmployeementHoursList: data.data.employeementTypes });
      }else{
        if(data.message == 'Authorization has been denied for this request.'){
          SystemHelpers.SessionOut();
          this.props.history.push("/login");
        }else{
          SystemHelpers.ToastError(data.message);
        }
      }
      this.hideLoader();
    })
    .catch(error => {
      console.log('GetUserWiseTimeSheetData error');
      this.props.history.push("/error-500");
    });
  }

  // Drop down Api

  ClearRecord = ()=> e => {
    e.preventDefault();

    this.setState({ AddentitlementYear: '' });
    this.setState({ AddentitlementCategory: '' });
    this.setState({ AddentitlementSubCategory: '' });
    this.setState({ AddentitlementAction: '' });
    this.setState({ AddentitlementQty: '' });
    this.setState({ AddentitlementQtyhours: '' });
    this.setState({ AddentitlementQtyminutes: '' });
    this.setState({ AddentitlementDescription: '' });

    this.setState({ AddApplyEmploymentHoursOperator: '' });
    ///this.setState({ AddApplyEmploymentHoursField: '' });
    this.setState({ AddApplyEmploymentHoursValue: '' });

    this.setState({ AddApplyLocationOperator: '' });
    //this.setState({ AddApplyLocationField: '' });
    this.setState({ AddApplyLocationValue: '' });

    $('input[type="checkbox"]').prop('checked', false);


    this.setState({ IsViewHistoryContactApplyAddRecord: false });
    
    this.setState({ errormsg: '' });
  }

  AddRecord = () => e => {
      e.preventDefault();

      let step1Errors = {};

      //alert(this.state["AddentitlementQtyminutes"]);

      if (this.state["AddentitlementYear"] == '' || this.state["AddentitlementYear"] == null) {
        step1Errors["AddentitlementYear"] = "Year is mandatory";
      }

      if (this.state["AddentitlementCategory"] == '' || this.state["AddentitlementCategory"] == null) {
        step1Errors["AddentitlementCategory"] = "Entitlement Category is mandatory";
      } 
      
      if (this.state["AddentitlementAction"] == '' || this.state["AddentitlementAction"] == null) {
        step1Errors["AddentitlementAction"] = "Action is mandatory";
      }

      /*if (this.state["AddentitlementQty"] == '' || this.state["AddentitlementQty"] == null) {
        step1Errors["AddentitlementQty"] = "Qty is mandatory";
      }*/

      if(this.state["AddentitlementSubCategory"] == process.env.API_ENTITLEMENT_IDEL_COVID19)
      {
        if (this.state["AddentitlementQtydays"] == '' || this.state["AddentitlementQtydays"] == null) {
          step1Errors["AddentitlementQtydays"] = "Days is mandatory";
        }
      }
      else
      {
        if (this.state["AddentitlementQtyhours"] == '' || this.state["AddentitlementQtyhours"] == null) {
          step1Errors["AddentitlementQtyhours"] = "Hours is mandatory";
        }
        if (this.state["AddentitlementQtyminutes"] == '' || this.state["AddentitlementQtyminutes"] == null) {
          step1Errors["AddentitlementQtyminutes"] = "Minutes is mandatory";
        }
      }

      /*if (this.state["AddentitlementUnit"] == '' || this.state["AddentitlementUnit"] == null) {
        step1Errors["AddentitlementUnit"] = "Unit is mandatory";
      }*/

      if (this.state["AddentitlementSubCategory"] == '' || this.state["AddentitlementSubCategory"] == null) {
        step1Errors["AddentitlementSubCategory"] = "Reason is mandatory";
      }

      if (this.state["AddentitlementDescription"] == '' || this.state["AddentitlementDescription"] == null || this.state["AddentitlementDescription"].trim() == '') {
        step1Errors["AddentitlementDescription"] = "Description is mandatory";
      }

      if(this.state.IsViewHistoryContactApplyAddRecord == false)
      {
        if(this.state.AddEmploymentHoursFieldCheckbox == false && this.state.AddLocationFieldCheckbox == false)
        {
          step1Errors["AddFieldErrorMsg"] = "Atleast one option from Applies to section should be selected.";
        }
      }

      if ($('#AddEmploymentHoursFieldCheckbox').is(":checked")) {
        //alert('123');
        if (this.state["AddApplyEmploymentHoursOperator"] == '' || this.state["AddApplyEmploymentHoursOperator"] == null) {
        step1Errors["AddApplyEmploymentHoursOperator"] = "Operator is mandatory";
        }

        if (this.state["AddApplyEmploymentHoursValue"] == '' || this.state["AddApplyEmploymentHoursValue"] == null) {
          step1Errors["AddApplyEmploymentHoursValue"] = "Field Selection is mandatory";
        }
      } else {
        //alert('111');
      }

      if ($('#AddLocationFieldCheckbox').is(":checked")) {
        //alert('123');
        if (this.state["AddApplyLocationOperator"] == '' || this.state["AddApplyLocationOperator"] == null) {
        step1Errors["AddApplyLocationOperator"] = "Operator is mandatory";
        }

        if (this.state["AddApplyLocationValue"] == '' || this.state["AddApplyLocationValue"] == null) {
          step1Errors["AddApplyLocationValue"] = "Field Selection is mandatory";
        }
      } else {
        //alert('111');
      }

      
      //console.log(step1Errors);

      this.setState({ errormsg: step1Errors });
          
      if (Object.keys(step1Errors).length > 0) {
        return false;
      }

      this.showLoader();
      
      let bodyarray = {};
      //bodyarray["contactId"] = this.state.staffContactID;
      bodyarray["loggedInUserId"] = this.state.staffContactID;
      bodyarray["yearId"] = this.state["AddentitlementYear"];
      bodyarray["entitlementCategoryId"] = this.state["AddentitlementCategory"];
      bodyarray["entitlementAction"] = this.state["AddentitlementAction"];
      //bodyarray["entitlementQty"] = this.state["AddentitlementQty"];

      if(this.state["AddentitlementSubCategory"] == process.env.API_ENTITLEMENT_IDEL_COVID19)
      {
        bodyarray["entitlementQty"] = this.state["AddentitlementQtydays"]+':';
      }
      else
      {
        bodyarray["entitlementQty"] = this.state["AddentitlementQtyhours"]+':'+this.state["AddentitlementQtyminutes"];
      }

      bodyarray["entitlementSubCategoryId"] = this.state["AddentitlementSubCategory"];
      bodyarray["entitlementDescription"] = this.state["AddentitlementDescription"];

      if(this.state.IsViewHistoryContactApplyAddRecord == false)
      {
        //alert(this.state.AddEmploymentHoursFieldCheckbox);
        //alert(this.state.AddLocationFieldCheckbox);
        

        var ApplyEmploymentHoursOperator = "";
        if(this.state["AddApplyEmploymentHoursOperator"]=="Equal" ){
          ApplyEmploymentHoursOperator = this.state.DesignEqual;
        }else if(this.state["AddApplyEmploymentHoursOperator"]=="Not equal"){
          ApplyEmploymentHoursOperator = this.state.DesignNotequal;
        }
        
        var ApplyLocationOperator = "";
        if(this.state["AddApplyLocationOperator"]=="Equal" ){
          ApplyLocationOperator = this.state.DesignEqual;
        }else if(this.state["AddApplyLocationOperator"]=="Not equal"){
          ApplyLocationOperator = this.state.DesignNotequal;
        }
        
        var ApplyEmploymentHoursValue = "";
        if(this.state["AddApplyEmploymentHoursValue"] == "All"){
          ApplyEmploymentHoursValue = null;
        }else{
          ApplyEmploymentHoursValue = this.state["AddApplyEmploymentHoursValue"];
        }

        if (this.state.AddEmploymentHoursFieldCheckbox == true && this.state.AddLocationFieldCheckbox == false ) {
          let ArrayJson=[{
            appliesToFieldId : "EH",
            operator : ApplyEmploymentHoursOperator,
            //appliesToFieldValue : this.state["AddApplyEmploymentHoursValue"],
            appliesToFieldValue : ApplyEmploymentHoursValue,
          }];
         
          bodyarray["entitlementAppliesTos"] = ArrayJson;
        }else if(this.state.AddLocationFieldCheckbox == true && this.state.AddEmploymentHoursFieldCheckbox == false) {
          let ArrayJson=[{
            appliesToFieldId : "L",
            operator : ApplyLocationOperator,
            //appliesToFieldValue : localStorage.getItem("primaryLocationGuid"),
            appliesToFieldValue : this.state["AddApplyLocationValue"],
          }];

          bodyarray["entitlementAppliesTos"] = ArrayJson;
        }else if(this.state.AddEmploymentHoursFieldCheckbox == true && this.state.AddLocationFieldCheckbox == true ){
          
          let ArrayJson=[{
            appliesToFieldId : "EH",
            operator : ApplyEmploymentHoursOperator,
            //appliesToFieldValue : this.state["AddApplyEmploymentHoursValue"],
            appliesToFieldValue : ApplyEmploymentHoursValue,
          },{
            appliesToFieldId : "L",
            operator : ApplyLocationOperator,
            //appliesToFieldValue : localStorage.getItem("primaryLocationGuid"),
            appliesToFieldValue : this.state["AddApplyLocationValue"],
          }];

          bodyarray["entitlementAppliesTos"] = ArrayJson; 
        }
      }
      else if(this.state.IsViewHistoryContactApplyAddRecord == true)
      {
        let ArrayJson=[{
          appliesToFieldId : this.state.AddApplyContactField,
          operator : this.state.AddApplyContactOperator,
          appliesToFieldValue : this.state.AddApplyContactValue,
        }];
       
        bodyarray["entitlementAppliesTos"] = ArrayJson;
      }
      
      
      //console.log(ArrayJson);
      //console.log('PostEntitlement');
      //console.log(bodyarray);
      //return false;
      var url=process.env.API_API_URL+'PostEntitlement';
      fetch(url, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'bearer '+localStorage.getItem("token")
        },
        body: JSON.stringify(bodyarray)
      }).then((response) => response.json())
      .then(data => {
          //console.log("responseJson PostEntitlement");
          //console.log(data);
          if (data.responseType === "1") {
            SystemHelpers.ToastSuccess(data.responseMessge);  
            $( ".close" ).trigger( "click" ); 
            this.ClearRecord();
            this.setState({ IsViewHistoryContactApplyAddRecord: false });
            //this.GetUsersEntitlementBalanceView();
            this.GetUsersEntitlementBalanceView(this.state.currentPage,this.state.pageSize,this.state.searchText);
            
            
          }else if (data.responseType === "2" || data.responseType === "3" || data.responseType === "4") {
            SystemHelpers.ToastError(data.responseMessge);  
          } else{
            SystemHelpers.ToastError(data.message);
          }
          this.hideLoader();
      })
      .catch(error => {
        this.props.history.push("/error-500");
      });
  }

  GetUsersEntitlementBalanceView (currentPage,pageSize,searchText) {
      
      this.setState({ ListGrid : [] });

      // Pagination
      let bodyarray = {};
      bodyarray["currentPage"] = 1;
      bodyarray["nextPage"] = false;
      bodyarray["pageSize"] = 5;
      bodyarray["previousPage"] = false;
      bodyarray["totalCount"] = 0;
      bodyarray["totalPages"] = 0;
      
      this.setState({ pagingData : bodyarray });

      this.setState({ currentPage: currentPage });
      this.setState({ pageSize: pageSize });

      var sort_Column = this.state.sortColumn;
      var Sort_Type = this.state.SortType;
      
      var IsSortingEnabled = true;

      var url_paging_para = '&pageNumber='+currentPage+'&pageSize='+pageSize+'&searchText='+searchText+'&sortColumn='+sort_Column+'&SortType='+Sort_Type+'&IsSortingEnabled='+IsSortingEnabled;
      // Pagination

      /* Role Management */
      var getrole = SystemHelpers.GetRole();
      //console.log('Schedule Get role');
      //console.log(getrole.entitlement_can);
      
      //let canViewall = getrole.entitlement_can.entitlement_can_viewall;
      /* Role Management */

      var SearchYear = this.state.SearchentitlementYear;
      //alert('GetUsersEntitlementBalanceView');
      //alert(SearchYear);
      var SearchLocation = this.state["SearchentitlementLocation"]
      var SearchRole = this.state["SearchentitlementRole"]
      var SearchEmploymenthours = this.state["SearchentitlementEmploymenthours"]

      this.showLoader();
      var url_para = '?yearId='+SearchYear+'&locationId='+SearchLocation+'&roleId='+SearchRole+'&employeementHours='+SearchEmploymenthours;
      var url=process.env.API_API_URL+'GetUsersEntitlementBalanceView'+url_para+url_paging_para;
      fetch(url, {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'bearer '+localStorage.getItem("token")
        },
      }).then((response) => response.json())
      .then(data => {
          console.log("responseJson GetUsersEntitlementBalanceView");
          console.log(data);
          //console.log(data.data.sceduleDetails);
          if (data.responseType === "1") {
              //this.setState({ ListGrid: this.rowData(data.data) });
              this.setState({ ListGrid: data.data });
              this.setState({ pagingData: data.pagingData });
              this.hideLoader();
          }else{
            if(data.message == 'Authorization has been denied for this request.'){
              SystemHelpers.SessionOut();
              this.props.history.push("/login");
            }else{
              SystemHelpers.ToastError(data.message);
            }
            this.hideLoader();    
          }
      })
      .catch(error => {
        this.props.history.push("/error-500");
      });
  }

  

  Edit_Update_Btn_Func(record){
    let return_push = [];

    if(this.state.role_entitlement_menu_can.entitlement_menu_can_create == true){
        
        

        let View_push = [];
        if(this.state.role_entitlement_menu_can.entitlement_menu_can_view == true){
          var url ="/entitlements/"+record.contactId;
          View_push.push(
          <a href={url} onClick={this.ViewRecord(record.contactId)}  className="dropdown-item" data-toggle="modal" data-target="#"><i className="fa fa-eye m-r-5" ></i> View</a>
          );
        }
        

        return_push.push(
          <div className="dropdown dropdown-action">
            <a aria-expanded="false" data-toggle="dropdown" className="action-icon dropdown-toggle" href="#"><i className="material-icons">more_vert</i></a>
            <div className="dropdown-menu dropdown-menu-right">
              {View_push}
            </div>
          </div>
        );
      }
      return return_push;
  }

  ViewRecord = (record) => e => {
    
    //ViewRecord (record)  {
    //alert(this.state.HistoryContactId);
    e.preventDefault();
    //console.log('View Entitlement');
    //console.log(record);
    this.setState({ HistoryListGrid: [] });
    //alert(record);
    this.showLoader();

    //var SearchYear = this.state["SearchentitlementYear"];

    
    //console.log('View Entitlement SearchYear');
    //console.log(SearchYear);
    //var userContactId = record.contactId;
    //var userContactId = this.state.HistoryContactId;

    // Pagination Static 
    //var Sort_Type = this.state.SortType;
    var IsSortingEnabled = true;

    //var Sort_Type = this.state.SortType;
    var sortColumnEntHistory = this.state.sortColumnEntHistory;
    var SortTypeIdEntHistory = this.state.SortTypeIdEntHistory;

    console.log("ViewRecord");
    console.log(sortColumnEntHistory);
    console.log(SortTypeIdEntHistory);
    // Pagination Static 

    var userContactId = record;
    var SearchYear = this.state["SearchentitlementYear"];

    var url=process.env.API_API_URL+'GetUsersEntitlementBalanceVieWHistory?contactId='+userContactId+'&yearId='+SearchYear+'&sortColumn='+sortColumnEntHistory+'&SortType='+SortTypeIdEntHistory+'&IsSortingEnabled='+IsSortingEnabled;
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
    }).then((response) => response.json())
    .then(data => {
        console.log("responseJson GetUsersEntitlementBalanceVieWHistory");
        console.log(data);
        if (data.responseType === "1") {
          //history_entitlement
          $("#history_entitlement").modal("show");
          this.setState({ HistoryListGrid: this.rowData(data.data,0) });
        }else{
          SystemHelpers.ToastError(data.message); 
        }
        this.hideLoader();
    })
    .catch(error => {
      this.props.history.push("/error-500");
    });

  }

  HistoryChangeViewRecord = (year) => e => {
    
    this.setState({ HistoryListGrid: [] });
    this.showLoader();

    var userContactId = this.state.HistoryContactId;
    var SearchYear = year;

    var IsSortingEnabled = true;
    var sortColumnEntHistory = this.state.sortColumnEntHistory;
    var SortTypeIdEntHistory = this.state.SortTypeIdEntHistory;

    console.log("HistoryChangeViewRecord");
    console.log(sortColumnEntHistory);
    console.log(SortTypeIdEntHistory);

    var url=process.env.API_API_URL+'GetUsersEntitlementBalanceVieWHistory?contactId='+userContactId+'&yearId='+SearchYear+'&sortColumn='+sortColumnEntHistory+'&SortType='+SortTypeIdEntHistory+'&IsSortingEnabled='+IsSortingEnabled;
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
    }).then((response) => response.json())
    .then(data => {
        console.log("responseJson History Change GetUsersEntitlementBalanceVieWHistory");
        console.log(data);
        if (data.responseType === "1") {
          this.setState({ HistoryListGrid: this.rowData(data.data,1) });
        }else{
          SystemHelpers.ToastError(data.message); 
        }
        this.hideLoader();
    })
    .catch(error => {
      console.log(error);
      this.props.history.push("/error-500");
    });

  }

  rowData(HistoryListGrid,TypeYear) {
      //alert('rowData history Entitlement');
      //console.log('history Entitlement');
      //console.log(this.state.HistoryListGrid);

      if(HistoryListGrid.userEntitlementViews != null){
        var HistoryListGrid_length = HistoryListGrid.userEntitlementViews.length;
      }else{
        var HistoryListGrid_length = 0;
      }
      
      
      let dataArray = [];
      var i=1;

      this.setState({ HistoryContactId: HistoryListGrid.contactId});

      this.setState({ HistoryemployeeName: HistoryListGrid.employeeName});
      this.setState({ HistoryemployeementHours: HistoryListGrid.employeementHours});

      if(HistoryListGrid.hireDate !="" && HistoryListGrid.hireDate !=null){
        this.setState({ HistoryhireDate: moment(HistoryListGrid.hireDate,'DD-MM-YYYY').format(process.env.DATE_FORMAT)});
      }else{
        this.setState({ HistoryhireDate: ""});
      }
      
      if(HistoryListGrid.noOfHours!="" && HistoryListGrid.noOfHours!=null){
        this.setState({ HistorynoOfHours: HistoryListGrid.noOfHours+'hours'});
      }else{
        this.setState({ HistorynoOfHours: '0hours'});
      }
      
      if(TypeYear == 0){
        this.setState({ HistoryYear: this.state["SearchentitlementYear"]});
      }
      // Entitlements Current Year

      // Entitlements Year
      let yearsList= this.state.HistoryYearList;

      if(TypeYear == 0){
        //alert('if');
        //alert(this.state["SearchentitlementYear"]);
        var YearHistory = this.state["SearchentitlementYear"];
      }else{
        //alert('else');
        //alert(this.state.HistoryYear);
        var YearHistory = this.state["HistoryYear"];
      }
         
      var length = yearsList.length;
      
      if (length > 0) {
        for (var zz = 0; zz < length; zz++) {
          if(yearsList[zz].guidId === YearHistory){
            var current_year=yearsList[zz].name;
            this.setState({ HistoryEntitlementsCurrentYear: current_year });
            //$("#HistoryYear").css("background-color", "#dee2e6");
          }
        }
        zz++;
      }
      // Entitlements Year

      if(HistoryListGrid.vacationHoursCurrent!="" && HistoryListGrid.vacationHoursCurrent!=null){
        this.setState({ HistoryEntitlementsCurrentVacation: HistoryListGrid.vacationHoursCurrent+'h'});
      }else{
        this.setState({ HistoryEntitlementsCurrentVacation: '0h'});
      }
      
      if(HistoryListGrid.sickTimeHoursCurrent!="" && HistoryListGrid.sickTimeHoursCurrent!=null){
        this.setState({ HistoryEntitlementsCurrentSickTime: HistoryListGrid.sickTimeHoursCurrent+'h'});
      }else{
        this.setState({ HistoryEntitlementsCurrentSickTime: '0h'});
      }
      // Entitlements Current Year

      // Carried Forward Year
      moment().subtract(1, 'year').format(process.env.ENTITLEMENTS_DATE_FORMAT)

      var CarryForward_year=this.state.HistoryEntitlementsCurrentYear;
      this.setState({ HistoryEntitlementsCarryYear: moment(CarryForward_year).subtract(1, 'year').format(process.env.ENTITLEMENTS_DATE_FORMAT)});


      if(HistoryListGrid.vacationHoursCarryForward!="" && HistoryListGrid.vacationHoursCarryForward!=null){
        this.setState({ HistoryEntitlementsCarryVacation: HistoryListGrid.vacationHoursCarryForward+'h'});
      }else{
        this.setState({ HistoryEntitlementsCarryVacation: '0h'});
      }
      
      if(HistoryListGrid.overTimeTotal!="" && HistoryListGrid.overTimeTotal!=null){
        this.setState({ HistoryEntitlementsCarryOverTime: HistoryListGrid.overTimeTotal+'h'});
      }else{
        this.setState({ HistoryEntitlementsCarryOverTime: '0h'});
      }
      
      // Carried Forward Year

      // History Grid
      if(HistoryListGrid_length > 0)
      {
        for (var z = 0; z < HistoryListGrid_length; z++) {
          var tempdataArray = [];
          
          /* var tt = "3";  
          if(tt.match(/-/)){
            alert('true');
          }else{
            alert('false');
          }*/
          tempdataArray.ent_date = moment(HistoryListGrid.userEntitlementViews[z].transactionDate).format(process.env.DATE_FORMAT);//format(process.env.DATE_TIME_FORMAT);
          //tempdataArray.ent_date = formatDate(HistoryListGrid.userEntitlementViews[z].transactionDate);
          tempdataArray.description = HistoryListGrid.userEntitlementViews[z].description;

          var vacation_history= HistoryListGrid.userEntitlementViews[z].vacationHours;
          if(vacation_history!=null && vacation_history.match(/-/)){
            tempdataArray.vacation = <div className="text-align" style={{'color' : 'red'}}>{vacation_history}</div>;
          }else{
            tempdataArray.vacation = <div className="text-align">{vacation_history}</div>;
          }
          
          var sickTimeHours_history= HistoryListGrid.userEntitlementViews[z].sickTimeHours;
          if(sickTimeHours_history!=null && sickTimeHours_history.match(/-/)){
            tempdataArray.sicktime = <div className="text-align" style={{'color' : 'red'}}>{sickTimeHours_history}</div>;
          }else{
            tempdataArray.sicktime = <div className="text-align">{sickTimeHours_history}</div>;
          }
          
          var overtimeHours_history = HistoryListGrid.userEntitlementViews[z].overtimeHours;
          if(overtimeHours_history!=null && overtimeHours_history.match(/-/)){
            tempdataArray.overtime = <div className="text-align" style={{'color' : 'red'}}>{overtimeHours_history}</div>;
          }else{
            tempdataArray.overtime = <div className="text-align">{overtimeHours_history}</div>;
          }

          var specialBankTime_history = HistoryListGrid.userEntitlementViews[z].specialBankTime;
          if(specialBankTime_history!=null && specialBankTime_history.match(/-/)){
            tempdataArray.specialBank = <div className="text-align" style={{'color' : 'red'}}>{specialBankTime_history}</div>;
          }else{
            tempdataArray.specialBank = <div className="text-align">{specialBankTime_history}</div>;
          }
          //tempdataArray.specialBank = <div>{specialBankTime_history}</div>;

          var otherLeaves_history= HistoryListGrid.userEntitlementViews[z].otherLeaves;
          if(otherLeaves_history!=null && otherLeaves_history.match(/-/)){
            tempdataArray.leaves = <div className="text-align" style={{'color' : 'red'}}>{otherLeaves_history}</div>;
          }else{
            tempdataArray.leaves = <div className="text-align">{otherLeaves_history}</div>;
          }

          
          dataArray.push(tempdataArray);
          i++;
        }
      }

      return dataArray;
      // History Grid
  }

  HistoryContactApplyAddRecordView = (contactId) => e => {
    e.preventDefault();
    
    console.log(contactId);  
    this.setState({ IsViewHistoryContactApplyAddRecord: true });
    
    $("#add_entitlement").modal('show');
    
    this.setState({ AddApplyContactField: "C" });
    this.setState({ AddApplyContactOperator: this.state.DesignEqual });
    this.setState({ AddApplyContactValue: contactId });
  
  }

  // Profile Entitlement
  
  // Profile Entitlemet

  // Pagination Design
  PaginationDesign ()
  {
    let PageOutput = [];
    //console.log('pagination');
    //console.log(this.state.pagingData);
    
    if(this.state.pagingData !="" && this.state.pagingData !="undefined")
    {
      var Page_Count = this.state.pagingData.totalPages;
      //alert(this.state.pagingData.currentPage);
    //console.log('page count = ' + Page_Count);
    /* pagination count */


        var Page_Start=1;
        var Page_End=1;

        if(this.state.pagingData.currentPage == 1){
            Page_Start=1;

            if(Page_Count <= 10){
                Page_End=Page_Count;
            }else{
                Page_End=10;
            }
            
        }else{

            if(this.state.pagingData.currentPage < 5){
                Page_Start=1;
                Page_End=Page_Count;
                //console.log("Page_End 1 "+ Page_End);
            }else{
                Page_Start=parseInt(this.state.pagingData.currentPage) - parseInt(4);
                Page_End=parseInt(this.state.pagingData.currentPage) + parseInt(6);
                //console.log("Page_End 2 "+ Page_End);
                if(Page_End > Page_Count){
                    Page_End=Page_Count;
                    //console.log("Page_End 3 "+ Page_End);
                }
            }

        }
      let Page = [];
      var i = 1;
      for (var z=Page_Start; z <= Page_End ; z++)
      {
        if(z==this.state.pagingData.currentPage)
        {
          Page.push(<li className="page-item active pk-active">
            <a className="page-link pk-active" id={z} href="#" onClick={this.PageGetGridData}>{z}<span className="sr-only">(current)</span></a>
          </li>);
        }
        else
        {
          Page.push(<li className="page-item"><a className="page-link" id={z} href="#" onClick={this.PageGetGridData} >{z}</a></li>);
        }
        i++;
      }

      let PagePrev = [];

      if(this.state.pagingData.currentPage == 1){
        PagePrev.push(<li className="page-item disabled">
          <a className="page-link" href="#">Previous</a>
        </li>);
      }else{
        PagePrev.push(<li className="page-item">
          <a className="page-link" href="#" id={parseInt(this.state.pagingData.currentPage)-parseInt(1)} tabIndex={-1} onClick={this.PageGetGridData}>Previous</a>
        </li>);
      }

      let PageNext = [];

      if(this.state.pagingData.currentPage == Page_Count){
        PageNext.push(<li className="page-item disabled">
          <a className="page-link" href="#">Next</a>
        </li>);
      }else{
        PageNext.push(
          <li className="page-item">
            <a className="page-link" href="#" id={parseInt(this.state.pagingData.currentPage)+parseInt(1)} onClick={this.PageGetGridData}>Next</a>
          </li>
        );
      }

      let PageLast = [];

      if(this.state.pagingData.currentPage == Page_Count){
        PageLast.push(<li className="page-item disabled">
          <a className="page-link" href="#">Last</a>
        </li>);
      }else{
        PageLast.push(
          <li className="page-item">
            <a className="page-link" href="#" id={parseInt(Page_Count)} onClick={this.PageGetGridData}>Last</a>
          </li>
        );
      }



      PageOutput.push(<section className="comp-section" id="comp_pagination">
                        <div className="pagination-box">
                          <div>
                            <ul className="pagination">
                              
                              {PagePrev}
                              {Page}
                              {PageNext}
                              {PageLast}
                              
                            </ul>
                          </div>
                        </div>
                      </section>);
    }
    
    return PageOutput;
  }

  PageGetGridData = e => {

    e.preventDefault();
    //console.log(e.target.id);
    let current_page= e.target.id;
    this.GetUsersEntitlementBalanceView(current_page,this.state.pageSize,this.state.searchText)
  }

  SearchGridData = e => {
    this.setState({ pageSize: this.state.TempsearchText });
    this.GetUsersEntitlementBalanceView(1,this.state.pageSize,this.state.TempsearchText);
  }
  // Pagination Design

  render() {
      

      const history_data = {
        columns: [
          {
            label: 'Date',
            field: 'ent_date',
            sort: 'disabled',
            width: 150
          },
          {
            label: 'Description',
            field: 'description',
            sort: 'disabled',
            width: 150
          },
          {
            label: 'Vacation',
            field: 'vacation',
            sort: 'disabled',
            width: 150
          },
          {
            label: 'Sick time',
            field: 'sicktime',
            sort: 'disabled',
            width: 150
          },
          {
            label: 'Overtime',
            field: 'overtime',
            sort: 'disabled',
            width: 100
          },
          {
            label: 'Miscellaneous Bank Time',
            field: 'specialBank',
            sort: 'disabled',
            width: 100
          },
          {
            label: 'Leaves',
            field: 'leaves',
            sort: 'disabled',
            width: 100
          }
        ],
        rows: this.state.HistoryListGrid
      };
                        
    return ( 
      <div className="main-wrapper">
          {/* Toast & Loder method use */}
          {(this.state.loading) ? <Loader /> : null} 
          {/* Toast & Loder method use */}
      <Header/>
        
          <div className={this.state.unauthorized_cls}>
            {/*<h1>403</h1>*/}
            <h4><i className="fa fa-warning"></i> Sorry, you are not authorized to access this page.</h4>
            {/*<p>Sorry, you are not authorized to access this page.</p>*/}
          </div>  
        

        <div className={this.state.page_wrapper_cls}>
            <Helmet>
                <title>{process.env.WEB_TITLE}</title>
                <meta name="description" content="Login page"/>         
            </Helmet>
              {/* Page Content */}
              
                <div className="content container-fluid">
                  {/* Page Header */}
                  <div className="page-header">
                    <div className="row align-items-center">
                      <div className="col">
                        <h3 className="page-title">Entitlements</h3>
                        <ul className="breadcrumb">
                          <li className="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                          <li className="breadcrumb-item active">Entitlements</li>
                        </ul>
                      </div>
                      <div className="col-auto float-right ml-auto">

                        { this.state.role_entitlement_menu_can.entitlement_menu_can_create == true  ?

                          <a href="#" className="btn add-btn" data-toggle="modal" data-target="#add_entitlement"><i className="fa fa-plus" /> Add Entitlements</a>
                        :<a href="#" className="phss-lock"><i className="fa fa-lock" /></a>
                        }

                        
                      </div>
                    </div>
                  </div>
                  {/* /Page Header */}

                  {/* Search Filter */}
                    <div className="row filter-row">
                      
                      
                      <div className="col-sm-6 col-md-3"> 
                        <div className="form-group  form-focus select-focus">
                          <select className="form-control floating" value={this.state.SearchentitlementYear} onChange={this.handleChange('SearchentitlementYear')}> 
                            {/*<option value="">-</option>*/}
                            {this.state.SearchEntitlementYearList.map(( listValue, index ) => {
                                return (
                                  <option key={index} value={listValue.guidId} >{listValue.name}</option>
                                );
                            })}
                          </select>
                          
                          <label className="focus-label">Year</label>
                        </div>
                      </div>
                      

                      <div className="col-sm-6 col-md-3"> 
                        <div className="form-group  form-focus select-focus">
                          <select className="form-control floating" value={this.state.SearchentitlementLocation} onChange={this.handleChange('SearchentitlementLocation')}> 
                            <option value="">All</option>
                            {this.state.SearchEntitlementLocationList.map(( listValue, index ) => {
                              return (
                                <option key={index} value={listValue.locationGuid} >{listValue.locationName}</option>
                              );
                            })}
                          </select>
                          
                          <label className="focus-label">Location</label>
                        </div>
                      </div>

                      
                      <div className="col-sm-6 col-md-3"> 
                        <div className="form-group form-focus select-focus">
                          <select className="form-control" value={this.state.SearchentitlementRole}  onChange={this.handleChange('SearchentitlementRole')} >
                            <option value=''>-</option>
                            {this.state.SearchEntitlementRoleList.map(( listValue, index ) => {
                                return (
                                  <option key={index} value={listValue.id}>{listValue.name}</option>
                                );
                            })}
                          </select>
                          <label className="focus-label">Role</label>
                        </div>
                      </div>

                      <div className="col-sm-6 col-md-3"> 
                        <div className="form-group form-focus select-focus">
                          <select className="form-control" value={this.state.SearchentitlementEmploymenthours}  onChange={this.handleChange('SearchentitlementEmploymenthours')} >
                            <option value=''>-</option>
                            {this.state.SearchEntitlementEmployeementHoursList.map(( listValue, index ) => {
                                return (
                                  <option key={index} value={listValue.name}>{listValue.name}</option>
                                );
                            })}
                          </select>
                          <label className="focus-label">Employment hours</label>
                        </div>
                      </div>


                      <div className="col-sm-6 col-md-3"> 
                        <div className="form-group form-focus select-focus">
                          <select className="form-control floating" id="sortColumnId" value={this.state.sortColumn} onChange={this.handleChange('sortColumn')}> 
                            <option value="">-</option>
                            <option value="employeeName">Name</option>
                            <option value="roleName">Role</option>
                            <option value="locationName">Location</option>
                          </select>
                          <label className="focus-label">Sorting</label>
                        </div>
                      </div>
                      <div className="col-sm-6 col-md-3"> 
                        <div className="form-group form-focus select-focus">
                          <select className="form-control floating" id="SortTypeId" value={this.state.SortType} onChange={this.handleChange('SortType')}> 
                            <option value="">-</option>
                            <option value="false">Ascending</option>
                            <option value="true">Descending</option>
                          </select>
                          <label className="focus-label">Sorting Order</label>
                        </div>
                      </div>
                      <div className="col-sm-6 col-md-3">  
                        <div className="form-group form-focus">
                          <input className="form-control floating" type="text" value={this.state.TempsearchText}  onChange={this.handleChange('TempsearchText')}/>
                          <label className="focus-label">Search</label>
                        </div>
                      </div>  

                      <div className="col-sm-6 col-md-3">  
                        <a href="#" className="btn btn-success btn-block" onClick={this.SearchGridData}> Search </a>  
                      </div> 

                    </div>
                  {/* /Search Filter */}  

                  <div className="row filter-row">
                    <div className="col-sm-6 col-md-2"> 
                      <div className="form-group form-focus select-focus">
                        <select className="form-control floating" value={this.state.pageSize}  onChange={this.handleChange('pageSize')}> 
                          <option value="5">5/Page</option>
                          <option value="10">10/Page</option>
                          <option value="50">50/Page</option>
                          <option value="100">100/Page</option>
                        </select>
                        <label className="focus-label">Per Page</label>
                      </div>
                    </div>
                  </div>

                  {/* Table */}
                  <div className="row">
                    <div className="col-md-12">
                      <div className="table-responsive pk-overflow-hide">
                        
                        <table className="table table-striped custom-table mb-0 datatable">
                          <thead>
                            <tr>
                              <th>Employee Name</th>
                              <th>Role</th>
                              <th>Location</th>
                              <th>Vacation hours</th>
                              <th>Overtime hours</th>
                              <th>Sick time hours</th>
                              <th>Miscellaneous Bank Time</th>
                              <th>Leaves</th>
                              <th>Action</th>
                            </tr>
                          </thead>
                          <tbody>
                            {this.state.ListGrid.map(( listValue, index ) => {
                                
                                return (
                                  <tr key={index}>
                                    <td>{listValue.employeeName}</td>
                                    <td>{listValue.roleName}</td>
                                    <td>{listValue.locationName}</td>
                                    <td>{listValue.vacationHours}</td>
                                    <td>{listValue.overtimeHours}</td>
                                    <td>{listValue.sickTimeHours}</td>
                                    <td>{listValue.specialBankTime}</td>
                                    <td>{listValue.otherLeaves}</td>
                                    
                                    <td>{this.Edit_Update_Btn_Func(listValue)}</td>
                                  </tr>
                                );
                              
                            })}
                            
                          </tbody>
                        </table> 

                        {/* Pagination */}
                        {this.PaginationDesign()}
                        {/* /Pagination */}
                         
                      </div>
                    </div>
                  </div>
                  {/* Table */}

                </div>
              
              {/* /Page Content */}

              {/* Add Entitlement Modal */}
              <div id="add_entitlement" data-backdrop="static" className="modal custom-modal" role="dialog">
                <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
                  <div className="modal-content">
                    <div className="modal-header">
                      <h5 className="modal-title">Create a Entitlements</h5>
                      <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={this.ClearRecord()}>
                        <span aria-hidden="true">×</span>
                      </button>
                    </div>
                    <div className="modal-body">
                      <div className="modal-content">
                        <div className="modal-body">
                          <form>
                            <div className="row">

                              <div className="col-md-4">
                                <div className="form-group">
                                  <label>Year<span className="text-danger">*</span></label>
                                  <select className="form-control floating" id="AddentitlementYear" value={this.state.AddentitlementYear} onChange={this.handleChange('AddentitlementYear')}> 
                                    <option value="">-</option>
                                    {this.state.EntitlementYearList.map(( listValue, index ) => {
                                     
                                        return (
                                          <option key={index} value={listValue.guidId} >{listValue.name}</option>
                                        );
                                     
                                    })}
                                  </select>
                                  <span className="form-text error-font-color">{this.state.errormsg["AddentitlementYear"]}</span>
                                </div>
                              </div>

                              <div className="col-md-4">
                                <div className="form-group">
                                  <label>Entitlement Category<span className="text-danger">*</span></label>
                                  <select className="form-control floating" id="AddentitlementCategory" value={this.state.AddentitlementCategory} onChange={this.handleChange('AddentitlementCategory')}> 
                                    <option value="">-</option>
                                    {this.state.EntitlementCategoryList.map(( listValue, index ) => {
                                        
                                        return (
                                          <option key={index} value={listValue.entitlementCategoryId} >{listValue.entitlementCategoryName}</option>
                                        );
                                     
                                    })}
                                  </select>
                                  <span className="form-text error-font-color">{this.state.errormsg["AddentitlementCategory"]}</span>
                                </div>
                              </div>

                              <div className="col-md-4">
                                <div className="form-group">
                                  <label>Action<span className="text-danger">*</span></label>
                                  <select className="form-control floating" id="AddentitlementAction" value={this.state.AddentitlementAction} onChange={this.handleChange('AddentitlementAction')}> 
                                    <option value="">-</option>
                                    <option value="Add">Add</option>
                                    <option value="Subtract">Subtract</option>
                                  </select>
                                  <span className="form-text error-font-color">{this.state.errormsg["AddentitlementAction"]}</span>
                                </div>
                              </div>

                              

                              <div className="col-md-6">
                                <div className="form-group">
                                  <label>Reason<span className="text-danger">*</span></label>
                                  <select className="form-control floating" id="AddentitlementSubCategory" value={this.state.AddentitlementSubCategory} onChange={this.handleChange('AddentitlementSubCategory')}> 
                                    <option value="">-</option>
                                    {this.state.EntitlementSubCategoryList.map(( listValue, index ) => {
                                     
                                        return (
                                          <option key={index} value={listValue.entitlementSubCategoryId} >{listValue.entitlementSubCategoryName}</option>
                                        );
                                     
                                    })}
                                  </select>
                                  <span className="form-text error-font-color">{this.state.errormsg["AddentitlementSubCategory"]}</span>
                                </div>
                              </div>
                              {/*<div className="col-md-3">
                                <div className="form-group">
                                  <label>Qty<span className="text-danger">*</span></label>
                                  <input type="text" className="form-control" value={this.state.AddentitlementQty} onChange={this.handleChange('AddentitlementQty')} />
                                  <Datetime
                                    inputProps={{readOnly: true,disabled: false}}
                                    closeOnTab={true}
                                    input={true}
                                    value=""
                                    onChange={this.handleEntitlementQtyAdd}
                                    dateFormat={false}
                                    timeFormat={process.env.ENTITLEMENTS_TIME_FORMAT}
                                    timeConstraints={{
                                      hours: { min: 0, max: 23 },
                                      minutes: { min: 0, max: 59, step: 15 }
                                    }}
                                    renderInput={(props) => {
                                         return <input {...props} value={(this.state.AddentitlementQty) ? props.value : ''} />
                                    }}
                                  />
                                  <span className="form-text error-font-color">{this.state.errormsg["AddentitlementQty"]}</span>
                                </div>
                              </div>*/}

                              <div className="col-md-4" id="id_days_div">
                                <div className="form-group">
                                  <label>Days<span className="text-danger">*</span></label>
                                  <input type="number" className="form-control" value={this.state.AddentitlementQtydays} onChange={this.handleChange('AddentitlementQtydays')} />
                                  <span className="form-text error-font-color">{this.state.errormsg["AddentitlementQtydays"]}</span>
                                </div>
                              </div>
                              <div className="col-md-2" id="id_hours_div">
                                <div className="form-group">
                                  <label>Hours<span className="text-danger">*</span></label>
                                  <input type="number" className="form-control" value={this.state.AddentitlementQtyhours} onChange={this.handleChange('AddentitlementQtyhours')} />
                                  <span className="form-text error-font-color">{this.state.errormsg["AddentitlementQtyhours"]}</span>
                                </div>
                              </div>
                              <div className="col-md-2" id="id_minutes_div">
                                <div className="form-group">
                                  <label>Minutes<span className="text-danger">*</span></label>
                                  <select className="form-control floating" value={this.state.AddentitlementQtyminutes} onChange={this.handleChange('AddentitlementQtyminutes')}> 
                                    <option value="">-</option>
                                    <option value="00">00</option>
                                    <option value="15">15</option>
                                    <option value="30">30</option>
                                    <option value="45">45</option>
                                  </select>
                                  <span className="form-text error-font-color">{this.state.errormsg["AddentitlementQtyminutes"]}</span>
                                </div>
                              </div>
                              <div className="col-md-2">
                                <div className="form-group">
                                  <label>Unit<span className="text-danger">*</span></label>
                                  <input type="text" className="form-control" disabled value={this.state.AddentitlementUnit} onChange={this.handleChange('AddentitlementUnit')} style={{"background-color" : "#f6f6f6"}} />
                                  <span className="form-text error-font-color"></span>
                                </div>
                              </div>

                              <div className="col-md-12">
                                <div className="form-group">
                                  <label>Description<span className="text-danger">*</span></label>
                                  <textarea rows="3" cols="5" class="form-control" value={this.state.AddentitlementDescription} onChange={this.handleChange('AddentitlementDescription')}></textarea>
                                  <span className="form-text error-font-color">{this.state.errormsg["AddentitlementDescription"]}</span>
                                </div>
                              </div>

                              {this.state.IsViewHistoryContactApplyAddRecord == false ? 
                                <div className="modal-content">
                                  <div className="modal-header">
                                    <h5 className="card-title">Apply to:</h5>
                                  </div>
                                  <div className="modal-body">
                                    
                                    <div className="row">
                                      <div className="col-md-3"></div>
                                      <div className="col-md-9">
                                        <span className="form-text error-font-color">{this.state.errormsg["AddFieldErrorMsg"]}</span>
                                      </div>
                                    </div>

                                    <div className="row">


                                        <div className="col-md-1">
                                          <div className="form-group">
                                            <label></label>
                                            <input type="checkbox" className="pk-entitlement-applyto-checkbox1" name="AddEmploymentHoursFieldCheckbox" id="AddEmploymentHoursFieldCheckbox" value={this.state.AddEmploymentHoursFieldCheckbox} onChange={this.handleChange('AddEmploymentHoursFieldCheckbox')} />
                                            <span className="form-text error-font-color"></span>
                                          </div>
                                        </div>
                                        <div className="col-md-4">
                                          <div className="form-group">
                                            <label>Field<span className="text-danger">*</span></label>
                                            <input type="text" className="form-control" value={this.state.AddApplyEmploymentHoursField} onChange={this.handleChange('AddApplyEmploymentHoursField')}/>
                                            <span className="form-text error-font-color"></span>
                                          </div>
                                        </div>
                                        <div className="col-md-3">
                                          <div className="form-group">
                                            <label>Operator<span className="text-danger">*</span></label>
                                            <select className="form-control" id="AddApplyEmploymentHoursOperator" value={this.state.AddApplyEmploymentHoursOperator}  onChange={this.handleChange('AddApplyEmploymentHoursOperator')} >
                                              <option value=''></option> 
                                              <option value='Equal'>{this.state.DesignEqual}</option>
                                              <option value='Not equal'>{this.state.DesignNotequal}</option>
                                            </select>
                                            <span className="form-text error-font-color">{this.state.errormsg["AddApplyEmploymentHoursOperator"]}</span>
                                          </div>
                                        </div>
                                        <div className="col-md-4">
                                          <div className="form-group">
                                            <label>Field<span className="text-danger">*</span></label>
                                            <select className="form-control" id="sortRoleId" value={this.state.AddApplyEmploymentHoursValue}  onChange={this.handleChange('AddApplyEmploymentHoursValue')} >
                                              <option value="">-</option>
                                              <option value="All">All</option>
                                                {this.state.EntitlementEmployeementHoursList.map(( listValue, index ) => {
                                                    if(this.state.AddentitlementCategory == "5b128f60-37f0-eb11-94ef-000d3a3e5463")
                                                    {
                                                      if(listValue.name != "Part-Time")
                                                      {
                                                        return (
                                                          <option key={index} value={listValue.id} >{listValue.name}</option>
                                                        );
                                                      }
                                                    }
                                                    else
                                                    {
                                                      return (
                                                        <option key={index} value={listValue.id} >{listValue.name}</option>
                                                      );
                                                    }
                                                 
                                                })}
                                            </select>
                                            <span className="form-text error-font-color">{this.state.errormsg["AddApplyEmploymentHoursValue"]}</span>
                                          </div>
                                        </div>
                                      

                                      
                                        <div className="col-md-1">
                                          <div className="form-group">
                                            <label></label>
                                            <input type="checkbox" className="pk-entitlement-applyto-checkbox" name="AddLocationFieldCheckbox" id="AddLocationFieldCheckbox" value={this.state.AddLocationFieldCheckbox} onChange={this.handleChange('AddLocationFieldCheckbox')}  />
                                            <span className="form-text error-font-color"></span>
                                          </div>
                                        </div>
                                        <div className="col-md-4">
                                          <div className="form-group">
                                            <input type="text" className="form-control" value={this.state.AddApplyLocationField} onChange={this.handleChange('AddApplyLocationField')} />
                                            <span className="form-text error-font-color"></span>
                                          </div>
                                        </div>
                                        <div className="col-md-3">
                                          <div className="form-group">
                                            <select className="form-control" value={this.state.AddApplyLocationOperator} onChange={this.handleChange('AddApplyLocationOperator')} >
                                              <option value=''></option>
                                              <option value='Equal'>{this.state.DesignEqual}</option>
                                              <option value='Not equal'>{this.state.DesignNotequal}</option>
                                            </select>
                                            <span className="form-text error-font-color">{this.state.errormsg["AddApplyLocationOperator"]}</span>
                                          </div>
                                        </div>
                                        <div className="col-md-4">
                                          <div className="form-group">
                                            {/*<input type="text" className="form-control" value={localStorage.getItem("primaryLocationName")} onChange={this.handleChange('AddApplyLocationValue')} />*/}
                                            <select className="form-control floating" value={this.state.AddApplyLocationValue} onChange={this.handleChange('AddApplyLocationValue')}> 
                                              <option value="">-</option>
                                              {this.state.EntitlementLocationList.map(( listValue, index ) => {
                                                return (
                                                  <option key={index} value={listValue.locationGuid} >{listValue.locationName}</option>
                                                );
                                              })}
                                            </select>
                                            <span className="form-text error-font-color">{this.state.errormsg["AddApplyLocationValue"]}</span>
                                          </div>
                                        </div>
                                      

                                    </div>
                                  </div>
                                </div>
                              : null
                              }
                            </div>

                            <div className="submit-section">
                              <button className="btn btn-primary submit-btn">Cancel</button>
                              <button className="btn btn-primary submit-btn" onClick={this.AddRecord()}>Submit</button>
                            </div>

                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              {/* /Add Entitlement Modal */}

              {/* Edit Entitlement Modal */}
              <div id="history_entitlement" className="modal custom-modal fade" role="dialog">
                <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
                  <div className="modal-content">
                    <div className="modal-header">
                      <h5 className="modal-title">Entitlements History</h5>
                      <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={this.ClearRecord()}>
                        <span aria-hidden="true">×</span>
                      </button>
                    </div>
                    <div className="modal-body">
                      <form>
                        <div className="row">

                          <div className="col-md-6">
                            <div className="form-group row">
                              <label className="col-lg-12 col-form-label">Employee Name : {this.state.HistoryemployeeName}</label>
                            </div>
                          </div>
                          <div className="col-md-6">
                            <div className="form-group row">
                              <label className="col-lg-12 col-form-label">Employment hours : {this.state.HistoryemployeementHours}</label>
                            </div>
                          </div>

                          <div className="col-md-6">
                            <div className="form-group row">
                              <label className="col-lg-12 col-form-label">Hire Date : {this.state.HistoryhireDate}</label>
                            </div>
                          </div>
                          <div className="col-md-6">
                            <div className="form-group row">
                              <label className="col-lg-12 col-form-label">No. of hours : {this.state.HistorynoOfHours}</label>
                            </div>
                          </div>

                          <div className="col-md-6">
                            <div className="form-group row">
                              <label className="col-lg-2 col-form-label">Year</label>
                              <div className="col-lg-4">
                                <select className="form-control floating" id="HistoryYear" value={this.state.HistoryYear} onChange={this.handleChange('HistoryYear')} > 
                                  <option value="">-</option>
                                  {this.state.HistoryYearList.map(( listValue, index ) => {
                                      return (
                                        <option key={index} value={listValue.guidId} >{listValue.name}</option>
                                      );
                                  })}
                                </select>
                              </div>
                            </div>
                          </div>
                          <div className="col-md-6">
                            <div className="form-group row float-right">
                              { this.state.role_entitlement_menu_can.entitlement_menu_can_create == true  ?

                                <a href="#" className="btn add-btn" onClick={this.HistoryContactApplyAddRecordView(this.state.HistoryContactId)} data-toggle="modal" data-target="#"><i className="fa fa-plus" /> Add Entitlements</a>
                                :<a href="#" className="phss-lock"><i className="fa fa-lock" /></a>
                              }
                            </div>
                          </div>

                          <div className="col-md-6">
                            <div className="form-group row">
                              <label className="col-lg-12 col-form-label">Entitlements {this.state.HistoryEntitlementsCurrentYear} : </label>
                            </div>
                          </div>
                          <div className="col-md-6" style={{"background": "lightgray"}}>
                            <div className="form-group row">
                              <label className="col-lg-12 col-form-label">Carried forward {this.state.HistoryEntitlementsCarryYear} :</label>
                            </div>
                          </div>

                          <div className="col-md-3">
                            <div className="form-group row">
                              <label className="col-lg-12 col-form-label">Vacation : {this.state.HistoryEntitlementsCurrentVacation}</label>
                            </div>
                          </div>
                          <div className="col-md-3">
                            <div className="form-group row">
                              <label className="col-lg-12 col-form-label">Sick Time : {this.state.HistoryEntitlementsCurrentSickTime}</label>
                            </div>
                          </div>
                          <div className="col-md-3" style={{"background": "lightgray"}}>
                            <div className="form-group row">
                              <label className="col-lg-12 col-form-label">Vacation : {this.state.HistoryEntitlementsCarryVacation}</label>
                            </div>
                          </div>
                          <div className="col-md-3" style={{"background": "lightgray"}}>
                            <div className="form-group row">
                              <label className="col-lg-12 col-form-label">Overtime : {this.state.HistoryEntitlementsCarryOverTime}</label>
                            </div>
                          </div>

                          {/* Ordering */}
                          <div className="col-lg-12 col-sm-4 col-xs-12">
                            <div className="form-group row">
                              <label className="col-lg-12 col-form-label"></label>
                            </div>
                          </div>

                          <div className="col-lg-12">
                            <div className="row filter-row">
                              <div className="col-lg-4 col-sm-4 col-xs-12"> 
                                <div className="form-group form-focus select-focus">
                                  <select className="form-control floating" id="sortColumnEntHistory" value={this.state.sortColumnEntHistory} onChange={this.handleChange('sortColumnEntHistory')}> 
                                    <option value="">-</option>
                                    <option value="transactionDate">Date</option>
                                    <option value="description">Description</option>
                                    <option value="vacationHours">Vacation</option>
                                    <option value="sickTimeHours">Sick time</option>
                                    <option value="specialBankTime">Miscellaneous Bank Time</option>
                                    <option value="otherLeaves">Leaves</option>
                                  </select>
                                  <label className="focus-label">Sorting</label>
                                </div>
                              </div>
                              <div className="col-lg-4 col-sm-4 col-xs-12">
                                <div className="form-group form-focus select-focus">
                                  <select className="form-control floating" id="SortTypeIdEntHistory" value={this.state.SortTypeIdEntHistory} onChange={this.handleChange('SortTypeIdEntHistory')}> 
                                    <option value="">-</option>
                                    <option value="false">Ascending</option>
                                    <option value="true">Descending</option>
                                  </select>
                                  <label className="focus-label">Sorting Order</label>
                                </div>
                              </div>
                              <div className="col-lg-4 col-sm-4 col-xs-12">  
                                <a href="#" className="col-lg-12 col-sm-4 float-right btn btn-success" onClick={this.HistoryChangeViewRecord(this.state.HistoryYear)}> Search </a>  
                              </div>
                            </div>
                          </div>
                          {/* Ordering */}

                          {/* Table */}
                          
                            <div className="col-md-12">
                              <div className="table-responsive pk-overflow-hide">
                                
                                <MDBDataTable
                                  striped
                                  bordered
                                  small
                                  data={history_data}
                                  entriesOptions={[5, 10, 20, 50, 100]} entries={5} pagesAmount={4}
                                  className="table table-striped custom-table mb-0 datatable"
                                />
                                 
                              </div>
                            </div>
                          
                          {/* Table */}
                        </div>
                        
                      </form>
                    </div>
                  </div>
                </div>
              </div>
              {/* /Edit Entitlement Modal */}

              {/* Delete Entitlement  Modal */}
              {/* /Delete Entitlement Modal */}
            </div>
          <SidebarContent/>
      </div>
        );
      
   }
}

export default Entitlements;
