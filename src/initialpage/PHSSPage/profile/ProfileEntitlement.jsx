/**
 * Signin Firebase
 */

import React, { Component } from 'react';
import { Helmet } from "react-helmet";

import moment from 'moment';

import Loader from '../../Loader';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import SystemHelpers from '../../Helpers/SystemHelper';

//table
import { MDBDataTable } from 'mdbreact';
import '@fortawesome/fontawesome-free/css/all.min.css';
import 'bootstrap-css-only/css/bootstrap.min.css';
import 'mdbreact/dist/css/mdb.css';
//table

import CryptoAES from 'crypto-js/aes';
import CryptoENC from 'crypto-js/enc-utf8';

import Datetime from "react-datetime";

import Entitlements from "../../PHSSPage/Entitlements";

class ProfileLocation extends Component {
  constructor(props) {
    super(props);

    this.state = {

        // Pagination
        totalCount : 0,
        pageSize : 5,
        currentPage : 1,
        totalPages : 0,
        previousPage : false,
        nextPage : false,
        searchText : '',
        pagingData : {},
        TempsearchText:'',

        sortColumn : 'transactionDate',
        SortType : false,
        IsSortingEnabled : true,
        // Pagination
       
        errormsg : '',
         
        staffContactID:this.props.staffContactID,
        staffContactName:localStorage.getItem("fullName"),

        role_entitlement_can: {},

        HistoryListGrid:[],

        HistoryYearList:[],

        // Add time Dropdown list
        EntitlementYearList : [],
        EntitlementCategoryList : [],
        EntitlementSubCategoryList : [],
        // Add time Dropdown list

        // Search
        // Search

        // History 
        HistoryYear : localStorage.getItem('LocalStorageCurrentYearGuid'),

        HistoryEntitlementsCurrentYear : '',
        HistoryEntitlementsCurrentVacation : '',
        HistoryEntitlementsCurrentSickTime : '',
        HistoryEntitlementsVacationBalanceFor : '',

        HistoryEntitlementsCarryYear : '',
        HistoryEntitlementsCarryVacation : '',
        HistoryEntitlementsCarryOverTime : '',

        HistoryContactId : '',

        HistoryCovidEntitlementsFor : '',
        HistoryCovidBalanceUpToDate : '',
        // History

        // Table grid Total Row
        TotalCurrentBalanceVactionHrs : '0',
        TotalCurrentBalanceOvertimeHrs : '0',
        TotalCurrentBalanceSicktimeHrs : '0',
        TotalCurrentBalanceSpecialBankTotal : '0',
        TotalCurrentBalanceLeaves : '0',
        // Table grid Total Row

        // Add
        AddentitlementYear : '',
        AddentitlementCategory : '',
        AddentitlementSubCategory : '',
        AddentitlementAction : '',
        AddentitlementQtydays : '',
        AddentitlementQtyhours : '',
        AddentitlementQtyminutes : '',
        AddentitlementUnit : 'hours',
        AddentitlementDescription : '',

        AddentitlementAppliesTos : [],

        AddApplyContactField : 'Contact',
        AddApplyContactOperator : '',
        AddApplyContactValue : '',


        // Add

        isDelete : false,
        
        staffContactFullname : localStorage.getItem('fullName')
    };
    this.setPropState = this.setPropState.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  setPropState(key, value) {
      this.setState({ [key]: value });
  }

  handleChange = input => e => {
    this.setState({ [input]: e.target.value.replace(/^\s+/g, '') });
    if (this.state[input] != '') {
        delete this.state.errormsg[input];
    }

    // Year Change event
    if([input]=="HistoryYear")
    {
      this.setState({ HistoryYear : e.target.value });
      console.log("HistoryYear="+e.target.value);
      this.GetUsersEntitlementBalanceVieWHistory(this.state.currentPage,this.state.pageSize,this.state.searchText,e.target.value);
    }
    // Year Change event

    // Add time
    if([input]=="AddentitlementCategory")
    {
      this.setState({ EntitlementSubCategoryList: [] });
    
      this.GetEntitlementSubCategoryView(e.target.value);
      this.setState({ AddentitlementSubCategory: '' });
    }
    // Add time

    // Add Time COVID 19 Hours and Minutes Hide
    if([input]=="AddentitlementSubCategory")
    {
      //console.log([input]);
      //console.log(e.target.value);
      //console.log(process.env.API_ENTITLEMENT_IDEL_COVID19);
      if(e.target.value == process.env.API_ENTITLEMENT_IDEL_COVID19)
      {
        //alert(e.target.value);
        //console.log(e.target.value);
        $("#id_days_div").show();

        $("#id_hours_div").hide();
        $("#id_minutes_div").hide();
        this.setState({ AddentitlementQtyhours: '' });
        this.setState({ AddentitlementQtyminutes: '' });
        this.setState({ AddentitlementUnit: 'days' });
      }
      else
      {
        $("#id_hours_div").show();
        $("#id_minutes_div").show();

        $("#id_days_div").hide();
        this.setState({ AddentitlementQtyhours: '' });
        this.setState({ AddentitlementUnit: 'hours' });
      }
    }
    // Add Time COVID 19 Hours and Minutes Hide

    if([input]=="pageSize")
    {
      this.setState({ current_page: 1 });
      this.GetUsersEntitlementBalanceVieWHistory(1,e.target.value,this.state.searchText,this.state.HistoryYear)
    }

  }

  // Loader hide show method
  hideLoader = () => {
    this.setState({ loading: false });
  }

  showLoader = () => {
    this.setState({ loading: true });
  }
  // Loader hide show method

  componentDidMount() {

    /* Role Management */
    console.log('Role Store entitlement_can');
    console.log(this.props.entitlement_can);
    let entitlement_can = this.props.entitlement_can;
    this.setState({ role_entitlement_can: this.props.entitlement_can });
    /* Role Management */

    // this.GetEntitlementCategoryView();
    // this.GetPhssYears();
    // this.GetUsersEntitlementBalanceVieWHistory(this.state.HistoryYear);
    $("#id_days_div").hide();
  }

  TabClickOnLoadEntitlementsCurrentBalance = () => e => {
    //debugger;
    e.preventDefault();

    this.GetEntitlementCategoryView();
    this.GetPhssYears();
    this.GetUsersEntitlementBalanceVieWHistory(this.state.currentPage,this.state.pageSize,this.state.searchText,this.state.HistoryYear);
  }

  // Drop down Api
  GetEntitlementCategoryView(){
    this.showLoader();
    var url=process.env.API_API_URL+'GetEntitlementCategoryView';
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
    }).then((response) => response.json())
    .then(data => {
        //console.log("responseJson GetEntitlementCategoryView");
        //console.log(data);
        if (data.responseType === "1") {
          this.setState({ EntitlementCategoryList: data.data});
        }else{
          SystemHelpers.ToastError(data.message);   
        }
        this.hideLoader();
    })
    .catch(error => {
      this.props.history.push("/error-500");
    });
  }

  GetEntitlementSubCategoryView(id){
    this.showLoader();
    var url=process.env.API_API_URL+'GetEntitlementSubCategoryView?categoryId='+id;
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
    }).then((response) => response.json())
    .then(data => {
        //console.log("responseJson GetEntitlementSubCategoryView");
        //console.log(data);
        if (data.responseType === "1") {
          this.setState({ EntitlementSubCategoryList: data.data});
        }else{
          SystemHelpers.ToastError(data.message);   
        }
        this.hideLoader();
        
    })
    .catch(error => {
      this.props.history.push("/error-500");
    });
  }

  GetPhssYears(){
    this.showLoader();
    var url=process.env.API_API_URL+'GetPhssYears';
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
    }).then((response) => response.json())
    .then(data => {
        console.log("responseJson GetPhssYears");
        console.log(data);
        if (data.responseType === "1") {
          this.setState({ EntitlementYearList: data.data});
          this.setState({ HistoryYearList: data.data});
        }else{
          SystemHelpers.ToastError(data.message);   
        }
        this.hideLoader();
    })
    .catch(error => {
      this.props.history.push("/error-500");
    });
  }
  // Drop down Api

  GetUsersEntitlementBalanceVieWHistory (currentPage,pageSize,searchText,YearId) {
  //GetUsersEntitlementBalanceVieWHistory = (currentPage,pageSize,searchText,YearId)  => e => {
  //GetUsersEntitlementBalanceVieWHistory = (YearId) => e => {

      //e.preventDefault();
      
      this.setState({ HistoryListGrid: [] });
      this.setState({ TotalCurrentBalanceVactionHrs : '0'});
      this.setState({ TotalCurrentBalanceOvertimeHrs : '0'});
      this.setState({ TotalCurrentBalanceSicktimeHrs : '0'});
      this.setState({ TotalCurrentBalanceSpecialBankTotal : '0'});
      this.setState({ TotalCurrentBalanceLeaves : '0'});

      // Pagination
      let bodyarray = {};
      bodyarray["currentPage"] = 1;
      bodyarray["nextPage"] = false;
      bodyarray["pageSize"] = 5;
      bodyarray["previousPage"] = false;
      bodyarray["totalCount"] = 0;
      bodyarray["totalPages"] = 0;
      
      this.setState({ pagingData : bodyarray });

      this.setState({ currentPage: currentPage });
      this.setState({ pageSize: pageSize });

      var sort_Column = this.state.sortColumn;
      var Sort_Type = this.state.SortType;
      
      var IsSortingEnabled = true;

      var url_paging_para = '&pageNumber='+currentPage+'&pageSize='+pageSize+'&searchText='+searchText+'&sortColumn='+sort_Column+'&SortType='+Sort_Type+'&IsSortingEnabled='+IsSortingEnabled;
      // Pagination

      /* Role Management */
      var getrole = SystemHelpers.GetRole();
      //console.log('Profile Entitlement Get role');
      /* Role Management */

      //var SearchYear = this.state["HistoryYear"];
      var SearchYear = YearId;
      console.log("get api="+SearchYear);
      
      this.showLoader();
      var url=process.env.API_API_URL+'GetUsersEntitlementBalanceVieWHistory?contactId='+this.state.staffContactID+'&yearId='+SearchYear+url_paging_para;
      fetch(url, {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'bearer '+localStorage.getItem("token")
        },
      }).then((response) => response.json())
      .then(data => {
          console.log("responseJson GetUsersEntitlementBalanceVieWHistory");
          console.log(data);
          if (data.responseType == "1") {


            if(data.data.userEntitlementViews != null){
              this.setState({ HistoryListGrid : data.data.userEntitlementViews });
              this.setState({ pagingData: data.pagingData });
            }
            
            this.setState({ TotalCurrentBalanceVactionHrs : data.data.vacationTotal});
            this.setState({ TotalCurrentBalanceOvertimeHrs : data.data.overTimeTotal});
            this.setState({ TotalCurrentBalanceSicktimeHrs : data.data.sickTotal});
            this.setState({ TotalCurrentBalanceSpecialBankTotal : data.data.specialBankTotal});
            this.setState({ TotalCurrentBalanceLeaves : data.data.leaveTotal});

            this.setState({ HistoryContactId: data.data.contactId});

            // Entitlements Current Year

              // Entitlements Year
              let yearsList= this.state.HistoryYearList;
              var YearHistory = this.state.HistoryYear;

              var length = yearsList.length;
              
              if (length > 0) {
                for (var zz = 0; zz < length; zz++) {
                  if(yearsList[zz].guidId === YearHistory){
                    var current_year=yearsList[zz].name;
                    this.setState({ HistoryEntitlementsCurrentYear: current_year });
                  }
                }
                zz++;
              }
              // Entitlements Year

            this.setState({ HistoryEntitlementsCurrentVacation: data.data.vacationHoursCurrent+'h'});
            
            this.setState({ HistoryEntitlementsCurrentSickTime: data.data.sickTimeHoursCurrent+'h'});

            this.setState({ HistoryEntitlementsVacationBalanceFor: data.data.vacationTotal+'h'});
            // Entitlements Current Year

            // Carried Forward Year
            var CarryForward_year=this.state.HistoryEntitlementsCurrentYear;
            this.setState({ HistoryEntitlementsCarryYear: moment(CarryForward_year).subtract(1, 'year').format(process.env.ENTITLEMENTS_DATE_FORMAT)});
            
            this.setState({ HistoryEntitlementsCarryVacation: '0h'});
            
            this.setState({ HistoryEntitlementsCarryOverTime: '0h'});
            // Carried Forward Year

            // COVID
            this.setState({ HistoryCovidEntitlementsFor: data.data.covid19ForYear+'d'});
            this.setState({ HistoryCovidBalanceUpToDate: data.data.covid19UpToDate+'d'});
            // COVID

            
            
          }else{
            SystemHelpers.ToastError(data.message); 
          }
          this.hideLoader();
      })
      .catch(error => {
        console.log("catch");
        console.log(error);
        this.props.history.push("/error-500");
      });
  }

  GetUsersEntitlementBalanceVieWHistorySearch = (currentPage,pageSize,searchText,YearId)  => e => {
  //GetUsersEntitlementBalanceVieWHistory = (YearId) => e => {

      //e.preventDefault();
      
      this.setState({ HistoryListGrid: [] });
      this.setState({ TotalCurrentBalanceVactionHrs : '0'});
      this.setState({ TotalCurrentBalanceOvertimeHrs : '0'});
      this.setState({ TotalCurrentBalanceSicktimeHrs : '0'});
      this.setState({ TotalCurrentBalanceSpecialBankTotal : '0'});
      this.setState({ TotalCurrentBalanceLeaves : '0'});

      // Pagination
      let bodyarray = {};
      bodyarray["currentPage"] = 1;
      bodyarray["nextPage"] = false;
      bodyarray["pageSize"] = 5;
      bodyarray["previousPage"] = false;
      bodyarray["totalCount"] = 0;
      bodyarray["totalPages"] = 0;
      
      this.setState({ pagingData : bodyarray });

      this.setState({ currentPage: currentPage });
      this.setState({ pageSize: pageSize });

      var sort_Column = this.state.sortColumn;
      var Sort_Type = this.state.SortType;
      
      console.log("GetUsersEntitlementBalanceVieWHistorySearch");
      console.log(sort_Column);
      console.log(Sort_Type);

      var IsSortingEnabled = true;

      var url_paging_para = '&pageNumber='+currentPage+'&pageSize='+pageSize+'&searchText='+searchText+'&sortColumn='+sort_Column+'&SortType='+Sort_Type+'&IsSortingEnabled='+IsSortingEnabled;
      // Pagination

      /* Role Management */
      var getrole = SystemHelpers.GetRole();
      //console.log('Profile Entitlement Get role');
      /* Role Management */

      //var SearchYear = this.state["HistoryYear"];
      var SearchYear = YearId;
      console.log("get api="+SearchYear);
      
      this.showLoader();
      var url=process.env.API_API_URL+'GetUsersEntitlementBalanceVieWHistory?contactId='+this.state.staffContactID+'&yearId='+SearchYear+url_paging_para;
      fetch(url, {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'bearer '+localStorage.getItem("token")
        },
      }).then((response) => response.json())
      .then(data => {
          console.log("responseJson GetUsersEntitlementBalanceVieWHistorySearch");
          console.log(data);
          if (data.responseType == "1") {


            if(data.data.userEntitlementViews != null){
              this.setState({ HistoryListGrid : data.data.userEntitlementViews });
              this.setState({ pagingData: data.pagingData });
            }
            
            this.setState({ TotalCurrentBalanceVactionHrs : data.data.vacationTotal});
            this.setState({ TotalCurrentBalanceOvertimeHrs : data.data.overTimeTotal});
            this.setState({ TotalCurrentBalanceSicktimeHrs : data.data.sickTotal});
            this.setState({ TotalCurrentBalanceSpecialBankTotal : data.data.specialBankTotal});
            this.setState({ TotalCurrentBalanceLeaves : data.data.leaveTotal});

            this.setState({ HistoryContactId: data.data.contactId});

            // Entitlements Current Year

              // Entitlements Year
              let yearsList= this.state.HistoryYearList;
              var YearHistory = this.state.HistoryYear;

              var length = yearsList.length;
              
              if (length > 0) {
                for (var zz = 0; zz < length; zz++) {
                  if(yearsList[zz].guidId === YearHistory){
                    var current_year=yearsList[zz].name;
                    this.setState({ HistoryEntitlementsCurrentYear: current_year });
                  }
                }
                zz++;
              }
              // Entitlements Year

            this.setState({ HistoryEntitlementsCurrentVacation: data.data.vacationHoursCurrent+'h'});
            
            this.setState({ HistoryEntitlementsCurrentSickTime: data.data.sickTimeHoursCurrent+'h'});

            this.setState({ HistoryEntitlementsVacationBalanceFor: data.data.vacationTotal+'h'});
            // Entitlements Current Year

            // Carried Forward Year
            var CarryForward_year=this.state.HistoryEntitlementsCurrentYear;
            this.setState({ HistoryEntitlementsCarryYear: moment(CarryForward_year).subtract(1, 'year').format(process.env.ENTITLEMENTS_DATE_FORMAT)});
            
            this.setState({ HistoryEntitlementsCarryVacation: '0h'});
            
            this.setState({ HistoryEntitlementsCarryOverTime: '0h'});
            // Carried Forward Year

            // COVID
            this.setState({ HistoryCovidEntitlementsFor: data.data.covid19ForYear+'d'});
            this.setState({ HistoryCovidBalanceUpToDate: data.data.covid19UpToDate+'d'});
            // COVID

            
            
          }else{
            SystemHelpers.ToastError(data.message); 
          }
          this.hideLoader();
      })
      .catch(error => {
        console.log("catch");
        console.log(error);
        this.props.history.push("/error-500");
      });
  }

  ClearRecord = ()=> e => {
    e.preventDefault();

    this.setState({ AddentitlementYear: '' });
    this.setState({ AddentitlementCategory: '' });
    this.setState({ AddentitlementSubCategory: '' });
    this.setState({ AddentitlementAction: '' });
    this.setState({ AddentitlementQtyhours: '' });
    this.setState({ AddentitlementQtyminutes: '' });
    this.setState({ AddentitlementDescription: '' });

    this.setState({ errormsg: '' });
  }

  AddRecord = () => e => {
      e.preventDefault();

      let step1Errors = {};

      if (this.state["AddentitlementYear"] == '' || this.state["AddentitlementYear"] == null) {
        step1Errors["AddentitlementYear"] = "Year is mandatory";
      }

      if (this.state["AddentitlementCategory"] == '' || this.state["AddentitlementCategory"] == null) {
        step1Errors["AddentitlementCategory"] = "Entitlement Category is mandatory";
      } 
      
      if (this.state["AddentitlementAction"] == '' || this.state["AddentitlementAction"] == null) {
        step1Errors["AddentitlementAction"] = "Action is mandatory";
      }

      if(this.state["AddentitlementSubCategory"] == process.env.API_ENTITLEMENT_IDEL_COVID19)
      {
        if (this.state["AddentitlementQtydays"] == '' || this.state["AddentitlementQtydays"] == null) {
          step1Errors["AddentitlementQtydays"] = "Days is mandatory";
        }
      }
      else
      {
        if (this.state["AddentitlementQtyhours"] == '' || this.state["AddentitlementQtyhours"] == null) {
          step1Errors["AddentitlementQtyhours"] = "Hours is mandatory";
        }

        if (this.state["AddentitlementQtyminutes"] == '' || this.state["AddentitlementQtyminutes"] == null) {
          step1Errors["AddentitlementQtyminutes"] = "Minutes is mandatory";
        }
      }

      
      
      if (this.state["AddentitlementSubCategory"] == '' || this.state["AddentitlementSubCategory"] == null) {
        step1Errors["AddentitlementSubCategory"] = "Reason is mandatory";
      }

      if (this.state["AddentitlementDescription"] == '' || this.state["AddentitlementDescription"] == null) {
        step1Errors["AddentitlementDescription"] = "Description is mandatory";
      }
      
      //console.log(step1Errors);

      this.setState({ errormsg: step1Errors });
          
      if (Object.keys(step1Errors).length > 0) {
        return false;
      }

      this.showLoader();
      
      let bodyarray = {};
      bodyarray["loggedInUserId"] = this.state.staffContactID;
      bodyarray["yearId"] = this.state["AddentitlementYear"];
      bodyarray["entitlementCategoryId"] = this.state["AddentitlementCategory"];
      bodyarray["entitlementAction"] = this.state["AddentitlementAction"];

      if(this.state["AddentitlementSubCategory"] == process.env.API_ENTITLEMENT_IDEL_COVID19)
      {
        bodyarray["entitlementQty"] = this.state["AddentitlementQtydays"]+':';
      }
      else
      {
        bodyarray["entitlementQty"] = this.state["AddentitlementQtyhours"]+':'+this.state["AddentitlementQtyminutes"];
      }
      
      bodyarray["entitlementSubCategoryId"] = this.state["AddentitlementSubCategory"];
      bodyarray["entitlementDescription"] = this.state["AddentitlementDescription"];

      let ArrayJson=[{
        appliesToFieldId : "C",
        operator : "=",
        appliesToFieldValue : this.state.staffContactID,
      }];
      bodyarray["entitlementAppliesTos"] = ArrayJson;
      
      var url=process.env.API_API_URL+'PostEntitlement';
      fetch(url, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'bearer '+localStorage.getItem("token")
        },
        body: JSON.stringify(bodyarray)
      }).then((response) => response.json())
      .then(data => {
          //console.log("responseJson PostEntitlement");
          //console.log(data);
          if (data.responseType === "1") {
            SystemHelpers.ToastSuccess(data.responseMessge);  
            $( ".close" ).trigger( "click" ); 
            
            this.setState({ AddentitlementYear: '' });
            this.setState({ AddentitlementCategory: '' });
            this.setState({ AddentitlementSubCategory: '' });
            this.setState({ AddentitlementAction: '' });
            this.setState({ AddentitlementQtyhours: '' });
            this.setState({ AddentitlementQtyminutes: '' });
            this.setState({ AddentitlementDescription: '' });

            this.setState({ errormsg: '' });

            this.GetUsersEntitlementBalanceVieWHistory(this.state.currentPage,this.state.pageSize,this.state.searchText,this.state.HistoryYear);
          }else if (data.responseType === "2" || data.responseType === "3" || data.responseType === "4") {
            SystemHelpers.ToastError(data.responseMessge);  
          } else{
            SystemHelpers.ToastError(data.message);
          }
          this.hideLoader();
      })
      .catch(error => {
        this.props.history.push("/error-500");
      });
  }
  


  // Pagination Design
  PaginationDesign ()
  {
    let PageOutput = [];
    //console.log('pagination');
    //console.log(this.state.pagingData);
    
    if(this.state.pagingData !="" && this.state.pagingData !="undefined")
    {
      var Page_Count = this.state.pagingData.totalPages;
      //alert(this.state.pagingData.currentPage);
    //console.log('page count = ' + Page_Count);
    /* pagination count */


        var Page_Start=1;
        var Page_End=1;

        if(this.state.pagingData.currentPage == 1){
            Page_Start=1;

            if(Page_Count <= 10){
                Page_End=Page_Count;
            }else{
                Page_End=10;
            }
            
        }else{

            if(this.state.pagingData.currentPage < 5){
                Page_Start=1;
                Page_End=Page_Count;
                //console.log("Page_End 1 "+ Page_End);
            }else{
                Page_Start=parseInt(this.state.pagingData.currentPage) - parseInt(4);
                Page_End=parseInt(this.state.pagingData.currentPage) + parseInt(6);
                //console.log("Page_End 2 "+ Page_End);
                if(Page_End > Page_Count){
                    Page_End=Page_Count;
                    //console.log("Page_End 3 "+ Page_End);
                }
            }

        }
      let Page = [];
      var i = 1;
      for (var z=Page_Start; z <= Page_End ; z++)
      {
        if(z==this.state.pagingData.currentPage)
        {
          Page.push(<li className="page-item active pk-active">
            <a className="page-link pk-active" id={z} href="#" onClick={this.PageGetGridData}>{z}<span className="sr-only">(current)</span></a>
          </li>);
        }
        else
        {
          Page.push(<li className="page-item"><a className="page-link" id={z} href="#" onClick={this.PageGetGridData} >{z}</a></li>);
        }
        i++;
      }

      let PagePrev = [];

      if(this.state.pagingData.currentPage == 1){
        PagePrev.push(<li className="page-item disabled">
          <a className="page-link" href="#">Previous</a>
        </li>);
      }else{
        PagePrev.push(<li className="page-item">
          <a className="page-link" href="#" id={parseInt(this.state.pagingData.currentPage)-parseInt(1)} tabIndex={-1} onClick={this.PageGetGridData}>Previous</a>
        </li>);
      }

      let PageNext = [];

      if(this.state.pagingData.currentPage == Page_Count){
        PageNext.push(<li className="page-item disabled">
          <a className="page-link" href="#">Next</a>
        </li>);
      }else{
        PageNext.push(
          <li className="page-item">
            <a className="page-link" href="#" id={parseInt(this.state.pagingData.currentPage)+parseInt(1)} onClick={this.PageGetGridData}>Next</a>
          </li>
        );
      }

      let PageLast = [];

      if(this.state.pagingData.currentPage == Page_Count){
        PageLast.push(<li className="page-item disabled">
          <a className="page-link" href="#">Last</a>
        </li>);
      }else{
        PageLast.push(
          <li className="page-item">
            <a className="page-link" href="#" id={parseInt(Page_Count)} onClick={this.PageGetGridData}>Last</a>
          </li>
        );
      }



      PageOutput.push(<section className="comp-section" id="comp_pagination">
                        <div className="pagination-box">
                          <div>
                            <ul className="pagination">
                              
                              {PagePrev}
                              {Page}
                              {PageNext}
                              {PageLast}
                              
                            </ul>
                          </div>
                        </div>
                      </section>);
    }
    
    return PageOutput;
  }

  PageGetGridData = e => {

    e.preventDefault();
    //console.log(e.target.id);
    let current_page= e.target.id;
    //this.GetUsersEntitlementBalanceView(current_page,this.state.pageSize,this.state.searchText)
    //this.GetUsersEntitlementBalanceVieWHistory(this.state.currentPage,this.state.pageSize,this.state.searchText,this.state.HistoryYear);
    this.GetUsersEntitlementBalanceVieWHistory(current_page,this.state.pageSize,this.state.searchText,this.state.HistoryYear);
  }

  SearchGridData = e => {
    this.setState({ pageSize: this.state.TempsearchText });
    //this.GetUsersEntitlementBalanceView(1,this.state.pageSize,this.state.TempsearchText);
    this.GetUsersEntitlementBalanceVieWHistory(this.state.currentPage,this.state.pageSize,this.state.searchText,this.state.HistoryYear);
  }
  // Pagination Design



  render() {
      

      return (
        <div>
        {/* Toast & Loder method use */}
            
        {(this.state.loading) ? <Loader /> : null} 
        {/* Toast & Loder method use */}
        <div className="row">
          <div className="col-md-12 d-flex">
            <div className="card profile-box flex-fill">

              <div className="row">
                <button className="btn btn-primary submit-btn pk-profiletab-refreshbtn-hide" id="TabClickOnLoadEntitlementsCurrentBalance" onClick={this.TabClickOnLoadEntitlementsCurrentBalance()}>Refresh</button>
              </div>
              <div className="card-body">
                
                <h3 className="card-title">Entitlements - Current Balance</h3>
                
                
                {/* Search & View */}
                <div className="row filter-row">

                  <div className="col-md-2">
                    <div className="form-group">
                      <label>Year</label>
                      <select className="form-control floating" value={this.state.HistoryYear} onChange={this.handleChange('HistoryYear')}> 
                        <option value="">-</option>
                        {this.state.HistoryYearList.map(( listValue, index ) => {
                            return (
                              <option key={index} value={listValue.guidId} >{listValue.name}</option>
                            );
                        })}
                      </select>
                      <span className="form-text error-font-color">{this.state.errormsg["HistoryYear"]}</span>
                    </div>
                  </div>
                  <div className="col-md-2">
                    <div className="form-group">
                      <label>Per Page</label>
                      <select className="form-control floating" value={this.state.pageSize}  onChange={this.handleChange('pageSize')}> 
                        <option value="5">5/Page</option>
                        <option value="10">10/Page</option>
                        <option value="50">50/Page</option>
                        <option value="100">100/Page</option>
                      </select>
                    </div>
                  </div>
                  <div className="col-md-3">
                    <div className="form-group">
                      <label className="focus-label">Sorting</label>
                      <select className="form-control floating" id="sortColumn" value={this.state.sortColumn} onChange={this.handleChange('sortColumn')}> 
                        <option value="">-</option>
                        <option value="transactionDate">Date</option>
                        <option value="description">Description</option>
                        <option value="vacationHours">Vacation</option>
                        <option value="sickTimeHours">Sick time</option>
                        <option value="specialBankTime">Miscellaneous Bank Time</option>
                        <option value="otherLeaves">Leaves</option>
                      </select>
                    </div>
                  </div>
                  <div className="col-md-3">
                    <div className="form-group">
                      <label className="focus-label">Sorting Order</label>
                      <select className="form-control floating" id="SortTypeId" value={this.state.SortType} onChange={this.handleChange('SortType')}> 
                        <option value="">-</option>
                        <option value="false">Ascending</option>
                        <option value="true">Descending</option>
                      </select>
                    </div>
                  </div>
                  <div className="col-md-2">
                    <div className="form-group">
                      <label className="focus-label"></label>
                      <a href="#" className="col-lg-12 col-sm-4 float-right btn btn-success" onClick={this.GetUsersEntitlementBalanceVieWHistorySearch(this.state.currentPage,this.state.pageSize,this.state.searchText,this.state.HistoryYear)} > Search </a>  
                    </div>
                  </div>

                  <div className="col-md-9">
                    <div className="form-group">
                    </div>
                  </div>
                  <div className="col-md-3">
                    <div className="form-group">
                      {this.state.role_entitlement_can.entitlement_can_create == true ?

                        <a href="#" className="btn btn-primary mr-2 float-right" data-toggle="modal" data-target="#add_entitlement">Add Entitlement</a>
                          : null
                        }
                    </div>
                  </div>

                  <div className="col-md-4">
                    <div className="form-group">
                      <label><b>Carried forward {this.state.HistoryEntitlementsCarryYear}:</b></label>
                    </div>
                  </div>
                  <div className="col-md-4">
                    <div className="form-group">
                      <label><b>Entitlements {this.state.HistoryEntitlementsCurrentYear}:</b></label>
                    </div>
                  </div>
                  <div className="col-md-4">
                    <div className="form-group">
                    </div>
                  </div>

                  <div className="col-md-2">
                    <div className="form-group">
                    <label><b>Vacation:</b> {this.state.HistoryEntitlementsCarryVacation}</label>
                    </div>
                  </div>
                  <div className="col-md-2">
                    <div className="form-group">
                      <label><b>Overtime:</b> {this.state.HistoryEntitlementsCarryOverTime}</label>
                    </div>
                  </div>
                  <div className="col-md-2">
                    <div className="form-group">
                      <label><b>Vacation:</b> {this.state.HistoryEntitlementsCurrentVacation}</label>
                    </div>
                  </div>
                  <div className="col-md-2">
                    <div className="form-group">
                      <label><b>Sick Time:</b> {this.state.HistoryEntitlementsCurrentSickTime}</label>
                    </div>
                  </div>
                  <div className="col-md-3">
                    <div className="form-group">
                    <label><b>COVID entitlements for {this.state.HistoryEntitlementsCurrentYear}:</b> {this.state.HistoryCovidEntitlementsFor}</label>
                    </div>
                  </div>
                  {/*<div className="col-md-3">
                    <div className="form-group">
                    <label></label>
                    </div>
                  </div>*/}

                  <div className="col-md-4">
                    <div className="form-group">
                    </div>
                  </div>
                  <div className="col-md-4">
                    <div className="form-group">
                      <label><b>Vacation Balance for {this.state.HistoryEntitlementsCurrentYear}:</b> {this.state.HistoryEntitlementsVacationBalanceFor}</label>
                    </div>
                  </div>
                  <div className="col-md-4">
                    <div className="form-group">
                      <label><b>COVID Balance up to date:</b> {this.state.HistoryCovidBalanceUpToDate}</label>
                    </div>
                  </div>

                </div>
                {/* Search & View */}

                {/* Table */}
                <div className="row"></div>
                
                <div className="card-body">
                      <div className="row">
                          <div className="col-md-12">
                            <div className="table-responsive pk-overflow-hide">
                              
                              <table className="table table-striped custom-table mb-0 datatable">
                                <thead>
                                  <tr>
                                    <th>Date</th>
                                    <th>Description</th>
                                    <th>Vacation hours</th>
                                    <th>Overtime hours</th>
                                    <th>Sick time hours</th>
                                    <th>Miscellaneous Bank Time</th>
                                    <th>Leaves</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  {this.state.HistoryListGrid.map(( listValue, index ) => {

                                        if(listValue.vacationHours!=null && listValue.vacationHours.match(/-/)){
                                          var vacationHours = (<span style={{'color' : 'red', 'text-align' : 'center !important'}}>{listValue.vacationHours}</span>);
                                        }else{
                                          var vacationHours = (<span style={{'text-align' : 'center !important'}}>{listValue.vacationHours}</span>);
                                        }
                                        
                                        if(listValue.sickTimeHours!=null && listValue.sickTimeHours.match(/-/)){
                                          var sickTimeHours = (<span style={{'color' : 'red'}}>{listValue.sickTimeHours}</span>);
                                        }else{
                                          var sickTimeHours = (<span>{listValue.sickTimeHours}</span>);
                                        }

                                        if(listValue.overtimeHours!=null && listValue.overtimeHours.match(/-/)){
                                          var overtimeHours = (<span style={{'color' : 'red'}}>{listValue.overtimeHours}</span>);
                                        }else{
                                          var overtimeHours = (<span>{listValue.overtimeHours}</span>);
                                        }

                                        if(listValue.specialBankTime!=null && listValue.specialBankTime.match(/-/)){
                                          var specialBankTime = (<span style={{'color' : 'red'}}>{listValue.specialBankTime}</span>);
                                        }else{
                                          var specialBankTime = (<span>{listValue.specialBankTime}</span>);
                                        }

                                        if(listValue.otherLeaves!=null &&  listValue.otherLeaves.match(/-/)){
                                          var otherLeaves = (<span style={{'color' : 'red'}}>{listValue.otherLeaves}</span>);
                                        }else{
                                          var otherLeaves = (<span>{listValue.otherLeaves}</span>);
                                        }
                                        
                                        
                                            return (
                                              <tr key={index}>
                                                <td>{moment(listValue.transactionDate).format(process.env.DATE_TIME_FORMAT)}</td>
                                                <td className="text-align">{listValue.description}</td>
                                                <td className="text-align">{vacationHours}</td>
                                                <td className="text-align">{overtimeHours}</td>
                                                <td className="text-align">{sickTimeHours}</td>
                                                <td className="text-align">{specialBankTime}</td>
                                                <td className="text-align">{otherLeaves}</td>
                                                
                                              </tr>
                                            );
                                     
                                    
                                  })}
                                  <tr className="pk-profileEntitlement-table-tr">
                                    <td className="pk-profileEntitlement-table-tr-td" colSpan="2">Current Balance</td>
                                    <td className="pk-profileEntitlement-table-tr-td text-align">{this.state.TotalCurrentBalanceVactionHrs}</td>
                                    <td className="pk-profileEntitlement-table-tr-td text-align">{this.state.TotalCurrentBalanceOvertimeHrs }</td>
                                    <td className="pk-profileEntitlement-table-tr-td text-align">{this.state.TotalCurrentBalanceSicktimeHrs }</td>
                                    <td className="pk-profileEntitlement-table-tr-td text-align">{this.state.TotalCurrentBalanceSpecialBankTotal }</td>
                                    <td className="pk-profileEntitlement-table-tr-td text-align">{this.state.TotalCurrentBalanceLeaves }</td>
                                  </tr>
                                </tbody>
                              </table>

                              {/* Pagination */}
                              {this.PaginationDesign()}
                              {/* /Pagination */}

                              </div>
                          </div>
                      </div>
                </div>
                {/* Table */}
              </div>
            </div>
          </div>
        </div>
        {/* ****************** Profile Entitlement Tab Modals ******************* */}
          {/* Entitlement Modal */}
            {/*<Entitlements 
                staffContactID={this.state.staffContactID}
                entitlement_can={this.state.role_entitlement_can}
                page_type="profile_entitlement_add"
                setPropState={this.setPropState}
                ProfileYearID={this.state.HistoryYear}
              />*/}
            <div id="add_entitlement" data-backdrop="static" className="modal custom-modal" role="dialog">
                <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
                  <div className="modal-content">
                    <div className="modal-header">
                      <h5 className="modal-title">Create a Entitlements</h5>
                      <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={this.ClearRecord()}>
                        <span aria-hidden="true">×</span>
                      </button>
                    </div>
                    <div className="modal-body">
                      <div className="">
                        <div className="">
                          <form>
                            <div className="row">

                              <div className="col-sm-4 col-md-6">
                                <div className="form-group">
                                  <label>Year<span className="text-danger">*</span></label>
                                  <select className="form-control floating" id="AddentitlementYear" value={this.state.AddentitlementYear} onChange={this.handleChange('AddentitlementYear')}> 
                                    <option value="">-</option>
                                    {this.state.EntitlementYearList.map(( listValue, index ) => {
                                     
                                        return (
                                          <option key={index} value={listValue.guidId} >{listValue.name}</option>
                                        );
                                     
                                    })}
                                  </select>
                                  <span className="form-text error-font-color">{this.state.errormsg["AddentitlementYear"]}</span>
                                </div>
                              </div>

                              <div className="col-sm-4 col-md-6">
                                <div className="form-group">
                                  <label>Entitlement Category<span className="text-danger">*</span></label>
                                  <select className="form-control floating" id="AddentitlementCategory" value={this.state.AddentitlementCategory} onChange={this.handleChange('AddentitlementCategory')}> 
                                    <option value="">-</option>
                                    {this.state.EntitlementCategoryList.map(( listValue, index ) => {
                                     
                                        return (
                                          <option key={index} value={listValue.entitlementCategoryId} >{listValue.entitlementCategoryName}</option>
                                        );
                                     
                                    })}
                                  </select>
                                  <span className="form-text error-font-color">{this.state.errormsg["AddentitlementCategory"]}</span>
                                </div>
                              </div>

                              <div className="col-sm-4 col-md-6">
                                <div className="form-group">
                                  <label>Action<span className="text-danger">*</span></label>
                                  <select className="form-control floating" id="AddentitlementAction" value={this.state.AddentitlementAction} onChange={this.handleChange('AddentitlementAction')}> 
                                    <option value="">-</option>
                                    <option value="Add">Add</option>
                                    <option value="Subtract">Subtract</option>
                                  </select>
                                  <span className="form-text error-font-color">{this.state.errormsg["AddentitlementAction"]}</span>
                                </div>
                              </div>

                              

                              <div className="col-sm-4 col-md-6">
                                <div className="form-group">
                                  <label>Reason<span className="text-danger">*</span></label>
                                  <select className="form-control floating" id="AddentitlementSubCategory" value={this.state.AddentitlementSubCategory} onChange={this.handleChange('AddentitlementSubCategory')}> 
                                    <option value="">-</option>
                                    {this.state.EntitlementSubCategoryList.map(( listValue, index ) => {
                                     
                                        return (
                                          <option key={index} value={listValue.entitlementSubCategoryId} >{listValue.entitlementSubCategoryName}</option>
                                        );
                                     
                                    })}
                                  </select>
                                  <span className="form-text error-font-color">{this.state.errormsg["AddentitlementSubCategory"]}</span>
                                </div>
                              </div>
                              

                              <div className="col-sm-4 col-md-3" id="id_days_div">
                                <div className="form-group">
                                  <label>Days<span className="text-danger">*</span></label>
                                  <input type="number" className="form-control" value={this.state.AddentitlementQtydays} onChange={this.handleChange('AddentitlementQtydays')} />
                                  <span className="form-text error-font-color">{this.state.errormsg["AddentitlementQtydays"]}</span>
                                </div>
                              </div>

                              <div className="col-sm-4 col-md-3" id="id_hours_div">
                                <div className="form-group">
                                  <label>Hours<span className="text-danger">*</span></label>
                                  <input type="number" className="form-control" value={this.state.AddentitlementQtyhours} onChange={this.handleChange('AddentitlementQtyhours')} />
                                  <span className="form-text error-font-color">{this.state.errormsg["AddentitlementQtyhours"]}</span>
                                </div>
                              </div>

                              <div className="col-sm-4 col-md-3" id="id_minutes_div">
                                <div className="form-group">
                                  <label>Minutes<span className="text-danger">*</span></label>
                                  <select className="form-control floating" value={this.state.AddentitlementQtyminutes} onChange={this.handleChange('AddentitlementQtyminutes')}> 
                                    <option value="">-</option>
                                    <option value="00">00</option>
                                    <option value="15">15</option>
                                    <option value="30">30</option>
                                    <option value="45">45</option>
                                  </select>
                                  <span className="form-text error-font-color">{this.state.errormsg["AddentitlementQtyminutes"]}</span>
                                </div>
                              </div>
                              
                              <div className="col-sm-4 col-md-3">
                                <div className="form-group">
                                  <label>Unit<span className="text-danger">*</span></label>
                                  <input type="text" className="form-control" disabled value={this.state.AddentitlementUnit} onChange={this.handleChange('AddentitlementUnit')} style={{"background-color" : "#f6f6f6"}} />
                                  <span className="form-text error-font-color"></span>
                                </div>
                              </div>

                              <div className="col-md-12">
                                <div className="form-group">
                                  <label>Description<span className="text-danger">*</span></label>
                                  <textarea rows="3" cols="5" class="form-control" value={this.state.AddentitlementDescription} onChange={this.handleChange('AddentitlementDescription')}></textarea>
                                  <span className="form-text error-font-color">{this.state.errormsg["AddentitlementDescription"]}</span>
                                </div>
                              </div>

                              
                            </div>

                            <div className="submit-section">
                              <button className="btn btn-primary submit-btn">Cancel</button>
                              <button className="btn btn-primary submit-btn" onClick={this.AddRecord()}>Submit</button>
                            </div>

                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
          {/* //Entitlement Modal */}

          {/* Entitlement Ediit Modal */}
            
          {/* //Entitlement Edit Modal */}
        {/* /****************** Profile Entitlement Tab Modals ******************* */}
        </div>
      );
   }
}

export default ProfileLocation;
