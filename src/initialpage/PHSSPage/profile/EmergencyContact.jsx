/**
 * Signin Firebase
 */

import React, { Component } from 'react';
import { Helmet } from "react-helmet";

import InputMask from 'react-input-mask';



import PhoneInput from 'react-phone-input-2';
import 'react-phone-input-2/lib/style.css';

import Loader from '../../Loader';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import SystemHelpers from '../../Helpers/SystemHelper';
import moment from 'moment';

//table
import { MDBDataTable } from 'mdbreact';
import '@fortawesome/fontawesome-free/css/all.min.css';
import 'bootstrap-css-only/css/bootstrap.min.css';
import 'mdbreact/dist/css/mdb.css';
//table

import CryptoAES from 'crypto-js/aes';
import CryptoENC from 'crypto-js/enc-utf8';

import Datetime from "react-datetime";

class EmergencyContact extends Component {
  constructor(props) {
    super(props);

    this.state = {
        errormsg :  '',
        user_role: [],
        ListGrid : [],
        staffContactID:this.props.staffContactID,
        emgRelationShipTypeList : [],
        emgContactypeList : [],

        ListCountry: [],
        ListProvince:[],
        ListCity:[],

        // Edit model Start
        "EditaddressType": 0,
        "EditfirstName": "",
        "EditlastName": "",
        "EditrelationShip": "",
        "EditphoneNumber": "",
        "EditalternetPhoneNumber": '',
        "Editemail": "",
        "Editnote": "",
        "EditemgContactId": "",
        "EditphoneNumberCountryCode": "",
        "EditalternetPhoneNumberCountryCode": '',
        "EditStartDate":'',
        "EditEndDate":'',
        "EditAddress1Line1":'',
        "EditCountry":'',
        "EditProvince":'',
        "EditCity":'',
        "EditPostalCode":'',
        // Edit model Start

        // Add model Start
        "AddaddressType": 0,
        "AddfirstName": "",
        "AddlastName": "",
        "AddrelationShip": "",
        "AddphoneNumber": "",
        "AddalternetPhoneNumber": '',
        "Addemail": "",
        "Addnote": "",
        "AddphoneNumberCountryCode": "",
        "AddalternetPhoneNumberCountryCode": '',
        "AddStartDate":'',
        "AddEndDate":'',
        "AddAddress1Line1":'',
        "AddCountry":'',
        "AddProvince":'',
        "AddCity":'',
        "AddPostalCode":'',
        // Add model Start
        role_emergency_contact_can: {},

        isDelete : false,

        header_data : [],

        staffContactFullname : localStorage.getItem('fullName')
    };
    this.setPropState = this.setPropState.bind(this);
    this.handleChange = this.handleChange.bind(this);

    this.handleAddStartDate = this.handleAddStartDate.bind(this);
    this.handleAddEndDate = this.handleAddEndDate.bind(this);

    this.handleEditStartDate = this.handleEditStartDate.bind(this);
    this.handleEditEndDate = this.handleEditEndDate.bind(this);
  }


  handleAddStartDate = (date) =>{
    console.log('AddStartDate => '+ date);
    this.setState({ AddStartDate : date });
    this.setState({ AddEndDate : '' });
  };

  handleAddEndDate = (date) =>{
    console.log('AddEndDate => '+ date);
    this.setState({ AddEndDate : date });
  };

  handleEditStartDate = (date) =>{
    console.log('EditStartDate => '+ date);
    this.setState({ EditStartDate : date });
    this.setState({ EditEndDate : '' });
  };

  handleEditEndDate = (date) =>{
    console.log('EditEndDate => '+ date);
    this.setState({ EditEndDate : date });
  };

  validationAddEndDate = (currentDate) => {
    return currentDate.isAfter(moment(this.state.AddStartDate));
  };

  validationEditEndDate = (currentDate) => {
    return currentDate.isAfter(moment(this.state.EditStartDate));
  };

  setPropState(key, value) {
      this.setState({ [key]: value });
  }

  handleChange = input => e => {
    this.setState({ [input]: e.target.value.replace(/^\s+/g, '') });
    //console.log(input);
    if (this.state[input] != '') {
        delete this.state.errormsg[input];
    }
    if([input]=="AddStartDate")
    {
      //this.setState({ AddEndDate: moment(e.target.value).format('YYYY-MM-DD') });
      this.setState({ AddEndDate: '' });
    }

    if([input]=="EditStartDate")
    {
      //this.setState({ EditEndDate: moment(e.target.value).format('YYYY-MM-DD') });
      this.setState({ EditEndDate: '' });
    }

    // Add time
    if([input]=="AddCountry")
    {
      this.setState({ ListProvince: [] });
      this.setState({ ListCity: [] });

      this.GetProvince(e.target.value);
      this.setState({ AddProvince: '' });
      this.setState({ AddCity: '' });
    }

    if([input]=="AddProvince")
    {
      this.setState({ ListCity: [] });

      this.GetCity(e.target.value);
      this.setState({ AddCity: '' });
    }
    // Add time

    // Edit time
    if([input]=="EditCountry")
    {
      this.setState({ ListProvince: [] });
      this.setState({ ListCity: [] });

      this.GetProvince(e.target.value);
      this.setState({ EditProvince: '' });
      this.setState({ EditCity: '' });
    }

    if([input]=="EditProvince")
    {
      this.setState({ ListCity: [] });

      this.GetCity(e.target.value);
      this.setState({ EditCity: '' });
    } 
    // Edit time
  }

  // Loader hide show method
  hideLoader = () => {
    this.setState({ loading: false });
  }

  showLoader = () => {
    this.setState({ loading: true });
  }
  // Loader hide show method

  GetCountry(){
    this.showLoader();
    var url=process.env.API_API_URL+'GetCountry';
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        //'Authorization': 'bearer '+localStorage.getItem("token")
      },
      //body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
        console.log("responseJson GetCountry");
        console.log(data);
        //console.log(data.data.userRole);
        // debugger;
        if (data.responseType === "1") {
            // Profile & Contact
            if(data.data != null){
               this.setState({ ListCountry: data.data});
            } 
        }else{
          SystemHelpers.ToastError(data.message);   
        }
        this.hideLoader();
        
    })
    .catch(error => {
      this.props.history.push("/error-500");
    });
  }

  GetProvince(id){
    this.showLoader();
    var url=process.env.API_API_URL+'GetProvince?id='+id;
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        //'Authorization': 'bearer '+localStorage.getItem("token")
      },
      //body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
        console.log("responseJson GetProvince");
        console.log(data);
        //console.log(data.data.userRole);
        // debugger;
        if (data.responseType === "1") {
            // Profile & Contact
            if(data.data != null){
               this.setState({ ListProvince: data.data});
            } 
        }else{
          SystemHelpers.ToastError(data.message);   
        }
        this.hideLoader();
        
    })
    .catch(error => {
      this.props.history.push("/error-500");
    });
  }

  GetCity(id){
    this.showLoader();
    var url=process.env.API_API_URL+'GetCity?id='+id;
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        //'Authorization': 'bearer '+localStorage.getItem("token")
      },
      //body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
        console.log("responseJson GetCity");
        console.log(data);
        //console.log(data.data.userRole);
        // debugger;
        if (data.responseType === "1") {
            // Profile & Contact
            if(data.data != null){
               this.setState({ ListCity: data.data});
            } 
        }else{
          SystemHelpers.ToastError(data.message);   
        }
        this.hideLoader();
        
    })
    .catch(error => {
      this.props.history.push("/error-500");
    });
  }

  componentDidMount() {
    console.log("EmergencyContact");
    
    /* Role Management */
    console.log('Role Store emergency_contact_can');
    /*var getrole = SystemHelpers.GetRole();
    let emergency_contact_can = getrole.emergency_contact_can;
    this.setState({ role_emergency_contact_can: emergency_contact_can });
    console.log(emergency_contact_can);*/

    console.log(this.props.emergency_contact_can);
    let emergency_contact_can = this.props.emergency_contact_can;
    this.setState({ role_emergency_contact_can: this.props.emergency_contact_can });
    /* Role Management */

    console.log(localStorage.getItem("token"));

    // $(this.refs['EditaddressType']).select2({
    //      change: this.handleChange
    // });

    // Start OnLoad Methode Hide

    //this.GetCountry();
    //this.GetEmergencyContact();

    // End OnLoad Methode Hide

    // Delete Permison
    if(emergency_contact_can.emergency_contact_can_delete == true){
      var columns = [
                {
                  label: 'Emergency Contact Type',
                  field: 'addressType',
                  sort: 'asc',
                  width: 150
                },
                {
                  label: 'Name',
                  field: 'name',
                  sort: 'asc',
                  width: 150
                },
                {
                  label: 'Relationship',
                  field: 'relationShip',
                  sort: 'asc',
                  width: 150
                },
                {
                  label: 'Phone Number',
                  field: 'phoneNumber',
                  sort: 'asc',
                  width: 150
                },
                {
                  label: 'Alternate Phone',
                  field: 'alternetPhoneNumber',
                  sort: 'asc',
                  width: 150
                },
                {
                  label: 'Email',
                  field: 'email',
                  sort: 'asc',
                  width: 150
                },
                {
                  label: 'Status',
                  field: 'status',
                  sort: 'asc',
                  width: 150
                },
                {
                  label: 'Action',
                  field: 'action',
                  width: 270
                }
              ];

      this.setState({ header_data: columns });
    }else{
      var columns = [
                {
                  label: 'Emergency Contact Type',
                  field: 'addressType',
                  sort: 'asc',
                  width: 150
                },
                {
                  label: 'Name',
                  field: 'name',
                  sort: 'asc',
                  width: 150
                },
                {
                  label: 'Relationship',
                  field: 'relationShip',
                  sort: 'asc',
                  width: 150
                },
                {
                  label: 'Phone Number',
                  field: 'phoneNumber',
                  sort: 'asc',
                  width: 150
                },
                {
                  label: 'Alternate Phone',
                  field: 'alternetPhoneNumber',
                  sort: 'asc',
                  width: 150
                },
                {
                  label: 'Email',
                  field: 'email',
                  sort: 'asc',
                  width: 150
                },
                {
                  label: 'Action',
                  field: 'action',
                  width: 270
                }
              ];

      this.setState({ header_data: columns });
    }
    // Delete Permison
  }

  TabClickOnLoadEmergencyContacts = () => e => {
    //debugger;
    e.preventDefault();

    this.GetCountry();
    this.GetEmergencyContact();
  }

  //
  handleOnChangePhoneMobileAdd(value, data, event, formattedValue)
  {
    var phone = value;
    var str2 = "+";
    var country_code=data.dialCode;
    if(phone.indexOf(str2) != -1){
        console.log(str2 + " found");
    }else{
        value='+'+value;
        country_code="+"+data.dialCode;
    }
    this.setState({ AddphoneNumber: value});
    this.setState({ AddphoneNumberCountryCode : country_code });   
  }
  handleOnChangeAlternetMobileAdd(value, data, event, formattedValue)
  {
    var phone = value;
    var str2 = "+";
    var country_code=data.dialCode;
    if(phone.indexOf(str2) != -1){
        console.log(str2 + " found");
    }else{
        value='+'+value;
        country_code="+"+data.dialCode;
    }
    this.setState({ AddalternetPhoneNumber: value});
    this.setState({ AddalternetPhoneNumberCountryCode : country_code });   
  }
  handleOnChangePhoneMobileEdit(value, data, event, formattedValue)
  {
    var phone = value;
    var str2 = "+";
    var country_code=data.dialCode;
    if(phone.indexOf(str2) != -1){
        console.log(str2 + " found");
    }else{
        value='+'+value;
        country_code="+"+data.dialCode;
    }
    this.setState({ EditphoneNumber: value});
    this.setState({ EditphoneNumberCountryCode : country_code });   
  }
  handleOnChangeAlternetMobileEdit(value, data, event, formattedValue)
  {
    var phone = value;
    var str2 = "+";
    var country_code=data.dialCode;
    if(phone.indexOf(str2) != -1){
        console.log(str2 + " found");
    }else{
        value='+'+value;
        country_code="+"+data.dialCode;
    }
    this.setState({ EditalternetPhoneNumber: value});
    this.setState({ EditalternetPhoneNumberCountryCode : country_code });   
  }
  //

  GetEmergencyContact(){

    /* Role Management */
    var getrole = SystemHelpers.GetRole();
    /* Role Management */

    let canDelete = getrole.emergency_contact_can.emergency_contact_can_delete;

    this.showLoader();
    var url=process.env.API_API_URL+'GetUserEmergencyAddress?contactId='+this.state.staffContactID+'&canDelete='+canDelete;
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
      //body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
        console.log("responseJson GetEmergencyContact");
        console.log(data);
        //console.log(data.data.userRole);
        // debugger;
        if (data.responseType === "1") {
            // Profile & Contact
            if(data.data != null){
              //this.setState({ ListGrid: data.data.userEmergencyContactViews });
              this.setState({ ListGrid: this.rowData(data.data.userEmergencyContactViews) })
              this.setState({ emgContactypeList: data.data.emgContactypeList });
              this.setState({ emgRelationShipTypeList: data.data.emgRelationShipTypeList });
              
            }
            
            
        }else{
              if(data.message == 'Authorization has been denied for this request.'){
                SystemHelpers.SessionOut();
                this.props.history.push("/login");
              }else{
                SystemHelpers.ToastError(data.message);
              }
              
        }
        this.hideLoader();
        
    })
    .catch(error => {
      this.props.history.push("/error-500");
    });
  }

  Edit_Update_Btn_Func(emgContactId){
    //console.log("Edit_Update_Btn_Func");
    //console.log(emgContactId);

    let return_push = [];

    if(this.state.role_emergency_contact_can.emergency_contact_can_update == true || this.state.role_emergency_contact_can.emergency_contact_can_delete == true){
      let Edit_push = [];
      if(this.state.role_emergency_contact_can.emergency_contact_can_update == true && emgContactId.isDelete == false){
        Edit_push.push(
          <a href="#" onClick={this.EditRecord(emgContactId)} className="dropdown-item" data-toggle="modal" data-target="#EmergencyContacts_emergency_contacts_Edit_modal"><i className="fa fa-pencil m-r-5" ></i> Edit</a>
        );
      }
      let Delete_push = [];
      if(this.state.role_emergency_contact_can.emergency_contact_can_delete == true){
        if(emgContactId.isDelete == false)
        {  
          Delete_push.push(
            <a href="#" onClick={this.EditRecord(emgContactId)} data-toggle="modal" data-target="#delete_emergency_contact" className="dropdown-item"><i className="fa fa-trash-o m-r-5" ></i> Inactive</a>
          );
        }
        else
        {
          Delete_push.push(
            <a href="#" onClick={this.EditRecord(emgContactId)} data-toggle="modal" data-target="#delete_emergency_contact" className="dropdown-item"><i className="fa fa-trash-o m-r-5" ></i> Active</a>
          );
        }
      }
      
      return_push.push(
        <div className="dropdown dropdown-action">
          <a aria-expanded="false" data-toggle="dropdown" className="action-icon dropdown-toggle" href="#"><i className="material-icons">more_vert</i></a>
          <div className="dropdown-menu dropdown-menu-right">
            {Edit_push}
            {Delete_push}
          </div>
        </div>
      );
    }
    return return_push;
  }

  EditRecord = (record) => e => {
    e.preventDefault();

    console.log("edit Emergency Contact");
    console.log(record);

    this.setState({ errormsg: '' });

    this.setState({ EditaddressType: record.addressType });
    this.setState({ EditfirstName: record.firstName });
    this.setState({ EditlastName: record.lastName });
    this.setState({ EditrelationShip: record.relationShip });
    this.setState({ EditphoneNumber: record.phoneNumber });
    if(record.alternetPhoneNumber == null || record.alternetPhoneNumber == ''){
      this.setState({ EditalternetPhoneNumber: '' });
    }else{
      this.setState({ EditalternetPhoneNumber: record.alternetPhoneNumber });
    }
    
    this.setState({ Editemail: record.email });
    this.setState({ EditemgContactId: record.emgContactId });
    //$("#EditaddressType").select2("val", record.addressType);
    //$('#EditaddressType').val(record.addressType).trigger('change');
    //alert(emgContactId);
    //this.setState({ EditStartDate: moment(record.startDate).format('YYYY-MM-DD') });
    this.setState({ EditStartDate: moment(record.startDate,process.env.API_DATE_FORMAT) });

    if(record.endDate != ""){
      //this.setState({ EditEndDate: moment(record.endDate).format('YYYY-MM-DD') });
      this.setState({ EditEndDate: moment(record.endDate,process.env.API_DATE_FORMAT) });
    }else
    {
      this.setState({ EditEndDate: "" });
    }
    

    this.setState({ EditAddress1Line1: record.address });
    this.setState({ EditCountry: record.countryId });
    this.setState({ EditProvince: record.provinceId });
    this.setState({ EditCity: record.cityId });
    this.setState({ EditPostalCode: record.zipCode });
    this.setState({ Editnote: record.notes });

    this.GetProvince(record.countryId);
    this.GetCity(record.provinceId);

    this.setState({ isDelete: record.isDelete });

    var alternetPhoneNumberCode = record.alternetPhoneNumber;
    
    if(alternetPhoneNumberCode != null && alternetPhoneNumberCode != ''){
      var CountryCodeCell = alternetPhoneNumberCode.substring(0, alternetPhoneNumberCode.length-10)
      this.setState({ EditalternetPhoneNumberCountryCode: CountryCodeCell });
    }
  }

  UpdateRecord = () => e => {
    //debugger;
    e.preventDefault();

    let step1Errors = {};
    
    
    if (this.state["EditaddressType"] == '' || this.state["EditaddressType"] == null) {
      step1Errors["EditaddressType"] = "Please Select Contact Type.";
    }
    
    
    if (this.state["EditrelationShip"] == '' || this.state["EditfirstName"] == null) {
      step1Errors["EditrelationShip"] = "RelationShip is mandatory.";
    }

    if (this.state["EditfirstName"] == "" || this.state["EditfirstName"] == null) {
      step1Errors["EditfirstName"] = "First Name is mandatory";
    }

    if (this.state["EditlastName"] == "" || this.state["EditlastName"] == null) {
      step1Errors["EditlastName"] = "Last Name is mandatory";
    }

    var EditphoneNumber =this.state["EditphoneNumber"];
    //var EditphoneNumberCountryCode =this.state["EditphoneNumberCountryCode"];
    var EditphoneNumberCountryCode =EditphoneNumber.length - 10;
    
    if (EditphoneNumber == '' || EditphoneNumber == null) {
        step1Errors["EditphoneNumber"] = "Phone number is mandatory";
    }
    else {
      //var totalPhoneEdit = EditphoneNumber.length - EditphoneNumberCountryCode.length;
      var totalPhoneEdit = EditphoneNumber.length - EditphoneNumberCountryCode;
      if(totalPhoneEdit!=10) {
        step1Errors["EditphoneNumber"] = "Please Enter a Valid Phone Number.";
      }
    }

    var EditalternetPhoneNumber =this.state["EditalternetPhoneNumber"];
    var EditalternetPhoneNumberCountryCode =this.state["EditalternetPhoneNumberCountryCode"];
    //var EditalternetPhoneNumberCountryCode = EditalternetPhoneNumber.length - 10;

    if(EditalternetPhoneNumber != '' && EditalternetPhoneNumber != '+'){
      
      var totalAlterPhoneEdit = EditalternetPhoneNumber.length - EditalternetPhoneNumberCountryCode.length;
      
      // alert(EditalternetPhoneNumber.length);
      // alert(EditalternetPhoneNumberCountryCode.length);
      // alert(totalAlterPhoneEdit);

      if(totalAlterPhoneEdit!=10) {
        step1Errors["EditalternetPhoneNumber"] = "Please Enter a Valid Alternate Phone Number.";
      }
    }
    
    
    if(EditalternetPhoneNumber == '+'){
      EditalternetPhoneNumber = '';
    }

    if (this.state["Editemail"] == '' || this.state["Editemail"] == null) {
        step1Errors["Editemail"] = "Email is mandatory";
    }else if (typeof this.state["Editemail"] !== "undefined") {
        let lastAtPos = this.state["Editemail"].lastIndexOf('@');
        let lastDotPos = this.state["Editemail"].lastIndexOf('.');

        if (!(lastAtPos < lastDotPos && lastAtPos > 0 && this.state["Editemail"].indexOf('@@') == -1 && lastDotPos > 2 && (this.state["Editemail"].length - lastDotPos) > 2)) {
            step1Errors["Editemail"] = "Email is not valid";
        }
    }

    if (this.state["EditStartDate"] == '') {
      step1Errors["EditStartDate"] = "Start Date is mandatory";
    }

    console.log(step1Errors);

    this.setState({ errormsg: step1Errors });
        
    if (Object.keys(step1Errors).length > 0) {
        return false;
    }


    this.showLoader();

    var EditStartDate=moment(this.state["EditStartDate"]).format('MM-DD-YYYY');
    var NewEditStartDate = moment(EditStartDate, "MM-DD-YYYY").add(1, 'days');
    var edit_startdate=moment(NewEditStartDate).format('MM-DD-YYYY');
    //alert(this.state["EditEndDate"]);
    var edit_enddate ='';
    if (this.state["EditEndDate"] != '' && this.state["EditEndDate"] != '')
    {
      var EditEndDate=moment(this.state["EditEndDate"]).format('MM-DD-YYYY');
      var NewEditEndDate = moment(EditEndDate, "MM-DD-YYYY").add(1, 'days');
      var edit_enddate=moment(NewEditEndDate).format('MM-DD-YYYY');
    }
    //alert(edit_enddate);
    // console.log(NewEditStartDate);
    // console.log(edit_enddate);
    // return false;
    //Invalid date
    let ArrayJson = {
          addressType: this.state["EditaddressType"],
          firstName: this.state["EditfirstName"],
          lastName: this.state["EditlastName"],
          relationShip: this.state["EditrelationShip"],
          phoneNumber: EditphoneNumber,
          alternetPhoneNumber: EditalternetPhoneNumber,
          emgContactId: this.state["EditemgContactId"],
          email : this.state["Editemail"],
          startDate : edit_startdate,
          endDate : edit_enddate,

          address: this.state["EditAddress1Line1"],
          countryId: this.state["EditCountry"],
          provinceId: this.state["EditProvince"],
          cityId: this.state["EditCity"],
          zipCode: this.state["EditPostalCode"],
          notes: this.state["Editnote"]
    };
    
    let ArrayJson11 = {
          AddressType: this.state["EditaddressType"],
          FirstName: this.state["EditfirstName"],
          LastName: this.state["EditlastName"],
          RelationShip: this.state["EditrelationShip"],
          PhoneNumber: EditphoneNumber,
          AlternetPhoneNumber: EditalternetPhoneNumber,
          EmgContactId: this.state["EditemgContactId"],
          Email : this.state["Editemail"],
          StartDate : NewEditStartDate,
          EndDate : edit_enddate,

          Address: this.state["EditAddress1Line1"],
          CountryId: this.state["EditCountry"],
          ProvinceId: this.state["EditProvince"],
          CityId: this.state["EditCity"],
          ZipCode: this.state["EditPostalCode"],
          Notes: this.state["Editnote"]
    };

    let bodyarray = {};
    bodyarray["contactId"] = this.state.staffContactID;
    bodyarray["userEmergencyContactView"] = ArrayJson;
    bodyarray["userName"] = this.state.staffContactFullname;

    console.log(bodyarray);
    //return false;
    var url=process.env.API_API_URL+'UpdateEmergencyUserAddress';
    fetch(url, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
      body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
        console.log("responseJson UpdateEmergencyUserAddress");
        console.log(data);
        //console.log(responseJson);
        // debugger;
        if (data.responseType === "1") {
            //this.props.history.push('/dashboard');
            SystemHelpers.ToastSuccess(data.responseMessge);  
            $( ".close" ).trigger( "click" ); 
            this.GetEmergencyContact();
        }else if (data.responseType === "2") {
          //SystemHelpers.ToastSuccess(data.responseMessge); 
          SystemHelpers.ToastError(data.responseMessge);   
          //$( ".close" ).trigger( "click" ); 
          //this.GetEmergencyContact();
        }
        else{
            SystemHelpers.ToastError(data.message);
        }
        this.hideLoader();
        
    })
    .catch(error => {
      this.props.history.push("/error-500");
    });
  }

  ClearRecord = ()=> e => {
    e.preventDefault();

    this.setState({ AddaddressType: '' });
    this.setState({ AddrelationShip: '' });
    this.setState({ AddfirstName: '' });
    this.setState({ AddlastName: '' });
    this.setState({ AddphoneNumber: '' });
    this.setState({ AddphoneNumberCountryCode: '' });
    this.setState({ AddalternetPhoneNumber : '' });
    this.setState({ AddalternetPhoneNumberCountryCode: '' });
    this.setState({ Addemail: '' });
    this.setState({ Addnote: '' });

    this.setState({ AddStartDate: '' });
    this.setState({ AddEndDate: '' });
    this.setState({ AddAddress1Line1: '' });
    this.setState({ AddCountry: '' });
    this.setState({ AddProvince: '' });
    this.setState({ AddCity: '' });
    this.setState({ AddPostalCode: '' });

    this.setState({ ListProvince: [] });
    this.setState({ ListCity: [] });

    this.setState({ errormsg: '' });
  }
  
  AddRecord = () => e => {
    //debugger;
    e.preventDefault();

    let step1Errors = {};

    //alert(this.state.["AddEndDate"]);
    
    if (this.state["AddaddressType"] == '' || this.state["AddfirstName"] == null) {
      step1Errors["AddaddressType"] = "Please Select Contact Type.";
    }
    
     
    if (this.state["AddrelationShip"] == '' || this.state["AddrelationShip"] == null) {
      step1Errors["AddrelationShip"] = "RelationShip is mandatory.";
    }

    if (this.state["AddfirstName"] == "" || this.state["AddfirstName"] == null) {
      step1Errors["AddfirstName"] = "First Name is mandatory";
    }

    if (this.state["AddlastName"] == "" || this.state["AddlastName"] == null) {
      step1Errors["AddlastName"] = "Last Name is mandatory";
    }

    var AddphoneNumber =this.state["AddphoneNumber"];
    var AddphoneNumberCountryCode =this.state["AddphoneNumberCountryCode"];
    if (this.state["AddphoneNumber"] == '' || this.state["AddphoneNumber"] == null) {
      step1Errors["AddphoneNumber"] = "Phone number is mandatory";
    }
    else {
      var totalPhoneAdd = AddphoneNumber.length - AddphoneNumberCountryCode.length;
      if(totalPhoneAdd!=10) {
        step1Errors["AddphoneNumber"] = "Please Enter a Valid Phone Number.";
      }
    }

    var AddalternetPhoneNumber =this.state["AddalternetPhoneNumber"];
    var AddalternetPhoneNumberCountryCode =this.state["AddalternetPhoneNumberCountryCode"];
    if(AddalternetPhoneNumber != '' && AddalternetPhoneNumber != '+'){
      var totalAlternetPhone = AddalternetPhoneNumber.length - AddalternetPhoneNumberCountryCode.length;
      if(totalAlternetPhone!=10) {
        step1Errors["AddalternetPhoneNumber"] = "Please Enter a Valid Alternate Phone Number.";
      }
    }

    if(AddalternetPhoneNumber == '+'){
        AddalternetPhoneNumber = '';
    }
    
    if (this.state["Addemail"] == '' || this.state["Addemail"] == null) {
        step1Errors["Addemail"] = "Email is mandatory";
    }else if (typeof this.state["Addemail"] !== "undefined") {
        let lastAtPos = this.state["Addemail"].lastIndexOf('@');
        let lastDotPos = this.state["Addemail"].lastIndexOf('.');

        if (!(lastAtPos < lastDotPos && lastAtPos > 0 && this.state["Addemail"].indexOf('@@') == -1 && lastDotPos > 2 && (this.state["Addemail"].length - lastDotPos) > 2)) {
            step1Errors["Addemail"] = "Email is not valid";
        }
    }

    if (this.state["AddStartDate"] == '') {
      step1Errors["AddStartDate"] = "Start Date is mandatory";
    }

    console.log(step1Errors);

    this.setState({ errormsg: step1Errors });
        
    if (Object.keys(step1Errors).length > 0) {
        return false;
    }

    this.showLoader();

    var AddStartDate=moment(this.state["AddStartDate"]).format('MM-DD-YYYY');
    var NewAddStartDate = moment(AddStartDate, "MM-DD-YYYY").add(1, 'days');

    var add_enddate ='';
    if (this.state["AddEndDate"] != '') {
      var AddEndDate=moment(this.state["AddEndDate"]).format('MM-DD-YYYY');
      var NewAddEndDate = moment(AddEndDate, "MM-DD-YYYY").add(1, 'days');
      var add_enddate=moment(NewAddEndDate).format('MM-DD-YYYY');
    }

    let ArrayJson = {
          addressType: this.state["AddaddressType"],
          firstName: this.state["AddfirstName"],
          lastName: this.state["AddlastName"],
          relationShip: this.state["AddrelationShip"],
          phoneNumber: AddphoneNumber,
          alternetPhoneNumber: AddalternetPhoneNumber,
          email : this.state["Addemail"],
          startDate : NewAddStartDate,
          endDate : add_enddate,

          address: this.state["AddAddress1Line1"],
          countryId: this.state["AddCountry"],
          provinceId: this.state["AddProvince"],
          cityId: this.state["AddCity"],
          zipCode: this.state["AddPostalCode"],
          notes: this.state["Addnote"]
    };
     
    let bodyarray = {};
    bodyarray["contactId"] = this.state.staffContactID;
    bodyarray["userEmergencyContactView"] = ArrayJson;

    console.log(bodyarray);
    //return false;
    var url=process.env.API_API_URL+'CreateEmergencyUserAddress';
    fetch(url, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
      body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
        console.log("responseJson CreateEmergencyUserAddress");
        console.log(data);
        //console.log(responseJson);
        // debugger;
        if (data.responseType === "1") {
            //this.props.history.push('/dashboard');

            this.setState({ AddaddressType: '' });
            this.setState({ AddrelationShip: '' });
            this.setState({ AddfirstName: '' });
            this.setState({ AddlastName: '' });
            this.setState({ AddphoneNumber: '' });
            this.setState({ AddphoneNumberCountryCode: '' });
            this.setState({ AddalternetPhoneNumber : '' });
            this.setState({ AddalternetPhoneNumberCountryCode: '' });
            this.setState({ Addemail: '' });
            this.setState({ Addnote: '' });

            this.setState({ AddStartDate: '' });
            this.setState({ AddEndDate: '' });
            this.setState({ AddAddress1Line1: '' });
            this.setState({ AddCountry: '' });
            this.setState({ AddProvince: '' });
            this.setState({ AddCity: '' });
            this.setState({ AddPostalCode: '' });

            this.setState({ ListProvince: [] });
            this.setState({ ListCity: [] });

            SystemHelpers.ToastSuccess(data.responseMessge);  
            $( ".close" ).trigger( "click" ); 
            this.GetEmergencyContact();
        }else if (data.responseType === "2") {
          //SystemHelpers.ToastSuccess(data.responseMessge); 
          SystemHelpers.ToastError(data.responseMessge);   
          //$( ".close" ).trigger( "click" ); 
          //this.GetEmergencyContact();
        }
        else{
            SystemHelpers.ToastError(data.message);
        }
        this.hideLoader();
        
    })
    .catch(error => {
      this.props.history.push("/error-500");
    });
  }


  DeleteRecord = () => e => {
    e.preventDefault();

    var isdelete = '';
    if(this.state.isDelete== true)
    {
      isdelete = false;
    }
    else
    {
      isdelete = true;
    }

    this.showLoader();
    var url=process.env.API_API_URL+'DeleteUserEmergencyAddress?contactId='+this.state.EditemgContactId+'&isDelete='+isdelete+'&userName='+this.state.staffContactFullname;
    fetch(url, {
      method: 'PUT',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
      //body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
        console.log("responseJson DeleteUserEmergencyAddress");
        console.log(data);
        //console.log(data.data.userRole);
        // debugger;
        if (data.responseType === "1") {
            // Profile & Contact
            SystemHelpers.ToastSuccess(data.responseMessge);
            $( ".cancel-btn" ).trigger( "click" );
            this.GetEmergencyContact();
            this.hideLoader();
        }else if (data.responseType == "2" || data.responseType == "3") {
            SystemHelpers.ToastError(data.responseMessge);
            $( ".cancel-btn" ).trigger( "click" );
            this.hideLoader();
        }else{
              if(data.message == 'Authorization has been denied for this request.'){
                SystemHelpers.SessionOut();
                this.props.history.push("/login");
              }else{
                SystemHelpers.ToastError(data.message);
              }
              this.hideLoader();
              $( ".cancel-btn" ).trigger( "click" );
        }
        
        
    })
    .catch(error => {
      this.props.history.push("/error-500");
    });
  }

  rowData(ListGrid) {
    //console.log(userList)
    
    /* Role Management */
    var getrole = SystemHelpers.GetRole();
    let canDelete = getrole.emergency_contact_can.emergency_contact_can_delete;
    /* Role Management */

    

      var ListGrid_length = ListGrid.length;
      let dataArray = [];
      var i=1;
      for (var z = 0; z < ListGrid_length; z++) {
        var tempdataArray = [];
        //tempdataArray.rownum = i;
        tempdataArray.addressType = ListGrid[z].addressType;
        tempdataArray.name = ListGrid[z].firstName +" "+ ListGrid[z].lastName;
        tempdataArray.relationShip = ListGrid[z].relationShip;
        tempdataArray.phoneNumber = ListGrid[z].phoneNumber;
        tempdataArray.alternetPhoneNumber = ListGrid[z].alternetPhoneNumber;
        tempdataArray.email = ListGrid[z].email;

        if(canDelete == true){
          var status = "";
          if(ListGrid[z].isDelete == true){
            tempdataArray.status = <div><span class="badge bg-inverse-warning">Inactive</span></div>;
          }else{
            tempdataArray.status = <div><span class="badge bg-inverse-success">Active</span></div>;
          }
        }

        tempdataArray.action = this.Edit_Update_Btn_Func(ListGrid[z]);

        dataArray.push(tempdataArray);
        i++;
      }

      return dataArray;
  }

  render() {
    const { EditaddressType, EditfirstName,EditlastName, EditrelationShip, EditphoneNumber,EditalternetPhoneNumber, Editemail, Editnote } = this.props; 
    const data = {
              columns: this.state.header_data,
              rows: this.state.ListGrid
            };
    return (
            <div>
              
            {/* Toast & Loder method use */}
            
            {(this.state.loading) ? <Loader /> : null} 
            {/* Toast & Loder method use */}
              <div className="row">
                <div className="col-md-12 d-flex">
                  <div className="card profile-box flex-fill">
                    <div className="row">
                      <button className="btn btn-primary submit-btn pk-profiletab-refreshbtn-hide" id="TabClickOnLoadEmergencyContacts" onClick={this.TabClickOnLoadEmergencyContacts()}>Refresh</button>
                    </div>
                    <div className="card-body">
                      {this.state.role_emergency_contact_can.emergency_contact_can_create == true ?
                        <h3 className="card-title">Emergency Contacts<a href="#" className="edit-icon" data-toggle="modal" data-target="#EmergencyContacts_emergency_contacts_Add_modal"><i className="fa fa-plus" /></a></h3>
                        : <h3 className="card-title">Emergency Contacts <a href="#" className="phss-lock"><i className="fa fa-lock" /></a></h3>
                      }
                      
                      <div className="table-responsive">
                        <MDBDataTable
                            striped
                            bordered
                            small
                            data={data}
                            entriesOptions={[5, 10, 20, 50, 100]} entries={5} pagesAmount={4}
                            className="table table-striped custom-table mb-0 datatable"
                          />
                      </div>
                    </div>
                  </div>
                </div>
              </div>

            {/* ************ Emergency Contacts Tab Modals ************* */}
            {/* Emergency Contacts Add Modal */}
            <div id="EmergencyContacts_emergency_contacts_Add_modal" className="modal custom-modal fade" role="dialog">
              <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div className="modal-content">
                  <div className="modal-header">
                    <h5 className="modal-title">Emergency Contacts</h5>
                    <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={this.ClearRecord()} >
                      <span aria-hidden="true">×</span>
                    </button>
                  </div>
                  <div className="modal-body">
                    <form>
                      <div className="card">
                        <div className="card-body">
                          <div className="row">
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Contact Type<span className="text-danger">*</span></label>
                                <select  className="form-control" value={this.state.AddaddressType}  onChange={this.handleChange('AddaddressType')}>
                                  <option value="">      -       </option>
                                  {this.state.emgContactypeList.map(( listValue, index ) => {
                                    return (
                                      <option key={index} value={listValue.name}>{listValue.name}</option>
                                    );
                                  })}
                                </select>
                                <span className="form-text error-font-color">{this.state.errormsg["AddaddressType"]}</span>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Relationship<span className="text-danger">*</span></label>
                                <select  className="form-control" value={this.state.AddrelationShip}  onChange={this.handleChange('AddrelationShip')}>
                                  <option value="">      -       </option>
                                  {this.state.emgRelationShipTypeList.map(( listValue, index ) => {
                                    return (
                                      <option key={index} value={listValue.name}>{listValue.name}</option>
                                    );
                                  })}
                                </select>
                                <span className="form-text error-font-color">{this.state.errormsg["AddrelationShip"]}</span>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>First Name<span className="text-danger">*</span></label>
                                <input type="text" className="form-control" value={this.state.AddfirstName} onChange={this.handleChange('AddfirstName')} />
                                <span className="form-text error-font-color">{this.state.errormsg["AddfirstName"]}</span>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Last Name<span className="text-danger">*</span></label>
                                <input type="text" className="form-control" value={this.state.AddlastName} onChange={this.handleChange('AddlastName')} />
                                <span className="form-text error-font-color">{this.state.errormsg["AddlastName"]}</span>
                              </div>
                            </div>
                            
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Phone Number<span className="text-danger">*</span></label>
                                {/*<InputMask className="form-control" mask="(999) 999-9999" value={this.state.AddphoneNumber} onChange={this.handleChange('AddphoneNumber')} ></InputMask>*/}
                                <PhoneInput
                                  inputClass='form-control'
                                  inputStyle= {{'width': '100%'}}
                                  country={'ca'}
                                  onlyCountries={['ca', 'in' ,'us']}
                                  value={this.state.AddphoneNumber}
                                  onChange={this.handleOnChangePhoneMobileAdd.bind(this)}
                                />
                                <span className="form-text error-font-color">{this.state.errormsg["AddphoneNumber"]}</span>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Alternate Phone</label>
                                {/*<InputMask className="form-control" mask="(999) 999-9999" value={this.state.AddalternetPhoneNumber} onChange={this.handleChange('AddalternetPhoneNumber')} ></InputMask>*/}
                                <PhoneInput
                                  inputClass='form-control'
                                  inputStyle= {{'width': '100%'}}
                                  country={'ca'}
                                  onlyCountries={['ca', 'in' ,'us']}
                                  value={this.state.AddalternetPhoneNumber}
                                  onChange={this.handleOnChangeAlternetMobileAdd.bind(this)}
                                />
                                <span className="form-text error-font-color">{this.state.errormsg["AddalternetPhoneNumber"]}</span>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Email<span className="text-danger">*</span></label>
                                <input type="text" className="form-control" value={this.state.Addemail} onChange={this.handleChange('Addemail')} />
                                <span className="form-text error-font-color">{this.state.errormsg["Addemail"]}</span>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Notes    </label>
                                {/*<input type="text" className="form-control" value={this.state.Addnote} onChange={this.handleChange('Addnote')} />*/}
                                <textarea rows="3" cols="5" class="form-control" value={this.state.Addnote} onChange={this.handleChange('Addnote')}></textarea>
                                <span className="form-text error-font-color">{this.state.errormsg["Addnote"]}</span>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Start Date<span className="text-danger">*</span></label>
                                <Datetime
                                inputProps={{readOnly: true}}
                                closeOnTab={true}
                                input={true}
                                value={(this.state.AddStartDate) ? this.state.AddStartDate : ''}
                                onChange={this.handleAddStartDate}
                                dateFormat={process.env.DATE_FORMAT}
                                timeFormat={false}
                                renderInput={(props) => {
                                   return <input {...props} value={(this.state.AddStartDate) ? props.value : ''} />
                                }}
                              />
                                {/*<input className="form-control" type="date" value={this.state.AddStartDate} onChange={this.handleChange('AddStartDate')} max={moment().format("YYYY-MM-DD")} />*/}
                                <span className="form-text error-font-color">{this.state.errormsg["AddStartDate"]}</span>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>End Date</label>
                                <Datetime
                                isValidDate={this.validationAddEndDate}
                                inputProps={{readOnly: true}}
                                closeOnTab={true}
                                input={true}
                                value={(this.state.AddEndDate) ? this.state.AddEndDate : ''}
                                onChange={this.handleAddEndDate}
                                dateFormat={process.env.DATE_FORMAT}
                                timeFormat={false}
                                renderInput={(props) => {
                                   return <input {...props} value={(this.state.AddEndDate) ? props.value : ''} />
                                }}
                              />
                                {/*<input className="form-control" type="date" value={this.state.AddEndDate} onChange={this.handleChange('AddEndDate')} min={moment().format(this.state.AddStartDate,"YYYY-MM-DD")} />*/}
                                <span className="form-text error-font-color">{this.state.errormsg["AddEndDate"]}</span>
                              </div>
                            </div>

                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Address Line 1</label>
                                <input className="form-control" type="text" value={this.state.AddAddress1Line1} onChange={this.handleChange('AddAddress1Line1')}/>
                                <span className="form-text error-font-color">{this.state.errormsg["AddAddress1Line1"]}</span>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Country</label>
                                <select className="form-control" value={this.state.AddCountry} onChange={this.handleChange('AddCountry')}>
                                  <option value="">-</option>
                                  {this.state.ListCountry.map(( listValue, index ) => {
                                    return (
                                      <option key={index} value={listValue.countryId}>{listValue.countryName}</option>
                                    );
                                  })}
                                </select>
                                <span className="form-text error-font-color">{this.state.errormsg["AddCountry"]}</span>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Province</label>
                                <select className="form-control" value={this.state.AddProvince} onChange={this.handleChange('AddProvince')}>
                                  <option value="">-</option>
                                  {this.state.ListProvince.map(( listValue, index ) => {
                                    return (
                                      <option key={index} value={listValue.provinceId}>{listValue.provinceName}</option>
                                    );
                                  })}
                                </select>
                                <span className="form-text error-font-color">{this.state.errormsg["AddProvince"]}</span>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>City</label>
                                <select className="form-control" value={this.state.AddCity} onChange={this.handleChange('AddCity')}>
                                  <option value="">-</option>
                                  {this.state.ListCity.map(( listValue, index ) => {
                                    return (
                                      <option key={index} value={listValue.cityId}>{listValue.cityName}</option>
                                    );
                                  })}
                                </select>
                                <span className="form-text error-font-color">{this.state.errormsg["AddCity"]}</span>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Postal Code</label>
                                <input className="form-control" type="text" value="123-456-7890" value={this.state.AddPostalCode} onChange={this.handleChange('AddPostalCode')}/>
                                <span className="form-text error-font-color">{this.state.errormsg["AddPostalCode"]}</span>
                              </div>
                            </div>

                          </div>
                          <div className="submit-section">
                            <button className="btn btn-primary submit-btn" onClick={this.AddRecord()}>Submit</button>
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
            {/* /Emergency Contacts Add Modal */}
            {/* Emergency Contacts Edit Modal */}
            <div id="EmergencyContacts_emergency_contacts_Edit_modal" className="modal custom-modal fade" role="dialog">
              <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div className="modal-content">
                  <div className="modal-header">
                    <h5 className="modal-title">Emergency Contacts</h5>
                    <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">×</span>
                    </button>
                  </div>
                  <div className="modal-body">
                    <form>
                      <div className="card">
                        <div className="card-body">
                          <div className="row">
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Contact Type<span className="text-danger">*</span></label>
                                <select  className="form-control" value={this.state.EditaddressType}  onChange={this.handleChange('EditaddressType')}>
                                  <option value="">      -       </option>
                                  {this.state.emgContactypeList.map(( listValue, index ) => {
                                    return (
                                      <option key={index} value={listValue.name}>{listValue.name}</option>
                                    );
                                  })}
                                </select>
                                <span className="form-text error-font-color">{this.state.errormsg["EditaddressType"]}</span>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Relationship<span className="text-danger">*</span></label>
                                <select  className="form-control" value={this.state.EditrelationShip}  onChange={this.handleChange('EditrelationShip')}>
                                  <option value="">      -       </option>
                                  {this.state.emgRelationShipTypeList.map(( listValue, index ) => {
                                    return (
                                      <option key={index} value={listValue.name}>{listValue.name}</option>
                                    );
                                  })}
                                </select>
                                <span className="form-text error-font-color">{this.state.errormsg["EditrelationShip"]}</span>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>First Name<span className="text-danger">*</span></label>
                                <input type="text" className="form-control" value={this.state.EditfirstName} onChange={this.handleChange('EditfirstName')} />
                                <span className="form-text error-font-color">{this.state.errormsg["EditfirstName"]}</span>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Last Name<span className="text-danger">*</span></label>
                                <input type="text" className="form-control" value={this.state.EditlastName} onChange={this.handleChange('EditlastName')} />
                                <span className="form-text error-font-color">{this.state.errormsg["EditlastName"]}</span>
                              </div>
                            </div>
                            
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Phone Number<span className="text-danger">*</span></label>
                                {/*<InputMask className="form-control" mask="(999) 999-9999" value={this.state.EditphoneNumber} onChange={this.handleChange('EditphoneNumber')} ></InputMask>*/}
                                <PhoneInput
                                  inputClass='form-control'
                                  inputStyle= {{'width': '100%'}}
                                  country={'ca'}
                                  onlyCountries={['ca', 'in' ,'us']}
                                  value={this.state.EditphoneNumber}
                                  onChange={this.handleOnChangePhoneMobileEdit.bind(this)}
                                />
                                <span className="form-text error-font-color">{this.state.errormsg["EditphoneNumber"]}</span>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Alternate Phone</label>
                                {/*<InputMask className="form-control" mask="(999) 999-9999" value={this.state.EditalternetPhoneNumber} onChange={this.handleChange('EditalternetPhoneNumber')} ></InputMask>*/}
                                <PhoneInput
                                  inputClass='form-control'
                                  inputStyle= {{'width': '100%'}}
                                  country={'ca'}
                                  onlyCountries={['ca', 'in' ,'us']}
                                  value={this.state.EditalternetPhoneNumber}
                                  onChange={this.handleOnChangeAlternetMobileEdit.bind(this)}
                                />
                                <span className="form-text error-font-color">{this.state.errormsg["EditalternetPhoneNumber"]}</span>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Email<span className="text-danger">*</span></label>
                                <input type="text" className="form-control" value={this.state.Editemail} onChange={this.handleChange('Editemail')} />
                                <span className="form-text error-font-color">{this.state.errormsg["Editemail"]}</span>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Notes    </label>
                                {/*<input type="text" className="form-control" value={this.state.Editnote} onChange={this.handleChange('Editnote')} />*/}
                                <textarea rows="3" cols="5" class="form-control" value={this.state.Editnote} onChange={this.handleChange('Editnote')}></textarea>
                                <span className="form-text error-font-color">{this.state.errormsg["Editnote"]}</span>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Start Date<span className="text-danger">*</span></label>
                                {/*<input className="form-control" type="date" value={this.state.EditStartDate} onChange={this.handleChange('EditStartDate')} max={moment().format("YYYY-MM-DD")} />*/}
                                <Datetime
                                  inputProps={{readOnly: true}}
                                  closeOnTab={true}
                                  input={true}
                                  value={(this.state.EditStartDate) ? this.state.EditStartDate : ''}
                                  onChange={this.handleEditStartDate}
                                  dateFormat={process.env.DATE_FORMAT}
                                  timeFormat={false}
                                  renderInput={(props) => {
                                     return <input {...props} value={(this.state.EditStartDate) ? props.value : ''} />
                                  }}
                                />
                                <span className="form-text error-font-color">{this.state.errormsg["EditStartDate"]}</span>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>End Date</label>
                                <Datetime
                                  inputProps={{readOnly: true}}
                                  isValidDate={this.validationEditEndDate}
                                  closeOnTab={true}
                                  input={true}
                                  value={(this.state.EditEndDate) ? this.state.EditEndDate : ''}
                                  onChange={this.handleEditEndDate}
                                  dateFormat={process.env.DATE_FORMAT}
                                  timeFormat={false}
                                  renderInput={(props) => {
                                     return <input {...props} value={(this.state.EditEndDate) ? props.value : ''} />
                                  }}
                                />
                                {/*<input className="form-control" type="date" value={this.state.EditEndDate} onChange={this.handleChange('EditEndDate')} max={moment().format(this.state.EditStartDate,"YYYY-MM-DD")} />*/}
                                <span className="form-text error-font-color">{this.state.errormsg["EditEndDate"]}</span>
                              </div>
                            </div>

                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Address Line 1</label>
                                <input className="form-control" type="text" value={this.state.EditAddress1Line1} onChange={this.handleChange('EditAddress1Line1')}/>
                                <span className="form-text error-font-color">{this.state.errormsg["EditAddress1Line1"]}</span>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Country</label>
                                <select className="form-control" value={this.state.EditCountry} onChange={this.handleChange('EditCountry')}>
                                  <option value="">-</option>
                                  {this.state.ListCountry.map(( listValue, index ) => {
                                    return (
                                      <option key={index} value={listValue.countryId}>{listValue.countryName}</option>
                                    );
                                  })}
                                </select>
                                <span className="form-text error-font-color">{this.state.errormsg["EditCountry"]}</span>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Province</label>
                                <select className="form-control" value={this.state.EditProvince} onChange={this.handleChange('EditProvince')}>
                                  <option value="">-</option>
                                  {this.state.ListProvince.map(( listValue, index ) => {
                                    return (
                                      <option key={index} value={listValue.provinceId}>{listValue.provinceName}</option>
                                    );
                                  })}
                                </select>
                                <span className="form-text error-font-color">{this.state.errormsg["EditProvince"]}</span>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>City</label>
                                <select className="form-control" value={this.state.EditCity} onChange={this.handleChange('EditCity')}>
                                  <option value="">-</option>
                                  {this.state.ListCity.map(( listValue, index ) => {
                                    return (
                                      <option key={index} value={listValue.cityId}>{listValue.cityName}</option>
                                    );
                                  })}
                                </select>
                                <span className="form-text error-font-color">{this.state.errormsg["EditCity"]}</span>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Postal Code</label>
                                <input className="form-control" type="text" value="123-456-7890" value={this.state.EditPostalCode} onChange={this.handleChange('EditPostalCode')}/>
                                <span className="form-text error-font-color">{this.state.errormsg["EditPostalCode"]}</span>
                              </div>
                            </div>

                          </div>
                          <div className="submit-section">
                            <button className="btn btn-primary submit-btn" onClick={this.UpdateRecord()}>Update</button>
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
            {/* /Emergency Contacts Edit Modal */}
            {/* Delete Trained Locations  Modal */}
            <div className="modal custom-modal fade" id="delete_emergency_contact" role="dialog">
              <div className="modal-dialog modal-dialog-centered">
                <div className="modal-content">
                  <div className="modal-body">
                    <div className="form-header">
                      <h3>Emergency Contact</h3>
                      <p>Are you sure you want to mark emergency contact as {this.state.isDelete == true ? 'Active' : 'Inactive' } ?</p>
                    </div>
                    <div className="modal-btn delete-action">
                      <div className="row">
                        <div className="col-6">
                          <a  onClick={this.DeleteRecord()} className="btn btn-primary continue-btn">{this.state.isDelete == true ? 'Active' : 'Inactive' }</a>
                        </div>
                        <div className="col-6">
                          <a href="" data-dismiss="modal" className="btn btn-primary cancel-btn">Cancel</a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        {/* /Delete Trained Locations Modal */}
            {/* ************ Emergency Contacts Tab Modals ************* */}
            </div>
    );
   }
}

export default EmergencyContact;
