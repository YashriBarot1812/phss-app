/**
 * Signin Firebase
 */

import React, { Component } from 'react';
import { Helmet } from "react-helmet";

import moment from 'moment';

import Loader from '../../Loader';
import SystemHelpers from '../../Helpers/SystemHelper';

//table
import { MDBDataTable } from 'mdbreact';
import '@fortawesome/fontawesome-free/css/all.min.css';
import 'bootstrap-css-only/css/bootstrap.min.css';
import 'mdbreact/dist/css/mdb.css';
//table

import CryptoAES from 'crypto-js/aes';
import CryptoENC from 'crypto-js/enc-utf8';

import Datetime from "react-datetime";

/* 
1) Dev and Qa
Skill Category = Human Resources
GUID = 'e261ef96-216d-eb11-a812-000d3a3df1ae'

2) ETL
Skill Category = Human Resources
GUID = '08737bcf-2f96-eb11-b1ac-000d3a84e784'
value="08737bcf-2f96-eb11-b1ac-000d3a84e784"
*/


class Skills extends Component {
  constructor(props) {
    super(props);

    this.state = {
        ListGrid:[],
        errormsg : '',
        user_role: [],
        staffContactID:this.props.staffContactID,
        AllSubCategory:[],
        userSkillCategories:[],
        AddCategory:'',
        AddSubCategory:'',
        AddStartDate:'',
        AddEndDate:'',
        //DropCategory : 'c957d58a-216d-eb11-a812-000d3a3df1ae', // Skill Training id = c957d58a-216d-eb11-a812-000d3a3df1ae
        //DropCategoryName : 'Skill Training',

        DropCategory : '', // Skill Training id = c957d58a-216d-eb11-a812-000d3a3df1ae
        //DropCategoryName : 'All',
        DropCategoryName : 'Skills',

        SkillLevel : [],
        AddSkillLevel : '',

        MainListGrid : [],
        role_skills_can: {},

        isDelete : false,

        header_data : [],

        AddSubCategoryfrequency : '',
        EditSubCategoryfrequency : '',

        AddskillCheckBox: '',
        AddskillDescription: '',
        
        FrequencyAddCheckBox : false,
        FrequencyAddInputBox : false,
        FrequencyAddStartDateDisplay : false,
        FrequencyAddEndDateDisplay : false,

        FrequencyAddStartDateMandatory : false,
        FrequencyAddEndDateMandatory : false,
        

        EditskillCheckBox: '',
        EditskillDescription: '',
        

        FrequencyEditCheckBox : false,
        FrequencyEditInputBox : false,
        FrequencyEditStartDateDisplay : false,
        FrequencyEditEndDateDisplay : false,

        FrequencyEditStartDateMandatory : false,
        FrequencyEditEndDateMandatory : false,
        checked : false,
        staffContactFullname : localStorage.getItem('fullName')
        
    };
    this.setPropState = this.setPropState.bind(this);

    this.handleAddStartDate = this.handleAddStartDate.bind(this);
    this.handleAddEndDate = this.handleAddEndDate.bind(this);

    this.handleEditStartDate = this.handleEditStartDate.bind(this);
    this.handleEditEndDate = this.handleEditEndDate.bind(this);
  }


  handleAddStartDate = (date) =>{
    console.log('AddStartDate => '+ date);
    this.setState({ AddStartDate : date });
    this.setState({ AddEndDate : '' });
  };

  handleAddEndDate = (date) =>{
    console.log('AddEndDate => '+ date);
    this.setState({ AddEndDate : date });
  };

  handleEditStartDate = (date) =>{
    console.log('EditStartDate => '+ date);
    this.setState({ EditStartDate : date });
    this.setState({ EditEndDate : '' });
  };

  handleEditEndDate = (date) =>{
    console.log('EditEndDate => '+ date);
    this.setState({ EditEndDate : date });
  };

  validationAddEndDate = (currentDate) => {
    return currentDate.isAfter(moment(this.state.AddStartDate));
  };

  validationEditEndDate = (currentDate) => {
    return currentDate.isAfter(moment(this.state.EditStartDate));
  };

  setPropState(key, value) {
      this.setState({ [key]: value });
  }

  handleChange = input => e => {
    this.setState({ [input]: e.target.value.replace(/^\s+/g, '') });
    //console.log(e.target);

    // Add time SubCategory
    if([input] == "AddSubCategory")
    {

      //$('#AddskillCheckBox').prop('checked', false);
      this.setState({ AddskillCheckBox: false });

      console.log(e.target[e.target.selectedIndex].getAttribute('data-frequency'));
      
      this.setState({ AddSubCategoryfrequency: e.target[e.target.selectedIndex].getAttribute('data-frequency') });
      
      var frequency =e.target[e.target.selectedIndex].getAttribute('data-frequency') ;
      
      delete this.state.errormsg["AddStartDate"];
      delete this.state.errormsg["AddEndDate"];
      
      if(frequency == "Check Mark With Date") 
      {
        // Start date, End date, Check box are Mandatory
        this.setState({ FrequencyAddCheckBox: true });
        this.setState({ FrequencyAddInputBox: false });
        this.setState({ FrequencyAddStartDateDisplay: true });
        this.setState({ FrequencyAddEndDateDisplay: true });
        
        this.setState({ FrequencyAddStartDateMandatory: true });
        this.setState({ FrequencyAddEndDateMandatory: false });
        
      }
      else if(frequency == "One Time Date")
      {
        // Start date is Mandatory
        this.setState({ FrequencyAddCheckBox: false });
        this.setState({ FrequencyAddInputBox: false });
        this.setState({ FrequencyAddStartDateDisplay: true });
        this.setState({ FrequencyAddEndDateDisplay: true });

        this.setState({ FrequencyAddStartDateMandatory: true });
        this.setState({ FrequencyAddEndDateMandatory: false });
        
      }
      else if(frequency == "Yearly")
      {
        // Start date, End date are Mandatory. Start date select automatically select End date
        this.setState({ FrequencyAddCheckBox: false });
        this.setState({ FrequencyAddInputBox: false });
        this.setState({ FrequencyAddStartDateDisplay: true });
        this.setState({ FrequencyAddEndDateDisplay: true });

        this.setState({ FrequencyAddStartDateMandatory: true });
        this.setState({ FrequencyAddEndDateMandatory: true });
        
      }
      else if(frequency == "Check Mark")
      {
        // Check box is Mandatory.
        this.setState({ FrequencyAddCheckBox: true });
        this.setState({ FrequencyAddInputBox: false });
        this.setState({ FrequencyAddStartDateDisplay: true });
        this.setState({ FrequencyAddEndDateDisplay: true });

        this.setState({ FrequencyAddStartDateMandatory: false });
        this.setState({ FrequencyAddEndDateMandatory: false });
        
      }
      else if(frequency == "Check Mark & Input")
      {
        // Check box and Input box are Mandatory.
        this.setState({ FrequencyAddCheckBox: true });
        this.setState({ FrequencyAddInputBox: true });
        this.setState({ FrequencyAddStartDateDisplay: true });
        this.setState({ FrequencyAddEndDateDisplay: true });

        this.setState({ FrequencyAddStartDateMandatory: false });
        this.setState({ FrequencyAddEndDateMandatory: false });
        
      }
      else
      {
        this.setState({ FrequencyAddCheckBox: false });
        this.setState({ FrequencyAddInputBox: false });
        this.setState({ FrequencyAddStartDateDisplay: true });
        this.setState({ FrequencyAddEndDateDisplay: true });

        this.setState({ FrequencyAddStartDateMandatory: false });
        this.setState({ FrequencyAddEndDateMandatory: false });
        
      }
    }
    // Add time SubCategory

    // Edit time SubCategory
    if([input] == "EditSubCategory")
    {
      //$('#EditskillCheckBox').prop('checked', false);
      this.setState({ EditskillCheckBox: false });


      console.log("EditSubCategory Permison");
      console.log(e.target[e.target.selectedIndex].getAttribute('data-Editfrequency'));
      
      this.setState({ EditSubCategoryfrequency: e.target[e.target.selectedIndex].getAttribute('data-Editfrequency') });
      
      var frequency =e.target[e.target.selectedIndex].getAttribute('data-Editfrequency') ;
      
      delete this.state.errormsg["EditStartDate"];
      delete this.state.errormsg["EditStartDate"];
      
      if(frequency == "Check Mark With Date") 
      {
        // Start date, End date, Check box are Mandatory
        this.setState({ FrequencyEditCheckBox: true });
        this.setState({ FrequencyEditInputBox: false });
        this.setState({ FrequencyEditStartDateDisplay: true });
        this.setState({ FrequencyEditEndDateDisplay: true });
        
        this.setState({ FrequencyEditStartDateMandatory: true });
        this.setState({ FrequencyEditEndDateMandatory: false });
        
      }
      else if(frequency == "One Time Date")
      {
        // Start date is Mandatory
        this.setState({ FrequencyEditCheckBox: false });
        this.setState({ FrequencyEditInputBox: false });
        this.setState({ FrequencyEditStartDateDisplay: true });
        this.setState({ FrequencyEditEndDateDisplay: true });

        this.setState({ FrequencyEditStartDateMandatory: true });
        this.setState({ FrequencyEditEndDateMandatory: false });
        
      }
      else if(frequency == "Yearly")
      {
        // Start date, End date are Mandatory. Start date select automatically select End date
        this.setState({ FrequencyEditCheckBox: false });
        this.setState({ FrequencyEditInputBox: false });
        this.setState({ FrequencyEditStartDateDisplay: true });
        this.setState({ FrequencyEditEndDateDisplay: true });

        this.setState({ FrequencyEditStartDateMandatory: true });
        this.setState({ FrequencyEditEndDateMandatory: true });
        
      }
      else if(frequency == "Check Mark")
      {
        // Check box is Mandatory.
        this.setState({ FrequencyEditCheckBox: true });
        this.setState({ FrequencyEditInputBox: false });
        this.setState({ FrequencyEditStartDateDisplay: true });
        this.setState({ FrequencyEditEndDateDisplay: true });

        this.setState({ FrequencyEditStartDateMandatory: false });
        this.setState({ FrequencyEditEndDateMandatory: false });
        
      }
      else if(frequency == "Check Mark & Input")
      {
        // Check box and Input box are Mandatory.
        this.setState({ FrequencyEditCheckBox: true });
        this.setState({ FrequencyEditInputBox: true });
        this.setState({ FrequencyEditStartDateDisplay: true });
        this.setState({ FrequencyEditEndDateDisplay: true });

        this.setState({ FrequencyEditStartDateMandatory: false });
        this.setState({ FrequencyEditEndDateMandatory: false });
        
      }
      else
      {
        this.setState({ FrequencyEditCheckBox: false });
        this.setState({ FrequencyEditInputBox: false });
        this.setState({ FrequencyEditStartDateDisplay: true });
        this.setState({ FrequencyEditEndDateDisplay: true });

        this.setState({ FrequencyEditStartDateMandatory: false });
        this.setState({ FrequencyEditEndDateMandatory: false });
        
      }
    }
    // Edit time SubCategory

    if (this.state[input] != '') {
        delete this.state.errormsg[input];
    }

    if([input]=="AddStartDate")
    {
      //this.setState({ AddEndDate: moment(e.target.value).format('YYYY-MM-DD') });
      if(this.state.AddSubCategoryfrequency == 'Yearly')
      {
        this.setState({ AddEndDate: moment(e.target.value).add(12, 'M').format('YYYY-MM-DD') });
      }
      else if(this.state.FrequencyAddEndDate == false)
      {
        this.setState({ AddEndDate: '' });
      }
      
    }

    if([input]=="EditStartDate")
    {
      //this.setState({ EditndDate: moment(e.target.value).format('YYYY-MM-DD') });
      if(this.state.EditSubCategoryfrequency == 'Yearly')
      {
        this.setState({ EditEndDate: moment(e.target.value).add(12, 'M').format('YYYY-MM-DD') });
      }
      else
      {
        this.setState({ EditEndDate: '' });
      }
      
    }

    if([input]=="AddCategory")
    {
      //alert(e.target.value);
      this.setState({ AllSubCategory:[]});
      this.GetSkillSubCategory(e.target.value);
    }
    
    if([input]=="EditCategory")
    {
      //alert(e.target.value);
      this.setState({ AllSubCategory:[]});
      this.GetSkillSubCategory(e.target.value);
    }

    if([input]=="DropCategory")
    {
      var DropCategoryNm= $('#DropCategoryId option:selected').text();
      var DropCategoryId= $('#DropCategoryId option:selected').val();
      
      //this.setState({ DropCategoryName : DropCategoryNm});
      //alert($('#DropCategoryId option:selected').val());
      //this.GetSkillSubCategory(DropCategoryId);
      
      this.setState({ ListGrid: this.rowData(DropCategoryId) });
    }
  }

  // Loader hide show method
  hideLoader = () => {
    this.setState({ loading: false });
  }

  showLoader = () => {
    this.setState({ loading: true });
  }
  // Loader hide show method

  setPropState(key, value) {
      this.setState({ [key]: value });
  }

  componentDidMount() {
    // console.log("skills");
    // console.log(this.props.skills_can_create);
    // console.log(this.props.skills_can_update);
    // console.log(this.props.skills_can_view);
    // console.log(this.props.skills_can_delete);
    // console.log(this.props.skills_can_approve);
    // console.log(this.props.skills_can_export);

    /* Role Management */
     console.log('Role Store skills_can');
     /*var getrole = SystemHelpers.GetRole();
     let skills_can = getrole.skills_can;
     this.setState({ role_skills_can: skills_can });
     console.log(skills_can);*/
     
     console.log(this.props.skills_can);
    let skills_can = this.props.skills_can;
    this.setState({ role_skills_can: this.props.skills_can });
    /* Role Management */

    if(this.props.page_type=="dashboard_Expiry" || this.props.page_type=="dashboard_Expiry90Days")
    {
      this.GetUserSkillByExpire();
    }
    this.GetUserSkillINfo();

    // Delete Permison
    if(skills_can.skills_can_delete == true){
      if(this.props.page_type=="dashboard_Expiry" || this.props.page_type=="dashboard_Expiry90Days")
      {
        var columns = [
          {
            label: 'Category',
            field: 'category',
            sort: 'asc',
            width: 150
          },
          {
            label: 'SubCategory',
            field: 'subcategory',
            sort: 'asc',
            width: 150
          },
          {
            label: 'Start Date',
            field: 'startdate',
            sort: 'asc',
            width: 150
          },
          {
            label: 'End Date',
            field: 'enddate',
            sort: 'asc',
            width: 150
          },
          {
            label: 'Action',
            field: 'action',
            width: 270
          }
        ];
      }else{
        var columns = [
          {
            label: 'Category',
            field: 'category',
            sort: 'asc',
            width: 150
          },
          {
            label: 'SubCategory',
            field: 'subcategory',
            sort: 'asc',
            width: 150
          },
          {
            label: 'Start Date',
            field: 'startdate',
            sort: 'asc',
            width: 150
          },
          {
            label: 'End Date',
            field: 'enddate',
            sort: 'asc',
            width: 150
          },
          {
            label: 'Verified',
            field: 'Verified',
            sort: 'asc',
            width: 150
          },
          {
            label: 'Remarks',
            field: 'Remarks',
            sort: 'asc',
            width: 150
          },
          {
            label: 'Status',
            field: 'status',
            sort: 'asc',
            width: 150
          },
          {
            label: 'Action',
            field: 'action',
            width: 270
          }
        ];
      }
      

      this.setState({ header_data: columns });
    }else{
      if(this.props.page_type=="dashboard_Expiry" || this.props.page_type=="dashboard_Expiry90Days")
      {
        var columns = [
          {
            label: 'Category',
            field: 'category',
            sort: 'asc',
            width: 150
          },
          {
            label: 'SubCategory',
            field: 'subcategory',
            sort: 'asc',
            width: 150
          },
          {
            label: 'Start Date',
            field: 'startdate',
            sort: 'asc',
            width: 150
          },
          {
            label: 'End Date',
            field: 'enddate',
            sort: 'asc',
            width: 150
          },
          {
            label: 'Action',
            field: 'action',
            width: 270
          }
        ];
      }else{
        var columns = [
          {
            label: 'Category',
            field: 'category',
            sort: 'asc',
            width: 150
          },
          {
            label: 'SubCategory',
            field: 'subcategory',
            sort: 'asc',
            width: 150
          },
          {
            label: 'Start Date',
            field: 'startdate',
            sort: 'asc',
            width: 150
          },
          {
            label: 'End Date',
            field: 'enddate',
            sort: 'asc',
            width: 150
          },
          {
            label: 'Verified',
            field: 'Verified',
            sort: 'asc',
            width: 150
          },
          {
            label: 'Remarks',
            field: 'Remarks',
            sort: 'asc',
            width: 150
          },
          {
            label: 'Action',
            field: 'action',
            width: 270
          }
        ];
      }
      

      this.setState({ header_data: columns });
    }
    // Delete Permison
  }

  TabClickOnLoadSkills = () => e => {
    //debugger;
    e.preventDefault();

    // if(this.props.page_type=="dashboard_Expiry" || this.props.page_type=="dashboard_Expiry90Days")
    // {
    //   this.GetUserSkillByExpire();
    // }
    this.GetUserSkillINfo();
  }

  GetUserSkillINfo(){

    /* Role Management */
    var getrole = SystemHelpers.GetRole();
    let canDelete = getrole.skills_can.skills_can_delete;
    /* Role Management */
    
    this.showLoader();
    var url=process.env.API_API_URL+'GetUserSkillINfo?contactId='+this.state.staffContactID+'&canDelete='+canDelete;
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
      //body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
        console.log("responseJson GetUserSkillINfo");
        console.log(data);
        //console.log(data.data.userRole);
        // debugger;
        if (data.responseType === "1") {
            // Profile & Contact
            //this.setState({ ListGrid: data.data.userSkillInfo });
            if(this.props.page_type == "main_skill_page"){
              this.setState({ MainListGrid: data.data.userSkillInfo });
            }

            this.setState({ ListGrid: this.rowData(this.state.DropCategory) });
            
            this.setState({ userSkillCategories: data.data.userSkillCategories });
            this.setState({ SkillLevel : data.data.skillLevel});
            
        }else{
              if(data.message == 'Authorization has been denied for this request.'){
                SystemHelpers.SessionOut();
                this.props.history.push("/login");
              }else{
                SystemHelpers.ToastError(data.message);
              }
              
        }
        this.hideLoader();
        
    })
    .catch(error => {
      this.props.history.push("/error-500");
    });
  }

  GetUserSkillByExpire()
  {

    this.showLoader();
    var url=process.env.API_API_URL+'GetUserSkillByExpire?contactId='+this.state.staffContactID;
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
      //body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
        console.log("responseJson GetUserSkillByExpire");
        console.log(data);
        //console.log(data.data.userRole);
        // debugger;
        if (data.responseType === "1") {
            // Profile & Contact
            if(data.data.userSkillInfo != null && data.data.userSkillInfo != [] && this.props.page_type == "dashboard_Expiry"){
              this.setState({ MainListGrid: data.data.userSkillInfo });
            }
            if(data.data.skillExpiryIn90Days != null && data.data.skillExpiryIn90Days != [] && this.props.page_type == "dashboard_Expiry90Days"){
              this.setState({ MainListGrid: data.data.skillExpiryIn90Days });
            }
            this.setState({ ListGrid: this.rowData(this.state.DropCategory) });
                        
        }else{
              if(data.message == 'Authorization has been denied for this request.'){
                SystemHelpers.SessionOut();
                this.props.history.push("/login");
              }else{
                SystemHelpers.ToastError(data.message);
              }
              
        }
        this.hideLoader();
        
    })
    .catch(error => {
      this.props.history.push("/error-500");
    });
  }

  GetSkillSubCategory(CatId){
    this.showLoader();
    var url=process.env.API_API_URL+'GetSkillSubCategory?categoryId='+CatId;
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
      //body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
        console.log("responseJson GetSkillSubCategory");
        console.log(data);
        //console.log(data.data.userRole);
        // debugger;
        if (data.responseType === "1") {
            // Profile & Contact
           this.setState({ AllSubCategory: data.data });
            
        }else{
              if(data.message == 'Authorization has been denied for this request.'){
                SystemHelpers.SessionOut();
                this.props.history.push("/login");
              }else{
                SystemHelpers.ToastError(data.message);
              }
              
        }
        this.hideLoader();
        
    })
    .catch(error => {
      this.props.history.push("/error-500");
    });
  }

  Edit_Update_Btn_Func(record){
    let return_push = [];

    if(this.state.role_skills_can.skills_can_update == true || this.state.role_skills_can.skills_can_delete == true){
      let Edit_push = [];
      if(this.state.role_skills_can.skills_can_update == true){
        Edit_push.push(
          <a href="#" onClick={this.EditRecord(record)} className="dropdown-item" data-toggle="modal" data-target={this.props.page_type == "dashboard_Expiry90Days" ? "#Skills_skills_Edit_modal90" : "#Skills_skills_Edit_modal"  }><i className="fa fa-pencil m-r-5" ></i> Edit </a>
        );
      }
      let Delete_push = [];
      if(this.state.role_skills_can.skills_can_delete == true){
        if(record.isDelete == false)
        {
          Delete_push.push(
            <a href="#" onClick={this.DeleteInfo(record)}  className="dropdown-item" data-toggle="modal" data-target={this.props.page_type == "dashboard_Expiry90Days" ? "#Skills_skills_Delete_modal90" : "#Skills_skills_Delete_modal"  }><i className="fa fa-trash-o m-r-5"></i> Inactive</a>
          );
        }
        else
        {
          Delete_push.push(
            <a href="#" onClick={this.DeleteInfo(record)}  className="dropdown-item" data-toggle="modal" data-target={this.props.page_type == "dashboard_Expiry90Days" ? "#Skills_skills_Delete_modal90" : "#Skills_skills_Delete_modal" }><i className="fa fa-trash-o m-r-5" ></i> Active </a>
          );
        }
      }
      
      return_push.push(
        <div className="dropdown dropdown-action">
          <a aria-expanded="false" data-toggle="dropdown" className="action-icon dropdown-toggle" href="#"><i className="material-icons">more_vert</i></a>
          <div className="dropdown-menu dropdown-menu-right">
            {Edit_push}
            {Delete_push}
          </div>
        </div>
      );
    }
    return return_push;
  }

  EditRecord = (record) => e => {
    e.preventDefault();
    console.log('Skill Training edit');
    console.log(record);

    this.setState({ errormsg: '' });

    this.setState({ skillId: record.skillId });
    this.setState({ EditCategory: record.skillCategoryId });
    this.setState({ EditSubCategory: record.skillSubCategoryId });
    if(record.startDate != "")
    {
      //this.setState({ EditStartDate: moment(record.startDate).format('YYYY-MM-DD') });
      this.setState({ EditStartDate: moment(record.startDate,process.env.API_DATE_FORMAT) });
    }
    else
    {
      this.setState({ EditStartDate: record.startDate });
    }
    if(record.endDate != "")
    {
      //this.setState({ EditEndDate: moment(record.endDate).format('YYYY-MM-DD') });
      this.setState({ EditEndDate: moment(record.endDate,process.env.API_DATE_FORMAT) });
    }
    else
    {
      this.setState({ EditEndDate: record.endDate });
    }
    this.setState({ EditSkillLevel: record.skillLevel });
    this.GetSkillSubCategory(record.skillCategoryId);

    // Edit time frequency
    this.setState({ EditskillCheckBox: record.hasSkill });

    if(record.hasSkill == true){
      //alert('1');
      
      $('#EditskillCheckBox').prop('checked', true);
      
    }else{
      
      //alert('2');
      $('#EditskillCheckBox').prop('checked', false);
    }
    
    
    this.setState({ EditskillDescription: record.skillDescription });

    this.setState({ EditSubCategoryfrequency: record.frequency });

    
      
      var frequency = record.frequency ;
      
      if(frequency == "Check Mark With Date") 
      {
        // Start date, End date, Check box are Mandatory
        this.setState({ FrequencyEditCheckBox: true });
        this.setState({ FrequencyEditInputBox: false });
        this.setState({ FrequencyEditStartDateDisplay: true });
        this.setState({ FrequencyEditEndDateDisplay: true });
        
        this.setState({ FrequencyEditStartDateMandatory: true });
        this.setState({ FrequencyEditEndDateMandatory: false });
        
      }
      else if(frequency == "One Time Date")
      {
        // Start date is Mandatory
        this.setState({ FrequencyEditCheckBox: false });
        this.setState({ FrequencyEditInputBox: false });
        this.setState({ FrequencyEditStartDateDisplay: true });
        this.setState({ FrequencyEditEndDateDisplay: true });

        this.setState({ FrequencyEditStartDateMandatory: true });
        this.setState({ FrequencyEditEndDateMandatory: false });
        
      }
      else if(frequency == "Yearly")
      {
        // Start date, End date are Mandatory. Start date select automatically select End date
        this.setState({ FrequencyEditCheckBox: false });
        this.setState({ FrequencyEditInputBox: false });
        this.setState({ FrequencyEditStartDateDisplay: true });
        this.setState({ FrequencyEditEndDateDisplay: true });

        this.setState({ FrequencyEditStartDateMandatory: true });
        this.setState({ FrequencyEditEndDateMandatory: true });
        
      }
      else if(frequency == "Check Mark")
      {
        // Check box is Mandatory.
        this.setState({ FrequencyEditCheckBox: true });
        this.setState({ FrequencyEditInputBox: false });
        this.setState({ FrequencyEditStartDateDisplay: true });
        this.setState({ FrequencyEditEndDateDisplay: true });

        this.setState({ FrequencyEditStartDateMandatory: false });
        this.setState({ FrequencyEditEndDateMandatory: false });
        
      }
      else if(frequency == "Check Mark & Input")
      {
        // Check box and Input box are Mandatory.
        this.setState({ FrequencyEditCheckBox: true });
        this.setState({ FrequencyEditInputBox: true });
        this.setState({ FrequencyEditStartDateDisplay: true });
        this.setState({ FrequencyEditEndDateDisplay: true });

        this.setState({ FrequencyEditStartDateMandatory: false });
        this.setState({ FrequencyEditEndDateMandatory: false });
        
      }
      else
      {
        this.setState({ FrequencyEditCheckBox: false });
        this.setState({ FrequencyEditInputBox: false });
        this.setState({ FrequencyEditStartDateDisplay: true });
        this.setState({ FrequencyEditEndDateDisplay: true });

        this.setState({ FrequencyEditStartDateMandatory: false });
        this.setState({ FrequencyEditEndDateMandatory: false });
        
      }
    // Edit time frequency

  }

  DeleteInfo = (record) => e =>{
    e.preventDefault();

    this.setState({ skillId: record.skillId });
    this.setState({ isDelete: record.isDelete });
  }

  ClearRecord = ()=> e => {
    e.preventDefault();

    this.setState({ AddCategory: 0 });
    this.setState({ AddSubCategory: 0 });
    this.setState({ AddStartDate: '' });
    this.setState({ AddEndDate: '' });
    this.setState({ AddSkillLevel: '' });

    this.setState({ AddskillDescription: '' });
    $('#AddskillCheckBox').prop('checked', false);

    this.setState({ FrequencyAddCheckBox: false });
    this.setState({ FrequencyAddInputBox: false });

    this.setState({ errormsg: '' });
  }

  AddRecord = () => e => {
    //debugger;
    e.preventDefault();

    let step1Errors = {};
    
    if (this.state["AddCategory"] =='') {
      step1Errors["AddCategory"] = "Category is mandatory";
    }

 
    if (this.state["AddSubCategory"] == '') {
      step1Errors["AddSubCategory"] = "SubCategory is mandatory";
    }

    if(this.state.FrequencyAddStartDateDisplay == true && this.state.FrequencyAddStartDateMandatory == true)
    {
      if (this.state["AddStartDate"] == '') {
        step1Errors["AddStartDate"] = "Start Date is mandatory";
      }
    }

    if(this.state.FrequencyAddEndDateDisplay == true && this.state.FrequencyAddEndDateMandatory == true)
    {
      if (this.state["AddEndDate"] == '') {
        step1Errors["AddEndDate"] = "End Date is mandatory";
      }
    }

    if(this.state.FrequencyAddInputBox == true)
    {
      if (this.state["AddskillDescription"] == '') {
        step1Errors["AddskillDescription"] = "Skill Description is mandatory";
      }
      var AddskillDescription = this.state["AddskillDescription"];
    }
    else
    {
      var AddskillDescription = "";
    }

    if(this.state.FrequencyAddCheckBox == true)
    {
      if ($('#AddskillCheckBox').is(":checked"))
      {
        var AddskillCheckBox = true;
      }
      else
      {
        var AddskillCheckBox = false;
      }
    }
    else
    {
      var AddskillCheckBox = false;
    }

    //console.log(step1Errors);

    this.setState({ errormsg: step1Errors });
        
    if (Object.keys(step1Errors).length > 0) {
        return false;
    }

    //return false;
    //alert(this.state["AddSkillLevel"]);

    this.showLoader();

    var add_start_date ='';
    if (this.state["AddStartDate"] != '' && this.state["AddStartDate"] != null)
    {
      var add_start_date=moment(this.state["AddStartDate"]).format('MM-DD-YYYY');
      //var NewAddStartDate = moment(AddStartDate, "MM-DD-YYYY").add(1, 'days');
      //var add_start_date=moment(NewAddStartDate).format('MM-DD-YYYY');
    }

    var add_end_date ='';
    if (this.state["AddEndDate"] != '' && this.state["AddEndDate"] != null)
    {
      var add_end_date=moment(this.state["AddEndDate"]).format('MM-DD-YYYY');
      //var NewAddEndDate = moment(AddEndDate, "MM-DD-YYYY").add(1, 'days');
      //var add_end_date=moment(NewAddEndDate).format('MM-DD-YYYY');
    }
    
    //alert(add_start_date);
    //alert(add_end_date);
    //return false;
    //alert(moment(NewAddEndDate).format('MM-DD-YYYY'));

    let ArrayJson = {
          skillCategoryId: this.state["AddCategory"],
          skillSubCategoryId: this.state["AddSubCategory"],
          //startDate: moment(NewAddStartDate).format('MM-DD-YYYY'),
          startDate: add_start_date,
          endDate: add_end_date,
          skillLevel: this.state["AddSkillLevel"],
          hasSkill: AddskillCheckBox,
          skillDescription: AddskillDescription
    };
     
    let bodyarray = {};
    bodyarray["contactId"] = this.state.staffContactID;
    bodyarray["userSkillInfo"] = ArrayJson;

    console.log(bodyarray);
    //return false;
    var url=process.env.API_API_URL+'CreateUserSkillSet';
    fetch(url, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
      body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
        console.log("responseJson userSkillInfo");
        console.log(data);
        //console.log(responseJson);
        // debugger;
        if (data.responseType === "1") {
            //this.props.history.push('/dashboard');
            this.setState({ AddCategory: 0 });
            this.setState({ AddSubCategory: 0 });
            this.setState({ AddStartDate: '' });
            this.setState({ AddEndDate: '' });
            this.setState({ AddSkillLevel: '' });
            this.setState({ AddskillDescription: '' });
            $('#AddskillCheckBox').prop('checked', false);

            SystemHelpers.ToastSuccess(data.responseMessge);
            $( ".close" ).trigger( "click" );
            this.GetUserSkillINfo();    
        }
        else{
            SystemHelpers.ToastError(data.message  );
        }
        this.hideLoader();
        
    })
    .catch(error => {
      this.props.history.push("/error-500");
    });
    return false;
  }

  UpdateRecord = () => e => {
    //debugger;
    e.preventDefault();

    let step1Errors = {};
    
    if (this.state["EditCategory"] =='') {
      step1Errors["EditCategory"] = "Category is mandatory";
    }

 
    if (this.state["EditSubCategory"] == '') {
      step1Errors["EditSubCategory"] = "SubCategory is mandatory";
    }

    /*if (this.state["EditStartDate"] == '') {
      step1Errors["EditStartDate"] = "Start Date is mandatory";
    }*/

    /*if (this.state["EditEndDate"] == '') {
      step1Errors["EditEndDate"] = "End Date is mandatory";
    }*/

    if(this.state.FrequencyEditStartDateDisplay == true && this.state.FrequencyEditStartDateMandatory == true)
    {
      if (this.state["EditStartDate"] == '') {
        step1Errors["EditStartDate"] = "Start Date is mandatory";
      }
    }

    if(this.state.FrequencyEditEndDateDisplay == true && this.state.FrequencyEditEndDateMandatory == true)
    {
      if (this.state["EditEndDate"] == '') {
        step1Errors["EditEndDate"] = "End Date is mandatory";
      }
    }

    if(this.state.FrequencyEditInputBox == true)
    {
      if (this.state["EditskillDescription"] == '') {
        step1Errors["EditskillDescription"] = "Skill Description is mandatory";
      }
      var EditskillDescription = this.state["EditskillDescription"];
    }
    else
    {
      var EditskillDescription = "";
    }

    if(this.state.FrequencyEditCheckBox == true)
    {
      if ($('#EditskillCheckBox').is(":checked"))
      {
        var EditskillCheckBox = true;
      }
      else
      {
        var EditskillCheckBox = false;
      }
    }
    else
    {
      var EditskillCheckBox = false;
    }


    //console.log(step1Errors);

    this.setState({ errormsg: step1Errors });
        
    if (Object.keys(step1Errors).length > 0) {
        return false;
    }

    //return false;

    this.showLoader();
    var edit_start_date ='';
    //alert(this.state["EditStartDate"]);
    if (this.state["EditStartDate"] != '' && this.state["EditStartDate"] != null)
    {
      var edit_start_date=moment(this.state["EditStartDate"]).format('MM-DD-YYYY');
      //var NewEditStartDate = moment(EditStartDate, "MM-DD-YYYY").add(1, 'days');
      //var edit_start_date=moment(NewEditStartDate).format('MM-DD-YYYY');
    }

    var edit_end_date ='';
    //alert(this.state["EditEndDate"]);
    if (this.state["EditEndDate"] != '' && this.state["EditEndDate"] != null)
    {
      var edit_end_date=moment(this.state["EditEndDate"]).format('MM-DD-YYYY');
      //var NewEditEndDate = moment(EditEndDate, "MM-DD-YYYY").add(1, 'days');
      //var edit_end_date=moment(NewEditEndDate).format('MM-DD-YYYY');
    }    
    //alert(edit_end_date);
    let ArrayJson = {
          skillCategoryId: this.state["EditCategory"],
          skillSubCategoryId: this.state["EditSubCategory"],
          //startDate: moment(NewEditStartDate).format('MM-DD-YYYY'),
          startDate: edit_start_date,
          endDate: edit_end_date,
          skillId: this.state["skillId"],
          skillLevel: this.state["EditSkillLevel"],
          hasSkill: EditskillCheckBox,
          skillDescription: EditskillDescription
    };
     
    let bodyarray = {};
    bodyarray["contactId"] = this.state.staffContactID;
    bodyarray["userSkillInfo"] = ArrayJson;
    bodyarray["userName"] = this.state.staffContactFullname;

    console.log(bodyarray);
    //return false;
    var url=process.env.API_API_URL+'UpdateUserSkillSet';
    fetch(url, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
      body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
        console.log("responseJson UpdateUserSkillSet");
        console.log(data);
        //console.log(responseJson);
        // debugger;
        if (data.responseType === "1") {
            //this.props.history.push('/dashboard');
            SystemHelpers.ToastSuccess(data.responseMessge);
            $( ".close" ).trigger( "click" );

            if(this.props.page_type=="dashboard_Expiry" || this.props.page_type=="dashboard_Expiry90Days")
            {
              this.GetUserSkillByExpire();
            }
            else
            {
              this.GetUserSkillINfo();
            }    
        }
        else{
            SystemHelpers.ToastError(data.message  );
        }
        this.hideLoader();
        
    })
    .catch(error => {
      this.props.history.push("/error-500");
    });
    return false;
  }

  DeleteRecord = () => e => {
    e.preventDefault();

    var isdelete = '';
    if(this.state.isDelete== true)
    {
      isdelete = false;
    }
    else
    {
      isdelete = true;
    }

    this.showLoader();
    console.log(this.state.skillId);
    var url=process.env.API_API_URL+'DeleteUserSkillSet?skillId='+this.state.skillId+'&isDelete='+isdelete+'&userName='+this.state.staffContactFullname;
    fetch(url, {
      method: 'PUT',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
      //body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
        console.log("responseJson DeleteUserSkillSet");
        console.log(data);
        //console.log(data.data.userRole);
        // debugger;
        if (data.responseType === "1") {
            // Profile & Contact
            SystemHelpers.ToastSuccess(data.responseMessge);
            $( ".cancel-btn" ).trigger( "click" );
            
            if(this.props.page_type=="dashboard_Expiry" || this.props.page_type=="dashboard_Expiry90Days")
            {
              this.GetUserSkillByExpire();
            }
            else
            {
              this.GetUserSkillINfo();
            }
            
            this.hideLoader();
        }else if (data.responseType == "2" || data.responseType == "3") {
            SystemHelpers.ToastError(data.responseMessge);
            $( ".cancel-btn" ).trigger( "click" );
            this.hideLoader();
        }else{
              if(data.message == 'Authorization has been denied for this request.'){
                SystemHelpers.SessionOut();
                this.props.history.push("/login");
              }else{
                SystemHelpers.ToastError(data.message);
              }
              this.hideLoader();
              $( ".cancel-btn" ).trigger( "click" );
        }
        
        
    })
    .catch(error => {
      this.props.history.push("/error-500");
    });
  }

  rowData(DropCategoryID) {
    
    /* Role Management */
    var getrole = SystemHelpers.GetRole();
    let canDelete = getrole.skills_can.skills_can_delete;
    /* Role Management */

    var ListGrid = this.DropFilter(DropCategoryID);

    // console.log('Row Skills')
    // console.log(ListGrid);

      var ListGrid_length = ListGrid.length;
      let dataArray = [];
      if(ListGrid_length > 0)
      {
          var i=1;
          for (var z = 0; z < ListGrid_length; z++) {
            
            var tempdataArray = [];
            
            tempdataArray.category = ListGrid[z].skillCategoryName;
            tempdataArray.subcategory = ListGrid[z].skillSubCategoryName;

            if(ListGrid[z].startDate == ""){
              tempdataArray.startdate = '';
            }else{
              tempdataArray.startdate = moment(ListGrid[z].startDate,process.env.API_DATE_FORMAT).format(process.env.DATE_FORMAT);
            }
            
            if(ListGrid[z].endDate == ""){
              tempdataArray.enddate = '';
            }else{
              tempdataArray.enddate = moment(ListGrid[z].endDate,process.env.API_DATE_FORMAT).format(process.env.DATE_FORMAT);
            }
            


            
            if((ListGrid[z].frequency == "Check Mark With Date" || ListGrid[z].frequency == "Check Mark" || ListGrid[z].frequency == "Check Mark & Input"))
            {
              if(ListGrid[z].hasSkill == true )
              {
                //var Verified = <div><span class='badge bg-inverse-warning'>Verified</span></div>;
                var Verified = <div><input type="checkbox" className="skill_checkbox" disabled="disabled" checked /></div>;
              }else{
                ///var Verified = <div><span class='badge bg-inverse-success'>Not Verified</span></div>;
                var Verified = <div><input type="checkbox" className="skill_checkbox" disabled="disabled" /></div>;
              } 
            } 
            else
            {
              var Verified = <div></div>;
            }

            if(this.props.page_type != "dashboard_Expiry" && this.props.page_type !="dashboard_Expiry90Days")
            {
              tempdataArray.Verified = Verified;

              tempdataArray.Remarks = ListGrid[z].skillDescription;

              if(canDelete == true)
              {
                var status = "";
                if(ListGrid[z].isDelete == true){
                  tempdataArray.status = <div><span class="badge bg-inverse-warning">Inactive</span></div>;
                }else{
                  tempdataArray.status = <div><span class="badge bg-inverse-success">Active</span></div>;
                }
              }
            }  
            
            
              

            tempdataArray.action = this.Edit_Update_Btn_Func(ListGrid[z]);
            dataArray.push(tempdataArray);
            
            i++;
          }
      }
      return dataArray;
  }

  DropFilter(CategoryDropID)
  {
    var MainListGrid = this.state.MainListGrid;
    
    //console.log('Row Skills DropFilter')
    //console.log(MainListGrid);

    var ListGrid_length = MainListGrid.length;
    let dataArray = [];
    var i=1;
    for (var z = 0; z < ListGrid_length; z++) {
        var tempdataArray = [];
        
        if(MainListGrid[z].skillCategoryId == CategoryDropID)
        {
          dataArray.push(MainListGrid[z]);
        }
        if(CategoryDropID == "" && MainListGrid[z].skillCategoryId != process.env.API_HUMAN_RESOURCES)
        {
          dataArray.push(MainListGrid[z]);
        }
        i++;
    }
    
    return dataArray;
  }

  handleCheck() {
    this.setState({checked: !this.state.checked});
  }
  render() {
      
      const data = {
        columns: this.state.header_data,
        rows: this.state.ListGrid
      };

      return (
        <div>
        {/* Toast & Loder method use */}
            
        {(this.state.loading) ? <Loader /> : null} 
        {/* Toast & Loder method use */}
        <div className="row">
          <div className="col-md-12 d-flex">
            <div className="card profile-box flex-fill">

              <div className="row">
                <button type="hidden" className="btn btn-primary submit-btn pk-profiletab-refreshbtn-hide" id="TabClickOnLoadSkills" onClick={this.TabClickOnLoadSkills()}>Refresh</button>
              </div>

              <div className="card-body">
                
                {this.state.role_skills_can.skills_can_create == true && this.props.page_type == "main_skill_page" ?

                <h3 className="card-title">{this.state.DropCategoryName}<a href="#" className="edit-icon" data-toggle="modal" data-target="#Skills_skills_Add_modal"><i className="fa fa-plus" /></a></h3>
                  : null
                }

                {this.state.role_skills_can.skills_can_create == false && this.props.page_type == "main_skill_page" ?

                <h3 className="card-title">{this.state.DropCategoryName} <a href="#" className="phss-lock"><i className="fa fa-lock" /></a></h3>
                  : null
                }

                { this.props.page_type == "main_skill_page" ?
                  <div className="row">
                    
                    <div className="col-md-4">
                      <select className="form-control" id="DropCategoryId" value={this.state.DropCategory} onChange={this.handleChange('DropCategory')}>
                        <option value="">All</option>
                        {this.state.userSkillCategories.map(( listValue, index ) => {
                          if(listValue.categoryId != process.env.API_HUMAN_RESOURCES) { //Human Resources
                            return (
                              <option key={index} value={listValue.categoryId} >{listValue.categoryName}</option>
                            );
                          }
                        })}
                      </select>
                    </div>

                    <div className="col-md-8"></div>
                  </div> : null
                }
                <br/>
                <div className="table-responsive">
                <MDBDataTable
                    striped
                    bordered
                    small
                    data={data}
                    entriesOptions={[5, 10, 20, 50, 100]} entries={5} pagesAmount={4}
                    className="table table-striped custom-table mb-0 datatable"
                  />
              </div>
              </div>
            </div>
          </div>
        </div>
          {/* ****************** Skills Tab Modals ******************* */}
            {/* Skills Modal */}
            <div id={this.props.page_type == "dashboard_Expiry90Days" ? "Skills_skills_Add_modal90" : "Skills_skills_Add_modal" } className="modal custom-modal fade" role="dialog">
              <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div className="modal-content">
                  <div className="modal-header">
                    <h5 className="modal-title">Skills</h5>
                    <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={this.ClearRecord()} >
                      <span aria-hidden="true">×</span>
                    </button>
                  </div>
                  <div className="modal-body">
                    <form>
                      <div className="card">
                        <div className="card-body">

                          <div className="row">

                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Category<span className="text-danger">*</span></label>
                                <select className="form-control" value={this.state.AddCategory} onChange={this.handleChange('AddCategory')}>
                                  <option value="">-</option>
                                  {this.state.userSkillCategories.map(( listValue, index ) => {
                                    if(listValue.categoryId != process.env.API_HUMAN_RESOURCES) { //Human Resources
                                      return (
                                        <option key={index} value={listValue.categoryId}>{listValue.categoryName}</option>
                                      );
                                    }
                                  })}
                                </select>
                                <span className="form-text error-font-color">{this.state.errormsg["AddCategory"]}</span>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>SubCategory<span className="text-danger">*</span></label>
                                <select className="form-control" value={this.state.AddSubCategory} onChange={this.handleChange('AddSubCategory')}>
                                  <option value="">-</option>
                                  {this.state.AllSubCategory.map(( listValue, index ) => {
                                    return (
                                      <option key={index} value={listValue.subCategoryId} data-frequency={listValue.frequency}>{listValue.subCategoryName}</option>
                                    );
                                  })}
                                </select>
                                <span className="form-text error-font-color">{this.state.errormsg["AddSubCategory"]}</span>
                              </div>
                            </div>

                            {this.state.FrequencyAddCheckBox == true ?
                              <div className="col-md-6">
                                <div className="form-group">
                                  <label></label>
                                  <div class="checkbox">
                                    <label>Verified<input type="checkbox" className="skill_checkbox" name="AddskillCheckBox" id="AddskillCheckBox" /></label>
                                  </div>
                                  <span className="form-text error-font-color">{this.state.errormsg["AddskillCheckBox"]}</span>
                                </div>
                              </div>: null
                            }

                            {this.state.FrequencyAddCheckBox == true && this.state.FrequencyAddInputBox == true ?
                              <div className="col-md-6">
                                <div className="form-group">
                                  <label>Remarks</label>
                                  <input className="form-control" type="text" value={this.state.AddskillDescription} onChange={this.handleChange('AddskillDescription')}/>
                                  <span className="form-text error-font-color">{this.state.errormsg["AddskillDescription"]}</span>
                                </div>
                              </div>: null
                            }

                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Start Date {this.state.FrequencyAddStartDateMandatory == true ? <span className="text-danger">*</span> : null } </label>
                                <Datetime
                                  inputProps={{readOnly: true}}
                                  closeOnTab={true}
                                  input={true}
                                  value={(this.state.AddStartDate) ? this.state.AddStartDate : ''}
                                  onChange={this.handleAddStartDate}
                                  dateFormat={process.env.DATE_FORMAT}
                                  timeFormat={false}
                                  renderInput={(props) => {
                                     return <input {...props} value={(this.state.AddStartDate) ? props.value : ''} />
                                  }}
                                />
                                {/*<input className="form-control" type="date" value={this.state.AddStartDate} onChange={this.handleChange('AddStartDate')} />*/}
                                <span className="form-text error-font-color">{this.state.errormsg["AddStartDate"]}</span>
                              </div>
                            </div>

                            <div className="col-md-6">
                              <div className="form-group">
                                <label>End Date {this.state.FrequencyAddEndDateMandatory == true ? <span className="text-danger">*</span> : null } </label>
                                <Datetime
                                isValidDate={this.validationAddEndDate}
                                inputProps={{readOnly: true}}
                                closeOnTab={true}
                                input={true}
                                value={(this.state.AddEndDate) ? this.state.AddEndDate : ''}
                                onChange={this.handleAddEndDate}
                                dateFormat={process.env.DATE_FORMAT}
                                timeFormat={false}
                                renderInput={(props) => {
                                   return <input {...props} value={(this.state.AddEndDate) ? props.value : ''} />
                                }}
                              />
                                {/*<input className="form-control" type="date" value={this.state.AddEndDate} onChange={this.handleChange('AddEndDate')} min={moment().format(this.state.AddStartDate,"YYYY-MM-DD")} />*/}
                                <span className="form-text error-font-color">{this.state.errormsg["AddEndDate"]}</span>
                              </div>
                            </div>

                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Skill Level</label>
                                <select className="form-control" value={this.state.AddSkillLevel} onChange={this.handleChange('AddSkillLevel')}>
                                  <option value="">-</option>
                                  {this.state.SkillLevel.map(( listValue, index ) => {
                                    return (
                                      <option key={index} value={listValue.name}>{listValue.name}</option>
                                    );
                                  })}
                                </select>
                                <span className="form-text error-font-color">{this.state.errormsg["AddSkillLevel"]}</span>
                              </div>
                            </div>

                          </div>

                          <div className="submit-section">
                            <button className="btn btn-primary submit-btn" onClick={this.AddRecord()}>Submit</button>
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
            {/* //Skills Modal */}

            {/* Skills Ediit Modal */}
            <div id={this.props.page_type == "dashboard_Expiry90Days" ? "Skills_skills_Edit_modal90" : "Skills_skills_Edit_modal" } className="modal custom-modal fade" role="dialog">
              <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div className="modal-content">
                  <div className="modal-header">
                    <h5 className="modal-title">Skills</h5>
                    <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">×</span>
                    </button>
                  </div>
                  <div className="modal-body">
                    <form>
                      <div className="card">
                        <div className="card-body">
                          <div className="row">
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Category<span className="text-danger">*</span></label>
                                <select className="form-control" value={this.state.EditCategory} onChange={this.handleChange('EditCategory')}>
                                  <option value="">-</option>
                                  {this.state.userSkillCategories.map(( listValue, index ) => {
                                    if(listValue.categoryId != process.env.API_HUMAN_RESOURCES) { //Human Resources
                                      return (
                                        <option key={index} value={listValue.categoryId}>{listValue.categoryName}</option>
                                      );
                                    }
                                  })}
                                </select>
                                <span className="form-text error-font-color">{this.state.errormsg["EditCategory"]}</span>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>SubCategory<span className="text-danger">*</span></label>
                                <select className="form-control" value={this.state.EditSubCategory} onChange={this.handleChange('EditSubCategory')}>
                                  <option value="">-</option>
                                  {this.state.AllSubCategory.map(( listValue, index ) => {
                                    return (
                                      <option key={index} value={listValue.subCategoryId} data-Editfrequency={listValue.frequency} >{listValue.subCategoryName}</option>
                                    );
                                  })}
                                </select>
                                <span className="form-text error-font-color">{this.state.errormsg["EditSubCategory"]}</span>
                              </div>
                            </div>

                            {this.state.FrequencyEditCheckBox == true ?
                              <div className="col-md-6">
                                <div className="form-group">
                                  <label></label>
                                  {/*<div class="checkbox">
                                    {this.state.EditskillCheckBox == true ?
                                      <label>Verified<input type="checkbox" className="skill_checkbox" name="EditskillCheckBox" id="EditskillCheckBox" checked /></label>
                                      :<label>Verified<input type="checkbox" className="skill_checkbox" name="EditskillCheckBox" id="EditskillCheckBox" /></label> check={this.state.checked} onChange={this.handleCheck}
                                    }
                                  </div>*/}
                                  <div class="checkbox">
                                    <label>Verified<input type="checkbox" className="skill_checkbox" name="EditskillCheckBox" id="EditskillCheckBox"  /> </label>
                                    
                                  </div>
                                  <span className="form-text error-font-color">{this.state.errormsg["EditskillCheckBox"]}</span>
                                </div>
                              </div>: null
                            }

                            {this.state.FrequencyEditInputBox == true && this.state.FrequencyEditInputBox == true ?
                              <div className="col-md-6">
                                <div className="form-group">
                                  <label>Remarks</label>
                                  <input className="form-control" type="text" value={this.state.EditskillDescription} onChange={this.handleChange('EditskillDescription')}/>
                                  <span className="form-text error-font-color">{this.state.errormsg["EditskillDescription"]}</span>
                                </div>
                              </div>: null
                            }

                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Start Date {this.state.FrequencyEditStartDateMandatory == true ? <span className="text-danger">*</span> : null } </label>
                                <Datetime
                                  inputProps={{readOnly: true}}
                                  closeOnTab={true}
                                  input={true}
                                  value={(this.state.EditStartDate) ? this.state.EditStartDate : ''}
                                  onChange={this.handleEditStartDate}
                                  dateFormat={process.env.DATE_FORMAT}
                                  timeFormat={false}
                                  renderInput={(props) => {
                                     return <input {...props} value={(this.state.EditStartDate) ? props.value : ''} />
                                  }}
                                />
                                {/*<input className="form-control" type="date" value={this.state.EditStartDate} onChange={this.handleChange('EditStartDate')} />*/}
                                <span className="form-text error-font-color">{this.state.errormsg["EditStartDate"]}</span>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>End Date {this.state.FrequencyEditEndDateMandatory == true ? <span className="text-danger">*</span> : null } </label>
                                <Datetime
                                  inputProps={{readOnly: true}}
                                  isValidDate={this.validationEditEndDate}
                                  closeOnTab={true}
                                  input={true}
                                  value={(this.state.EditEndDate) ? this.state.EditEndDate : ''}
                                  onChange={this.handleEditEndDate}
                                  dateFormat={process.env.DATE_FORMAT}
                                  timeFormat={false}
                                  renderInput={(props) => {
                                     return <input {...props} value={(this.state.EditEndDate) ? props.value : ''} />
                                  }}
                                />
                                {/*<input className="form-control" type="date" value={this.state.EditEndDate} onChange={this.handleChange('EditEndDate')} min={moment().format(this.state.EditStartDate,"YYYY-MM-DD")} />*/}
                                <span className="form-text error-font-color">{this.state.errormsg["EditEndDate"]}</span>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Skill Level</label>
                                <select className="form-control" value={this.state.EditSkillLevel} onChange={this.handleChange('EditSkillLevel')}>
                                  <option value="">-</option>
                                  {this.state.SkillLevel.map(( listValue, index ) => {
                                    return (
                                      <option key={index} value={listValue.name}>{listValue.name}</option>
                                    );
                                  })}
                                </select>
                                <span className="form-text error-font-color">{this.state.errormsg["EditSkillLevel"]}</span>
                              </div>
                            </div>
                          </div>
                          <div className="submit-section">
                            <button className="btn btn-primary submit-btn" onClick={this.UpdateRecord()}>Update</button>
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
            {/* //Skills Edit Modal */}

            {/* Delete Skills Locations  Modal */}
            <div className="modal custom-modal fade" id={this.props.page_type == "dashboard_Expiry90Days" ? "Skills_skills_Delete_modal90" : "Skills_skills_Delete_modal" } role="dialog">
              <div className="modal-dialog modal-dialog-centered">
                <div className="modal-content">
                  <div className="modal-body">
                    <div className="form-header">
                      <h3>{this.state.DropCategoryName}</h3>
                      <p>Are you sure you want to mark skills as {this.state.isDelete == true ? 'Active' : 'Inactive' } ?</p>
                    </div>
                    <div className="modal-btn delete-action">
                      <div className="row">
                        <div className="col-6">
                          <a  onClick={this.DeleteRecord()} className="btn btn-primary continue-btn">{this.state.isDelete == true ? 'Active' : 'Inactive' }</a>
                        </div>
                        <div className="col-6">
                          <a href="" data-dismiss="modal" className="btn btn-primary cancel-btn">Cancel</a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            {/* /Delete Trained Locations Modal */}


          {/* /****************** Skills Tab Modals ******************* */}
        </div>
      );
   }
}

export default Skills;
