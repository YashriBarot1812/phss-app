/**
 * Signin Firebase
 */

import React, { Component } from 'react';
import { Helmet } from "react-helmet";

import moment from 'moment';

import Loader from '../../Loader';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import SystemHelpers from '../../Helpers/SystemHelper';

//table
import { MDBDataTable } from 'mdbreact';
import '@fortawesome/fontawesome-free/css/all.min.css';
import 'bootstrap-css-only/css/bootstrap.min.css';
import 'mdbreact/dist/css/mdb.css';
//table

import CryptoAES from 'crypto-js/aes';
import CryptoENC from 'crypto-js/enc-utf8';

import Datetime from "react-datetime";

/* 
1) Dev and Qa
Skill Category = Human Resources
GUID = 'e261ef96-216d-eb11-a812-000d3a3df1ae'

2) ETL
Skill Category = Human Resources
GUID = '08737bcf-2f96-eb11-b1ac-000d3a84e784'
value="08737bcf-2f96-eb11-b1ac-000d3a84e784"
*/

class Skills extends Component {
  constructor(props) {
    super(props);

    this.state = {
        ListGrid:[],
        errormsg : '',
        user_role: [],
        staffContactID:this.props.staffContactID,
        AllSubCategory:[],
        userSkillCategories:[],
        AddCategory:process.env.API_HUMAN_RESOURCES,
        AddSubCategory:'',
        AddStartDate:'',
        AddEndDate:'',
        EditCategory :process.env.API_HUMAN_RESOURCES,
        role_human_resource_can: {},

        isDelete : false,

        header_data : [],

        //HR
        BenefitPlan: false,
        InsuranceClass: '',

        benefitPlan_update: false,
        insuranceClass_update : '',

        staffContactFullname : localStorage.getItem('fullName')

    };
    this.setPropState = this.setPropState.bind(this);

    this.handleAddStartDate = this.handleAddStartDate.bind(this);
    this.handleAddEndDate = this.handleAddEndDate.bind(this);

    this.handleEditStartDate = this.handleEditStartDate.bind(this);
    this.handleEditEndDate = this.handleEditEndDate.bind(this);

    this.handleChange = this.handleChange.bind(this);
  }

  setPropState(key, value) {
      this.setState({ [key]: value });
  }

  handleAddStartDate = (date) =>{
    console.log('AddStartDate => '+ date);
    this.setState({ AddStartDate : date });
    this.setState({ AddEndDate : '' });
  };

  handleAddEndDate = (date) =>{
    console.log('AddEndDate => '+ date);
    this.setState({ AddEndDate : date });
  };

  handleEditStartDate = (date) =>{
    console.log('EditStartDate => '+ date);
    this.setState({ EditStartDate : date });
    this.setState({ EditEndDate : '' });
  };

  handleEditEndDate = (date) =>{
    console.log('EditEndDate => '+ date);
    this.setState({ EditEndDate : date });
  };

  validationAddEndDate = (currentDate) => {
    return currentDate.isAfter(moment(this.state.AddStartDate,process.env.API_DATE_FORMAT));
  };

  validationEditEndDate = (currentDate) => {
    return currentDate.isAfter(moment(this.state.EditStartDate,process.env.API_DATE_FORMAT));
  };

  handleChange = input => e => {
    this.setState({ [input]: e.target.value.replace(/^\s+/g, '') });
    //console.log(input);
    if (this.state[input] != '') {
        delete this.state.errormsg[input];
    }

    if([input]=="AddStartDate")
    {
      //this.setState({ AddEndDate: moment(e.target.value).format('YYYY-MM-DD') });
      this.setState({ AddEndDate: '' });
    }

    if([input]=="EditStartDate")
    {
      //this.setState({ EditndDate: moment(e.target.value).format('YYYY-MM-DD') });
      this.setState({ EditndDate: '' });
    }

    
  }

  // Loader hide show method
  hideLoader = () => {
    this.setState({ loading: false });
  }

  showLoader = () => {
    this.setState({ loading: true });
  }
  // Loader hide show method

  componentDidMount() {

    /* Role Management */
     console.log('Role Store human_resource_can');
     /*var getrole = SystemHelpers.GetRole();
     let human_resource_can = getrole.human_resource_can;
     this.setState({ role_human_resource_can: human_resource_can });
     console.log(human_resource_can);*/

    console.log(this.props.human_resource_can);
    let human_resource_can = this.props.human_resource_can;
    this.setState({ role_human_resource_can: this.props.human_resource_can });
    /* Role Management */

    // this.GetUserSkillINfo();
    // this.GetSkillSubCategory();

    // this.GetContactHRModel();

    // Delete Permison
    if(human_resource_can.human_resource_can_delete == true){
    var columns = [
                {
                  label: 'Category',
                  field: 'skillCategoryName',
                  sort: 'asc',
                  width: 150
                },
                {
                  label: 'SubCategory',
                  field: 'skillSubCategoryName',
                  sort: 'asc',
                  width: 150
                },
                {
                  label: 'Start Date',
                  field: 'startDate',
                  sort: 'asc',
                  width: 150
                },
                {
                  label: 'End Date',
                  field: 'endDate',
                  sort: 'asc',
                  width: 150
                },
                {
                  label: 'Status',
                  field: 'status',
                  sort: 'asc',
                  width: 150
                },
                {
                  label: 'Action',
                  field: 'action',
                  width: 270
                }
              ];

    this.setState({ header_data: columns });
    }else{
    var columns = [
                {
                  label: 'Category',
                  field: 'skillCategoryName',
                  sort: 'asc',
                  width: 150
                },
                {
                  label: 'SubCategory',
                  field: 'skillSubCategoryName',
                  sort: 'asc',
                  width: 150
                },
                {
                  label: 'Start Date',
                  field: 'startDate',
                  sort: 'asc',
                  width: 150
                },
                {
                  label: 'End Date',
                  field: 'endDate',
                  sort: 'asc',
                  width: 150
                },
                {
                  label: 'Action',
                  field: 'action',
                  width: 270
                }
              ];

    this.setState({ header_data: columns });
    }
    // Delete Permison
  }

  TabClickOnLoadHumanResources = () => e => {
    //debugger;
    e.preventDefault();

    this.GetUserSkillINfo();
    this.GetSkillSubCategory();

    this.GetContactHRModel();
  }

  GetContactHRModel(){
      //this.showLoader();
      var url=process.env.API_API_URL+'GetContactHRModel?contactId='+this.state.staffContactID;
      fetch(url, {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'bearer '+localStorage.getItem("token")
        },
      }).then((response) => response.json())
      .then(data => {
          console.log("responseJson GetContactHRModel");
          console.log(data);
          if (data.responseType === "1") {
              
              
                if(data.data.benefitPlan == true){
                  
                  this.setState({ BenefitPlan: "Yes" });
                  this.setState({ benefitPlan_update: "true" });
                }else{
                  
                  this.setState({ BenefitPlan: "No" });
                  this.setState({ benefitPlan_update: "false" });
                }
               
               
             
              
              if(data.data.insuranceClass != "" && data.data.insuranceClass != null){
               this.setState({ InsuranceClass: data.data.insuranceClass });
               this.setState({ insuranceClass_update: data.data.insuranceClass });
              }

          }else{
            if(data.message == 'Authorization has been denied for this request.'){
              SystemHelpers.SessionOut();
              this.props.history.push("/login");
            }else{
              SystemHelpers.ToastError(data.message);
            }
                
          }
          //this.hideLoader();
          
      })
      .catch(error => {
        this.props.history.push("/error-500");
      });
  }

  UpdateContactHRModel = () => e => {
    //debugger;
    e.preventDefault();

    let step1Errors = {};
    
    if (this.state["benefitPlan_update"] =='') {
      step1Errors["benefitPlan_update"] = "Benefit Plan is mandatory";
    }

 
    if (this.state["insuranceClass_update"].trim() == '') {
      step1Errors["insuranceClass_update"] = "Insurance Class is mandatory";
    }



    //console.log(step1Errors);

    this.setState({ errormsg: step1Errors });
        
    if (Object.keys(step1Errors).length > 0) {
        return false;
    }

    //return false;

    /* *************************************************************************** */
      if(this.state["insuranceClass_update"] != '' && this.state["insuranceClass_update"] != null){
        var insuranceClass_update = this.state["insuranceClass_update"].trim();  
      }else{
        var insuranceClass_update = this.state["insuranceClass_update"];
      }
    /* *************************************************************************** */

    this.showLoader();
    

     
    let bodyarray = {};
    bodyarray["benefitPlan"] = this.state["benefitPlan_update"];
    bodyarray["insuranceClass"] = insuranceClass_update;
    bodyarray["contactId"] = this.state.staffContactID;
    bodyarray["loggedInUserName"] = this.state.staffContactFullname;
    
    console.log(bodyarray);
    //return false;
    var url=process.env.API_API_URL+'UpdateContactHRModel';
    fetch(url, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
      body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
        console.log("responseJson UpdateContactHRModel");
        console.log(data);
        //console.log(responseJson);
        // debugger;
        if (data.responseType === "1") {
            //this.props.history.push('/dashboard');
            SystemHelpers.ToastSuccess('Human Resources Updated Successfully..!');
            //SystemHelpers.ToastSuccess(data.responseMessge);
            $( ".close" ).trigger( "click" );
            this.GetContactHRModel();    
        }
        else{
            SystemHelpers.ToastError(data.message  );
        }
        this.hideLoader();
        
    })
    .catch(error => {
      this.props.history.push("/error-500");
    });
    return false;
  }

  GetUserSkillINfo(){
    this.showLoader();

    /* Role Management */
    var getrole = SystemHelpers.GetRole();
    let canDelete = getrole.human_resource_can.human_resource_can_delete;
    /* Role Management */

    var url=process.env.API_API_URL+'GetUserSkillINfo?contactId='+this.state.staffContactID+'&canDelete='+canDelete;
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
      //body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
        console.log("responseJson GetUserSkillINfo");
        console.log(data);
        //console.log(data.data.userRole);
        // debugger;
        if (data.responseType === "1") {
            // Profile & Contact
            //this.setState({ ListGrid: data.data.userSkillInfo });
            console.log(data.data.userSkillInfo);
            this.setState({ ListGrid: this.rowData(data.data.userSkillInfo) })
            this.setState({ userSkillCategories: data.data.userSkillCategories });

            
        }else{
              if(data.message == 'Authorization has been denied for this request.'){
                SystemHelpers.SessionOut();
                this.props.history.push("/login");
              }else{
                SystemHelpers.ToastError(data.message);
              }
              
        }
        this.hideLoader();
        
    })
    .catch(error => {
      this.props.history.push("/error-500");
    });
  }

  GetSkillSubCategory(CatId){
    this.showLoader();
    var CatId = this.state.AddCategory;
    var url=process.env.API_API_URL+'GetSkillSubCategory?categoryId='+CatId;
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
      //body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
        console.log("responseJson GetSkillSubCategory");
        console.log(data);
        //console.log(data.data.userRole);
        // debugger;
        if (data.responseType === "1") {
            // Profile & Contact
           this.setState({ AllSubCategory: data.data });
            
        }else{
              if(data.message == 'Authorization has been denied for this request.'){
                SystemHelpers.SessionOut();
                this.props.history.push("/login");
              }else{
                SystemHelpers.ToastError(data.message);
              }
              
        }
        this.hideLoader();
        
    })
    .catch(error => {
      this.props.history.push("/error-500");
    });
  }

  Edit_Update_Btn_Func(record){
    let return_push = [];

    if(this.state.role_human_resource_can.human_resource_can_update == true || this.state.role_human_resource_can.human_resource_can_delete == true){
      let Edit_push = [];
      if(this.state.role_human_resource_can.human_resource_can_update == true){
        Edit_push.push(
          <a href="#" onClick={this.EditRecord(record)} className="dropdown-item" data-toggle="modal" data-target="#HumanResources_Edit_modal"><i className="fa fa-pencil m-r-5" ></i> Edit</a>
        );
      }
      let Delete_push = [];
      if(this.state.role_human_resource_can.human_resource_can_delete == true){
        if(record.isDelete == false)
        {  
          Delete_push.push(
            <a href="#" onClick={this.DeleteInfo(record)}  className="dropdown-item" data-toggle="modal" data-target="#HumanResources_Delete_modal"><i className="fa fa-trash-o m-r-5" ></i> Inactive</a>
          );
        }
        else
        {
          Delete_push.push(
            <a href="#" onClick={this.DeleteInfo(record)}  className="dropdown-item" data-toggle="modal" data-target="#HumanResources_Delete_modal"><i className="fa fa-trash-o m-r-5" ></i> Active</a>
          );
        }
      }
      
      return_push.push(
        <div className="dropdown dropdown-action">
          <a aria-expanded="false" data-toggle="dropdown" className="action-icon dropdown-toggle" href="#"><i className="material-icons">more_vert</i></a>
          <div className="dropdown-menu dropdown-menu-right">
            {Edit_push}
            {Delete_push}
          </div>
        </div>
      );
    }
    return return_push;
  }

  EditRecord = (record) => e => {
    e.preventDefault();
    console.log(record);

    this.setState({ errormsg: '' });

    this.setState({ skillId: record.skillId });
    this.setState({ EditCategory: record.skillCategoryId });
    this.setState({ EditSubCategory: record.skillSubCategoryId });
    //this.setState({ EditStartDate: moment(record.startDate).format('YYYY-MM-DD') });
    this.setState({ EditStartDate: moment(record.startDate,process.env.API_DATE_FORMAT) });
    if(record.endDate != '')
    {
      //this.setState({ EditEndDate: moment(record.endDate).format('YYYY-MM-DD') });
      this.setState({ EditEndDate: moment(record.endDate,process.env.API_DATE_FORMAT) });
    }
    else
    {
      this.setState({ EditEndDate: '' });
    }
    this.GetSkillSubCategory(record.skillCategoryId);
  }

  DeleteInfo = (record) => e =>{
    e.preventDefault();

    this.setState({ skillId: record.skillId });
    this.setState({ isDelete: record.isDelete });
  }

  ClearRecord = ()=> e => {
    e.preventDefault();

    this.setState({ AddCategory: process.env.API_HUMAN_RESOURCES });
    this.setState({ AddSubCategory : 0 });
    this.setState({ AddStartDate: '' });
    this.setState({ AddEndDate: '' });

    this.setState({ errormsg: '' });
  }

  AddRecord = () => e => {
    //debugger;
    e.preventDefault();

    let step1Errors = {};
    
    if (this.state["AddCategory"] =='') {
      step1Errors["AddCategory"] = "Category is mandatory";
    }

    if (this.state["AddSubCategory"] == '') {
      step1Errors["AddSubCategory"] = "SubCategory is mandatory";
    }

    if (this.state["AddStartDate"] == '') {
      step1Errors["AddStartDate"] = "Start Date is mandatory";
    }

    /*if (this.state["AddEndDate"] == '') {
      step1Errors["AddEndDate"] = "End Date is mandatory";
    }*/


    //console.log(step1Errors);

    this.setState({ errormsg: step1Errors });
        
    if (Object.keys(step1Errors).length > 0) {
        return false;
    }

    //return false;

    this.showLoader();
    var AddStartDate=moment(this.state["AddStartDate"]).format('MM-DD-YYYY');
    //var NewAddStartDate = moment(AddStartDate, "MM-DD-YYYY").add(1, 'days');

    var end_date ='';
    if (this.state["AddEndDate"] != '') {
      var end_date=moment(this.state["AddEndDate"]).format('MM-DD-YYYY');
      //var NewAddEndDate = moment(AddEndDate, "MM-DD-YYYY").add(1, 'days');
      //var end_date=moment(NewAddEndDate).format('MM-DD-YYYY');
    }    
    

    let ArrayJson = {
          skillCategoryId: this.state["AddCategory"],
          skillSubCategoryId: this.state["AddSubCategory"],
          //startDate: moment(NewAddStartDate).format('MM-DD-YYYY'),
          startDate: AddStartDate,
          endDate: end_date
    };
     
    let bodyarray = {};
    bodyarray["contactId"] = this.state.staffContactID;
    bodyarray["userSkillInfo"] = ArrayJson;

    console.log(bodyarray);
    //return false;
    var url=process.env.API_API_URL+'CreateUserSkillSet';
    fetch(url, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
      body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
        console.log("responseJson userSkillInfo");
        console.log(data);
        //console.log(responseJson);
        // debugger;
        if (data.responseType === "1") {
            //this.props.history.push('/dashboard');

            this.setState({ AddCategory: process.env.API_HUMAN_RESOURCES });
            this.setState({ AddSubCategory : 0 });
            this.setState({ AddStartDate: '' });
            this.setState({ AddEndDate: '' });
            //this.setState({ AllSubCategory : [] });
            //AllSubCategory userSkillCategories
            SystemHelpers.ToastSuccess('Human Resources Created Successfully..!');
            //SystemHelpers.ToastSuccess(data.responseMessge);
            $( ".close" ).trigger( "click" );
            this.GetUserSkillINfo();    
        }
        else{
            SystemHelpers.ToastError(data.message  );
        }
        this.hideLoader();
        
    })
    .catch(error => {
      this.props.history.push("/error-500");
    });
    return false;
  }

  UpdateRecord = () => e => {
    //debugger;
    e.preventDefault();

    let step1Errors = {};
    
    if (this.state["EditCategory"] =='') {
      step1Errors["EditCategory"] = "Category is mandatory";
    }

 
    if (this.state["EditSubCategory"] == '') {
      step1Errors["EditSubCategory"] = "SubCategory is mandatory";
    }

    if (this.state["EditStartDate"] == '') {
      step1Errors["EditStartDate"] = "Start Date is mandatory";
    }

    /*if (this.state["EditEndDate"] == '') {
      step1Errors["EditEndDate"] = "End Date is mandatory";
    }*/


    //console.log(step1Errors);

    this.setState({ errormsg: step1Errors });
        
    if (Object.keys(step1Errors).length > 0) {
        return false;
    }

    //return false;

    this.showLoader();
    var EditStartDate=moment(this.state["EditStartDate"]).format('MM-DD-YYYY');
    //var NewEditStartDate = moment(EditStartDate, "MM-DD-YYYY").add(1, 'days');
    

    var edit_end_date ='';
    if (this.state["EditEndDate"] != '') {
      var edit_end_date=moment(this.state["EditEndDate"]).format('MM-DD-YYYY');
      //var NewEditEndDate = moment(EditEndDate, "MM-DD-YYYY").add(1, 'days');
      //var edit_end_date=moment(NewEditEndDate).format('MM-DD-YYYY');
    }

    let ArrayJson = {
          skillCategoryId: this.state["EditCategory"],
          skillSubCategoryId: this.state["EditSubCategory"],
          //startDate: moment(NewEditStartDate).format('MM-DD-YYYY'),
          startDate: EditStartDate,
          endDate: edit_end_date,
          skillId: this.state["skillId"]
    };
     
    let bodyarray = {};
    bodyarray["contactId"] = this.state.staffContactID;
    bodyarray["userSkillInfo"] = ArrayJson;
    bodyarray["userName"] = this.state.staffContactFullname;

    console.log(bodyarray);
    //return false;
    var url=process.env.API_API_URL+'UpdateUserSkillSet';
    fetch(url, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
      body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
        console.log("responseJson UpdateUserSkillSet");
        console.log(data);
        //console.log(responseJson);
        // debugger;
        if (data.responseType === "1") {
            //this.props.history.push('/dashboard');
            SystemHelpers.ToastSuccess('Human Resources Updated Successfully..!');
            //SystemHelpers.ToastSuccess(data.responseMessge);
            $( ".close" ).trigger( "click" );
            this.GetUserSkillINfo();    
        }
        else{
            SystemHelpers.ToastError(data.message  );
        }
        this.hideLoader();
        
    })
    .catch(error => {
      this.props.history.push("/error-500");
    });
    return false;
  }

  DeleteRecord = () => e => {
    e.preventDefault();

    var isdelete = '';
    if(this.state.isDelete== true)
    {
      isdelete = false;
    }
    else
    {
      isdelete = true;
    }

    this.showLoader();
    console.log(this.state.skillId);
    var url=process.env.API_API_URL+'DeleteUserSkillSet?skillId='+this.state.skillId+'&isDelete='+isdelete+'&userName='+this.state.staffContactFullname;
    fetch(url, {
      method: 'PUT',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
      //body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
        console.log("responseJson DeleteUserSkillSet");
        console.log(data);
        //console.log(data.data.userRole);
        // debugger;
        if (data.responseType === "1") {
            // Profile & Contact
            SystemHelpers.ToastSuccess(data.responseMessge);
            $( ".cancel-btn" ).trigger( "click" );
            this.GetUserSkillINfo();
            this.hideLoader();
        }else if (data.responseType == "2" || data.responseType == "3") {
            SystemHelpers.ToastError(data.responseMessge);
            $( ".cancel-btn" ).trigger( "click" );
            this.hideLoader();
        }else{
              if(data.message == 'Authorization has been denied for this request.'){
                SystemHelpers.SessionOut();
                this.props.history.push("/login");
              }else{
                SystemHelpers.ToastError(data.message);
              }
              this.hideLoader();
              $( ".cancel-btn" ).trigger( "click" );
        }
        
        
    })
    .catch(error => {
      this.props.history.push("/error-500");
    });
  }

  rowData(ListGrid) {

    console.log('RowData get human Resources')
    
    /* Role Management */
    var getrole = SystemHelpers.GetRole();
    let canDelete = getrole.human_resource_can.human_resource_can_delete;
    /* Role Management */


      var ListGrid_length = ListGrid.length;
      let dataArray = [];
      var i=1;
      for (var z = 0; z < ListGrid_length; z++) {
        var tempdataArray = [];
        //tempdataArray.rownum = i;
        //console.log(ListGrid[z].skillCategoryId)
        if(ListGrid[z].skillCategoryId == this.state.AddCategory) { //Human Resources
          tempdataArray.skillCategoryName = ListGrid[z].skillCategoryName;
          tempdataArray.skillSubCategoryName = ListGrid[z].skillSubCategoryName;
          tempdataArray.startDate = moment(ListGrid[z].startDate,process.env.API_DATE_FORMAT).format(process.env.DATE_FORMAT);
          
          if(ListGrid[z].endDate == ""){
            tempdataArray.endDate = "";
          }else{
            tempdataArray.endDate = moment(ListGrid[z].endDate,process.env.API_DATE_FORMAT).format(process.env.DATE_FORMAT);
          }
          

          if(canDelete == true)
          {
            var status = "";
            if(ListGrid[z].isDelete == true){
              tempdataArray.status = <div><span class="badge bg-inverse-warning">Inactive</span></div>;
            }else{
              tempdataArray.status = <div><span class="badge bg-inverse-success">Active</span></div>;
            }
          }
          tempdataArray.action = this.Edit_Update_Btn_Func(ListGrid[z]);
          
        }
        dataArray.push(tempdataArray);
        i++; 
      }

      return dataArray;
  }
  
  render() {
     const data = {
              columns: this.state.header_data,
              rows: this.state.ListGrid
            };
      return (
        <div>
        {/* Toast & Loder method use */}
            
        {(this.state.loading) ? <Loader /> : null} 
        {/* Toast & Loder method use */}
        <div className="row">
          <div className="col-md-6 d-flex">
              <div className="card profile-box flex-fill pk-profile-box">

                <div className="row">
                  <button className="btn btn-primary submit-btn pk-profiletab-refreshbtn-hide" id="TabClickOnLoadHumanResources" onClick={this.TabClickOnLoadHumanResources()}>Refresh</button>
                </div>

                <div className="card-body">
                  
                    <h3 className="card-title">Human Resources<a href="#" className="edit-icon" data-toggle="modal" data-target="#human_resources_new"><i className="fa fa-pencil" /></a></h3>
                    
                  <ul className="personal-info">
                    <li>
                      <div className="title">Benefit Plan</div>
                      {this.state.BenefitPlan == '' || this.state.BenefitPlan == null? <div className="text hide-font">None</div> : <div className="text">{this.state.BenefitPlan}</div> }
                    </li>
                    <li>
                      <div className="title">Insurance Class</div>
                      {this.state.InsuranceClass == '' || this.state.InsuranceClass == null? <div className="text hide-font">None</div> : <div className="text">{this.state.InsuranceClass}</div> }
                    </li>
                    
                  </ul>
                </div>
              </div>
            </div>

            {/* PHSS Employment Information Modal */}
            <div id="human_resources_new" className="modal custom-modal fade" role="dialog">
              <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div className="modal-content">
                  <div className="modal-header">
                    <h5 className="modal-title">Human Resources</h5>
                    <button type="button" className="close" id="close_btn_hr_new" data-dismiss="modal" aria-label="Close" onClick={this.ClearRecord()} >
                      <span aria-hidden="true">×</span>
                    </button>
                  </div>
                  <div className="modal-body">
                    <form>
                      <div className="card">
                        <div className="card-body">
                          <div className="row">
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Benefit Plan<span className="text-danger">*</span></label>
                                <select className="form-control" id="benefitPlan_update" value={this.state.benefitPlan_update} onChange={this.handleChange('benefitPlan_update')} >
                                  <option value="">-</option>
                                  <option value="true">Yes</option>
                                  <option value="false">No</option>
                                </select>
                                <span className="form-text error-font-color">{this.state.errormsg["benefitPlan_update"]}</span> 
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Insurance Class<span className="text-danger">*</span></label>
                                <input type="text" className="form-control" value={this.state.insuranceClass_update} onChange={this.handleChange('insuranceClass_update')} />
                                <span className="form-text error-font-color">{this.state.errormsg["insuranceClass_update"]}</span>
                              </div>
                            </div>
                            

                          </div>
                          <div className="submit-section">
                            <button className="btn btn-primary submit-btn" type="submit" onClick={this.UpdateContactHRModel()} >Submit</button>
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
            {/* /PHSS Employment Information Modal */}
        </div>
        <div className="row">
          <div className="col-md-12 d-flex">
            <div className="card profile-box flex-fill">
              <div className="card-body">
                
                {this.state.role_human_resource_can.human_resource_can_create == true ?

                <h3 className="card-title">Human Resources<a href="#" className="edit-icon" data-toggle="modal" data-target="#HumanResources_Add_modal"><i className="fa fa-plus" /></a></h3>
                  : <h3 className="card-title">Human Resources<a href="#" className="phss-lock"><i className="fa fa-lock" /></a></h3>
                }

                
                <div className="table-responsive">
                  <MDBDataTable
                    striped
                    bordered
                    small
                    data={data}
                    entriesOptions={[5, 10, 20, 50, 100]} entries={5} pagesAmount={4}
                    className="table table-striped custom-table mb-0 datatable"
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
        {/* ****************** Human Resources Tab Modals ******************* */}
            {/* Human Resources Modal */}
            <div id="HumanResources_Add_modal" className="modal custom-modal fade" role="dialog">
              <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div className="modal-content">
                  <div className="modal-header">
                    <h5 className="modal-title">Human Resources</h5>
                    <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={this.ClearRecord()} >
                      <span aria-hidden="true">×</span>
                    </button>
                  </div>
                  <div className="modal-body">
                    <form>
                      <div className="card">
                        <div className="card-body">
                          <div className="row">
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Category<span className="text-danger">*</span></label>
                                <select className="form-control" value={this.state.AddSubCategory} onChange={this.handleChange('AddSubCategory')}>
                                  <option value="">-</option>
                                  {this.state.AllSubCategory.map(( listValue, index ) => {
                                    return (
                                      <option key={index} value={listValue.subCategoryId}>{listValue.subCategoryName}</option>
                                    );
                                  })}
                                </select>
                                <span className="form-text error-font-color">{this.state.errormsg["AddSubCategory"]}</span>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Start Date<span className="text-danger">*</span></label>
                                <Datetime
                                inputProps={{readOnly: true}}
                                closeOnTab={true}
                                input={true}
                                value={(this.state.AddStartDate) ? this.state.AddStartDate : ''}
                                onChange={this.handleAddStartDate}
                                dateFormat={process.env.DATE_FORMAT}
                                timeFormat={false}
                                renderInput={(props) => {
                                   return <input {...props} value={(this.state.AddStartDate) ? props.value : ''} />
                                }}
                              />
                                {/*<input className="form-control" type="date" value={this.state.AddStartDate} onChange={this.handleChange('AddStartDate')} />*/}
                                <span className="form-text error-font-color">{this.state.errormsg["AddStartDate"]}</span>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>End Date</label>
                                <Datetime
                                isValidDate={this.validationAddEndDate}
                                inputProps={{readOnly: true}}
                                closeOnTab={true}
                                input={true}
                                value={(this.state.AddEndDate) ? this.state.AddEndDate : ''}
                                onChange={this.handleAddEndDate}
                                dateFormat={process.env.DATE_FORMAT}
                                timeFormat={false}
                                renderInput={(props) => {
                                   return <input {...props} value={(this.state.AddEndDate) ? props.value : ''} />
                                }}
                              />
                                {/*<input className="form-control" type="date" value={this.state.AddEndDate} onChange={this.handleChange('AddEndDate')} min={moment().format(this.state.AddStartDate,"YYYY-MM-DD")} />*/}
                                <span className="form-text error-font-color">{this.state.errormsg["AddEndDate"]}</span>
                              </div>
                            </div>
                          </div>
                          <div className="submit-section">
                            <button className="btn btn-primary submit-btn" onClick={this.AddRecord()}>Submit</button>
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
            {/* //Human Resources Modal */}

          {/* Human Resources Ediit Modal */}
            <div id="HumanResources_Edit_modal" className="modal custom-modal fade" role="dialog">
              <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div className="modal-content">
                  <div className="modal-header">
                    <h5 className="modal-title">Human Resources</h5>
                    <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">×</span>
                    </button>
                  </div>
                  <div className="modal-body">
                    <form>
                      <div className="card">
                        <div className="card-body">
                          <div className="row">
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Category<span className="text-danger">*</span></label>
                                <select className="form-control" value={this.state.EditSubCategory} onChange={this.handleChange('EditSubCategory')}>
                                  <option value="">-</option>
                                  {this.state.AllSubCategory.map(( listValue, index ) => {
                                    return (
                                      <option key={index} value={listValue.subCategoryId}>{listValue.subCategoryName}</option>
                                    );
                                  })}
                                </select>
                                <span className="form-text error-font-color">{this.state.errormsg["EditSubCategory"]}</span>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Start Date<span className="text-danger">*</span></label>
                                <Datetime
                                  inputProps={{readOnly: true}}
                                  closeOnTab={true}
                                  input={true}
                                  value={(this.state.EditStartDate) ? this.state.EditStartDate : ''}
                                  onChange={this.handleEditStartDate}
                                  dateFormat={process.env.DATE_FORMAT}
                                  timeFormat={false}
                                  renderInput={(props) => {
                                     return <input {...props} value={(this.state.EditStartDate) ? props.value : ''} />
                                  }}
                                />
                                {/*<input className="form-control" type="date" value={this.state.EditStartDate} onChange={this.handleChange('EditStartDate')} />*/}
                                <span className="form-text error-font-color">{this.state.errormsg["EditStartDate"]}</span>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>End Date</label>
                                <Datetime
                                  inputProps={{readOnly: true}}
                                  isValidDate={this.validationEditEndDate}
                                  closeOnTab={true}
                                  input={true}
                                  value={(this.state.EditEndDate) ? this.state.EditEndDate : ''}
                                  onChange={this.handleEditEndDate}
                                  dateFormat={process.env.DATE_FORMAT}
                                  timeFormat={false}
                                  renderInput={(props) => {
                                     return <input {...props} value={(this.state.EditEndDate) ? props.value : ''} />
                                  }}
                                />
                                {/*<input className="form-control" type="date" value={this.state.EditEndDate} onChange={this.handleChange('EditEndDate')} min={moment().format(this.state.EditStartDate,"YYYY-MM-DD")} />*/}
                                <span className="form-text error-font-color">{this.state.errormsg["EditEndDate"]}</span>
                              </div>
                            </div>
                          </div>
                          <div className="submit-section">
                            <button className="btn btn-primary submit-btn" onClick={this.UpdateRecord()}>Update</button>
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          {/* //Human Resources Edit Modal */}

          {/* Delete Human Resources Locations  Modal */}
            <div className="modal custom-modal fade" id="HumanResources_Delete_modal" role="dialog">
              <div className="modal-dialog modal-dialog-centered">
                <div className="modal-content">
                  <div className="modal-body">
                    <div className="form-header">
                      <h3>Human Resources</h3>
                      <p>Are you sure you want to mark human resources as {this.state.isDelete == true ? 'Active' : 'Inactive' } ?</p>
                    </div>
                    <div className="modal-btn delete-action">
                      <div className="row">
                        <div className="col-6">
                          <a  onClick={this.DeleteRecord()} className="btn btn-primary continue-btn">{this.state.isDelete == true ? 'Active' : 'Inactive' }</a>
                        </div>
                        <div className="col-6">
                          <a href="" data-dismiss="modal" className="btn btn-primary cancel-btn">Cancel</a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          {/* /Delete Human Resources Modal */}


            {/* /****************** Human Resources Tab Modals ******************* */}
        </div>
      );
   }
}

export default Skills;
