/**
 * Signin Firebase
 */

import React, { Component } from 'react';
import { Helmet } from "react-helmet";
import SystemHelpers from '../../Helpers/SystemHelper';

class RoleManager extends Component {
  constructor(props) {
    super(props);

    this.state = {
        errormsg :  '',
        user_role: [],
        role_func_call : false,
        new_user_role: [],
        role_Address:[],
        role_Awards_Recognitions:[],
        role_Consents_Waivers:[],
        role_Contact_information:[],
        role_Course_Training:[],
        role_Education:[],
        role_Emergency_Contact:[],
        role_Employment_History:[],
        role_Human_Resource:[],
        role_License_AND_Certification:[],
        role_Locations_Departments:[],
        role_Skills:[],
        role_Staff:[],
        role_Volunteer:[],

        profile_info_can_create : false,
        profile_info_can_update : false,
        profile_info_can_view : false,
        profile_info_can_delete : false,

        contact_information_can_create : false,
        contact_information_can_update : false,
        contact_information_can_view : false,
        contact_information_can_delete : false,

        personal_info_can_create : false,
        personal_info_can_update : false,
        personal_info_can_view : false,
        personal_info_can_delete : false,

        phss_empinfo_can_create : false,
        phss_empinfo_can_update : false,
        phss_empinfo_can_view : false,
        phss_empinfo_can_delete : false,

        address_can_create : false,
        address_can_update : false,
        address_can_view : false,
        address_can_delete : false,
        

        emergency_contact_can_create : false,
        emergency_contact_can_update : false,
        emergency_contact_can_view : false,
        emergency_contact_can_delete : false,
        

        employment_history_can_create : false,
        employment_history_can_update : false,
        employment_history_can_view : false,
        employment_history_can_delete : false,
        

        course_training_can_create : false,
        course_training_can_update : false,
        course_training_can_view : false,
        course_training_can_delete : false,
        

        skills_can_create : false,
        skills_can_update : false,
        skills_can_view : false,
        skills_can_delete : false,
        

        consents_waivers_can_create : false,
        consents_waivers_can_update : false,
        consents_waivers_can_view : false,
        consents_waivers_can_delete : false,
        

        awards_recognitions_can_create : false,
        awards_recognitions_can_update : false,
        awards_recognitions_can_view : false,
        awards_recognitions_can_delete : false,
        

        human_resource_can_create : false,
        human_resource_can_update : false,
        human_resource_can_view : false,
        human_resource_can_delete : false,
        

        documents_can_create : false,
        documents_can_update : false,
        documents_can_view : false,
        documents_can_delete : false,

        audit_can_view: false,

        timesheet_can_create : false,
        timesheet_can_update : false,
        timesheet_can_view : false,
        timesheet_can_delete : false,
        timesheet_can_export : false,
        timesheet_can_summary : false,
        timesheet_can_approve : false,

        user_can_create:false,
        user_can_update:false,
        user_can_view:false,
        user_can_delete:false,

        contactlist_can_create:false,
        contactlist_can_update:false,
        contactlist_can_view:false,
        contactlist_can_delete:false,

        locations_can_create:false,
        locations_can_update:false,
        locations_can_view:false,
        locations_can_delete:false,

        employees_can_create:false,
        employees_can_update:false,
        employees_can_view:false,
        employees_can_delete:false
   
    };

    this.setPropState = this.setPropState.bind(this);
  }

  setPropState(key, value) {
      this.setState({ [key]: value });
  }

  // Loader hide show method
  hideLoader = () => {
    this.setState({ loading: false });
  }

  showLoader = () => {
    this.setState({ loading: true });
  }
  // Loader hide show method

  

  componentDidMount() {
    console.log('user_role');
    this.GetUserRoleDetails();
  }

  GetUserRoleDetails(){
    this.showLoader();
    var url=process.env.API_API_URL+'GetUserRoleDetails?contactId='+localStorage.getItem("contactId");
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
      //body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
        console.log("responseJson GetUserRoleDetails");
        console.log(data);
        //console.log(data.data.userRole);
         //debugger;
        if (data.responseType === "1") {
            // Profile & Contact
            var row = data.data;
            var row_count = row.length;
            //console.log("row array");
            var i = 1;
            for (var zz = 0; zz < row_count; zz++) {
              //debugger;
              console.log(i+"JsonArrayfunc");
              var JsonArray = row[zz].userRoleString
              //console.log(JsonArray);
              //var json_replace = JsonArray.replace(/\\|\//g,'')
              //console.log(json_replace);
              var JsonCreate = JSON.parse(JsonArray);
              console.log('json_replace');
              console.log(JsonCreate);
              console.log(JsonCreate.profile_management);
              var RoleJson_profile_management = JsonCreate.profile_management;

              // console.log(i+" profile_info_can_create => "+this.state['profile_info_can_create']);
              // console.log(i+" profile_info_can_update => "+this.state['profile_info_can_update']);
              // console.log(i+" profile_info_can_view => "+this.state['profile_info_can_view']);
              // console.log(i+" profile_info_can_delete => "+this.state['profile_info_can_delete']);



              /* ********************* profile management ********************* */

              var ProfileInfo=RoleJson_profile_management.ProfileInfo;
              if(this.state['profile_info_can_create'] == false){
                this.setState({ profile_info_can_create: ProfileInfo.canCreate });
                this.props.setPropState('profile_info_can_create', ProfileInfo.canCreate);
              }
              
              if(this.state['profile_info_can_update'] == false){
                this.setState({ profile_info_can_update: ProfileInfo.canUpdate })
                this.props.setPropState('profile_info_can_update', ProfileInfo.canUpdate);
              }
              
              if(this.state['profile_info_can_view'] == false){
                this.setState({ profile_info_can_view: ProfileInfo.canView })
                this.props.setPropState('profile_info_can_view', ProfileInfo.canView);
              }

              if(this.state['profile_info_can_delete'] == false){
                this.setState({ profile_info_can_delete: ProfileInfo.canDelete })
                this.props.setPropState('profile_info_can_delete', ProfileInfo.canDelete);
              }


              var Contact_information=RoleJson_profile_management.Contact_information;
              if(this.state['contact_information_can_create'] == false){
                this.setState({ contact_information_can_create: Contact_information.canCreate });
                this.props.setPropState('contact_information_can_create', Contact_information.canCreate);
              }
              
              if(this.state['contact_information_can_update'] == false){
                this.setState({ contact_information_can_update: Contact_information.canUpdate });
                this.props.setPropState('contact_information_can_update', Contact_information.canUpdate);
              }
              
              if(this.state['contact_information_can_view'] == false){
                this.setState({ contact_information_can_view: Contact_information.canView });
                this.props.setPropState('contact_information_can_view', Contact_information.canView);
              }

              if(this.state['contact_information_can_delete'] == false){
                this.setState({ contact_information_can_delete: Contact_information.canDelete });
                this.props.setPropState('contact_information_can_delete', Contact_information.canDelete);
              }

              var PersonalInfo=RoleJson_profile_management.PersonalInfo;
              if(this.state['personal_info_can_create'] == false){
                this.setState({ personal_info_can_create: PersonalInfo.canCreate });
                this.props.setPropState('personal_info_can_create', PersonalInfo.canCreate);
              }
              
              if(this.state['personal_info_can_update'] == false){
                this.setState({ personal_info_can_update: PersonalInfo.canUpdate });
                this.props.setPropState('personal_info_can_update', PersonalInfo.canUpdate);
              }
              
              if(this.state['personal_info_can_view'] == false){
                this.setState({ personal_info_can_view: PersonalInfo.canView });
                this.props.setPropState('personal_info_can_view', PersonalInfo.canView);
              }

              if(this.state['personal_info_can_delete'] == false){
                this.setState({ personal_info_can_delete: PersonalInfo.canDelete });
                this.props.setPropState('personal_info_can_delete', PersonalInfo.canDelete);
              }



              var Phss_EmpInfo=RoleJson_profile_management.Phss_EmpInfo;
              if(this.state['phss_empinfo_can_create'] == false){
                this.setState({ phss_empinfo_can_create: Phss_EmpInfo.canCreate });
                this.props.setPropState('phss_empinfo_can_create', Phss_EmpInfo.canCreate);
              }
              
              if(this.state['phss_empinfo_can_update'] == false){
                this.setState({ phss_empinfo_can_update: Phss_EmpInfo.canUpdate });
                this.props.setPropState('phss_empinfo_can_update', Phss_EmpInfo.canUpdate);
              }
              
              if(this.state['phss_empinfo_can_view'] == false){
                this.setState({ phss_empinfo_can_view: Phss_EmpInfo.canView });
                this.props.setPropState('phss_empinfo_can_view', Phss_EmpInfo.canView);
              }

              if(this.state['phss_empinfo_can_delete'] == false){
                this.setState({ phss_empinfo_can_delete: Phss_EmpInfo.canDelete });
                this.props.setPropState('phss_empinfo_can_delete', Phss_EmpInfo.canDelete);
              }

              

              

              var Address=RoleJson_profile_management.Address;
              if(this.state['address_can_create'] == false){
                this.setState({ address_can_create: Address.canCreate });
                this.props.setPropState('address_can_create', Address.canCreate);
              }
              if(this.state['address_can_update'] == false){
                this.setState({ address_can_update: Address.canUpdate });
                this.props.setPropState('address_can_update', Address.canUpdate);
              }
              if(this.state['address_can_view'] == false){
                this.setState({ address_can_view: Address.canView });
                this.props.setPropState('address_can_view', Address.canView);
              }
              if(this.state['address_can_delete'] == false){
                this.setState({ address_can_delete: Address.canDelete });
                this.props.setPropState('address_can_delete', Address.canDelete);
              }
              

              var Emergency_Contact=RoleJson_profile_management.Emergency_Contact;
              if(this.state['emergency_contact_can_create'] == false){
                this.setState({ emergency_contact_can_create: Emergency_Contact.canCreate });
                this.props.setPropState('emergency_contact_can_create', Emergency_Contact.canCreate);
              }
              if(this.state['emergency_contact_can_update'] == false){
                this.setState({ emergency_contact_can_update: Emergency_Contact.canUpdate });               
                this.props.setPropState('emergency_contact_can_update', Emergency_Contact.canUpdate);
              }
              if(this.state['emergency_contact_can_view'] == false){
                this.setState({ emergency_contact_can_view: Emergency_Contact.canView });
                this.props.setPropState('emergency_contact_can_view', Emergency_Contact.canView);
              }
              if(this.state['emergency_contact_can_delete'] == false){ 
                this.setState({ emergency_contact_can_delete: Emergency_Contact.canDelete });               
                this.props.setPropState('emergency_contact_can_delete', Emergency_Contact.canDelete);
              }
              

              var Employment_History=RoleJson_profile_management.Employment_History;
              if(this.state['employment_history_can_create'] == false){
                this.setState({ employment_history_can_create: Employment_History.canCreate });
                this.props.setPropState('employment_history_can_create', Employment_History.canCreate);
              }
              if(this.state['employment_history_can_update'] == false){
                this.setState({ employment_history_can_update: Employment_History.canUpdate });
                this.props.setPropState('employment_history_can_update', Employment_History.canUpdate);
              }
              if(this.state['employment_history_can_view'] == false){
                this.setState({ employment_history_can_view: Employment_History.canView });
                this.props.setPropState('employment_history_can_view', Employment_History.canView);
              }
              if(this.state['employment_history_can_delete'] == false){
                this.setState({ employment_history_can_delete: Employment_History.canDelete });
                this.props.setPropState('employment_history_can_delete', Employment_History.canDelete);
              }
              

              var Course_Training=RoleJson_profile_management.Course_Training;
              if(this.state['course_training_can_create'] == false){
                this.setState({ course_training_can_create: Course_Training.canCreate });
                this.props.setPropState('course_training_can_create', Course_Training.canCreate);
              }
              if(this.state['course_training_can_update'] == false){
                this.setState({ course_training_can_update: Course_Training.canUpdate });
                this.props.setPropState('course_training_can_update', Course_Training.canUpdate);
              }
              if(this.state['course_training_can_view'] == false){
                this.setState({ course_training_can_view: Course_Training.canView });
                this.props.setPropState('course_training_can_view', Course_Training.canView);
              }
              if(this.state['course_training_can_delete'] == false){
                this.setState({ course_training_can_delete: Course_Training.canDelete });
                this.props.setPropState('course_training_can_delete', Course_Training.canDelete);
              }
              

              var Skills=RoleJson_profile_management.Skills;
              if(this.state['skills_can_create'] == false){
                this.setState({ skills_can_create: Skills.canCreate });
                this.props.setPropState('skills_can_create', Skills.canCreate);
              }
              if(this.state['skills_can_update'] == false){
                this.setState({ skills_can_update: Skills.canUpdate });
                this.props.setPropState('skills_can_update', Skills.canUpdate);
              }
              if(this.state['skills_can_view'] == false){
                this.setState({ skills_can_view: Skills.canView });
                this.props.setPropState('skills_can_view', Skills.canView);
              }
              if(this.state['skills_can_delete'] == false){
                this.setState({ skills_can_delete: Skills.canDelete });
                this.props.setPropState('skills_can_delete', Skills.canDelete);
              }
              
              
              var Consents_Waivers=RoleJson_profile_management.Consents_Waivers;
              if(this.state['consents_waivers_can_create'] == false){
                this.setState({ consents_waivers_can_create: Consents_Waivers.canCreate });
                this.props.setPropState('consents_waivers_can_create', Consents_Waivers.canCreate);
              }
              if(this.state['consents_waivers_can_update'] == false){
                this.setState({ consents_waivers_can_update: Consents_Waivers.canUpdate });
                this.props.setPropState('consents_waivers_can_update', Consents_Waivers.canUpdate);
              }
              if(this.state['consents_waivers_can_view'] == false){
                this.setState({ consents_waivers_can_view: Consents_Waivers.canView });
                this.props.setPropState('consents_waivers_can_view', Consents_Waivers.canView);
              }
              if(this.state['consents_waivers_can_delete'] == false){
                this.setState({ consents_waivers_can_delete: Consents_Waivers.canDelete });
                this.props.setPropState('consents_waivers_can_delete', Consents_Waivers.canDelete);
              }
              

              var Awards_Recognitions=RoleJson_profile_management.Awards_Recognitions;
              if(this.state['awards_recognitions_can_create'] == false){
                this.setState({ awards_recognitions_can_create: Awards_Recognitions.canCreate });
                this.props.setPropState('awards_recognitions_can_create', Awards_Recognitions.canCreate);
              }
              if(this.state['awards_recognitions_can_update'] == false){
                this.setState({ awards_recognitions_can_update: Awards_Recognitions.canUpdate });
                this.props.setPropState('awards_recognitions_can_update', Awards_Recognitions.canUpdate);
              }
              if(this.state['awards_recognitions_can_view'] == false){
                this.setState({ awards_recognitions_can_view: Awards_Recognitions.canView });
                this.props.setPropState('awards_recognitions_can_view', Awards_Recognitions.canView);
              }
              if(this.state['awards_recognitions_can_delete'] == false){
                this.setState({ awards_recognitions_can_delete: Awards_Recognitions.canDelete });
                this.props.setPropState('awards_recognitions_can_delete', Awards_Recognitions.canDelete);
              }
              
             
              var HumanResource=RoleJson_profile_management.HumanResource;
              

              if(this.state['human_resource_can_create'] == false){
                this.setState({ human_resource_can_create: HumanResource.canCreate });
                this.props.setPropState('human_resource_can_create', HumanResource.canCreate);
              }
              
              if(this.state['human_resource_can_update'] == false){
                this.setState({ human_resource_can_update: HumanResource.canUpdate });
                this.props.setPropState('human_resource_can_update', HumanResource.canUpdate);
              }
              
              if(this.state['human_resource_can_view'] == false){
                this.setState({ human_resource_can_view: HumanResource.canView });
                this.props.setPropState('human_resource_can_view', HumanResource.canView);
              }
             
              if(this.state['human_resource_can_delete'] == false){
                this.setState({ human_resource_can_delete: HumanResource.canDelete });
                this.props.setPropState('human_resource_can_delete', HumanResource.canDelete);
              }
              


              var Documents=RoleJson_profile_management.Documents;
              
              if(this.state['documents_can_create'] == false){
                this.setState({ documents_can_create: Documents.canCreate });
                this.props.setPropState('documents_can_create', Documents.canCreate);
              }
              if(this.state['documents_can_update'] == false){
                this.setState({ documents_can_update: Documents.canUpdate });
                this.props.setPropState('documents_can_update', Documents.canUpdate);
              }
              if(this.state['documents_can_view'] == false){
                this.setState({ documents_can_view: Documents.canView });
                this.props.setPropState('documents_can_view', Documents.canView);
              }
              if(this.state['documents_can_delete'] == false){
                this.setState({ documents_can_delete: Documents.canDelete });
                this.props.setPropState('documents_can_delete', Documents.canDelete);
              }


              
              /* ********************* profile management ********************* */
              /* ********************* timeSheet management ********************* */
              console.log(i+'timeSheetJson');
              var JsontimeSheetJson = row[zz].timeSheetJson
              var JsontimeSheet = JSON.parse(JsontimeSheetJson);
              console.log(JsontimeSheet);
              var RoleJson_timeSheetJson = JsontimeSheet.Timesheet;

              var Audit=RoleJson_timeSheetJson.Audit;

              if(this.state['audit_can_view'] == false){
                this.setState({ audit_can_view: Employment_History.canDelete });
                this.props.setPropState('audit_can_view', Audit.canView);
              }


              var Timesheet=RoleJson_timeSheetJson.Timesheet;
              
              if(this.state['timesheet_can_create'] == false){
                this.setState({ timesheet_can_create: Timesheet.canCreate });
                this.props.setPropState('timesheet_can_create', Timesheet.canCreate);
              }
              if(this.state['timesheet_can_update'] == false){
                this.setState({ timesheet_can_update: Timesheet.canUpdate });
                this.props.setPropState('timesheet_can_update', Timesheet.canUpdate);
              }
              if(this.state['timesheet_can_view'] == false){
                this.setState({ timesheet_can_view: Timesheet.canView });
                this.props.setPropState('timesheet_can_view', Timesheet.canView);
              }
              if(this.state['timesheet_can_delete'] == false){
                this.setState({ timesheet_can_delete: Timesheet.canDelete });
                this.props.setPropState('timesheet_can_delete', Timesheet.canDelete);
              }

              if(this.state['timesheet_can_export'] == false){
                this.setState({ timesheet_can_export: Timesheet.Export });
                this.props.setPropState('timesheet_can_export', Timesheet.Export);
              }

              if(this.state['timesheet_can_summary'] == false){
                this.setState({ timesheet_can_summary: Employment_History.Summary });
                this.props.setPropState('timesheet_can_summary', Timesheet.Summary);
              }

              if(this.state['timesheet_can_approve'] == false){
                this.setState({ timesheet_can_approve: Employment_History.canApprove });
                this.props.setPropState('timesheet_can_approve', Timesheet.canApprove);
              }

              /* ********************* timeSheet management ********************* */
              /* ********************* User ProfileJson management ********************* */
              console.log(i+'userProfileJson');
              var userProfileJson = row[zz].userProfileJson
              var JsonuserProfile = JSON.parse(userProfileJson);
              console.log(JsonuserProfile);
              var RoleJson_UserManagement = JsonuserProfile.UserManagement;

              var User=RoleJson_UserManagement.User;
              
              if(this.state['user_can_create'] == false){
                this.setState({ user_can_create: User.canCreate });
                this.props.setPropState('user_can_create', User.canCreate);
              }
              if(this.state['user_can_update'] == false){
                this.setState({ user_can_update: User.canUpdate });
                this.props.setPropState('user_can_update', User.canUpdate);
              }
              if(this.state['user_can_view'] == false){
                this.setState({ user_can_view: User.canView });
                this.props.setPropState('user_can_view', User.canView);
              }
              if(this.state['user_can_delete'] == false){
                this.setState({ user_can_delete: User.canDelete });
                this.props.setPropState('user_can_delete', User.canDelete);
              }


              var ContactList=RoleJson_UserManagement.ContactList;
              
              if(this.state['contactlist_can_create'] == false){
                this.setState({ contactlist_can_create: ContactList.canCreate });
                this.props.setPropState('contactlist_can_create', ContactList.canCreate);
              }
              if(this.state['contactlist_can_update'] == false){
                this.setState({ contactlist_can_update: ContactList.canUpdate });
                this.props.setPropState('contactlist_can_update', ContactList.canUpdate);
              }
              if(this.state['contactlist_can_view'] == false){
                this.setState({ contactlist_can_view: ContactList.canView });
                this.props.setPropState('contactlist_can_view', ContactList.canView);
              }
              if(this.state['contactlist_can_delete'] == false){
                this.setState({ contactlist_can_delete: ContactList.canDelete });
                this.props.setPropState('contactlist_can_delete', ContactList.canDelete);
              }
              /* ********************* User ProfileJson management ********************* */

              /* ********************* masterJson management ********************* */
              console.log(i+'masterJson');
              var masterJson = row[zz].masterJson
              var Jsonmaster = JSON.parse(masterJson);
              console.log(Jsonmaster);
              var RoleJson_Jsonmaster = Jsonmaster.Masters;

              var Locations=RoleJson_Jsonmaster.Locations;
              
              if(this.state['locations_can_create'] == false){
                this.setState({ locations_can_create: Locations.canCreate });
                this.props.setPropState('locations_can_create', Locations.canCreate);
              }
              if(this.state['locations_can_update'] == false){
                this.setState({ locations_can_update: Locations.canUpdate });
                this.props.setPropState('locations_can_update', Locations.canUpdate);
              }
              if(this.state['locations_can_view'] == false){
                this.setState({ locations_can_view: Locations.canView });
                this.props.setPropState('locations_can_view', Locations.canView);
              }
              if(this.state['locations_can_delete'] == false){
                this.setState({ locations_can_delete: Locations.canDelete });
                this.props.setPropState('locations_can_delete', Locations.canDelete);
              }



              var Employees=RoleJson_Jsonmaster.Employees;
              //var tempflage = false;
              if(this.state['employees_can_create'] == false){
                this.setState({ employees_can_create: Employees.canCreate });
                this.props.setPropState('employees_can_create', Employees.canCreate);
              }
              if(this.state['employees_can_update'] == false){
                this.setState({ employees_can_update: Employees.canUpdate });
                this.props.setPropState('employees_can_update', Employees.canUpdate);
              }
              if(this.state['employees_can_view'] == false){
                this.setState({ employees_can_view: Employees.canView });
                this.props.setPropState('employees_can_view', Employees.canView);
              }
              if(this.state['employees_can_delete'] == false){
                this.setState({ employees_can_delete: Employees.canDelete });
                this.props.setPropState('employees_can_delete', Employees.canDelete); 
              }
              //alert("Hello 1 " + i+" "+row_count+" " +tempflage);
              /* ********************* masterJson management ********************* */
              if(i == row_count){
                this.props.setPropState('role_func_call', true);
              }
              i++;
            }
            
            
        }else{
              if(data.message == 'Authorization has been denied for this request.'){
                SystemHelpers.SessionOut();
                this.props.history.push("/login");
              }else{
                SystemHelpers.ToastError(data.message);
              }
              
        }
        this.hideLoader();
        
    })
    .catch(error => {
      this.props.history.push("/error-500");
    });
  }

  

  

  

  render() {
     
      return (
        <div>
        
        </div>
      );
   }
}

export default RoleManager;
