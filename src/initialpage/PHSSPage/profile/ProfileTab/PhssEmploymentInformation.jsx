
/**
 * Signin Firebase
 */

import React, { Component } from 'react';
import { Helmet } from "react-helmet";
//import {Avatar_02,Avatar_05,Avatar_09,Avatar_10,Avatar_16 } from '../../../../../Entryfile/imagepath'
import moment from 'moment';


import Loader from '../../../Loader';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import CryptoAES from 'crypto-js/aes';
import CryptoENC from 'crypto-js/enc-utf8';

import SystemHelpers from '../../../Helpers/SystemHelper';
import { Multiselect } from 'multiselect-react-dropdown';

import Datetime from "react-datetime";

class PhssEmploymentInformation extends Component {
  constructor(props) {
    super(props);

    this.state = {
        errormsg :  '',
        user_role: [],
        staffContactID:this.props.staffContactID,
        all_data :this.props.all_data,
        empDeterminationList:[],
        locationList:[],
        role_phss_empinfo_can:{},

        GetCRMUserList : [],

        timeSheetApproverName : '',
        timeSheetReportName : '',

        empHistoryJobTitleList:[],
        employeeTypeList:[],
        employeementHourList:[],
        roleList : [],

        AddApproverTo : '',
        AddReportTo : '',
        AddjobTitle :  '',
        AddemployeeType :  '',
        AddemployeeHours :  '',
        AdduserRole : [],
        AddhireDate : '',
        AddPhotoShow : '',
        selectedValueRole : [],

        AddDateOfLeaving : '',

        // Display
        staffNumberIdDisplay : '',
        jobTitleNameDisplay : '',
        primaryLocationDisplay : '',
        hireDateDisplay : '',
        employmentTypeDisplay : '',
        employmentStatusDisplay : '',
        employmentHoursDisplay : '',
        photoRequiredDisplay : '',
        reportToUserNameDisplay : '',
        timeSheetApproverNameDisplay : '',
        userRoleDisplayDisplay : '',
        dateofleavingDisplay : '',
        // Display

        staffContactFullname : localStorage.getItem('fullName')
    };
    this.setPropState = this.setPropState.bind(this);
    this.handleChange = this.handleChange.bind(this);

    this.onSelectRole = this.onSelectRole.bind(this);
    this.onRemoveRole = this.onRemoveRole.bind(this);
    this.roleListRef = React.createRef();

    this.handleEditAddhireDate = this.handleEditAddhireDate.bind(this);
    this.handleEditAddDateOfLeaving = this.handleEditAddDateOfLeaving.bind(this);
  }

  handleEditAddhireDate = (date) =>{
    //alert(date);
    console.log('AddhireDate => '+ date);
    //console.log('AddTimeOut_Max => '+ NewDate);
    this.setState({ AddhireDate : date });
  };

  handleEditAddDateOfLeaving = (date) =>{
    console.log('AddDateOfLeaving => '+ date);
    this.setState({ AddDateOfLeaving : date });
  };

  handleChange = input => e => {
    this.setState({ [input]: e.target.value.replace(/^\s+/g, '') });

    if([input]=="Employment_Status_Model")
    {
      if(e.target.value == "Inactive" || e.target.value == "On Hold")
      {
        $('#EmpStateDateOfLeavingDivId').show();
      }
      else
      {
        this.setState({ AddDateOfLeaving : "" });
        $('#EmpStateDateOfLeavingDivId').hide();
      }
    }
  }

  setPropState(key, value) {
      this.setState({ [key]: value });
  }

   // Loader hide show method
  hideLoader = () => {
    this.setState({ loading: false });
  }

  showLoader = () => {
    this.setState({ loading: true });
  }
  // Loader hide show method

  // toast hide show method
  ToastSuccess(msg){
      toast.success(msg, {
        position: "top-right",
        autoClose: process.env.API_TOAST_TIME,
        hideProgressBar: false,
        closeOnClick: false,
        pauseOnHover: false,
        draggable: false
      });
  }

  ToastError(msg){
      toast.error(msg, {
        position: "top-right",
        autoClose: process.env.API_TOAST_TIME,
        hideProgressBar: false,
        closeOnClick: false,
        pauseOnHover: false,
        draggable: false
      });
  }
  // toast hide show method

  // Check Date 
  validationDateHiredate = (currentDate) => {
    const today = moment();
    //min={this.state.AddTimeIn_Min} max={this.state.AddTimeIn_Max}
    //return currentDate.isBefore(moment(this.state.AddTimeIn_Max)) && currentDate.isAfter(moment(this.state.AddTimeIn_Min).subtract(1, 'day'));
    return currentDate.isBefore(today);
  };
  // Check Date

  componentDidMount() {

    /* Role Management */
     console.log('Role Store phss_empinfo_can');
     /*var getrole = SystemHelpers.GetRole();
     let phss_empinfo_can = getrole.phss_empinfo_can;
     this.setState({ role_phss_empinfo_can: phss_empinfo_can });
     console.log(phss_empinfo_can);*/

     console.log(this.props.phss_empinfo_can);
     let phss_empinfo_can = this.props.phss_empinfo_can;
     this.setState({ role_phss_empinfo_can: this.props.phss_empinfo_can });
    /* Role Management */
    console.log(phss_empinfo_can);
    
    console.log("DidMount PhssEmploymentInformation");
    console.log(this.state.all_data);
    console.log(this.state.all_data.photoRequired);
    // Extended Profile -> PHSS Employment Information mode

    this.DisplayFuncation(this.state.all_data);

    this.GetUserRegistrationDetail();

    this.GetCRMUserList();
    //this.GetProfile();
    
    
    
    console.log(this.state.locationList);
    console.log(this.state.all_data.locationList);
    console.log('===============');

    
  }

  // multi select Role
  onSelectRole(selectedList, selectedItem) {
    let lang =[];
    for (var i = 0; i<selectedList.length; i++) {
      var number1 = selectedList[i].id;
      var number =  number1.toString();
      lang.push(number);
    }
    this.setState({ AdduserRole: lang });

    delete this.state.errormsg['AdduserRole'];
  }

  onRemoveRole(selectedList, removedItem) {
      let lang =[];
      for (var i = 0; i<selectedList.length; i++) {
        var number1 = selectedList[i].id;
        var number =  number1.toString();
        lang.push(number);
      }
      if(lang.length != 0){
        this.setState({ AdduserRole: lang });
      }

      delete this.state.errormsg['AdduserRole'];
  }
  // multi select Role

  push_func(){
    this.props.history.push("/");
    this.hideLoader();
  }

  SessionOut(){
    this.showLoader();
  
    localStorage.removeItem("token");
    localStorage.removeItem("contactId");
    localStorage.removeItem("eMailAddress");
    localStorage.removeItem("fullName");

    //this.ToastError('Session Timeout');
    
    setTimeout( () => {this.push_func()}, 5000);
    
    return null;
  }

  selectedRole(RoleList)
  {
    
    var roledisplay = this.state.all_data.userRoleDisplay;
    let roledisplay_array = roledisplay.split(',');
    
    console.log('select role');
    console.log(roledisplay_array);

    let lang =[];
    let selectlang =[];
    for (var i = 0; i<RoleList.length; i++) 
    {
      for (var j = 0; j<roledisplay_array.length; j++) 
      {
        if(RoleList[i].name == roledisplay_array[j])
        {
          lang.push(RoleList[i]);

          // default select time set "AdduserRole" state
          var number1 = RoleList[i].id;
          var number =  number1.toString();
          selectlang.push(number);
          // default select time set "AdduserRole" state
        }
      }
    }

    this.setState({ AdduserRole: selectlang });

    console.log(lang);
    return lang;
  }


  location_list_fun(location_list){
    let lang =[];
    
    for (var i = 0; i<location_list.length; i++) {
      let temp ={};
     // console.log(location_list[i].locationName);
       temp['id']=location_list[i].locationId;
       temp['name']=location_list[i].locationName;
       lang.push(temp);
    }
    if(lang.length != 0){
      console.log(lang);
      return lang;
      //this.setState({ locationList: lang });
    }
  }

  GetUserRegistrationDetail(){
    this.showLoader();

    var getrole = SystemHelpers.GetRole();
    let hierarchyId = getrole.hierarchy_can.hierarchy_can_id;

    var url=process.env.API_API_URL+'GetUserRegistrationDetail?rolePriorityId='+hierarchyId+'&contactId='+localStorage.getItem("contactId");
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
      //body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
        console.log("responseJson GetUserRegistrationDetail");
        console.log(data);
        //console.log(data.data.userRole);
        // debugger;
        if (data.responseType === "1") {
            // Profile & Contact
            if(data.data != null){
              //this.setState({ ListGrid: data.data.userEmergencyContactViews });
              this.setState({ userTypeList: data.data.userTypeList });
              this.setState({ roleList: data.data.roleList });
              this.setState({ selectedValueRole: this.selectedRole(data.data.roleList) });
              
              //var location_list=data.data.locationList;
              //this.setState({ locationList: this.location_list_fun(location_list) }); 
              this.setState({ empHistoryJobTitleList: data.data.jobTitle });
              this.setState({ employeeTypeList: data.data.employementType });
              this.setState({ employeementHourList: data.data.employementHours });
            }
            
            
        }else{
              if(data.message == 'Authorization has been denied for this request.'){
                SystemHelpers.SessionOut();
                this.props.history.push("/login");
              }else{
                SystemHelpers.ToastError(data.message);
              }
              
        }
        this.hideLoader();
        
    })
    .catch(error => {
      this.props.history.push("/error-500");
    });
  }

  DisplayFuncation(all_data){

    console.log('DisplayFuncation phhsemp');
    console.log(all_data);

    this.setState({ Staff_Number_Model: all_data.staffNumberId });
    this.setState({ AddjobTitle: all_data.jobTittle }); 
    this.setState({ Primary_Location_Model: all_data.primaryLocation });

    if(all_data.hireDate != "" && all_data.hireDate != null)
    {
      //this.setState({ AddhireDate: moment(all_data.hireDate).format('YYYY-MM-DD') });
      this.setState({ AddhireDate: moment(all_data.hireDate,process.env.API_DATE_FORMAT) });
    }
    else
    {
      this.setState({ AddhireDate: '' });
    }

    this.setState({ AddemployeeType: all_data.employmentType });
    this.setState({ Employment_Status_Model: all_data.employmentStatus });
    this.setState({ AddemployeeHours: all_data.employmentHours });

    

    this.setState({ AddReportTo: all_data.timeSheetReportId }); 
    this.setState({ AddApproverTo: all_data.timeSheetApproverId }); 
    
    this.setState({ empDeterminationList: all_data.employeementStatusList });
    this.setState({ locationList: all_data.locationList }); 
    this.setState({ timeSheetReportName: all_data.reportToUserName }); 
    this.setState({ timeSheetApproverName: all_data.timeSheetApproverName }); 
    
    
    this.setState({ selectedValueRole: this.selectedRole(all_data.roleList) });

    if(all_data.photoRequired == true){
      //AddPhotoShow
      $('#AddPhotoShow').prop('checked', true);
    }else{
      $('#AddPhotoShow').prop('checked', false);
    }

    
    if(all_data.employmentStatus == "Inactive" || all_data.employmentStatus == "On Hold")
    {
      $('#EmpStateDateOfLeavingDivId').show();
      if(all_data.dateOfLeaving != "" && all_data.dateOfLeaving != null){
        this.setState({ AddDateOfLeaving: moment(all_data.dateOfLeaving,process.env.API_DATE_FORMAT) });
      }else{
        this.setState({ AddDateOfLeaving: "" });
      }
      
    }
    else
    {
      this.setState({ AddDateOfLeaving : "" });
      $('#EmpStateDateOfLeavingDivId').hide();
    }
    

    //display
    this.setState({ staffNumberIdDisplay: all_data.staffNumberId }); 
    this.setState({ jobTitleNameDisplay: all_data.jobTitleName });
    this.setState({ primaryLocationDisplay: all_data.primaryLocation });

    if(all_data.hireDate != "" && all_data.hireDate != null){
      this.setState({ hireDateDisplay: all_data.hireDate });
    }else{
      this.setState({ hireDateDisplay: '' });
    }

    this.setState({ employmentTypeDisplay: all_data.employmentType });
    this.setState({ employmentStatusDisplay: all_data.employmentStatus });
    this.setState({ employmentHoursDisplay: all_data.employmentHours });

    if(all_data.photoRequired == true){
      this.setState({ photoRequiredDisplay: "Yes" });
    }else if(all_data.photoRequired == false){
      this.setState({ photoRequiredDisplay: "No" });
    }

    this.setState({ reportToUserNameDisplay : all_data.reportToUserName });

    this.setState({ timeSheetApproverNameDisplay: all_data.timeSheetApproverName });
    this.setState({ userRoleDisplayDisplay: all_data.userRoleDisplay });

    //this.props.setPropState('isProfileImageShow', all_data.photoRequired);
    localStorage.setItem('headerisProfileImageShow', all_data.photoRequired);
    
    this.props.setPropState('TimeSheetApproverName', all_data.timeSheetApproverName);
    
    localStorage.setItem("employmentHoursName", all_data.employmentHours);
    localStorage.setItem("employmentStatusName", all_data.employmentStatus);

    if(all_data.employmentStatus == "Inactive" || all_data.employmentStatus == "On Hold")
    {
      if(all_data.dateOfLeaving != "" && all_data.dateOfLeaving != null){
        this.setState({ dateofleavingDisplay: all_data.dateOfLeaving });
      }else{
        this.setState({ dateofleavingDisplay: "" });
      }
    }
    else
    {
      this.setState({ dateofleavingDisplay: "" });
    }
    //display
  }

  //
  GetProfile(){
    console.log(localStorage.getItem("token"));
    this.showLoader();
    var url=process.env.API_API_URL+'GetUserBasicInfoById?contactId='+this.state.staffContactID;
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
      //body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
        console.log("responseJson GetUserBasicInfoById phhsemp");
        console.log(data);
        //console.log(data.data.userRole);
        // debugger;
        
         
        if (data.responseType === "1") {


            //this.setState({ all_data: data.data});

            this.DisplayFuncation(data.data);

            /*if(data.data.photoRequired == true){
              this.setState({ photoRequiredDisplay: "Yes" });
            }else if(data.data.photoRequired == false){
              this.setState({ photoRequiredDisplay: "No" });
            }
            
            this.setState({ reportToUserNameDisplay : data.data.reportToUserName });*/

            localStorage.setItem("primaryLocationId", data.data.primaryLocationId);
            localStorage.setItem("isPayrollAdmin", data.data.isPayrollAdmin);
            localStorage.setItem("primaryLocationName", data.data.primaryLocation);
            this.props.setPropState('primaryLocationName', data.data.primaryLocation);
            
            this.setState({ Primary_Location_Model: data.data.primaryLocation });
        }else{
              if(data.message == 'Authorization has been denied for this request.'){
                this.SessionOut();
              }else{
                this.ToastError(data.message);
              }
              
        }
        this.hideLoader();
        
    })
    .catch(error => {
      this.props.history.push("/error-500");
    });
  }
  //

  // PHSS Employment Information model Update api
  UpdateUserExtendedTabPHSS_EmpInfoModel_API = () => e => {  
      e.preventDefault();  
      //debugger;
      //alert($('#Extended_Marital_Status_Model').val());
      //alert(this.state["AdduserRole"]);
      let step1Errors = {};
      // if (this.state["Staff_Number_Model"] === '') {
      //   step1Errors["Staff_Number_Model"] = "Please Enter Staff Number/ID"
      // }
      
      if (this.state["Primary_Location_Model"] === '' || this.state["Primary_Location_Model"] === null) {
        step1Errors["Primary_Location_Model"] = "Please Enter Primary Location"
      }
      if (this.state["Employment_Status_Model"] === '' ) {
        step1Errors["Employment_Status_Model"] = "Please Select Employment Status"
      }
      // if (this.state["AddReportTo"] === '' ) {
      //   step1Errors["AddReportTo"] = "Please Select Report to"
      // }
      // if (this.state["AddApproverTo"] === '' ) {
      //   step1Errors["AddApproverTo"] = "Please Select Timesheet Approver"
      // }
      
      if (this.state["AddjobTitle"] =='') {
        step1Errors["AddjobTitle"] = "Job Title is mandatory";
      }
      if (this.state["AddhireDate"] =='') {
        step1Errors["AddhireDate"] = "Hire date is mandatory";
      }
      if (this.state["AddemployeeType"] == '') {
        step1Errors["AddemployeeType"] = "Employment Type is mandatory";
      }
      if (this.state["AddemployeeHours"]  == '') {
        step1Errors["AddemployeeHours"] = "Employment Hours is mandatory";
      }
      if (this.state["AdduserRole"] == "" || this.state["AdduserRole"] == null) {
        step1Errors["AdduserRole"] = "Role is mandatory";
      }

      if ($('#AddPhotoShow').is(":checked"))
      {
        var AddPhotoShow = true;
      }
      else
      {
        var AddPhotoShow = false;
      }

      if(this.state["Employment_Status_Model"]== "Inactive" || this.state["Employment_Status_Model"]== "On Hold")
      {
        if (this.state["AddDateOfLeaving"] == '') {
          step1Errors["AddDateOfLeaving"] = "Date of leaving is mandatory";
        }
      }

      this.setState({ errormsg: step1Errors });
        
      if (Object.keys(step1Errors).length > 0) {
          return false;
      }

      this.showLoader();
      
      var hire_Date ='';
      if (this.state["AddhireDate"] != '') {
        var hire_Date=moment(this.state["AddhireDate"]).format('MM-DD-YYYY');
        //var NewAddhireDate = moment(AddhireDate, "MM-DD-YYYY").add(1, 'days');
        //var hire_Date=moment(NewAddhireDate).format('MM-DD-YYYY');
      }

      var Leaving_Date ='';
      if(this.state["Employment_Status_Model"]== "Inactive" || this.state["Employment_Status_Model"]== "On Hold")
      {
        //AddDateOfLeaving
        if (this.state["AddDateOfLeaving"] != '') {
          var Leaving_Date=moment(this.state["AddDateOfLeaving"]).format('MM-DD-YYYY');
        }
      }

      /* *************************************************************************** */
        if(this.state["Primary_Location_Model"] != '' && this.state["Primary_Location_Model"] != null){
          var Primary_Location_Model = this.state["Primary_Location_Model"].trim();  
        }else{
          var Primary_Location_Model = this.state["Primary_Location_Model"];
        }

        if(this.state["Employment_Status_Model"] != '' && this.state["Employment_Status_Model"] != null){
          var Employment_Status_Model = this.state["Employment_Status_Model"].trim();  
        }else{
          var Employment_Status_Model = this.state["Employment_Status_Model"];
        }

        if(this.state["AddReportTo"] != '' && this.state["AddReportTo"] != null){
          var AddReportTo = this.state["AddReportTo"].trim();  
        }else{
          var AddReportTo = this.state["AddReportTo"];
        }

        if(this.state["AddApproverTo"] != '' && this.state["AddApproverTo"] != null){
          var AddApproverTo = this.state["AddApproverTo"].trim();  
        }else{
          var AddApproverTo = this.state["AddApproverTo"];
        }

        if(this.state["AddjobTitle"] != '' && this.state["AddjobTitle"] != null){
          var AddjobTitle = this.state["AddjobTitle"].trim();  
        }else{
          var AddjobTitle = this.state["AddjobTitle"];
        }

        if(this.state["AddemployeeType"] != '' && this.state["AddemployeeType"] != null){
          var AddemployeeType = this.state["AddemployeeType"].trim();  
        }else{
          var AddemployeeType = this.state["AddemployeeType"];
        }

        if(this.state["AddemployeeHours"] != '' && this.state["AddemployeeHours"] != null){
          var AddemployeeHours = this.state["AddemployeeHours"].trim();  
        }else{
          var AddemployeeHours = this.state["AddemployeeHours"];
        }

        if(AddPhotoShow != '' && AddPhotoShow != null){
          var AddPhotoShow = AddPhotoShow.trim();  
        }else{
          var AddPhotoShow = AddPhotoShow;
        }

        var AdduserRole = this.state["AdduserRole"];

        if(Leaving_Date != '' && Leaving_Date != null){
          var Leaving_Date = Leaving_Date.trim();  
        }else{
          var Leaving_Date = Leaving_Date;
        }

      /* *************************************************************************** */
      

      let User_extended_profile_PHHS_EmpInfo = {
        //staffNumberId: this.state["Staff_Number_Model"],
        //primaryLocation: this.state["Primary_Location_Model"],
        primarylocationid: Primary_Location_Model,
        employmentStatus: Employment_Status_Model,
        timeSheetReportId: AddReportTo,
        timeSheetApproverId: AddApproverTo,
        
        jobTittle: AddjobTitle,
        hireDate: hire_Date,
        employmentType: AddemployeeType,
        employmentHours: AddemployeeHours,
        photoRequired: AddPhotoShow,
        roleId: AdduserRole,
        dateOfLeaving:Leaving_Date,
        
      };

      console.log(User_extended_profile_PHHS_EmpInfo);
      let bodyarray = {};
      bodyarray["contactId"] = this.state.staffContactID;
      bodyarray["userBasicInfo"] = User_extended_profile_PHHS_EmpInfo;
      bodyarray["userName"] = this.state.staffContactFullname;
      
      var url=process.env.API_API_URL+'UpdateUserEmpInfo';
      fetch(url, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'bearer '+localStorage.getItem("token")
        },
        body: JSON.stringify(bodyarray)
      }).then((response) => response.json())
      .then(data => {
        //console.log("responseJson UpdateUserBasicProfile");
        //console.log(data);
        //console.log(responseJson);
        // debugger;

        if (data.responseType === "1") {
          //this.ToastSuccess(data.responseMessge);  
          setTimeout(
              function() {
                  this.GetProfile();
              }
              .bind(this),
              10000
          );
          //this.GetProfile();  
          this.ToastSuccess(data.responseMessge);
          $( "#close_btn_PHSSEmpInfo" ).trigger( "click" );
          
        }
        else{
          this.ToastError(data.responseMessge);
        }
        this.hideLoader();
      })
      .catch(error => {
        this.props.history.push("/error-500");
      });
  }
  // PHSS Employment Information model Update api

  ClearRecord = ()=> e => {
    console.log('ClearRecord phss emp');
    console.log(this.state.all_data);
    e.preventDefault();

    this.setState({ errormsg: '' });

    // Extended Profile -> PHSS Employment Information mode

    //this.GetUserRegistrationDetail();

    this.setState({ Staff_Number_Model: this.state.all_data.staffNumberId });
    this.setState({ Primary_Location_Model: this.state.all_data.primaryLocation });
    this.setState({ Employment_Status_Model: this.state.all_data.employmentStatus });
    // Extended Profile -> PHSS Employment Information mode
    this.setState({ empDeterminationList: this.state.all_data.employeementStatusList });
    this.setState({ locationList: this.state.all_data.locationList }); 
    this.setState({ timeSheetReportName: this.state.all_data.reportToUserName }); 
    this.setState({ timeSheetApproverName: this.state.all_data.timeSheetApproverName });

    this.setState({ AddReportTo: this.state.all_data.timeSheetReportId }); 
    this.setState({ AddApproverTo: this.state.all_data.timeSheetApproverId }); 

    this.setState({ AddjobTitle: this.state.all_data.jobTittle }); 

    if(this.state.all_data.hireDate != "" && this.state.all_data.hireDate != null)
    {
      this.setState({ AddhireDate: moment(this.state.all_data.hireDate,process.env.API_DATE_FORMAT) });
    }
    else
    {
      this.setState({ AddhireDate: '' });
    }

    this.setState({ AddemployeeType: this.state.all_data.employmentType });
    this.setState({ AddemployeeHours: this.state.all_data.employmentHours }); 
    //this.setState({ AdduserRole: this.state.all_data.roleList });

    if(this.state.all_data.employmentStatus == "Inactive" || this.state.all_data.employmentStatus == "On Hold")
    {
      $('#EmpStateDateOfLeavingDivId').show();
      this.setState({ AddDateOfLeaving: moment(this.state.all_data.dateOfLeaving,process.env.API_DATE_FORMAT) });
    }
    else
    {
      this.setState({ AddDateOfLeaving : "" });
      $('#EmpStateDateOfLeavingDivId').hide();
    }

    //this.resetValues();
    //this.GetProfile();
  }

  resetValues() {
    // By calling the belowe method will reset the selected values programatically
    //this.userTypeListRef.current.resetSelectedValues();
    this.roleListRef.current.resetSelectedValues();
    //this.locationListRef.current.resetSelectedValues();
  }

  GetCRMUserList(){
      //this.showLoader();
      var url=process.env.API_API_URL+'GetCRMUserList';
      fetch(url, {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'bearer '+localStorage.getItem("token")
        },
      }).then((response) => response.json())
      .then(data => {
          console.log("responseJson GetCRMUserList");
          console.log(data);
          if (data.responseType === "1") {
              this.setState({ GetCRMUserList: data.data });
          }else{
            if(data.message == 'Authorization has been denied for this request.'){
              SystemHelpers.SessionOut();
              this.props.history.push("/login");
            }else{
              SystemHelpers.ToastError(data.message);
            }
                
          }
          //this.hideLoader();
          
      })
      .catch(error => {
        this.props.history.push("/error-500");
      });
  }
   
  render() {
     
      return (
        
          <div className="row">
            {(this.state.loading) ? <Loader /> : null}
            <div className="col-md-12 d-flex">
              <div className="card profile-box flex-fill">
                <div className="card-body">
                  {this.state.role_phss_empinfo_can.phss_empinfo_can_update == true ? 
                    <h3 className="card-title">PHSS Employment Information<a href="#" className="edit-icon" data-toggle="modal" data-target="#ExtendedProfileTab_phss_employment_info_modal"><i className="fa fa-pencil" /></a></h3>
                    : <h3 className="card-title">PHSS Employment Information <a href="#" className="phss-lock"><i className="fa fa-lock" /></a></h3>
                  }
                  <ul className="personal-info">
                    <li>
                      <div className="title">Staff Number/ID</div>
                      {this.state.staffNumberIdDisplay == '' || this.state.staffNumberIdDisplay == null? <div className="text hide-font">None</div> : <div className="text">{this.state.staffNumberIdDisplay}</div> }
                    </li>
                    <li>
                      <div className="title">Job Title</div>
                      {this.state.jobTitleNameDisplay == '' || this.state.jobTitleNameDisplay == null? <div className="text hide-font">None</div> : <div className="text">{this.state.jobTitleNameDisplay}</div> }
                    </li>
                    <li>
                      <div className="title">Primary Location</div>
                      {this.state.primaryLocationDisplay == '' || this.state.primaryLocationDisplay == null? <div className="text hide-font">None</div> : <div className="text">{this.state.primaryLocationDisplay}</div> }
                    </li>
                    <li>
                      <div className="title">Hire date</div>
                      {this.state.hireDateDisplay == '' || this.state.hireDateDisplay == null? <div className="text hide-font">None</div> : <div className="text">{moment(this.state.hireDateDisplay,process.env.API_DATE_FORMAT).format(process.env.DATE_FORMAT)}</div> }
                    </li>
                    <li>
                      <div className="title">Employment type</div>
                      {this.state.employmentTypeDisplay == '' || this.state.employmentTypeDisplay == null? <div className="text hide-font">None</div> : <div className="text">{this.state.employmentTypeDisplay}</div> }
                    </li>
                    <li>
                      <div className="title">Employment Status</div>
                      {this.state.employmentStatusDisplay == '' || this.state.employmentStatusDisplay == null? <div className="text hide-font">None</div> : <div className="text">{this.state.employmentStatusDisplay}</div> }
                    </li>

                    <li>
                      <div className="title">Date of leaving</div>
                      {this.state.dateofleavingDisplay == '' || this.state.dateofleavingDisplay == null? <div className="text hide-font">None</div> : <div className="text">{moment(this.state.dateofleavingDisplay,process.env.API_DATE_FORMAT).format(process.env.DATE_FORMAT)}</div> }
                    </li>
                    
                    <li>
                      <div className="title">Employment hours</div>
                      {this.state.employmentHoursDisplay == '' || this.state.employmentHoursDisplay == null? <div className="text hide-font">None</div> : <div className="text">{this.state.employmentHoursDisplay}</div> }
                    </li>
                    <li>
                      <div className="title">Photo authorization</div>
                      {this.state.photoRequiredDisplay == '' || this.state.photoRequiredDisplay == null? <div className="text hide-font">None123</div> : <div className="text">{this.state.photoRequiredDisplay}</div> }
                    </li>
                    <li>
                      <div className="title">Report to</div>
                      {this.state.reportToUserNameDisplay == '' || this.state.reportToUserNameDisplay == null? <div className="text hide-font">None</div> : <div className="text">{this.state.reportToUserNameDisplay}</div> }
                    </li>
                    <li>
                      <div className="title">Timesheet Approver</div>
                      {this.state.timeSheetApproverNameDisplay == '' || this.state.timeSheetApproverNameDisplay == null? <div className="text hide-font">None</div> : <div className="text">{this.state.timeSheetApproverNameDisplay}</div> }
                    </li>
                    <li>
                      <div className="title">Role</div>
                      {this.state.userRoleDisplayDisplay == '' || this.state.userRoleDisplayDisplay == null? <div className="text hide-font">None</div> : <div className="text">{this.state.userRoleDisplayDisplay}</div> }
                    </li>
                  </ul>
                </div>
              </div>
            </div>
             {/* PHSS Employment Information Modal */}
            <div id="ExtendedProfileTab_phss_employment_info_modal" className="modal custom-modal fade" role="dialog">
              <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div className="modal-content">
                  <div className="modal-header">
                    <h5 className="modal-title">PHSS Employment Information</h5>
                    <button type="button" className="close" id="close_btn_PHSSEmpInfo" data-dismiss="modal" aria-label="Close" onClick={this.ClearRecord()} >
                      <span aria-hidden="true">×</span>
                    </button>
                  </div>
                  <div className="modal-body">
                    <form>
                      <div className="card">
                        <div className="card-body">
                          <div className="row">
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Staff Number/ID<span className="text-danger">*</span></label>
                                <input type="text" className="form-control" value={this.state.Staff_Number_Model} disabled />
                                 
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Job Title<span className="text-danger">*</span></label>
                                <select className="form-control" value={this.state.AddjobTitle} onChange={this.handleChange('AddjobTitle')}>
                                  <option value="">-</option>
                                  {this.state.empHistoryJobTitleList.map(( listValue, index ) => {
                                    return (
                                      <option key={index} value={listValue.jobTitleId}>{listValue.jobTitleName}</option>
                                    );
                                  })}
                                </select>
                                <span className="form-text error-font-color">{this.state.errormsg["AddjobTitle"]}</span>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Primary Location<span className="text-danger">*</span></label>
                                <select className="form-control" value={this.state.Primary_Location_Model} onChange={this.handleChange('Primary_Location_Model')}>
                                  <option value="">-</option>
                                  {this.state.locationList.map(( listValue, index ) => {
                                    return (
                                      <option key={index} value={listValue.locationName} >{listValue.locationName}</option>
                                    );
                                  })}
                                </select>
                                <span className="form-text error-font-color">{this.state.errormsg["Primary_Location_Model"]}</span> 
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Hire date<span className="text-danger">*</span></label>
                                <Datetime
                                  inputProps={{readOnly: true}}
                                  closeOnTab={true}
                                  input={true}
                                  value={(this.state.AddhireDate) ? this.state.AddhireDate : ''}
                                  onChange={this.handleEditAddhireDate}
                                  dateFormat={process.env.DATE_FORMAT}
                                  timeFormat={false}
                                  isValidDate={this.validationDateHiredate}
                                  renderInput={(props) => {
                                     return <input {...props} value={(this.state.AddhireDate) ? props.value : ''} />
                                  }}
                                />
                                {/*<input className="form-control" type="date" value={this.state.AddhireDate} onChange={this.handleChange('AddhireDate')}/>*/}
                                <span className="form-text error-font-color">{this.state.errormsg["AddhireDate"]}</span> 
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Employment Type<span className="text-danger">*</span></label>
                                <select className="form-control" value={this.state.AddemployeeType} onChange={this.handleChange('AddemployeeType')}>
                                  <option value="">-</option>
                                  {this.state.employeeTypeList.map(( listValue, index ) => {
                                    return (
                                      <option key={index} value={listValue.name}>{listValue.name}</option>
                                    );
                                  })}
                                </select>
                                <span className="form-text error-font-color">{this.state.errormsg["AddemployeeType"]}</span>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Employment Status<span className="text-danger">*</span></label>
                                <select className="form-control" value={this.state.Employment_Status_Model} onChange={this.handleChange('Employment_Status_Model')}>
                                  <option value="">-</option>
                                  {this.state.empDeterminationList.map(( listValue, index ) => {
                                    return (
                                      <option key={index} value={listValue.name}>{listValue.name}</option>
                                    );
                                  })}
                                </select>
                                <span className="form-text error-font-color">{this.state.errormsg["Employment_Status_Model"]}</span> 
                              </div>
                            </div>
                            

                              <div className="col-md-12" id="EmpStateDateOfLeavingDivId">
                                <div className="form-group">
                                  <label>Date of leaving<span className="text-danger">*</span></label>
                                  <Datetime
                                    inputProps={{readOnly: true}}
                                    closeOnTab={true}
                                    input={true}
                                    value={(this.state.AddDateOfLeaving) ? this.state.AddDateOfLeaving : ''}
                                    onChange={this.handleEditAddDateOfLeaving}
                                    dateFormat={process.env.DATE_FORMAT}
                                    timeFormat={false}
                                    renderInput={(props) => {
                                       return <input {...props} value={(this.state.AddDateOfLeaving) ? props.value : ''} />
                                    }}
                                  />
                                  <span className="form-text error-font-color">{this.state.errormsg["AddDateOfLeaving"]}</span> 
                                </div>
                              </div>
                            
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Employment Hours<span className="text-danger">*</span></label>
                                <select className="form-control" value={this.state.AddemployeeHours} onChange={this.handleChange('AddemployeeHours')}>
                                  <option value=''>-</option>
                                  {this.state.employeementHourList.map(( listValue, index ) => {
                                    return (
                                      <option key={index} value={listValue.name}>{listValue.name}</option>
                                    );
                                  })}
                                </select>
                                <span className="form-text error-font-color">{this.state.errormsg["AddemployeeHours"]}</span>
                              </div>
                            </div>

                            <div className="col-md-6" id="AddReportToDiv">
                              <div className="form-group">
                                <label>Report to</label>
                                <select className="form-control" id="AddReportTo" value={this.state.AddReportTo}  onChange={this.handleChange('AddReportTo')} >
                                  <option value=''>-</option>
                                  {this.state.GetCRMUserList.map(( listValue, index ) => {
                                    if(listValue.userTypeDisplay == "Staff"){  
                                      return (
                                        <option key={index} value={listValue.contactId}>{listValue.firstName} {listValue.lastName}</option>
                                      );
                                    }
                                  })}
                                </select>
                                <span className="form-text error-font-color">{this.state.errormsg["AddReportTo"]}</span>
                              </div>
                            </div>

                             <div className="col-md-6" id="AddApproverToDiv">
                              <div className="form-group">
                                <label>Timesheet Approver</label>
                                <select className="form-control" id="AddApproverTo" value={this.state.AddApproverTo}  onChange={this.handleChange('AddApproverTo')} >
                                  <option value=''>-</option>
                                  {this.state.GetCRMUserList.map(( listValue, index ) => {
                                    if(listValue.isTimesheeetApprover == true){
                                      return (
                                        <option key={index} value={listValue.contactId}>{listValue.firstName} {listValue.lastName}</option>
                                      );
                                    }
                                    
                                  })}
                                </select>
                                <span className="form-text error-font-color">{this.state.errormsg["AddApproverTo"]}</span>
                              </div>
                            </div>

                            <div className="form-group col-sm-6">
                              <label>Role <span className="text-danger">*</span></label>
                              <Multiselect
                                options={this.state.roleList} // Options to display in the dropdown
                                selectedValues={this.state.selectedValueRole} // Preselected value to persist in dropdown
                                onSelect={this.onSelectRole} // Function will trigger on select event
                                onRemove={this.onRemoveRole} // Function will trigger on remove event
                                displayValue="name" // Property name to display in the dropdown options
                                ref={this.roleListRef}
                              />
                              <span className="form-text error-font-color">{this.state.errormsg["AdduserRole"]}</span>
                            </div>

                            <div className="col-md-6">
                              <div className="form-group">
                                <label></label>
                                  <div class="checkbox">
                                    <label>Photo Authorization<input type="checkbox" className="skill_checkbox" name="AddPhotoShow" id="AddPhotoShow" /></label>
                                  </div>
                                 
                              </div>
                            </div>

                          </div>
                          <div className="submit-section">
                            <button className="btn btn-primary submit-btn" type="submit" onClick={this.UpdateUserExtendedTabPHSS_EmpInfoModel_API({})} >Submit</button>
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
            {/* /PHSS Employment Information Modal */}
          </div>
        
      );
   }
}

export default PhssEmploymentInformation;
