/**
 * Signin Firebase
 */

import React, { Component } from 'react';
import { Helmet } from "react-helmet";
import InputMask from 'react-input-mask';

import Loader from '../../../Loader';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
//import {Avatar_02,Avatar_05,Avatar_09,Avatar_10,Avatar_16 } from '../../../../../Entryfile/imagepath'
import PhoneInput from 'react-phone-input-2';
import 'react-phone-input-2/lib/style.css';

import CryptoAES from 'crypto-js/aes';
import CryptoENC from 'crypto-js/enc-utf8';

import SystemHelpers from '../../../Helpers/SystemHelper';

class ConatctView extends Component {
  constructor(props) {
    super(props);

    this.state = {
        errormsg :  '',
        user_role: [],
        all_data :this.props.all_data,
        staffContactID:this.props.staffContactID,
        Preferred_Method_Model :  '',
        PHHS_Email_Model :  '',
        Personal_Email_Model :  '',
        Cell_Phone_Model :  '',
        Home_Phone_Model :  '',
        CellPhoneNumberCountryCode : '',
        HomePhoneNumberCountryCode : '',
        role_contact_information_can :{},

        cellPhoneDisplay : '',
        homePhoneDisplay : '',
        staffContactFullname : localStorage.getItem('fullName')
    };
    this.setPropState = this.setPropState.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange = input => e => {
    this.setState({ [input]: e.target.value });
  }

  setPropState(key, value) {
      this.setState({ [key]: value });
  }

   // Loader hide show method
  hideLoader = () => {
    this.setState({ loading: false });
  }

  showLoader = () => {
    this.setState({ loading: true });
  }
  // Loader hide show method

  push_func(){
    this.props.history.push("/");
    this.hideLoader();
  }

  SessionOut(){
    this.showLoader();
  
    localStorage.removeItem("token");
    localStorage.removeItem("contactId");
    localStorage.removeItem("eMailAddress");
    localStorage.removeItem("fullName");

    //this.ToastError('Session Timeout');
    
    setTimeout( () => {this.push_func()}, 5000);
    
    return null;
  }
  // toast hide show method
  ToastSuccess(msg){
      toast.success(msg, {
        position: "top-right",
        autoClose: process.env.API_TOAST_TIME,
        hideProgressBar: false,
        closeOnClick: false,
        pauseOnHover: false,
        draggable: false
      });
  }

  ToastError(msg){
      toast.error(msg, {
        position: "top-right",
        autoClose: process.env.API_TOAST_TIME,
        hideProgressBar: false,
        closeOnClick: false,
        pauseOnHover: false,
        draggable: false
      });
  }
  // toast hide show method

  componentDidMount() {
    console.log("ConatctView");

    /* Role Management */
     console.log('Role Store contact_information_can');
     /*var getrole = SystemHelpers.GetRole();
     let contact_information_can = getrole.contact_information_can;
     this.setState({ role_contact_information_can: contact_information_can });
     console.log(contact_information_can);*/
     
     console.log(this.props.contact_information_can);
     this.setState({ role_contact_information_can: this.props.contact_information_can });
    /* Role Management */

    console.log(this.state.all_data);

    this.setState({ Preferred_Method_Model: this.state.all_data.preferredMethod });
    $('#Preferred_Method_Model').val(this.state.all_data.preferredMethod).trigger('change');
    this.setState({ PHHS_Email_Model: this.state.all_data.phssEmail });
    this.setState({ Personal_Email_Model: this.state.all_data.personalEmail });
    
    if(this.state.all_data.cellPhone != '' && this.state.all_data.cellPhone != null){
      this.setState({ Cell_Phone_Model: this.state.all_data.cellPhone });
    }
    
    if(this.state.all_data.homePhone != '' && this.state.all_data.homePhone != null){
      this.setState({ Home_Phone_Model: this.state.all_data.homePhone });
    }
    var CellCountryCode = this.state.all_data.cellPhone;
    
    if(CellCountryCode != null && CellCountryCode != ''){
      var CountryCodeCell = CellCountryCode.substring(0, CellCountryCode.length-10)
      this.setState({ CellPhoneNumberCountryCode: CountryCodeCell });
    }
    

    // Display
    
    var CellCountryCodeDisplay = this.state.all_data.cellPhone;
    //alert(CellCountryCodeDisplay);
    if(CellCountryCodeDisplay != null && CellCountryCodeDisplay != ''){
      var CountryCodeCellDisplay = CellCountryCodeDisplay.substring(0, CellCountryCodeDisplay.length-10)
      //alert(CountryCodeCellDisplay);

      var ret = CellCountryCodeDisplay.replace(CountryCodeCellDisplay,'');
      //alert(ret);
      var format_CellePhoneDisplay=CountryCodeCellDisplay+" "+ ret.replace(/(\d{3})(\d{3})(\d{4})/, "$1-$2-$3");
      this.setState({ cellPhoneDisplay: format_CellePhoneDisplay });
    }

    var HomeCountryCodeDisplay = this.state.all_data.homePhone;
    //alert(HomeCountryCodeDisplay);
    if(HomeCountryCodeDisplay != null && HomeCountryCodeDisplay != ''){
      var CountryCodeHomeDisplay = HomeCountryCodeDisplay.substring(0, HomeCountryCodeDisplay.length-10)
      //alert(CountryCodeHomeDisplay);

      var ret = HomeCountryCodeDisplay.replace(CountryCodeHomeDisplay,'');
      //alert(ret);
      var format_HomePhoneDisplay=CountryCodeHomeDisplay+" "+ ret.replace(/(\d{3})(\d{3})(\d{4})/, "$1-$2-$3");
      this.setState({ homePhoneDisplay: format_HomePhoneDisplay });
    }

    
    // Display

  }

  //
  handleOnChangeCellMobile(value, data, event, formattedValue)
  {
    var phone = value;
    var str2 = "+";
    var country_code=data.dialCode;
    if(phone.indexOf(str2) != -1){
        console.log(str2 + " found");
    }else{
        value='+'+value;
        country_code="+"+data.dialCode;
    }
    console.log('Cell value ' + value);
    console.log('Cell formattedValue ' + formattedValue);
    console.log('Cell country_code ' + country_code);
    this.setState({ Cell_Phone_Model: value});
    this.setState({ CellPhoneNumberCountryCode : country_code });   
  }
  handleOnChangeHomeMobile(value, data, event, formattedValue)
  {
    var phone = value;
    var str2 = "+";
    var country_code=data.dialCode;
    if(phone.indexOf(str2) != -1){
        console.log(str2 + " found");
    }else{
        value='+'+value;
        country_code="+"+data.dialCode;
    }
    console.log('Home value ' + value);
    console.log('Home formattedValue ' + formattedValue);
    console.log('Home country_code ' + country_code);
    this.setState({ Home_Phone_Model: value});
    this.setState({ HomePhoneNumberCountryCode : country_code });   
  }
  //

  //
  GetProfile(){
    console.log(localStorage.getItem("token"));
    this.showLoader();
    var url=process.env.API_API_URL+'GetUserBasicInfoById?contactId='+this.state.staffContactID;
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
      //body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
        console.log("responseJson GetUserBasicInfoById");
        console.log(data);
        //console.log(data.data.userRole);
        // debugger;
        
         
        if (data.responseType === "1") {
            localStorage.setItem("primaryLocationId", data.data.primaryLocationId);
            localStorage.setItem("isPayrollAdmin", data.data.isPayrollAdmin);
            
            this.setState({ all_data: data.data});

            // Display
    
          var CellCountryCodeDisplay = data.data.cellPhone;
          if(CellCountryCodeDisplay != null && CellCountryCodeDisplay != ''){
            var CountryCodeCellDisplay = CellCountryCodeDisplay.substring(0, CellCountryCodeDisplay.length-10)
            
            var ret = CellCountryCodeDisplay.replace(CountryCodeCellDisplay,'');
            var format_CellePhoneDisplay=CountryCodeCellDisplay+" "+ ret.replace(/(\d{3})(\d{3})(\d{4})/, "$1-$2-$3");
            this.setState({ cellPhoneDisplay: format_CellePhoneDisplay });
          }

          var HomeCountryCodeDisplay = data.data.homePhone;
          if(HomeCountryCodeDisplay != null && HomeCountryCodeDisplay != ''){
            var CountryCodeHomeDisplay = HomeCountryCodeDisplay.substring(0, HomeCountryCodeDisplay.length-10)
            
            var ret = HomeCountryCodeDisplay.replace(CountryCodeHomeDisplay,'');
            var format_HomePhoneDisplay=CountryCodeHomeDisplay+" "+ ret.replace(/(\d{3})(\d{3})(\d{4})/, "$1-$2-$3");
            this.setState({ homePhoneDisplay: format_HomePhoneDisplay });
          }

          
          // Display

        }else{
              if(data.message == 'Authorization has been denied for this request.'){
                this.SessionOut();
              }else{
                this.ToastError(data.message);
              }
              
        }
        this.hideLoader();
        
    })
    .catch(error => {
      this.props.history.push("/error-500");
    });
  }
  //

  // Contact model Update api
  UpdateUserProfileTabContactModel_API = () => e => {  
      e.preventDefault();  
      //debugger;
      //alert();
      let step1Errors = {};

      /*var Preferred_Method_Model = $("#Preferred_Method_Model").val();
      if (Preferred_Method_Model == '' || Preferred_Method_Model == null) {
        step1Errors["Preferred_Method_Model"] = "Preferred Method is mandatory"
      }*/

      /*if(this.state["Personal_Email_Model"] == '' || this.state["Personal_Email_Model"] == null)
      {
        step1Errors["Personal_Email_Model"] = "Personal Email is mandatory";
      }
      else if (this.state["Personal_Email_Model"] !== "undefined" || this.state["Personal_Email_Model"] != '' || this.state["Personal_Email_Model"] != null) {
        let lastAtPos = this.state["Personal_Email_Model"].lastIndexOf('@');
        let lastDotPos = this.state["Personal_Email_Model"].lastIndexOf('.');

        if (!(lastAtPos < lastDotPos && lastAtPos > 0 && this.state["Personal_Email_Model"].indexOf('@@') == -1 && lastDotPos > 2 && (this.state["Personal_Email_Model"].length - lastDotPos) > 2)) {
            step1Errors["Personal_Email_Model"] = "Email is not valid";
        }
      }*/
      
      var CellPhoneModel =this.state["Cell_Phone_Model"];
      var CellPhoneNumberCountryCode =this.state["CellPhoneNumberCountryCode"];
      if(this.state["Cell_Phone_Model"] == '' || this.state["Cell_Phone_Model"] == null || this.state["Cell_Phone_Model"] == '+'){
        step1Errors["Cell_Phone_Model"] = "Cell Phone is mandatory";
      }else{
        var total = CellPhoneModel.length - CellPhoneNumberCountryCode.length;
        if(total!=10)
        {
          step1Errors["Cell_Phone_Model"] = "Please Enter a Valid Cell Phone";
        }
      }
      
      
      var HomePhoneModel =this.state["Home_Phone_Model"];
      //alert(HomePhoneModel);
      var HomePhoneNumberCountryCode =this.state["HomePhoneNumberCountryCode"];
      if(this.state["Home_Phone_Model"] != '' && this.state["Home_Phone_Model"] != '+'){
        var total = HomePhoneModel.length - HomePhoneNumberCountryCode.length;
        if(total!=10) {
          step1Errors["Home_Phone_Model"] = "Please Enter a Valid Home Phone";
        }
      }

      var HomePhoneModel =this.state["Home_Phone_Model"];
      
      this.setState({ errormsg: step1Errors });
        
      if (Object.keys(step1Errors).length > 0) {
          return false;
      }

      this.showLoader();

      if(HomePhoneModel == '+'){
        HomePhoneModel = '';
      }

      let User_Basic_contact = {
        //preferredMethod: Preferred_Method_Model,
        preferredMethod: this.state["Preferred_Method_Model"],
        personalEmail: this.state["Personal_Email_Model"],
        cellPhone: this.state["Cell_Phone_Model"],
        homePhone: HomePhoneModel
      };

      let bodyarray = {};
      bodyarray["contactId"] = this.state.staffContactID;
      bodyarray["userBasicInfo"] = User_Basic_contact;
      bodyarray["userName"] = this.state.staffContactFullname;
      
      var url=process.env.API_API_URL+'UpdateUserProfileContact';
      fetch(url, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'bearer '+localStorage.getItem("token")
        },
        body: JSON.stringify(bodyarray)
      }).then((response) => response.json())
      .then(data => {
        //console.log("responseJson UpdateUserProfileContact");
        //console.log(data);
        //console.log(responseJson);
        // debugger;

        if (data.responseType === "1") {
          //this.ToastSuccess(data.responseMessge);  
          this.GetProfile();  
          this.ToastSuccess('Contact updated successfully');
          $( "#close_btn_contact" ).trigger( "click" );
        }
        else{
          this.ToastError(data.responseMessge);
        }
        this.hideLoader();
      })
      .catch(error => {
        this.props.history.push("/error-500");
      });
  }
  // Contact model Update api

  ClearRecord = ()=> e => {
    e.preventDefault();

    this.setState({ errormsg: '' });

    this.setState({ Preferred_Method_Model: this.state.all_data.preferredMethod });
    $('#Preferred_Method_Model').val(this.state.all_data.preferredMethod).trigger('change');
    this.setState({ PHHS_Email_Model: this.state.all_data.phssEmail });
    this.setState({ Personal_Email_Model: this.state.all_data.personalEmail });
    this.setState({ Cell_Phone_Model: this.state.all_data.cellPhone });
    this.setState({ Home_Phone_Model: this.state.all_data.homePhone });
    var CellCountryCode = this.state.all_data.cellPhone;
    
    if(CellCountryCode != null && CellCountryCode != ''){
      var CountryCodeCell = CellCountryCode.substring(0, CellCountryCode.length-10)
      this.setState({ CellPhoneNumberCountryCode: CountryCodeCell });
    }
  }

   render() {
     
      return (
        
          <div className="col-md-6 d-flex">
            {(this.state.loading) ? <Loader /> : null}
            <div className="card profile-box flex-fill pk-profile-box">
              <div className="card-body">
                {this.state.role_contact_information_can.contact_information_can_update == true ? 
                  <h3 className="card-title">Contact <a href="#" className="edit-icon" data-toggle="modal" data-target="#ProfileTab_contact_modal"><i className="fa fa-pencil" /></a></h3>
                  : <h3 className="card-title">Contact <a href="#" className="phss-lock"><i className="fa fa-lock" /></a></h3>
                }
                {/*<h5 className="section-title">Primary</h5>*/}
                <ul className="personal-info">
                  {/*<li>
                    <div className="title">Preferred Method</div>
                    {this.state.all_data.preferredMethod == '' || this.state.all_data.preferredMethod == null? <div className="text hide-font">None</div> : <div className="text">{this.state.all_data.preferredMethod}</div> }
                  </li>*/}
                  <li>
                    <div className="title">PHSS Email</div>
                    {this.state.all_data.phssEmail == '' || this.state.all_data.phssEmail == null? <div className="text hide-font">None</div> : <div className="text">{this.state.all_data.phssEmail}</div> }
                  </li>
                  {/*<li>
                    <div className="title">Personal Email</div>
                    {this.state.all_data.personalEmail == '' || this.state.all_data.personalEmail == null? <div className="text hide-font">None</div> : <div className="text">{this.state.all_data.personalEmail}</div> }
                  </li>*/}
                  <li>
                    <div className="title">Cell Phone </div>
                    {this.state.cellPhoneDisplay == '' || this.state.cellPhoneDisplay == null? <div className="text hide-font">None</div> : <div className="text">{this.state.cellPhoneDisplay}</div> }
                  </li>
                  <li>
                    <div className="title">Home Phone </div>
                    {this.state.homePhoneDisplay == '' || this.state.homePhoneDisplay == null? <div className="text hide-font">None</div> : <div className="text">{this.state.homePhoneDisplay}</div> }
                  </li>

                </ul>
              </div>
            </div>

              {/* Staff Contact Information Modal */}
            <div id="ProfileTab_contact_modal" className="modal custom-modal fade" role="dialog">
              <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div className="modal-content">
                  <div className="modal-header">
                    <h5 className="modal-title">Contact Information</h5>
                    <button type="button" className="close" id="close_btn_contact" data-dismiss="modal" aria-label="Close" onClick={this.ClearRecord()} >
                      <span aria-hidden="true">×</span>
                    </button>
                  </div>
                  <div className="modal-body">
                    <form>
                      <div className="card">
                        <div className="card-body">
                          {/* <h3 className="card-title">Primary Contact</h3> */}
                          <div className="row">
                            {/*<div className="col-md-6">
                              <div className="form-group">
                                <label>Preferred Method<span className="text-danger">*</span></label>
                                <select className="select form-control" id="Preferred_Method_Model" onChange={this.handleChange('Preferred_Method_Model')}>
                                  <option value="">-</option>
                                  <option value="Any">Any</option>
                                  <option value="Email">Email</option>
                                  <option value="Phone Call">Phone call</option>
                                </select>
                                <span className="form-text error-font-color">{this.state.errormsg["Preferred_Method_Model"]}</span>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Personal Email</label>
                                <input className="form-control" type="text" value={this.state.Personal_Email_Model} onChange={this.handleChange('Personal_Email_Model')} />
                                <span className="form-text error-font-color">{this.state.errormsg["Personal_Email_Model"]}</span>
                              </div>
                            </div>*/}
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>PHSS Email</label>
                                <input className="form-control" type="text" value={this.state.PHHS_Email_Model} readOnly = {true} />
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Cell Phone <span className="text-danger">*</span></label>
                                {/*<InputMask className="form-control" mask="(999) 999-9999" value={this.state.Cell_Phone_Model} onChange={this.handleChange('Cell_Phone_Model')} ></InputMask>*/}
                                <PhoneInput
                                  inputClass='form-control'
                                  inputStyle= {{'width': '100%'}}
                                  country={"ca"}
                                  onlyCountries={['ca', 'in' ,'us']}
                                  value={this.state.Cell_Phone_Model}
                                  onChange={this.handleOnChangeCellMobile.bind(this)}
                                />
                                <span className="form-text error-font-color">{this.state.errormsg["Cell_Phone_Model"]}</span>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Home Phone </label>
                                {/*<InputMask className="form-control" mask="(999) 999-9999" value={this.state.Home_Phone_Model} onChange={this.handleChange('Home_Phone_Model')} ></InputMask>*/}
                                <PhoneInput
                                  inputClass='form-control'
                                  inputStyle= {{'width': '100%'}}
                                  country={"ca"}
                                  onlyCountries={['ca', 'in' ,'us']}
                                  value={this.state.Home_Phone_Model}
                                  onChange={this.handleOnChangeHomeMobile.bind(this)}
                                />
                                <span className="form-text error-font-color">{this.state.errormsg["Home_Phone_Model"]}</span>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="submit-section">
                        <button className="btn btn-primary submit-btn" type="submit" onClick={this.UpdateUserProfileTabContactModel_API({})} >Submit</button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
            {/* //Staff Contact Information Modal */}
          </div>
        
      );
   }
}

export default ConatctView;
