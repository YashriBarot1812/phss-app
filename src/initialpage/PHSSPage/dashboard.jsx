/**
 * Signin Firebase
 */

import React, { Component } from 'react';
import { Helmet } from "react-helmet";
import { withRouter } from 'react-router-dom';
import {User,Avatar_19,Avatar_07,Avatar_06,Avatar_14} from '../../Entryfile/imagepath.jsx'

import {BarChart,Bar, Cell,ResponsiveContainer,
  LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend,
} from 'recharts';

import "../MainPage/index.css"

import Header from '../Sidebar/header.jsx';
import SidebarContent from '../Sidebar/sidebar';
import Loader from '../Loader';

import Helpers from '../Helpers/FileUploadHelper';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import SystemHelpers from '../Helpers/SystemHelper';



import CryptoAES from 'crypto-js/aes';
import CryptoENC from 'crypto-js/enc-utf8';

//table
import { MDBDataTable } from 'mdbreact';
import '@fortawesome/fontawesome-free/css/all.min.css';
import 'bootstrap-css-only/css/bootstrap.min.css';
import 'mdbreact/dist/css/mdb.css';
//table
// import 'Assets/plugins/morris/morris.min.js';
// import 'Assets/plugins/raphael/raphael.min.js';
// import 'Assets/js/chart.js';

import SkillsTab from "./profile/Skills";

import SelectDropdown from 'react-dropdown-select';

import moment from 'moment';

const barchartdata = [
      { y: '2006', "Total Income" : 100, 'Total Outcome' : 90 },
			{ y: '2007', "Total Income" : 75,  'Total Outcome' : 65 },
			{ y: '2008', "Total Income" : 50,  'Total Outcome' : 40 },
			{ y: '2009', "Total Income" : 75,  'Total Outcome' : 65 },
			{ y: '2010', "Total Income" : 50,  'Total Outcome' : 40 },
			{ y: '2011', "Total Income" : 75,  'Total Outcome' : 65 },
			{ y: '2012', "Total Income" : 100, 'Total Outcome' : 90 }
];
const linechartdata = [
      { y: '2006', "Total Sales": 50, 'Total Revenue': 90 },
			{ y: '2007', "Total Sales": 75,  'Total Revenue': 65 },
			{ y: '2008', "Total Sales": 50,  'Total Revenue': 40 },
			{ y: '2009', "Total Sales": 75,  'Total Revenue': 65 },
			{ y: '2010', "Total Sales": 50,  'Total Revenue': 40 },
			{ y: '2011', "Total Sales": 75,  'Total Revenue': 65 },
			{ y: '2012', "Total Sales": 100, 'Total Revenue': 50 }
];
class AdminDashboard extends Component {
  UNSAFE_componentWillMount (){
    let firstload = localStorage.getItem("firstload")
    if(firstload === "true"){
        setTimeout(function() {
          window.location.reload(1)
          localStorage.removeItem("firstload")
        },1000)
    }
  }

  constructor(props) {
      super(props);

      this.state = {
          // Pagination
          totalCount : 0,
          pageSize : 5,
          currentPage : 1,
          totalPages : 0,
          previousPage : false,
          nextPage : false,
          searchText : '',
          pagingData : {},
          TempsearchText:'',
          // Pagination
          questions: [],
          password : '',
          errormsg :  '',
          step : 1,
          setuserName : '',
          setMobileNumber : '',
          setPassword : '',
          setConfirmPassword : '',
          setQue1  : 0,
          setQue2 : 0,
          setQue3 : 0,
          setAns1 : '',
          setAns2 : '',
          setAns3 : '',
          LocationList:[],
          locationID:localStorage.getItem("primaryLocationId"),
          ListGrid:[],
          UserId : '',

          DropLocationName : 'All',
          role_contactlist_can : {},
          role_skills_can : {},

          isDelete : false,

          sortColumn : '',
          SortType : false,
          IsSortingEnabled : true,
          roleList : [],
          sortRoleId:'',

          ListGridSkillExpiry : [],
          ListGridSkillExpiry90Days : [],

          staffContactID:localStorage.getItem("contactId"),

          SelectFLocationList : []

      };
      this.handleChange = this.handleChange.bind(this)
  }

  // Input box Type method
  handleChange = input => e => {
    this.setState({ [input]: e.target.value });

    if (this.state[input] != '') {
        delete this.state.errormsg[input];
    }

    // if([input]=="locationID")
    // {
    //   this.setState({ ListGrid:[]});
    //   this.GetUsersByLocations(1,this.state.pageSize,this.state.searchText,e.target.value);
    // }
    
    if([input]=="locationID")
    {
      var DropLocationNm= $('#locationID option:selected').text();
      this.setState({ DropLocationName : DropLocationNm});

      
    }

    if([input]=="pageSize")
    {
      this.setState({ current_page: 1 });
      this.GetUsersByLocations(1,e.target.value,this.state.searchText,this.state.locationID);
    }

  }

  componentDidMount(){

    SystemHelpers.GetUserWiseTimeSheetData(localStorage.getItem("contactId"),localStorage.getItem("token"));
    
      if (location.pathname.includes("account_verification") || location.pathname.includes("login") || location.pathname.includes("register") || location.pathname.includes("forgotpassword")
      || location.pathname.includes("resetpassword") || location.pathname.includes("mobilesubmit") || location.pathname.includes("otp")|| location.pathname.includes("lockscreen") ) {
          $('body').addClass('account-page');
      }else if (location.pathname.includes("error-404") || location.pathname.includes("error-500") ) {
          $('body').addClass('error-page');
      }

      if (localStorage.getItem("token") != null) {
        this.props.history.push("/dashboard");
      }
      $("body").removeClass("account-page");
      
      

      // Role Store
      SystemHelpers.FetchRole(localStorage.getItem("contactId"),localStorage.getItem("token"));
      this.GetUsersByLocations(this.state.currentPage,this.state.pageSize,this.state.searchText,localStorage.getItem("primaryLocationId"));
      //var role_func = SystemHelpers.RoleFunc();
      //console.log('role_func');
      //console.log(role_func);

      // var Role_str_hrd = CryptoAES.encrypt('rolemanage', localStorage.getItem("contactId"));
      // var Role_str = CryptoAES.encrypt(role_func, localStorage.getItem("contactId"));

      // var _ciphertext = CryptoAES.decrypt(Role_str.toString(), 'Mct#123*111');
      // console.log('Role Store');
      // console.log(_ciphertext.toString(CryptoENC));
      //localStorage.setItem(Role_str_hrd, Role_str);
      // Role Store
      
      /* Role Management */
       console.log('Role Store contactlist_can');
       var getrole = SystemHelpers.GetRole();
       let contactlist_can = getrole.contactlist_can;
       this.setState({ role_contactlist_can: contactlist_can });
       console.log(contactlist_can);

        console.log(getrole);
      /* Role Management */

      /* Role Management */
        console.log('Role Store skills_can dashboard');
        let skills_can = getrole.skills_can;
        this.setState({ role_skills_can: skills_can });
        console.log(skills_can);

        
      /* Role Management */

      this.GetProfile();

      this.GetUserRegistrationDetail();

      this.GetUserSkillByExpire();
      
      this.GetPhssYearsCheckCurrentYear();
  }

  // Entitlement 
  GetPhssYearsCheckCurrentYear(){
    this.showLoader();
    var url=process.env.API_API_URL+'GetPhssYears';
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
    }).then((response) => response.json())
    .then(data => {
        //console.log("responseJson GetPhssYears");
        //console.log(data);
        if (data.responseType === "1") {
          
          let years= data.data;
          
          var current_year=moment().format(process.env.ENTITLEMENTS_DATE_FORMAT);
          
          var length = years.length;
          
            if (length > 0) {
              
              for (var zz = 0; zz < length; zz++) {
                if(years[zz].name === current_year){
                  localStorage.setItem('LocalStorageCurrentYearGuid', years[zz].guidId);
                  localStorage.setItem('LocalStorageCurrentYearName', years[zz].name);
                }
              }
              zz++;
            }

        }else{
          SystemHelpers.ToastError(data.message);   
        }
        this.hideLoader();
    })
    .catch(error => {
      this.props.history.push("/error-500");
    });
  }
  // Entitlement 

  GetUserRegistrationDetail(){
    //this.showLoader();
    /* Role Management */
    var getrole = SystemHelpers.GetRole();
    //let viewall = getrole.contactlist_can.contactlist_can_viewall;
    let hierarchyId = getrole.hierarchy_can.hierarchy_can_id;
    //rolePriorityId
    //alert(viewall);
    /* Role Management */
      var url=process.env.API_API_URL+'GetUserRegistrationDetail?contactId='+localStorage.getItem("contactId")+'&rolePriorityId='+hierarchyId+'&pageType=D';
      fetch(url, {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'bearer '+localStorage.getItem("token")
        },
        //body: JSON.stringify(bodyarray)
      }).then((response) => response.json())
      .then(data => {
          console.log("responseJson GetUserRegistrationDetail");
          console.log(data);
          //console.log(data.data.userRole);
          // debugger;
          if (data.responseType === "1") {
              // Profile & Contact
              if(data.data != null){
                //this.setState({ ListGrid: data.data.userEmergencyContactViews });
                //this.setState({ employeementHourList: data.data.employementHours });
                //this.setState({ userTypeList: data.data.userTypeList });
                this.setState({ roleList: data.data.roleList });
                
                if(data.data.locationList != "" && data.data.locationList != null)
                {
                  //console.log('mct location length');
                  //console.log((data.data.locationList).length)
                  this.setState({ LocationList: data.data.locationList });
                }
                
                //this.setState({ SelectFLocationList: this.FuncFLocationList(data.data.locationList) });
                //var location_list=data.data.locationList;
                //this.setState({ locationList: this.location_list_fun(location_list) }); 
              }
              
              
          }else{
                if(data.message == 'Authorization has been denied for this request.'){
                  SystemHelpers.SessionOut();
                  this.props.history.push("/login");
                }else{
                  SystemHelpers.ToastError(data.message);
                }
                
          }
          //this.hideLoader();
          
      })
      .catch(error => {
        this.props.history.push("/error-500");
      });
  }
  
  FuncFLocationList(Location)
  {
    console.log('fun FuncFLocationList');
    //console.log(Location);
    var Location_lenght = Location.length;
    let dataArray = [];
    var i=1;
    if(Location_lenght > 0)
    {
      for (var z = 0; z < Location_lenght; z++) {
        var tempdataArray = [];
        //SelectFLocationList
        if(Location[z].locationId == localStorage.getItem("primaryLocationId"))
        {
          //tempdataArray.name = Location[z].locationName;
          //tempdataArray.id = Location[z].locationId;

          dataArray.push(Location[z]);
        }
        z++;
      }
    }
    console.log(dataArray);
    return dataArray;
    
  }

  FilterLocationSelect (location){
    
    console.log('FilterLocationSelect');
    console.log(location);
    if(location.length > 0)
    {
      this.setState({ locationID: location[0].locationId })
    }
  }
      
  // Loader hide show method
  hideLoader = () => {
    this.setState({ loading: false });
  }

  showLoader = () => {
    this.setState({ loading: true });
  }
  // Loader hide show method

  Edit_Update_Btn_Func(record){
    let return_push = [];

     if(this.state.role_contactlist_can.contactlist_can_update == true || this.state.role_contactlist_can.contactlist_can_delete == true){
        let Edit_push = [];
        if(this.state.role_contactlist_can.contactlist_can_update == true){
          var url ="/employee-profile/"+record.contactId;
          Edit_push.push(<a href={url}  className="dropdown-item"><i className="fa fa-pencil m-r-5" ></i> Edit</a>);
        }
        let Delete_push = [];
        if(this.state.role_contactlist_can.contactlist_can_delete == true){
          if(record.isDelete == false)
          {
            Delete_push.push(
              //<a href="#"  className="dropdown-item" data-toggle="modal" data-target="#Documents_documents_Delete_modal"><i className="fa fa-trash-o m-r-5" /> Delete</a>
              <a href="#" onClick={this.DeleteInfo(record)}  className="dropdown-item" data-toggle="modal" data-target="#MyLocation_Delete_modal"><i className="fa fa-trash-o m-r-5" > </i> Inactive</a>
            );
          }
          else
          {
            Delete_push.push(
              //<a href="#"  className="dropdown-item" data-toggle="modal" data-target="#Documents_documents_Delete_modal"><i className="fa fa-trash-o m-r-5" /> Delete</a>
              <a href="#" onClick={this.DeleteInfo(record)}  className="dropdown-item" data-toggle="modal" data-target="#MyLocation_Delete_modal"><i className="fa fa-trash-o m-r-5" > </i> Active</a>
            );
          }
        }
        
        return_push.push(
          <div className="dropdown dropdown-action">
            <a aria-expanded="false" data-toggle="dropdown" className="action-icon dropdown-toggle" href="#"><i className="material-icons">more_vert</i></a>
            <div className="dropdown-menu dropdown-menu-right">
              {Edit_push}
              {/*{Delete_push}*/}
            </div>
          </div>
        );
      }
      return return_push;
  }

  DeleteInfo = (record) => e =>{
    e.preventDefault();

    this.setState({ UserId: record.contactId });
    this.setState({ isDelete: record.isDelete });
  }

  DeleteRecord = () => e => {
    e.preventDefault();

    var isdelete = '';
    if(this.state.isDelete== true)
    {
      isdelete = false;
    }
    else
    {
      isdelete = true;
    }

    this.showLoader();
    console.log(this.state.UserId);
    var url=process.env.API_API_URL+'DeleteUserEmpInfo?contactId='+this.state.UserId+'&isDelete='+isdelete+'&userName='+localStorage.getItem('fullName');
    fetch(url, {
      method: 'PUT',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
      //body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
        console.log("responseJson DeleteUserEmpInfo");
        console.log(data);
        //console.log(data.data.userRole);
        // debugger;
        if (data.responseType === "1") {
            // Profile & Contact
            SystemHelpers.ToastSuccess(data.responseMessge);
            $( ".cancel-btn" ).trigger( "click" );
            this.GetUserSkillINfo();
            this.hideLoader();
        }else if (data.responseType == "2" || data.responseType == "3") {
            SystemHelpers.ToastError(data.responseMessge);
            $( ".cancel-btn" ).trigger( "click" );
            this.hideLoader();
        }else{
              if(data.message == 'Authorization has been denied for this request.'){
                SystemHelpers.SessionOut();
                this.props.history.push("/login");
              }else{
                SystemHelpers.ToastError(data.message);
              }
              this.hideLoader();
              $( ".cancel-btn" ).trigger( "click" );
        }
        
        
    })
    .catch(error => {
      this.props.history.push("/error-500");
    });
  }

  

  
  GetProfile(){
    console.log(localStorage.getItem("token"));
    this.showLoader();
    var url=process.env.API_API_URL+'GetUserBasicInfoById?contactId='+localStorage.getItem("contactId");
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
      //body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
        console.log("responseJson GetUserBasicInfoById dashboard");
        console.log(data);
        //console.log(data.data.userRole);
        // debugger;
        if (data.responseType === "1") {
            this.setState({ preferredName: data.data.preferredName});
            this.setState({ locationID: data.data.primaryLocationId});
            //this.GetUsersByLocations(this.state.currentPage,this.state.pageSize,this.state.searchText,data.data.primaryLocationId);
            this.setState({ DropLocationName : data.data.primaryLocation});
            localStorage.setItem("primaryLocationId", data.data.primaryLocationId);
            localStorage.setItem("isPayrollAdmin", data.data.isPayrollAdmin);
            
            // User Type store session
            let UserTypeSession = {};
            UserTypeSession.isCordinator=data.data.isCordinator;
            UserTypeSession.isPayrollAdmin=data.data.isPayrollAdmin;
            UserTypeSession.isSeniorCordinator=data.data.isSeniorCordinator;
            UserTypeSession.isServiceCoordinatorLead=data.data.isServiceCoordinatorLead;

            console.log('UserTypeSession');
            console.log(UserTypeSession);

            var pwd = localStorage.getItem("contactId")+"Phss@123";
            var UserType_str = CryptoAES.encrypt(JSON.stringify(UserTypeSession), pwd);
            localStorage.setItem('usertypesession', UserType_str);
            // User Type store session




        }else{
              if(data.message == 'Authorization has been denied for this request.'){
                SystemHelpers.SessionOut();
                this.props.history.push("/login");
              }else{
                SystemHelpers.ToastError(data.message);
              }
              
        }
        this.hideLoader();
        
    })
    .catch(error => {
      console.log('GetUserBasicInfoById error');
      //this.props.history.push("/error-500");
    });
  }

  

  GetUsersByLocations(currentPage,pageSize,searchText,locationId){

    // console.log('dashboard 123');
    // console.log(currentPage);
    // console.log(pageSize);
    // console.log(searchText);
    // console.log(locationId);
    //return false;

    this.setState({ ListGrid : [] });

    let bodyarray = {};
    bodyarray["currentPage"] = 1;
    bodyarray["nextPage"] = false;
    bodyarray["pageSize"] = 5;
    bodyarray["previousPage"] = false;
    bodyarray["totalCount"] = 0;
    bodyarray["totalPages"] = 0;
    
    this.setState({ pagingData : bodyarray });

    

    /* Role Management */
    var getrole = SystemHelpers.GetRole();
    let canDelete = getrole.contactlist_can.contactlist_can_delete;
    /* Role Management */

    /* Role Management */
    //var getrole = SystemHelpers.GetRole();
    //let viewall = getrole.contactlist_can.contactlist_can_viewall;
    let hierarchyId = getrole.hierarchy_can.hierarchy_can_id;
    //alert(viewall);
    /* Role Management */
    
    var sort_Column = this.state.sortColumn;
    var Sort_Type = this.state.SortType;
    var roleId = this.state.sortRoleId;

    var IsSortingEnabled = true;
    

    console.log(localStorage.getItem("token"));
    this.showLoader();
    //var pass_url = 'GetUsersByLocations?contactId='+localStorage.getItem("contactId")+'&locationId='+locationId+'&pageNumber='+currentPage+'&pageSize='+pageSize+'&searchText='+searchText+'&canDelete='+canDelete;
    var pass_url = 'GetUsersByLocations?contactId='+localStorage.getItem("contactId")+'&locationId='+locationId+'&pageNumber='+currentPage+'&pageSize='+pageSize+'&searchText='+searchText+'&sortColumn='+sort_Column+'&SortType='+Sort_Type+'&IsSortingEnabled='+IsSortingEnabled+'&roleId='+roleId+'&rolePriorityId='+hierarchyId;
    console.log('pass_url');
    console.log(pass_url);
    var url=process.env.API_API_URL+pass_url;
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
      //body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
        console.log("responseJson GetUsersByLocations");
        console.log(data);
        //console.log(data.data.userRole);
        // debugger;
        
         
        if (data.responseType === "1") {
          
            this.setState({ currentPage: currentPage });
            this.setState({ pageSize: pageSize });

            this.setState({ ListGrid: data.data });
            this.setState({ pagingData: data.pagingData });

            this.setState({ IsSortingEnabled: false });

            //this.setState({ locationID : data.data.primaryLocationId});
        }else{
              if(data.message == 'Authorization has been denied for this request.'){
                SystemHelpers.SessionOut();
                this.props.history.push("/login");
              }else{
                //SystemHelpers.ToastError(data.message);
              }
              
        }
        this.hideLoader();
        
    })
    .catch(error => {
      console.log('GetUsersByLocations error');
      this.props.history.push("/error-500");
    });
  }


  GetUserSkillByExpire()
  {

    this.showLoader();
    var url=process.env.API_API_URL+'GetUserSkillByExpire?contactId='+localStorage.getItem("contactId");
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
      //body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
        console.log("responseJson GetUserSkillByExpire");
        console.log(data);
        //console.log(data.data.userRole);
        // debugger;
        if (data.responseType === "1") {
            // Profile & Contact
            if(data.data.userSkillInfo != null && data.data.userSkillInfo != []){
              this.setState({ ListGridSkillExpiry: data.data.userSkillInfo });
            }
            if(data.data.skillExpiryIn90Days != null && data.data.skillExpiryIn90Days != []){
              this.setState({ ListGridSkillExpiry90Days: data.data.skillExpiryIn90Days });
            }
                        
        }else{
              if(data.message == 'Authorization has been denied for this request.'){
                SystemHelpers.SessionOut();
                this.props.history.push("/login");
              }else{
                SystemHelpers.ToastError(data.message);
              }
              
        }
        this.hideLoader();
        
    })
    .catch(error => {
      this.props.history.push("/error-500");
    });
  }

  

  // Pagination Design
  PaginationDesign ()
  {
    let PageOutput = [];
    console.log('pagination');
    console.log(this.state.pagingData);
    
    if(this.state.pagingData !="" && this.state.pagingData !="undefined")
    {
      var Page_Count = this.state.pagingData.totalPages;
      //alert(this.state.pagingData.currentPage);
    //console.log('page count = ' + Page_Count);
    /* pagination count */


        var Page_Start=1;
        var Page_End=1;

        if(this.state.pagingData.currentPage == 1){
            Page_Start=1;

            if(Page_Count <= 10){
                Page_End=Page_Count;
            }else{
                Page_End=10;
            }
            
        }else{

            if(this.state.pagingData.currentPage < 5){
                Page_Start=1;
                Page_End=Page_Count;
                //console.log("Page_End 1 "+ Page_End);
            }else{
                Page_Start=parseInt(this.state.pagingData.currentPage) - parseInt(4);
                Page_End=parseInt(this.state.pagingData.currentPage) + parseInt(6);
                //console.log("Page_End 2 "+ Page_End);
                if(Page_End > Page_Count){
                    Page_End=Page_Count;
                    //console.log("Page_End 3 "+ Page_End);
                }
            }

        }
      let Page = [];
      var i = 1;
      for (var z=Page_Start; z <= Page_End ; z++)
      {
        if(z==this.state.pagingData.currentPage)
        {
          Page.push(<li className="page-item active pk-active">
            <a className="page-link pk-active" id={z} href="#" onClick={this.PageGetGridData}>{z}<span className="sr-only">(current)</span></a>
          </li>);
        }
        else
        {
          Page.push(<li className="page-item"><a className="page-link" id={z} href="#" onClick={this.PageGetGridData} >{z}</a></li>);
        }
        i++;
      }

      let PagePrev = [];

      if(this.state.pagingData.currentPage == 1){
        PagePrev.push(<li className="page-item disabled">
          <a className="page-link" href="#">Previous</a>
        </li>);
      }else{
        PagePrev.push(<li className="page-item">
          <a className="page-link" href="#" id={parseInt(this.state.pagingData.currentPage)-parseInt(1)} tabIndex={-1} onClick={this.PageGetGridData}>Previous</a>
        </li>);
      }

      let PageNext = [];

      if(this.state.pagingData.currentPage == Page_Count){
        PageNext.push(<li className="page-item disabled">
          <a className="page-link" href="#">Next</a>
        </li>);
      }else{
        PageNext.push(
          <li className="page-item">
            <a className="page-link" href="#" id={parseInt(this.state.pagingData.currentPage)+parseInt(1)} onClick={this.PageGetGridData}>Next</a>
          </li>
        );
      }

      let PageLast = [];

      if(this.state.pagingData.currentPage == Page_Count){
        PageLast.push(<li className="page-item disabled">
          <a className="page-link" href="#">Last</a>
        </li>);
      }else{
        PageLast.push(
          <li className="page-item">
            <a className="page-link" href="#" id={parseInt(Page_Count)} onClick={this.PageGetGridData}>Last</a>
          </li>
        );
      }



      PageOutput.push(<section className="comp-section" id="comp_pagination">
                        <div className="pagination-box">
                          <div>
                            <ul className="pagination">
                              
                              {PagePrev}
                              {Page}
                              {PageNext}
                              {PageLast}
                              
                            </ul>
                          </div>
                        </div>
                      </section>);
    }
    
    return PageOutput;
  }



  PageGetGridData = e => {

    e.preventDefault();
    console.log(e.target.id);
    let current_page= e.target.id;
    this.GetUsersByLocations(current_page,this.state.pageSize,this.state.searchText,this.state.locationID)
  }

  SearchGridData = e => {
    this.setState({ pageSize: this.state.TempsearchText });
    this.GetUsersByLocations(1,this.state.pageSize,this.state.TempsearchText,this.state.locationID);
  }
  // Pagination Design

  



   render() {

    
      return (
        <div className="main-wrapper">
       
        {/* Toast & Loder method use */}
            
        {(this.state.loading) ? <Loader /> : null} 
        {/* Toast & Loder method use */}
          <Header/>
        <div className="page-wrapper">
             <Helmet>
                    <title>{process.env.WEB_TITLE}</title>
                    <meta name="description" content="Dashboard"/>					
            </Helmet>
          {/* Page Content */}
          <div className="content container-fluid">
            {/* Page Header */}
            <div className="page-header">
              <div className="row">
                <div className="col-sm-12">
                  <h3 className="page-title">Welcome {this.state.preferredName}!</h3>
                  <ul className="breadcrumb">
                    <li className="breadcrumb-item active">Dashboard</li>
                  </ul>
                </div>
              </div>
            </div>
            {/* /Page Header */}
            <div className="row hide-div">
              <div className="col-md-6 col-sm-6 col-lg-6 col-xl-3">
                <div className="card dash-widget">
                  <div className="card-body">
                    <span className="dash-widget-icon"><i className="fa fa-cubes" /></span>
                    <div className="dash-widget-info">
                      <h3>112</h3>
                      <span>Projects</span>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-md-6 col-sm-6 col-lg-6 col-xl-3">
                <div className="card dash-widget">
                  <div className="card-body">
                    <span className="dash-widget-icon"><i className="fa fa-usd" /></span>
                    <div className="dash-widget-info">
                      <h3>44</h3>
                      <span>Clients</span>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-md-6 col-sm-6 col-lg-6 col-xl-3">
                <div className="card dash-widget">
                  <div className="card-body">
                    <span className="dash-widget-icon"><i className="fa fa-diamond" /></span>
                    <div className="dash-widget-info">
                      <h3>37</h3>
                      <span>Tasks</span>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-md-6 col-sm-6 col-lg-6 col-xl-3">
                <div className="card dash-widget">
                  <div className="card-body">
                    <span className="dash-widget-icon"><i className="fa fa-user" /></span>
                    <div className="dash-widget-info">
                      <h3>218</h3>
                      <span>Employees</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="row hide-div">
              <div className="col-md-12">
                <div className="row">
                  <div className="col-md-6 text-center">
                    <div className="card">
                      <div className="card-body">
                        <h3 className="card-title">Total Revenue</h3>
                        {/* <div id="bar-charts" /> */}
                        <ResponsiveContainer width='100%' height={300}>
                        <BarChart
                           
                           data={barchartdata}
                           margin={{
                             top: 5, right: 5, left: 5, bottom: 5,
                           }}
                         >
                           <CartesianGrid />
                           <XAxis dataKey="y" />
                           <YAxis />
                           <Tooltip />
                           <Legend />
                           <Bar dataKey="Total Income" fill="#00c5fb" />
                           <Bar dataKey="Total Outcome" fill="#0253cc" />
                         </BarChart>
                        </ResponsiveContainer>
                                             
                      </div>
                    </div>
                  </div>
                  <div className="col-md-6 text-center">
                    <div className="card">
                      <div className="card-body">
                        <h3 className="card-title">Sales Overview</h3>
                        <ResponsiveContainer width='100%' height={300}>
                        <LineChart data={linechartdata}
                            margin={{ top: 5, right: 5, left: 5, bottom: 5 }}>
                          <CartesianGrid  />
                          <XAxis dataKey="y" />
                          <YAxis />
                          <Tooltip />
                          <Legend />
                          <Line type="monotone" dataKey="Total Sales" stroke="#00c5fb" fill="#00c5fb" strokeWidth={3} dot={{r : 3}} activeDot={{ r: 7 }} />
                          <Line type="monotone" dataKey="Total Revenue" stroke="#0253cc" fill="#0253cc" strokeWidth={3} dot={{r : 3}} activeDot={{ r: 7 }} />
                        </LineChart>
                        </ResponsiveContainer>
                        
                        {/* <div id="line-charts" /> */}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="row hide-div">
              <div className="col-md-12">
                <div className="card-group m-b-30">
                  <div className="card">
                    <div className="card-body">
                      <div className="d-flex justify-content-between mb-3">
                        <div>
                          <span className="d-block">New Employees</span>
                        </div>
                        <div>
                          <span className="text-success">+10%</span>
                        </div>
                      </div>
                      <h3 className="mb-3">10</h3>
                      <div className="progress mb-2" style={{height: '5px'}}>
                        <div className="progress-bar bg-primary" role="progressbar" style={{width: '70%'}} aria-valuenow={40} aria-valuemin={0} aria-valuemax={100} />
                      </div>
                      <p className="mb-0">Overall Employees 218</p>
                    </div>
                  </div>
                  <div className="card">
                    <div className="card-body">
                      <div className="d-flex justify-content-between mb-3">
                        <div>
                          <span className="d-block">Earnings</span>
                        </div>
                        <div>
                          <span className="text-success">+12.5%</span>
                        </div>
                      </div>
                      <h3 className="mb-3">$1,42,300</h3>
                      <div className="progress mb-2" style={{height: '5px'}}>
                        <div className="progress-bar bg-primary" role="progressbar" style={{width: '70%'}} aria-valuenow={40} aria-valuemin={0} aria-valuemax={100} />
                      </div>
                      <p className="mb-0">Previous Month <span className="text-muted">$1,15,852</span></p>
                    </div>
                  </div>
                  <div className="card">
                    <div className="card-body">
                      <div className="d-flex justify-content-between mb-3">
                        <div>
                          <span className="d-block">Expenses</span>
                        </div>
                        <div>
                          <span className="text-danger">-2.8%</span>
                        </div>
                      </div>
                      <h3 className="mb-3">$8,500</h3>
                      <div className="progress mb-2" style={{height: '5px'}}>
                        <div className="progress-bar bg-primary" role="progressbar" style={{width: '70%'}} aria-valuenow={40} aria-valuemin={0} aria-valuemax={100} />
                      </div>
                      <p className="mb-0">Previous Month <span className="text-muted">$7,500</span></p>
                    </div>
                  </div>
                  <div className="card">
                    <div className="card-body">
                      <div className="d-flex justify-content-between mb-3">
                        <div>
                          <span className="d-block">Profit</span>
                        </div>
                        <div>
                          <span className="text-danger">-75%</span>
                        </div>
                      </div>
                      <h3 className="mb-3">$1,12,000</h3>
                      <div className="progress mb-2" style={{height: '5px'}}>
                        <div className="progress-bar bg-primary" role="progressbar" style={{width: '70%'}} aria-valuenow={40} aria-valuemin={0} aria-valuemax={100} />
                      </div>
                      <p className="mb-0">Previous Month <span className="text-muted">$1,42,000</span></p>
                    </div>
                  </div>
                </div>
              </div>	
            </div>
            
            <div className="row hide-div">
              <div className="col-md-6 d-flex">
                <div className="card card-table flex-fill">
                  <div className="card-header">
                    <h3 className="card-title mb-0">Invoices</h3>
                  </div>
                  <div className="card-body">
                    <div className="table-responsive">
                      <table className="table table-nowrap custom-table mb-0">
                        <thead>
                          <tr>
                            <th>Invoice ID</th>
                            <th>Client</th>
                            <th>Due Date</th>
                            <th>Total</th>
                            <th>Status</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td><a href="#">#INV-0001</a></td>
                            <td>
                              <h2><a href="#">Global Technologies</a></h2>
                            </td>
                            <td>11 Mar 2019</td>
                            <td>$380</td>
                            <td>
                              <span className="badge bg-inverse-warning">Partially Paid</span>
                            </td>
                          </tr>
                          <tr>
                            <td><a href="#">#INV-0002</a></td>
                            <td>
                              <h2><a href="#">Delta Infotech</a></h2>
                            </td>
                            <td>8 Feb 2019</td>
                            <td>$500</td>
                            <td>
                              <span className="badge bg-inverse-success">Paid</span>
                            </td>
                          </tr>
                          <tr>
                            <td><a href="#">#INV-0003</a></td>
                            <td>
                              <h2><a href="#">Cream Inc</a></h2>
                            </td>
                            <td>23 Jan 2019</td>
                            <td>$60</td>
                            <td>
                              <span className="badge bg-inverse-danger">Unpaid</span>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                  <div className="card-footer">
                    <a href="#">View all invoices</a>
                  </div>
                </div>
              </div>
              <div className="col-md-6 d-flex">
                <div className="card card-table flex-fill">
                  <div className="card-header">
                    <h3 className="card-title mb-0">Payments</h3>
                  </div>
                  <div className="card-body">
                    <div className="table-responsive">	
                      <table className="table custom-table table-nowrap mb-0">
                        <thead>
                          <tr>
                            <th>Invoice ID</th>
                            <th>Client</th>
                            <th>Payment Type</th>
                            <th>Paid Date</th>
                            <th>Paid Amount</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td><a href="#">#INV-0001</a></td>
                            <td>
                              <h2><a href="#">Global Technologies</a></h2>
                            </td>
                            <td>Paypal</td>
                            <td>11 Mar 2019</td>
                            <td>$380</td>
                          </tr>
                          <tr>
                            <td><a href="#">#INV-0002</a></td>
                            <td>
                              <h2><a href="#">Delta Infotech</a></h2>
                            </td>
                            <td>Paypal</td>
                            <td>8 Feb 2019</td>
                            <td>$500</td>
                          </tr>
                          <tr>
                            <td><a href="#">#INV-0003</a></td>
                            <td>
                              <h2><a href="#">Cream Inc</a></h2>
                            </td>
                            <td>Paypal</td>
                            <td>23 Jan 2019</td>
                            <td>$60</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                  <div className="card-footer">
                    <a href="/app/accounts/payments">View all payments</a>
                  </div>
                </div>
              </div>
            </div>

                {/* Search Filter */}
                <div className="row filter-row hidden-xs hidden-sm hidden-md">
                  
                  
                  
                  <div className="col-lg-3 col-sm-4 col-xs-12"> 
                    <div className="form-group form-focus select-focus">
                        {/*<SelectDropdown
                          options={this.state.LocationList}
                          valueField="locationId"
                          labelField="locationName"
                          searchBy="locationName"
                          className="form-control floating"
                          placeholder="Search Location"
                          id="locationID"
                         
                          values={this.state.SelectFLocationList}
                          onChange={(values) => this.FilterLocationSelect(values)}
                          
                        />*/}
                      
                      <select className="form-control floating" id="locationID" value={this.state.locationID} onChange={this.handleChange('locationID')}> 
                        <option value="">All</option>
                        {this.state.LocationList.map(( listValue, index ) => {
                         
                            return (
                              <option key={index} value={listValue.locationId} >{listValue.locationName}</option>
                            );
                         
                        })}
                      </select>
                      <label className="focus-label">Search Location</label>
                      
                    </div>
                  </div>
                  

                  <div className="col-lg-3 col-sm-4 col-xs-12"> 
                    <div className="form-group form-focus select-focus">
                      <select className="form-control floating" id="sortColumnId" value={this.state.sortColumn} onChange={this.handleChange('sortColumn')}> 
                        <option value="">-</option>
                        <option value="FirstName">Name</option>
                        <option value="PhssEmail">Email</option>
                        <option value="UserRoleDisplay">Role</option>
                      </select>
                      <label className="focus-label">Sorting</label>
                    </div>
                  </div>

                  <div className="col-lg-3 col-sm-4 col-xs-12"> 
                    <div className="form-group form-focus select-focus">
                      <select className="form-control" id="sortRoleId" value={this.state.sortRoleId}  onChange={this.handleChange('sortRoleId')} >
                        <option value=''>-</option>
                        {this.state.roleList.map(( listValue, index ) => {
                            return (
                              <option key={index} value={listValue.id}>{listValue.name}</option>
                            );
                        })}
                      </select>
                      <label className="focus-label">Role</label>
                    </div>
                  </div>

                  <div className="col-lg-3 col-sm-4 col-xs-12"> 
                    <div className="form-group form-focus select-focus">
                      <select className="form-control floating" id="SortTypeId" value={this.state.SortType} onChange={this.handleChange('SortType')}> 
                        <option value="">-</option>
                        <option value="false">Ascending</option>
                        <option value="true">Descending</option>
                      </select>
                      <label className="focus-label">Sorting Order</label>
                    </div>
                  </div> 

                  <div className="col-lg-3 col-sm-4 col-xs-12"> 
                    <div className="form-group form-focus select-focus">
                      <select className="form-control floating" value={this.state.pageSize}  onChange={this.handleChange('pageSize')}> 
                        <option value="5">5/Page</option>
                        <option value="10">10/Page</option>
                        <option value="50">50/Page</option>
                        <option value="100">100/Page</option>
                      </select>
                      <label className="focus-label">Per Page</label>
                    </div>
                  </div>

                  <div className="col-lg-3 col-sm-4 col-xs-12">  
                    <div className="form-group form-focus">
                      <input className="form-control floating" type="text" value={this.state.TempsearchText}  onChange={this.handleChange('TempsearchText')}/>
                      <label className="focus-label">Search</label>
                    </div>
                  </div>

                  <div className="col-lg-6 col-sm-12 col-xs-12">  
                    <a href="#" className="col-lg-4 col-sm-4 col-xs-12 float-right btn btn-success btn-block" onClick={this.SearchGridData}> Search </a>  
                  </div>

                  

                </div>
                {/* /Search Filter */}    
                {/* /Page Header */}
                <div className="row">
                  <div className="col-md-12 d-flex">
                    <div className="card card-table flex-fill">
                      <div className="card-header">
                        <div className="row">
                          <div className="col-md-8">
                            <h3 className="card-title mb-0">My Location Staff {this.state.DropLocationName}</h3>
                          </div>
                        </div>
                    <br/>
                      </div>
                      <div className="card-body">
                        <div className="table-responsive">
                          <table className="table custom-table mb-0">
                            <thead>
                              <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Phone Number</th>
                                <th>Location</th>
                                <th className="d-none d-sm-table-cell">Role</th>
                                
                                
                                
                              </tr>
                            </thead>
                            <tbody>
                              {this.state.ListGrid.map(( listValue, index ) => {
                                    var status = "";
                                    if(listValue.isDelete == false){
                                      status = <div><span class="badge bg-inverse-success">Active</span></div>;
                                    }else{
                                      status = <div><span class="badge bg-inverse-warning">Inactive</span></div>;
                                    }

                                    // Contact number display format
                                    var cellPhone= listValue.cellPhone;
                                    var format_CellePhoneDisplay ='';
                                    if(cellPhone != null && cellPhone != ''){
                                        var CountryCodeCellDisplay = cellPhone.substring(0, cellPhone.length-10)
                                        var ret = cellPhone.replace(CountryCodeCellDisplay,'');
                                        var format_CellePhoneDisplay=CountryCodeCellDisplay+" "+ ret.replace(/(\d{3})(\d{3})(\d{4})/, "$1-$2-$3");
                                    }
                                    // Contact number display format
                                    return (
                                      <tr key={index}>
                                        <td>{listValue.firstName} {listValue.lastName}</td>
                                        <td>{listValue.phssEmail}</td>
                                        <td>{format_CellePhoneDisplay}</td>
                                        <td>{listValue.primaryLocation}</td>
                                        <td>{listValue.userRoleDisplay}</td>
                                        
                                        
                                      </tr>
                                    );
                                 
                              })}
                            </tbody>
                          </table>
                          {/* Pagination */}
                          {this.PaginationDesign()}
                          {/* /Pagination */}
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-md-6 d-flex hide-div">
                    <div className="card card-table flex-fill">
                      <div className="card-header">
                        <h3 className="card-title mb-0">Recent Projects</h3>
                      </div>
                      <div className="card-body">
                        <div className="table-responsive">
                          <table className="table custom-table mb-0">
                            <thead>
                              <tr>
                                <th>Project Name </th>
                                <th>Progress</th>
                                <th className="text-right">Action</th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <td>
                                  <h2><a href="#">Office Management</a></h2>
                                  <small className="block text-ellipsis">
                                    <span>1</span> <span className="text-muted">open tasks, </span>
                                    <span>9</span> <span className="text-muted">tasks completed</span>
                                  </small>
                                </td>
                                <td>
                                  <div className="progress progress-xs progress-striped">
                                    <div className="progress-bar" role="progressbar" data-toggle="tooltip" title="65%" style={{width: '65%'}} />
                                  </div>
                                </td>
                                <td className="text-right">
                                  <div className="dropdown dropdown-action">
                                    <a href="#" className="action-icon dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i className="material-icons">more_vert</i></a>
                                    <div className="dropdown-menu dropdown-menu-right">
                                      <a className="dropdown-item" href=""><i className="fa fa-pencil m-r-5" /> Edit</a>
                                      <a className="dropdown-item" href=""><i className="fa fa-trash-o m-r-5" /> Delete</a>
                                    </div>
                                  </div>
                                </td>
                              </tr>
                              <tr>
                                <td>
                                  <h2><a href="#">Project Management</a></h2>
                                  <small className="block text-ellipsis">
                                    <span>2</span> <span className="text-muted">open tasks, </span>
                                    <span>5</span> <span className="text-muted">tasks completed</span>
                                  </small>
                                </td>
                                <td>
                                  <div className="progress progress-xs progress-striped">
                                    <div className="progress-bar" role="progressbar" data-toggle="tooltip" title="15%" style={{width: '15%'}} />
                                  </div>
                                </td>
                                <td className="text-right">
                                  <div className="dropdown dropdown-action">
                                    <a href="#" className="action-icon dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i className="material-icons">more_vert</i></a>
                                    <div className="dropdown-menu dropdown-menu-right">
                                      <a className="dropdown-item" href=""><i className="fa fa-pencil m-r-5" /> Edit</a>
                                      <a className="dropdown-item" href=""><i className="fa fa-trash-o m-r-5" /> Delete</a>
                                    </div>
                                  </div>
                                </td>
                              </tr>
                              <tr>
                                <td>
                                  <h2><a href="#">Video Calling App</a></h2>
                                  <small className="block text-ellipsis">
                                    <span>3</span> <span className="text-muted">open tasks, </span>
                                    <span>3</span> <span className="text-muted">tasks completed</span>
                                  </small>
                                </td>
                                <td>
                                  <div className="progress progress-xs progress-striped">
                                    <div className="progress-bar" role="progressbar" data-toggle="tooltip" title="49%" style={{width: '49%'}} />
                                  </div>
                                </td>
                                <td className="text-right">
                                  <div className="dropdown dropdown-action">
                                    <a href="#" className="action-icon dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i className="material-icons">more_vert</i></a>
                                    <div className="dropdown-menu dropdown-menu-right">
                                      <a className="dropdown-item" href=""><i className="fa fa-pencil m-r-5" /> Edit</a>
                                      <a className="dropdown-item" href=""><i className="fa fa-trash-o m-r-5" /> Delete</a>
                                    </div>
                                  </div>
                                </td>
                              </tr>
                              <tr>
                                <td>
                                  <h2><a href="#">Hospital Administration</a></h2>
                                  <small className="block text-ellipsis">
                                    <span>12</span> <span className="text-muted">open tasks, </span>
                                    <span>4</span> <span className="text-muted">tasks completed</span>
                                  </small>
                                </td>
                                <td>
                                  <div className="progress progress-xs progress-striped">
                                    <div className="progress-bar" role="progressbar" data-toggle="tooltip" title="88%" style={{width: '88%'}} />
                                  </div>
                                </td>
                                <td className="text-right">
                                  <div className="dropdown dropdown-action">
                                    <a href="#" className="action-icon dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i className="material-icons">more_vert</i></a>
                                    <div className="dropdown-menu dropdown-menu-right">
                                      <a className="dropdown-item" href=""><i className="fa fa-pencil m-r-5" /> Edit</a>
                                      <a className="dropdown-item" href=""><i className="fa fa-trash-o m-r-5" /> Delete</a>
                                    </div>
                                  </div>
                                </td>
                              </tr>
                              <tr>
                                <td>
                                  <h2><a href="#">Digital Marketplace</a></h2>
                                  <small className="block text-ellipsis">
                                    <span>7</span> <span className="text-muted">open tasks, </span>
                                    <span>14</span> <span className="text-muted">tasks completed</span>
                                  </small>
                                </td>
                                <td>
                                  <div className="progress progress-xs progress-striped">
                                    <div className="progress-bar" role="progressbar" data-toggle="tooltip" title="100%" style={{width: '100%'}} />
                                  </div>
                                </td>
                                <td className="text-right">
                                  <div className="dropdown dropdown-action">
                                    <a href="#" className="action-icon dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i className="material-icons">more_vert</i></a>
                                    <div className="dropdown-menu dropdown-menu-right">
                                      <a className="dropdown-item" href=""><i className="fa fa-pencil m-r-5" /> Edit</a>
                                      <a className="dropdown-item" href=""><i className="fa fa-trash-o m-r-5" /> Delete</a>
                                    </div>
                                  </div>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                      </div>
                      <div className="card-footer">
                        <a href="#">View all projects</a>
                      </div>
                    </div>
                  </div>
                </div>

                {/* Delete My Location Staff Locations  Modal */}
                <div className="modal custom-modal fade" id="MyLocation_Delete_modal" role="dialog">
                  <div className="modal-dialog modal-dialog-centered">
                    <div className="modal-content">
                      <div className="modal-body">
                        <div className="form-header">
                          <h3>My Location Staff</h3>
                          <p>Are you sure want to {this.state.isDelete == true ? 'Active' : 'Inactive' } ?</p>
                        </div>
                        <div className="modal-btn delete-action">
                          <div className="row">
                            <div className="col-6">
                              <a  onClick={this.DeleteRecord()} className="btn btn-primary continue-btn">{this.state.isDelete == true ? 'Active' : 'Inactive' }</a>
                            </div>
                            <div className="col-6">
                              <a href="" data-dismiss="modal" className="btn btn-primary cancel-btn">Cancel</a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                {/* //Delete My Location Staff Modal */}

                {/* Skill Dashboard */}
                {this.state.role_skills_can.skills_can_view == true && localStorage.getItem("sessiontoken") != null && localStorage.sessiontoken != 'undefined' ? 
                  <div className="row">
                    <div className="col-md-12">
                      <div className="card">
                        <div className="card-body">
                          <h4 className="card-title">Skill Statistics</h4>
                          <ul className="nav nav-tabs nav-tabs-solid nav-justified">
                            <li className="nav-item"><a className="nav-link active" href="#solid-justified-tab1" data-toggle="tab">Expired skills<h3 className="pk-h3">{this.state.ListGridSkillExpiry.length}</h3></a></li>
                            <li className="nav-item"><a className="nav-link" href="#solid-justified-tab2" data-toggle="tab">Skills Expiring within 3 Months<h3 className="pk-h3">{this.state.ListGridSkillExpiry90Days.length}</h3></a></li>
                          </ul>
                          <div className="tab-content">
                            <div className="tab-pane show active" id="solid-justified-tab1">
                              <SkillsTab 
                                staffContactID={this.state.staffContactID}
                                skills_can={this.state.role_skills_can}
                                page_type="dashboard_Expiry"
                                setPropState={this.setPropState}
                              />
                            </div>
                            <div className="tab-pane" id="solid-justified-tab2">
                              <SkillsTab 
                                  staffContactID={this.state.staffContactID}
                                  skills_can={this.state.role_skills_can}
                                  page_type="dashboard_Expiry90Days"
                                  setPropState={this.setPropState}
                                />
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                :null
                }
                {/* /Skill Dashboard */}

          </div>
          {/* /Page Content */}
        </div>
        <SidebarContent/>
      </div>
      );
   }
}

export default withRouter(AdminDashboard);
