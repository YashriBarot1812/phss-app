/**
 * TermsCondition Page
 */
import React, { Component } from 'react';
import { Helmet } from "react-helmet";
import {Reportto,ProfileImage,Avatar_02,Avatar_05,Avatar_09,Avatar_10,Avatar_16,ProfileImageFemale } from '../../Entryfile/imagepath'

import Header from '../Sidebar/header.jsx';
import SidebarContent from '../Sidebar/sidebar';
import moment from 'moment';
// import other file

import AddressInfo from "./profile/address";
import EmergencyContactTab from "./profile/EmergencyContact";
import EmploymentInformationTab from "./profile/EmploymentInformation";
import SkillsTab from "./profile/Skills";
import HumanResourcesTab from "./profile/HumanResources";
import ConsentsAwardsTab from "./profile/ConsentsAwards";
import DocumentsTab from "./profile/Documents";
import RoleManager from "./profile/RoleManager";
import InputMask from 'react-input-mask';
import AwardsRecognitionTab from "./profile/AwardsRecognition";
import ProfileLocationTab from "./profile/ProfileLocation";
import VolunteerTab from "./profile/Volunteer";
import AvailabilityTab from "./profile/Availability";
import EntitlementTab from "./profile/ProfileEntitlement";
// import other file

// Profile Tab
import ProfileView from "./profile/ProfileTab/ProfileView";
import ContactView from "./profile/ProfileTab/ContactView";
import PersonalInformation from "./profile/ProfileTab/PersonalInformation";
import PhssEmploymentInformation from "./profile/ProfileTab/PhssEmploymentInformation";

// Profile Tab

import Loader from '../Loader';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import SystemHelpers from '../Helpers/SystemHelper';

import CryptoAES from 'crypto-js/aes';
import CryptoENC from 'crypto-js/enc-utf8';

export default class EmployeeProfile extends Component {
  constructor(props) {
    super(props);

    this.state = {
        errormsg :  '',
        all_data :  [],
        //user_role :  {},
        
        /* role_func_call */
        role_func_call : false,
        get_profile_call : false,
        primarylocation: '',
        staffContactID:this.props.match.params.id,

        /* role_func_call */
        role_func_call : false,
        get_profile_call : false,
        primarylocation: '',
        primaryLocationId:'',
        role_personal_info_can:{},
        role_phss_empinfo_can:{},
        role_emergency_contact_can:{},
        role_employment_history_can:{},
        role_course_training_can:{},
        role_skills_can:{},
        role_consents_waivers_can:{},
        role_awards_recognitions_can:{},
        role_documents_can:{},
        role_human_resource_can:{},
        role_profile_info_can:{},
        role_contact_information_can:{},
        role_address_can :{},
        role_availability_can :{},
        role_locations_departments_can :{},
        role_volunteer_can :{},
        role_entitlement_can :{},

        /* role_func_call */

        isProfileImageShow : true,
        primaryLocationName :''
    };
    this.handleChange = this.handleChange.bind(this);
    this.setPropState = this.setPropState.bind(this);
  }

  setPropState(key, value) {
      this.setState({ [key]: value });
  }

  

  // Loader hide show method
  hideLoader = () => {
    this.setState({ loading: false });
  }

  showLoader = () => {
    this.setState({ loading: true });
  }
  // Loader hide show method

 

  // Input box Type method
  handleChange = input => e => {
    this.setState({ [input]: e.target.value });
  }
  componentDidMount() {
    var staffContactID=this.props.match.params.id;
    this.setState({ staffContactID: staffContactID });
    this.GetProfile();

     /* Role Management */
    var pwd = localStorage.getItem("contactId")+"Phss@123";
    var Role_session = localStorage.getItem('sessiontoken');

    var _ciphertext = CryptoAES.decrypt(Role_session, pwd);
    console.log('Role Store awards_recognitions_can');
    //console.log(_ciphertext.toString(CryptoENC));
    var JsonCreate = JSON.parse(_ciphertext.toString(CryptoENC));
    console.log(JsonCreate);
    
    this.setState({ role_profile_info_can: JsonCreate.staff_profile_info_can });
    this.setState({ role_contact_information_can: JsonCreate.staff_contact_information_can });
    this.setState({ role_address_can : JsonCreate.staff_address_can });
    this.setState({ role_availability_can : JsonCreate.staff_availability_can });
    this.setState({ role_personal_info_can: JsonCreate.staff_personal_info_can });
    this.setState({ role_phss_empinfo_can: JsonCreate.staff_phss_empinfo_can });
    this.setState({ role_emergency_contact_can: JsonCreate.staff_emergency_contact_can });
    this.setState({ role_employment_history_can: JsonCreate.staff_employment_history_can });
    this.setState({ role_course_training_can: JsonCreate.staff_course_training_can });
    this.setState({ role_skills_can: JsonCreate.staff_skills_can });
    this.setState({ role_consents_waivers_can: JsonCreate.staff_consents_waivers_can });
    this.setState({ role_awards_recognitions_can: JsonCreate.staff_awards_recognitions_can });
    this.setState({ role_documents_can: JsonCreate.staff_documents_can });
    this.setState({ role_human_resource_can: JsonCreate.staff_human_resource_can });
    this.setState({ role_locations_departments_can: JsonCreate.staff_locations_departments_can });
    this.setState({ role_volunteer_can: JsonCreate.staff_volunteer_can });
    this.setState({ role_entitlement_can: JsonCreate.staff_entitlement_can });
    
    console.log(JsonCreate);
    console.log(JsonCreate.awards_recognitions_can);
    /* Role Management */

    $('#profileimageID').attr('src', ProfileImage);
  }

  TabClickOnLoad= (TabName) => e => {
      //debugger;
      e.preventDefault();

      if(TabName == 'emergency_contacts')
      {
        //alert(TabName);
        $( "#TabClickOnLoadEmergencyContacts" ).trigger( "click" );
        return false;
      }
      else if(TabName == 'skills')
      {
        $( "#TabClickOnLoadSkills" ).trigger( "click" );
        return false;
      }
      else if(TabName == 'consents_waivers')
        {
        $( "#TabClickOnLoadConsentsWaivers" ).trigger( "click" );
        return false;
      }
      else if(TabName == 'awards_recognitions')
      {
        $( "#TabClickOnLoadAwardsRecognition" ).trigger( "click" );
        return false;
      }
      else if(TabName == 'documents')
      {
        $( "#TabClickOnLoadDocuments" ).trigger( "click" );
        return false;
      }
      else if(TabName == 'human_resource')
      {
        $( "#TabClickOnLoadHumanResources" ).trigger( "click" );
        return false;
      }
      else if(TabName == 'locations_departments')
      {
        $( "#TabClickOnLoadLocationsDepartments" ).trigger( "click" );
        return false;
      }
      else if(TabName == 'volunteer')
      {
        $( "#TabClickOnLoadVolunteer" ).trigger( "click" );
        return false;
      }
      else if(TabName == 'entitlement')
      {
        $( "#TabClickOnLoadEntitlementsCurrentBalance" ).trigger( "click" );
        return false;
      }
      else if(TabName == 'employment_history')
      {
        $( "#TabClickOnLoadEmploymentHistory" ).trigger( "click" );
        return false;
      }
      else if(TabName == 'availability')
      {
        $( "#TabClickOnLoadAvailability" ).trigger( "click" );
        return false;
      }
  }

  GetProfile(){
    console.log(localStorage.getItem("token"));
    this.showLoader();
    var url=process.env.API_API_URL+'GetUserBasicInfoById?contactId='+this.state.staffContactID;
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
      //body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {

        console.log("responseJson GetUserBasicInfoById staff");
        //console.log(this.state.staffContactID);
        //console.log(localStorage.getItem("contactId"));
        console.log(data);
        //console.log(data.data.userRole);
        // debugger;
        
         
        if (data.responseType === "1") {
            this.setState({ all_data: data.data});
            this.setState({ Preferred_Name: data.data.preferredName});
            this.setState({ get_profile_call: true});
            this.setState({ staffNumberId: data.data.staffNumberId});
            this.setState({ primarylocation: data.data.primaryLocation});  
            this.setState({ reportToUserName: data.data.reportToUserName});  
            this.setState({ userRoleDisplay: data.data.userRoleDisplay}); 
            this.setState({ primaryLocationName: data.data.primaryLocation});
            if(data.data.photoAuthorization == null )
            {
              if(this.state.userGender == "Female"){
                $('#profileimageID').attr('src', ProfileImageFemale);
              }else{
                $('#profileimageID').attr('src', ProfileImage);
              }
              
            }else{
              //if(data.data.photoRequired == true){
                $('#profileimageID').attr('src', data.data.photoAuthorization);
              /*}else{
                if(this.state.userGender == "Female"){
                  $('#profileimageID').attr('src', ProfileImageFemale);
                }else{
                  $('#profileimageID').attr('src', ProfileImage);
                }
              }*/
            }

            this.setState({ isProfileImageShow: data.data.photoRequired});

        }else{
              if(data.message == 'Authorization has been denied for this request.'){
                SystemHelpers.SessionOut();
                this.props.history.push("/login");
              }else{
                SystemHelpers.ToastError(data.message);
              }
              
        }
        // setTimeout(function() {
        //   $( "#profile_tab_click" ).trigger( "click" );
        // }, 8000);
        
        this.hideLoader();
        
    })
    .catch(error => {
      this.props.history.push("/error-500");
    });
  }

  

render() {
    return (
      <div className="main-wrapper">
        {/* Toast & Loder method use */}
        <ToastContainer position="top-right"
            autoClose={process.env.API_TOAST_TIMEOUT}
            hideProgressBar={false}
            newestOnTop={false}
            closeOnClick={false}
            rtl={false}
            pauseOnVisibilityChange={false}
            draggable={false}
            pauseOnHover={false} />
        {(this.state.loading) ? <Loader /> : null} 
        {/* Toast & Loder method use */}

      {/*<RoleManager 
        setPropState={this.setPropState}
      />*/}
      <Header/>
        <div className="page-wrapper">
            <Helmet>
              <title>{process.env.WEB_TITLE}</title>
              <meta name="description" content="Reactify Blank Page" />
            </Helmet>

            {/* Page Content */}
            <div className="content container-fluid">
              {/* Page Header */}
              
              

              <div className="page-header">
                <div className="row">
                  <div className="col-sm-12">
                    <h3 className="page-title">Profile</h3>
                    <ul className="breadcrumb">
                      <li className="breadcrumb-item"><a href="/app/main/dashboard">Dashboard</a></li>
                      <li className="breadcrumb-item active">Profile</li>
                    </ul>
                  </div>
                </div>
              </div>
              {/* /Page Header */}
              <div className="card mb-0">
                <div className="card-body">
                  <div className="row">
                    <div className="col-md-12">
                      <div className="profile-view">
                        <div className="profile-img-wrap">
                          <div className="profile-img">
                            
                            <a href="#"><img alt="" id="profileimageID" src="" /></a>
                            
                          </div>
                        </div>
                        <div className="profile-basic">
                          <div className="row">
                            <div className="col-lg-5">
                              <div className="profile-info-left">
                                <h3 className="user-name m-t-0 mb-0">{this.state.Preferred_Name == '' || this.state.Preferred_Name == null? <div className="text hide-font">None</div> : <div className="text">{this.state.Preferred_Name}</div> }</h3>
                                <h6 className="text-muted"></h6>
                                <small className="text-muted"></small>
                                {/*<div className="staff-id">Employee ID : {this.state.staffNumberId}</div>
                                <div className="staff-id">Primary location : {this.state.primaryLocationName}</div>
                                <div className="staff-id">Role : {this.state.userRoleDisplay}</div>*/}
                                <ul className="personal-info">
                                  <li>
                                    <div className="title">Employee ID:</div>
                                    {this.state.staffNumberId == '' || this.state.staffNumberId == null? <div className="text hide-font">None</div> : <div className="text">{this.state.staffNumberId}</div> }
                                  </li>
                                  <li>
                                    <div className="title">Primary location:</div>
                                    {this.state.primaryLocationName == '' || this.state.primaryLocationName == null? <div className="text hide-font">None</div> : <div className="text">{this.state.primaryLocationName}</div> }
                                  </li>
                                  <li>
                                    <div className="title">Role:</div>
                                    {this.state.userRoleDisplay == '' || this.state.userRoleDisplay == null? <div className="text hide-font">None</div> : <div className="text">{this.state.userRoleDisplay}</div> }
                                  </li>
                                </ul>
                              </div>
                            </div>
                            <div className="col-lg-7">
                              <ul className="personal-info">
                                <li>
                                  <div className="title">Phone:</div>
                                  {this.state.all_data.cellPhone == '' || this.state.all_data.cellPhone == null? <div className="text hide-font">None</div> : <div className="text">{this.state.all_data.cellPhone}</div> }
                                </li>
                                <li>
                                  <div className="title">Email:</div>
                                  {this.state.all_data.phssEmail == '' || this.state.all_data.phssEmail == null? <div className="text hide-font">None</div> : <div className="text">{this.state.all_data.phssEmail}</div> }
                                </li>
                                <li>
                                  <div className="title">Birthday:</div>
                                  {this.state.all_data.dateOfBirth == '' || this.state.all_data.dateOfBirth == null? <div className="text hide-font">None</div> : <div className="text">{moment(this.state.all_data.dateOfBirth,process.env.API_DATE_FORMAT).format(process.env.DATE_FORMAT)}</div> }
                                </li>
                                {/*
                                <li>
                                  <div className="title">Gender:</div>
                                  {this.state.all_data.gender == '' || this.state.all_data.gender == null? <div className="text hide-font">None</div> : <div className="text">{this.state.all_data.gender}</div> }
                                </li>
                                */}
                                <li>
                                  <div className="title">Reports to:</div>
                                  <div className="text">
                                    <div className="avatar-box">
                                      <div className="avatar avatar-xs">
                                        <img src={Reportto} alt="" />
                                      </div>
                                    </div>
                                    <a href="#" className="capitalize-title">
                                      {this.state.reportToUserName}
                                    </a>
                                  </div>
                                </li>
                                <li>
                                  <div className="title">Timesheet Approver:</div>
                                  <div className="text">{this.state.TimeSheetApproverName}</div>
                                </li>
                                
                              </ul>
                            </div>
                          </div>
                        </div>
                        {/*<div className="pro-edit"><a data-target="#profile_info" data-toggle="modal" className="edit-icon" href="#"><i className="fa fa-pencil" /></a></div> */}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="card tab-box">
                <div className="row user-tabs">
                  <div className="col-lg-12 col-md-12 col-sm-12 line-tabs">
                    <ul className="nav nav-tabs nav-tabs-bottom">
                    {this.state.get_profile_call == true && (this.state.role_profile_info_can.profile_info_can_create == true || this.state.role_contact_information_can.contact_information_can_view == true || this.state.role_address_can.address_can_view == true) ?
                      <li className="nav-item"><a href="#profile_tab" id="profile_tab_click" data-toggle="tab" className="nav-link active">Profile</a></li>
                    :null }
                    {this.state.get_profile_call == true && (this.state.role_personal_info_can.personal_info_can_view == true || this.state.role_phss_empinfo_can.phss_empinfo_can_view == true) ?
                      <li className="nav-item"><a href="#extended_profile_tab" data-toggle="tab" className="nav-link">Extended Profile</a></li>
                    :null }
                    {this.state.get_profile_call == true && this.state.role_emergency_contact_can.emergency_contact_can_view == true ?
                      <li className="nav-item"><a href="#emergency_contacts_tab" data-toggle="tab" className="nav-link" onClick={this.TabClickOnLoad('emergency_contacts')} >Emergency Contacts </a></li>
                    :null }
                    {this.state.get_profile_call == true && (this.state.role_employment_history_can.employment_history_can_view == true || this.state.role_course_training_can.course_training_can_view == true) ?
                      <li className="nav-item"><a href="#employment_information_tab" data-toggle="tab" className="nav-link" onClick={this.TabClickOnLoad('employment_history')} >Employment History</a></li>
                    :null }
                    {this.state.get_profile_call == true && this.state.role_skills_can.skills_can_view == true ?
                      <li className="nav-item"><a href="#skills_tab" data-toggle="tab" className="nav-link" onClick={this.TabClickOnLoad('skills')} >Skills</a></li>
                    :null }
                    {this.state.get_profile_call == true && this.state.role_consents_waivers_can.consents_waivers_can_view == true ?
                      <li className="nav-item"><a href="#consents_waivers_tab" data-toggle="tab" className="nav-link" onClick={this.TabClickOnLoad('consents_waivers')} >Consents and Waivers</a></li>
                    :null }
                    {this.state.get_profile_call == true && this.state.role_awards_recognitions_can.awards_recognitions_can_view == true ?
                      <li className="nav-item"><a href="#awards_recognition_tab" data-toggle="tab" className="nav-link" onClick={this.TabClickOnLoad('awards_recognitions')} >Awards and Recognition</a></li>
                    :null }
                    {this.state.get_profile_call == true && this.state.role_documents_can.documents_can_view == true ?
                      <li className="nav-item"><a href="#documents_tab" data-toggle="tab" className="nav-link" onClick={this.TabClickOnLoad('documents')} >Documents</a></li>
                    :null }
                    {this.state.get_profile_call == true && this.state.role_human_resource_can.human_resource_can_view == true ?
                      <li className="nav-item"><a href="#human_resources_tab" data-toggle="tab" className="nav-link" onClick={this.TabClickOnLoad('human_resource')} >Human Resources</a></li>
                    :null }
                    
                    
                    {this.state.get_profile_call == true && this.state.role_locations_departments_can.locations_departments_can_view == true ?
                    <li className="nav-item"><a href="#profile_location_tab" data-toggle="tab" className="nav-link" onClick={this.TabClickOnLoad('locations_departments')} >Locations</a></li>
                    :null }
                    

                    {this.state.get_profile_call == true && this.state.role_volunteer_can.volunteer_can_view == true ?
                    <li className="nav-item"><a href="#volunteer_tab" data-toggle="tab" className="nav-link" onClick={this.TabClickOnLoad('volunteer')} >Volunteer</a></li>
                    :null }

                    {process.env.Availability_Tab == "true" && this.state.get_profile_call == true && this.state.role_availability_can.availability_can_view == true ?
                    
                    <li className="nav-item"><a href="#availability_tab" data-toggle="tab" className="nav-link" onClick={this.TabClickOnLoad('availability')} >Availability</a></li>

                    :null }

                    {this.state.get_profile_call == true && this.state.role_entitlement_can.entitlement_can_view == true ?
                      <li className="nav-item"><a href="#entitlement_tab" data-toggle="tab" className="nav-link" onClick={this.TabClickOnLoad('entitlement')} >Entitlement</a></li>
                    :null }
                    
                    </ul>
                  </div>
                </div>
              </div>

              <div className="tab-content">
                {/* //============ Profile Contact Tab ============ */}
                <div id="profile_tab" className="tab-pane fade show active">
                  <div className="row">
                    
                  {this.state.get_profile_call == true && this.state.role_profile_info_can.profile_info_can_view == true ?
                    <ProfileView
                      staffContactID={this.state.staffContactID}

                      profile_info_can={this.state.role_profile_info_can}
                      all_data={this.state.all_data}

                      setPropState={this.setPropState}
                    />
                  :null }

                  {this.state.get_profile_call == true && this.state.role_contact_information_can.contact_information_can_view == true ?
                    <ContactView
                      staffContactID={this.state.staffContactID}
                      
                      contact_information_can={this.state.role_contact_information_can}
                      all_data={this.state.all_data}
                      
                      setPropState={this.setPropState}
                    />
                  :null }


                    
                  </div>
                  {/* //============ Address Grid ============ */}

                  {this.state.get_profile_call == true && this.state.role_address_can.address_can_view == true ? 
                    <div>
                        <AddressInfo
                          
                          staffContactID={this.state.staffContactID}
                          address_can={this.state.role_address_can}
                          
                          setPropState={this.setPropState}
                        />
                    </div>
                    :null
                  }
                              
                   {/* //============ Address Grid ============ */}
                </div>
                {/* //============ Profile Contact Tab ============ */}

                {/* //============ Extended Profile Tab ============ */}
                <div id="extended_profile_tab" className="pro-overview tab-pane fade">
                  
                    {this.state.get_profile_call == true && this.state.role_personal_info_can.personal_info_can_view == true ?
                      <PersonalInformation
                        staffContactID={this.state.staffContactID}
                        
                        personal_info_can={this.state.role_personal_info_can}
                        all_data={this.state.all_data}
                        
                        setPropState={this.setPropState}
                      />
                    :null }

                    {this.state.get_profile_call == true && this.state.role_phss_empinfo_can.phss_empinfo_can_view == true ?
                      <PhssEmploymentInformation
                        staffContactID={this.state.staffContactID}
                        
                        phss_empinfo_can={this.state.role_phss_empinfo_can}
                        all_data={this.state.all_data}
                        
                        setPropState={this.setPropState}
                      />
                    :null }
                  
                  
                </div>
                {/* //============ Extended Profile Tab ============ */}

                {/* //=========== Emergency Contacts Tab =========== */}
                <div id="emergency_contacts_tab" className="tab-pane fade">
                
                  {this.state.get_profile_call == true && this.state.role_emergency_contact_can.emergency_contact_can_view == true ?
                    <EmergencyContactTab 
                      staffContactID={this.state.staffContactID}
                      
                      emergency_contact_can={this.state.role_emergency_contact_can}
                      
                      setPropState={this.setPropState}
                    />
                    :<div></div>
                  }
                </div>
                {/* //============ Emergency Contacts Tab ============ */}

                {/* ============ Employment Information infomation Tab ============ */}
                <div id="employment_information_tab" className="tab-pane fade">
                
                  {this.state.get_profile_call == true && (this.state.role_employment_history_can.employment_history_can_view == true || this.state.role_course_training_can.course_training_can_view == true) ?
                    <EmploymentInformationTab 
                      primaryLocationId={this.state.all_data.primaryLocationId}
                      staffContactID={this.state.staffContactID}

                      employment_history_can={this.state.role_employment_history_can}
                      course_training_can={this.state.role_course_training_can}

                      setPropState={this.setPropState}
                    />
                  :<div></div>
                  }
                
                </div>
                {/* //============ Employment infomation Tab ============ */}

                {/* ============ Skills infomation Tab ============ */}
                <div id="skills_tab" className="tab-pane fade">
                {this.state.role_skills_can.skills_can_view == true ? 
                  <SkillsTab 
                    staffContactID={this.state.staffContactID}
                    
                    skills_can={this.state.role_skills_can}
                    page_type="main_skill_page"
                    setPropState={this.setPropState}
                  />
                :<div></div>
                }
                </div>
                {/* //============ Skills infomation Tab ============ */}

                {/* ============ Consents and Waivers Tab ============ */}
                <div id="consents_waivers_tab" className="tab-pane fade">
                  {this.state.role_consents_waivers_can.consents_waivers_can_view == true ?
                    <ConsentsAwardsTab
                      staffContactID={this.state.staffContactID}

                      consents_waivers_can={this.state.role_consents_waivers_can}

                      setPropState={this.setPropState}  
                     />
                    :<div></div>
                  }
                </div>
                {/* //============ Consents and Awards Tab ============ */}

                {/* ============ Awards and Recognition Tab ============ */}
                <div id="awards_recognition_tab" className="tab-pane fade">
                  {this.state.role_awards_recognitions_can.awards_recognitions_can_view == true ?
                    <AwardsRecognitionTab
                      staffContactID={this.state.staffContactID}

                      awards_recognitions_can={this.state.role_awards_recognitions_can}
                      
                      setPropState={this.setPropState}  
                     />
                    :<div></div>
                  }
                </div>
                {/* //============ Awards and Recognition Tab ============ */}

                {/* ============ Documents Tab ============  && this.state.documents_can_view == true */}
                <div id="documents_tab" className="tab-pane fade">

                  { this.state.role_documents_can.documents_can_view == true  ? 

                    <DocumentsTab 
                      staffContactID={this.state.staffContactID}

                      documents_can={this.state.role_documents_can}
                      
                      setPropState={this.setPropState}
                    />
                  :<div></div>
                  }
                 
                </div>
                {/* //============ Documents Tab ============ */}

                {/* ============ human resources Tab ============ */}
                <div id="human_resources_tab" className="tab-pane fade">
                  {this.state.role_human_resource_can.human_resource_can_view == true ? 
                    <HumanResourcesTab 
                      staffContactID={this.state.staffContactID}

                      human_resource_can={this.state.role_human_resource_can}
                      
                      setPropState={this.setPropState}
                    />
                  :<div></div>
                  }
                 
                </div>
                {/* //============ human resources Tab ============ */}

              {/* ============ profile location Tab ============ */}
                <div id="profile_location_tab" className="tab-pane fade">
                  { this.state.role_locations_departments_can.locations_departments_can_view == true ? 
                    <ProfileLocationTab 
                      staffContactID={this.state.staffContactID}

                      locations_departments_can={this.state.role_locations_departments_can}
                      
                      setPropState={this.setPropState}
                    />
                  :<div></div>
                  }
                 
                </div>
              {/* //============ profile location Tab ============ */}

              
              {/* //============ Volunteer Tab ============ */}
                <div id="volunteer_tab" className="pro-overview tab-pane fade">
                  
                  {this.state.get_profile_call == true && this.state.role_volunteer_can.volunteer_can_view == true ?
                    <VolunteerTab
                      staffContactID={this.state.staffContactID}

                      volunteer_can={this.state.role_volunteer_can}
                      all_data={this.state.all_data}
                      
                      setPropState={this.setPropState}
                    />
                  :null }
                  
                  
                </div>
              {/* //============ Volunteer Tab ============ */}

              {/* ============ availability Tab ============ */}
                
                  <div id="availability_tab" className="tab-pane fade">
                      {this.state.get_profile_call == true && this.state.role_availability_can.availability_can_view == true ?
                        <AvailabilityTab 
                            staffContactID={this.state.staffContactID}
                            availability_can={this.state.role_availability_can}
                            all_data={this.state.all_data}
                            setPropState={this.setPropState}
                        />
                      :null }
                  </div>
                  
              {/* //============ availability Tab ============ */}


              {/* //============ Entitlement Tab ============ */}
                <div id="entitlement_tab" className="pro-overview tab-pane fade">
                  
                  {this.state.get_profile_call == true && this.state.role_entitlement_can.entitlement_can_view == true ?
                    <EntitlementTab
                      staffContactID={this.state.staffContactID}
                      entitlement_can={this.state.role_entitlement_can}
                      all_data={this.state.all_data}
                      setPropState={this.setPropState}
                    />
                  :null }
                  
                  
                </div>
              {/* //============ Entitlement Tab ============ */}

              </div>

            </div>
            {/* //Page Content */}


            {/* ------------------------- Modes ------------------------- */}

            {/* Profile Modal */}
            <div id="profile_info" className="modal custom-modal fade" role="dialog">
              <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div className="modal-content">
                  <div className="modal-header">
                    <h5 className="modal-title">Profile Information</h5>
                    <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">×</span>
                    </button>
                  </div>
                  <div className="modal-body">
                    <form>
                      <div className="row">
                        <div className="col-md-12">
                          <div className="profile-img-wrap edit-img">
                            <img className="inline-block" src={Avatar_02} alt="user" />
                            <div className="fileupload btn">
                              <span className="btn-text">edit</span>
                              <input className="upload" type="file" />
                            </div>
                          </div>
                          <div className="row">
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>First Name</label>
                                <input type="text" className="form-control" defaultValue="John" />
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Last Name</label>
                                <input type="text" className="form-control" defaultValue="Doe" />
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Birth Date</label>
                                <div className="cal-icon">
                                  <input className="form-control datetimepicker" type="text" defaultValue="05/06/1985" />
                                </div>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Gender</label>
                                <select className="select form-control">
                                  <option value="male selected">Male</option>
                                  <option value="female">Female</option>
                                </select>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-md-12">
                          <div className="form-group">
                            <label>Address</label>
                            <input type="text" className="form-control" defaultValue="4487 Snowbird Lane" />
                          </div>
                        </div>
                        <div className="col-md-6">
                          <div className="form-group">
                            <label>State</label>
                            <input type="text" className="form-control" defaultValue="New York" />
                          </div>
                        </div>
                        <div className="col-md-6">
                          <div className="form-group">
                            <label>Country</label>
                            <input type="text" className="form-control" defaultValue="United States" />
                          </div>
                        </div>
                        <div className="col-md-6">
                          <div className="form-group">
                            <label>Pin Code</label>
                            <input type="text" className="form-control" defaultValue={10523} />
                          </div>
                        </div>
                        <div className="col-md-6">
                          <div className="form-group">
                            <label>Phone Number</label>
                            <input type="text" className="form-control" defaultValue="631-889-3206" />
                          </div>
                        </div>
                        <div className="col-md-6">
                          <div className="form-group">
                            <label>Department <span className="text-danger">*</span></label>
                            <select className="select">
                              <option>Select Department</option>
                              <option>Web Development</option>
                              <option>IT Management</option>
                              <option>Marketing</option>
                            </select>
                          </div>
                        </div>
                        <div className="col-md-6">
                          <div className="form-group">
                            <label>Designation <span className="text-danger">*</span></label>
                            <select className="select">
                              <option>Select Designation</option>
                              <option>Web Designer</option>
                              <option>Web Developer</option>
                              <option>Android Developer</option>
                            </select>
                          </div>
                        </div>
                        <div className="col-md-6">
                          <div className="form-group">
                            <label>Reports To <span className="text-danger">*</span></label>
                            <select className="select">
                              <option>-</option>
                              <option>Wilmer Deluna</option>
                              <option>Lesley Grauer</option>
                              <option>Jeffery Lalor</option>
                            </select>
                          </div>
                        </div>
                      </div>
                      <div className="submit-section">
                        <button className="btn btn-primary submit-btn">Submit</button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
            {/* /Profile Modal */}

            {/* ****************** Profile Tab Modals ****************** */}
            

            

            
        </div>
      <SidebarContent/>
      </div>
    );
  }

}
