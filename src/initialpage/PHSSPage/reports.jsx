
import React, { Component } from 'react';
import { Helmet } from "react-helmet";
import {  Avatar_01 ,Avatar_02,Avatar_03,Avatar_04, Avatar_05, Avatar_08, Avatar_09, Avatar_10,
    Avatar_11,Avatar_12,Avatar_13,Avatar_16   } from "../../Entryfile/imagepath"

import { Table } from 'antd';
import 'antd/dist/antd.css';
import {itemRender,onShowSizeChange} from "./pagination/paginationfunction"
import "../MainPage/antdstyle.css"
import { Multiselect } from 'multiselect-react-dropdown';

//import Select from 'react-select-me';
import 'react-select-me/lib/ReactSelectMe.css';

import Header from '../Sidebar/header.jsx';
import SidebarContent from '../Sidebar/sidebar';

import Loader from '../Loader';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import SystemHelpers from '../Helpers/SystemHelper';
import FileUploadHelper from '../Helpers/FileUploadHelper';

import CryptoAES from 'crypto-js/aes';
import CryptoENC from 'crypto-js/enc-utf8';

import moment from 'moment';

import Select from 'react-select';

import SelectDropdown from 'react-dropdown-select';

//table
import { MDBDataTable } from 'mdbreact';
import '@fortawesome/fontawesome-free/css/all.min.css';
import 'bootstrap-css-only/css/bootstrap.min.css';
import 'mdbreact/dist/css/mdb.css';
//table

class Reports extends Component {
  constructor(props) {
    super(props);
    this.state = {

        locationID:localStorage.getItem("primaryLocationId"),
        staffContactID:localStorage.getItem("contactId"),
        fullName:localStorage.getItem("fullName"),

        role_reports_can : {},
        role_employees_can : {},
        role_hierarchy_can : {},

        errormsg :  '',
        isDelete : false,
        ListGrid : [],

        header_data : [],

        payPeriodListFilter : [],
        locationListFilter : [],
        serviceCodeListFilter :[],
        employmentTypeListFilter :[],
        employeeStatusListFilter :[],
        EmployeeNameListFilter : [],

        FilterPayPeriod : '',
        FilterLocation : '',
        FilterServiceCode : '',
        FilterEmploymentType : '',
        FilterEmployeeStatus : '',
        FilterStartDate : '',
        FilterEndDate : '',
        FilterEmployee : '',

        ExportFilter : {},
       
        GetCRMUserList:[],
        selectedOptionLocation: null,
        selectedOptionEmployees: null,
        locationName:'',
        employeeName:''
    };

    this.setPropState = this.setPropState.bind(this);
    this.handleChange = this.handleChange.bind(this);
    
    this.onChangeSearch = this.onChangeSearch.bind(this);
  }

  onChangeSearch(value) {
    console.log(value);
    this.setState({ value });
  }

  setPropState(key, value) {
      this.setState({ [key]: value });
  }

  // Loader hide show method
  hideLoader = () => {
    this.setState({ loading: false });
  }

  showLoader = () => {
    this.setState({ loading: true });
  }
  // Loader hide show method

  handleChange = input => e => {
    this.setState({ [input]: e.target.value });
    //console.log('handleChange');
    //console.log(input);

    if (this.state[input] != '') {
      delete this.state.errormsg[input];
    }

    if([input] == "FilterStartDate")
    {
      this.setState({ FilterEndDate: "" });
    }
    
    // ========== Filter conditions ========== //
    // 1) When pay period select then startdate and enddate disabled
    if([input]=="FilterPayPeriod" && e.target.value != "")
    {
      this.setState({ FilterStartDate: "" });
      this.setState({ FilterEndDate: "" });
      $('#FilterStartDate').prop('disabled', true);
      $('#FilterEndDate').prop('disabled', true);
    }

    if([input]=="FilterPayPeriod" && e.target.value == "") 
    {
      $('#FilterStartDate').prop('disabled', false);
      $('#FilterEndDate').prop('disabled', false);
    }
    // 1) When pay period select then startdate and enddate disabled

    // 2) When start date select then pay period disabled
    if([input]=="FilterStartDate" && e.target.value != "") 
    {
      this.setState({ FilterPayPeriod: "" });
      $('#FilterPayPeriod').prop('disabled', true);
    }
    if([input]=="FilterStartDate" && e.target.value == "") 
    {
      $('#FilterPayPeriod').prop('disabled', false);
    }
    // 2) When start date select then pay period disabled
    // ========== Filter conditions ========== //
  }

  componentDidMount() {
    console.log("Reports");

    /* Role Management */
    console.log('Role Store reports_can');
    var getrole = SystemHelpers.GetRole();
    let report_can = getrole.report_can;
    this.setState({ role_reports_can: report_can });
    console.log(report_can);

    console.log('Role Store hierarchy_can');
    var getrole_hierarchy = SystemHelpers.GetRole();
    let hierarchy_can = getrole_hierarchy.hierarchy_can;
    this.setState({ role_hierarchy_can: hierarchy_can });
    console.log(hierarchy_can);
    /* Role Management */
    console.log(localStorage.getItem("token"));
    
    this.GetTimeSheetReportView();
    
    // Table header
    var columns = [
        {
          label: 'Code location',
          field: 'codelocation',
          sort: 'asc',
          width: 150
        },
        {
          label: 'Location',
          field: 'location',
          sort: 'asc',
          width: 150
        },
        {
          label: 'Employee name',
          field: 'employeename',
          sort: 'asc',
          width: 150
        },
        {
          label: 'Service Name',
          field: 'servicename',
          sort: 'asc',
          width: 150
        },
        {
          label: 'Employment Type',
          field: 'employmenttype',
          sort: 'asc',
          width: 150
        },
        {
          label: '# Hours by period',
          field: 'hoursbyperiod',
          sort: 'asc',
          width: 150
        },
        {
          label: 'YTDHours',
          field: 'YTDHours',
          sort: 'asc',
          width: 150
        }
      ];

    this.setState({ header_data: columns });
    // Table header
  }

  GetTimeSheetReportView(){

    /* Role Management */
      var getrole = SystemHelpers.GetRole();
      console.log('Location Get role');
      console.log(getrole.locations_can);
      //let canDelete = getrole.locations_can.locations_can_delete;
      //let locationscanViewall = getrole.locations_can.locations_can_viewall;
      let hierarchyId = getrole.hierarchy_can.hierarchy_can_id;
    /* Role Management */
    this.showLoader();
    var url=process.env.API_API_URL+'GetTimeSheetReportView?loggedInUserId='+this.state.staffContactID+'&rolePriorityId='+hierarchyId;
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
      //body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
      console.log("responseJson Report GetTimeSheetReportView");
      console.log(data);
      
      // debugger;
      if (data.responseType === "1") {

        this.setState({ payPeriodListFilter: data.data.timeSheetPeriodViews});

        if(this.state.role_hierarchy_can.hierarchy_can_id == "1")
        {
          $('#FilterLocation').prop('disabled', true);

          $('#FilterEmployee').prop('disabled', true);
          this.setState({ FilterEmployee: this.state.staffContactID });

          $('#FilterEmploymentType').prop('disabled', true);
          this.setState({ FilterEmploymentType: localStorage.getItem("employmentHoursName") });
          
          $('#FilterEmployeeStatus').prop('disabled', true);
          this.setState({ FilterEmployeeStatus: localStorage.getItem("employmentStatusName") });
        }
        
        //this.setState({ locationListFilter: data.data.locationViews });
        this.setState({ serviceCodeListFilter: data.data.serviceViews });
        this.setState({ employmentTypeListFilter: data.data.employeementTypes });
        this.setState({ employeeStatusListFilter: data.data.employeeStatus });
        //this.setState({ EmployeeNameListFilter: data.data.employees });

        /* ************************************************************ */
        let locationViews_push = [];
        var locationViews = data.data.locationViews;

        let locationtemp = {};
        locationtemp["value"] = "";
        locationtemp["label"] = "All";
        locationViews_push.push(locationtemp);

        for (var zz = 0; zz < locationViews.length; zz++) {
            let locationtemp = {};
            locationtemp["value"] = locationViews[zz].locationId;
            locationtemp["label"] = locationViews[zz].locationName;
            locationViews_push.push(locationtemp);
        }

        this.setState({ LocationListFilter: locationViews_push });
        //this.setState({ locationListFilter: data.data.locationViews });
        /* ************************************************************ */

        /* ************************************************************ */
        let employeeViews_push = [];
        var employeeViews = data.data.employees;

        let employeetemp = {};
        employeetemp["value"] = "";
        employeetemp["label"] = "All";
        employeeViews_push.push(employeetemp);

        for (var zz = 0; zz < employeeViews.length; zz++) {
            let employeetemp = {};
            employeetemp["value"] = employeeViews[zz].contactId;
            employeetemp["label"] = employeeViews[zz].employeeName;
            employeeViews_push.push(employeetemp);
        }

        this.setState({ EmployeeNameListFilter: employeeViews_push });
        //this.setState({ EmployeeNameListFilter: data.data.employees });
        /* ************************************************************ */
        
        this.Default_TimeSheet(data.data.timeSheetPeriodViews);
      }else{
        if(data.message == 'Authorization has been denied for this request.'){
          SystemHelpers.SessionOut();
          this.props.history.push("/login");
        }else{
          SystemHelpers.ToastError(data.message);
        }
      }
      this.hideLoader();
    })
    .catch(error => {
      console.log('GetUserWiseTimeSheetData error');
      this.props.history.push("/error-500");
    });
  }
  
  Default_TimeSheet(timeSheetPeriod){
      var length = timeSheetPeriod.length;
      
      if (length > 0) {
        var i = 1;
        for (var zz = 0; zz < length; zz++) {

          if(timeSheetPeriod[zz].isCurrentTimeSheet == true){

            this.setState({ FilterPayPeriod: timeSheetPeriod[zz].timeSheetPeriodId});
            
            $('#FilterStartDate').prop('disabled', true);
            $('#FilterEndDate').prop('disabled', true);
          }
          i++;
        }
      }
  }

  GetReportListGrid = () => e => {
    //alert(moment().format('MM/DD/YYYY'));
    //return false;

      e.preventDefault();

      this.setState({ ListGrid : [] });
      this.setState({ ExportFilter: {} });

      /* Role Management */
      var getrole = SystemHelpers.GetRole();
      let canDelete = getrole.employees_can.employees_can_delete;
      let canViewall = getrole.employees_can.employees_can_viewall
      console.log('employee getrole');
      console.log(getrole.employees_can.employees_can_viewall);
      console.log(getrole);
      /* Role Management */

      var FilterPayPeriod = this.state.FilterPayPeriod;
      var payperiodname = $('#FilterPayPeriod').find(':selected').data('payperiodname');
      
      var FilterLocation = this.state.FilterLocation;
      //var locationname = $('#FilterLocation').find(':selected').data('locationname');
      var locationname = this.state.locationName;

      var FilterServiceCode = this.state.FilterServiceCode;
      var servicename = $('#FilterServiceCode').find(':selected').data('servicename');

      var FilterEmploymentType = this.state.FilterEmploymentType;

      var FilterEmployeeStatus = this.state.FilterEmployeeStatus;

      if(this.state.FilterStartDate != ""){
        var FilterStartDate = moment(this.state.FilterStartDate).format("MM/DD/YYYY");
      }else{
        var FilterStartDate = "";
      }
      
      if(this.state.FilterEndDate !=""){
        var FilterEndDate = moment(this.state.FilterEndDate).format("MM/DD/YYYY");
      }else{
        var FilterEndDate = "";
      }
      

      var FilterEmployee = this.state.FilterEmployee;
      var employeename = $('#FilterEmployee').find(':selected').data('employeename');
      var employeename = this.state.employeeName;

      console.log('FilterPayPeriod = '+FilterPayPeriod+' FilterPayPeriodName = '+payperiodname);
      console.log('FilterLocation = '+FilterLocation+' FilterLocationName = '+locationname);
      console.log('FilterServiceCode = '+FilterServiceCode+' FilterServiceCodeName = '+servicename);
      console.log('FilterEmploymentType = '+FilterEmploymentType);
      console.log('FilterEmployeeStatus = '+FilterEmployeeStatus);
      console.log('FilterStartDate = '+FilterStartDate);
      console.log('FilterEndDate = '+FilterEndDate);
      console.log('FilterEmployeeId = '+FilterEmployee+' FilterEmployeeName = '+employeename);

      
      var paraPayPeriod = 'periodProfileId='+FilterPayPeriod+'&periodProfileName='+payperiodname;
      
      var paraLocation = '&locationId='+FilterLocation+'&locationName='+locationname;
      
      var paraServiceCode = '&serviceCodeId='+FilterServiceCode+'&codeOfService='+servicename;

      var paraEmploymentType = '&employeementType='+FilterEmploymentType;
      var paraStartDate = '&startDate='+FilterStartDate;
      var paraEndDate = '&endDate='+FilterEndDate;
      
      var paraEmployeeStatus = '&employeeStatus='+FilterEmployeeStatus;

      var paraContactId = '&contactId='+FilterEmployee+'&employeeName='+employeename;

      var ReportDate = moment().format(process.env.DATE_FORMAT);
      var paraReportDate = '&reportDate='+ReportDate;
      
      //ExportFilter
      let ExportFilterArray = {
        FilterPayPeriod: FilterPayPeriod,
        payperiodname : payperiodname,
        FilterLocation: FilterLocation,
        locationname: locationname,
        FilterServiceCode: FilterServiceCode,
        servicename: servicename,
        FilterEmploymentType: FilterEmploymentType,
        FilterStartDate: FilterStartDate,
        FilterEndDate: FilterEndDate,
        FilterEmployeeStatus: FilterEmployeeStatus,
        FilterEmployee: FilterEmployee,
        employeename: employeename,
        ReportDate: ReportDate
      };
      this.setState({ ExportFilter: ExportFilterArray });
      //ExportFilter

      var pass_url = 'GetTimesheetReportDataJson?'+paraPayPeriod+paraLocation+paraServiceCode+paraEmploymentType+paraStartDate+paraEndDate+paraEmployeeStatus+paraContactId+paraReportDate;
      console.log(pass_url);
      //return false;

      this.showLoader();
      var url=process.env.API_API_URL+pass_url;
      fetch(url, {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'bearer '+localStorage.getItem("token")
        },
      }).then((response) => response.json())
      .then(data => {
          console.log("responseJson GetTimesheetReportDataJson");
          console.log(data);
          if (data.responseType === "1") {
              if(data.data.exportTimesheetReportLocationView != null)
              {
                this.setState({ ListGrid: this.rowData(data.data.exportTimesheetReportLocationView) });
              }
              
              
              this.hideLoader();
          }else{
            if(data.message == 'Authorization has been denied for this request.'){
              SystemHelpers.SessionOut();
              this.props.history.push("/login");
            }else{
              SystemHelpers.ToastError(data.message);
            }
            this.hideLoader();    
          }
          
          
      })
      .catch(error => {
        this.props.history.push("/error-500");
      });
  }

  ExportReportData = (fileType) => e => {

      e.preventDefault();
      
      /* Role Management */
      var getrole = SystemHelpers.GetRole();
      let canDelete = getrole.employees_can.employees_can_delete;
      /* Role Management */

      var FilterPara = this.state.ExportFilter;
      console.log('payload GetTimesheetReportDataExport');
      console.log(FilterPara);
      //console.log(FilterPara.FilterPayPeriod);
      //return false;
      
      var paraPayPeriod = 'periodProfileId='+FilterPara.FilterPayPeriod+'&periodProfileName='+FilterPara.payperiodname;
      
      var paraLocation = '&locationId='+FilterPara.FilterLocation+'&locationName='+FilterPara.locationname;
      
      var paraServiceCode = '&serviceCodeId='+FilterPara.FilterServiceCode+'&codeOfService='+FilterPara.servicename;

      var paraEmploymentType = '&employeementType='+FilterPara.FilterEmploymentType;
      var paraStartDate = '&startDate='+FilterPara.FilterStartDate;
      var paraEndDate = '&endDate='+FilterPara.FilterEndDate;
      
      var paraEmployeeStatus = '&employeeStatus='+FilterPara.FilterEmployeeStatus;

      var paraContactId = '&contactId='+FilterPara.FilterEmployee+'&employeeName='+FilterPara.employeename;
      
      var exportType = '&exportType='+fileType;

      var paraReportDate = '&reportDate='+FilterPara.ReportDate;


      var pass_url = 'GetTimesheetReportDataExport?'+paraPayPeriod+paraLocation+paraServiceCode+paraEmploymentType+paraStartDate+paraEndDate+paraEmployeeStatus+paraContactId+exportType+paraReportDate;
      console.log(pass_url);
      //return false;

      this.showLoader();
      var url=process.env.API_API_URL+pass_url;
      fetch(url, {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'bearer '+localStorage.getItem("token")
        },
      }).then((response) => response.json())
      .then(data => {
          console.log("responseJson GetTimesheetReportDataExport");
          console.log(data);
          if (data.responseType === "1") {

              if(data.data != null){
                
                var today = new Date();
                var y = today.getFullYear();
                var m = today.getMonth() + 1;
                var d = today.getDate();
                var h = today.getHours();
                var mi = today.getMinutes();
                var s = today.getSeconds();
                var ms = today.getMilliseconds();
                var time = "PHSS000"+y  + m  + d  + h  + mi  + s + ms+FileUploadHelper.randomFun();

                var ext='';
                if(fileType == "PDF"){
                  var createBase64 = "data:application/pdf;base64,"+data.data;
                  ext=time+'.pdf';
                }else if(fileType == "EXCEL"){
                  var createBase64 = "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,"+data.data;
                  ext=time+'.xlsx';
                }
                
                const linkSource = createBase64;
                const downloadLink = document.createElement("a");
                const fileName = ext;

                downloadLink.href = linkSource;
                downloadLink.download = fileName;
                downloadLink.click();

              }
              
              this.hideLoader();
          }else{
            if(data.message == 'Authorization has been denied for this request.'){
              SystemHelpers.SessionOut();
              this.props.history.push("/login");
            }else{
              SystemHelpers.ToastError(data.message);
            }
            this.hideLoader();    
          }
          
          
      })
      .catch(error => {
        this.props.history.push("/error-500");
      });
  }

  rowData(ListGrid) {
    //console.log('rowData GetTimesheetReportDataJson');
    //console.log(ListGrid);

    /* Role Management */
    var getrole = SystemHelpers.GetRole();
    let canDelete = getrole.employment_history_can.employment_history_can_delete;
    /* Role Management */
    
    var ListGrid_length = ListGrid.length;
    let dataArray = [];
    

    for (var z = 0; z < ListGrid_length; z++)
    {
      
      
      var exportTimesheetReport = ListGrid[z].exportTimesheetReportDataViews;
      if(exportTimesheetReport != null)
      {
        for (var x = 0; x < exportTimesheetReport.length; x++)
        {
          var tempdataArray = [];

          tempdataArray.codelocation = exportTimesheetReport[x].locationCode;
          tempdataArray.location = exportTimesheetReport[x].locationName;
          tempdataArray.employeename = exportTimesheetReport[x].employeeName;
          tempdataArray.servicename = exportTimesheetReport[x].serviceName;
          tempdataArray.employmenttype = exportTimesheetReport[x].employeementType;
          tempdataArray.hoursbyperiod = exportTimesheetReport[x].hoursByPeriod;
          tempdataArray.YTDHours = exportTimesheetReport[x].ytdHours;

          dataArray.push(tempdataArray);
        }
      }
    
    }
    //console.log('Return rowdata array');
    //console.log(dataArray);
    return dataArray;
  }

  handleChangeSelectLocation = (selectedOptionLocation) => {
    this.setState({ selectedOptionLocation });
    //console.log(`Option selected:`, selectedOptionLocation.value);
    this.setState({ FilterLocation: selectedOptionLocation.value });

    if(selectedOptionLocation.label != 'All'){
      this.setState({ locationName: selectedOptionLocation.label });
    }else{
      this.setState({ locationName: '' });
    }
    

    //LocationFilterID
  };

  handleChangeSelectEmployees = (selectedOptionEmployees) => {
    this.setState({ selectedOptionEmployees });
    //console.log(`Option selected:`, selectedOptionLocation.value);
    this.setState({ FilterEmployee: selectedOptionEmployees.value });

    if(selectedOptionEmployees.label != 'All'){
      this.setState({ employeeName: selectedOptionEmployees.label });
    }else{
      this.setState({ employeeName: '' });
    }
    //LocationFilterID
  };

  render() {

    const data = {
      columns: this.state.header_data,
      rows: this.state.ListGrid
    };

    const { selectedOptionLocation,selectedOptionEmployees  } = this.state;

    return ( 
      <div className="main-wrapper">
     
        {/* Toast & Loder method use */}
          
        {(this.state.loading) ? <Loader /> : null} 
        {/* Toast & Loder method use */}
        <Header/>

        <div className="page-wrapper">
            <Helmet>
                <title>{process.env.WEB_TITLE}</title>
                <meta name="description" content="Login page"/>         
            </Helmet>
              {/* Page Content */}
              <div className="content container-fluid">
                {/* Page Header */}
                <div className="page-header">
                  <div className="row align-items-center">
                    <div className="col">
                      <h3 className="page-title">Reports</h3>
                      <ul className="breadcrumb">
                        <li className="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                        <li className="breadcrumb-item active">YTD Hours Reports</li>
                      </ul>
                    </div>
                    <div className="col-auto float-right ml-auto">
                      
                    </div>
                  </div>
                </div>

                {/* Search Filter */}
                  <div className="row filter-row">
                    
                    <div className="col-lg-3 col-sm-4 col-xs-12"> 
                      <div className="form-group form-focus select-focus">
                        <select className="form-control floating" id="FilterPayPeriod" value={this.state.FilterPayPeriod} onChange={this.handleChange('FilterPayPeriod')}> 
                          <option value="" data-payperiodname="" >-</option>
                          {this.state.payPeriodListFilter.map(( listValue, index ) => {
                              var payPeriodName = moment(listValue.timeSheetPeriodStartDate,process.env.API_DATE_FORMAT).format(process.env.DATE_FORMAT)+ " to " +moment(listValue.timeSheetPeriodEndDate,process.env.API_DATE_FORMAT).format(process.env.DATE_FORMAT);
                              return (
                                <option key={index} value={listValue.timeSheetPeriodId} data-payperiodname={payPeriodName} >{moment(listValue.timeSheetPeriodStartDate,process.env.API_DATE_FORMAT).format(process.env.DATE_FORMAT)} to {moment(listValue.timeSheetPeriodEndDate,process.env.API_DATE_FORMAT).format(process.env.DATE_FORMAT)}</option>
                              );
                           
                          })}
                        </select>
                        <label className="focus-label">Pay period</label>
                      </div>
                    </div>
                    <div className="col-lg-3 col-sm-4 col-xs-12"> 
                      <div className="form-group form-focus select-focus">
                        {/*<select className="form-control floating" id="FilterLocation" value={this.state.FilterLocation} onChange={this.handleChange('FilterLocation')}> 
                          <option value="" data-locationname="">All</option>
                          {this.state.locationListFilter.map(( listValue, index ) => {
                           
                              return (
                                <option key={index} value={listValue.locationId} data-locationname={listValue.locationName} >{listValue.locationName}</option>
                              );
                           
                          })}
                        </select>*/}
                        <Select
                          value={selectedOptionLocation}
                          onChange={this.handleChangeSelectLocation}
                          options={this.state.LocationListFilter}
                          defaultValue={""}
                          isSearchable={true}
                          placeholder="Search Location"
                        />
                        {/*<label className="focus-label">Location</label>*/}
                      </div>
                    </div>
                    <div className="col-lg-3 col-sm-4 col-xs-12"> 
                      <div className="form-group form-focus select-focus">
                        <select className="form-control floating" id="FilterServiceCode" value={this.state.FilterServiceCode} onChange={this.handleChange('FilterServiceCode')}> 
                          <option value="" data-servicename="" >All</option>
                          {this.state.serviceCodeListFilter.map(( listValue, index ) => {
                           
                              return (
                                <option key={index} value={listValue.serviceId} data-servicename={listValue.serviceName} >{listValue.serviceName}  (  {listValue.serviceCode} )</option>
                              );
                           
                          })}
                        </select>
                        <label className="focus-label">Service name</label>
                      </div>
                    </div>
                    <div className="col-lg-3 col-sm-4 col-xs-12"> 
                      <div className="form-group form-focus select-focus">
                        <select className="form-control floating" id="FilterEmploymentType" value={this.state.FilterEmploymentType} onChange={this.handleChange('FilterEmploymentType')}> 
                          <option value="">All</option>
                          {this.state.employmentTypeListFilter.map(( listValue, index ) => {
                           
                              return (
                                <option key={index} value={listValue.name} >{listValue.name}</option>
                              );
                           
                          })}
                        </select>
                        <label className="focus-label">Employment Type</label>
                      </div>
                    </div>

                    
                    <div className="col-lg-3 col-sm-4 col-xs-12"> 
                      <div className="form-group form-focus select-focus">
                        {/*<input className="form-control" type="text" id="FilterEmployeeName" value={this.state.FilterEmployeeName}  onChange={this.handleChange('FilterEmployeeName')} />*/}
                        {/*<select className="form-control" id="FilterEmployee" value={this.state.FilterEmployee}  onChange={this.handleChange('FilterEmployee')} >
                          <option value='' data-employeename="" >-</option>
                          {this.state.EmployeeNameListFilter.map(( listValue, index ) => {
                            return (
                              <option key={index}  value={listValue.contactId} data-employeename={listValue.employeeName} >{listValue.employeeName}</option>
                            );
                          })}
                        </select>
                        <label className="focus-label">Employee name</label>*/}
                        <Select
                          value={selectedOptionEmployees}
                          onChange={this.handleChangeSelectEmployees}
                          options={this.state.EmployeeNameListFilter}
                          defaultValue={""}
                          isSearchable={true}
                          placeholder="Search Employee"
                        />
                      </div>
                    </div>
                    
                    <div className="col-lg-3 col-sm-4 col-xs-12"> 
                      <div className="form-group form-focus select-focus">
                        <select className="form-control" id="FilterEmployeeStatus" value={this.state.FilterEmployeeStatus}  onChange={this.handleChange('FilterEmployeeStatus')} >
                          <option value=''>-</option>
                          {this.state.employeeStatusListFilter.map(( listValue, index ) => {
                              return (
                                <option key={index} value={listValue.name}>{listValue.name}</option>
                              );
                          })}
                        </select>
                        <label className="focus-label">Employee status</label>
                      </div>
                    </div>

                    <div className="col-lg-2 col-sm-4 col-xs-12"> 
                      <div className="form-group form-focus select-focus">
                        <input className="form-control" type="date" id="FilterStartDate" value={this.state.FilterStartDate}  onChange={this.handleChange('FilterStartDate')} />
                        <label className="focus-label">Start date</label>
                      </div>
                    </div>
                    
                    <div className="col-lg-2 col-sm-4 col-xs-12"> 
                      <div className="form-group form-focus select-focus">
                        <input className="form-control" type="date" id="FilterEndDate" value={this.state.FilterEndDate}  onChange={this.handleChange('FilterEndDate')} min={moment().format(this.state.FilterStartDate,"YYYY-MM-DD")} />
                        <label className="focus-label">End date</label>
                      </div>
                    </div>



                    <div className="col-lg-2 col-sm-4 col-xs-12">  
                      <a href="#" className="btn btn-success btn-block" onClick={this.GetReportListGrid()}> Search </a>  
                    </div> 

                    

                  </div>
                {/* /Search Filter */}    
                {/* /Page Header */}

                {this.state.ListGrid.length > 0 ?
                  <div className="row">
                      <div className="col-sm-12">  
                        <a href="#" className="btn btn-danger mr-1 float-right" onClick={this.ExportReportData('EXCEL')}> Export EXCEL </a>  
                        <a href="#" className="btn btn-danger mr-1 float-right" onClick={this.ExportReportData('PDF')}> Export PDF </a>  
                      </div>
                  </div>
                  : null
                }
                <div className="row">
                  <div className="col-md-12">
                      <div className="table-responsive">
                      
               
                        {this.state.ListGrid.length > 0 ?
                          <MDBDataTable
                            striped
                            bordered
                            small
                            data={data}
                            entriesOptions={[10, 20, 50, 100]} entries={10} pagesAmount={10}
                            className="table table-striped custom-table mb-0 datatable"
                          />
                          : null
                        }
                     

                    </div>
                  </div>
                </div>
              </div>
              {/* /Page Content */}

              {/* Add Reports Modal */}
              {/* /Add Reports Modal */}

              {/* Edit Reports Modal */}
              {/* /Edit Reports Modal */}

              {/* Delete Today Work Modal */}
              {/* Delete Today Work Modal */}
            </div>
          <SidebarContent/>
      </div>
        );
      
   }
}

export default Reports;
