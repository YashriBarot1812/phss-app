
import React, { Component } from 'react';
import { Helmet } from "react-helmet";
import {  Avatar_01 ,Avatar_02,Avatar_03,Avatar_04, Avatar_05, Avatar_08, Avatar_09, Avatar_10,
    Avatar_11,Avatar_12,Avatar_13,Avatar_16   } from "../../Entryfile/imagepath"

import { Table } from 'antd';
import 'antd/dist/antd.css';
import {itemRender,onShowSizeChange} from "./pagination/paginationfunction"
import "../MainPage/antdstyle.css"
import { Multiselect } from 'multiselect-react-dropdown';

import Select from 'react-select-me';
import 'react-select-me/lib/ReactSelectMe.css';

import Header from '../Sidebar/header.jsx';
import SidebarContent from '../Sidebar/sidebar';

import Loader from '../Loader';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import SystemHelpers from '../Helpers/SystemHelper';


import CryptoAES from 'crypto-js/aes';
import CryptoENC from 'crypto-js/enc-utf8';

//import SelectSearch from 'react-select-search';

//import '../../assets/css/serach-dropwn.css';

import SelectDropdown from 'react-dropdown-select';

class Employees extends Component {
  constructor(props) {
    super(props);
    this.state = {

        // Pagination
        totalCount : 0,
        pageSize : 50,
        currentPage : 1,
        totalPages : 0,
        previousPage : false,
        nextPage : false,
        searchText : '',
        pagingData : {},
        TempsearchText:'',
        display_Inactive:false,
        // Pagination
        
        locationID:localStorage.getItem("primaryLocationId"),

       errormsg :  '',
       userTypeList : [],
       roleList : [],
       userName : '',
       userEmail : '',
       userType : [],         
       userRole : [],
       firstName : '',
       lastName : '',
       ListGrid: [],
       location_insert:[],
       location:'',
       AddPrimaryLocation:'',
       AddApproverTo:'',
       AddReportTo:'',
       locationList:[{name: 'Srigar', id: 1},{name: 'Sam', id: 2}],
       FLocationList:[],
       
      GetCRMUserList:[],

      user_can_create : false,
      user_can_update : false,
      user_can_view : false,
      user_can_delete : false,
      user_can_approve : false,
      user_can_export : false,

      IsUserNameExists : false,
      IsUserEmailExists : false,

      options : [
        { value: 1, label: 'Label 1' },
        { value: 2, label: 'Label 2' },
      ],

      isUserTypeStaffSelectAdd :false,
      isUserTypeStaffSelectEdit :false,

      UserId : '',
      //isDelete : false,
      isDelete : 0,
      isDeleteDisplay : '',

      role_employees_can : {},

      sortColumn : '',
      SortType : false,
      IsSortingEnabled : true,
      sortRoleId:'',

      AddpartTimeType :  '',
      employeementHourList : [],

      options : [
        {name: 'Swedish', value: 'sv'},
        {name: 'English', value: 'en'}
      ],

      SelectFLocationList : [],

      hierarchyId:'',
      page_wrapper_cls:'',
      unauthorized_cls:'',

    };
    this.setPropState = this.setPropState.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.onSelectRole = this.onSelectRole.bind(this);
    this.onRemoveRole = this.onRemoveRole.bind(this);
    this.onSelectUserType = this.onSelectUserType.bind(this);
    this.onRemoveUserType = this.onRemoveUserType.bind(this);
    this.onSelectLocation = this.onSelectLocation.bind(this);
    this.onRemoveLocation = this.onRemoveLocation.bind(this);
    this.onChangeSearch = this.onChangeSearch.bind(this);
    this.userTypeListRef = React.createRef();
    this.roleListRef = React.createRef();
    this.locationListRef = React.createRef();
    
  }

  onChangeSearch(value) {
    console.log(value);
    this.setState({ value });
  }

  setPropState(key, value) {
      this.setState({ [key]: value });
  }

  handleChange = input => e => {
    this.setState({ [input]: e.target.value });
    console.log('handleChange');
    console.log(input);

    if (this.state[input] != '') {
        delete this.state.errormsg[input];
    }

    if( [input] == 'display_Inactive'){

      if(this.state.display_Inactive == true){
        this.setState({ display_Inactive: false });
      }else{
        this.setState({ display_Inactive: true });
      }
    }

    if([input]=="userName")
    {
      /*if(this.state.userName !="")
      {*/
        this.UserNameExistsCheck();
        //}
      
    }

    if([input]=="userEmail")
    {
      //alert();
      this.UserEmailExistsCheck();
    }

    if([input]=="pageSize")
    {
      this.setState({ current_page: 1 });
      this.GetCRMRegisterUser(1,e.target.value,this.state.searchText)
    }

    
    
  }

  // Loader hide show method
  hideLoader = () => {
    this.setState({ loading: false });
  }

  showLoader = () => {
    this.setState({ loading: true });
  }
  // Loader hide show method

  

  onSelectRole(selectedList, selectedItem) {
    let lang =[];
    for (var i = 0; i<selectedList.length; i++) {
      var number1 = selectedList[i].id;
      var number =  number1.toString();
      lang.push(number);
    }
    this.setState({ userRole: lang });

    delete this.state.errormsg['userRole'];
  }

  onRemoveRole(selectedList, removedItem) {
      let lang =[];
      for (var i = 0; i<selectedList.length; i++) {
        var number1 = selectedList[i].id;
        var number =  number1.toString();
        lang.push(number);
      }
      if(lang.length != 0){
        this.setState({ userRole: lang });
      }

      delete this.state.errormsg['userRole'];
  }

  onSelectUserType(selectedList, selectedItem) {
    let lang =[];

    $("#AddReportToDiv").hide();
    $("#AddApproverToDiv").hide();
    this.setState({ isUserTypeStaffSelectAdd: false });

    for (var i = 0; i<selectedList.length; i++) {
      var number1 = selectedList[i].id;
      var number =  number1.toString();
      lang.push(number);
      console.log('user type select ');
      console.log(selectedList);
      if(selectedList[i].name=="Staff")
      {
        $("#AddReportToDiv").show();
        $("#AddApproverToDiv").show();
        this.setState({ isUserTypeStaffSelectAdd: true });
      }

    }
    this.setState({ userType: lang });
    delete this.state.errormsg['userType'];
  }

  onRemoveUserType(selectedList, removedItem) {
      let lang =[];
      $("#AddReportToDiv").hide();
      $("#AddApproverToDiv").hide();
      this.setState({ isUserTypeStaffSelectAdd: false });
      for (var i = 0; i<selectedList.length; i++) {
        var number1 = selectedList[i].id;
        var number =  number1.toString();
        lang.push(number);

        if(selectedList[i].name=="Staff")
        {
          $("#AddReportToDiv").show();
          $("#AddApproverToDiv").show();
          this.setState({ isUserTypeStaffSelectAdd: true });
        }
      }
      if(lang.length != 0){
        this.setState({ userType: lang });
      }
      delete this.state.errormsg['userType'];
  }


  onSelectLocation(selectedList, selectedItem) {
    let lang =[];
    for (var i = 0; i<selectedList.length; i++) {
      var number1 = selectedList[i].id;
      var number =  number1.toString();
      lang.push(number);
    }
    this.setState({ location_insert: lang });
    delete this.state.errormsg['location'];
  }

  onRemoveLocation(selectedList, removedItem) {
      let lang =[];
      for (var i = 0; i<selectedList.length; i++) {
        var number1 = selectedList[i].id;
        var number =  number1.toString();
        lang.push(number);
      }
      if(lang.length != 0){
        this.setState({ location_insert: lang });
      }
      delete this.state.errormsg['location'];
  }

  componentDidMount() {
    console.log("Employees");

    /* Role Management */
    /*var pwd = localStorage.getItem("contactId")+"Phss@123";
    var Role_session = localStorage.getItem('sessiontoken');

    var _ciphertext = CryptoAES.decrypt(Role_session, pwd);
    console.log('Role Store employees_can');
    //console.log(_ciphertext.toString(CryptoENC));
    var JsonCreate = JSON.parse(_ciphertext.toString(CryptoENC));
    this.setState({ role_employees_can: JsonCreate.employees_can });
    
    console.log(JsonCreate.employees_can);*/
    /* Role Management */

    /* Role Management */
     console.log('Role Store employees_can');
     var getrole = SystemHelpers.GetRole();
     let employees_can = getrole.employees_can;
     this.setState({ role_employees_can: employees_can });
     console.log(employees_can);
    /* Role Management */

    /* Role Management */
    let hierarchyId = getrole.hierarchy_can.hierarchy_can_id;
    this.setState({ hierarchyId: hierarchyId });

    if(hierarchyId == 1){
      this.setState({ page_wrapper_cls: 'page-wrapper pk-page-wrapper-remove hide-div' });
      this.setState({ unauthorized_cls: 'error-box pk-margin-top' });

      setTimeout(() => {
        $('.pk-page-wrapper-remove').remove()
      }, 5000);
      
    }else{
      this.setState({ page_wrapper_cls: 'page-wrapper' });
      this.setState({ unauthorized_cls: 'error-box hide-div' });
    }
    /* Role Management */

    
    console.log(localStorage.getItem("token"));
    this.GetUserRegistrationDetail();
    //this.GetCRMRegisterUser();
    
    this.GetCRMRegisterUser(this.state.currentPage,this.state.pageSize,this.state.searchText);
   
    
    this.GetCRMUserList();
    //this.GetUserRoleDetails();
    $("#AddReportToDiv").hide();
    $("#AddApproverToDiv").hide();
    
  }

  location_list_fun(location_list){
    let lang =[];
    
    for (var i = 0; i<location_list.length; i++) {
      let temp ={};
      //console.log(location_list[i].locationName);
       temp['id']=location_list[i].locationId;
       temp['name']=location_list[i].locationName;
       lang.push(temp);
    }
    if(lang.length != 0){
      //console.log(lang);
      return lang;
      //this.setState({ locationList: lang });
    }
  }

  GetUserRegistrationDetail(){
    //this.showLoader();
    /* Role Management */
    var getrole = SystemHelpers.GetRole();
    //console.log('employee GetUserRegistrationDetail can role');
    //console.log(getrole.employees_can);
    //let viewall = getrole.employees_can.employees_can_viewall;
    let hierarchyId = getrole.hierarchy_can.hierarchy_can_id;
    //alert(viewall);
    /* Role Management */
      var url=process.env.API_API_URL+'GetUserRegistrationDetail?contactId='+localStorage.getItem("contactId")+'&rolePriorityId='+hierarchyId;
      //var url=process.env.API_API_URL+'GetUserRegistrationDetail?contactId='+localStorage.getItem("contactId")+'&canViewAll=true';
      fetch(url, {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'bearer '+localStorage.getItem("token")
        },
        //body: JSON.stringify(bodyarray)
      }).then((response) => response.json())
      .then(data => {
          console.log("responseJson GetUserRegistrationDetail");
          console.log(data);
          //console.log(data.data.userRole);
          // debugger;
          if (data.responseType === "1") {
              // Profile & Contact
              if(data.data != null){
                //this.setState({ ListGrid: data.data.userEmergencyContactViews });
                this.setState({ employeementHourList: data.data.employementHours });
                this.setState({ userTypeList: data.data.userTypeList });
                this.setState({ roleList: data.data.roleList });

                
                

                if(data.data.locationList != "" && data.data.locationList != null)
                {
                  this.setState({ FLocationList: data.data.locationList });
                  this.setState({ SelectFLocationList: this.FuncFLocationList(data.data.locationList) });
                  console.log('1234567890');
                  //console.log(this.FuncFLocationList(data.data.locationList));
                  console.log(data.data.locationList);

                  var location_list=data.data.locationList;
                  this.setState({ locationList: this.location_list_fun(location_list) });
                }
                 
              }
              
              
          }else{
                if(data.message == 'Authorization has been denied for this request.'){
                  SystemHelpers.SessionOut();
                  this.props.history.push("/login");
                }else{
                  SystemHelpers.ToastError(data.message);
                }
                
          }
          //this.hideLoader();
          
      })
      .catch(error => {
        this.props.history.push("/error-500");
      });
  }

  FuncFLocationList(Location)
  {
    console.log('fun FuncFLocationList');
    //console.log(Location);
    var Location_lenght = Location.length;
    let dataArray = [];
    var i=1;
    if(Location_lenght > 0)
    {
      for (var z = 0; z < Location_lenght; z++) {
        var tempdataArray = [];
        //SelectFLocationList
        if(Location[z].locationId == localStorage.getItem("primaryLocationId"))
        {
          //tempdataArray.name = Location[z].locationName;
          //tempdataArray.id = Location[z].locationId;

          dataArray.push(Location[z]);
        }
        z++;
      }
    }
    console.log(dataArray);
    return dataArray;
    
  }

  FilterLocationSelect (location){
    
    console.log('FilterLocationSelect');
    console.log(location);
    if(location.length > 0)
    {
      this.setState({ AddPrimaryLocation: location[0].locationName })
    }
  }

  resetValues() {
    // By calling the belowe method will reset the selected values programatically
    this.userTypeListRef.current.resetSelectedValues();
    this.roleListRef.current.resetSelectedValues();
    this.locationListRef.current.resetSelectedValues();
  }
  ClearRecord = ()=> e => {
    e.preventDefault();

    this.setState({ userName: '' });
    this.setState({ userEmail: '' });
    this.setState({ userType: [] });
    this.setState({ userRole: [] });
    this.setState({ firstName: '' });
    this.setState({ lastName: '' });
    this.setState({ location_insert: [] });
    this.setState({ AddPrimaryLocation: '' });
    this.setState({ AddApproverTo: '' });
    this.setState({ AddReportTo: '' });
    this.setState({ AddpartTimeType: '' });

    this.resetValues();

    
    this.setState({ errormsg: '' });
  }

  AddRecord = () => e => {
      //debugger;
      e.preventDefault();
      //$("#ConfirmEmailExistsAdd_emergency_contact").trigger("click");
      //alert(this.state["AddPrimaryLocation"]);
      //return false;
      //return false;
      let step1Errors = {};
      
      /*if(this.state.IsUserNameExists == true)
      {
        step1Errors["userName"] = "UserName already exists";
        this.setState({ errormsg: step1Errors });
      }*/
      if(this.state.IsUserEmailExists == true)
      {
        step1Errors["userEmail"] = "Email address already exists and linked with another staff account";
        this.setState({ errormsg: step1Errors });
      }


      /*var usernm =this.state["userName"];
      if (this.state["userName"] == '' || this.state["userName"] == null) {
        step1Errors["userName"] = "User Name is mandatory";
      }else if(usernm.match(/\s/g)){
        step1Errors["userName"] = "space not allowed in User Name";
      }else if(usernm.length < 8){
        step1Errors["userName"] = "Minimum 8 Characters";
      }*/
      
       
      if (this.state["userType"] == '' || this.state["userType"] == null) {
        step1Errors["userType"] = "User Type is mandatory.";
      }

      if (this.state["userRole"] == "" || this.state["userRole"] == null) {
        step1Errors["userRole"] = "Role is mandatory";
      }

      if (this.state["firstName"] == "" || this.state["firstName"] == null) {
        step1Errors["firstName"] = "First Name is mandatory";
      }

      if (this.state["lastName"] == "" || this.state["lastName"] == null) {
        step1Errors["lastName"] = "Last Name is mandatory";
      }

      if (this.state["location_insert"] == "") {
        step1Errors["location"] = "Location is mandatory";
      }

      if (this.state["AddPrimaryLocation"] == "") {
        step1Errors["AddPrimaryLocation"] = "Primary Location is mandatory";
      }

      // if(this.state.isUserTypeStaffSelectAdd == true)
      // {
      //   if (this.state["AddApproverTo"] == "") {
      //     step1Errors["AddApproverTo"] = "AddApproverTover To is mandatory";
      //   }

      //   if (this.state["AddReportTo"] == "") {
      //     step1Errors["AddReportTo"] = "Report To is mandatory";
      //   }
      // }
      
      if (this.state["userEmail"] == '' || this.state["userEmail"] == null) {
          step1Errors["userEmail"] = "Email is mandatory";
      }else if (typeof this.state["userEmail"] !== "undefined") {
          let lastAtPos = this.state["userEmail"].lastIndexOf('@');
          let lastDotPos = this.state["userEmail"].lastIndexOf('.');

          if (!(lastAtPos < lastDotPos && lastAtPos > 0 && this.state["userEmail"].indexOf('@@') == -1 && lastDotPos > 2 && (this.state["userEmail"].length - lastDotPos) > 2)) {
              step1Errors["userEmail"] = "Email is not valid";
          }
      }

      if (this.state["AddpartTimeType"] == "") {
        step1Errors["AddpartTimeType"] = "Employment Hours is mandatory";
      }

      console.log(step1Errors);

      this.setState({ errormsg: step1Errors });
          
      if (Object.keys(step1Errors).length > 0) {
          return false;
      }


      this.showLoader();

      
       
      let bodyarray = {};
      bodyarray["userName"] = this.state["userName"];
      bodyarray["userEmail"] = this.state["userEmail"];
      bodyarray["userType"] = this.state["userType"];
      bodyarray["userRole"] = this.state["userRole"];
      bodyarray["firstName"] = this.state["firstName"];
      bodyarray["lastName"] = this.state["lastName"];
      bodyarray["location"] = this.state["location_insert"];
      bodyarray["primaryLocationId"] = this.state["AddPrimaryLocation"];
      bodyarray["timeSheetApproverId"] = this.state["AddApproverTo"];
      bodyarray["timeSheetReportId"] = this.state["AddReportTo"];
      bodyarray["noOfHours"] = this.state["AddpartTimeType"];

      console.log(bodyarray);
      //return false;
      var url=process.env.API_API_URL+'CreateUserRegistration';
      fetch(url, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'bearer '+localStorage.getItem("token")
        },
        body: JSON.stringify(bodyarray)
      }).then((response) => response.json())
      .then(data => {
          console.log("responseJson CreateUserRegistration");
          console.log(data);
          //console.log(responseJson);
          // debugger;
          if (data.responseType === "1") {
              //this.props.history.push('/dashboard');
              SystemHelpers.ToastSuccess(data.responseMessge);  
              $( ".close" ).trigger( "click" ); 
              //this.GetUserRegistrationDetail();
              this.GetCRMRegisterUser(this.state.currentPage,this.state.pageSize,this.state.searchText);

              this.setState({ userName: '' });
              this.setState({ userEmail: '' });
              this.setState({ userType: [] });
              this.setState({ userRole: [] });
              this.setState({ firstName: '' });
              this.setState({ lastName: '' });
              this.setState({ location_insert: [] });
              this.setState({ AddPrimaryLocation: '' });
              this.setState({ AddApproverTo: '' });
              this.setState({ AddReportTo: '' });
              this.setState({ AddpartTimeType: '' });
              this.resetValues();

          }else if (data.responseType === "2" || data.responseType === "3" || data.responseType === "4") {
              //this.props.history.push('/dashboard');
              SystemHelpers.ToastError(data.responseMessge);  
              //this.GetUserRegistrationDetail();
          }
          else{
              SystemHelpers.ToastError(data.message);
          }
          this.hideLoader();
          
      })
      .catch(error => {
        this.props.history.push("/error-500");
      });
  }

  GetCRMRegisterUser(currentPage,pageSize,searchText){

      //alert(this.state.display_Inactive);
      this.setState({ ListGrid : [] });

      let bodyarray = {};
      bodyarray["currentPage"] = 1;
      bodyarray["nextPage"] = false;
      bodyarray["pageSize"] = 5;
      bodyarray["previousPage"] = false;
      bodyarray["totalCount"] = 0;
      bodyarray["totalPages"] = 0;
      
      this.setState({ pagingData : bodyarray });
      /*totalCount : 0,
        pageSize : 10,
        currentPage : 1,
        totalPages : 0,
        previousPage : false,
        nextPage : false,
        searchText*/
      /*console.log("responseJson GetCRMRegisterUser123");
      console.log(currentPage);
      console.log(pageSize);
      console.log(searchText); */
      //alert(this.props.employees_can_delete);
      //let canDelete = false

      //if(this.props.employees_can_delete == true){
        /* Role Management */
      var getrole = SystemHelpers.GetRole();
      let canDelete = getrole.employees_can.employees_can_delete;
      //let canViewall = getrole.employees_can.employees_can_viewall
      let hierarchyId = getrole.hierarchy_can.hierarchy_can_id;
      console.log('employee getrole');
      console.log(getrole.employees_can.employees_can_viewall);
      console.log(getrole);
        //let canDelete = this.state.role_employees_can.employees_can_delete;
      //}


      this.setState({ currentPage: currentPage });
      this.setState({ pageSize: pageSize });

      var sort_Column = this.state.sortColumn;
      var Sort_Type = this.state.SortType;
      var roleId = this.state.sortRoleId;
      var locationID = this.state.locationID;

      var IsSortingEnabled = true;
      

      var pass_url = 'GetCRMRegisterUser?pageNumber='+currentPage+'&pageSize='+pageSize+'&searchText='+searchText+'&canDelete='+canDelete+'&sortColumn='+sort_Column+'&SortType='+Sort_Type+'&IsSortingEnabled='+IsSortingEnabled+'&roleId='+roleId+'&rolePriorityId='+hierarchyId+"&locationId="+locationID+"&contactId="+localStorage.getItem("contactId")+"&displayInActiveEmployee="+this.state.display_Inactive;


      this.showLoader();
      var url=process.env.API_API_URL+pass_url;
      fetch(url, {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'bearer '+localStorage.getItem("token")
        },
      }).then((response) => response.json())
      .then(data => {
          console.log("responseJson GetCRMRegisterUser");
          console.log(data);
          if (data.responseType === "1") {
              this.setState({ ListGrid: data.data });
              this.setState({ pagingData: data.pagingData });

              //this.setState({ IsSortingEnabled: true });

              this.hideLoader();
          }else{
            if(data.message == 'Authorization has been denied for this request.'){
              SystemHelpers.SessionOut();
              this.props.history.push("/login");
            }else{
              SystemHelpers.ToastError(data.message);
            }
            this.hideLoader();    
          }
          
          
      })
      .catch(error => {
        this.props.history.push("/error-500");
      });
  }



  GetCRMUserList(){
      //this.showLoader();
      var url=process.env.API_API_URL+'GetCRMUserList';
      fetch(url, {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'bearer '+localStorage.getItem("token")
        },
      }).then((response) => response.json())
      .then(data => {
          console.log("responseJson GetCRMUserList");
          console.log(data);
          if (data.responseType === "1") {
              this.setState({ GetCRMUserList: data.data });
          }else{
            if(data.message == 'Authorization has been denied for this request.'){
              SystemHelpers.SessionOut();
              this.props.history.push("/login");
            }else{
              SystemHelpers.ToastError(data.message);
            }
                
          }
          //this.hideLoader();
          
      })
      .catch(error => {
        this.props.history.push("/error-500");
      });
  }

  GetUserRoleDetails(){
      
      var url=process.env.API_API_URL+'GetUserRoleDetails?contactId='+localStorage.getItem("contactId");
      fetch(url, {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'bearer '+localStorage.getItem("token")
        },
        //body: JSON.stringify(bodyarray)
      }).then((response) => response.json())
      .then(data => {
          console.log("responseJson GetUserRoleDetails sidebar");
          console.log(data);
          //console.log(data.data.userRole);
          // debugger;
          if (data.responseType === "1") {
              // Profile & Contact
              var row = data.data;
              var row_count = row.length;
              //console.log("row array");
              for (var zz = 0; zz < row_count; zz++) {
                console.log("JsonArrayfunc");
                var JsonArray = row[zz].userRoleString
                var json_replace = JsonArray.replace(/\\|\//g,'')
                //console.log(json_replace);
                var JsonCreate = JSON.parse(json_replace);
                var RoleJson = JsonCreate.Timesheet;
                var UserManagement = JsonCreate.UserManagement;
                console.log('RoleJson');
                console.log(RoleJson);
                this.role_setup("user_can_",UserManagement.User);
                this.role_setup("timesheet_can_",RoleJson.Timesheet);
               
              }
              
              
          }else{
                
                
          }
         
          
      })
      .catch(error => {
        this.props.history.push("/error-500");
      });
  }

  Edit_Update_Btn_Func(record){
      let return_push = [];
      //alert(this.props.employees_can_delete);
      console.log('emp edit button');
      console.log(record);
      if(this.state.role_employees_can.employees_can_update == true || this.state.role_employees_can.employees_can_delete == true){
        let Edit_push = [];
        if(this.state.role_employees_can.employees_can_update == true){
          var url ="/employee-profile/"+record.contactId;
          Edit_push.push(
          <a href={url}  className="dropdown-item"><i className="fa fa-pencil m-r-5" ></i> Edit</a>
          );
          if(record.isLinkExpired == true){
            Edit_push.push(
            <a onClick={this.UserActiveUserLink24hrs(record.contactId)} className="dropdown-item"><i className="fa fa-pencil m-r-5" ></i> Resend Email</a>
            );
          }
          if(record.isUserBlocked == true){
            Edit_push.push(
            <a onClick={this.UserUnBlockUserByAdmin(record.contactId)} className="dropdown-item"><i className="fa fa-pencil m-r-5" ></i> Unblock </a>
            );
          }
        }
        let Delete_push = [];
        if(this.state.role_employees_can.employees_can_delete == true){
          if(record.employementStatus == "Active")
          {
            Delete_push.push(
              <a href="#" onClick={this.DeleteInfo(record,3)}  className="dropdown-item" data-toggle="modal" data-target="#Employee_Delete_modal"><i className="fa fa-lock m-r-5" ></i> Inactive</a>
            );
            Delete_push.push(
              <a href="#" onClick={this.DeleteInfo(record,4)}  className="dropdown-item" data-toggle="modal" data-target="#Employee_Delete_modal"><i className="fa fa-hand-paper-o m-r-5" ></i> On Hold</a>
            );
          }
          else if(record.employementStatus == "Inactive")
          {
            Delete_push.push(
              <a href="#" onClick={this.DeleteInfo(record,2)}  className="dropdown-item" data-toggle="modal" data-target="#Employee_Delete_modal"><i className="fa fa-unlock m-r-5" ></i> Active</a>
            );
            Delete_push.push(
              <a href="#" onClick={this.DeleteInfo(record,4)}  className="dropdown-item" data-toggle="modal" data-target="#Employee_Delete_modal"><i className="fa fa-hand-paper-o m-r-5" ></i> On Hold</a>
            );
          }
          else if(record.employementStatus == "On Hold")
          {
            Delete_push.push(
              <a href="#" onClick={this.DeleteInfo(record,2)}  className="dropdown-item" data-toggle="modal" data-target="#Employee_Delete_modal"><i className="fa fa-unlock m-r-5" ></i> Active</a>
            );
            Delete_push.push(
              <a href="#" onClick={this.DeleteInfo(record,3)}  className="dropdown-item" data-toggle="modal" data-target="#Employee_Delete_modal"><i className="fa fa-lock m-r-5" ></i> Inactive</a>
            );
          }

          Delete_push.push(
            <a href="#" onClick={this.DeleteInfo(record,1)}  className="dropdown-item" data-toggle="modal" data-target="#Employee_Delete_modal"><i className="fa fa-trash m-r-5" ></i> Delete</a>
          ); 
        }
        
        return_push.push(
          <div className="dropdown dropdown-action">
            <a aria-expanded="false" data-toggle="dropdown" className="action-icon dropdown-toggle" href="#"><i className="material-icons">more_vert</i></a>
            <div className="dropdown-menu dropdown-menu-right">
              {Edit_push}
              {Delete_push}
            </div>
          </div>
        );
      }
      return return_push;
  }

  DeleteInfo = (record,deleteStatus) => e =>{
    e.preventDefault();

    this.setState({ UserId: record.contactId });
    //this.setState({ isDelete: record.isDelete });
    this.setState({ isDelete: deleteStatus });

    if(deleteStatus == 1){
      this.setState({ isDeleteDisplay: "Delete" });
    }else if(deleteStatus == 2){
      this.setState({ isDeleteDisplay: "Active" });
    }else if(deleteStatus == 3){
      this.setState({ isDeleteDisplay: "Inactive" });
    }else if(deleteStatus == 4){
      this.setState({ isDeleteDisplay: "On Hold" });
    }
    
  }

  DeleteRecord = () => e => {
    e.preventDefault();
    var isdelete = this.state.isDelete;
    
    /*alert(this.state.isDeleteDisplay);
    alert(isdelete);
    return false;*/
    this.showLoader();
    console.log(this.state.UserId);
    //var url=process.env.API_API_URL+'DeleteUserEmpInfo?contactId='+this.state.UserId+'&isDelete='+isdelete+'&userName='+localStorage.getItem('fullName');
    var url=process.env.API_API_URL+'DeleteUserEmpInfo?contactId='+this.state.UserId+'&deleteStatus='+isdelete+'&userName='+localStorage.getItem('fullName');
    fetch(url, {
      method: 'PUT',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
      //body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
        console.log("responseJson DeleteUserEmpInfo Employee");
        console.log(data);
        //console.log(data.data.userRole);
        // debugger;
        if (data.responseType === "1") {
            // Profile & Contact
            SystemHelpers.ToastSuccess(data.responseMessge);
            $( ".cancel-btn" ).trigger( "click" );
            this.GetCRMRegisterUser(this.state.currentPage,this.state.pageSize,this.state.searchText);
            this.hideLoader();
        }else if (data.responseType == "2" || data.responseType == "3") {
            SystemHelpers.ToastError(data.responseMessge);
            $( ".cancel-btn" ).trigger( "click" );
            this.hideLoader();
        }else{
              if(data.message == 'Authorization has been denied for this request.'){
                SystemHelpers.SessionOut();
                this.props.history.push("/login");
              }else{
                SystemHelpers.ToastError(data.message);
              }
              this.hideLoader();
              $( ".cancel-btn" ).trigger( "click" );
        }
        
        
    })
    .catch(error => {
      this.props.history.push("/error-500");
    });
  }

  role_setup(module_nm,rolejson)
  {
      console.log('rolejson');
      console.log(module_nm);
      console.log(rolejson);

      var  create = module_nm+'create';
      var  update = module_nm+'update';
      var  view = module_nm+'view';
      var  strdelete = module_nm+'delete';
      var  approve = module_nm+'approve';
      var  strexport = module_nm+'export';

      console.log(create +" "+ this.state[create]);
      console.log(update +" "+this.state[update]);
      console.log(view +" "+this.state[view]);
      console.log(strdelete +" "+this.state[strdelete]);
      console.log(approve +" "+this.state[approve]);
      console.log(strexport +" "+this.state[strexport]);


      // this.setState({ create : rolejson.canCreate});
      // this.setState({ update : rolejson.canUpdate});
      // this.setState({ view : rolejson.canView});
      // this.setState({ strdelete : rolejson.canDelete});
      // this.setState({ approve : rolejson.canApprove});
      // this.setState({ strexport : rolejson.canExport});
      //alert(create +" " +this.state[create]);
      if(this.state[create] == false){
        this.setPropState(create, rolejson.canCreate);
      }
      if(this.state[update] == false){
        this.setPropState(update, rolejson.canUpdate);
      }
      
      if(this.state[view] == false){
        this.setPropState(view, rolejson.canView);
      }
      if(this.state[strdelete] == false){
        this.setPropState(strdelete, rolejson.canDelete);
      }
      if(this.state[approve] == false && module_nm == 'timesheet_can_'){
        this.setPropState(approve, rolejson.canApprove);
      }

      if(this.state[strexport] == false && module_nm == 'timesheet_can_'){
        this.setPropState(strexport, rolejson.canExport);
      }

      if(module_nm == 'timesheet_can_'){
        this.setPropState('role_func_call', true);
        //this.props.setPropState('role_func_call', true);
      }
  }


  // Check UserName already exists or not
  UserNameExistsCheck () {

    let step1Errors = {};
    var userName= $('#userName').val();
    //console.log('123'+userName);
    if(userName==""){
        //errormsg
        return false;
    }

    this.setState({ IsUserNameExists: false }); 

    var url=process.env.API_API_URL+'CheckUserNameExitsOrNot?userName='+userName;
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
      //body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
      //console.log("responseJson CheckUserNameExitsOrNot");
      //console.log(data);
      //console.log(responseJson);
      // debugger;
      if (data.responseType === "2") {
        step1Errors["userName"] = "UserName already exists";
        this.setState({ errormsg: step1Errors });
        this.setState({ IsUserNameExists: true }); 
      }
      this.hideLoader();
    })
    .catch(error => {
      this.props.history.push("/error-500");
    });
  }
  // Check UserName already exists or not

  // Check User Email already exists or not
  UserEmailExistsCheck () {

    let step1Errors = {};
    var userEmail= $('#userEmail').val();
    //console.log('123'+userEmail);
    if(userEmail==""){
        //errormsg
        return false;
    }

    //alert(userEmail);
    this.setState({ IsUserEmailExists: false }); 

    var url=process.env.API_API_URL+'CheckEmailExitsOrNot?emailId='+userEmail;
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
      //body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
      console.log("responseJson CheckEmailExitsOrNot");
      console.log(data);
      //console.log(responseJson);
      // debugger;
      if (data.responseType === "2") {
        step1Errors["userEmail"] = "Email address already exists and linked with another staff account";
        this.setState({ errormsg: step1Errors });
        this.setState({ IsUserEmailExists: true }); 
      }
      this.hideLoader();
    })
    .catch(error => {
      this.props.history.push("/error-500");
    });
  }
  // Check User Email already exists or not

  // User UnBlock
  UserUnBlockUserByAdmin  = (contactId) => e => {

    let step1Errors = {};

    this.showLoader();

    var url=process.env.API_API_URL+'UnBlockUserByAdmin?contactId='+contactId;
    fetch(url, {
      method: 'PUT',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
      //body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
      console.log("responseJson UnBlockUserByAdmin");
      console.log(data);
      //console.log(responseJson);
      // debugger;
      if (data.responseType === "1") {
        SystemHelpers.ToastSuccess(data.responseMessge);
      }
      this.hideLoader();
    })
    .catch(error => {
      this.props.history.push("/error-500");
    });
  }
  // User UnBlock

  // Resend Email 24 hrs Expire
  UserActiveUserLink24hrs  = (contactId) => e => {

    let step1Errors = {};

    this.showLoader();

    var url=process.env.API_API_URL+'ActiveUserLink?contactId='+contactId;
    fetch(url, {
      method: 'PUT',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
      //body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
      console.log("responseJson ActiveUserLink");
      console.log(data);
      //console.log(responseJson);
      // debugger;
      if (data.responseType === "1") {
        SystemHelpers.ToastSuccess(data.responseMessge);
      }
      this.hideLoader();
    })
    .catch(error => {
      this.props.history.push("/error-500");
    });
  }
  // Resend Email 24 hrs Expire

  // Pagination Design
  PaginationDesign ()
  {
    let PageOutput = [];
    console.log('pagination');
    console.log(this.state.pagingData);
    
    if(this.state.pagingData !="" && this.state.pagingData !="undefined")
    {
      var Page_Count = this.state.pagingData.totalPages;
      //alert(this.state.pagingData.currentPage);
    //console.log('page count = ' + Page_Count);
    /* pagination count */


        var Page_Start=1;
        var Page_End=1;

        if(this.state.pagingData.currentPage == 1){
            Page_Start=1;

            if(Page_Count <= 10){
                Page_End=Page_Count;
            }else{
                Page_End=10;
            }
            
        }else{

            if(this.state.pagingData.currentPage < 5){
                Page_Start=1;
                Page_End=Page_Count;
                //console.log("Page_End 1 "+ Page_End);
            }else{
                Page_Start=parseInt(this.state.pagingData.currentPage) - parseInt(4);
                Page_End=parseInt(this.state.pagingData.currentPage) + parseInt(6);
                //console.log("Page_End 2 "+ Page_End);
                if(Page_End > Page_Count){
                    Page_End=Page_Count;
                    //console.log("Page_End 3 "+ Page_End);
                }
            }

        }
      let Page = [];
      var i = 1;
      for (var z=Page_Start; z <= Page_End ; z++)
      {
        if(z==this.state.pagingData.currentPage)
        {
          Page.push(<li className="page-item active pk-active">
            <a className="page-link pk-active" id={z} href="#" onClick={this.PageGetGridData}>{z}<span className="sr-only">(current)</span></a>
          </li>);
        }
        else
        {
          Page.push(<li className="page-item"><a className="page-link" id={z} href="#" onClick={this.PageGetGridData} >{z}</a></li>);
        }
        i++;
      }

      let PagePrev = [];

      if(this.state.pagingData.currentPage == 1){
        PagePrev.push(<li className="page-item disabled">
          <a className="page-link" href="#">Previous</a>
        </li>);
      }else{
        PagePrev.push(<li className="page-item">
          <a className="page-link" href="#" id={parseInt(this.state.pagingData.currentPage)-parseInt(1)} tabIndex={-1} onClick={this.PageGetGridData}>Previous</a>
        </li>);
      }

      let PageNext = [];

      if(this.state.pagingData.currentPage == Page_Count){
        PageNext.push(<li className="page-item disabled">
          <a className="page-link" href="#">Next</a>
        </li>);
      }else{
        PageNext.push(
          <li className="page-item">
            <a className="page-link" href="#" id={parseInt(this.state.pagingData.currentPage)+parseInt(1)} onClick={this.PageGetGridData}>Next</a>
          </li>
        );
      }

      let PageLast = [];

      if(this.state.pagingData.currentPage == Page_Count){
        PageLast.push(<li className="page-item disabled">
          <a className="page-link" href="#">Last</a>
        </li>);
      }else{
        PageLast.push(
          <li className="page-item">
            <a className="page-link" href="#" id={parseInt(Page_Count)} onClick={this.PageGetGridData}>Last</a>
          </li>
        );
      }



      PageOutput.push(<section className="comp-section" id="comp_pagination">
                        <div className="pagination-box">
                          <div>
                            <ul className="pagination">
                              
                              {PagePrev}
                              {Page}
                              {PageNext}
                              {PageLast}
                              
                            </ul>
                          </div>
                        </div>
                      </section>);
    }
    
    return PageOutput;
  }

  PageGetGridData = e => {

    e.preventDefault();
    console.log(e.target.id);
    let current_page= e.target.id;
    this.GetCRMRegisterUser(current_page,this.state.pageSize,this.state.searchText)
  }

  SearchGridData = e => {
    this.setState({ pageSize: this.state.TempsearchText });
    this.GetCRMRegisterUser(1,this.state.pageSize,this.state.TempsearchText);
  }
  // Pagination Design

  render() {

                        
      return ( 
      <div className="main-wrapper">
     
          {/* Toast & Loder method use */}
            
          {(this.state.loading) ? <Loader /> : null} 
          {/* Toast & Loder method use */}
      <Header/>
        <div className={this.state.unauthorized_cls}>
          {/*<h1>403</h1>*/}
          <h4><i className="fa fa-warning"></i> Sorry, you are not authorized to access this page.</h4>
          {/*<p>Sorry, you are not authorized to access this page.</p>*/}
        </div>
        <div className={this.state.page_wrapper_cls}>
            <Helmet>
                <title>{process.env.WEB_TITLE}</title>
                <meta name="description" content="Login page"/>         
            </Helmet>
              {/* Page Content */}
              <div className="content container-fluid">
                {/* Page Header */}
                <div className="page-header">
                  <div className="row align-items-center">
                    <div className="col">
                      <h3 className="page-title">Employee Management</h3>
                      <ul className="breadcrumb">
                        <li className="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                        <li className="breadcrumb-item active">Employee Management</li>
                      </ul>
                    </div>
                    <div className="col-auto float-right ml-auto">
                      {this.state.role_employees_can.employees_can_create == true ?
                      <a href="#" className="btn add-btn" data-toggle="modal" data-target="#add_emp"><i className="fa fa-plus" /> Add Employee</a>
                      :
                      <h3 className="card-title"><a href="#" className="phss-lock"><i className="fa fa-lock" /></a></h3>
                      }
                    </div>
                  </div>
                </div>

                {/* Search Filter */}
                  <div className="row filter-row">
                    
                    
                    

                    <div className="col-lg-3 col-sm-4 col-xs-12"> 
                      <div className="form-group">
                        {/*<SelectDropdown
                          options={this.state.FLocationList}
                          valueField="locationId"
                          labelField="locationName"
                          searchBy="locationName"
                          className="form-control floating"
                          placeholder="Search Location"
                          id="locationID"
                         
                          values={this.state.SelectFLocationList}
                          onChange={(values) => this.FilterLocationSelect(values)}
                          
                        />*/}
                        
                        <select className="form-control floating" id="locationID" value={this.state.locationID} onChange={this.handleChange('locationID')}> 
                          <option value="">All</option>
                          {this.state.FLocationList.map(( listValue, index ) => {
                           
                              return (
                                <option key={index} value={listValue.locationId} >{listValue.locationName}</option>
                              );
                           
                          })}
                        </select>
                        
                        {/*<label className="focus-label">Search Location</label>*/}
                      </div>
                    </div>

                    


                    <div className="col-lg-3 col-sm-4 col-xs-12"> 
                      <div className="form-group form-focus select-focus">
                        <select className="form-control floating" id="sortColumnId" value={this.state.sortColumn} onChange={this.handleChange('sortColumn')}> 
                          <option value="">-</option>
                          <option value="FirstName">Name</option>
                          <option value="PhssEmail">Email</option>
                          <option value="UserRoleDisplay">Role</option>
                        </select>
                        <label className="focus-label">Sorting</label>
                      </div>
                    </div>

                     <div className="col-lg-3 col-sm-4 col-xs-12"> 
                      <div className="form-group form-focus select-focus">
                        <select className="form-control" id="sortRoleId" value={this.state.sortRoleId}  onChange={this.handleChange('sortRoleId')} >
                          <option value=''>-</option>
                          {this.state.roleList.map(( listValue, index ) => {
                              return (
                                <option key={index} value={listValue.id}>{listValue.name}</option>
                              );
                          })}
                        </select>
                        <label className="focus-label">Role</label>
                      </div>
                    </div>

                    <div className="col-lg-3 col-sm-4 col-xs-12"> 
                      <div className="form-group form-focus select-focus">
                        <select className="form-control floating" id="SortTypeId" value={this.state.SortType} onChange={this.handleChange('SortType')}> 
                          <option value="">-</option>
                          <option value="false">Ascending</option>
                          <option value="true">Descending</option>
                        </select>
                        <label className="focus-label">Sorting Order</label>
                      </div>
                    </div> 

                    <div className="col-lg-3 col-sm-4 col-xs-12"> 
                      <div className="form-group form-focus select-focus">
                        <select className="form-control floating" value={this.state.pageSize}  onChange={this.handleChange('pageSize')}> 
                          <option value="5">5/Page</option>
                          <option value="10">10/Page</option>
                          <option value="50">50/Page</option>
                          <option value="100">100/Page</option>
                        </select>
                        <label className="focus-label">Per Page</label>
                      </div>
                    </div> 

                    <div className="col-lg-3 col-sm-4 col-xs-12">  
                      <div className="form-group form-focus">
                        <input className="form-control floating" type="text" value={this.state.TempsearchText}  onChange={this.handleChange('TempsearchText')}/>
                        <label className="focus-label">Search</label>
                      </div>
                    </div>

                    <div className="col-lg-3 col-sm-4 col-xs-12">  
                      <div className="form-group form-focus pk-emp-padding-top">
                        <div className="checkbox">
                          <label>
                            <input type="checkbox" name="display_Inactive" value="true" onChange={this.handleChange('display_Inactive')}  checked={this.state.display_Inactive == true ? "true" : null} /> Display Inactive Employees
                          </label>
                        </div>
                      </div>
                    </div>  

                    <div className="col-lg-3 col-sm-8 col-xs-12">  
                      <a href="#" className="col-lg-4 col-sm-4 float-right col-xs-12 btn btn-success btn-block" onClick={this.SearchGridData}> Search </a>  
                    </div>

                  </div>
                {/* /Search Filter */}    
                {/* /Page Header */}
                <div className="row">
                  <div className="col-md-12">
                    <div className="table-responsive">
                      
               
                       <table className="table table-striped custom-table mb-0 datatable">
                        <thead>
                          <tr>
                            {/*<th>User Name</th>*/}
                            <th>Full Name</th>
                            <th>Email</th>
                            <th>User Type</th>
                            <th>Primary Location</th>
                            <th className="d-none d-sm-table-cell">Role</th>
                            <th>Status</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          {this.state.ListGrid.map(( listValue, index ) => {
                              var status = "";
                              if(listValue.employementStatus == "Active"){
                                status = <div><span class="badge bg-inverse-success">Active</span></div>;
                              }else if(listValue.employementStatus == "Inactive"){
                                status = <div><span class="badge bg-inverse-warning">Inactive</span></div>;
                              }else if(listValue.employementStatus == "On Hold"){
                                status = <div><span class="badge bg-inverse-info">On Hold</span></div>;
                              }
                              return (
                                <tr key={index}>
                                  {/*<td>{listValue.userName}</td>*/}
                                  <td>{listValue.firstName} {listValue.lastName}</td>
                                  <td>{listValue.userEmail}</td>
                                  <td>{listValue.userTypeDisplay}</td>
                                  <td>{listValue.primaryLocation}</td>
                                  <td>{listValue.userRoleDisplay}</td>
                                  <td>{status}</td>
                                  
                                  <td>{this.Edit_Update_Btn_Func(listValue)}</td>
                                </tr>
                              );
                            
                          })}
                          
                        </tbody>
                      </table> 

                      {/* Pagination */}
                      {this.PaginationDesign()}
                      {/* /Pagination */}

                    </div>
                  </div>
                </div>
              </div>
              {/* /Page Content */}
              {/* Add Employees Modal */}
              <div id="add_emp" className="modal custom-modal fade" role="dialog">
                <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
                  <div className="modal-content">
                    <div className="modal-header">
                      <h5 className="modal-title">Add Employee details</h5>
                      <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={this.ClearRecord()} >
                        <span aria-hidden="true">×</span>
                      </button>
                    </div>
                    <div className="modal-body">
                      <form>
                        <div className="row">
                          {/*<div className="form-group col-sm-6">
                            <label>User Name <span className="text-danger">*</span></label>
                            <input className="form-control" type="text" id="userName" value={this.state.userName}  onChange={this.handleChange('userName')} />
                            <span className="form-text error-font-color">{this.state.errormsg["userName"]}</span>
                          </div>*/}
                          <div className="form-group col-sm-6">
                            <label>Email <span className="text-danger">*</span></label>
                            <input className="form-control" type="text" id="userEmail" value={this.state.userEmail}  onChange={this.handleChange('userEmail')} />
                            <span className="form-text error-font-color">{this.state.errormsg["userEmail"]}</span>
                          </div>
                          <div className="form-group col-sm-6">
                            <label>First Name <span className="text-danger">*</span></label>
                            <input className="form-control" type="text" value={this.state.firstName}  onChange={this.handleChange('firstName')} />
                            <span className="form-text error-font-color">{this.state.errormsg["firstName"]}</span>
                          </div>
                          <div className="form-group col-sm-6">
                            <label>Last Name <span className="text-danger">*</span></label>
                            <input className="form-control" type="text" value={this.state.lastName}  onChange={this.handleChange('lastName')} />
                            <span className="form-text error-font-color">{this.state.errormsg["lastName"]}</span>
                          </div>
                          <div className="form-group col-sm-6">
                            <label>User Type <span className="text-danger">*</span></label>
                            <Multiselect
                              options={this.state.userTypeList} // Options to display in the dropdown
                              selectedValues={this.state.selectedValue} // Preselected value to persist in dropdown
                              onSelect={this.onSelectUserType} // Function will trigger on select event
                              onRemove={this.onRemoveUserType} // Function will trigger on remove event
                              displayValue="name" // Property name to display in the dropdown options
                              ref={this.userTypeListRef}
                            />
                            <span className="form-text error-font-color">{this.state.errormsg["userType"]}</span>
                          </div>
                          <div className="form-group col-sm-6">
                            <label>Role <span className="text-danger">*</span></label>
                            <Multiselect
                              options={this.state.roleList} // Options to display in the dropdown
                              //selectedValues={this.state.selectedValue} // Preselected value to persist in dropdown
                              onSelect={this.onSelectRole} // Function will trigger on select event
                              onRemove={this.onRemoveRole} // Function will trigger on remove event
                              displayValue="name" // Property name to display in the dropdown options
                              ref={this.roleListRef}
                            />
                            <span className="form-text error-font-color">{this.state.errormsg["userRole"]}</span>
                          </div>

                           <div className="col-md-6">
                              <div className="form-group">
                              
                                <label>Location<span className="text-danger">*</span></label>
                                <Multiselect
                                  options={this.state.locationList} // Options to display in the dropdown
                                  //selectedValues={this.state.selectedValue} // Preselected value to persist in dropdown
                                  onSelect={this.onSelectLocation} // Function will trigger on select event
                                  onRemove={this.onRemoveLocation} // Function will trigger on remove event
                                  displayValue="name" // Property name to display in the dropdown options
                                  ref={this.locationListRef}
                                  
                                />
                                <span className="form-text error-font-color">{this.state.errormsg["location"]}</span>
                              </div>
                            </div>

                            <div className="col-md-6" id="AddReportToDiv">
                              <div className="form-group">
                                <label>Report to</label>
                                <select className="form-control" id="AddReportTo" value={this.state.AddReportTo}  onChange={this.handleChange('AddReportTo')} >
                                  <option value=''>-</option>
                                  {this.state.GetCRMUserList.map(( listValue, index ) => {
                                    if(listValue.userTypeDisplay == "Staff"){  
                                      return (
                                        <option key={index} value={listValue.contactId}>{listValue.firstName} {listValue.lastName}</option>
                                      );
                                    }
                                  })}
                                </select>
                                <span className="form-text error-font-color">{this.state.errormsg["AddReportTo"]}</span>
                              </div>
                            </div>

                             <div className="col-md-6" id="AddApproverToDiv">
                              <div className="form-group">
                                <label>Timesheet Approver</label>
                                <select className="form-control" id="AddApproverTo" value={this.state.AddApproverTo}  onChange={this.handleChange('AddApproverTo')} >
                                  <option value=''>-</option>
                                  {this.state.GetCRMUserList.map(( listValue, index ) => {
                                    if(listValue.isTimesheeetApprover == true){
                                      return (
                                        <option key={index} value={listValue.contactId}>{listValue.firstName} {listValue.lastName}</option>
                                      );
                                    }
                                    
                                  })}
                                </select>
                                <span className="form-text error-font-color">{this.state.errormsg["AddApproverTo"]}</span>
                              </div>
                            </div>

                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Primary Location<span className="text-danger">*</span></label>
                                {/*<select className="form-control" value={this.state.AddPrimaryLocation}  onChange={this.handleChange('AddPrimaryLocation')}>
                                  <option value=''>-</option>
                                  {this.state.locationList.map(( listValue, index ) => {
                                    return (
                                      <option key={index} value={listValue.name}>{listValue.name}</option>
                                    );
                                  })}
                                </select>*/}
                                <SelectDropdown
                                    options={this.state.FLocationList}
                                    valueField="locationName"
                                    labelField="locationName"
                                    searchBy="locationName"
                                    className="form-control"
                                    placeholder="Search Location"
                                    id="locationID"
                                   
                                    values={this.state.SelectFLocationList}
                                    onChange={(values) => this.FilterLocationSelect(values)}
                                    
                                />
                                <span className="form-text error-font-color">{this.state.errormsg["AddPrimaryLocation"]}</span>
                              </div>
                            </div>
                          
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Employment Hours<span className="text-danger">*</span></label>
                                <select className="form-control" value={this.state.AddpartTimeType} onChange={this.handleChange('AddpartTimeType')}>
                                  <option value=''>-</option>
                                  {this.state.employeementHourList.map(( listValue, index ) => {
                                    return (
                                      <option key={index} value={listValue.name}>{listValue.name}</option>
                                    );
                                  })}
                                </select>
                                <span className="form-text error-font-color">{this.state.errormsg["AddpartTimeType"]}</span>
                              </div>
                            </div>

                        </div>
                        <div className="submit-section">
                          <button className="btn btn-primary submit-btn" onClick={this.AddRecord()}>Submit</button>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
                {/* ConfirmEmailExists Modal */}
                <div className="modal custom-modal fade" id="ConfirmEmailExistsAdd_emergency_contact" role="dialog">
                  <div className="modal-dialog modal-dialog-centered">
                    <div className="modal-content">
                      <div className="modal-body">
                        <div className="form-header">
                          <h3>Emergency Contact</h3>
                          <p>Are you sure want to delete selected record?</p>
                        </div>
                        <div className="modal-btn delete-action">
                          <div className="row">
                            <div className="col-6">
                              <a className="btn btn-primary continue-btn">Yes</a>
                            </div>
                            <div className="col-6">
                              <a href="" data-dismiss="modal" className="btn btn-primary cancel-btn">No</a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                {/* /ConfirmEmailExists Modal */}
              </div>
              {/* /Add Employees Modal */}
              {/* Edit Employees Modal */}
              
              {/* /Edit Employees Modal */}
              {/* Delete Today Work Modal */}
              <div className="modal custom-modal fade" id="Employee_Delete_modal" role="dialog">
                <div className="modal-dialog modal-dialog-centered">
                  <div className="modal-content">
                    <div className="modal-body">
                      <div className="form-header">
                        <h3>Employee</h3>
                        <p>Are you sure you want to mark employee as {this.state.isDeleteDisplay} ?</p>
                      </div>
                      <div className="modal-btn delete-action">
                        <div className="row">
                          <div className="col-6">
                            <a onClick={this.DeleteRecord()}  className="btn btn-primary continue-btn" style={{"color" : "#fff"}}> {this.state.isDeleteDisplay} </a>
                          </div>
                          <div className="col-6">
                            <a href="" data-dismiss="modal" className="btn btn-primary cancel-btn">Cancel</a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              {/* Delete Today Work Modal */}
            </div>
          <SidebarContent/>
      </div>
        );
      
   }
}

export default Employees;
