
import React, { Component } from 'react';
import { Helmet } from "react-helmet";
import {Reportto,ProfileImage,Avatar_02,Avatar_05,Avatar_09,Avatar_10,Avatar_16,User,Attachment,PlaceHolder } from '../../Entryfile/imagepath'

import { Table } from 'antd';
import 'antd/dist/antd.css';
import {itemRender,onShowSizeChange} from "./pagination/paginationfunction"
import "../MainPage/antdstyle.css"

import Header from '../Sidebar/header.jsx';
import SidebarContent from '../Sidebar/sidebar';

import moment from 'moment';
import Loader from '../Loader';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import SystemHelpers from '../Helpers/SystemHelper';

import Datetime from "react-datetime";


//import UpdateFileUploadPreview from '../FileUpload/UpdateFileUploadPreviewNotes'
import FileUploadPreview from '../FileUpload/FileUploadPreviewNotes'
import DocxImg from '../../assets/img/doc/docx.png'
import ExcelImg from '../../assets/img/doc/excel.png'
import PdfImg from '../../assets/img/doc/pdf.png'

//table
import { MDBDataTable } from 'mdbreact';
import '@fortawesome/fontawesome-free/css/all.min.css';
import 'bootstrap-css-only/css/bootstrap.min.css';
import 'mdbreact/dist/css/mdb.css';
//table

import axios from 'axios';
import { saveAs } from 'file-saver';

import CryptoAES from 'crypto-js/aes';
import CryptoENC from 'crypto-js/enc-utf8';

import { CSVLink, CSVDownload } from "react-csv";

import {alasql} from "alasql";

import Select from 'react-select';

class Timesheet extends Component {
  
  constructor(props) {
    super(props);
    this.state = {
      ListGrid:[],
      role_timesheet_can:[],
      ListGridPast:[],
      errormsg : '',
      serviceView:[],
      locationViews:[],
      ListGridPastDetails:[],
      ListGridPastDetailsCount:0,
      timeSheetContactMasterId:'',
      timeSheetPeriodViews:[],
      
      timeSheetHeaderView:[],
      
      tempfullName:'',
      LocationList:[],
      timeSheetDisplayUserViews:[],
      
      locationWiseTimeSheeetViews:[],
      
      CurrenttimeSheetPeriodId:'',
      CurrentDate_Min:'',
      CurrentDate_Max:'',
      isTimeSheetSubmitted:'',
      isCurrent: false,
      ContactlocationID:'',
      locationIdGuid:'',
      
      TimeSheetContactMasterArray:[],
      TimeSheetContactMasterArrayChild:[],
      past_display_amendment_due_date:'',
      past_display_dueDateAmendment:'',
      //add model 
      AddDate:'',
      AddTimeIn:'',
      AddTimeOut:'',
      Addlocation:'',
      AddService:'',
      AddNotes:'',
      AddNoOfHours:'',
      timeSheetPeriodId:'',
      IsNoOfHoursvalid: false,
      AddTimeOutDisabled : false,
      //Edit model
      timeSheetTransactionId: '',
      EditService: '',
      Editlocation: '',
      EditTimeIn: '',
      EditTimeOut: '',
      EditNoOfHours: '',
      EditDate: '',
      IsNoOfHoursvalidEdit:true,
      DeletetimesheetTransactionId:'',
      staffContactID:localStorage.getItem("contactId"),
      fullName:localStorage.getItem("fullName"),
      IsNoOfHoursvalidEdit:true,
      AddTimeIn_Max:'',
      AddTimeIn_Min:'',
      EdittimeSheetContactMasterId:'',
      EdittimeSheetContactId:'',
      EdittimeSheetStartDate:'',
      EdittimeSheetEndDate:'',
      isPayrollAdmin:false,
      EditworkedHour:'',
      EditTimeOutDisabled : false,
      EditTimeInDisabled : false,
      GetChatListLoad:false,

      isTimeSheetSubmittedToPayroll:false,
      isTimeSheetApproved: false,
      is_btn_display:false,
      
      check_coordinator_can: false,
      DueDate : '',
      totalSchedualHours:'',
      templocationId:'',
      
      csvdata:[],
      headers:[
        { label: "BADGE", key: "badge" },
        { label: "DTADDRESS", key: "dtaddress" },
        { label: "FUNC_COD", key: "funC_COD" },
        { label: "DATETIME", key: "datetime" },
        { label: "LCF1", key: "lcF1" }
      ],
      selectedOptionLocation: null,
      selectedOptionEmployees: null,
      isroleListFilter : false,
      isLocationListFilter : false,
      isemployeesListFilter :false,
      roleListFilter : [],
      LocationListFilter : [],
      employeesListFilter :[],
      employeeFilterID:'',
      LocationFilterID:'',
      roleFilterID:'',

      hierarchyId:'',
      page_wrapper_cls:'',
      unauthorized_cls:'',

      isAmendmends: false,
      recordType:'',
      isAmendmendsSubmitted: false,
      isTimeSheetSubmitedToPayroll: false,
      IsAmendmendsEnabled: false,

      load_attendance_summary : false,
      role_permission: {},

      AddNotesResetflag:false,
      AddMsg:'',
      filePreviewsFinal:[],
      ListNotes:[],
      allow_emp: false,
      isDelete : false,
      EditNoteId: '',
      IsEditBtn: false,

      AddPreference:[],
      EditPreference:[],
      AddPreferenceService:'',
      EditPreferenceService:'',
      filePreviewsFinalEdit:[],
      filePreviewsFinalEditActive:0,
      isPayrollAdminSession: false,
      approverDueDateForAmendmend :'',

      // Timesheet Entitlement Summary Summation Location Wsie
      DisplaytotalRegularHours : '00.00',  
      DisplaytotalAsleepHours : '00.00',
      DisplaytotalSickHours : '00.00',
      DisplaytotalTripHours : '00.00',
      DisplaytotalOTHours : '00.00',
      DisplaytotalBRVHours : '00.00',
      DisplaytotalLeaveHours : '00.00',
      DisplaytotalOnCallHours : '00.00',
      DisplaytotalStatHours : '00.00',
      // Timesheet Entitlement Summary Summation Location Wsie

      // HR 4-10-2021 Search With Drop Down
      SelectLocationList: [],
      DefaultemployeesListFilter :[{"employeeName":"All","contactId": ""}],
      // HR 4-10-2021 Search With Drop Down

    };
    this.setPropState = this.setPropState.bind(this);
    this.handleChange = this.handleChange.bind(this);

    this.handleDateAddIn = this.handleDateAddIn.bind(this);
    this.handleDateAddOut = this.handleDateAddOut.bind(this);
    this.handleDateEditIn = this.handleDateEditIn.bind(this);
    this.handleDateEditOut = this.handleDateEditOut.bind(this);
  }

  handleDateAddIn = (date) =>{
    //alert(date);

    // validation check
      //alert(date.isBefore(moment(this.state.AddTimeIn_Max)) && date.isAfter(moment(this.state.AddTimeIn_Min).subtract(1, 'day')));

    // validation check
    //var date_temp = moment(moment(date).add(2, 'day').format('YYYY/MM/DD HH:mm A'));
    this.setState({ AddTimeOut_Min: moment(date).subtract(1, 'day')});
    this.setState({ AddTimeOut_Max: moment(date).add(2, 'day')});
    this.setState({ AddTimeIn: date});

  //  console.log('date' + date);
    //console.log('AddTimeOut_Min' + moment(date).subtract(1, 'day'));
    //console.log('AddTimeOut_Max' + moment(date).add(2, 'day'));

    var AddTimeOut = this.state.AddTimeOut;

    if(this.state.AddService == process.env.API_24HOURS_SERVICE ){
       this.setState({ AddTimeOutDisabled: true});
      
      var AddTimeOut = moment(moment(date).add(24, 'hours'));
    }

    if(this.state.AddService != process.env.API_24HOURS_SERVICE ){
      this.setState({ AddTimeOutDisabled: false});
    }
    //this.setState({AddTimeOut: ''});
    //$('.AddTimeOut .form-control').val('null'); 
    
    //this.props.setPropState('AddTimeOut', null);
    this.setState({ AddTimeOut: AddTimeOut});

    if(date !='' && AddTimeOut != ''){
      var Get_Hour = this.Get_Hour(date,AddTimeOut);
    //  console.log(Get_Hour);
      this.setState({ AddNoOfHours: Get_Hour });
    }
    

    //console.log('AddTimeOut_Min => '+ date);
    //console.log('AddTimeOut_Max => '+ NewDate);
  };

  handleDateAddOut = (date) =>{
    //alert(date);
    this.setState({ AddTimeOut: date});

    var StarTime = this.state.AddTimeIn;
    var EndTime = date;


  //  console.log(StarTime);
  //  console.log(EndTime);

    if(StarTime != '' && EndTime !=''){
      var Get_Hour = this.Get_Hour(StarTime,EndTime);
    //  console.log(Get_Hour);
      this.setState({ AddNoOfHours: Get_Hour });
    }

  };

  handleDateEditIn = (date) =>{
    //alert(date);
    this.setState({ EditTimeOut_Min: moment(date).subtract(1, 'day')});
    this.setState({ EditTimeOut_Max: moment(date).add(2, 'day')});
    this.setState({ EditTimeIn: date});

    var EditTimeOut = this.state.EditTimeOut;
    if(this.state.EditService == process.env.API_24HOURS_SERVICE ){
      var EditTimeOut = moment(moment(date).add(24, 'hours'));
    }
    
    this.setState({ EditTimeOut: EditTimeOut});
    

    if(date !='' && EditTimeOut != ''){
      var Get_Hour = this.Get_Hour_Edit(date,EditTimeOut);
    //  console.log(Get_Hour);
      this.setState({ EditNoOfHours: Get_Hour });
    }
    

    //console.log('EditTimeOut_Min => '+ date);
    //console.log('EditTimeOut_Max => '+ NewDate);
  };

  handleDateEditOut = (date) =>{
    //alert(date);
    this.setState({ EditTimeOut: date});

    var StarTime = this.state.EditTimeIn;
    var EndTime = date;


  //  console.log(StarTime);
  //  console.log(EndTime);

    if(StarTime != '' && EndTime !=''){
      var Get_Hour = this.Get_Hour_Edit(StarTime,EndTime);
    //  console.log(Get_Hour);
      this.setState({ EditNoOfHours: Get_Hour });
    }

  };

  

  validationDateAddIn = (currentDate) => {
    //min={this.state.AddTimeIn_Min} max={this.state.AddTimeIn_Max}
    return currentDate.isBefore(moment(this.state.AddTimeIn_Max)) && currentDate.isAfter(moment(this.state.AddTimeIn_Min).subtract(1, 'day'));
  };

  validationDateAddOut = (currentDate) => {
    //min={this.state.AddTimeIn_Min} max={this.state.AddTimeIn_Max}
    if(this.state.AddService == process.env.API_ON_CALL || this.state.AddService == process.env.API_ZERO_HOURS){
      return currentDate.isBefore(moment(this.state.AddTempTimeOut_Max)) && currentDate.isAfter(moment(this.state.AddTimeOut_Min));
    }else{
      return currentDate.isBefore(moment(this.state.AddTimeOut_Max)) && currentDate.isAfter(moment(this.state.AddTimeOut_Min));
    }
  };

  AddTimeOutonClose = (date) =>{
    /* Payout Preference Start */

    if(this.state.AddService !="" && this.state.Addlocation !=""){
      var service = $("#AddService").find(':selected').data('basserivce');
      this.GetPayOffDetailsForUser(this.state.AddService,date,service,this.state.Addlocation,'Add');
    }

    /* Payout Preference End */
  }

  EditTimeOutonClose = (date) =>{
    /* Payout Preference Start */

    if(this.state.EditService !="" && this.state.Editlocation !=""){
      var service = $("#EditService").find(':selected').data('basserivce');
      this.GetPayOffDetailsForUser(this.state.EditService,date,service,this.state.Editlocation,'Edit');
    }

    /* Payout Preference End */
  }

  validationDateEditIn = (currentDate) => {
    //min={this.state.EditTimeIn_Min} max={this.state.EditTimeIn_Max}
    //return currentDate.isBefore(this.state.EditTimeOut_Min) && currentDate.isAfter(this.state.EditTimeOut_Min);
    return currentDate.isBefore(moment(this.state.EditTimeIn_Max)) && currentDate.isAfter(moment(this.state.EditTimeIn_Min));
  };

  validationDateEditOut = (currentDate) => {
    //min={this.state.AddTimeIn_Min} max={this.state.AddTimeIn_Max}
    // min={this.state.EditTimeOut_Min} max={this.state.EditTimeOut_Max}
    if(this.state.EditService == process.env.API_ON_CALL || this.state.EditService == process.env.API_ZERO_HOURS){
      return currentDate.isBefore(moment(this.state.EditTempTimeOut_Max)) && currentDate.isAfter(moment(this.state.EditTimeOut_Min));
    }else{
      return currentDate.isBefore(moment(this.state.EditTimeOut_Max)) && currentDate.isAfter(moment(this.state.EditTimeOut_Min));
    }
    
  };

  handleChange = input => e => {
    if(input !="allow_emp"){
      this.setState({ [input]: e.target.value });
    }
    
    console.log(input);
    console.log(e.target);

    if(input=="allow_emp")
    {
      if(this.state.allow_emp == true){
        this.setState({ allow_emp: false });
      }else{
       this.setState({ allow_emp: true }); 
      }
    }

    if (e.target.value != '') {
        delete this.state.errormsg[input];
    }

    /* Payout Preference Start */
    if(([input]=="AddService" && e.target.value !='') &&  this.state.Addlocation !="" && this.state.AddTimeIn !=""){
      var service = $("#AddService").find(':selected').data('basserivce');
      this.GetPayOffDetailsForUser(e.target.value,this.state.AddTimeOut,service,this.state.Addlocation,'Add');
    }

    if(([input]=="Addlocation" && e.target.value !='') &&  this.state.AddService !="" && this.state.AddTimeIn !=""){
      var service = $("#AddService").find(':selected').data('basserivce');
      this.GetPayOffDetailsForUser(this.state.AddService,this.state.AddTimeOut,service,e.target.value,'Add');
    }

    if(([input]=="EditService" && e.target.value !='') &&  this.state.Editlocation !="" && this.state.EditTimeIn !=""){
      var service = $("#EditService").find(':selected').data('basserivce');
      this.GetPayOffDetailsForUser(e.target.value,this.state.EditTimeOut,service,this.state.Editlocation,'Edit');
    }

    if([input]=="AddService" && this.state.AddTimeIn !='' && this.state.AddTimeOut != ''){
        var Get_Hour = this.Get_Hour(this.state.AddTimeIn,this.state.AddTimeOut);
        this.setState({ AddNoOfHours: Get_Hour });
    }

    if([input]=="EditService" && this.state.EditTimeIn !='' && this.state.EditTimeOut != ''){
        var Get_Hour = this.Get_Hour_Edit(this.state.EditTimeIn,this.state.EditTimeOut);
        this.setState({ EditNoOfHours: Get_Hour });
    }

    if(([input]=="Editlocation" && e.target.value !='') &&  this.state.EditService !="" && this.state.EditTimeIn !=""){
      var service = $("#EditService").find(':selected').data('basserivce');
      this.GetPayOffDetailsForUser(this.state.EditService,this.state.EditTimeOut,service,e.target.value,'Edit');
    }

    // if([input]=="AddService" && e.target.value == process.env.API_ON_CALL){
    //   alert(this.state.AddTempTimeOut_Max);
    //   this.setState({ AddTimeOut_Max: this.state.AddTempTimeOut_Max});
    // }else if([input]=="AddService" && e.target.value !=''){
    //   this.setState({ AddTimeOut_Max: this.state.AddTempTimeOut_Max});
    // }

    // if([input]=="EditService" && e.target.value == process.env.API_ON_CALL){
    //   this.setState({ EditTimeOut_Max: this.state.EditTempTimeOut_Max});
    // }else if([input]=="EditService" && e.target.value !=''){
    //   this.setState({ EditTimeOut_Max: this.state.EditTempTimeOut_Max});
    // }

    /* Payout Preference End */

    /* Filter Search */
    if([input]=="LocationFilterID" && e.target.value != ''){
      this.setState({ employeeFilterID: ''});
      this.setState({ roleFilterID: ''});

      $("#employeeFilterID").prop( "disabled", true );
      $("#roleFilterID").prop( "disabled", true );
    }

    if(([input]=="employeeFilterID" && e.target.value != '') || ([input]=="roleFilterID" && e.target.value != '') ){
      this.setState({ LocationFilterID: ''});
      $("#LocationFilterID").prop( "disabled", true );
    }

    if([input]=="LocationFilterID" && e.target.value == ''){
      this.setState({ employeeFilterID: ''});
      this.setState({ roleFilterID: ''});

      $("#employeeFilterID").prop( "disabled", false );
      $("#roleFilterID").prop( "disabled", false );
    }

    if(([input]=="employeeFilterID" && e.target.value == '') && (this.state.roleFilterID == '') ){
      this.setState({ LocationFilterID: ''});
      $("#LocationFilterID").prop( "disabled", false );
    }

    if(([input]=="roleFilterID" && e.target.value == '') && (this.state.employeeFilterID == '') ){
      this.setState({ LocationFilterID: ''});
      $("#LocationFilterID").prop( "disabled", false );
    }



    /* Filter Search */

    if([input]=="AddService" && this.state.AddTimeIn !='' && e.target.value == process.env.API_24HOURS_SERVICE){
      this.setState({ AddTimeOutDisabled: true});
      var AddTimeOut = moment(moment(this.state.AddTimeIn).add(24, 'hours'));
      //alert(NewDate);
      this.setState({ AddTimeOut: AddTimeOut});
      
      var StarTime = this.state.AddTimeIn;
      var EndTime = AddTimeOut;

    //  console.log(StarTime);
    //  console.log(EndTime);

      if(StarTime != '' && EndTime !=''){
        var Get_Hour = this.Get_Hour(StarTime,EndTime);
      //  console.log(Get_Hour);
        this.setState({ AddNoOfHours: Get_Hour });
      }  
    }

    if([input]=="AddService" && e.target.value != process.env.API_24HOURS_SERVICE ){
      this.setState({ AddTimeOutDisabled: false});
    }


    if([input]=="EditService" && e.target.value == process.env.API_24HOURS_SERVICE  && this.state.EditTimeIn !=''){
      this.setState({ EditTimeOutDisabled: true});
      $("#EditTimeOut").prop( "disabled", true );

      var NewDate = moment(moment(this.state.EditTimeIn).add(24, 'hours'));
      //var NewDate = moment(this.state.EditTimeIn).add(24, 'hours').format('YYYY/MM/DD HH:mm A');
      //alert(NewDate);

      
      this.setState({ EditTimeOut: NewDate});
      var StarTime = this.state.EditTimeIn;
      var EndTime = NewDate;

    //  console.log(StarTime);
    //  console.log(EndTime);

      if(StarTime != '' && EndTime !=''){
        var Get_Hour = this.Get_Hour_Edit(StarTime,EndTime);
      //  console.log(Get_Hour);
        this.setState({ EditNoOfHours: Get_Hour });
      }  
    }else if([input]=="EditService" && e.target.value != process.env.API_24HOURS_SERVICE ){
      $("#EditTimeOut").prop( "disabled", false );
      this.setState({ EditTimeOutDisabled: false});
    }
  }

  GetPayOffDetailsForUser(AddService,date,baseservice,Addlocation,calltype){
    //alert(baseservice);
    this.setState({ AddPreference: [] });
    this.setState({ EditPreference: [] });
    this.setState({ AddPreferenceService : '' });
    //this.setState({ EditPreferenceService : '' });
    // if(baseservice == false){
    //   return false;
    // }

    if(calltype == 'Add'){
     this.setState({ AddPreference: [] });
    }else{
      this.setState({ EditPreference: [] });
    }
    
    var Servicedate=moment(date).format('MM/DD/YYYY');

    console.log(Servicedate);

    this.showLoader();
    var url=process.env.API_API_URL+'GetPayOffDetailsForUser?serviceCodeId='+AddService+"&inTimeDate="+Servicedate+"&basedOnServiceCode="+baseservice+"&locationId="+Addlocation;
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
      //body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
      console.log("responseJson GetPayOffDetailsForUser");
      console.log(data);
      
      // debugger;
      if (data.responseType === "1") {
        // Profile & Contact

        if(data != null){

          if(calltype == 'Add'){
           this.setState({ AddPreference: data.data });
          }else{
            this.setState({ EditPreference: data.data });
          }
        }
        
      }else{
        if(data.message == 'Authorization has been denied for this request.'){
          SystemHelpers.SessionOut();
          this.props.history.push("/login");
        }else{
          SystemHelpers.ToastError(data.message);
        }
      }
      this.hideLoader();
    })
    .catch(error => {
      this.props.history.push("/error-500");
    });
  }

  Get_Hour(StarTime,EndTime){
  //  console.log('StarTime =>'+StarTime);
  //  console.log('EndTime =>'+EndTime);
    var StarTime =moment(StarTime).format("DD/MM/YYYY HH:mm:ss");
    var EndTime =moment(EndTime).format("DD/MM/YYYY HH:mm:ss");
  //  console.log(StarTime);
  //  console.log(EndTime);

    var ms = moment(EndTime,"DD/MM/YYYY HH:mm:ss").diff(moment(StarTime,"DD/MM/YYYY HH:mm:ss"));
    var d = moment.duration(ms);
    var totalHours = Math.floor(d.asHours()) + moment.utc(ms).format(":mm");
    //alert(d.asHours());

      console.log(this.state.AddService);
      console.log(process.env.API_ZERO_HOURS);

    let step1Errors = {};
    if ((d.asHours() < 0 || totalHours == '0:00') && this.state.AddService != process.env.API_ON_CALL ) {
      step1Errors["AddNoOfHours"] = "Enter Valid TimeIn and TimeOut.";
      this.setState({ IsNoOfHoursvalid: false });
    }else if(d.asHours() > 24 && this.state.AddService != process.env.API_ZERO_HOURS && this.state.AddPreference.length == 0){
      step1Errors["AddNoOfHours"] = "you can add max 24 hours.";
      this.setState({ IsNoOfHoursvalid: false });
    }
    else{
      delete this.state.errormsg['AddNoOfHours'];
      this.setState({ IsNoOfHoursvalid: true });
    }

    //console.log(this.state.AddService);
    
    if(this.state.AddService == process.env.API_ZERO_HOURS){
      delete this.state.errormsg['AddNoOfHours'];
      this.setState({ IsNoOfHoursvalid: true });
      totalHours = '00:00';
      let step1Errors = {};
    }

    this.setState({ errormsg: step1Errors });

    //var totalHours = moment.utc(moment(StarTime,"DD/MM/YYYY HH:mm:ss").diff(moment(EndTime,"DD/MM/YYYY HH:mm:ss"))).format("HH:mm:ss");
    return totalHours;
  }


  Get_Hour_Edit(StarTime,EndTime){
  //  console.log(StarTime);
  //  console.log(EndTime);
    var StarTime =moment(StarTime).format("DD/MM/YYYY HH:mm:ss");
    var EndTime =moment(EndTime).format("DD/MM/YYYY HH:mm:ss");
  //  console.log(StarTime);
  //  console.log(EndTime);

    var ms = moment(EndTime,"DD/MM/YYYY HH:mm:ss").diff(moment(StarTime,"DD/MM/YYYY HH:mm:ss"));
    var d = moment.duration(ms);
    var totalHours = Math.floor(d.asHours()) + moment.utc(ms).format(":mm");
    //alert(d.asHours());

    let step1Errors = {};
    if (d.asHours() < 0 && (this.state.EditService != process.env.API_ON_CALL || this.state.EditService != process.env.API_ZERO_HOURS)) {
      step1Errors["EditNoOfHours"] = "Enter Valid TimeIn and TimeOut.";
      this.setState({ IsNoOfHoursvalidEdit: false });
    }else if(d.asHours() > 24 && (this.state.EditService != process.env.API_ON_CALL || this.state.EditService != process.env.API_ZERO_HOURS)){
      step1Errors["EditNoOfHours"] = "you can add max 24 hours.";
      this.setState({ IsNoOfHoursvalidEdit: false });
    }
    else{
      delete this.state.errormsg['EditNoOfHours'];
      this.setState({ IsNoOfHoursvalidEdit: true });
    }

    if(this.state.EditService == process.env.API_ZERO_HOURS){
      delete this.state.errormsg['EditNoOfHours'];
      this.setState({ IsNoOfHoursvalidEdit: true });
      totalHours = '00:00';
      let step1Errors = {};
    }

    this.setState({ errormsg: step1Errors });

    //var totalHours = moment.utc(moment(StarTime,"DD/MM/YYYY HH:mm:ss").diff(moment(EndTime,"DD/MM/YYYY HH:mm:ss"))).format("HH:mm:ss");
    return totalHours;
  }


  setPropState(key, value) {
    this.setState({ [key]: value });
  }

  // Loader hide show method
  hideLoader = () => {
    this.setState({ loading: false });
  }

  showLoader = () => {
    this.setState({ loading: true });
  }
  // Loader hide show method

  componentDidMount() {
    var CurrenttimeSheetPeriodId = localStorage.getItem('CurrenttimeSheetPeriodId');
    this.setState({ CurrenttimeSheetPeriodId: CurrenttimeSheetPeriodId });
    /* Decode Str*/
       var DecodeStr = SystemHelpers.GetDecodeStr('timeSheetPeriodViews');
       this.setState({ timeSheetPeriodViews: DecodeStr });
       this.Default_TimeSheet(DecodeStr);
       //console.log('DecodeStr');
       //console.log(DecodeStr);
    /* Decode Str */

    /* Role Management */
   //  console.log('Role Store check_coordinator_can');
     var getrole = SystemHelpers.GetRole();
     let check_coordinator_can = getrole.check_coordinator_can;
     this.setState({ check_coordinator_can: check_coordinator_can });
    //alert(check_coordinator_can);
    /* Role Management */

    /* Role Management */
  //  console.log('Role Store timesheet_can');
    var getrole = SystemHelpers.GetRole();
    let timesheet_can = getrole.timesheet_can;
    this.setState({ role_timesheet_can: timesheet_can });
  //  console.log(timesheet_can);

    /* Role Management */

    /* Role Management */
    let hierarchyId = getrole.hierarchy_can.hierarchy_can_id;
    this.setState({ hierarchyId: hierarchyId });

    if(hierarchyId == 1){
      this.setState({ page_wrapper_cls: 'page-wrapper pk-page-wrapper-remove hide-div' });
      this.setState({ unauthorized_cls: 'error-box pk-margin-top' });

      setTimeout(() => {
        $('.pk-page-wrapper-remove').remove()
      }, 5000);
      
    }else{
      this.setState({ page_wrapper_cls: 'page-wrapper' });
      this.setState({ unauthorized_cls: 'error-box hide-div' });
    }
    /* Role Management */

    this.GetUserLocations();
    this.GetProfile();
    
    this.GetTimeSheetReportView();
    //this.DownloadCSVADPJson();

    if (localStorage.getItem("CurrenttimeSheetPeriodId") != null) {
      this.GetUserTimeSheetDataByLocation();
    }

    
    
    
    $( "#AddTimeOut" ).prop( "disabled", true );
  }

  

  GetTimeSheetReportView(){

     /* Role Management */
      var getrole = SystemHelpers.GetRole();
    //  console.log('Location Get role');
    //  console.log(getrole.locations_can);
      //let canDelete = getrole.locations_can.locations_can_delete;
      //let locationscanViewall = getrole.locations_can.locations_can_viewall;
      let hierarchyId = getrole.hierarchy_can.hierarchy_can_id;
    /* Role Management */
    this.showLoader();
    var url=process.env.API_API_URL+'GetTimeSheetReportView?loggedInUserId='+this.state.staffContactID+"&rolePriorityId="+hierarchyId;
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
      //body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
      console.log("responseJson GetTimeSheetReportView");
      console.log(data);
      
      // debugger;
      if (data.responseType === "1") {

        /* ************************************************************ */
        let locationViews_push = [];
        var locationViews = data.data.locationViews;

        let locationtemp = {};
        locationtemp["value"] = "";
        locationtemp["label"] = "All";
        locationViews_push.push(locationtemp);

        for (var zz = 0; zz < locationViews.length; zz++) {
            let locationtemp = {};
            locationtemp["value"] = locationViews[zz].locationId;
            locationtemp["label"] = locationViews[zz].locationName;
            locationViews_push.push(locationtemp);
        }

        this.setState({ LocationListFilter: locationViews_push });

        /* ************************************************************ */
        //this.setState({ locationViews: data.data.locationViews });

        // HR 4-10-2021 Search With Drop Down
        this.setState({ SelectLocationList: data.data.locationViews });
        // HR 4-10-2021 Search With Drop Down

        this.setState({ roleListFilter: data.data.roleList });
        

        /* ************************************************************ */
        let employeeViews_push = [];
        var employeeViews = data.data.employees;

        let employeetemp = {};
        employeetemp["value"] = "";
        employeetemp["label"] = "All";
        employeeViews_push.push(employeetemp);

        for (var zz = 0; zz < employeeViews.length; zz++) {
            let employeetemp = {};
            employeetemp["value"] = employeeViews[zz].contactId;
            employeetemp["label"] = employeeViews[zz].employeeName;
            employeeViews_push.push(employeetemp);
        }

        this.setState({ employeesListFilter: employeeViews_push });
        //this.setState({ employeesListFilter: data.data.employees });
        /* ************************************************************ */
        

        this.setState({ serviceView: data.data.serviceViews });
        
        this.setState({ timeSheetPeriodViews: data.data.timeSheetPeriodViews });

        this.setState({ timeSheetPeriodViews: data.data.timeSheetPeriodViews });
        this.Default_TimeSheet(data.data.timeSheetPeriodViews);

        var pwd = localStorage.getItem("contactId")+"Phss@123";
        var Period_str = CryptoAES.encrypt(JSON.stringify(data.data.timeSheetPeriodViews), pwd);
        localStorage.setItem('timeSheetPeriodViews', Period_str);

        // Profile & Contact
        //  timesheetContactView CurrenttimeSheetPeriodId
        if(data != null){
          
        }
        
        //console.log(data.data.userSkillInfo);
        //this.setState({ ListGrid: this.rowData(data.data.userSkillInfo) })
      }else{
        if(data.message == 'Authorization has been denied for this request.'){
          SystemHelpers.SessionOut();
          this.props.history.push("/login");
        }else{
          SystemHelpers.ToastError(data.message);
        }
      }
      this.hideLoader();
    })
    .catch(error => {
    //  console.log('GetTimeSheetReportView error');
      this.props.history.push("/error-500");
    });
  }

  

  GetUserTimeSheetDataByLocation() {
    console.log("GetUserTimeSheetDataByLocation");
    this.showLoader();
    this.setState({ load_attendance_summary: false });

    var employeeFilterID = this.state.employeeFilterID;
    var LocationFilterID = this.state.LocationFilterID;
    var roleFilterID = this.state.roleFilterID;
    var PendingForApproval = false;
    if(employeeFilterID == ''){
      this.setState({ isemployeesListFilter: false });
    }else{
      this.setState({ isemployeesListFilter: true });
    }

    if(LocationFilterID == ''){
      this.setState({ isLocationListFilter: false });
    }else{
      this.setState({ isLocationListFilter: true });
    }

    if(roleFilterID == ''){
      this.setState({ isroleListFilter: false });
    }else{
      this.setState({ isroleListFilter: true });
    }

    if($('#pendingforapproval').prop('checked')) {
        PendingForApproval = true;
    } else {
        PendingForApproval = false;
    }

    //alert(PendingForApproval);


    
    var staffContactID = this.state.staffContactID;



    var timesheetPeriodId = $("#timeSheetPeriod").val();

    //this.setState({ locationWiseTimeSheeetViews: [] });
     this.setState({ CurrenttimeSheetPeriodId: timesheetPeriodId});
     this.setState({ timeSheetPeriodId: timesheetPeriodId});

    if(timesheetPeriodId == '' || timesheetPeriodId == null){
      var CurrenttimeSheetPeriodId = localStorage.getItem('CurrenttimeSheetPeriodId');
    }else{
      var CurrenttimeSheetPeriodId = timesheetPeriodId;
    }
    
    var ContactlocationID = localStorage.getItem("primaryLocationId");
    var isPayrollAdmin = localStorage.getItem("isPayrollAdmin");
    
    //alert(isPayrollAdmin);

    // User type session decode
      var pwd = localStorage.getItem("contactId")+"Phss@123";
      var UserType_session = localStorage.getItem("usertypesession");

      var _ciphertext = CryptoAES.decrypt(UserType_session, pwd);
      var UserType_JsonCreate = JSON.parse(_ciphertext.toString(CryptoENC));

    //  console.log('UserType_JsonCreate');
    //  console.log(UserType_JsonCreate);
      
      this.setState({ isCordinatorSession : UserType_JsonCreate.isCordinator });
      this.setState({ isPayrollAdminSession : UserType_JsonCreate.isPayrollAdmin });
      this.setState({ isSeniorCordinatorSession : UserType_JsonCreate.isSeniorCordinator });
      this.setState({ isServiceCoordinatorLeadSession : UserType_JsonCreate.isServiceCoordinatorLead });
    // User type session decode

    this.Default_TimeSheet2(timesheetPeriodId);

    var isCordinatorSession =UserType_JsonCreate.isCordinator;
    var isPayrollAdminSession =UserType_JsonCreate.isPayrollAdmin;
    var isSeniorCordinatorSession =UserType_JsonCreate.isSeniorCordinator;
    var isServiceCoordinatorLeadSession =UserType_JsonCreate.isServiceCoordinatorLead;
    var primaryLocationId = localStorage.getItem("primaryLocationId");
    
     /* Role Management */
      var getrole = SystemHelpers.GetRole();
    //  console.log('Location Get hierarchy_can');
      //let canDelete = getrole.locations_can.locations_can_delete;
      //let locationscanViewall = getrole.locations_can.locations_can_viewall;
      let hierarchyId = getrole.hierarchy_can.hierarchy_can_id;
    /* Role Management */

    var url=process.env.API_API_URL+'GetLoggedInUserWiseStaffAttendanceSummary?loggedInUserId='+staffContactID+"&periodProfileId="+CurrenttimeSheetPeriodId+"&locationGuid="+LocationFilterID+"&locationId="+LocationFilterID+"&contactId="+employeeFilterID+"&roleId="+roleFilterID+'&rolePriorityId='+hierarchyId+"&onlyShowPendingForApproval="+PendingForApproval
    
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
      //body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
      console.log("responseJson GetLoggedInUserWiseStaffAttendanceSummary 444");
      console.log(data);


      this.setState({ load_attendance_summary: true });
      // debugger;
      if (data.responseType === "1") {
        // Profile & Contact

        if(data != null){
          
          /* Decode Str*/
           // var DecodeStr = SystemHelpers.GetDecodeStr('timeSheetPeriodViews');
           // this.setState({ timeSheetPeriodViews: DecodeStr });
           // this.Default_TimeSheet(DecodeStr);
           //console.log('DecodeStr');
           //console.log(DecodeStr);
          /* Decode Str */
          
          if(data.data.timeSheetHeaderView != null){
            
            //  console.log(data.data.timeSheetConatctSheetViews);
              this.setState({ timeSheetHeaderView: data.data.timeSheetHeaderView });

              this.setState({ is_btn_display: true});
          }

          if(data.data.locationWiseTimeSheeetViews != null && data.data.locationWiseTimeSheeetViews.length != 0){
              
              console.log('locationWiseTimeSheeetViews dev');
              console.log(data.data.locationWiseTimeSheeetViews);
              this.setState({ locationWiseTimeSheeetViews: data.data.locationWiseTimeSheeetViews });
              this.TimeSheetContactMasterFunc(data.data.locationWiseTimeSheeetViews,'parent');
              this.setState({ is_btn_display: true});
          }

          if(data.data.totalSchedualHours != null){
            this.setState({ totalSchedualHours: data.data.totalSchedualHours });
          }

          if(data.data.timeSheetDisplayUserViews != null){
            //  console.log('timeSheetDisplayUserViews');
            //  console.log(data.data.timeSheetDisplayUserViews);
              this.setState({ timeSheetDisplayUserViews: data.data.timeSheetDisplayUserViews });
          }

          

          if(data.data.timeSheetPeriodViews != null){
            // this.setState({ timeSheetPeriodViews: data.data.timeSheetPeriodViews });
            //this.Default_TimeSheet(data.data.timeSheetPeriodViews);
          }

          
        }
        
        //console.log(data.data.userSkillInfo);
        //this.setState({ ListGrid: this.rowData(data.data.userSkillInfo) })
      }else{
        if(data.message == 'Authorization has been denied for this request.'){
          SystemHelpers.SessionOut();
          this.props.history.push("/login");
        }else{
          //SystemHelpers.ToastError(data.message);
        }
      }
      this.hideLoader();
      return false;
    })
    .catch(error => {
    //  console.log('GetUserTimeSheetDataByLocation error');
      //this.props.history.push("/error-500");
    });
  }


  ChangePeriod = e => {
    e.preventDefault();
    this.GetUserTimeSheetDataByLocation();
  }

  


  Default_TimeSheet(timeSheetPeriod){
    //  console.log('timeSheetPeriod func');
    //  console.log(timeSheetPeriod);
    //  console.log(this.state.timeSheetPeriodId);

      var length = timeSheetPeriod.length;
      var MinDate = moment(new Date()).format("YYYY-MM-DD");
      var MaxDate = moment(new Date()).format("YYYY-MM-DD");
      //alert(MaxDate);
      if (length > 0) {
        var i = 1;
        for (var zz = 0; zz < length; zz++) {

          if(timeSheetPeriod[zz].isCurrentTimeSheet == true && this.state.timeSheetPeriodId == ""){
            var AddDate_Max =  moment(timeSheetPeriod[zz].timeSheetPeriodEndDate,process.env.API_DATE_FORMAT).format('YYYY-MM-DD');
            this.setState({ AddTimeIn_Max: AddDate_Max+"T00:01"});
            this.setState({ EditTimeIn_Max: AddDate_Max+"T00:01"});

            this.setState({ AddTempTimeOut_Max: AddDate_Max+"T00:01"});
            this.setState({ EditTempTimeOut_Max: AddDate_Max+"T00:01"});
            
            var AddDate_Min = moment(timeSheetPeriod[zz].timeSheetPeriodStartDate,process.env.API_DATE_FORMAT).subtract(1, "days").format('YYYY-MM-DD')
            this.setState({ AddTimeIn_Min: AddDate_Min+"T00:01"});
            var EditDate_Min = moment(timeSheetPeriod[zz].timeSheetPeriodStartDate,process.env.API_DATE_FORMAT).subtract(2, "days").format('YYYY-MM-DD');
            this.setState({ EditTimeIn_Min: EditDate_Min+"T00:01"});

            this.setState({ CurrentDate_Min: AddDate_Min});
            this.setState({ CurrentDate_Max: AddDate_Max});
            this.setState({ isCurrent: timeSheetPeriod[zz].isCurrentTimeSheet});
            
            this.setState({ past_display_dueDateAmendment: timeSheetPeriod[zz].dueDateAmendment });
            
            $('#timeSheetPeriod').val(timeSheetPeriod[zz].timeSheetPeriodId).trigger('change');

            this.setState({ timeSheetPeriod: timeSheetPeriod[zz].timeSheetPeriodId});
            this.setState({ timeSheetPeriodId: timeSheetPeriod[zz].timeSheetPeriodId});

            this.setState({ CurrenttimeSheetPeriodId: timeSheetPeriod[zz].timeSheetPeriodId});

            //this.GetUserTimeSheetDataByLocation();
          }else if(this.state.timeSheetPeriodId == timeSheetPeriod[zz].timeSheetPeriodId){
            //alert(timeSheetPeriod[zz].isCurrentTimeSheet);
            //alert(timeSheetPeriod[zz].isCurrentTimeSheet);
            var AddDate_Max =  moment(timeSheetPeriod[zz].timeSheetPeriodEndDate,process.env.API_DATE_FORMAT).format('YYYY-MM-DD');
            this.setState({ AddTimeIn_Max: AddDate_Max+"T00:01"});
            this.setState({ EditTimeIn_Max: AddDate_Max+"T00:01"});

            this.setState({ AddTempTimeOut_Max: AddDate_Max+"T00:01"});
            this.setState({ EditTempTimeOut_Max: AddDate_Max+"T00:01"});
            
            var AddDate_Min = moment(timeSheetPeriod[zz].timeSheetPeriodStartDate,process.env.API_DATE_FORMAT).subtract(1, "days").format('YYYY-MM-DD')
            this.setState({ AddTimeIn_Min: AddDate_Min+"T00:01"});
            var EditDate_Min = moment(timeSheetPeriod[zz].timeSheetPeriodStartDate,process.env.API_DATE_FORMAT).subtract(2, "days").format('YYYY-MM-DD');
            this.setState({ EditTimeIn_Min: EditDate_Min+"T00:01"});

            this.setState({ CurrentDate_Min: AddDate_Min});
            this.setState({ CurrentDate_Max: AddDate_Max});
            this.setState({ isCurrent: timeSheetPeriod[zz].isCurrentTimeSheet});
            
            this.setState({ past_display_dueDateAmendment: timeSheetPeriod[zz].dueDateAmendment });
             
            $('#timeSheetPeriod').val(timeSheetPeriod[zz].timeSheetPeriodId).trigger('change');

             this.setState({ timeSheetPeriod: timeSheetPeriod[zz].timeSheetPeriodId});
             this.setState({ timeSheetPeriodId: timeSheetPeriod[zz].timeSheetPeriodId});

             this.setState({ CurrenttimeSheetPeriodId: timeSheetPeriod[zz].timeSheetPeriodId});

            // this.GetUserTimeSheetDataByLocation();
          }
          i++;
        }
      }
  }

  Default_TimeSheet2(timesheetPeriodId){
      var timeSheetPeriod = this.state.timeSheetPeriodViews;
      var length = timeSheetPeriod.length;
      var MinDate = moment(new Date()).format("YYYY-MM-DD");
      var MaxDate = moment(new Date()).format("YYYY-MM-DD");
      //alert(this.state.timeSheetPeriodId);
      console.log(timeSheetPeriod);
      if (length > 0) {
        var i = 1;
         for (var zz = 0; zz < length; zz++) {

          if(timeSheetPeriod[zz].isCurrentTimeSheet == true && this.state.timeSheetPeriodId == ""){
            var AddDate_Max =  moment(timeSheetPeriod[zz].timeSheetPeriodEndDate,process.env.API_DATE_FORMAT).format('YYYY-MM-DD');
            this.setState({ AddTimeIn_Max: AddDate_Max+"T00:01"});
            this.setState({ EditTimeIn_Max: AddDate_Max+"T00:01"});

            this.setState({ AddTempTimeOut_Max: AddDate_Max+"T00:01"});
            this.setState({ EditTempTimeOut_Max: AddDate_Max+"T00:01"});
            
            var AddDate_Min = moment(timeSheetPeriod[zz].timeSheetPeriodStartDate,process.env.API_DATE_FORMAT).subtract(1, "days").format('YYYY-MM-DD')
            this.setState({ AddTimeIn_Min: AddDate_Min+"T00:01"});
            var EditDate_Min = moment(timeSheetPeriod[zz].timeSheetPeriodStartDate,process.env.API_DATE_FORMAT).subtract(2, "days").format('YYYY-MM-DD');
            this.setState({ EditTimeIn_Min: EditDate_Min+"T00:01"});

            this.setState({ CurrentDate_Min: AddDate_Min});
            this.setState({ CurrentDate_Max: AddDate_Max});
            this.setState({ isCurrent: timeSheetPeriod[zz].isCurrentTimeSheet});
            this.setState({ past_display_dueDateAmendment: timeSheetPeriod[zz].dueDateAmendment });
            
            $('#timeSheetPeriod').val(timeSheetPeriod[zz].timeSheetPeriodId).trigger('change');

             this.setState({ timeSheetPeriod: timeSheetPeriod[zz].timeSheetPeriodId});
            this.setState({ timeSheetPeriodId: timeSheetPeriod[zz].timeSheetPeriodId});

            this.setState({ CurrenttimeSheetPeriodId: timeSheetPeriod[zz].timeSheetPeriodId});

            //this.GetUserTimeSheetDataByLocation();
          }else if(timesheetPeriodId == timeSheetPeriod[zz].timeSheetPeriodId){
            //alert(timeSheetPeriod[zz].isCurrentTimeSheet);
            //alert(timeSheetPeriod[zz].isCurrentTimeSheet);
            var AddDate_Max =  moment(timeSheetPeriod[zz].timeSheetPeriodEndDate,process.env.API_DATE_FORMAT).format('YYYY-MM-DD');
            this.setState({ AddTimeIn_Max: AddDate_Max+"T00:01"});
            this.setState({ EditTimeIn_Max: AddDate_Max+"T00:01"});

            this.setState({ AddTempTimeOut_Max: AddDate_Max+"T00:01"});
            this.setState({ EditTempTimeOut_Max: AddDate_Max+"T00:01"});
            
            var AddDate_Min = moment(timeSheetPeriod[zz].timeSheetPeriodStartDate,process.env.API_DATE_FORMAT).subtract(1, "days").format('YYYY-MM-DD')
            this.setState({ AddTimeIn_Min: AddDate_Min+"T00:01"});
            var EditDate_Min = moment(timeSheetPeriod[zz].timeSheetPeriodStartDate,process.env.API_DATE_FORMAT).subtract(2, "days").format('YYYY-MM-DD');
            this.setState({ EditTimeIn_Min: EditDate_Min+"T00:01"});

            this.setState({ CurrentDate_Min: AddDate_Min});
            this.setState({ CurrentDate_Max: AddDate_Max});
            this.setState({ isCurrent: timeSheetPeriod[zz].isCurrentTimeSheet});
            this.setState({ past_display_dueDateAmendment: timeSheetPeriod[zz].dueDateAmendment });

            
            $('#timeSheetPeriod').val(timeSheetPeriod[zz].timeSheetPeriodId).trigger('change');

            this.setState({ timeSheetPeriod: timeSheetPeriod[zz].timeSheetPeriodId});
            this.setState({ timeSheetPeriodId: timeSheetPeriod[zz].timeSheetPeriodId});

            this.setState({ CurrenttimeSheetPeriodId: timeSheetPeriod[zz].timeSheetPeriodId});

            //this.GetUserTimeSheetDataByLocation();
          }
          i++;
        }
      }
  }
 
  GetUserPastTimeSheetData(){
    this.showLoader();
    var url=process.env.API_API_URL+'GetUserPastTimeSheetData?contactIdFillFor='+this.state.staffContactID;
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
      //body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
    //  console.log("responseJson GetUserPastTimeSheetData");
    //  console.log(data);
    //  console.log(data.data);
      // debugger;
      if (data.responseType === "1") {
        // Profile & Contact
        if(data.data != null){
          this.setState({ ListGridPast: data.data });
        }
        
        //console.log(data.data.userSkillInfo);
        //this.setState({ ListGrid: this.rowData(data.data.userSkillInfo) })
      }else{
        if(data.message == 'Authorization has been denied for this request.'){
          SystemHelpers.SessionOut();
          this.props.history.push("/login");
        }else{
          SystemHelpers.ToastError(data.message);
        }
      }
      this.hideLoader();
    })
    .catch(error => {
    //  console.log('GetUserPastTimeSheetData error');
      this.props.history.push("/error-500");
    });
  }

  Edit_Update_Btn_Func(record){
    let return_push = [];

    //if(this.props.human_resource_can_update == true || this.props.human_resource_can_delete == true){
      let Edit_push = [];
      //if(record.wantToDisplayInTimeSheet == true && record.payTimeOffId == '' ){
        Edit_push.push(
          <a href="#" onClick={this.EditRecord(record)} className="dropdown-item" data-toggle="modal" data-target="#CurrentTimeStaff_Edit_modal"><i className="fa fa-pencil m-r-5" /> Edit</a>
        );
      //}
      
      
      // 
      // onClick={this.DeleteInfo(record)}
      let Delete_push = [];
      
      Delete_push.push(
        <a href="#" onClick={this.DeleteInfo(record)}  className="dropdown-item" data-toggle="modal" data-target="#timesheet_Delete_modal"><i className="fa fa-trash-o m-r-5" /> Delete</a>
      );
      
      
      return_push.push(
        <div className="dropdown dropdown-action">
          <a aria-expanded="false" data-toggle="dropdown" className="action-icon dropdown-toggle" href="#"><i className="material-icons">more_vert</i></a>
          <div className="dropdown-menu dropdown-menu-right">
            {Edit_push}
            {Delete_push}
          </div>
        </div>
      );
    //}
    return return_push;
  }

  DeleteInfo = (record) => e =>{
    e.preventDefault();
    console.log(record);

    var isAmendment = false;
    if(this.state.isTimeSheetSubmitted == true){
      isAmendment = true;
    }else{
      isAmendment = false;
    }
    this.setState({ isAmedmends: isAmendment });
    
    var temparray = '';

    var ListGrid = this.state.ListGridPastDetails;

    for (var z = 0; z < ListGrid.length; z++)
    {
      if(ListGrid[z].tempGuid == record.tempGuid)
      {
        temparray=temparray+ListGrid[z].timeSheetTransactionId+",";
      }  
    }
    temparray=temparray.substring(0, temparray.length - 1);
    //this.setState({ DeletetimesheetTransactionId: record.timeSheetTransactionId });
    this.setState({ DeletetimesheetTransactionId: temparray });

    //console.log('temparray');
    //console.log(temparray);
  }

  DeleteRecord = () => e => {
    e.preventDefault();
    this.showLoader();
    console.log("DeletetimesheetTransactionId");
    console.log(this.state.DeletetimesheetTransactionId);
    //return false;
    var url=process.env.API_API_URL+'DeleteUserTimeSheetByTransactionId?timesheetTransactionId='+this.state.DeletetimesheetTransactionId+'&userName='+this.state.fullName;
    fetch(url, {
      method: 'PUT',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
      //body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
      //  console.log("responseJson DeleteUserTimeSheetByTransactionId");
      //  console.log(data);
        //console.log(data.data.userRole);
        // debugger;
        if (data.responseType === "1") {
            // Profile & Contact
            this.GetUserTimeSheetDataByLocation();
            SystemHelpers.ToastSuccess(data.responseMessge);
            $( ".cancel-btn" ).trigger( "click" );
            
            this.hideLoader();
        }else if (data.responseType == "2" || data.responseType == "3") {
            SystemHelpers.ToastError(data.responseMessge);
            $( ".cancel-btn" ).trigger( "click" );
            this.hideLoader();
        }else{
              if(data.message == 'Authorization has been denied for this request.'){
                SystemHelpers.SessionOut();
                this.props.history.push("/login");
              }else{
                SystemHelpers.ToastError(data.message);
              }
              this.hideLoader();
              $( ".cancel-btn" ).trigger( "click" );
        }
        
        
    })
    .catch(error => {
    //  console.log('DeleteUserTimeSheetByTransactionId error');
      this.props.history.push("/error-500");
    });
  }

  EditRecord = (record) => e => {

    console.log(record);
    this.setState({ timeSheetTransactionId: record.timeSheetTransactionId });
    this.setState({ EditPreference: [] });
    this.setState({ AddPreference: [] });

    this.setState({ EditDate: moment(record.timeSheetDate,process.env.API_DATE_FORMAT).format('YYYY-MM-DD') });
    this.setState({ EditTimeIn: moment(record.timeIN) });
    this.setState({ EditTimeOut:  moment(record.timeOUT) });

    this.setState({ recordType:  record.recordType });
    this.setState({ isAmendmends:  record.isAmendmends });
    
    //this.setState({ EditTimeOut_Min: moment(moment(record.timeIN).subtract(1, 'day').format('YYYY/MM/DD HH:mm A'))});
    //this.setState({ EditTimeOut_Max: moment(moment(record.timeIN).add(1, 'day').format('YYYY/MM/DD HH:mm A'))});

    this.setState({ EditTimeOut_Min: moment(record.timeIN).subtract(1, 'day')});
    this.setState({ EditTimeOut_Max: moment(record.timeIN).add(2, 'day')});

    if(record.serviceId ==  process.env.API_24HOURS_SERVICE ){
      this.setState({ EditTimeOutDisabled: true});
    }else{
      this.setState({ EditTimeOutDisabled: false});
    }

    this.setState({ Editlocation: record.timeLocationId });
    this.setState({ EditService: record.serviceId });
    this.setState({ EditNotes: record.note });
    this.setState({ EditNoOfHours: record.noOfHours });

    //alert(record.payTimeOffId);
    
    this.setState({ EditPreferenceService: record.payTimeOffId });

    if(record.payTimeOffId !='' && record.payTimeOffId != null){
      this.GetPayOffDetailsForUser(record.serviceId,record.timeOUT,true,record.timeLocationId,'Edit');
    }

    if(record.serviceId == process.env.API_ON_CALL){
      $('#EditService').prop('disabled', true);
      $('#Editlocation').prop('disabled', true);
      this.setState({ EditTimeInDisabled: true });
      this.setState({ EditTimeOutDisabled: true });
    }else{
      $('#EditService').prop('disabled', false);
      $('#Editlocation').prop('disabled', false);
      this.setState({ EditTimeInDisabled: false });
      this.setState({ EditTimeOutDisabled: false });
    }
  }

  GetProfile(){
  //  console.log(localStorage.getItem("token"));
    this.showLoader();
    var url=process.env.API_API_URL+'GetUserBasicInfoById?contactId='+this.state.staffContactID;
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
      //body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
        console.log("responseJson GetUserBasicInfoById attendancesummary");
        console.log(data);
        //console.log(data.data.userRole);
        // debugger;
        
         
        if (data.responseType === "1") {
            this.setState({ all_data: data.data});
            
            this.setState({ cellPhone: data.data.cellPhone});
            this.setState({ phssEmail: data.data.phssEmail});
            this.setState({ Preferred_Name: data.data.preferredName});
            this.setState({ get_profile_call: true});
            this.setState({ staffNumberId: data.data.staffNumberId});
            this.setState({ primaryLocationId: data.data.primaryLocationId}); 
            this.setState({ primarylocation: data.data.primaryLocation}); 
            this.setState({ reportToUserName: data.data.reportToUserName});  
            this.setState({ userRoleDisplay: data.data.userRoleDisplay});
            this.setState({ isPayrollAdmin: data.data.isPayrollAdmin}); 
            this.setState({ is_btn_display: true});
            this.setState({ Addlocation: data.data.locationIdGuid});

            // if(this.state.isPayrollAdminSession == false){
            //   this.setState({ locationViews: data.data.locationList });
            // }
            
            
            //this.setState({ ContactlocationID: this.GetLocationID(data.data.primaryLocation)});
          //  console.log('primaryLocationId' + data.data.primaryLocationId);
            
        }else{
              if(data.message == 'Authorization has been denied for this request.'){
                SystemHelpers.SessionOut();
                this.props.history.push("/login");
              }else{
                SystemHelpers.ToastError(data.message);
              }
              
        }
        this.hideLoader();
        
    })
    .catch(error => {
    //  console.log('GetUserBasicInfoById error');
      this.props.history.push("/error-500");
    });
  }

  GetTimeSheetId(TimeSheetStartDate){
  //  console.log(TimeSheetStartDate);
      var timeSheetPeriod = this.state.timeSheetPeriodViews;
      var length = timeSheetPeriod.length;

      if (length > 0) {
        var i = 1;
        for (var zz = 0; zz < length; zz++) {

          var StartDate = moment(timeSheetPeriod[zz].timeSheetPeriodStartDate,process.env.API_DATE_FORMAT).format('YYYY-MM-DD');
          var EndDate = moment(timeSheetPeriod[zz].timeSheetPeriodEndDate,process.env.API_DATE_FORMAT).format('YYYY-MM-DD');

          if(StartDate<=TimeSheetStartDate &&  TimeSheetStartDate<= EndDate){
            return timeSheetPeriod[zz];
          }

          if(i == length){
            return false;
          }

          i++;
        }
      } 
  }

  ClearRecord = ()=> e => {
    e.preventDefault();

    this.setState({ AddTimeIn : '' });
    this.setState({ AddTimeOut : '' });
    //this.setState({ Addlocation : '' });
    this.setState({ Addlocation: this.state.locationIdGuid}); 
    this.setState({ AddNotes : '' });
    this.setState({ AddNoOfHours : '' });
    this.setState({ AddService : '' });

    this.setState({ filePreviewsFinalEdit: [] });
    this.setState({ filePreviewsFinalEditActive: 0 });
    this.setState({ allow_emp: false });
    this.setState({ EditNoteId: '' });
    this.setState({ AddMsg: '' });

    this.setState({ errormsg: '' });
  }

  AddRecord = () => e => {
    //debugger;
    e.preventDefault();

    let step1Errors = {};

    var timeSheetStartDate = '';
    var timeSheetEndDate = '';
    var timeSheetContactMasterId = '';
    var timeSheetPeriodId = '';
    var dueDate = '';
    
    var TimeSheet_PeriodViews = this.state.timeSheetPeriodViews;

  //  console.log('timeSheetPeriodViews');
  //  console.log(TimeSheet_PeriodViews);
    var IsEntitlemntBasedOnStartDate = true;
    if(this.state["AddTimeIn"] != "")
    {
      

      for (var i = 0; i < TimeSheet_PeriodViews.length; i++) {
        // this.state["AddTimeOut"]

        var checkStartDate = moment(this.state["AddTimeIn"]).format('YYYY-MM-DD');
        var checkEndDate = moment(this.state["AddTimeOut"]).format('YYYY-MM-DD');

        if(checkStartDate != checkEndDate){
          

          var FirstDay1 = moment(this.state["AddTimeIn"]).format('YYYY-MM-DD HH:mm');
          var FirstDay2 = checkStartDate+" 24:00";

          var SecondDay1 = checkEndDate+" 00:00";
          var SecondDay2 = moment(this.state["AddTimeOut"]).format('YYYY-MM-DD HH:mm');

          console.log("FirstDay1 => "+FirstDay1);
          console.log("FirstDay2 => "+FirstDay2);

          console.log("SecondDay1 => "+SecondDay1);
          console.log("SecondDay2 => "+SecondDay2);

          var duration1 = moment.duration(moment(FirstDay2).diff(moment(FirstDay1)));
          var FirstDayMinutes = duration1.asMinutes();

          var duration2 = moment.duration(moment(SecondDay2).diff(moment(SecondDay1)));
          var SecondDayMinutes = duration2.asMinutes();

          console.log(FirstDayMinutes);
          console.log(SecondDayMinutes);

          if(FirstDayMinutes > SecondDayMinutes && FirstDayMinutes != SecondDayMinutes){
            var checkEndDate = moment(this.state["AddTimeIn"]).format('YYYY-MM-DD');
            IsEntitlemntBasedOnStartDate = true;
          }else{
            var checkEndDate = moment(this.state["AddTimeOut"]).format('YYYY-MM-DD');
            IsEntitlemntBasedOnStartDate = false;
          }

        }else{
          var checkEndDate = moment(this.state["AddTimeOut"]).format('YYYY-MM-DD');
          IsEntitlemntBasedOnStartDate = false;
        }

        console.log(checkEndDate);

        //var checkEndDate = moment(this.state["AddTimeOut"]).format('YYYY-MM-DD')
        //var checkEndDate = moment(this.state["AddTimeIn"]).format('YYYY-MM-DD')
        var start_date = moment(TimeSheet_PeriodViews[i].timeSheetPeriodStartDate,process.env.API_DATE_FORMAT).format('YYYY-MM-DD');
        var end_date = moment(TimeSheet_PeriodViews[i].timeSheetPeriodEndDate,process.env.API_DATE_FORMAT).format('YYYY-MM-DD');
        //var check_is_exists = moment(checkEndDate).isBetween(start_date, end_date);
        var check_is_exists = moment(checkEndDate).isBetween(start_date, end_date, null, '[]');
      //  console.log('start_date => '+ start_date + ' end_date => '+ end_date + ' checkEndDate => '+ checkEndDate);
      //  console.log('time sheet check end date');
      //  console.log(check_is_exists);

        if(check_is_exists == true || end_date == checkEndDate)
        {
          timeSheetStartDate = start_date;
          timeSheetEndDate = end_date;

        //  console.log('TimeSheet_PeriodViews[i]');
        //  console.log(TimeSheet_PeriodViews[i]);

          if(TimeSheet_PeriodViews[i].timeSheetContactMasterId != null && TimeSheet_PeriodViews[i].timeSheetContactMasterId != ''){
            timeSheetContactMasterId = TimeSheet_PeriodViews[i].timeSheetContactMasterId;
          //  console.log('timeSheetContactMasterId 123 => '+timeSheetContactMasterId);
          }else{
            timeSheetContactMasterId = '';
          }
          
          timeSheetPeriodId = TimeSheet_PeriodViews[i].timeSheetPeriodId;
          dueDate = TimeSheet_PeriodViews[i].dueDate;
        }
      //  console.log('timeSheetContactMasterId => '+timeSheetContactMasterId);
      }
    }
    //return false;

    // if (this.state["AddDate"] =='') {
    //   step1Errors["AddDate"] = "Date is mandatory";
    // }

    var AddTimeInDate = moment(this.state["AddTimeIn"]).format("MM-DD-YYYY");
    // var sheetinfo = this.GetTimeSheetId(AddTimeInDate);
    // console.log('GetTimeSheetId');
    // console.log(sheetinfo);

    // if(sheetinfo  == false){
    //   step1Errors["AddTimeIn"] = "Please Check Your Timesheet Date.";
    // }
    
    if (this.state["AddTimeIn"] == '') {
      step1Errors["AddTimeIn"] = "TimeIn is mandatory";
    }

    if (this.state["AddTimeOut"] == '') {
      step1Errors["AddTimeOut"] = "TimeOut is mandatory";
    }

    var AddTimeInDateTemp = moment(this.state["AddTimeIn"]).format("YYYY-MM-DD");
    var AddTimeOutDateTemp = moment(this.state["AddTimeOut"]).format("YYYY-MM-DD");
    //console.log("AddTimeInDateTemp =>"+AddTimeInDateTemp+" CurrentDate_Min =>"+this.state.CurrentDate_Min);
    if(AddTimeInDateTemp == this.state.CurrentDate_Min && AddTimeOutDateTemp == this.state.CurrentDate_Min){
      step1Errors["AddTimeOut"] = "Time IN and Time OUT values should not belong to Previous pay period dates.";
    }

    if (this.state["Addlocation"] == '') {
      step1Errors["Addlocation"] = "Location is mandatory";
    }

    if (this.state["AddService"] == '') {
      step1Errors["AddService"] = "Service is mandatory";
    }

    //if(this.state.IsNoOfHoursvalid == false){
    if(this.state.IsNoOfHoursvalid == false && this.state.AddService != process.env.API_ON_CALL){
      step1Errors["AddNoOfHours"] = "Enter Valid TimeIn and TimeOut.";
    }

    if(this.state.AddPreference.length > 0 && this.state.AddPreferenceService == ''){
      step1Errors["AddPreferenceService"] = "Payout Preference is mandatory.";
    }

    if(timeSheetPeriodId == "")
    {
      SystemHelpers.ToastError("Please contact system administrator");
      return false;
    }


    console.log(step1Errors);


    this.setState({ errormsg: step1Errors });
        
    if (Object.keys(step1Errors).length > 0) {
        return false;
    }


    // find CategoryId, SubCategoryId, OperationType
    var CategoryId = '';
    var SubCategoryId = '';
    var OperationType = '';

    var serviceView= this.state.serviceView;
    for (var z = 0; z < serviceView.length; z++)
    {
      if(serviceView[z].serviceId == this.state["AddService"])
      {
        // if(this.state.AddPreferenceService == process.env.API_BANK_HOURS){
        //   OperationType = serviceView[z].operationType;
        // }
        OperationType = serviceView[z].operationType;
        CategoryId = serviceView[z].categoryId;
        SubCategoryId = serviceView[z].subCategoryId;
        //OperationType = serviceView[z].operationType;
      }
      
    }


    // find CategoryId, SubCategoryId, OperationType

    var isAmendment = false;
    // if(this.state.isTimeSheetSubmittedToPayroll == true && this.state.isPayrollAdminSession == true){
    //   isAmendment = true;
    // }else if(this.state.isTimeSheetSubmittedToPayroll == false && this.state.isTimeSheetApproved == true && (this.state.isCordinatorSession == true  || this.state.isSeniorCordinatorSession == true || this.state.isServiceCoordinatorLeadSession == true)){
    //   isAmendment = true;
    // }
    // else{
    //   isAmendment = false;
    // }

    console.log('isAmendment 1 => '+isAmendment);
    if(this.state.isPayrollAdminSession == true){
      if(this.state.isTimeSheetSubmittedToPayroll == true || this.state.isTimeSheetApproved == true || this.state.isTimeSheetSubmitted == true){
        isAmendment = true;
      }else{
        isAmendment = false;
      }
    }else{
      if(this.state.isTimeSheetApproved == true || this.state.isTimeSheetSubmitted == true){
        isAmendment = true;
      }else{
        isAmendment = false;
      }
    }
    

    

    console.log('isAmendment 2 => '+isAmendment);

    let currentDate = moment();
    //let currentDate = moment('2021-08-15');
    let DueDate = moment(dueDate);
    console.log('DueDate => '+dueDate);
    var CheckisAmendment = DueDate.diff(currentDate, 'days');

    if(CheckisAmendment < 0){
      isAmendment = true;
    }


    console.log('isAmendment 3 => '+isAmendment);
    //return false;
    

    this.showLoader();
    var AddTimeIn=moment(this.state["AddTimeIn"]).format('YYYY-MM-DD hh:mm A');
    var AddTimeOut=moment(this.state["AddTimeOut"]).format('YYYY-MM-DD hh:mm A');
    
    //return false;
    let ArrayJson = [];

    if(this.state.AddService == process.env.API_ON_CALL){

      var today = moment();
      let datesCollection = [];
      console.log(this.state["AddTimeIn"]);
      console.log(this.state["AddTimeOut"]);
      var startdate = moment(this.state["AddTimeIn"], "DD/MM/YYYY");

      if(this.state["AddTimeOut"] != ''){
        var enddate = moment(this.state["AddTimeOut"], "DD/MM/YYYY");
        var diff_day_count = enddate.diff(startdate, 'days') // 1
      }else{
        var diff_day_count = 1;
      }
      

      console.log(diff_day_count);
      var z = 0;
      var zx = 1;

      var TempTimeIn=moment(this.state["AddTimeIn"]).format('hh:mm A');
      var TempTimeOut=moment(this.state["AddTimeOut"]).format('hh:mm A');

      ArrayJson.push({
            serviceId: this.state["AddService"],
            timeSheetDate : AddTimeInDate,
            PayTimeOffId: this.state["AddPreferenceService"],
            timeIN: AddTimeIn,
            timeOUT: AddTimeOut,
            timeLocationId: this.state["Addlocation"],
            note: this.state["AddNotes"],
            noOfHours: '00:00',
            timeSheetTransactionId:'',
            IsAmendmends:isAmendment,
            RecordType:'',
            WantToDisplayInTimesheet:true,
            categoryId:CategoryId,
            subCategoryId:SubCategoryId,
            operationType:OperationType,
            IsEntitlemntBasedOnStartDate : IsEntitlemntBasedOnStartDate
      });

      for (var i = 0; i <= diff_day_count; i++) {
        var AddTimeIn = moment(this.state["AddTimeIn"]).add(z, 'days').format('YYYY-MM-DD');
        var AddTimeOut = moment(this.state["AddTimeIn"]).add(zx, 'days').format('YYYY-MM-DD');

        ArrayJson.push({
            serviceId: this.state["AddService"],
            timeSheetDate : AddTimeIn,
            PayTimeOffId: this.state["AddPreferenceService"],
            timeIN: AddTimeIn+" "+TempTimeIn,
            timeOUT: AddTimeIn+" "+TempTimeOut,
            timeLocationId: this.state["Addlocation"],
            note: this.state["AddNotes"],
            noOfHours: '00:00',
            timeSheetTransactionId:'',
            IsAmendmends:isAmendment,
            RecordType:'',
            WantToDisplayInTimesheet:false,
            categoryId:CategoryId,
            subCategoryId:SubCategoryId,
            operationType:OperationType,
            IsEntitlemntBasedOnStartDate : IsEntitlemntBasedOnStartDate
        });

        z++;
        zx++;
      }

    }else{

      var AddTimeInDate = moment(checkEndDate,'YYYY-MM-DD').format("MM-DD-YYYY");
      ArrayJson.push({
            serviceId: this.state["AddService"],
            timeSheetDate : AddTimeInDate,
            PayTimeOffId: this.state["AddPreferenceService"],
            timeIN: AddTimeIn,
            timeOUT: AddTimeOut,
            timeLocationId: this.state["Addlocation"],
            note: this.state["AddNotes"],
            noOfHours: this.state["AddNoOfHours"],
            timeSheetTransactionId:'',
            IsAmendmends:isAmendment,
            RecordType:'',
            WantToDisplayInTimesheet:true,
            categoryId:CategoryId,
            subCategoryId:SubCategoryId,
            operationType:OperationType,
            IsEntitlemntBasedOnStartDate : IsEntitlemntBasedOnStartDate
      });
    }


    console.log(ArrayJson);
    //return false;
    //var timeSheetStartDate = moment(sheetinfo.timeSheetPeriodStartDate).format("YYYY-MM-DD");
    //var timeSheetEndDate = moment(sheetinfo.timeSheetPeriodEndDate).format("YYYY-MM-DD");

    //var timeSheetStartDate = moment(this.state.CurrentDate_Min).format("YYYY-MM-DD");
    //var timeSheetEndDate = moment(this.state.CurrentDate_Max).format("YYYY-MM-DD");
    
    //var timeSheetPeriodId = this.state.timeSheetPeriodId;
    var timeSheetContactMasterId=this.state.EdittimeSheetContactMasterId;

    var IsOnCall = false;
    if(this.state.AddService == process.env.API_ON_CALL){
      var IsOnCall = true;
    }

    let bodyarray = {};
    bodyarray["timeSheetContactMasterId"] = timeSheetContactMasterId;
    bodyarray["isTimeSheetSubmitted"] = false;
    bodyarray["TimeSheetPeriodId"] = timeSheetPeriodId;
    bodyarray["timeSheetStartDate"] = timeSheetStartDate;
    bodyarray["timeSheetEndDate"] = timeSheetEndDate;
    bodyarray["timeSheetContactId"] = this.state.EdittimeSheetContactId;
    bodyarray["timeSheetCreateBy"] = this.state.EdittimeSheetContactId;
    bodyarray["IsOnCall"] = IsOnCall;
    bodyarray["UpdateByName"] = this.state.fullName;
    bodyarray["timeSheetConatctSheetViews"] = ArrayJson;

  //  console.log(bodyarray);
    //return false;
    var url=process.env.API_API_URL+'InsertUpdateUserTimeSheetData';
    fetch(url, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
      body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
      //  console.log("responseJson userSkillInfo");
      //  console.log(data);
        //console.log(responseJson);
        // debugger;
        if (data.responseType === "1") {
            //this.props.history.push('/dashboard');
            this.setState({ AddTimeIn : '' });
            this.setState({ AddTimeOut : '' });
            //this.setState({ Addlocation : '' });
            this.setState({ Addlocation: ''}); 
            this.setState({ AddNotes : '' });
            this.setState({ AddNoOfHours : '' });
            this.setState({ AddService : '' });
            this.setState({ AddPreferenceService : '' });
            
            //this.setState({ AllSubCategory : [] });
            //AllSubCategory userSkillCategories


            SystemHelpers.ToastSuccess(data.responseMessge);
            this.setState({ timeSheetPeriodViews: data.data});
            
            $( ".close" ).trigger( "click" );

            this.GetUserTimeSheetDataByLocation();

            //this.GetUserTimeSheetDataByLocation();
            
            
            

              
        }
        else if(data.responseType === "2"){
           SystemHelpers.ToastError(data.responseMessge);
          // $( ".close" ).trigger( "click" );
        }
        else{
            SystemHelpers.ToastError(data.responseMessge);
        }
        this.hideLoader();
        
    })
    .catch(error => {
      this.props.history.push("/error-500");
    });
    return false;
  }

  UpdateRecord = () => e => {
    //debugger;
    e.preventDefault();

    let step1Errors = {};


    var timeSheetStartDate = '';
    var timeSheetEndDate = '';
    var timeSheetContactMasterId = '';
    var timeSheetPeriodId = '';
    var dueDate = '';
    
    var TimeSheet_PeriodViews = this.state.timeSheetPeriodViews;

  //  console.log('timeSheetPeriodViews');
  //  console.log(TimeSheet_PeriodViews);
    var IsEntitlemntBasedOnStartDate = true;
   
    if(this.state["EditTimeIn"] != "")
    {
      

      for (var i = 0; i < TimeSheet_PeriodViews.length; i++) {
        // this.state["AddTimeOut"]

        var checkStartDate = moment(this.state["EditTimeIn"]).format('YYYY-MM-DD');
        var checkEndDate = moment(this.state["EditTimeOut"]).format('YYYY-MM-DD');

        if(checkStartDate != checkEndDate){
          

          var FirstDay1 = moment(this.state["EditTimeIn"]).format('YYYY-MM-DD HH:mm');
          var FirstDay2 = checkStartDate+" 24:00";

          var SecondDay1 = checkEndDate+" 00:00";
          var SecondDay2 = moment(this.state["EditTimeOut"]).format('YYYY-MM-DD HH:mm');

          console.log("FirstDay1 => "+FirstDay1);
          console.log("FirstDay2 => "+FirstDay2);

          console.log("SecondDay1 => "+SecondDay1);
          console.log("SecondDay2 => "+SecondDay2);

          var duration1 = moment.duration(moment(FirstDay2).diff(moment(FirstDay1)));
          var FirstDayMinutes = duration1.asMinutes();

          var duration2 = moment.duration(moment(SecondDay2).diff(moment(SecondDay1)));
          var SecondDayMinutes = duration2.asMinutes();

          console.log(FirstDayMinutes);
          console.log(SecondDayMinutes);

          if(FirstDayMinutes > SecondDayMinutes && FirstDayMinutes != SecondDayMinutes){
            var checkEndDate = moment(this.state["EditTimeIn"]).format('YYYY-MM-DD');
            IsEntitlemntBasedOnStartDate = true;
          }else{
            var checkEndDate = moment(this.state["EditTimeOut"]).format('YYYY-MM-DD');
            IsEntitlemntBasedOnStartDate = false;
          }

        }else{
          var checkEndDate = moment(this.state["EditTimeOut"]).format('YYYY-MM-DD');
          IsEntitlemntBasedOnStartDate = false;
        }

        console.log(checkEndDate);
        //var checkEndDate = moment(this.state["EditTimeOut"]).format('YYYY-MM-DD');
        //var checkEndDate = moment(this.state["EditTimeIn"]).format('YYYY-MM-DD')
        var start_date = moment(TimeSheet_PeriodViews[i].timeSheetPeriodStartDate,process.env.API_DATE_FORMAT).format('YYYY-MM-DD');
        var end_date = moment(TimeSheet_PeriodViews[i].timeSheetPeriodEndDate,process.env.API_DATE_FORMAT).format('YYYY-MM-DD');
        //var check_is_exists = moment(checkEndDate).isBetween(start_date, end_date);
        var check_is_exists = moment(checkEndDate).isBetween(start_date, end_date, null, '[)');
      //  console.log('start_date => '+ start_date + ' end_date => '+ end_date + ' checkEndDate => '+ checkEndDate);
      //  console.log('time sheet check end date');
      //  console.log(check_is_exists);
        if(check_is_exists == true || end_date == checkEndDate)
        {
          timeSheetStartDate = start_date;
          timeSheetEndDate = end_date;
          timeSheetContactMasterId = TimeSheet_PeriodViews[i].timeSheetContactMasterId;
          timeSheetPeriodId = TimeSheet_PeriodViews[i].timeSheetPeriodId;
          dueDate = TimeSheet_PeriodViews[i].dueDate;
        }
      }
    }
    var timeSheetContactMasterId = this.state.EdittimeSheetContactMasterId; 
    //return false;
    
    // if (this.state["EditDate"] =='') {
    //   step1Errors["EditDate"] = "Date is mandatory";
    // }

 
    if (this.state["EditTimeIn"] == '') {
      step1Errors["EditTimeIn"] = "TimeIn is mandatory";
    }

    if (this.state["EditTimeOut"] == '') {
      step1Errors["EditTimeOut"] = "TimeOut is mandatory";
    }

    var EditTimeInDateTemp = moment(this.state["EditTimeIn"]).format("YYYY-MM-DD");
    var EditTimeOutDateTemp = moment(this.state["EditTimeOut"]).format("YYYY-MM-DD");
    if(EditTimeInDateTemp == this.state.CurrentDate_Min && EditTimeOutDateTemp == this.state.CurrentDate_Min){
      step1Errors["EditTimeOut"] = "Time IN and Time OUT values should not belong to Previous pay period dates.";
    }
    
    if (this.state["Editlocation"] == '') {
      step1Errors["Editlocation"] = "Location is mandatory";
    }

    if (this.state["EditService"] == '') {
      step1Errors["EditService"] = "Service is mandatory";
    }

    if(this.state.IsNoOfHoursvalidEdit == false){
      step1Errors["EditNoOfHours"] = "Enter Valid TimeIn and TimeOut.";
    }

    if(timeSheetPeriodId == "")
    {
      SystemHelpers.ToastError("Please contact system administrator");
      return false;
    }


  //  console.log(step1Errors);

    this.setState({ errormsg: step1Errors });
        
    if (Object.keys(step1Errors).length > 0) {
        return false;
    }


    // find CategoryId, SubCategoryId, OperationType
    var CategoryId = '';
    var SubCategoryId = '';
    var OperationType = '';

    var serviceView= this.state.serviceView;
    for (var z = 0; z < serviceView.length; z++)
    {
      if(serviceView[z].serviceId == this.state["EditService"])
      {
        // if(this.state.EditPreferenceService == process.env.API_BANK_HOURS){
        //   OperationType = serviceView[z].operationType;
        // }
        OperationType = serviceView[z].operationType;
        CategoryId = serviceView[z].categoryId;
        SubCategoryId = serviceView[z].subCategoryId;
        //OperationType = serviceView[z].operationType;
      }
      
    }


    // find CategoryId, SubCategoryId, OperationType

    

    this.showLoader();
    // var EditTimeIn=moment(this.state["EditDate"]+"T"+this.state["EditTimeIn"]).format('hh:mm A');
    // var EditTimeOut=moment(this.state["EditDate"]+"T"+this.state["EditTimeOut"]).format('hh:mm A');
    
    var EditTimeIn=moment(this.state["EditTimeIn"]).format('YYYY-MM-DD hh:mm A');
    var EditTimeOut=moment(this.state["EditTimeOut"]).format('YYYY-MM-DD hh:mm A');

  //  console.log("isTimeSheetSubmittedToPayroll  =>"+this.state.isTimeSheetSubmittedToPayroll);
  //  console.log("isTimeSheetApproved  =>"+this.state.isTimeSheetApproved);
  //  console.log("isPayrollAdminSession =>"+this.state.isPayrollAdminSession);
  //  console.log("isCordinatorSession  =>"+this.state.isCordinatorSession);
  //  console.log("isSeniorCordinatorSession  =>"+this.state.isSeniorCordinatorSession);
  //  console.log("isServiceCoordinatorLeadSession  =>"+this.state.isServiceCoordinatorLeadSession);

    var isAmendment = false;
    // if(this.state.isTimeSheetSubmittedToPayroll == true && this.state.isPayrollAdminSession == true){
    //   isAmendment = true;
    // }else if(this.state.isTimeSheetSubmittedToPayroll == false && this.state.isTimeSheetApproved == true && (this.state.isCordinatorSession == true  || this.state.isSeniorCordinatorSession == true || this.state.isServiceCoordinatorLeadSession == true)){
    //   isAmendment = true;
    // }
    // else{
    //   isAmendment = false;
    // }

    if(this.state.isPayrollAdminSession == true){
      if(this.state.isTimeSheetSubmittedToPayroll == true || this.state.isTimeSheetApproved == true || this.state.isTimeSheetSubmitted == true){
        isAmendment = true;
      }else{
        isAmendment = false;
      }
    }else{
      if(this.state.isTimeSheetApproved == true || this.state.isTimeSheetSubmitted == true){
        isAmendment = true;
      }else{
        isAmendment = false;
      }
    }

    if(this.state.isAmendmendsEnabled == true){
      isAmendment = true;
    }

    


    let currentDate = moment();
    //let currentDate = moment('2021-08-15');
    let DueDate = moment(dueDate);

    var CheckisAmendment = DueDate.diff(currentDate, 'days');

    if(CheckisAmendment < 0){
      isAmendment = true;
    }

    var EditTimeInDate = moment(checkEndDate,'YYYY-MM-DD').format("MM-DD-YYYY");

    let ArrayJson = [{
          serviceId: this.state["EditService"],
          timeSheetDate: EditTimeInDate,
          PayTimeOffId: this.state["EditPreferenceService"],
          timeIN: EditTimeIn,
          timeOUT: EditTimeOut,
          timeLocationId: this.state["Editlocation"],
          note: this.state["EditNotes"],
          noOfHours: this.state["EditNoOfHours"],
          timeSheetTransactionId:this.state.timeSheetTransactionId,
          isAmendmends:isAmendment,
          recordType:this.state.recordType,
          WantToDisplayInTimesheet:true,
          categoryId:CategoryId,
          subCategoryId:SubCategoryId,
          operationType:OperationType,
          IsEntitlemntBasedOnStartDate : IsEntitlemntBasedOnStartDate
    }];

    var IsOnCall = false;
    if(this.state.EditService == process.env.API_ON_CALL){
      var IsOnCall = true;
    }
     
    let bodyarray = {};
    
    bodyarray["timeSheetContactMasterId"] = timeSheetContactMasterId;
    bodyarray["isTimeSheetSubmitted"] = false;
    bodyarray["TimeSheetPeriodId"] = timeSheetPeriodId;
    bodyarray["timeSheetStartDate"] = timeSheetStartDate;
    bodyarray["timeSheetEndDate"] = timeSheetEndDate;
    bodyarray["timeSheetContactId"] = this.state.EdittimeSheetContactId;
    bodyarray["timeSheetCreateBy"] = this.state.staffContactID;
    bodyarray["IsOnCall"] = IsOnCall;
    bodyarray["UpdateByName"] = this.state.fullName;
    bodyarray["timeSheetConatctSheetViews"] = ArrayJson;

  //  console.log(bodyarray);
    //return false;
    var url=process.env.API_API_URL+'InsertUpdateUserTimeSheetData';
    fetch(url, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
      body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
      //  console.log("responseJson InsertUpdateUserTimeSheetData");
      //  console.log(data);
        //console.log(responseJson);
        // debugger;
        if (data.responseType === "1") {
            //this.props.history.push('/dashboard');

            
            //this.setState({ AllSubCategory : [] });
            //AllSubCategory userSkillCategories
            
            this.setState({ EditNotes : '' });

            SystemHelpers.ToastSuccess(data.responseMessge);


            this.GetUserTimeSheetDataByLocation();

           
           
            
            $( ".close" ).trigger( "click" );
            
            
        }
        else{
            if(data.message == 'Authorization has been denied for this request.'){
                SystemHelpers.SessionOut();
                this.props.history.push("/login");
              }else{
                SystemHelpers.ToastError(data.responseMessge);
              }
        }
        this.hideLoader();
        
    })
    .catch(error => {
      this.props.history.push("/error-500");
    });
    return false;
  }

  

  GetUserLocations(){
  //  console.log(localStorage.getItem("token"));
    this.showLoader();
    var url=process.env.API_API_URL+'GetUserLocations?contactId='+localStorage.getItem("contactId");
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
      //body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
      //  console.log("responseJson GetUserLocations");
      //  console.log(data);
        //console.log(data.data.userRole);
        // debugger;
        
         
        if (data.responseType === "1") {
            this.setState({ LocationList: data.data });
            this.GetProfile();
        }else{
              if(data.message == 'Authorization has been denied for this request.'){
                SystemHelpers.SessionOut();
                this.props.history.push("/login");
              }else{
                //SystemHelpers.ToastError(data.responseMessge);
              }
              
        }
        this.hideLoader();
        
    })
    .catch(error => {
    //  console.log('GetUserLocations error');
      this.props.history.push("/error-500");
    });
  }

  GetLocationID(LocationName){
    var LocationList = this.state.LocationList;
    var length = this.state.LocationList.length;
    for (var zz = 0; zz < length; zz++) {
      if(LocationList[zz].locationName ==LocationName){
        return LocationList[zz].locationId;
      }
    }
  }

  PastDetails = (role,timesheetPeriodId,contactId,tempfullName,isTimeSheetApproved,isTimeSheetSubmittedToPayroll,isSpecifcTimeSheetApprover,locationId,isPayrollAdmin,LocationGUID,IsEditBtn) => e => {
    e.preventDefault();
    this.showLoader();
    console.log("contactId => "+contactId+" isPayrollAdmin => "+isPayrollAdmin);
    console.log('role');
    console.log(role);
    this.GetUserLocationWise(contactId,isPayrollAdmin);

    this.GetTimseheetEntitlementSummarySummationLocationWsie(contactId,LocationGUID,timesheetPeriodId);

    this.setState({ role_permission : role });

    this.setState({ past_display_amendment_due_date: '' });
    this.setState({ past_display_dueDateAmendment: '' });
  //  console.log('timesheetPeriodId =>'+ timesheetPeriodId);
  //  console.log('contactId =>'+ contactId);
  //  console.log('tempfullName =>'+ tempfullName);
  //  console.log('isTimeSheetApproved =>'+ isTimeSheetApproved);
  //  console.log('isTimeSheetSubmittedToPayroll =>'+ isTimeSheetSubmittedToPayroll);
  //  console.log('isSpecifcTimeSheetApprover =>'+ isSpecifcTimeSheetApprover);

  //  console.log("isPayrollAdminSession =>"+this.state.isPayrollAdminSession);
  //  console.log("isCordinatorSession  =>"+this.state.isCordinatorSession);
  //  console.log("isSeniorCordinatorSession  =>"+this.state.isSeniorCordinatorSession);
  //  console.log("isServiceCoordinatorLeadSession  =>"+this.state.isServiceCoordinatorLeadSession);
    
    this.setState({ isTimeSheetSubmitted: false });
    this.setState({ ListGridPastDetails: [] });
    this.setState({ ListGridPastDetailsCount: 0 });
    this.setState({ GetChatListLoad: false });
    
    this.setState({ EdittimeSheetContactId: contactId });
    this.setState({ EdittimesheetPeriodId: timesheetPeriodId });
    this.setState({ EdittimeSheetContactMasterId: ''});
    //alert(isTimeSheetApproved);
    //alert(isTimeSheetSubmittedToPayroll);
    //alert(IsEditBtn);

    this.setState({ IsEditBtn: IsEditBtn });
    this.setState({ tempfullName: tempfullName });

    this.setState({ isAmendmendsSubmitted : false });
    this.setState({ isTimeSheetSubmitedToPayroll : false });

    this.setState({ Addlocation: LocationGUID});
   
    $('#Addlocation').prop('disabled', true);
    $('#Addlocation').css("background-color", "lightgray");
    

    this.setState({ isTimeSheetApproved: false });

    if(!isTimeSheetSubmittedToPayroll){
      this.setState({ isTimeSheetSubmittedToPayroll: false });
    }else{
      this.setState({ isTimeSheetSubmittedToPayroll: isTimeSheetSubmittedToPayroll });
    }
    
    // let locationid = "";
    // if(localStorage.getItem("isPayrollAdmin") == 'false')
    // {
    //   locationid = localStorage.getItem("primaryLocationId");
    // } 

    this.setState({ templocationId: '' });   
    
    //alert(localStorage.getItem("isPayrollAdmin"));
    //alert(locationid);
    this.setState({ EditworkedHour: '00:00:00 h' });
    var url=process.env.API_API_URL+'GetUserPastTimeSheetDataById?contactIdFillFor='+contactId+"&timesheetPeriodId="+timesheetPeriodId+"&locationId="+locationId+"&dedicatedApprover="+isSpecifcTimeSheetApprover;
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
      //body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
      console.log("responseJson GetUserPastTimeSheetDataById");
      console.log(data);
      //console.log(data.data.timeSheetConatctSheetViews);
      // debugger;
      
      if (data.responseType === "1") {
        // Profile & Contact

        if(data != null){

          if(data.data.timesheetContactView != null){
              var vartimeSheetConatctSheetViews = data.data.timesheetContactView.timeSheetConatctSheetViews;
              this.setState({ ListGridPastDetailsCount: vartimeSheetConatctSheetViews.length });
              //alert(vartimeSheetConatctSheetViews.length);
              this.setState({ ListGridPastDetails: data.data.timesheetContactView.timeSheetConatctSheetViews });  
              
              

              this.setState({ past_display_amendment_due_date: data.data.timesheetContactView.dueDate });
              this.setState({ past_display_dueDateAmendment: data.data.timesheetContactView.dueDateAmendment });

              this.setState({ isTimeSheetApproved: data.data.timesheetContactView.isTimeSheetApproved });
              this.setState({ IsAmendmendsEnabled : data.data.timesheetContactView.isAmendmendsEnabled });

              this.setState({ isAmendmendsSubmitted : data.data.timesheetContactView.isAmendmendsSubmitted });
              
              this.setState({ isTimeSheetSubmitedToPayroll : data.data.timesheetContactView.isTimeSheetSubmitedToPayroll });

              this.setState({ EdittimeSheetContactMasterId: data.data.timesheetContactView.timeSheetContactMasterId });

              if(vartimeSheetConatctSheetViews.length > 0){
                this.GetTimeSheetNotes(data.data.timesheetContactView.timeSheetContactMasterId,LocationGUID);  
              }
              
              
              this.setState({ isTimeSheetSubmitted: data.data.timesheetContactView.isTimeSheetSubmitted });

              
              this.setState({ EdittimeSheetStartDate: data.data.timesheetContactView.timeSheetStartDate });
              this.setState({ EdittimeSheetEndDate: data.data.timesheetContactView.timeSheetEndDate });
              this.setState({ EditworkedHour: data.data.timesheetContactView.workedHour });
              //this.setState({ timeSheetPeriodViews: data.data.timeSheetPeriodViews });

              this.setState({ templocationId: LocationGUID });
          }

          if(data.data.timeSheetPeriodViews != null){
          //  console.log('timeSheetPeriodViews update');
            this.setState({ timeSheetPeriodViews: data.data.timeSheetPeriodViews });
          }
        }
        
        //console.log(data.data.userSkillInfo);
        //this.setState({ ListGrid: this.rowData(data.data.userSkillInfo) })
      }else{
        if(data.message == 'Authorization has been denied for this request.'){
          SystemHelpers.SessionOut();
          this.props.history.push("/login");
        }else{
          SystemHelpers.ToastError(data.message);
        }
      }
      this.hideLoader();
    })
    .catch(error => {
      console.log('GetUserPastTimeSheetDataById error');
      this.props.history.push("/error-500");
    });
  }

  GetUserLocationWise(contactId,isPayrollAdmin){
  //  console.log(localStorage.getItem("token"));
    this.setState({ locationViews: [] });
    this.showLoader();
    var url=process.env.API_API_URL+'GetUserLocationWise?contactId='+contactId+"&payRoll="+isPayrollAdmin;
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
      //body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
        console.log("responseJson GetUserLocationWise");
        console.log(data);
        //console.log(data.data.userRole);
        // debugger;
        
         
        if (data.responseType === "1") {
            this.setState({ locationViews: data.data.locationList });
            //locationIdGuid
        }else{
              if(data.message == 'Authorization has been denied for this request.'){
                SystemHelpers.SessionOut();
                this.props.history.push("/login");
              }else{
                SystemHelpers.ToastError(data.message);
              }
              
        }
        this.hideLoader();
        
    })
    .catch(error => {
      console.log('GetUserLocationWise error');
      this.props.history.push("/error-500");
    });
  }


  GetFileName(file) {
   return file.substr( (file.lastIndexOf('/') +1) );
  }
  openFile(file) {
    var extension = file.substr( (file.lastIndexOf('.') +1) );
    switch(extension) {
      case 'jpg':
      case 'png':
      case 'jpeg':
          return 'jpg';  // There's was a typo in the example where
      break;                         // the alert ended with pdf instead of gif.
      case 'xls':
      case 'xlsx':
      case 'csv':
          return 'xls';
      break;
      case 'docx':
      case 'doc':
          return 'doc';
      break;
      case 'pdf':
          return 'pdf';
      break;
      default:
        return '';
    }
  };

  DeleteMsg = (MsgDetails) => e => {
      e.preventDefault();

      var isdelete = '';
      if(MsgDetails.isDelete== true)
      {
        isdelete = false;
      }
      else
      {
        isdelete = true;
      }

      if(confirm("Are you sure you want to delete this?"))
      {
          this.showLoader();
          var url=process.env.API_API_URL+'DeleteTimesheetNotes?noteId='+MsgDetails.noteId+'&isDelete='+isdelete;
          fetch(url, {
            method: 'PUT',
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json',
              'Authorization': 'bearer '+localStorage.getItem("token")
            },
            //body: JSON.stringify(bodyarray)
          }).then((response) => response.json())
          .then(data => {
            //  console.log("responseJson DeleteLocation");
            //  console.log(data);
              //console.log(data.data.userRole);
              // debugger;
              if (data.responseType === "1") {
                  // Profile & Contact
                  //SystemHelpers.ToastSuccess(data.responseMessge);
                  //$( ".cancel-btn" ).trigger( "click" );
                  this.GetTimeSheetNotesLoad(this.state.EdittimeSheetContactMasterId,this.state.templocationId);
                  this.hideLoader();
              }else if (data.responseType == "2" || data.responseType == "3") {
                  SystemHelpers.ToastError(data.responseMessge);
                  $( ".cancel-btn" ).trigger( "click" );
                  this.hideLoader();
              }else{
                    if(data.message == 'Authorization has been denied for this request.'){
                      SystemHelpers.SessionOut();
                      this.props.history.push("/login");
                    }else{
                      SystemHelpers.ToastError(data.message);
                    }
                    this.hideLoader();
                    $( ".cancel-btn" ).trigger( "click" );
              }    
          })
          .catch(error => {
            console.log('DeleteTimesheetNotes error');
            this.props.history.push("/error-500");
          });
      }
      else{
          return false;
      }

      
  }

  GetChatList(){

    //console.log('ListGridPastDetailsCount => '+this.state.ListGridPastDetailsCount);


    let final_list_push =[];
    let list_push =[];
    var list = this.state.ListNotes;
    for (var i = 0; i<list.length; i++) {

      let file_type_push =[];

      var isFileAttached = list[i].isFileAttached;
      var timeSheetNotesFile = list[i].timeSheetNotesFile;

      let att_img_push =[];
      let att_doc_push =[];

      if(isFileAttached == true){
        for (var j = 0; j<timeSheetNotesFile.length; j++) {
            var filetype=this.openFile(timeSheetNotesFile[j].base64String);

            if(filetype == 'jpg'){
              att_img_push.push(
                <a className="chat-img-attach" href={timeSheetNotesFile[j].base64String} target="_blank">
                  <img width={182} height={137} alt="" src={timeSheetNotesFile[j].base64String} />
                  <div className="chat-placeholder">
                    {/*<div className="chat-img-name">placeholder.jpg</div>
                    <div className="chat-file-desc">Download</div>*/}
                  </div>
                </a>
              );
            }else if(filetype == 'xls' || filetype == 'doc' || filetype == 'pdf' ){
                var filenm="Notes."+filetype;
                att_doc_push.push(
                  <ul class="attach-list">
                    <li><i class="fa fa-file"></i> <a href={timeSheetNotesFile[j].base64String} target="_blank" download="Notes" >{this.GetFileName(timeSheetNotesFile[j].base64String)}</a></li>
                  </ul>
                );
            }
            
        }

        file_type_push.push(
          <div className="chat-img-group clearfix">
            <p>Uploaded Files</p>
            {att_img_push}
          </div>
        );

      }

      

      
      

      let is_delete_push =[];
      let close_btn = [];
      let edit_btn = [];
      if(list[i].contactId == this.state.staffContactID){
        if(this.state.EditNoteId != '' && this.state.EditNoteId == list[i].noteId){
          close_btn.push(
            <li><a href="#" className="del-msg" onClick={this.ClearRecord()}><i className="fa fa-close" /></a></li>
          );
        }else{
          edit_btn.push(
            <li><a href="#" className="edit-msg" onClick={this.EditMsg(list[i])}><i className="fa fa-pencil" /></a></li>
          );
        }
        is_delete_push.push(
          <ul>
            {edit_btn}
            <li><a href="#" className="del-msg" onClick={this.DeleteMsg(list[i])}><i className="fa fa-trash-o" /></a></li>
            {close_btn}
          </ul>
        );
      }
      
      let msg_push =[];
      if((list[i].note != '' && list[i].note != null) && (isFileAttached == true && isFileAttached != null)){
        msg_push.push(
          <div className="chat-bubble 1">
            <div className="chat-content img-content">
              <span class="task-chat-user">{list[i].contactName}</span>
              <span class="chat-time chat-time-pk">{moment(list[i].createdOn).format(process.env.DATETIME_FORMAT)}</span>
              <p>{list[i].note}</p>
              <br/>
              {file_type_push}
              {att_doc_push}
            </div>
            <div className="chat-action-btns">
              {is_delete_push}
            </div>
          </div>
        );
      }else if((list[i].note != '' && list[i].note != null) && (isFileAttached == '' || isFileAttached == null)){
        msg_push.push(
          <div className="chat-bubble 2">
            <div class="chat-content">
              <span class="task-chat-user">{list[i].contactName}</span> 
              <span class="chat-time chat-time-pk">{moment(list[i].createdOn).format(process.env.DATETIME_FORMAT)}</span>
              <p>{list[i].note}</p>
            </div>

            <div className="chat-action-btns">
              {is_delete_push}
            </div>
          </div>
        );
      }else if((isFileAttached == true && isFileAttached != null) && (list[i].note == '' || list[i].note == null) ){
        msg_push.push(
          <div className="chat-bubble 3">
            <div className="chat-content img-content">
              <span class="task-chat-user">{list[i].contactName}</span>
              <span class="chat-time chat-time-pk">{moment(list[i].createdOn).format(process.env.DATETIME_FORMAT)}</span>
              
              {file_type_push}
            </div>
            <div className="chat-action-btns">
              {is_delete_push}
            </div>
          </div>
        );
      }

      list_push.push(
        <div className="chat chat-left">
          <div className="chat-avatar">
            <a href="/blue/app/profile/employee-profile" className="avatar">
              <img alt="" src={list[i].profileLink} />
            </a>
          </div>
          <div className="chat-body">
            {msg_push}
          </div>
        </div>
      );
    }

    final_list_push.push(
      <div className="chats">
      {list_push}
      </div>
    );
    

    return final_list_push;
  }

  GetTimeSheetNotes(timesheetcontactmasterId,locationGuid){
    this.setState({ ListNotes: [] });
    /* Role Management */
    var getrole = SystemHelpers.GetRole();
    //let canDelete = getrole.documents_can.documents_can_delete;
    let canDelete = false;
    /* Role Management */
    this.setState({ GetChatListLoad: false });

    this.showLoader();
    var url=process.env.API_API_URL+'GetTimesheetNotesDetails?timesheetcontactmasterId='+timesheetcontactmasterId+"&locationId="+locationGuid;
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
      //body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
      //  console.log("responseJson GetTimesheetNotesDetails");
      //  console.log(data);
        //console.log(data.data.userRole);
        this.setState({ GetChatListLoad: true });
        if (data.responseType === "1") {
            // Profile & Contact
            this.setState({ ListNotes: data.data });

             $(".chat-wrap-inner").stop().animate({ scrollTop: $(".chat-wrap-inner")[0].scrollHeight}, 1000);
        }else{
              if(data.message == 'Authorization has been denied for this request.'){
                SystemHelpers.SessionOut();
                this.props.history.push("/login");
              }else{
                SystemHelpers.ToastError(data.message);
              }
              
        }
        this.hideLoader();
        
    })
    .catch(error => {
      console.log('GetTimesheetNotesDetails error 1');
      this.props.history.push("/error-500");
    });
  }

  GetTimeSheetNotesLoad(timesheetcontactmasterId,locationGuid){
    //this.setState({ ListNotes: [] });
    /* Role Management */
    var getrole = SystemHelpers.GetRole();
    //let canDelete = getrole.documents_can.documents_can_delete;
    let canDelete = false;
    /* Role Management */

    this.showLoader();
    var url=process.env.API_API_URL+'GetTimesheetNotesDetails?timesheetcontactmasterId='+timesheetcontactmasterId+"&locationId="+locationGuid;
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
      //body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
      //  console.log("responseJson GetTimesheetNotesDetails");
      //  console.log(data);
        //console.log(data.data.userRole);
        
        if (data.responseType === "1") {
            // Profile & Contact
            this.setState({ ListNotes: data.data });
            $(".chat-wrap-inner").stop().animate({ scrollTop: $(".chat-wrap-inner")[0].scrollHeight}, 1000);
            
        }else{
              if(data.message == 'Authorization has been denied for this request.'){
                SystemHelpers.SessionOut();
                this.props.history.push("/login");
              }else{
                SystemHelpers.ToastError(data.message);
              }
              
        }
        this.hideLoader();
        
    })
    .catch(error => {
      console.log('GetTimesheetNotesDetails error 2');
      this.props.history.push("/error-500");
    });
  }


  
  row_display(timeSheetDisplayUserViews,locationId){
    //timeSheetHeaderView
    //timeSheetDisplayUserViews

    
    //var timeSheetDisplayUserViews = this.state.timeSheetDisplayUserViews;
    
    var row_count = timeSheetDisplayUserViews.length;
    //var row_count = 1;

    // console.log('row_display timeSheetDisplayUserViews');
    // console.log(timeSheetDisplayUserViews);
    // console.log('row_display');
    // console.log(row_count);

    var timeSheetHeaderView = this.state.timeSheetHeaderView;
    var row_header_count = timeSheetHeaderView.length;
    // console.log("row array");
    // console.log(row_count);
    // console.log(timeSheetDisplayUserViews);
    let row = [];
    
    for (var zz = 0; zz < row_count; zz++) {

      //console.log('export_btn_show '+timeSheetDisplayUserViews[zz].isTimeSheetSubmittedToPayroll);
      // if(timeSheetDisplayUserViews[zz].isTimeSheetSubmittedToPayroll == false){
      //   $('#export_btn_show').css('display','none');
      // }
      // if(timeSheetDisplayUserViews[zz].fullName =='Virat Kohli08'){
      //   console.log('timeSheetDisplayUserViews => '+ timeSheetDisplayUserViews[zz].fullName);
      //   console.log(timeSheetDisplayUserViews[zz]);
      // }

      let role_permission = {};
      role_permission["isCordinator"] = timeSheetDisplayUserViews[zz].isCordinator;
      role_permission["isPayrollAdmin"] = timeSheetDisplayUserViews[zz].isPayrollAdmin;
      role_permission["isSeniorCordinator"] = timeSheetDisplayUserViews[zz].isSeniorCordinator;
      role_permission["isServiceCoordinatorLead"] = timeSheetDisplayUserViews[zz].isServiceCoordinatorLead;
      

      let temp_row = [];
      let temp_row1 =[];
      let temp_row2 =[];
      let temp_row3 =[];
      let temp_row4 =[];
      let temp_row5 =[];

      let temp_row6 =[];

      var LocationGUID = timeSheetDisplayUserViews[zz].locationIdGuid;

      temp_row1.push(<td>{timeSheetDisplayUserViews[zz].fullName}</td>);
      var OnCallCount = 0;
      for (var jj = 0; jj < row_header_count; jj++) {
        
        // console.log('row_display row');
        // console.log(timeSheetDisplayUserViews[zz].timeSheetConatctSheetViews);
        var sheet_info =timeSheetDisplayUserViews[zz].timeSheetConatctSheetViews;
        var isPayrollAdmin=timeSheetDisplayUserViews[zz].isPayrollAdmin;

        var check_date = [];
        var temp_check_date = [];
        if(sheet_info != null){
          var row_header_count_sheet_info = sheet_info.length;
          
          for (var kk = 0; kk < row_header_count_sheet_info; kk++) {
              //var timeSheetDate = moment(sheet_info[kk].timeOUT).format('MM-DD-YYYY');
              

              /* Next Day Display */

              var checkStartDate = moment(sheet_info[kk].timeIN).format('YYYY-MM-DD');
              var checkEndDate = moment(sheet_info[kk].timeOUT).format('YYYY-MM-DD');

              if(checkStartDate != checkEndDate){
                

                var FirstDay1 = moment(sheet_info[kk].timeIN).format('YYYY-MM-DD HH:mm');
                var FirstDay2 = checkStartDate+" 24:00";

                var SecondDay1 = checkEndDate+" 00:00";
                var SecondDay2 = moment(sheet_info[kk].timeOUT).format('YYYY-MM-DD HH:mm');

                // console.log("FirstDay1 => "+FirstDay1);
                // console.log("FirstDay2 => "+FirstDay2);

                // console.log("SecondDay1 => "+SecondDay1);
                // console.log("SecondDay2 => "+SecondDay2);

                var duration1 = moment.duration(moment(FirstDay2).diff(moment(FirstDay1)));
                var FirstDayMinutes = duration1.asMinutes();

                var duration2 = moment.duration(moment(SecondDay2).diff(moment(SecondDay1)));
                var SecondDayMinutes = duration2.asMinutes();

                // console.log(FirstDayMinutes);
                // console.log(SecondDayMinutes);

                if(FirstDayMinutes > SecondDayMinutes){
                  var timeSheetDate = moment(sheet_info[kk].timeIN).format('MM-DD-YYYY');
                }else{
                  var timeSheetDate = moment(sheet_info[kk].timeOUT).format('MM-DD-YYYY');
                }

              }else{
                var timeSheetDate = moment(sheet_info[kk].timeOUT).format('MM-DD-YYYY');
              }

              //console.log(timeSheetDate);

              /* Next Day Display */

              //var timeSheetDate = moment(sheet_info[kk].timeSheetDate).format('MM-DD-YYYY');

              if(timeSheetHeaderView[jj].timeSheetStartDate == timeSheetDate){
                //console.log(timeSheetHeaderView[jj].timeSheetStartDate+" "+sheet_info[kk].timeSheetDate);
                
                  //console.log('sheet_info[kk] => '+kk +" "+ sheet_info[kk].serviceId);
                  if(sheet_info[kk].serviceId == process.env.API_ON_CALL && sheet_info[kk].wantToDisplayInTimeSheet == true){
                        OnCallCount++;
                  }
                
                


                if(sheet_info[kk].serviceId == process.env.API_ON_CALL && sheet_info[kk].wantToDisplayInTimeSheet == false){
                  temp_check_date.push(<div> On Call </div>);
                }else if(sheet_info[kk].serviceId == process.env.API_ON_CALL && sheet_info[kk].wantToDisplayInTimeSheet == true){
                  temp_check_date.push(<div>{sheet_info[kk].wantToDisplayInTimeSheet}</div>);
                }else{
                  temp_check_date.push(<div>{sheet_info[kk].noOfHours}</div>);
                }
                
              }
          }
          if(temp_check_date != ''){
            check_date.push(<td>{temp_check_date}</td>);
          }
          
        }
        

        if(check_date == ''){
          check_date.push(<td> - </td>);
        }

         temp_row2.push(check_date);
      }
      temp_row3.push(<td>{timeSheetDisplayUserViews[zz].totalNoOfHours} h</td>);
      // temp_row3.push(<td>{OnCallCount}</td>);
      var timeSheetPeriodId = timeSheetDisplayUserViews[zz].timeSheetPeriodId;
      var contactId = timeSheetDisplayUserViews[zz].contactId;
      var fullName = timeSheetDisplayUserViews[zz].fullName;
      
      var IsEditBtn = false;

      var AppDueDateAmendmend = timeSheetDisplayUserViews[zz].approverDueDateForAmendmend;
      var CurrentDate = moment(new Date()).format("MM-DD-YYYY HH:mm");//"09-15-2021 15:00";
      //var CurrentDate = "09-15-2021 15:00";
            
      if(Date.parse(AppDueDateAmendmend) > Date.parse(CurrentDate)){
          IsEditBtn = true;
      }

      var approverDueDateForAmendmend = timeSheetDisplayUserViews[zz].approverDueDateForAmendmend;

      //console.log('approverDueDateForAmendmend => '+approverDueDateForAmendmend);

      

      if(sheet_info != null && sheet_info.length !=0){
        var isTimeSheetApproved = timeSheetDisplayUserViews[zz].isTimeSheetApproved;
        var isTimeSheetSubmittedToPayroll = timeSheetDisplayUserViews[zz].isTimeSheetSubmittedToPayroll;
        var isSpecifcTimeSheetApprover = timeSheetDisplayUserViews[zz].isSpecifcTimeSheetApprover;
        //console.log(zz+" "+timeSheetDisplayUserViews[zz].isTimeSheetApproved+ " "+this.state.isPayrollAdmin+" "+timeSheetDisplayUserViews[zz].isTimeSheetSubmittedToPayroll);
        
        
        



        var edit_btn = [];
        var checkbox = [];
        var amendment_btn = [];
        let action_btn =[];
        let coordi_sr_submit_btn =[];
        var IsEditBtn = false;

        var timesheetDetails = timeSheetDisplayUserViews[zz];

        if(timesheetDetails.isAmendMends == true){
          amendment_btn.push(<i className="fa fa-adn pk-font-red-fa-fa" />);
        }

        
        var loc_mst = timeSheetDisplayUserViews[zz].timeSheetContactMasterId+","+locationId
        if(this.state.isPayrollAdminSession == true){
          IsEditBtn = true;
          if(timesheetDetails.isTimeSheetApproved == true && timesheetDetails.isTimeSheetSubmittedToPayroll == false){
            coordi_sr_submit_btn.push(<i className="fa fa-check pk-font-green-fa-fa" />);
          }

          edit_btn.push(<a href="#past_details_modal" className="edit-icon float-left" data-toggle="modal"  onClick={this.PastDetails(role_permission,timeSheetPeriodId,contactId,fullName,isTimeSheetApproved,isTimeSheetSubmittedToPayroll,isSpecifcTimeSheetApprover,locationId,isPayrollAdmin,LocationGUID,IsEditBtn)} ><i className="fa fa-pencil" /></a>);

          /* checkbox Button Condition Start*/
          if(timesheetDetails.isTimeSheetSubmittedToPayroll == true){
            checkbox.push(<input type="checkbox" className="pk-sheet-margin-top" value={loc_mst} name="checkbox" disabled="true" checked />);
          }else{
            checkbox.push(<input type="checkbox" className="pk-sheet-margin-top submit-staff-checkbox 567" value={loc_mst} name="checkbox" />);
          }
          /* checkbox Button Condition Start*/

        }
        else
        {
          /* EDIT Button Condition Start*/
          var AppDueDateAmendmend = timesheetDetails.approverDueDateForAmendmend;
          var CurrentDate = moment(new Date()).format("MM-DD-YYYY HH:mm");

          if(timesheetDetails.isTimeSheetSubmittedToPayroll == true || timesheetDetails.isTimeSheetApproved == true){
            if(Date.parse(AppDueDateAmendmend) > Date.parse(CurrentDate)){
              //console.log("AppDueDateAmendmend => "+AppDueDateAmendmend+" CurrentDate => "+CurrentDate);
              IsEditBtn = true;
              edit_btn.push(<a href="#past_details_modal" className="edit-icon float-left 7771" data-toggle="modal"  onClick={this.PastDetails(role_permission,timeSheetPeriodId,contactId,fullName,isTimeSheetApproved,isTimeSheetSubmittedToPayroll,isSpecifcTimeSheetApprover,locationId,isPayrollAdmin,LocationGUID,IsEditBtn)} ><i className="fa fa-pencil" /></a>);
            }else{
              edit_btn.push(<a href="#past_details_modal" className="edit-icon float-left 111" data-toggle="modal"  onClick={this.PastDetails(role_permission,timeSheetPeriodId,contactId,fullName,isTimeSheetApproved,isTimeSheetSubmittedToPayroll,isSpecifcTimeSheetApprover,locationId,isPayrollAdmin,LocationGUID,IsEditBtn)} ><i className="fa fa-eye" /></a>);
            }
          }else{
              //IsEditBtn = true;
              //edit_btn.push(<a href="#past_details_modal" className="edit-icon float-left 7772" data-toggle="modal"  onClick={this.PastDetails(role_permission,timeSheetPeriodId,contactId,fullName,isTimeSheetApproved,isTimeSheetSubmittedToPayroll,isSpecifcTimeSheetApprover,locationId,isPayrollAdmin,LocationGUID,IsEditBtn)} ><i className="fa fa-pencil" /></a>);

              if(Date.parse(AppDueDateAmendmend) > Date.parse(CurrentDate)){
                //console.log("AppDueDateAmendmend => "+AppDueDateAmendmend+" CurrentDate => "+CurrentDate);
                IsEditBtn = true;
                edit_btn.push(<a href="#past_details_modal" className="edit-icon float-left 7771" data-toggle="modal"  onClick={this.PastDetails(role_permission,timeSheetPeriodId,contactId,fullName,isTimeSheetApproved,isTimeSheetSubmittedToPayroll,isSpecifcTimeSheetApprover,locationId,isPayrollAdmin,LocationGUID,IsEditBtn)} ><i className="fa fa-pencil" /></a>);
              }else{
                edit_btn.push(<a href="#past_details_modal" className="edit-icon float-left 111" data-toggle="modal"  onClick={this.PastDetails(role_permission,timeSheetPeriodId,contactId,fullName,isTimeSheetApproved,isTimeSheetSubmittedToPayroll,isSpecifcTimeSheetApprover,locationId,isPayrollAdmin,LocationGUID,IsEditBtn)} ><i className="fa fa-eye" /></a>);
              }
          }
          /* EDIT Button Condition End*/

          /* checkbox Button Condition Start*/
          if(Date.parse(AppDueDateAmendmend) > Date.parse(CurrentDate)){
            if(timesheetDetails.isTimeSheetSubmittedToPayroll == true || timesheetDetails.isTimeSheetApproved == true){
              checkbox.push(<input type="checkbox" className="pk-sheet-margin-top 880895" value={loc_mst} name="checkbox" checked  disabled="true"/>);
            }else{
              checkbox.push(<input type="checkbox" className="pk-sheet-margin-top submit-staff-checkbox 7878" value={loc_mst} name="checkbox" />);
            }
          }else{
            if(timesheetDetails.isTimeSheetSubmittedToPayroll == true || timesheetDetails.isTimeSheetApproved == true){
              checkbox.push(<input type="checkbox" className="pk-sheet-margin-top 88089" value={loc_mst} name="checkbox" checked  disabled="true"/>);
            }else{
              checkbox.push(<input type="checkbox" className="pk-sheet-margin-top submit-staff-checkbox 7878" value={loc_mst} name="checkbox" disabled="true" />);
            }
          } 
          /* checkbox Button Condition End*/
        }

        temp_row4.push(<td>{edit_btn}{checkbox}{amendment_btn}{coordi_sr_submit_btn}</td>);

        //temp_row4.push(this.action_func(timeSheetDisplayUserViews[zz],timeSheetPeriodId,contactId,fullName,isTimeSheetApproved,isTimeSheetSubmittedToPayroll,isSpecifcTimeSheetApprover,locationId,isPayrollAdmin,LocationGUID));        
        // if(this.state.isPayrollAdminSession == true){
        //   if(timeSheetDisplayUserViews[zz].isTimeSheetSubmittedToPayroll == true){
            
        //     // Hr 03-09-2021
        //     var AppDueDateAmendmend = timeSheetDisplayUserViews[zz].approverDueDateForAmendmend;
        //     var CurrentDate = moment(new Date()).format("MM-DD-YYYY HH:mm");//"09-15-2021 15:00";
        //     IsEditBtn = true;
        //     //var CurrentDate = "09-15-2021 15:00";
        //     // if(Date.parse(AppDueDateAmendmend) > Date.parse(CurrentDate)){
        //       if(timeSheetDisplayUserViews[zz].isAmendMends == true){
        //         temp_row4.push(<td><a href="#past_details_modal" className="edit-icon float-left 111" data-toggle="modal"  onClick={this.PastDetails(role_permission,timeSheetPeriodId,contactId,fullName,isTimeSheetApproved,isTimeSheetSubmittedToPayroll,isSpecifcTimeSheetApprover,locationId,isPayrollAdmin,LocationGUID,IsEditBtn)} ><i className="fa fa-eye" /></a><input type="checkbox" className="pk-sheet-margin-top" value={timeSheetDisplayUserViews[zz].timeSheetContactMasterId} name="checkbox" checked  disabled="true"/> <i className="fa fa-adn pk-font-gray-fa-fa" /> </td>);
        //       }else{
        //         temp_row4.push(<td><a href="#past_details_modal" className="edit-icon float-left 222" data-toggle="modal"  onClick={this.PastDetails(role_permission,timeSheetPeriodId,contactId,fullName,isTimeSheetApproved,isTimeSheetSubmittedToPayroll,isSpecifcTimeSheetApprover,locationId,isPayrollAdmin,LocationGUID,IsEditBtn)} ><i className="fa fa-pencil" /></a><input type="checkbox" className="pk-sheet-margin-top" value={timeSheetDisplayUserViews[zz].timeSheetContactMasterId} name="checkbox" checked  disabled="true"/> </td>);
        //       }
        //     // }else{
        //     //   temp_row4.push(<td><a href="#past_details_modal" className="edit-icon float-left" data-toggle="modal"  onClick={this.PastDetails(role_permission,timeSheetPeriodId,contactId,fullName,isTimeSheetApproved,isTimeSheetSubmittedToPayroll,isSpecifcTimeSheetApprover,locationId,isPayrollAdmin,LocationGUID)} ><i className="fa fa-pencil" /></a><input type="checkbox" className="pk-sheet-margin-top" value={timeSheetDisplayUserViews[zz].timeSheetContactMasterId} name="checkbox" checked  disabled="true"/> </td>);
        //     // }
        //     // Hr 03-09-2021
            
        //   }else if(timeSheetDisplayUserViews[zz].isTimeSheetApproved == true && timeSheetDisplayUserViews[zz].isTimeSheetSubmittedToPayroll == false){
            
        //     if(timeSheetDisplayUserViews[zz].isAmendMends == true){
        //       temp_row4.push(<td><a href="#past_details_modal" className="edit-icon float-left 333" data-toggle="modal"  onClick={this.PastDetails(role_permission,timeSheetPeriodId,contactId,fullName,isTimeSheetApproved,isTimeSheetSubmittedToPayroll,isSpecifcTimeSheetApprover,locationId,isPayrollAdmin,LocationGUID,IsEditBtn)} ><i className="fa fa-pencil" /></a><input type="checkbox" className="pk-sheet-margin-top submit-staff-checkbox" value={timeSheetDisplayUserViews[zz].timeSheetContactMasterId} name="checkbox" /> <i className="fa fa-adn pk-font-red-fa-fa" /> </td>);
        //     }else{
        //       temp_row4.push(<td><a href="#past_details_modal" className="edit-icon float-left 444" data-toggle="modal"  onClick={this.PastDetails(role_permission,timeSheetPeriodId,contactId,fullName,isTimeSheetApproved,isTimeSheetSubmittedToPayroll,isSpecifcTimeSheetApprover,locationId,isPayrollAdmin,LocationGUID,IsEditBtn)} ><i className="fa fa-pencil" /></a><input type="checkbox" className="pk-sheet-margin-top submit-staff-checkbox" value={timeSheetDisplayUserViews[zz].timeSheetContactMasterId} name="checkbox" />  </td>);
        //     }
            
        //     //temp_row4.push(<td><a href="#past_details_modal" className="edit-icon float-left" data-toggle="modal"  onClick={this.PastDetails(role_permission,timeSheetPeriodId,contactId,fullName,isTimeSheetApproved,isTimeSheetSubmittedToPayroll,isSpecifcTimeSheetApprover,locationId,LocationGUID)} ><i className="fa fa-pencil" /></a><input type="checkbox" className="pk-sheet-margin-top submit-staff-checkbox" value={timeSheetDisplayUserViews[zz].timeSheetContactMasterId} checked name="checkbox" /> <i className="fa fa-adn pk-font-red-fa-fa" /> </td>);
        //   }else{
        //     if(timeSheetDisplayUserViews[zz].isAmendMends == true){
        //       temp_row4.push(<td><a href="#past_details_modal" className="edit-icon float-left 555" data-toggle="modal"  onClick={this.PastDetails(role_permission,timeSheetPeriodId,contactId,fullName,isTimeSheetApproved,isTimeSheetSubmittedToPayroll,isSpecifcTimeSheetApprover,locationId,isPayrollAdmin,LocationGUID,IsEditBtn)} ><i className="fa fa-pencil" /></a><input type="checkbox" className="pk-sheet-margin-top submit-staff-checkbox" value={timeSheetDisplayUserViews[zz].timeSheetContactMasterId} name="checkbox" /> <i className="fa fa-adn pk-font-red-fa-fa" /> </td>);
        //     }else{
        //       temp_row4.push(<td><a href="#past_details_modal" className="edit-icon float-left 666" data-toggle="modal"  onClick={this.PastDetails(role_permission,timeSheetPeriodId,contactId,fullName,isTimeSheetApproved,isTimeSheetSubmittedToPayroll,isSpecifcTimeSheetApprover,locationId,isPayrollAdmin,LocationGUID,IsEditBtn)} ><i className="fa fa-pencil" /></a><input type="checkbox" className="pk-sheet-margin-top submit-staff-checkbox" value={timeSheetDisplayUserViews[zz].timeSheetContactMasterId} name="checkbox" />  </td>);
        //     }
            
        //   }
        // }else if(this.state.isCordinatorSession == true || this.state.isSeniorCordinatorSession == true || this.state.isServiceCoordinatorLeadSession == true){
        //   if(timeSheetDisplayUserViews[zz].isTimeSheetApproved == true){

        //     // Hr 03-09-2021
        //     var AppDueDateAmendmend = timeSheetDisplayUserViews[zz].approverDueDateForAmendmend;
        //     var CurrentDate = moment(new Date()).format("MM-DD-YYYY HH:mm");//"09-15-2021 15:00";
        //     //var CurrentDate = "09-15-2021 15:00";
            
        //     if(Date.parse(AppDueDateAmendmend) > Date.parse(CurrentDate)){
        //       IsEditBtn = true;
        //       temp_row4.push(<td><a href="#past_details_modal" className="edit-icon float-left 777" data-toggle="modal"  onClick={this.PastDetails(role_permission,timeSheetPeriodId,contactId,fullName,isTimeSheetApproved,isTimeSheetSubmittedToPayroll,isSpecifcTimeSheetApprover,locationId,isPayrollAdmin,LocationGUID,IsEditBtn)} ><i className="fa fa-pencil" /></a><input type="checkbox" className="pk-sheet-margin-top" value={timeSheetDisplayUserViews[zz].timeSheetContactMasterId} name="checkbox" checked  disabled="true"/></td>);
        //     }else{
        //       if(timeSheetDisplayUserViews[zz].isAmendMends == true){
        //         temp_row4.push(<td><a href="#past_details_modal" className="edit-icon float-left 888" data-toggle="modal"  onClick={this.PastDetails(role_permission,timeSheetPeriodId,contactId,fullName,isTimeSheetApproved,isTimeSheetSubmittedToPayroll,isSpecifcTimeSheetApprover,locationId,isPayrollAdmin,LocationGUID,IsEditBtn)} ><i className="fa fa-eye" /></a><input type="checkbox" className="pk-sheet-margin-top" value={timeSheetDisplayUserViews[zz].timeSheetContactMasterId} name="checkbox" checked  disabled="true"/> <i className="fa fa-adn pk-font-gray-fa-fa" /> </td>);
        //       }else{
        //         temp_row4.push(<td><a href="#past_details_modal" className="edit-icon float-left 999" data-toggle="modal"  onClick={this.PastDetails(role_permission,timeSheetPeriodId,contactId,fullName,isTimeSheetApproved,isTimeSheetSubmittedToPayroll,isSpecifcTimeSheetApprover,locationId,isPayrollAdmin,LocationGUID,IsEditBtn)} ><i className="fa fa-eye" /></a><input type="checkbox" className="pk-sheet-margin-top" value={timeSheetDisplayUserViews[zz].timeSheetContactMasterId} name="checkbox" checked  disabled="true"/> </td>);
        //       }
        //     }
        //     // Hr 03-09-2021

            
            
        //   }else{
        //     if(timeSheetDisplayUserViews[zz].isAmendMends == true){
        //       temp_row4.push(<td><a href="#past_details_modal" className="edit-icon float-left 000" data-toggle="modal"  onClick={this.PastDetails(role_permission,timeSheetPeriodId,contactId,fullName,isTimeSheetApproved,isTimeSheetSubmittedToPayroll,isSpecifcTimeSheetApprover,locationId,isPayrollAdmin,LocationGUID,IsEditBtn)} ><i className="fa fa-pencil" /></a><input type="checkbox" className="pk-sheet-margin-top submit-staff-checkbox" value={timeSheetDisplayUserViews[zz].timeSheetContactMasterId} name="checkbox" /> <i className="fa fa-adn pk-font-red-fa-fa" /> </td>);
        //     }else{
        //       temp_row4.push(<td><a href="#past_details_modal" className="edit-icon float-left 101" data-toggle="modal"  onClick={this.PastDetails(role_permission,timeSheetPeriodId,contactId,fullName,isTimeSheetApproved,isTimeSheetSubmittedToPayroll,isSpecifcTimeSheetApprover,locationId,isPayrollAdmin,LocationGUID,IsEditBtn)} ><i className="fa fa-pencil" /></a><input type="checkbox" className="pk-sheet-margin-top submit-staff-checkbox" value={timeSheetDisplayUserViews[zz].timeSheetContactMasterId} name="checkbox" /> </td>);
        //     }
            
        //   }
        // }



        
        
      }else{

        var edit_btn = [];
        var amendment_btn = [];
        let action_btn =[];
        var IsEditBtn = false;

        // if(timeSheetDisplayUserViews[zz].isAmendMends == true){
        //   amendment_btn.push(<i className="fa fa-adn pk-font-red-fa-fa" />);
        // }

       if(this.state.isPayrollAdminSession == true){
          IsEditBtn = true;
          edit_btn.push(<a href="#past_details_modal" className="edit-icon float-left 102" data-toggle="modal"  onClick={this.PastDetails(role_permission,timeSheetPeriodId,contactId,fullName,isTimeSheetApproved,isTimeSheetSubmittedToPayroll,0,locationId,isPayrollAdmin,LocationGUID,IsEditBtn)} ><i className="fa fa-pencil" /></a>);
        }else{
          var AppDueDateAmendmend = timeSheetDisplayUserViews[zz].approverDueDateForAmendmend;
          var CurrentDate = moment(new Date()).format("MM-DD-YYYY HH:mm");

          if(Date.parse(AppDueDateAmendmend) > Date.parse(CurrentDate)){
            IsEditBtn = true;
            edit_btn.push(<a href="#past_details_modal" className="edit-icon float-left 102" data-toggle="modal"  onClick={this.PastDetails(role_permission,timeSheetPeriodId,contactId,fullName,isTimeSheetApproved,isTimeSheetSubmittedToPayroll,0,locationId,isPayrollAdmin,LocationGUID,IsEditBtn)} ><i className="fa fa-pencil" /></a>);
          }else{
            edit_btn.push(<i className="fa fa-eye-slash pk-font-red-fa-fa" />);
          }
        }

        

        temp_row4.push(<td>{edit_btn}{amendment_btn}</td>);

        
        
      }
      
      
      temp_row.push(<tr>{temp_row1}{temp_row2}{temp_row3}{temp_row4}</tr>);

      row.push(temp_row);
    }
    //row.push(<tr><td className="pk-td-border" colspan={row_header_count + 1}><b>Total full-time hours </b></td><td><b>{locationTotalHours}</b></td></tr>);
    return row;
  }

  

  location_wise_display_grid(){
    
    var locationWiseTimeSheeetViews = this.state.locationWiseTimeSheeetViews;
    var row_count = locationWiseTimeSheeetViews.length;
    // console.log('location_wise_display_grid 1');
    // console.log(locationWiseTimeSheeetViews);
    let row = [];
    let location_row = [];
    for (var zz = 0; zz < row_count; zz++) {
        //console.log('location_wise_display_grid' + row_count);
        var collapsehref = "#collapse-"+locationWiseTimeSheeetViews[zz].locationId;
        var collapseId = "collapse-"+locationWiseTimeSheeetViews[zz].locationId;

        

        if(row_count == 1){
          location_row.push( 
            <div>
              <div className="row">
                    <div className="col-lg-6">
                        <div className="dash-info-list pk-sheet-margin-bottom">
                          <a href="#" className="dash-card text-danger">
                            <div className="dash-card-container">
                              <div className="dash-card-icon">
                                <i className="fa fa-calendar" />
                              </div>
                              <div className="dash-card-content">
                                <p>Time Sheet Due by</p>
                              </div>
                              <div className="dash-card-avatars">
                                {moment(locationWiseTimeSheeetViews[zz].locationDueDate,process.env.API_DATE_FORMAT).format(process.env.DATE_FORMAT)}
                              </div>
                            </div>
                          </a>
                        </div>
                    </div>

                    <div className="col-lg-6">
                        <div className="dash-info-list pk-sheet-margin-bottom">
                          <a href="#" className="dash-card text-danger">
                            <div className="dash-card-container">
                              <div className="dash-card-icon">
                                <i className="fa fa-clock" />
                              </div>
                              <div className="dash-card-content">
                                <p>Total Hours</p>
                              </div>
                              <div className="dash-card-avatars">
                                {locationWiseTimeSheeetViews[zz].locationTotalHours}
                              </div>
                            </div>
                          </a>
                        </div>
                    </div>
              </div>
              <div className="row">
                <div className="col-lg-6 col-sm-12 col-xs-12">
                    <div className="dash-info-list pk-sheet-margin-bottom">
                      <a href="#" className="dash-card text-danger">
                        <div className="dash-card-container">
                          <div className="dash-card-icon">
                            <i className="fa fa-calendar" />
                          </div>
                          <div className="dash-card-content">
                            <p>Amendment Due Date For Approver</p>
                          </div>
                          <div className="dash-card-avatars">
                            {moment(locationWiseTimeSheeetViews[zz].timeSheetDisplayUserViews[0].approverDueDateForAmendmend,process.env.API_DATE_FORMAT).format(process.env.DATETIME_FORMAT)}
                          </div>
                        </div>
                      </a>
                    </div>
                </div>
              </div>
              <div className="table-responsive">
                <table className="table table-nowrap">
                  <thead>
                   <tr>
                      <th>Staff Member</th>
                      {this.state.timeSheetHeaderView.map(( listValue, index ) => {
                        return (<th>{listValue.timeSheetDateDisplay}</th>);
                      })}
                      <th>Total Hours</th>
                      {/*<th>On Call</th>*/}
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    {this.row_display(locationWiseTimeSheeetViews[zz].timeSheetDisplayUserViews,locationWiseTimeSheeetViews[zz].locationId)}
                  </tbody>
                </table>
              </div>
            </div>    
          );
        }else{
          location_row.push( <div className="card">
            <div className="card-header">
            <h4 className="card-title">
              <a className="collapsed" data-toggle="collapse" href={collapsehref}>{locationWiseTimeSheeetViews[zz].locationName}</a>
            </h4>
            </div>
            <div id={collapseId} className="card-collapse collapse">
            <div className="card-body">
              <div className="row">
                    <div className="col-lg-6">
                        <div className="dash-info-list pk-sheet-margin-bottom">
                          <a href="#" className="dash-card text-danger">
                            <div className="dash-card-container">
                              <div className="dash-card-icon">
                                <i className="fa fa-calendar" />
                              </div>
                              <div className="dash-card-content">
                                <p>Time Sheet Due by</p>
                              </div>
                              <div className="dash-card-avatars">
                                {moment(locationWiseTimeSheeetViews[zz].locationDueDate,process.env.API_DATE_FORMAT).format(process.env.DATE_FORMAT)}
                              </div>
                            </div>
                          </a>
                        </div>
                    </div>

                    <div className="col-lg-6">
                        <div className="dash-info-list pk-sheet-margin-bottom">
                          <a href="#" className="dash-card text-danger">
                            <div className="dash-card-container">
                              <div className="dash-card-icon">
                                <i className="fa fa-clock" />
                              </div>
                              <div className="dash-card-content">
                                <p>Total Hours</p>
                              </div>
                              <div className="dash-card-avatars">
                                {locationWiseTimeSheeetViews[zz].locationTotalHours}
                              </div>
                            </div>
                          </a>
                        </div>
                    </div>
              </div>
              <div className="row">
                <div className="col-lg-6">
                    <div className="dash-info-list pk-sheet-margin-bottom">
                      <a href="#" className="dash-card text-danger">
                        <div className="dash-card-container">
                          <div className="dash-card-icon">
                            <i className="fa fa-calendar" />
                          </div>
                          <div className="dash-card-content">
                            <p>Amendment Due Date For Approver</p>
                          </div>
                          <div className="dash-card-avatars">
                            {moment(locationWiseTimeSheeetViews[zz].timeSheetDisplayUserViews[0].approverDueDateForAmendmend,process.env.API_DATE_FORMAT).format(process.env.DATETIME_FORMAT)}
                          </div>
                        </div>
                      </a>
                    </div>
                </div>
              </div>
              <div className="table-responsive">
                <table className="table table-nowrap">
                  <thead>
                   <tr>
                      <th>Staff Member</th>
                      {this.state.timeSheetHeaderView.map(( listValue, index ) => {
                        return (<th>{listValue.timeSheetDateDisplay}</th>);
                      })}
                      <th>Total Hours</th>
                      {/*<th>On Call</th>*/}
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    {this.row_display(locationWiseTimeSheeetViews[zz].timeSheetDisplayUserViews,locationWiseTimeSheeetViews[zz].locationId)}
                  </tbody>
                </table>
              </div>
            </div>
            </div>
          </div> 
        );
        }
        
    }

    return location_row;
  }

  row_display_onegrid(timeSheetDisplayUserViews,locationName,locationId){
    //timeSheetHeaderView
    //timeSheetDisplayUserViews
    
    

    //var timeSheetDisplayUserViews = this.state.timeSheetDisplayUserViews;
    

    var row_count = timeSheetDisplayUserViews.length;
    //var row_count = 1;

    // console.log('row_display timeSheetDisplayUserViews');
    // console.log(timeSheetDisplayUserViews);
    // console.log('row_display');
    // console.log(row_count);

    var timeSheetHeaderView = this.state.timeSheetHeaderView;
    var row_header_count = timeSheetHeaderView.length;
    // console.log("row array");
    // console.log(row_count);
    // console.log(timeSheetDisplayUserViews);
    let row = [];
    for (var zz = 0; zz < row_count; zz++) {

      let role_permission = {};
      role_permission["isCordinator"] = timeSheetDisplayUserViews[zz].isCordinator;
      role_permission["isPayrollAdmin"] = timeSheetDisplayUserViews[zz].isPayrollAdmin;
      role_permission["isSeniorCordinator"] = timeSheetDisplayUserViews[zz].isSeniorCordinator;
      role_permission["isServiceCoordinatorLead"] = timeSheetDisplayUserViews[zz].isServiceCoordinatorLead;

      //console.log('export_btn_show '+timeSheetDisplayUserViews[zz].isTimeSheetSubmittedToPayroll);
      if(timeSheetDisplayUserViews[zz].isTimeSheetSubmittedToPayroll == false){
        //console.log('export_btn_show false');
        $('#export_btn_show').css('display','none');
      }

      let temp_row = [];
      let temp_row1 =[];
      let temp_row2 =[];
      let temp_row3 =[];
      let temp_row4 =[];
      let temp_row5 =[];

      var LocationGUID = timeSheetDisplayUserViews[zz].locationIdGuid;
      
      temp_row1.push(<td>{timeSheetDisplayUserViews[zz].fullName}</td>);
      temp_row2.push(<td>{locationName}</td>);
      temp_row2.push(<td>{timeSheetDisplayUserViews[zz].roleName}</td>);
      temp_row4.push(<td>{timeSheetDisplayUserViews[zz].totalNoOfHours} h</td>);
      temp_row4.push(<td></td>);
      var timeSheetPeriodId = timeSheetDisplayUserViews[zz].timeSheetPeriodId;
      var contactId = timeSheetDisplayUserViews[zz].contactId;
      var fullName = timeSheetDisplayUserViews[zz].fullName;
      var isPayrollAdmin=timeSheetDisplayUserViews[zz].isPayrollAdmin;
      
      var sheet_info =timeSheetDisplayUserViews[zz].timeSheetConatctSheetViews;

      var IsEditBtn = false;

      var AppDueDateAmendmend = timeSheetDisplayUserViews[zz].approverDueDateForAmendmend;
      var CurrentDate = moment(new Date()).format("MM-DD-YYYY HH:mm");//"09-15-2021 15:00";
      //var CurrentDate = "09-15-2021 15:00";
            
      if(Date.parse(AppDueDateAmendmend) > Date.parse(CurrentDate)){
          IsEditBtn = true;
      }

      var approverDueDateForAmendmend = timeSheetDisplayUserViews[zz].approverDueDateForAmendmend;

      //console.log('approverDueDateForAmendmend => '+approverDueDateForAmendmend);

      

      if(sheet_info != null && sheet_info.length !=0){
        var isTimeSheetApproved = timeSheetDisplayUserViews[zz].isTimeSheetApproved;
        var isTimeSheetSubmittedToPayroll = timeSheetDisplayUserViews[zz].isTimeSheetSubmittedToPayroll;
        var isSpecifcTimeSheetApprover = timeSheetDisplayUserViews[zz].isSpecifcTimeSheetApprover;
        //console.log(zz+" "+timeSheetDisplayUserViews[zz].isTimeSheetApproved+ " "+this.state.isPayrollAdmin+" "+timeSheetDisplayUserViews[zz].isTimeSheetSubmittedToPayroll);
        // if(this.state.isPayrollAdmin == true){
        //   temp_row5.push(<td><a href="#past_details_modal" className="edit-icon float-left" data-toggle="modal"  onClick={this.PastDetails(role_permission,timeSheetPeriodId,contactId,fullName,isTimeSheetApproved,isTimeSheetSubmittedToPayroll)} ><i className="fa fa-pencil" /></a><input type="checkbox" className="pk-sheet-margin-top submit-staff-checkbox" value={timeSheetDisplayUserViews[zz].timeSheetContactMasterId} name="checkbox" /></td>);
        // }else if(timeSheetDisplayUserViews[zz].canEditOrApproveTimeSheet == true){
        //   temp_row5.push(<td><a href="#past_details_modal" className="edit-icon float-left" data-toggle="modal"  onClick={this.PastDetails(role_permission,timeSheetPeriodId,contactId,fullName,isTimeSheetApproved,isTimeSheetSubmittedToPayroll)} ><i className="fa fa-pencil" /></a><input type="checkbox" className="pk-sheet-margin-top submit-staff-checkbox" value={timeSheetDisplayUserViews[zz].timeSheetContactMasterId} name="checkbox" /></td>);
        // }
        // else if((timeSheetDisplayUserViews[zz].canEditOrApproveTimeSheet == false) || (timeSheetDisplayUserViews[zz].isTimeSheetApproved == true && this.state.isPayrollAdmin == false) || (timeSheetDisplayUserViews[zz].isTimeSheetSubmittedToPayroll == true && this.state.isPayrollAdmin == true)){
        //   temp_row5.push(<td><a href="#past_details_modal" className="edit-icon float-left" data-toggle="modal"  onClick={this.PastDetails(role_permission,timeSheetPeriodId,contactId,fullName,isTimeSheetApproved,isTimeSheetSubmittedToPayroll)} ><i className="fa fa-eye" /></a><input type="checkbox" className="pk-sheet-margin-top" value={timeSheetDisplayUserViews[zz].timeSheetContactMasterId} name="checkbox" checked  disabled="true"/></td>);
        // }else{
        //   temp_row5.push(<td><a href="#past_details_modal" className="edit-icon float-left" data-toggle="modal"  onClick={this.PastDetails(role_permission,timeSheetPeriodId,contactId,fullName,isTimeSheetApproved,isTimeSheetSubmittedToPayroll)} ><i className="fa fa-pencil" /></a><input type="checkbox" className="pk-sheet-margin-top submit-staff-checkbox" value={timeSheetDisplayUserViews[zz].timeSheetContactMasterId} name="checkbox" /></td>);
        // }

        

        //  if(this.state.isPayrollAdminSession == true){
        //   if(timeSheetDisplayUserViews[zz].isTimeSheetSubmittedToPayroll == true){
            
        //     // Hr 03-09-2021
        //     var AppDueDateAmendmend = timeSheetDisplayUserViews[zz].approverDueDateForAmendmend;
        //     var CurrentDate = moment(new Date()).format("MM-DD-YYYY HH:mm");//"09-15-2021 15:00";
        //     IsEditBtn = true;
        //     //var CurrentDate = "09-15-2021 15:00";
        //     // if(Date.parse(AppDueDateAmendmend) > Date.parse(CurrentDate)){
        //       if(timeSheetDisplayUserViews[zz].isAmendMends == true){
        //         temp_row5.push(<td><a href="#past_details_modal" className="edit-icon float-left 111" data-toggle="modal"  onClick={this.PastDetails(role_permission,timeSheetPeriodId,contactId,fullName,isTimeSheetApproved,isTimeSheetSubmittedToPayroll,isSpecifcTimeSheetApprover,locationId,isPayrollAdmin,LocationGUID,IsEditBtn)} ><i className="fa fa-eye" /></a><input type="checkbox" className="pk-sheet-margin-top" value={timeSheetDisplayUserViews[zz].timeSheetContactMasterId} name="checkbox" checked  disabled="true"/> <i className="fa fa-adn pk-font-gray-fa-fa" /> </td>);
        //       }else{
        //         temp_row5.push(<td><a href="#past_details_modal" className="edit-icon float-left 222" data-toggle="modal"  onClick={this.PastDetails(role_permission,timeSheetPeriodId,contactId,fullName,isTimeSheetApproved,isTimeSheetSubmittedToPayroll,isSpecifcTimeSheetApprover,locationId,isPayrollAdmin,LocationGUID,IsEditBtn)} ><i className="fa fa-pencil" /></a><input type="checkbox" className="pk-sheet-margin-top" value={timeSheetDisplayUserViews[zz].timeSheetContactMasterId} name="checkbox" checked  disabled="true"/> </td>);
        //       }
        //     // }else{
        //     //   temp_row5.push(<td><a href="#past_details_modal" className="edit-icon float-left" data-toggle="modal"  onClick={this.PastDetails(role_permission,timeSheetPeriodId,contactId,fullName,isTimeSheetApproved,isTimeSheetSubmittedToPayroll,isSpecifcTimeSheetApprover,locationId,isPayrollAdmin,LocationGUID)} ><i className="fa fa-pencil" /></a><input type="checkbox" className="pk-sheet-margin-top" value={timeSheetDisplayUserViews[zz].timeSheetContactMasterId} name="checkbox" checked  disabled="true"/> </td>);
        //     // }
        //     // Hr 03-09-2021
            
        //   }else if(timeSheetDisplayUserViews[zz].isTimeSheetApproved == true && timeSheetDisplayUserViews[zz].isTimeSheetSubmittedToPayroll == false){
            
        //     if(timeSheetDisplayUserViews[zz].isAmendMends == true){
        //       temp_row5.push(<td><a href="#past_details_modal" className="edit-icon float-left 333" data-toggle="modal"  onClick={this.PastDetails(role_permission,timeSheetPeriodId,contactId,fullName,isTimeSheetApproved,isTimeSheetSubmittedToPayroll,isSpecifcTimeSheetApprover,locationId,isPayrollAdmin,LocationGUID,IsEditBtn)} ><i className="fa fa-pencil" /></a><input type="checkbox" className="pk-sheet-margin-top submit-staff-checkbox" value={timeSheetDisplayUserViews[zz].timeSheetContactMasterId} name="checkbox" /> <i className="fa fa-adn pk-font-red-fa-fa" /> </td>);
        //     }else{
        //       temp_row5.push(<td><a href="#past_details_modal" className="edit-icon float-left 444" data-toggle="modal"  onClick={this.PastDetails(role_permission,timeSheetPeriodId,contactId,fullName,isTimeSheetApproved,isTimeSheetSubmittedToPayroll,isSpecifcTimeSheetApprover,locationId,isPayrollAdmin,LocationGUID,IsEditBtn)} ><i className="fa fa-pencil" /></a><input type="checkbox" className="pk-sheet-margin-top submit-staff-checkbox" value={timeSheetDisplayUserViews[zz].timeSheetContactMasterId} name="checkbox" />  </td>);
        //     }
            
        //     //temp_row5.push(<td><a href="#past_details_modal" className="edit-icon float-left" data-toggle="modal"  onClick={this.PastDetails(role_permission,timeSheetPeriodId,contactId,fullName,isTimeSheetApproved,isTimeSheetSubmittedToPayroll,isSpecifcTimeSheetApprover,locationId,LocationGUID)} ><i className="fa fa-pencil" /></a><input type="checkbox" className="pk-sheet-margin-top submit-staff-checkbox" value={timeSheetDisplayUserViews[zz].timeSheetContactMasterId} checked name="checkbox" /> <i className="fa fa-adn pk-font-red-fa-fa" /> </td>);
        //   }else{
        //     if(timeSheetDisplayUserViews[zz].isAmendMends == true){
        //       temp_row5.push(<td><a href="#past_details_modal" className="edit-icon float-left 555" data-toggle="modal"  onClick={this.PastDetails(role_permission,timeSheetPeriodId,contactId,fullName,isTimeSheetApproved,isTimeSheetSubmittedToPayroll,isSpecifcTimeSheetApprover,locationId,isPayrollAdmin,LocationGUID,IsEditBtn)} ><i className="fa fa-pencil" /></a><input type="checkbox" className="pk-sheet-margin-top submit-staff-checkbox" value={timeSheetDisplayUserViews[zz].timeSheetContactMasterId} name="checkbox" /> <i className="fa fa-adn pk-font-red-fa-fa" /> </td>);
        //     }else{
        //       temp_row5.push(<td><a href="#past_details_modal" className="edit-icon float-left 666" data-toggle="modal"  onClick={this.PastDetails(role_permission,timeSheetPeriodId,contactId,fullName,isTimeSheetApproved,isTimeSheetSubmittedToPayroll,isSpecifcTimeSheetApprover,locationId,isPayrollAdmin,LocationGUID,IsEditBtn)} ><i className="fa fa-pencil" /></a><input type="checkbox" className="pk-sheet-margin-top submit-staff-checkbox" value={timeSheetDisplayUserViews[zz].timeSheetContactMasterId} name="checkbox" />  </td>);
        //     }
            
        //   }
        // }else if(this.state.isCordinatorSession == true || this.state.isSeniorCordinatorSession == true || this.state.isServiceCoordinatorLeadSession == true){
        //   if(timeSheetDisplayUserViews[zz].isTimeSheetApproved == true){

        //     // Hr 03-09-2021
        //     var AppDueDateAmendmend = timeSheetDisplayUserViews[zz].approverDueDateForAmendmend;
        //     var CurrentDate = moment(new Date()).format("MM-DD-YYYY HH:mm");//"09-15-2021 15:00";
        //     //var CurrentDate = "09-15-2021 15:00";
            
        //     if(Date.parse(AppDueDateAmendmend) > Date.parse(CurrentDate)){
        //       IsEditBtn = true;
        //       temp_row5.push(<td><a href="#past_details_modal" className="edit-icon float-left 777" data-toggle="modal"  onClick={this.PastDetails(role_permission,timeSheetPeriodId,contactId,fullName,isTimeSheetApproved,isTimeSheetSubmittedToPayroll,isSpecifcTimeSheetApprover,locationId,isPayrollAdmin,LocationGUID,IsEditBtn)} ><i className="fa fa-pencil" /></a><input type="checkbox" className="pk-sheet-margin-top" value={timeSheetDisplayUserViews[zz].timeSheetContactMasterId} name="checkbox" checked  disabled="true"/></td>);
        //     }else{
        //       if(timeSheetDisplayUserViews[zz].isAmendMends == true){
        //         temp_row5.push(<td><a href="#past_details_modal" className="edit-icon float-left 888" data-toggle="modal"  onClick={this.PastDetails(role_permission,timeSheetPeriodId,contactId,fullName,isTimeSheetApproved,isTimeSheetSubmittedToPayroll,isSpecifcTimeSheetApprover,locationId,isPayrollAdmin,LocationGUID,IsEditBtn)} ><i className="fa fa-eye" /></a><input type="checkbox" className="pk-sheet-margin-top" value={timeSheetDisplayUserViews[zz].timeSheetContactMasterId} name="checkbox" checked  disabled="true"/> <i className="fa fa-adn pk-font-gray-fa-fa" /> </td>);
        //       }else{
        //         temp_row5.push(<td><a href="#past_details_modal" className="edit-icon float-left 999" data-toggle="modal"  onClick={this.PastDetails(role_permission,timeSheetPeriodId,contactId,fullName,isTimeSheetApproved,isTimeSheetSubmittedToPayroll,isSpecifcTimeSheetApprover,locationId,isPayrollAdmin,LocationGUID,IsEditBtn)} ><i className="fa fa-eye" /></a><input type="checkbox" className="pk-sheet-margin-top" value={timeSheetDisplayUserViews[zz].timeSheetContactMasterId} name="checkbox" checked  disabled="true"/> </td>);
        //       }
        //     }
        //     // Hr 03-09-2021

            
            
        //   }else{
        //     if(timeSheetDisplayUserViews[zz].isAmendMends == true){
        //       temp_row5.push(<td><a href="#past_details_modal" className="edit-icon float-left 000" data-toggle="modal"  onClick={this.PastDetails(role_permission,timeSheetPeriodId,contactId,fullName,isTimeSheetApproved,isTimeSheetSubmittedToPayroll,isSpecifcTimeSheetApprover,locationId,isPayrollAdmin,LocationGUID,IsEditBtn)} ><i className="fa fa-pencil" /></a><input type="checkbox" className="pk-sheet-margin-top submit-staff-checkbox" value={timeSheetDisplayUserViews[zz].timeSheetContactMasterId} name="checkbox" /> <i className="fa fa-adn pk-font-red-fa-fa" /> </td>);
        //     }else{
        //       temp_row5.push(<td><a href="#past_details_modal" className="edit-icon float-left 101" data-toggle="modal"  onClick={this.PastDetails(role_permission,timeSheetPeriodId,contactId,fullName,isTimeSheetApproved,isTimeSheetSubmittedToPayroll,isSpecifcTimeSheetApprover,locationId,isPayrollAdmin,LocationGUID,IsEditBtn)} ><i className="fa fa-pencil" /></a><input type="checkbox" className="pk-sheet-margin-top submit-staff-checkbox" value={timeSheetDisplayUserViews[zz].timeSheetContactMasterId} name="checkbox" /> </td>);
        //     }
            
        //   }
        // }

        var edit_btn = [];
        var checkbox = [];
        var amendment_btn = [];
        var coordi_sr_submit_btn = [];
        let action_btn =[];
        var IsEditBtn = false;

        var timesheetDetails = timeSheetDisplayUserViews[zz];

        if(timesheetDetails.isAmendMends == true){
          amendment_btn.push(<i className="fa fa-adn pk-font-red-fa-fa" />);
        }

        
        var loc_mst = timeSheetDisplayUserViews[zz].timeSheetContactMasterId+","+locationId
        if(this.state.isPayrollAdminSession == true){
          IsEditBtn = true;
          if(timesheetDetails.isTimeSheetApproved == true && timesheetDetails.isTimeSheetSubmittedToPayroll == false){
            coordi_sr_submit_btn.push(<i className="fa fa-check pk-font-green-fa-fa" />);
          }

          edit_btn.push(<a href="#past_details_modal" className="edit-icon float-left" data-toggle="modal"  onClick={this.PastDetails(role_permission,timeSheetPeriodId,contactId,fullName,isTimeSheetApproved,isTimeSheetSubmittedToPayroll,isSpecifcTimeSheetApprover,locationId,isPayrollAdmin,LocationGUID,IsEditBtn)} ><i className="fa fa-pencil" /></a>);

          /* checkbox Button Condition Start*/
          if(timesheetDetails.isTimeSheetSubmittedToPayroll == true){
            checkbox.push(<input type="checkbox" className="pk-sheet-margin-top" value={loc_mst} name="checkbox" checked disabled="true" />);
          }else{
            checkbox.push(<input type="checkbox" className="pk-sheet-margin-top submit-staff-checkbox" value={loc_mst} name="checkbox" />);
          }
          /* checkbox Button Condition Start*/

        }
        else
        {
          /* EDIT Button Condition Start*/
          var AppDueDateAmendmend = timesheetDetails.approverDueDateForAmendmend;
          var CurrentDate = moment(new Date()).format("MM-DD-YYYY HH:mm");

          if(timesheetDetails.isTimeSheetSubmittedToPayroll == true || timesheetDetails.isTimeSheetApproved == true){
            if(Date.parse(AppDueDateAmendmend) > Date.parse(CurrentDate)){
              IsEditBtn = true;
              edit_btn.push(<a href="#past_details_modal" className="edit-icon float-left 777" data-toggle="modal"  onClick={this.PastDetails(role_permission,timeSheetPeriodId,contactId,fullName,isTimeSheetApproved,isTimeSheetSubmittedToPayroll,isSpecifcTimeSheetApprover,locationId,isPayrollAdmin,LocationGUID,IsEditBtn)} ><i className="fa fa-pencil" /></a>);
            }else{
              edit_btn.push(<a href="#past_details_modal" className="edit-icon float-left 111" data-toggle="modal"  onClick={this.PastDetails(role_permission,timeSheetPeriodId,contactId,fullName,isTimeSheetApproved,isTimeSheetSubmittedToPayroll,isSpecifcTimeSheetApprover,locationId,isPayrollAdmin,LocationGUID,IsEditBtn)} ><i className="fa fa-eye" /></a>);
            }
          }else{
              //IsEditBtn = true;
              //edit_btn.push(<a href="#past_details_modal" className="edit-icon float-left 777" data-toggle="modal"  onClick={this.PastDetails(role_permission,timeSheetPeriodId,contactId,fullName,isTimeSheetApproved,isTimeSheetSubmittedToPayroll,isSpecifcTimeSheetApprover,locationId,isPayrollAdmin,LocationGUID,IsEditBtn)} ><i className="fa fa-pencil" /></a>);
              if(Date.parse(AppDueDateAmendmend) > Date.parse(CurrentDate)){
                IsEditBtn = true;
                edit_btn.push(<a href="#past_details_modal" className="edit-icon float-left 777" data-toggle="modal"  onClick={this.PastDetails(role_permission,timeSheetPeriodId,contactId,fullName,isTimeSheetApproved,isTimeSheetSubmittedToPayroll,isSpecifcTimeSheetApprover,locationId,isPayrollAdmin,LocationGUID,IsEditBtn)} ><i className="fa fa-pencil" /></a>);
              }else{
                edit_btn.push(<a href="#past_details_modal" className="edit-icon float-left 111" data-toggle="modal"  onClick={this.PastDetails(role_permission,timeSheetPeriodId,contactId,fullName,isTimeSheetApproved,isTimeSheetSubmittedToPayroll,isSpecifcTimeSheetApprover,locationId,isPayrollAdmin,LocationGUID,IsEditBtn)} ><i className="fa fa-eye" /></a>);
              }
          }
          /* EDIT Button Condition End*/

          /* checkbox Button Condition Start*/
          if(Date.parse(AppDueDateAmendmend) > Date.parse(CurrentDate)){
            if(timesheetDetails.isTimeSheetSubmittedToPayroll == true || timesheetDetails.isTimeSheetApproved == true){
              checkbox.push(<input type="checkbox" className="pk-sheet-margin-top 880895" value={loc_mst} name="checkbox" checked  disabled="true"/>);
            }else{
              checkbox.push(<input type="checkbox" className="pk-sheet-margin-top submit-staff-checkbox 7878" value={loc_mst} name="checkbox" />);
            }
          }else{
            if(timesheetDetails.isTimeSheetSubmittedToPayroll == true || timesheetDetails.isTimeSheetApproved == true){
              checkbox.push(<input type="checkbox" className="pk-sheet-margin-top 88089" value={loc_mst} name="checkbox" checked  disabled="true"/>);
            }else{
              checkbox.push(<input type="checkbox" className="pk-sheet-margin-top submit-staff-checkbox 7878" value={loc_mst} name="checkbox" disabled="true" />);
            }
          } 
          /* checkbox Button Condition End*/
        }

        temp_row5.push(<td>{edit_btn}{checkbox}{amendment_btn}{coordi_sr_submit_btn}</td>);



        
        
      }else{
        // if(timeSheetDisplayUserViews[zz].isAmendMends == true){
        //   temp_row5.push(<td ><a href="#past_details_modal" className="edit-icon float-left 102" data-toggle="modal"  onClick={this.PastDetails(role_permission,timeSheetPeriodId,contactId,fullName,isTimeSheetApproved,isTimeSheetSubmittedToPayroll,0,locationId,isPayrollAdmin,LocationGUID,IsEditBtn)} ><i className="fa fa-pencil" /></a> <i className="fa fa-adn pk-font-red-fa-fa" /></td>);
        // }else{
        //   temp_row5.push(<td ><a href="#past_details_modal" className="edit-icon float-left 103" data-toggle="modal"  onClick={this.PastDetails(role_permission,timeSheetPeriodId,contactId,fullName,isTimeSheetApproved,isTimeSheetSubmittedToPayroll,0,locationId,isPayrollAdmin,LocationGUID,IsEditBtn)} ><i className="fa fa-pencil" /></a></td>);
        // }

        var edit_btn = [];
        var amendment_btn = [];
        let action_btn =[];
        var IsEditBtn = false;

        // if(timeSheetDisplayUserViews[zz].isAmendMends == true){
        //   amendment_btn.push(<i className="fa fa-adn pk-font-red-fa-fa" />);
        // }

        if(this.state.isPayrollAdminSession == true){
          IsEditBtn = true;
          edit_btn.push(<a href="#past_details_modal" className="edit-icon float-left 102" data-toggle="modal"  onClick={this.PastDetails(role_permission,timeSheetPeriodId,contactId,fullName,isTimeSheetApproved,isTimeSheetSubmittedToPayroll,0,locationId,isPayrollAdmin,LocationGUID,IsEditBtn)} ><i className="fa fa-pencil" /></a>);
        }else{
          var AppDueDateAmendmend = timeSheetDisplayUserViews[zz].approverDueDateForAmendmend;
          var CurrentDate = moment(new Date()).format("MM-DD-YYYY HH:mm");

          if(Date.parse(AppDueDateAmendmend) > Date.parse(CurrentDate)){
            IsEditBtn = true;
            edit_btn.push(<a href="#past_details_modal" className="edit-icon float-left 102" data-toggle="modal"  onClick={this.PastDetails(role_permission,timeSheetPeriodId,contactId,fullName,isTimeSheetApproved,isTimeSheetSubmittedToPayroll,0,locationId,isPayrollAdmin,LocationGUID,IsEditBtn)} ><i className="fa fa-pencil" /></a>);
          }else{
            edit_btn.push(<i className="fa fa-eye-slash pk-font-red-fa-fa" />);
          }
        }

        

        

        temp_row5.push(<td>{edit_btn}{amendment_btn}</td>);
        
      }
      

      temp_row.push(<tr>{temp_row1}{temp_row2}{temp_row3}{temp_row4}{temp_row5}</tr>);

      row.push(temp_row);
    }
    //row.push(<tr><td className="pk-td-border" colspan={row_header_count + 1}><b>Total full-time hours </b></td><td><b>{locationTotalHours}</b></td></tr>);
    return row;
  }

  location_wise_display_onegrid(){
    var locationWiseTimeSheeetViews = this.state.locationWiseTimeSheeetViews;
    var row_count = locationWiseTimeSheeetViews.length;
    // console.log('location_wise_display_grid 1');
    // console.log(locationWiseTimeSheeetViews);
    let row = [];
    let location_row = [];
    for (var zz = 0; zz < row_count; zz++) {
      location_row.push(this.row_display_onegrid(locationWiseTimeSheeetViews[zz].timeSheetDisplayUserViews,locationWiseTimeSheeetViews[zz].locationName,locationWiseTimeSheeetViews[zz].locationId))
    }

    let return_row = [];
    
    if(location_row.length > 0){
      return_row.push( 
            <div><div className="table-responsive">
                <table className="table table-nowrap">
                  <thead>
                   <tr>
                      <th>Full Name</th>
                      <th>Primary Location</th>
                      <th>Role</th>
                      <th>No Of Hours</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    {location_row}
                  </tbody>
                </table>
              </div>
            </div>
      );
    }
    

    return return_row;
  }


  TimeSheetContactMasterFunc(locationWiseTimeSheeetViews,arraytype){
    let mainarray = [];
  //  console.log('mainarray start');
  //  console.log(locationWiseTimeSheeetViews);
    //TimeSheetContactMasterArray
    
    var row_count = locationWiseTimeSheeetViews.length;
     //console.log('locationWiseTimeSheeetViews 1');
     //console.log(row_count);
    for (var zz = 0; zz < row_count; zz++) {
      var user_count = locationWiseTimeSheeetViews[zz].timeSheetDisplayUserViews;
      //console.log('timeSheetDisplayUserViews '+ zz);
      //console.log(user_count);
      for (var xx = 0; xx < user_count.length; xx++) {

          
          //console.log('TimeSheetContactMasterFunc');
          //console.log(locationWiseTimeSheeetViews[zz].timeSheetDisplayUserViews[xx]);
          var day_count = locationWiseTimeSheeetViews[zz].timeSheetDisplayUserViews[xx].timeSheetConatctSheetViews;
          let temp_TimeSheetContactTransactionId = [];

          //console.log('timeSheetDisplayUserViews '+ zz);
          //console.log(user_count);
          if(locationWiseTimeSheeetViews[zz].timeSheetDisplayUserViews[xx].timeSheetConatctSheetViews != null){
            for (var yy = 0; yy < day_count.length; yy++) {
              //console.log('timeSheetConatctSheetViews');
              //console.log(locationWiseTimeSheeetViews[zz].timeSheetDisplayUserViews[xx].timeSheetConatctSheetViews[yy]);
              temp_TimeSheetContactTransactionId.push(day_count[yy].timeSheetTransactionId);
            }

            if(temp_TimeSheetContactTransactionId.length > 0){
              let ArrayJson = {
                  locationId: user_count[xx].locationId,
                  TimeSheetContactMasterId: user_count[xx].timeSheetContactMasterId,
                  TimeSheetContactTransactionId : temp_TimeSheetContactTransactionId  
              };
              mainarray.push(ArrayJson);
            }
            
          }
          
      } 
    }

  //  console.log('mainarray');
  //  console.log(mainarray);

    if(arraytype == 'parent')
    {
      console.log('mainarray 2');
      console.log(mainarray);
      this.setState({ TimeSheetContactMasterArray: mainarray });
    }
  }

  

  SubmitUserTimeSheet = () => e => {
    console.log('SubmitUserTimeSheet');
    e.preventDefault();
    this.showLoader();
    let bodyarray = {};
    var MasterId =[];
  //  console.log(MasterId.length);
    $('.submit-staff-checkbox:checkbox:checked').each(function () {
        var sThisVal = (this.checked ? MasterId.push($(this).val()) : "");
    });

    if (MasterId.length == 0) {
      SystemHelpers.ToastError("Please select the staff.");
      $( ".cancel-btn" ).trigger( "click" );
      this.hideLoader();
      return false;
    }

  //  console.log('MasterId 1');
  //  console.log(MasterId);
    let TimesheetContactMasterSubmits = [];
    var TimeSheetContactMasterArray = this.state.TimeSheetContactMasterArray;
    console.log('TimeSheetContactMasterArray');
    console.log(TimeSheetContactMasterArray);
    var row_count = TimeSheetContactMasterArray.length;
  //  console.log("TimeSheetContactMasterArray =>"+ row_count);
    for (var zz = 0; zz < MasterId.length; zz++) {
        var result = MasterId[zz].split(',');
        console.log('result');
        console.log(result);
        for (var jj = 0; jj < row_count; jj++) {
          if(this.state.isemployeesListFilter == true || this.state.isroleListFilter == true){
            if(result[0] == TimeSheetContactMasterArray[jj].TimeSheetContactMasterId && result[1] == TimeSheetContactMasterArray[jj].locationId){
              TimesheetContactMasterSubmits.push(TimeSheetContactMasterArray[jj]);
            }
          }else{
            if(result[0] == TimeSheetContactMasterArray[jj].TimeSheetContactMasterId && result[1] == TimeSheetContactMasterArray[jj].locationId){
              TimesheetContactMasterSubmits.push(TimeSheetContactMasterArray[jj]);
            }
          }
        }
    }

    console.log(TimesheetContactMasterSubmits);
    //return false;
    bodyarray["ContactId"] = this.state.staffContactID;
    bodyarray["IsTimeSheetApprove"] = true;
    bodyarray["IsTimeSheetSubmitToPayRoll"] = false;
    bodyarray["TimesheetContactMasterSubmits"] = TimesheetContactMasterSubmits;
    bodyarray["SubmitByName"] = this.state.fullName;
    bodyarray["ApproveByName"] = this.state.fullName;
    //return false;
    var url=process.env.API_API_URL+'ApproveOrSubmitTimeSheetToPayroll';
    fetch(url, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
      body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
      //  console.log("responseJson ApproveOrSubmitTimeSheetToPayroll");
      //  console.log(data);
        if (data.responseType === "1") {
            this.GetUserTimeSheetDataByLocation();
            $( ".cancel-btn" ).trigger( "click" );
            SystemHelpers.ToastSuccess(data.responseMessge);
            this.DownloadCSVADPJson();
            
               
        }
        else{
            if(data.message == 'Authorization has been denied for this request.'){
                SystemHelpers.SessionOut();
                this.props.history.push("/login");
              }else{
                
              }
        }
        this.hideLoader();
        
    })
    .catch(error => {
      console.log('ApproveOrSubmitTimeSheetToPayroll error');
      this.props.history.push("/error-500");
    });
    return false;
  }

  SubmitPayroll = () => e => {
    console.log('SubmitPayroll');
    e.preventDefault();
    this.showLoader();
    let bodyarray = {};
    var MasterId =[];
  //  console.log(MasterId.length);
    $('.submit-staff-checkbox:checkbox:checked').each(function () {
        var sThisVal = (this.checked ? MasterId.push($(this).val()) : "");
    });

    if (MasterId.length == 0) {
      SystemHelpers.ToastError("Please select the staff.");
      $( ".cancel-btn" ).trigger( "click" );
      this.hideLoader();
      return false;
    }


    console.log('MasterId 2');
    console.log(MasterId);
    console.log(this.state.TimeSheetContactMasterArray);

    //return false;

    let TimesheetContactMasterSubmits = [];
    var TimeSheetContactMasterArray = this.state.TimeSheetContactMasterArray;
    var row_count = TimeSheetContactMasterArray.length;
    
    // console.log('TimeSheetContactMasterArray');
    // console.log(TimeSheetContactMasterArray);
    // console.log("TimeSheetContactMasterArray => "+ row_count);

    // console.log("isemployeesListFilter => "+ this.state.isemployeesListFilter);
    // console.log("isroleListFilter => "+ this.state.isroleListFilter);

    for (var zz = 0; zz < MasterId.length; zz++) {
        var result = MasterId[zz].split(',');
        console.log('result');
        console.log(result);
        for (var jj = 0; jj < row_count; jj++) {
          if(this.state.isemployeesListFilter == true || this.state.isroleListFilter == true){
            if(result[0] == TimeSheetContactMasterArray[jj].TimeSheetContactMasterId && result[1] == TimeSheetContactMasterArray[jj].locationId){
              TimesheetContactMasterSubmits.push(TimeSheetContactMasterArray[jj]);
            }
          }else{
          //  console.log("MasterId[zz] => "+ MasterId[zz]);
          //  console.log("TimeSheetContactMasterArray[jj].TimeSheetContactMasterId => "+ TimeSheetContactMasterArray[jj].TimeSheetContactMasterId);
          //  console.log("TimeSheetContactMasterArray[jj] => ")
          //  console.log(TimeSheetContactMasterArray[jj]);
          //  console.log("*****************************************");
            if(result[0] == TimeSheetContactMasterArray[jj].TimeSheetContactMasterId && result[1] == TimeSheetContactMasterArray[jj].locationId){
              TimesheetContactMasterSubmits.push(TimeSheetContactMasterArray[jj]);
            }
          }
          
        }
    }
    console.log(TimesheetContactMasterSubmits);

    //return false;
    bodyarray["ContactId"] = this.state.staffContactID;
    bodyarray["IsTimeSheetApprove"] = true;
    bodyarray["IsTimeSheetSubmitToPayRoll"] = true;
    bodyarray["TimesheetContactMasterSubmits"] = TimesheetContactMasterSubmits;
    bodyarray["SubmitByName"] = this.state.fullName;
    bodyarray["ApproveByName"] = this.state.fullName;

    //return false;
    var url=process.env.API_API_URL+'ApproveOrSubmitTimeSheetToPayroll';
    fetch(url, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
      body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
      //  console.log("responseJson SubmitUserTimeSheet");
      //  console.log(data);
        if (data.responseType === "1") {

           $( ".cancel-btn" ).trigger( "click" );
            SystemHelpers.ToastSuccess(data.responseMessge);
            
            this.GetUserTimeSheetDataByLocation(); 
            this.DownloadCSVADPJson();   
        }
        else{
            if(data.message == 'Authorization has been denied for this request.'){
                SystemHelpers.SessionOut();
                this.props.history.push("/login");
              }else{
                
              }
        }
        this.hideLoader();
        
    })
    .catch(error => {
      console.log('ApproveOrSubmitTimeSheetToPayroll error');
      this.props.history.push("/error-500");
    });
    return false;
  }

  SubmitAmendmendFromAttSummary = () => e => {
  //  console.log('SubmitAmendmendFromAttSummary');
    e.preventDefault();
    this.showLoader();

  //  console.log(this.state.ListGridPastDetails);

    var ListGridPastDetails = this.state.ListGridPastDetails;
    var row_count = ListGridPastDetails.length;
    var count = 1;

    var trans_id_str = '';
    for (var zz = 0; zz < row_count; zz++) {

      if(count == row_count){
        trans_id_str+=ListGridPastDetails[zz].timeSheetTransactionId;
      }else{
        trans_id_str+=ListGridPastDetails[zz].timeSheetTransactionId+",";
      }
      count++;

    }

  //  console.log(trans_id_str);
    //return false;
    var url=process.env.API_API_URL+'SubmitAmendmendFromAttSummary?submitByContactId='+this.state.staffContactID+"&timesheetTransactionId="+trans_id_str;
    fetch(url, {
      method: 'PUT',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      }
    }).then((response) => response.json())
    .then(data => {
      //  console.log("responseJson SubmitAmendmendFromAttSummary");
      //  console.log(data);
        if (data.responseType === "1") {
            this.GetUserTimeSheetDataByLocation();
            $( ".cancel-btn" ).trigger( "click" );
            SystemHelpers.ToastSuccess(data.responseMessge);
            this.DownloadCSVADPJson();
            
               
        }
        else{
            if(data.message == 'Authorization has been denied for this request.'){
                SystemHelpers.SessionOut();
                this.props.history.push("/login");
              }else{
                
              }
        }
        this.hideLoader();
        
    })
    .catch(error => {
      console.log('SubmitAmendmendFromAttSummary error');
      this.props.history.push("/error-500");
    });
    return false;
  }

  

  DownloadCSVADPJson1(){
    this.showLoader();

    var url=process.env.API_API_URL+'DownloadCSVADPJson?periodProfileId='+localStorage.getItem("CurrenttimeSheetPeriodId");
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      }
    }).then((response) => response.json())
    .then(data => {
      //  console.log("responseJson DownloadCSVADPJson");
      //  console.log(data);
        let CsvDateArray = [];
        alert.log();
        if(data.data != null){
          var CsvDate = data.data;
          var CsvDatelength = CsvDate.length;
          for (var z = 0; z < CsvDatelength; z++) {

            var badge = CsvDate[z].badge;
            var datetime = CsvDate[z].datetime;
            var dtaddress = CsvDate[z].dtaddress;
            var funC_COD = CsvDate[z].funC_COD;
            var lcF1 = CsvDate[z].lcF1;

            var tempdataArray = [];
            tempdataArray.badge = badge.replace(/\"/g, "");
            tempdataArray.datetime = datetime.replace(/\"/g, "");
            tempdataArray.dtaddress = dtaddress.replace(/\"/g, "");
            tempdataArray.funC_COD = funC_COD.replace(/\"/g, "");
            tempdataArray.lcF1 = lcF1.replace(/\"/g, "");
            CsvDateArray.push(tempdataArray);
          }
          this.setState({ csvdata: data.data });
          
        }

        this.hideLoader();
        
    })
    .catch(error => {
      console.log('DownloadCSVADPJson error');
      this.props.history.push("/error-500");
    });
    return false;
  }

  // this method call
  DownloadCSVADPJson = () => e => {

    e.preventDefault();
    this.showLoader();



    var url=process.env.API_API_URL+'DownloadCSVADPJson?periodProfileId='+this.state.CurrenttimeSheetPeriodId;
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      }
    }).then((response) => response.json())
    .then(data => {
      //  console.log("responseJson DownloadCSVADPJson");
      //  console.log(data);
        let CsvDateArray = [];
        if(data.data != null){
          var CsvDate = data.data;
          var CsvDatelength = CsvDate.length;
          for (var z = 0; z < CsvDatelength; z++) {

            var badge = CsvDate[z].badge;
            var datetime = CsvDate[z].datetime;
            var dtaddress = CsvDate[z].dtaddress;
            var funC_COD = CsvDate[z].funC_COD;
            var lcF1 = CsvDate[z].lcF1;

            var tempdataArray = [];
            tempdataArray.badge = badge.replace(/\"/g, "");
            tempdataArray.datetime = datetime.replace(/\"/g, "");
            tempdataArray.dtaddress = dtaddress.replace(/\"/g, "");
            tempdataArray.funC_COD = funC_COD.replace(/\"/g, "");
            tempdataArray.lcF1 = lcF1.replace(/\"/g, "");
            CsvDateArray.push(tempdataArray);
          }
          this.setState({ csvdata: data.data });
          this.csvLink.link.click();
          //debugger;
          //alasql("SELECT * INTO CSV('names.csv') FROM ?", [data.data]);

          
        }

        this.hideLoader();
        
    })
    .catch(error => {
      console.log('DownloadCSVADPJson error');
      this.props.history.push("/error-500");
    });
    return false;
  }

  DownloadCSVADP = () => e => {
    //alert();
    e.preventDefault();
    this.showLoader();

    var url=process.env.API_API_URL+'DownloadCSVADP?periodProfileId='+localStorage.getItem("CurrenttimeSheetPeriodId");
    axios.get(url, {
      headers: {
    'Authorization': 'bearer '+localStorage.getItem("token")
      },
    // include your additional POSTed data here
        responseType: "arraybuffer"
    }).then((response) => {
        //debugger;

        if (response.status === 200 && response.data.byteLength > 0) {
            var type = response.headers['content-type'];
            // var disposition = response.headers('Content-Disposition');
            // if (disposition) {
            //     var match = disposition.match(/.*filename=\"?([^;\"]+)\"?.*/);
            //     if (match[1])
            //         defaultFileName = match[1];
            // }
            // defaultFileName = defaultFileName.replace(/[<>:"\/\\|?*]+/g, '_');
            var blob = new Blob([response.data], { type: type });
            saveAs(blob, "Payroll.csv");
            this.hideLoader();
        }

    
    }).catch((error) => {
        // ...
        this.hideLoader();
    });
  }

  DownloadCSVADP1 = () => e => {
    e.preventDefault();
    debugger;
    this.showLoader();
    let bodyarray = {};
   
    //return false;
    var url=process.env.API_API_URL+'DownloadCSVADP?periodProfileId=1f63ab42-4aa4-eb11-b1ac-6045bd72c5ab';
    fetch(url, {
      method: 'GET',
      responseType: "blob",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'X-Requested-With': 'XMLHttpRequest',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
      //body: JSON.stringify(bodyarray)
    }).then((response) => response)
    .then(response => {
        debugger;
        //console.log("xml", response)
        //console.log("responseJson DownloadCSVADP");


      // console.log(data);

       if (response.status === 200 && response.data.byteLength > 0) {
            var type = response.headers('Content-Type');
            var disposition = response.headers('Content-Disposition');
            if (disposition) {
                var match = disposition.match(/.*filename=\"?([^;\"]+)\"?.*/);
                if (match[1])
                    defaultFileName = match[1];
            }
            defaultFileName = defaultFileName.replace(/[<>:"\/\\|?*]+/g, '_');
            var blob = new Blob([response.data], { type: type });
            saveAs(blob, defaultFileName);
        }
        else {
            // $scope.ShuffleSectionStudentView = null;
            // $scope.IsData = false;
            // ShowNotification(Notification.warning, 'Required Details Not Found..!', 'top right');
        }

        // var bytes = new Uint8Array(data); // pass your byte response to this constructor

        // var blob=new Blob([bytes], {type: "application/vnd.ms-excel"});// change resultByte to bytes

        // var link=document.createElement('a');
        // link.href=window.URL.createObjectURL(blob);
        // link.download="myFileName.csv";
        // link.click();


        // if (data !== null && data !== "" && data !== undefined) {
        //   if (data.byteLength > 0) {
        //       var type = data.headers('Content-Type');
        //       var disposition = data.headers('Content-Disposition');
        //       if (disposition) {
        //           var match = disposition.match(/.*filename=\"?([^;\"]+)\"?.*/);
        //           if (match[1])
        //               defaultFileName = match[1];
        //       }
        //       defaultFileName = defaultFileName.replace(/[<>:"\/\\|?*]+/g, '_');
        //       var blob = new Blob([data], { type: type });
        //       saveAs(blob, defaultFileName);
        //   }
        //   else {
        //       alert("error");
        //   }
        // }
      this.hideLoader();
        
    })
    .catch(error => {
      console.log('DownloadCSVADP error');
      this.props.history.push("/error-500");

    });
    return false;
  }

  AddNotes_Msg = () => e => {
    //debugger;
    
    e.preventDefault();

    let step1Errors = {};
    
    
    

    if(this.state.filePreviewsFinal.length == 0 && this.state["AddMsg"] == ''){

      if (this.state["AddMsg"] == '')
      {
        step1Errors["AddMsg"] = "Notes is mandatory";
      }

      if (this.state.filePreviewsFinal.length == 0)
      {
        step1Errors["Addattachment"] = "Please Select Attachment File.";
      }
        
    }

    this.setState({ errormsg: step1Errors });
        
    if (Object.keys(step1Errors).length > 0) {
      return false;
    }

    var isFileAttached = false;
    let File_Push = [];
    if(this.state.filePreviewsFinal.length > 0)
    {
      // var file = this.state.filePreviewsFinal[0].FileData;
      // var fileType = file.match(/[^:]\w+\/[\w-+\d.]+(?=;|,)/)[0];
      // var base64result = file.split(',')[1];
      var isFileAttached = true;
      
      var FileUpload = this.state.filePreviewsFinal;
      for (var z = 0; z < FileUpload.length; z++)
      {
        var file = FileUpload[z].FileData;
        var fileType = file.match(/[^:]\w+\/[\w-+\d.]+(?=;|,)/)[0];
        var base64result = file.split(',')[1];

        let File = {};
        File.FileType = fileType;
        File.Base64String = base64result;
        File.DocumentType = 'Timesheet Notes';
        File.documentName = FileUpload[z].FileName;
        File.documentId = null;
        File.status = true;
        File_Push.push(File);
      }
    }

    if(this.state.filePreviewsFinalEdit.length > 0){
      var isFileAttached = true;
      
      var FileUploadEdit = this.state.filePreviewsFinalEdit;

      for (var j = 0; j < FileUploadEdit.length; j++)
      {
        if(FileUploadEdit[j].status == false){

          let File = {};
          File.base64String = FileUploadEdit[j].base64String;
          File.documentId = FileUploadEdit[j].documentId;
          File.documentName = FileUploadEdit[j].documentName;
          File.documentType = FileUploadEdit[j].documentType;
          File.fileType = FileUploadEdit[j].fileType;
          File.status = FileUploadEdit[j].status;
          File_Push.push(File);

        }
        
      }

    }

    console.log(File_Push);

    //return false;
    this.showLoader();
    console.log('templocationId => '+ this.state.templocationId);
    
    
    let bodyarray = {
      noteId: this.state["EditNoteId"],
      note: this.state["AddMsg"],
      locationId: this.state.templocationId,
      isFileAttached: isFileAttached,
      allowUserToSeeNotes: this.state.allow_emp,
      //fileType: fileType,
      timesheetcontactmasterId: this.state.EdittimeSheetContactMasterId,
      contactId: this.state.staffContactID,
      TimeSheetNotesFile: File_Push
      //base64String: base64result
    };
     
    // let bodyarray = {};
    // bodyarray["contactId"] = this.state.staffContactID;
    // bodyarray["documentView"] = ArrayJson;

  //  console.log(bodyarray);
    //return false;
    var url=process.env.API_API_URL+'CreateUpdateTimesheetNoteDetails';
    fetch(url, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
      body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
      //  console.log("responseJson CreateUpdateTimesheetNoteDetails");
      //  console.log(data);
        //console.log(responseJson);
        // debugger;
        if (data.responseType === "1") {
            //this.props.history.push('/dashboard');
            this.setState({ AddMsg: '' });
            //alert();
            this.setState({ errormsg: '' });
            this.setState({ allow_emp : false });
            this.setState({ EditNoteId: '' });

            this.setState({ filePreviewsFinalEdit: [] });
            this.setState({ filePreviewsFinalEditActive: 0 });

            $( ".removeDocumentFunc" ).trigger( "click" );
            //this.setState({ AddNotesResetflag : true });
            //setTimeout(this.setState({ AddNotesResetflag : false }), 1000);
            this.GetTimeSheetNotesLoad(this.state.EdittimeSheetContactMasterId,this.state.templocationId,);
            //SystemHelpers.ToastSuccess(data.responseMessge);
            //$( ".close" ).trigger( "click" );
            //this.GetUserDocuments();    
        }
        else{
            SystemHelpers.ToastError(data.message  );
        }
        this.hideLoader();
        
    })
    .catch(error => {
      console.log('CreateUpdateTimesheetNoteDetails error');
      this.props.history.push("/error-500");
    });
    return false;
  }



  GetImageAllNew(baseurl){
      var imgbase=baseurl;
      var gettype=this.base64MimeType(baseurl);

      let Image_return = [];
      let Image_return_Final = [];

      if(gettype == 'image/png' || gettype == 'image/jpeg' || gettype == 'image/jpg' ){
          var image_var = <img src={baseurl} alt={'image preview'} style={{ width: 50, height:50, margin: 10 }} />;
      } else if(gettype == 'application/pdf'){
          var image_var = <img src={PdfImg} alt={'image preview'} style={{ width: 50, height:50, margin: 10 }} />;
      } else if(gettype == 'application/vnd.ms-excel'){
          var image_var = <img src={ExcelImg} alt={'image preview'} style={{ width: 50, height:50, margin: 10 }} />;
      } else if(gettype == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'){
          var image_var = <img src={ExcelImg} alt={'image preview'} style={{ width: 50, height:50, margin: 10 }} />;
      } else if(gettype == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' || gettype == 'application/msword'){
          var image_var = <img src={DocxImg} alt={'image preview'} style={{ width: 50, height:50, margin: 10 }} />;
      } 

      Image_return_Final.push(
          <div className="col-6 col-sm-4 col-md-3 col-lg-4 col-xl-3">
              <div className="card card-file">
                {/*<div className="dropdown-file">
                  <a href="" className="dropdown-link" data-toggle="dropdown"><i className="fa fa-ellipsis-v" /></a>
                  <div className="dropdown-menu dropdown-menu-right">
                    <a href="#" onClick={this.NewTabOpen(baseurl)} className="dropdown-item">Download</a>
                    <a href="#" className="dropdown-item">Delete</a>
                  </div>
                </div>*/}
                <div className="card-file-thumb">
                  {image_var}
                </div>
              </div>
          </div>
      );



      return Image_return_Final;
  }

  GetImageAllNewImg(ImgDetails){
      var FileNm = ImgDetails.documentName;
      var FileUrl = ImgDetails.base64String;
      var gettype = FileNm.substr( (FileNm.lastIndexOf('.') +1));

      console.log(gettype);
      // var imgbase=baseurl;
      // var gettype=this.base64MimeType(baseurl);

      let Image_return = [];
      let Image_return_Final = [];

      if(gettype == 'png' || gettype == 'jpeg' || gettype == 'jpg' ){
          var image_var = <img src={FileUrl} alt={'image preview'} style={{ width: 50, height:50, margin: 10 }} />;
      } else if(gettype == 'pdf'){
          var image_var = <img src={PdfImg} alt={'image preview'} style={{ width: 50, height:50, margin: 10 }} />;
      } else if(gettype == 'csv' || gettype == 'xlsx' || gettype == 'xls' ){
          var image_var = <img src={ExcelImg} alt={'image preview'} style={{ width: 50, height:50, margin: 10 }} />;
      }  else if(gettype == 'docx' || gettype == 'doc'){
          var image_var = <img src={DocxImg} alt={'image preview'} style={{ width: 50, height:50, margin: 10 }} />;
      } 

      if(ImgDetails.status == true){
        Image_return_Final.push(
            <div className="col-6 col-sm-4 col-md-3 col-lg-4 col-xl-3">
                <div className="card card-file">
                  <div className="dropdown-file">
                    <a href="" className="dropdown-link" data-toggle="dropdown"><i className="fa fa-ellipsis-v" /></a>
                    <div className="dropdown-menu dropdown-menu-right">
                      <a href={FileUrl} target="_blank" className="dropdown-item">Download</a>
                      <a href="#" onClick={this.DeleteImg(ImgDetails.documentId)} className="dropdown-item">Delete</a>
                    </div>
                  </div>
                  <div className="card-file-thumb">
                    {image_var}
                  </div>
                </div>
            </div>
        );
      }



      return Image_return_Final;
  }

  EditMsg = (MsgDetails) => e => {
    e.preventDefault();
    this.setState({ filePreviewsFinalEdit: [] });
    this.setState({ filePreviewsFinalEditActive: 0 });
    console.log(MsgDetails);
    this.setState({ allow_emp: MsgDetails.allowUserToSeeNotes });
    this.setState({ EditNoteId: MsgDetails.noteId });
    this.setState({ AddMsg: MsgDetails.note });

    if(MsgDetails.timeSheetNotesFile != null){
      console.log('timeSheetNotesFile');

      let temp = [];
      var FileList = MsgDetails.timeSheetNotesFile;
      for (var z = 0; z < FileList.length; z++)
      {
        temp.push({
          base64String: FileList[z].base64String,
          documentId: FileList[z].documentId,
          documentName: FileList[z].documentName,
          documentType: FileList[z].documentType,
          fileType: FileList[z].fileType,
          status: true
        });
      }

      this.setState({ filePreviewsFinalEditActive: FileList.length });
      this.setState({ filePreviewsFinalEdit: temp });
    }
  }

  DeleteImg = (documentId) => e => {
      e.preventDefault();
      console.log(documentId);
      if(confirm("Are you sure you want to delete this?"))
      {
        let temp = [];
        var FileList = this.state.filePreviewsFinalEdit;
        var active = 0;
        for (var z = 0; z < FileList.length; z++)
        {
          if(FileList[z].documentId == documentId){
            temp.push({
              base64String: FileList[z].base64String,
              documentId: FileList[z].documentId,
              documentName: FileList[z].documentName,
              documentType: FileList[z].documentType,
              fileType: FileList[z].fileType,
              status: false
            });
          }else{
            temp.push({
              base64String: FileList[z].base64String,
              documentId: FileList[z].documentId,
              documentName: FileList[z].documentName,
              documentType: FileList[z].documentType,
              fileType: FileList[z].fileType,
              status: FileList[z].status
            });

            if(FileList[z].status){
              active++;
            }
          }

        }

        this.setState({ filePreviewsFinalEditActive: active });
        this.setState({ filePreviewsFinalEdit: temp });
        return false;

      }else{
        return false;
      }
      
  }

  NewTabOpen = (baseurl) => e => {
      e.preventDefault();
      console.log(baseurl);
      
      const linkSource = baseurl;
      const downloadLink = document.createElement("a");
  

      downloadLink.href = linkSource;
      downloadLink.download = this.GetFileName(baseurl);
      downloadLink.click();
      return false;
  }

  GetImageAll(baseurl){
      var imgbase=baseurl;
      var gettype=this.base64MimeType(baseurl);

      let Image_return = [];

      if(gettype == 'image/png' || gettype == 'image/jpeg' || gettype == 'image/jpg' ){
          Image_return.push(<img src={baseurl} alt={'image preview'} style={{ width: 50, height:50, margin: 10 }} />);
      } else if(gettype == 'application/pdf'){
          Image_return.push(<img src={PdfImg} alt={'image preview'} style={{ width: 50, height:50, margin: 10 }} />);
      } else if(gettype == 'application/vnd.ms-excel'){
          Image_return.push(<img src={ExcelImg} alt={'image preview'} style={{ width: 50, height:50, margin: 10 }} />);
      } else if(gettype == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'){
          Image_return.push(<img src={ExcelImg} alt={'image preview'} style={{ width: 50, height:50, margin: 10 }} />);
      } else if(gettype == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' || gettype == 'application/msword'){
          Image_return.push(<img src={DocxImg} alt={'image preview'} style={{ width: 50, height:50, margin: 10 }} />);
      }

      return Image_return;
  }

  base64MimeType(encoded) {
    var result = null;

    if (typeof encoded !== 'string') {
      return result;
    }

    var mime = encoded.match(/data:([a-zA-Z0-9]+\/[a-zA-Z0-9-.+]+).*,.*/);

    if (mime && mime.length) {
      result = mime[1];
    }

    return result;
  }



  // Entitlement Summary
  GetTimseheetEntitlementSummarySummationLocationWsie(contactId,LocationGUID,periodProfileId){

      /* Role Management */
      var getrole = SystemHelpers.GetRole();
      /* Role Management */

      this.showLoader();
      var url=process.env.API_API_URL+'GetTimseheetSummarySummationDataLocationWsie'+'?contactId='+contactId+'&locationId='+LocationGUID+'&periodProfileId='+periodProfileId;
      fetch(url, {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'bearer '+localStorage.getItem("token")
        },
      }).then((response) => response.json())
      .then(data => {
          console.log("responseJson GetTimseheetSummarySummationDataLocationWsie");
          console.log(data);
          if (data.responseType === "1") {
              if(data.data != null)
              {
                this.setState({ DisplaytotalRegularHours: data.data.totalRegularHours });
                this.setState({ DisplaytotalAsleepHours: data.data.totalAsleepHours });
                this.setState({ DisplaytotalSickHours: data.data.totalSickHours });
                this.setState({ DisplaytotalTripHours: data.data.totalTripHours });
                this.setState({ DisplaytotalOTHours: data.data.totalOTHours });
                this.setState({ DisplaytotalBRVHours: data.data.totalBRVHours });
                this.setState({ DisplaytotalLeaveHours: data.data.totalLeaveHours });
                this.setState({ DisplaytotalOnCallHours: data.data.totalOnCallHours });
                this.setState({ DisplaytotalStatHours: data.data.totalStatHours });
              }
              this.hideLoader();
          }else{
            if(data.message == 'Authorization has been denied for this request.'){
              SystemHelpers.SessionOut();
              this.props.history.push("/login");
            }else{
              SystemHelpers.ToastError(data.message);
            }
            this.hideLoader();    
          }
      })
      .catch(error => {
        console.log('GetTimseheetSummarySummationDataLocationWsie error');
        this.props.history.push("/error-500");
      });
  }
  // Entitlement Summary
  
  handleChangeSelectLocation = (selectedOptionLocation) => {
    this.setState({ selectedOptionLocation });
    //console.log(`Option selected:`, selectedOptionLocation.value);
    this.setState({ LocationFilterID: selectedOptionLocation.value });
    //LocationFilterID
  };

  handleChangeSelectEmployees = (selectedOptionEmployees) => {
    this.setState({ selectedOptionEmployees });
    //console.log(`Option selected:`, selectedOptionLocation.value);
    this.setState({ employeeFilterID: selectedOptionEmployees.value });
    //LocationFilterID
  };

  

  render() {  
  const { selectedOptionLocation,selectedOptionEmployees  } = this.state;                 
  return ( 
    <div className="main-wrapper">
        {/* Toast & Loder method use */}
            
        {(this.state.loading) ? <Loader /> : null} 
        {/* Toast & Loder method use */}
      <Header/>
        <div className={this.state.unauthorized_cls}>
          {/*<h1>403</h1>*/}
          <h4><i className="fa fa-warning"></i> Sorry, you are not authorized to access this page.</h4>
          {/*<p>Sorry, you are not authorized to access this page.</p>*/}
        </div>

        <div className={this.state.page_wrapper_cls}>
            <Helmet>
                <title>Timesheet - PARTICIPATION HOUSE SUPPORT SERVICES</title>
                <meta name="description" content="Login page"/>         
            </Helmet>
              {/* Page Content */}
            <div className="content container-fluid">
              {/* Page Header */}

              

              <div className="page-header">

                <div className="row">
                  <div className="col-sm-8">
                    <h3 className="page-title">Time Sheet Summary</h3>
                    <ul className="breadcrumb">
                      <li className="breadcrumb-item"><a href="/dashboard">Time Sheet</a></li>
                      <li className="breadcrumb-item active">Time Sheet Summary</li>
                    </ul>
                  </div>
                  <div className="col-sm-4">  
                  { this.state.role_timesheet_can.timesheet_can_export == true ?
                    <CSVLink 
                      id="CSVLinkId"
                      style={{"display" : "none"}}
                      className="btn btn-danger mr-1 float-right" 
                      headers={this.state.headers} 
                      filename={"payroll.csv"} 
                      data={this.state.csvdata} 
                      ref={(r) => this.csvLink = r}
                      >Export CSV</CSVLink>
                    : null
                  }
                  
                  { this.state.role_timesheet_can.timesheet_can_export == true && this.state.load_attendance_summary == true ?
                    <button type="button" class="btn btn-danger mr-1 float-right" id="export_btn_show" onClick={this.DownloadCSVADPJson()}>Export CSV</button> : null
                  }
                </div>

                  

                </div>
              </div>
              {/* /Page Header */}

              

              <div className="card tab-box">



                <div className="row user-tabs">
                  <div className="col-lg-12 col-md-12 col-sm-12 line-tabs">
                    <ul className="nav nav-tabs nav-tabs-bottom">
                      <li className="nav-item"><a href="#current_time_staff_tab" data-toggle="tab" className="nav-link active">Time Sheet Summary</a></li>
                      
                      
                    </ul>
                  </div>
                </div>
              </div>
              
              

              <div className="tab-content">
                {/* //============ Current Time Staff Tab ============ */}
                <div id="current_time_staff_tab" className="pro-overview tab-pane fade show active">
                  
                  <div className="row">
                    <div className="col-lg-6">
                        <div className="dash-info-list pk-sheet-margin-bottom">
                          <a href="#" className="dash-card text-danger">
                            <div className="dash-card-container">
                              <div className="dash-card-icon">
                                <i className="fa fa-suitcase" />
                              </div>
                              <div className="dash-card-content">
                                <p>Primary location</p>
                              </div>
                              <div className="dash-card-avatars">
                                {this.state.primarylocation}
                              </div>
                            </div>
                          </a>
                        </div>
                    </div>
                    <div className="col-lg-6">
                        <div className="dash-info-list pk-sheet-margin-bottom">
                          <a href="#" className="dash-card text-danger">
                            <div className="dash-card-container">
                              <div className="dash-card-icon">
                                <i className="fa fa-hourglass-o" />
                              </div>
                              <div className="dash-card-content">
                                <p>Period</p>
                              </div>
                              <div className="dash-card-avatars">
                                {this.state.CurrentDate_Min != '' ? moment(this.state.CurrentDate_Min).add(1, 'day').format(process.env.DATE_FORMAT) : null} to {this.state.CurrentDate_Max != '' ? moment(this.state.CurrentDate_Max).format(process.env.DATE_FORMAT) : null }
                              </div>
                            </div>
                          </a>
                        </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-lg-6">
                      <div className="dash-info-list pk-sheet-margin-bottom">
                        <div className="dash-card">
                          <div className="dash-card-container">
                            <div className="dash-card-icon">
                              <i className="fa fa-building-o" />
                            </div>
                            <div className="dash-card-content">
                              <p>Total Time Shift</p>
                            </div>
                            <div className="dash-card-avatars">
                              {this.state.totalSchedualHours}
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div className="col-lg-6">
                      <div className="dash-info-list pk-sheet-margin-bottom">
                        <div className="dash-card">
                          <div className="dash-card-container">
                            <div className="dash-card-icon">
                              <i className="fa fa-user" />
                            </div>
                            <div className="dash-card-content">
                              <p>Role</p>
                            </div>
                            <div className="dash-card-avatars">
                              {this.state.userRoleDisplay}
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  
                  
                  
                  {/* Search Filter */}
                  <div className="row filter-row">
                    
                    
                    <div className="col-lg-3 col-sm-12 col-xs-12">
                     {this.state.isPayrollAdminSession == true ?
                        <div class="checkbox pk-margin-top-summary">
                          <label>
                            <input type="checkbox" name="checkbox" id="pendingforapproval" /> Show Pending For Approval
                          </label>
                        </div>
                      : null
                     } 
                    </div>
                    {/*<div className="col-sm-6 col-md-3"></div>*/}
                   

                    <div className="col-lg-6 col-sm-12 col-xs-12">  
                      <div className="form-group row">
                        <label className="col-lg-5 col-form-label">Time Sheet Period Selection :</label>
                        <div className="col-lg-7"> 
                          <select className="select floating" id="timeSheetPeriod" value={this.state.timeSheetPeriod} onChange={this.handleChange('timeSheetPeriod')}>
                            {this.state.timeSheetPeriodViews.map(( listValue, index ) => {
                              return (
                                <option key={index} value={listValue.timeSheetPeriodId}>{moment(listValue.timeSheetPeriodStartDate,process.env.API_DATE_FORMAT).format(process.env.DATE_FORMAT)} to {moment(listValue.timeSheetPeriodEndDate,process.env.API_DATE_FORMAT).format(process.env.DATE_FORMAT)}</option>
                              );
                            })}
                          </select>
                        </div>
                      </div>
                    </div>

                    <div className="col-lg-3 col-sm-12 col-xs-12 margin-bottom">  
                      <a href="#" className="btn btn-success btn-block" id="ChangePeriod" onClick={this.ChangePeriod}> Change Time Sheet Period </a>  
                    </div>     

                  </div>

                  {/* Search Filter */}
                <div className="row filter-row">
                  
                  
                  
                  <div className="col-lg-3 col-sm-4 col-xs-12"> 
                    <div className="form-group form-focus select-focus">
                        
                      
                      {/*<select className="form-control floating" id="LocationFilterID" value={this.state.LocationFilterID} onChange={this.handleChange('LocationFilterID')}> 
                        <option value="">All</option>
                        {this.state.LocationListFilter.map(( listValue, index ) => {
                         
                            return (
                              <option key={index} value={listValue.locationId} >{listValue.locationName}</option>
                            );
                         
                        })}
                      </select>
                      <label className="focus-label">Search Location</label>*/}

                      {/* HR 4-10-2021*/}
                      <Select
                        value={selectedOptionLocation}
                        onChange={this.handleChangeSelectLocation}
                        options={this.state.LocationListFilter}
                        defaultValue={""}
                        isSearchable={true}
                        placeholder="Search Location"
                      />
                      {/* HR 4-10-2021*/}
                      
                    </div>
                  </div>
                  

                  {/*<div className="col-sm-6 col-md-2"> 
                    <div className="form-group form-focus select-focus">
                      <select className="form-control floating" id="sortColumnId" value={this.state.sortColumn} onChange={this.handleChange('sortColumn')}> 
                        <option value="">-</option>
                        <option value="FirstName">Name</option>
                        <option value="PhssEmail">Email</option>
                        <option value="UserRoleDisplay">Role</option>
                      </select>
                      <label className="focus-label">Sorting</label>
                    </div>
                  </div>*/}

                  <div className="col-lg-3 col-sm-4 col-xs-12"> 
                    <div className="form-group form-focus select-focus">
                     {/* <select className="form-control" id="employeeFilterID" value={this.state.employeeFilterID}  onChange={this.handleChange('employeeFilterID')} >
                        <option value="">-</option>
                        {this.state.employeesListFilter.map(( listValue, index ) => {
                            return (
                              <option key={index} value={listValue.contactId}>{listValue.employeeName}</option>
                            );
                        })}
                      </select>
                      <label className="focus-label">Employees</label>*/}

                      {/* HR 4-10-2021*/}

                      <Select
                        value={selectedOptionEmployees}
                        onChange={this.handleChangeSelectEmployees}
                        options={this.state.employeesListFilter}
                        defaultValue={""}
                        isSearchable={true}
                        placeholder="Search Employee"
                      />
                      {/* HR 4-10-2021*/}

                    </div>
                  </div>

                  <div className="col-lg-3 col-sm-4 col-xs-12"> 
                    <div className="form-group form-focus select-focus">
                      <select className="form-control" id="roleFilterID" value={this.state.roleFilterID}  onChange={this.handleChange('roleFilterID')} >
                        <option value="">-</option>
                        {this.state.roleListFilter.map(( listValue, index ) => {
                            return (
                              <option key={index} value={listValue.id}>{listValue.name}</option>
                            );
                        })}
                      </select>
                      <label className="focus-label">Role</label>
                    </div>
                  </div>

                  {/*<div className="col-sm-6 col-md-2"> 
                    <div className="form-group form-focus select-focus">
                      <select className="form-control floating" id="SortTypeId" value={this.state.SortType} onChange={this.handleChange('SortType')}> 
                        <option value="">-</option>
                        <option value="false">Ascending</option>
                        <option value="true">Descending</option>
                      </select>
                      <label className="focus-label">Sorting Order</label>
                    </div>
                  </div>*/}

                  

                  <div className="col-lg-3 col-sm-12 col-xs-12 margin-bottom">  
                    <a href="#" id="SearchBtn" className="btn btn-success btn-block" onClick={this.ChangePeriod}> Search </a>  
                  </div> 

                  {/*<div className="col-sm-6 col-md-2"> 
                    <div className="form-group form-focus select-focus">
                      <select className="form-control floating" value={this.state.pageSize}  onChange={this.handleChange('pageSize')}> 
                        <option value="5">5/Page</option>
                        <option value="10">10/Page</option>
                        <option value="50">50/Page</option>
                        <option value="100">100/Page</option>
                      </select>
                      <label className="focus-label">Per Page</label>
                    </div>
                  </div>*/}

                  {/*<div className="col-sm-6 col-md-3">  
                    <div className="form-group form-focus">
                      <input className="form-control floating" type="text" value={this.state.TempsearchText}  onChange={this.handleChange('TempsearchText')}/>
                      <label className="focus-label">Search</label>
                    </div>
                  </div>*/}

                </div>
                {/* /Search Filter */}    
                  {/* /Search Filter */}   
                  <div className="row">
                    <div className="col-md-12 d-flex">
                      <div className="card profile-box flex-fill">
                        <div className="card-body">
                          <div className="faq-card">
                            {this.state.isemployeesListFilter == true || this.state.isroleListFilter == true ?
                              this.location_wise_display_onegrid()  :   this.location_wise_display_grid()
                            }

                            <div className="row justify-content">
                              {this.state.locationWiseTimeSheeetViews.length > 0 && this.state.isPayrollAdminSession == true && this.state.is_btn_display == true ?
                                <button className="btn btn-success mr-1" data-toggle="modal" data-target="#submit_payroll">APPROVE TIME SHEETS</button>
                                : null
                              }

                              {this.state.locationWiseTimeSheeetViews.length > 0 && this.state.isPayrollAdminSession == false && this.state.is_btn_display == true ?
                                <button className="btn btn-success mr-1" data-toggle="modal" data-target="#submit_to_payroll">Submit to Payroll</button>
                                : null
                              }
                            </div>

                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                {/* //============ Current Time Staff Tab ============ */}

                
              </div>
            </div>
            {/* //Page Content */}
             
               {/* ------------------------- Modes ------------------------- */}

            {/* ****************** Current Time Staff Tab Modals ****************** */}
            {/* Current Time Staff Modal */}
            <div id="CurrentTimeStaff_Add_modal" data-backdrop="static" className="modal custom-modal" role="dialog">
              <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div className="modal-content">
                  <div className="modal-header">
                    <h5 className="modal-title">Time Sheet Entry</h5>
                    <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={this.ClearRecord()}>
                      <span aria-hidden="true">×</span>
                    </button>
                  </div>
                  <div className="modal-body">
                   
                      <div className="card">
                        <div className="card-body">
                          <div className="row">
                        {/*    <div className="col-md-4">
                              <div className="form-group">
                                <label>Date <span className="text-danger">*</span></label>
                                <input className="form-control" type="date" min={this.state.AddDate_Min} max={this.state.AddDate_Max} value={this.state.AddDate} onChange={this.handleChange('AddDate')} readonly/>
                                <span className="form-text error-font-color">{this.state.errormsg["AddDate"]}</span>
                              </div>
                            </div> */}

                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Service Name <span className="text-danger">*</span></label>
                                <select className="form-control" id="AddService" value={this.state.AddService} onChange={this.handleChange('AddService')}>
                                  <option value="">-</option>
                                  {this.state.serviceView.map(( listValue, index ) => {
                                    
                                    if((this.state.role_permission.isCordinator == true || this.state.role_permission.isSeniorCordinator) && listValue.serviceId == process.env.API_ON_CALL ){
                                      return (
                                        <option key={index} data-basserivce={listValue.isBasedOnSerivceBase} value={listValue.serviceId}>{listValue.serviceName}</option>
                                      );
                                    }else if(listValue.serviceId != process.env.API_ON_CALL){
                                      return (
                                        <option key={index} data-basserivce={listValue.isBasedOnSerivceBase} value={listValue.serviceId}>{listValue.serviceName}</option>
                                      );
                                    }

                                    {/*return (
                                      <option key={index} data-basserivce={listValue.isBasedOnSerivceBase} value={listValue.serviceId}>{listValue.serviceName}</option>
                                    );*/}
                                  })}
                                </select>
                                <span className="form-text error-font-color">{this.state.errormsg["AddService"]}</span>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Location <span className="text-danger">*</span></label>
                                <select className="form-control" id="Addlocation" value={this.state.Addlocation} onChange={this.handleChange('Addlocation')}>
                                  <option value="">-</option>
                                  {this.state.locationViews.map(( listValue, index ) => {
                                    return (
                                      <option key={index} value={listValue.locationGuid}>{listValue.locationName}</option>
                                    );
                                  })}
                                </select>
                                <span className="form-text error-font-color">{this.state.errormsg["Addlocation"]}</span>
                              </div>
                            </div>

                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Time in <span className="text-danger">*</span></label>
                                <div className="row">
                                  {/*<div className="col-md-6">
                                    <input className="form-control" type="datetime-local" min={this.state.AddTimeIn_Min} max={this.state.AddTimeIn_Max} value={this.state.AddTimeIn} onChange={this.handleChange('AddTimeIn')}  /> 
                                  </div> */ }
                                  <div className="col-md-12">
                                    <Datetime
                                      
                                      isValidDate={this.validationDateAddIn}
                                      inputProps={{readOnly: true}}
                                      closeOnTab={true}
                                      input={true}
                                      value={(this.state.AddTimeIn) ? this.state.AddTimeIn : ''}
                                      onChange={this.handleDateAddIn}
                                      dateFormat={process.env.DATE_FORMAT}
                                      timeFormat={process.env.TIME_FORMAT}
                                      timeConstraints={{
                                        hours: { min: 0, max: 23 },
                                        minutes: { min: 0, max: 59, step: 15 }
                                      }}
                                      renderInput={(props) => {
                                         return <input {...props} value={(this.state.AddTimeIn) ? props.value : ''} />
                                      }}
                                    />
                                  </div>
                                </div>
                                <span className="form-text error-font-color">{this.state.errormsg["AddTimeIn"]}</span>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Time out <span className="text-danger">*</span></label>
                                {/* <input className="form-control" type="datetime-local" step="900" id="AddTimeOut" min={this.state.AddTimeOut_Min} max={this.state.AddTimeOut_Max} value={this.state.AddTimeOut} onChange={this.handleChange('AddTimeOut')} /> */}
                                <div className="row">
                                  <div className="col-md-12">
                                      <Datetime
                                        inputProps={{readOnly: true,disabled: this.state.AddTimeOutDisabled}}
                                        className="readonly-cls AddTimeOut"
                                        isValidDate={this.validationDateAddOut}
                                        onClose={this.AddTimeOutonClose}
                                        closeOnTab={true}
                                        input={true}
                                        value={(this.state.AddTimeOut) ? this.state.AddTimeOut : ''} 
                                        defaultValue={this.state.AddTimeOut}
                                        onChange={this.handleDateAddOut}
                                        dateFormat={process.env.DATE_FORMAT}
                                        timeFormat={process.env.TIME_FORMAT}
                                        timeConstraints={{
                                          hours: { min: 0, max: 23 },
                                          minutes: { min: 0, max: 59, step: 15 }
                                        }}
                                        renderInput={(props) => {
                                             return <input {...props} value={(this.state.AddTimeOut) ? props.value : ''} />
                                         }}
                                      /> 
                                  </div>
                                </div>
                                <span className="form-text error-font-color">{this.state.errormsg["AddTimeOut"]}</span>
                              </div>
                            </div>
                           

                            
                            
                            <div className="col-md-6">
                              { this.state.AddService != process.env.API_ON_CALL ?
                                <div className="form-group">
                                  <label># of hrs</label>
                                  <input className="form-control" type="text"  value={this.state.AddNoOfHours} onChange={this.handleChange('AddNoOfHours')} disabled/>
                                  <span className="form-text error-font-color">{this.state.errormsg["AddNoOfHours"]}</span>
                                </div>
                                : null
                              }
                              
                            </div>

                            { this.state.AddPreference.length > 0 ?
                              <div className="col-md-6">
                                <div className="form-group">
                                  <label>Payout Preference <span className="text-danger">*</span></label>
                                  <select className="form-control" id="AddPreferenceService" value={this.state.AddPreferenceService} onChange={this.handleChange('AddPreferenceService')}>
                                    <option value="">-</option>
                                    {this.state.AddPreference.map(( listValue, index ) => {
                                      return (
                                        <option key={index} value={listValue.guidId}>{listValue.name}</option>
                                      );
                                    })}
                                  </select>
                                  <span className="form-text error-font-color">{this.state.errormsg["AddPreferenceService"]}</span>
                                </div>
                              </div>
                              : null
                            }
                            
                            <div className="col-md-12">
                              <div className="form-group">
                                <label>Notes</label>
                                <textarea className="form-control" type="text" value={this.state.AddNotes} onChange={this.handleChange('AddNotes')} ></textarea>
                                <span className="form-text error-font-color">{this.state.errormsg["AddNotes"]}</span>
                              </div>
                            </div>
                            
                          </div>
                          
                          
                          <div className="submit-section">
                            <button className="btn btn-primary submit-btn" onClick={this.AddRecord()}>Save</button>
                          </div>
                        </div>
                      </div>
                    
                  </div>
                </div>
              </div>
            </div>
            {/* //Current Time Staff Modal */}

            {/* Current Time Staff Modal */}
            <div id="CurrentTimeStaff_Edit_modal" data-backdrop="static" className="modal custom-modal" role="dialog">
              <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div className="modal-content">
                  <div className="modal-header">
                    <h5 className="modal-title">Time Sheet Entry</h5>
                    <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">×</span>
                    </button>
                  </div>
                  <div className="modal-body">
                   
                      <div className="card">
                        <div className="card-body">
                          <div className="row">
                            {/*
                              <div className="col-md-6">
                              <div className="form-group">
                                <label>Date</label>
                                <input className="form-control" type="text"  value={this.state.EditDate} onChange={this.handleChange('EditDate')} disabled/>
                              </div>
                            </div>
                            */}

                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Service Name <span className="text-danger">*</span></label>
                                <select className="form-control" id="EditService" value={this.state.EditService} onChange={this.handleChange('EditService')}>
                                  <option value="">-</option>
                                  {this.state.serviceView.map(( listValue, index ) => {
                                    if(this.state.EditService == process.env.API_ON_CALL || listValue.serviceId != process.env.API_ON_CALL){
                                      return (
                                      <option key={index} data-basserivce={listValue.isBasedOnSerivceBase} value={listValue.serviceId}>{listValue.serviceName}</option>
                                    );
                                    }
                                  })}
                                </select>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Location <span className="text-danger">*</span></label>
                                <select className="form-control" id="Editlocation" value={this.state.Editlocation} onChange={this.handleChange('Editlocation')}>
                                  <option value="">-</option>
                                  {this.state.locationViews.map(( listValue, index ) => {
                                    return (
                                      <option key={index} value={listValue.locationGuid}>{listValue.locationName}</option>
                                    );
                                  })}
                                </select>
                              </div>
                            </div>
                            
                            
                            
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Time in <span className="text-danger">*</span></label>
                                {/*<input className="form-control" type="time"  value={this.state.EditTimeIn} onChange={this.handleChange('EditTimeIn')} /> */}
                                <div className="row">
                                  <div className="col-md-12">
                                      <Datetime
                                        inputProps={{readOnly: true,disabled: this.state.EditTimeInDisabled}}
                                        isValidDate={this.validationDateEditIn}
                                        closeOnTab={true}
                                        input={true}
                                        value={(this.state.EditTimeIn) ? this.state.EditTimeIn : ''}
                                        onChange={this.handleDateEditIn}
                                        dateFormat={process.env.DATE_FORMAT}
                                        timeFormat={process.env.TIME_FORMAT}
                                        timeConstraints={{
                                          hours: { min: 0, max: 23 },
                                          minutes: { min: 0, max: 59, step: 15 }
                                        }}
                                        renderInput={(props) => {
                                             return <input {...props} value={(this.state.EditTimeIn) ? props.value : ''} />
                                        }}
                                      />
                                  </div>
                                </div>
                                <span className="form-text error-font-color">{this.state.errormsg["EditTimeIn"]}</span>
                              </div>
                            </div>
                            {/*<div className="col-md-6">
                              <div className="form-group">
                                <label>Time out <span className="text-danger">*</span></label>
                                <input className="form-control" id="EditTimeOut" type="datetime-local" min={this.state.EditTimeOut_Min} max={this.state.EditTimeOut_Max}   value={this.state.EditTimeOut} onChange={this.handleChange('EditTimeOut')} />
                                <span className="form-text error-font-color">{this.state.errormsg["EditTimeOut"]}</span>
                              </div>
                            </div> */}

                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Time out <span className="text-danger">*</span></label>
                                {/* <input className="form-control" type="datetime-local" step="900" id="AddTimeOut" min={this.state.AddTimeOut_Min} max={this.state.AddTimeOut_Max} value={this.state.AddTimeOut} onChange={this.handleChange('AddTimeOut')} /> */}
                                <div className="row">
                                  <div className="col-md-12">
                                      <Datetime
                                        inputProps={{readOnly: true,disabled: this.state.EditTimeOutDisabled}}
                                        isValidDate={this.validationDateEditOut}
                                        onClose={this.EditTimeOutonClose}
                                        closeOnTab={true}
                                        input={true}
                                        value={(this.state.EditTimeOut) ? this.state.EditTimeOut : ''}
                                        onChange={this.handleDateEditOut}
                                        dateFormat={process.env.DATE_FORMAT}
                                        timeFormat={process.env.TIME_FORMAT}
                                        timeConstraints={{
                                          hours: { min: 0, max: 23 },
                                          minutes: { min: 0, max: 59, step: 15 }
                                        }}
                                        renderInput={(props) => {
                                             return <input {...props} value={(this.state.EditTimeOut) ? props.value : ''} />
                                        }}
                                      />
                                  </div>
                                </div>
                                <span className="form-text error-font-color">{this.state.errormsg["EditTimeOut"]}</span>
                              </div>
                            </div>
                            


                            <div className="col-md-6">
                              { this.state.EditService != process.env.API_ON_CALL ?
                                <div className="form-group">
                                  <label>No of hours</label>
                                  <input className="form-control" type="text"  value={this.state.EditNoOfHours} onChange={this.handleChange('EditNoOfHours')} disabled/>
                                  <span className="form-text error-font-color">{this.state.errormsg["EditNoOfHours"]}</span>
                                </div>
                                : null
                              }
                            </div>

                            { this.state.EditPreference.length > 0 ?
                              <div className="col-md-6">
                                <div className="form-group">
                                  <label>Payout Preference <span className="text-danger">*</span></label>
                                  <select className="form-control" id="EditPreferenceService" value={this.state.EditPreferenceService} onChange={this.handleChange('EditPreferenceService')}>
                                    <option value="">-</option>
                                    {this.state.EditPreference.map(( listValue, index ) => {
                                      return (
                                        <option key={index} value={listValue.guidId}>{listValue.name}</option>
                                      );
                                    })}
                                  </select>
                                  <span className="form-text error-font-color">{this.state.errormsg["EditPreferenceService"]}</span>
                                </div>
                              </div>
                              : null
                            }
                            
                            <div className="col-md-12">
                              <div className="form-group">
                                <label>Notes</label>
                                <textarea className="form-control" type="text" value={this.state.EditNotes} onChange={this.handleChange('EditNotes')} ></textarea>
                              </div>
                            </div>
                            
                          </div>
                            
                          
  
                          <div className="submit-section">
                            <button className="btn btn-primary submit-btn" onClick={this.UpdateRecord()}>Save</button>
                          </div>
                        </div>
                      </div>
                    
                  </div>
                </div>
              </div>
            </div>

            
            {/* //Current Time Staff Modal */}

            
            {/* ****************** Current Time Staff Tab Modals ****************** */}


            
            {/* Current Time Staff Modal */}
            <div id="past_details_modal" data-backdrop="static" className="modal custom-modal fade" role="dialog">
              <div className="modal-dialog modal-dialog-centered modal-xl" role="document">
                <div className="modal-content">
                  <div className="modal-header">
                   
                    <h3 className="card-title">{this.state.tempfullName} Time Sheet Entries</h3>
                    
                    
                    <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">×</span>
                    </button>
                  </div>
                  <div className="modal-body modal-body-past-details">
                   
                      <div className="card">
                        <div className="card-body">
                          <div className="row">
                            <div className="col-lg-6">
                                <div className="dash-info-list pk-sheet-margin-bottom">
                                  <a href="#" className="dash-card text-danger">
                                    <div className="dash-card-container">
                                      <div className="dash-card-icon">
                                        <i className="fa fa-calendar" />
                                      </div>
                                      <div className="dash-card-content">
                                        <p>Amendment Due Date</p>
                                      </div>
                                      <div className="dash-card-avatars">
                                        {this.state.past_display_amendment_due_date}
                                      </div>
                                    </div>
                                  </a>
                                </div>
                            </div>
                          </div>
                          <div className="row">
                            <div className="col-lg-12">
                              {( this.state.IsEditBtn == true ||this.state.isTimeSheetApproved == false || (this.state.isTimeSheetSubmittedToPayroll == false && this.state.isPayrollAdmin == true)) ?
                                <h3 className="card-title"><a href="#" className="edit-icon" data-toggle="modal" data-target="#CurrentTimeStaff_Add_modal"><i className="fa fa-plus" /></a></h3>
                              : null }
                             
                              <div className="table-responsive">
                                <table className="table table-nowrap">
                                  <thead>
                                   <tr>
                                      <th>Date</th>
                                      <th>Service Name</th>
                                      <th>Time In</th>
                                      <th>Time Out</th>
                                      <th>Location</th>
                                      <th>Note</th>
                                      <th>No of hours</th>
                                      <th>Record Type</th>
                                      <th>CreatedOn</th>
                                      {(this.state.isTimeSheetApproved == false || (this.state.isTimeSheetSubmittedToPayroll == false && this.state.isPayrollAdmin == true)) ?
                                        <th>Action</th>
                                      : null }
                                      
                                    </tr>
                                  </thead>
                                  <tbody>
                                    {this.state.ListGridPastDetails.map(( listValue, index ) => {
                                        if(listValue.isAmendmends == true && listValue.wantToDisplayInTimeSheet == true){
                                          var action_btn = '';
                                          var cls_nm = 'pk-strikethrough';
                                          

                                          return (
                                            <tr key={index}>
                                              <td className={cls_nm}>{moment(listValue.timeSheetDate,process.env.API_DATE_FORMAT).format(process.env.DATE_FORMAT)}</td>
                                              <td className={cls_nm}>{listValue.serviceName}</td>
                                              <td className={cls_nm}>{moment(listValue.timeIN).format(process.env.DATETIME_FORMAT)}</td>
                                              <td className={cls_nm}>{moment(listValue.timeOUT).format(process.env.DATETIME_FORMAT)}</td>
                                              <td className={cls_nm}>{listValue.locationName}</td>
                                              <td className={cls_nm}>{listValue.note}</td>
                                              <td className={cls_nm}>{listValue.noOfHours}</td>
                                              <td className={cls_nm}>{listValue.recordType}</td>
                                              <td className={cls_nm}>{SystemHelpers.TimeZone_DateTime(listValue.createdOn)}</td>
                                              <td></td>
                                            </tr>
                                          );
                                        }else{
                                          var action_btn = '';

                                          if(this.state.IsEditBtn == true || this.state.isTimeSheetApproved == false || (this.state.isTimeSheetSubmittedToPayroll == false && this.state.isPayrollAdmin == true)){
                                            action_btn = this.Edit_Update_Btn_Func(listValue);
                                          }

                                          if(listValue.wantToDisplayInTimeSheet == true){
                                            return (
                                              <tr key={index}>
                                                <td>{moment(listValue.timeSheetDate,process.env.API_DATE_FORMAT).format(process.env.DATE_FORMAT)}</td>
                                                <td>{listValue.serviceName}</td>
                                                <td>{moment(listValue.timeIN).format(process.env.DATETIME_FORMAT)}</td>
                                                <td>{moment(listValue.timeOUT).format(process.env.DATETIME_FORMAT)}</td>
                                                <td>{listValue.locationName}</td>
                                                <td>{listValue.note}</td>
                                                <td>{listValue.noOfHours}</td>
                                                <td>{listValue.recordType}</td>
                                                <td>{SystemHelpers.TimeZone_DateTime(listValue.createdOn)}</td>
                                                <td>{action_btn}</td>
                                              </tr>
                                            );
                                          }
                                        }
                                        
                                    })}
                                    <tr><td className="pk-td-border" colSpan="6"> Total hours </td>
                                        <td> {this.state.EditworkedHour}</td>
                                        <td></td>
                                        <td></td>
                                        {(this.state.isPayrollAdminSession == true && this.state.isAmendmendsSubmitted == false ) ||( this.state.isAmendmendsSubmitted == false && this.state.isTimeSheetSubmittedToPayroll == false && (this.state.isCordinatorSession == true  || this.state.isSeniorCordinatorSession == true || this.state.isServiceCoordinatorLeadSession == true)) ?
                                          <td></td>
                                        : null }
                                        
                                    </tr>
                                  </tbody>
                                </table>
                                {/*{  this.state.ListGridPastDetails.length > 0 &&   this.state.isAmendmendsSubmitted == false && this.state.IsAmendmendsEnabled == true ?
                                  <div className="row justify-content">
                                    <button className="btn btn-success mr-1"  data-toggle="modal" data-target="#submit_amendmend_from_attSummary">Submit for amendment</button>
                                  </div>
                                  : null
                                }*/}

                                {/*{  this.state.ListGridPastDetails.length > 0 && this.state.isTimeSheetSubmittedToPayroll == false && this.state.isTimeSheetSubmitedToPayroll == false && this.state.isAmendmendsSubmitted == false && this.state.isTimeSheetApproved == true && (this.state.isCordinatorSession == true  || this.state.isSeniorCordinatorSession == true || this.state.isServiceCoordinatorLeadSession == true) ?
                                  <div className="row justify-content">
                                    <button className="btn btn-success mr-1"  data-toggle="modal" data-target="#submit_amendmend_from_attSummary">Submit for amendment</button>
                                  </div>
                                  : null
                                }

                                { this.state.ListGridPastDetails.length > 0 && this.state.isTimeSheetSubmittedToPayroll == true && this.state.isPayrollAdminSession == true && this.state.isTimeSheetSubmitedToPayroll == false ?
                                  <div className="row justify-content">
                                    <button className="btn btn-success mr-1"  data-toggle="modal" data-target="#submit_amendmend_from_attSummary">Submit for amendment</button>
                                  </div>
                                  : null
                                }*/}
                              </div>
                            </div>
                          </div>

                          <br/>
                          <br/>

                          <div className="row">
                            
                            <div className="col-md-12 col-lg-12 col-xl-12 d-flex">
                              <div className="card flex-fill dash-statistics">
                                <div className="card-body">
                                  <h5 className="card-title">Summary of Entries</h5>
                                  
                                  <div className="row filter-row"> 

                                    <div className="col-lg-3 col-sm-12 col-xs-12">
                                      <div className="title">Total Regular Hours: {this.state.DisplaytotalRegularHours}</div>
                                      <div className="title">Total Asleep Hours: {this.state.DisplaytotalAsleepHours}</div>
                                      <div className="title"></div>
                                    </div>
                                    <div className="col-lg-3 col-sm-12 col-xs-12"> 
                                      <div className="title">Total Sick Hours: {this.state.DisplaytotalSickHours}</div>
                                      <div className="title">Total TRIP Hours: {this.state.DisplaytotalTripHours}</div>
                                      <div className="title"></div>
                                    </div>
                                    <div className="col-lg-3 col-sm-12 col-xs-12"> 
                                      <div className="title">Total OT Hours: {this.state.DisplaytotalOTHours}</div>
                                      <div className="title">Total Brv Hours: {this.state.DisplaytotalBRVHours}</div>
                                      <div className="title"></div>
                                    </div>
                                    <div className="col-lg-3 col-sm-12 col-xs-12"> 
                                      <div className="title">Total Leave Hours: {this.state.DisplaytotalLeaveHours}</div>
                                      <div className="title">Total On-call: {this.state.DisplaytotalOnCallHours}</div>
                                      <div className="title">Total STAT Hours: {this.state.DisplaytotalStatHours}</div>
                                    </div>
                                  </div>
                                      
                                </div>
                              </div>
                            </div>
                            
                            

                          </div>

                          <br/>
                          <br/>


                          { this.state.ListGridPastDetailsCount > 0  && this.state.EdittimeSheetContactMasterId != ''  ?
                          <div>
                            <div className="row">
                               <div className="col-lg-12">
                               <h3 class="card-title">Notes</h3>
                                {/* Chat Main Row */}
                                <div className="chat-main-row">
                                  {/* Chat Main Wrapper */}
                                  <div className="chat-main-wrapper">
                                    {/* Chats View */}
                                    <div className="col-lg-9 message-view task-view">
                                      <div className="chat-window">
                                        
                                        <div className="chat-contents">
                                          <div className="chat-content-wrap">
                                            <div className="chat-wrap-inner">
                                              <div className="chat-box">
                                                {this.GetChatList()}
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                        <div className="chat-footer chat-footer-pk">
                                          <div className="message-bar">
                                            <div className="message-inner">
                                              {/*<a className="link attach-icon" href="#" data-toggle="modal" data-target="#drag_files"><img src={Attachment} alt="" /></a>*/}
                                              <div className="message-area">
                                                <div className="input-group">
                                                  <textarea className="form-control" placeholder="Type Notes..." value={this.state.AddMsg} onChange={this.handleChange('AddMsg')} />
                                                  {/*<span className="input-group-append">
                                                    <button className="btn btn-custom send-btn-pk" type="button" onClick={this.AddNotes_Msg()}  ><i className="fa fa-send" /></button>
                                                  </span>*/}
                                                </div>
                                                <span className="form-text error-font-color">{this.state.errormsg["AddMsg"]}</span>
                                              </div>
                                            </div>
                                          </div>
                                          <div className="project-members">
                                            <div className="col-md-12 notes-padding-left">
                                              <div className="form-group">
                                                <div className="checkbox">
                                                  <label>
                                                    <input type="checkbox" name="allow_emp" value="true" onChange={this.handleChange('allow_emp')}  checked={this.state.allow_emp == true ? "true" : null} /> Allow the employees to see the notes
                                                  </label>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                        </div>

                                        




                                      </div>
                                    </div>
                                    {/* /Chats View */}

                                    
                                  </div>
                                  {/* /Chat Main Wrapper */}
                                </div>
                                {/* /Chat Main Row */}
                                
                               </div>
                            </div>
                            <div className="row">
                              <div className="col-md-12 notes-padding-left">
                                <div className="form-group">
                                  <label>Attachment</label>
                                  <FileUploadPreview
                                    ResetFileMethod={this.state.AddNotesResetflag}
                                    className="form-control" 
                                    setPropState={this.setPropState} />
                                    <input type="hidden" name="filePreviewsFinalEditActive" id="filePreviewsFinalEditActive" value={this.state.filePreviewsFinalEditActive}/>
                                  <span className="form-text success-font-color Guidelines_Doc">{process.env.ATTACHMENT_GUIDELINES}</span>
                                  <span className="form-text error-font-color">{this.state.errormsg["Addattachment"]}</span>
                                </div>
                              </div>
                            </div>

                    
                            {
                                this.state.filePreviewsFinal.length > 0 || this.state.filePreviewsFinalEditActive > 0 ?

                                <div>
                                    <h4>Preview Files</h4>
                                    <br/>
                                    <div className="row row-sm">

                                        {this.state.filePreviewsFinalEdit.map(( listValue2, index ) => {
                                          return (
                                            this.GetImageAllNewImg(listValue2)
                                          );
                                        })}

                                        {this.state.filePreviewsFinal.map(( listValue, index ) => {
                                          return (
                                            this.GetImageAllNew(listValue.FileData)
                                          );
                                        })}
                                        
                                    </div> 
                                </div>: undefined
                            }  
                          </div>
                          : null }
                          
                          {this.state.GetChatListLoad == true ?
                            <div className="submit-section">
                              <button className="btn btn-primary submit-btn" onClick={this.AddNotes_Msg()}>Save</button>
                            </div>
                            : null
                          }
                          
                        </div>
                      </div>   
                  </div>
                </div>
              </div>
            </div>
            {/* //Current Time Staff Modal */}

            {/* Delete timesheet  Modal */}
            <div id="timesheet_Delete_modal" data-backdrop="static" className="modal custom-modal"  role="dialog">
              <div className="modal-dialog modal-dialog-centered">
                <div className="modal-content">
                  <div className="modal-body">
                    <div className="form-header">
                      <h3>TimeSheet</h3>
                      <p>Are you sure want to delete selected record?</p>
                    </div>
                    <div className="modal-btn delete-action">
                      <div className="row">
                        <div className="col-6">
                          <a  onClick={this.DeleteRecord()} className="btn btn-primary continue-btn">Delete</a>
                        </div>
                        <div className="col-6">
                          <a href="" data-dismiss="modal" className="btn btn-primary cancel-btn">Cancel</a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          {/* /Delete Human Resources Modal */}

          {/* submit For Approval timesheet  Modal */}
            <div className="modal custom-modal fade" id="submit_to_payroll" role="dialog">
              <div className="modal-dialog modal-dialog-centered">
                <div className="modal-content">
                  <div className="modal-body">
                    <div className="form-header">
                      <h3>Attendance Summary</h3>
                      <p>Are you sure you want to submit Time Sheet for approval, after submission Time sheet would be read only</p>
                    </div>
                    <div className="modal-btn delete-action">
                      <div className="row">
                        <div className="col-6">
                          <a  onClick={this.SubmitUserTimeSheet()} className="btn btn-primary continue-btn">Yes</a>
                        </div>
                        <div className="col-6">
                          <a href="" data-dismiss="modal" className="btn btn-primary cancel-btn">No</a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          {/* submit For Approval timesheet  Modal */}

          {/* submit For Submit Amendmend From AttSummary  Modal */}
            <div className="modal custom-modal" id="submit_amendmend_from_attSummary" role="dialog">
              <div className="modal-dialog modal-dialog-centered">
                <div className="modal-content">
                  <div className="modal-body">
                    <div className="form-header">
                      <h3>Attendance Summary</h3>
                      <p>Are you sure you want to submit Time Sheet for amendment approval, after submission Time sheet would be read only</p>
                    </div>
                    <div className="modal-btn delete-action">
                      <div className="row">
                        <div className="col-6">
                          <a  onClick={this.SubmitAmendmendFromAttSummary()} className="btn btn-primary continue-btn">Yes</a>
                        </div>
                        <div className="col-6">
                          <a href="" data-dismiss="modal" className="btn btn-primary cancel-btn">No</a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          {/* submit For Submit Amendmend From AttSummary  Modal */}


          {/* submit For Approval timesheet  Modal */}
            <div className="modal custom-modal fade" id="submit_payroll" role="dialog">
              <div className="modal-dialog modal-dialog-centered">
                <div className="modal-content">
                  <div className="modal-body">
                    <div className="form-header">
                      <h3>Attendance Summary</h3>
                      <p>Are you sure you want to submit Time Sheet for approval, after submission Time sheet would be read only</p>
                    </div>
                    <div className="modal-btn delete-action">
                      <div className="row">
                        <div className="col-6">
                          <a  onClick={this.SubmitPayroll()} className="btn btn-primary continue-btn">Yes</a>
                        </div>
                        <div className="col-6">
                          <a href="" data-dismiss="modal" className="btn btn-primary cancel-btn">No</a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          {/* submit For Approval timesheet  Modal */}

          
          
          
            {/* ------------------------- Modes ------------------------- */}

            
            </div>
          <SidebarContent/>
      </div>
    );
  }
}

export default Timesheet;
