
import React from 'react';
import { useHistory } from "react-router-dom";

import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import CryptoAES from 'crypto-js/aes';
import CryptoENC from 'crypto-js/enc-utf8';

import MomentTimezone from 'moment-timezone';
import moment from 'moment';

const Systemhelpers = {
    SessionOut: function(){
    	localStorage.removeItem("token");
	    localStorage.removeItem("contactId");
	    localStorage.removeItem("eMailAddress");
	    localStorage.removeItem("fullName");
	    localStorage.removeItem("userRole");
	    localStorage.removeItem("sessiontoken");
      localStorage.removeItem("primaryLocationId");
      localStorage.removeItem("timeSheetPeriodViews");
      localStorage.removeItem("CurrenttimeSheetPeriodId");
      localStorage.removeItem("isPayrollAdmin");
      localStorage.removeItem("usertypesession");
      localStorage.removeItem("userGender");

      //localStorage.removeItem("usertypesession");
      //localStorage.removeItem("primaryLocationId");
      //localStorage.removeItem("isPayrollAdmin");
      localStorage.removeItem("primaryLocationGuid");

      localStorage.removeItem("Headerprofileimage");
      localStorage.removeItem("headerisProfileImageShow");
      localStorage.removeItem("TimeSheetApproverName");
      localStorage.removeItem("isProfileImageShow");
      localStorage.removeItem("ListGridNotificationsCount");

      localStorage.removeItem("employmentHoursName");
      localStorage.removeItem("employmentStatusName");
      
      //localStorage.removeItem("Headerprofileimage");
      localStorage.removeItem("headerisProfileImageShow");
      localStorage.removeItem("primaryLocationName");

      localStorage.removeItem("LocalStorageCurrentYearGuid");
      localStorage.removeItem("LocalStorageCurrentYearName");
      localStorage.removeItem("preferredName");
    },
    ToastError: function(msg){
        if(msg != 'Authorization has been denied for this request.'){
            toast.error(msg, {
                position: "top-right",
                autoClose: process.env.API_TOAST_TIME,
                hideProgressBar: false,
                closeOnClick: false,
                pauseOnHover: false,
                draggable: false
            });
        }
    },
    ToastSuccess: function(msg){
    	toast.success(msg, {
	        position: "top-right",
	        autoClose: process.env.API_TOAST_TIME,
	        hideProgressBar: false,
	        closeOnClick: false,
	        pauseOnHover: false,
	        draggable: false
	    });
    },
    ToastWarning: function(msg){
        toast.warn(msg, {
            position: "top-right",
            autoClose: process.env.API_TOAST_TIME,
            hideProgressBar: false,
            closeOnClick: false,
            pauseOnHover: false,
            draggable: false
        });
    },
    GetRole: function(){

    /* Role Management */
    var pwd = localStorage.getItem("contactId")+"Phss@123";
    var Role_session = localStorage.getItem('sessiontoken');

    var _ciphertext = CryptoAES.decrypt(Role_session, pwd);
    //console.log('Role Store all');
    //console.log(_ciphertext.toString(CryptoENC));
    var JsonCreate = JSON.parse(_ciphertext.toString(CryptoENC));
    return JsonCreate;
    //this.setState({ role_address_can: JsonCreate.role_type });
    
    //console.log(JsonCreate.address_can);
    /* Role Management */
    },

    FetchRole: function(contactId,token){
      var url=process.env.API_API_URL+'GetUserRoleDetails?contactId='+contactId;
      fetch(url, {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'bearer '+token
        },
        //body: JSON.stringify(bodyarray)
      }).then((response) => response.json())
      .then(data => {
          console.log("responseJson GetUserRoleDetails helper");
          console.log(data);
          //console.log(data.data.userRole);
           //debugger;
           //alert(data.responseType);
          if (data.responseType == 1) {
              // Profile & Contact
              var row = data.data;
              this.FetchRoleJson(row,contactId);
              
          }else{
                if(data.message == 'Authorization has been denied for this request.'){
                  SystemHelpers.SessionOut();
                  //this.props.history.push("/login");
                }else{
                  SystemHelpers.ToastError(data.message);
                }
                
          }
          this.hideLoader();
          
      })
      .catch(error => {
        //this.props.history.push("/error-500");
      });
    },

    FetchRoleJson: function(RoleJson,contactId){

      console.log('FetchRoleJson Helper');
      console.log(RoleJson);
      
      // Profile & Contact
      var row = RoleJson;
      var row_count = row.length;
      //console.log("row array");
      var i = 1;
      var RoleArray = {};

      /* >>>>>>>>>>>>>>>>>>>>>>>> Staff Role permission <<<<<<<<<<<<<<<<<<<<<<<<< */
        /* --------------------- Contact_information --------------------- */
        var staff_contact_information_can_create = false;
        var staff_contact_information_can_update = false;
        var staff_contact_information_can_view = false;
        var staff_contact_information_can_delete = false;
        /* --------------------- Contact_information --------------------- */

        /* --------------------- Address --------------------- */
        var staff_address_can_create = false;
        var staff_address_can_update = false;
        var staff_address_can_view = false;
        var staff_address_can_delete = false;
        /* --------------------- Address --------------------- */

        /* --------------------- Availability --------------------- */
        var staff_availability_can_create = false;
        var staff_availability_can_update = false;
        var staff_availability_can_view = false;
        var staff_availability_can_delete = false;
        /* --------------------- Availability --------------------- */

        /* --------------------- Emergency_Contact --------------------- */ 
        var staff_emergency_contact_can_create = false;
        var staff_emergency_contact_can_update = false;
        var staff_emergency_contact_can_view = false;
        var staff_emergency_contact_can_delete = false;
        /* --------------------- Emergency_Contact --------------------- */

        /* --------------------- Volunteer --------------------- */
        var staff_volunteer_can_create = false;
        var staff_volunteer_can_update = false;
        var staff_volunteer_can_view = false;
        var staff_volunteer_can_delete = false;
        /* --------------------- Volunteer --------------------- */ 

        /* --------------------- Employment_History --------------------- */
        var staff_employment_history_can_create = false;
        var staff_employment_history_can_update = false;
        var staff_employment_history_can_view = false;
        var staff_employment_history_can_delete = false;
        /* --------------------- Employment_History --------------------- */

        /* --------------------- Skills --------------------- */  
        var staff_skills_can_create = false;
        var staff_skills_can_update = false;
        var staff_skills_can_view = false;
        var staff_skills_can_delete = false;
        /* --------------------- Skills --------------------- */

        /* --------------------- Course_Training --------------------- */ 
        var staff_course_training_can_create = false;
        var staff_course_training_can_update = false;
        var staff_course_training_can_view = false;
        var staff_course_training_can_delete = false;
        /* --------------------- Course_Training --------------------- */

        /* --------------------- Awards_Recognitions --------------------- */
        var staff_awards_recognitions_can_create = false;
        var staff_awards_recognitions_can_update = false;
        var staff_awards_recognitions_can_view = false;
        var staff_awards_recognitions_can_delete = false;
        /* --------------------- Awards_Recognitions --------------------- */

        /* --------------------- Consents_Waivers --------------------- */
        var staff_consents_waivers_can_create = false;
        var staff_consents_waivers_can_update = false;
        var staff_consents_waivers_can_view = false;
        var staff_consents_waivers_can_delete = false;
        /* --------------------- Consents_Waivers --------------------- */

        /* ---------- Location Department profile tab ---------- */
        var staff_locations_departments_can_create = false;
        var staff_locations_departments_can_update = false;
        var staff_locations_departments_can_view = false;
        var staff_locations_departments_can_delete = false; 
        /* ---------- Location Department profile tab ---------- */

        /* --------------------- HumanResource --------------------- */
        var staff_human_resource_can_create = false;
        var staff_human_resource_can_update = false;
        var staff_human_resource_can_view = false;
        var staff_human_resource_can_delete = false;
        /* --------------------- HumanResource --------------------- */

        /* --------------------- PersonalInfo --------------------- */
        var staff_personal_info_can_create = false;
        var staff_personal_info_can_update = false;
        var staff_personal_info_can_view = false;
        var staff_personal_info_can_delete = false;
        /* --------------------- PersonalInfo --------------------- */

        /* ------------------------- ProfileInfo ------------------------- */
        var staff_profile_info_can_create = false;
        var staff_profile_info_can_update = false;
        var staff_profile_info_can_view = false;
        var staff_profile_info_can_delete = false;
        /* ------------------------- ProfileInfo ------------------------- */

        /* --------------------- Documents --------------------- */
        var staff_documents_can_create = false;
        var staff_documents_can_update = false;
        var staff_documents_can_view = false;
        var staff_documents_can_delete = false;
        /* --------------------- Documents --------------------- */

        /* --------------------- Phss_EmpInfo --------------------- */
        var staff_phss_empinfo_can_create = false;
        var staff_phss_empinfo_can_update = false;
        var staff_phss_empinfo_can_view = false;
        var staff_phss_empinfo_can_delete = false;
        /* --------------------- Phss_EmpInfo --------------------- */

        /* --------------------- Entitlement --------------------- */
        var staff_entitlement_can_view = false;
        var staff_entitlement_can_create = false;
        /* --------------------- Entitlement --------------------- */
      /* >>>>>>>>>>>>>>>>>>>>>>>> Staff Role permission <<<<<<<<<<<<<<<<<<<<<<<<< */

      /* >>>>>>>>>>>>>>>>>>>>>>>> Profile Role permission <<<<<<<<<<<<<<<<<<<<<<<<< */
        /* ------------------------- ProfileInfo ------------------------- */
        var profile_info_can_create = false;
        var profile_info_can_update = false;
        var profile_info_can_view = false;
        var profile_info_can_delete = false;
        /* ------------------------- ProfileInfo ------------------------- */

        /* --------------------- Contact_information --------------------- */
        var contact_information_can_create = false;
        var contact_information_can_update = false;
        var contact_information_can_view = false;
        var contact_information_can_delete = false;
        /* --------------------- Contact_information --------------------- */

        /* --------------------- PersonalInfo --------------------- */
        var personal_info_can_create = false;
        var personal_info_can_update = false;
        var personal_info_can_view = false;
        var personal_info_can_delete = false;
        /* --------------------- PersonalInfo --------------------- */

        /* --------------------- Phss_EmpInfo --------------------- */
        var phss_empinfo_can_create = false;
        var phss_empinfo_can_update = false;
        var phss_empinfo_can_view = false;
        var phss_empinfo_can_delete = false;
        /* --------------------- Phss_EmpInfo --------------------- */

        /* --------------------- Scheduler --------------------- */
        var scheduler_can_create = false;
        var scheduler_can_update = false;
        var scheduler_can_view = false;
        var scheduler_can_delete = false;
        var scheduler_can_assign = false;
        var scheduler_can_duplicate = false;
        /* --------------------- Scheduler --------------------- */

        /* --------------------- Address --------------------- */
        var address_can_create = false;
        var address_can_update = false;
        var address_can_view = false;
        var address_can_delete = false;
        /* --------------------- Address --------------------- */

        /* --------------------- Availability --------------------- */
        var availability_can_create = false;
        var availability_can_update = false;
        var availability_can_view = false;
        var availability_can_delete = false;
        /* --------------------- Availability --------------------- */
        
        /* --------------------- Emergency_Contact --------------------- */ 
        var emergency_contact_can_create = false;
        var emergency_contact_can_update = false;
        var emergency_contact_can_view = false;
        var emergency_contact_can_delete = false;
        /* --------------------- Emergency_Contact --------------------- */  
                
        /* --------------------- Employment_History --------------------- */
        var employment_history_can_create = false;
        var employment_history_can_update = false;
        var employment_history_can_view = false;
        var employment_history_can_delete = false;
        /* --------------------- Employment_History --------------------- */

        /* --------------------- Course_Training --------------------- */ 
        var course_training_can_create = false;
        var course_training_can_update = false;
        var course_training_can_view = false;
        var course_training_can_delete = false;
        /* --------------------- Course_Training --------------------- */

        /* --------------------- Skills --------------------- */  
        var skills_can_create = false;
        var skills_can_update = false;
        var skills_can_view = false;
        var skills_can_delete = false;
        /* --------------------- Skills --------------------- */

        /* --------------------- Consents_Waivers --------------------- */
        var consents_waivers_can_create = false;
        var consents_waivers_can_update = false;
        var consents_waivers_can_view = false;
        var consents_waivers_can_delete = false;
        /* --------------------- Consents_Waivers --------------------- */

        /* --------------------- Awards_Recognitions --------------------- */
        var awards_recognitions_can_create = false;
        var awards_recognitions_can_update = false;
        var awards_recognitions_can_view = false;
        var awards_recognitions_can_delete = false;
        /* --------------------- Awards_Recognitions --------------------- */   

        /* --------------------- HumanResource --------------------- */
        var human_resource_can_create = false;
        var human_resource_can_update = false;
        var human_resource_can_view = false;
        var human_resource_can_delete = false;
        /* --------------------- HumanResource --------------------- */

        /* --------------------- Documents --------------------- */
        var documents_can_create = false;
        var documents_can_update = false;
        var documents_can_view = false;
        var documents_can_delete = false;
        /* --------------------- Documents --------------------- */  
          
        /* --------------------- Timesheet --------------------- */
        var timesheet_can_create = false;
        var timesheet_can_update = false;
        var timesheet_can_view = false;
        var timesheet_can_delete = false;
        var timesheet_can_export = false;
        var timesheet_can_summary = false;
        var timesheet_can_approve = false;
        /* --------------------- Timesheet --------------------- */

        /* --------------------- User --------------------- */
        var user_can_create = false;
        var user_can_update = false;
        var user_can_view = false;
        var user_can_delete = false;
        /* --------------------- User --------------------- */

        /* --------------------- ContactList --------------------- */
        var contactlist_can_create = false;
        var contactlist_can_update = false;
        var contactlist_can_view = false;
        var contactlist_can_delete = false;
        var contactlist_can_viewall = false;
        /* --------------------- ContactList --------------------- */

        /* --------------------- Locations --------------------- */
        var locations_can_create = false;
        var locations_can_update = false;
        var locations_can_view = false;
        var locations_can_delete = false;
        var locations_can_viewall = false;
        /* --------------------- Locations --------------------- */

        /* --------------------- Employees --------------------- */
        var employees_can_create = false;
        var employees_can_update = false;
        var employees_can_view = false;
        var employees_can_delete = false;
        var employees_can_viewall = false;
        /* --------------------- Employees --------------------- */ 

        /* --------------------- Holidays --------------------- */
        var holidays_can_create = false;
        var holidays_can_update = false;
        var holidays_can_view = false;
        var holidays_can_delete = false;
        var holidays_can_viewall = false;
        /* --------------------- Holidays --------------------- */ 

        /* ---------- Location Department profile tab ---------- */
        var locations_departments_can_create = false;
        var locations_departments_can_update = false;
        var locations_departments_can_view = false;
        var locations_departments_can_delete = false; 
        /* ---------- Location Department profile tab ---------- */ 

        /* --------------------- Volunteer --------------------- */
        var volunteer_can_create = false;
        var volunteer_can_update = false;
        var volunteer_can_view = false;
        var volunteer_can_delete = false;
        /* --------------------- Volunteer --------------------- */ 

        /* --------------------- Entitlement --------------------- */
        var entitlement_can_view = false;
        var entitlement_can_create = false;
        /* --------------------- Entitlement --------------------- */
      /* >>>>>>>>>>>>>>>>>>>>>>>> Profile Role permission <<<<<<<<<<<<<<<<<<<<<<<<< */
      
      /* >>>>>>>>>>>>>>>>>>>>>>>> Hierarchy Role permission <<<<<<<<<<<<<<<<<<<<<<<<< */
        /* --------------------- Hierarchy --------------------- */
        var hierarchy_can_id = "";
        var hierarchy_can_type = "";
        /* --------------------- Hierarchy --------------------- */
      /* >>>>>>>>>>>>>>>>>>>>>>>> Hierarchy Role permission <<<<<<<<<<<<<<<<<<<<<<<<< */


      /* >>>>>>>>>>>>>>>>>>>>>>>> Side Menu Role permission <<<<<<<<<<<<<<<<<<<<<<<<< */
        /* --------------------- Report --------------------- */
        var report_can_view = false;
        /* --------------------- Report --------------------- */

        /* --------------------- Entitlement Side Menu --------------------- */
        var entitlement_menu_can_view = false;
        var entitlement_menu_can_create = false;
        /* --------------------- Entitlement Side Menu --------------------- */

        /* --------------------- Payroll Adjustment --------------------- */
        var payroll_adjustment_menu_can_view = false;
        /* --------------------- Payroll Adjustment --------------------- */

        /* --------------------- AuditReport --------------------- */
        var audit_report_menu_can_view = false;
        /* --------------------- AuditReport --------------------- */

        /* --------------------- BatchReport --------------------- */
        var batch_report_menu_can_view = false;
        /* --------------------- BatchReport --------------------- */
      /* >>>>>>>>>>>>>>>>>>>>>>>> Side Menu Role permission <<<<<<<<<<<<<<<<<<<<<<<<< */

      /* --------------------- Senior Coordinator --------------------- */
      var is_coordinator = false;
      /* --------------------- Senior Coordinator --------------------- */

      for (var zz = 0; zz < row_count; zz++) 
      {
          var TempRoleArray = {};
          console.log(i+"JsonArrayfunc");

          /* --------------------- check Senior Coordinator --------------------- */
          var check_senior_coordinator = row[zz].roleId;
          
          if(check_senior_coordinator == 'c0785240-3591-eb11-b1ac-000d3a3e040b')
          {
            is_coordinator = true;
          }
          

           
          TempRoleArray.check_coordinator_can = is_coordinator;
          /* --------------------- check Senior Coordinator --------------------- */
          
          /* >>>>>>>>>>>>>>>>>>>>>>>> Staff Role permission <<<<<<<<<<<<<<<<<<<<<<<<< */
            // Staff Role Permission
            var StaffJsonArray = row[zz].staffJson
            var StaffJsonCreate = JSON.parse(StaffJsonArray);
            var StaffRoleJson_profile_management = StaffJsonCreate.staff_profile_management;
            // Staff Role Permission
            console.log('StaffRoleJson_profile_management');
            console.log(StaffRoleJson_profile_management);
            /* --------------------- Staff Contact_information --------------------- */

            
              

              if(StaffRoleJson_profile_management.Contact_information !== undefined){
                
                var Staff_Contact_information=StaffRoleJson_profile_management.Contact_information;
                if(staff_contact_information_can_update == false && Staff_Contact_information.canUpdate == true){
                    staff_contact_information_can_update = true;
                }
                if(staff_contact_information_can_view == false && Staff_Contact_information.canView == true){
                    staff_contact_information_can_view = true;
                }
              }else{
                console.log('Role Status => StaffRoleJson_profile_management.Contact_information not found.');    
              }
              
              var staff_temp_contact_information_can = {};

              staff_temp_contact_information_can.contact_information_can_update = staff_contact_information_can_update;
              staff_temp_contact_information_can.contact_information_can_view = staff_contact_information_can_view;
              
              TempRoleArray.staff_contact_information_can = staff_temp_contact_information_can;
            /* --------------------- Staff Contact_information --------------------- */

            /* --------------------- Staff Address --------------------- */

            if(StaffRoleJson_profile_management.Address !== undefined){
                var staff_Address =StaffRoleJson_profile_management.Address;

                if(staff_address_can_create == false && staff_Address.canCreate == true){
                    staff_address_can_create = true;
                }
                
                if(staff_address_can_update == false && staff_Address.canUpdate == true){
                    staff_address_can_update = true;
                }

                if(staff_address_can_view == false && staff_Address.canView == true){
                    staff_address_can_view = true;
                }

                if(staff_address_can_delete == false && staff_Address.canDelete == true){
                    staff_address_can_delete = true;
                }

            }else{
                console.log('Role Status => StaffRoleJson_profile_management.Address not found.');
            }
              
              
              var staff_temp_address_can = {};

              staff_temp_address_can.address_can_create = staff_address_can_create;
              staff_temp_address_can.address_can_update = staff_address_can_update;
              staff_temp_address_can.address_can_view = staff_address_can_view;
              staff_temp_address_can.address_can_delete = staff_address_can_delete;

              TempRoleArray.staff_address_can = staff_temp_address_can;
            /* --------------------- Staff Address --------------------- */

            /* --------------------- Staff Availability --------------------- */
              if(StaffRoleJson_profile_management.Availability !== undefined){
                var staff_availability =StaffRoleJson_profile_management.Availability;

                  if(staff_availability_can_create == false && staff_availability.canCreate == true){
                    staff_availability_can_create = true;
                  }
                  if(staff_availability_can_update == false && staff_availability.canUpdate == true){
                    staff_availability_can_update = true;
                  }
                  if(staff_availability_can_view == false && staff_availability.canView == true){
                    staff_availability_can_view = true;
                  }
                  if(staff_availability_can_delete == false && staff_availability.canDelete == true){
                    staff_availability_can_delete = true;
                  }
              }else{
                console.log('Role Status => StaffRoleJson_profile_management.Availability not found.');
              }
              
              
              var staff_temp_availability_can = {};

              staff_temp_availability_can.availability_can_create = staff_availability_can_create;
              staff_temp_availability_can.availability_can_update = staff_availability_can_update;
              staff_temp_availability_can.availability_can_view = staff_availability_can_view;
              staff_temp_availability_can.availability_can_delete = staff_availability_can_delete;

              TempRoleArray.staff_availability_can = staff_temp_availability_can;
            /* --------------------- Staff Availability --------------------- */

            /* --------------------- Staff Emergency_Contact --------------------- */

            if(StaffRoleJson_profile_management.Emergency_Contact !== undefined){
                var staff_Emergency_Contact=StaffRoleJson_profile_management.Emergency_Contact;

                if(staff_emergency_contact_can_create == false && staff_Emergency_Contact.canCreate == true){
                    staff_emergency_contact_can_create = true;
                }

                if(staff_emergency_contact_can_update == false && staff_Emergency_Contact.canUpdate == true){
                    staff_emergency_contact_can_update = true;
                }

                if(staff_emergency_contact_can_view == false && staff_Emergency_Contact.canView == true){
                    staff_emergency_contact_can_view = true;
                }

                if(staff_emergency_contact_can_delete == false && staff_Emergency_Contact.canDelete == true){
                    staff_emergency_contact_can_delete = true;
                }

            }else{
                console.log('Role Status => StaffRoleJson_profile_management.Emergency_Contact not found.');
            }
              
                    
              var temp_staff_emergency_contact_can = {};

              temp_staff_emergency_contact_can.emergency_contact_can_create = staff_emergency_contact_can_create;
              temp_staff_emergency_contact_can.emergency_contact_can_update = staff_emergency_contact_can_update;
              temp_staff_emergency_contact_can.emergency_contact_can_view = staff_emergency_contact_can_view;
              temp_staff_emergency_contact_can.emergency_contact_can_delete = staff_emergency_contact_can_delete;

              TempRoleArray.staff_emergency_contact_can = temp_staff_emergency_contact_can;
            /* --------------------- Staff Emergency_Contact --------------------- */

            /* --------------------- Staff Volunteer --------------------- */

            if(StaffRoleJson_profile_management.Volunteer !== undefined){
                  var staff_Volunteer=StaffRoleJson_profile_management.Volunteer;
              
                  if(staff_volunteer_can_create == false && staff_Volunteer.canCreate == true){
                    staff_volunteer_can_create = true;
                  }
                  if(staff_volunteer_can_update == false && staff_Volunteer.canUpdate == true){
                    staff_volunteer_can_update = true;
                  }
                  if(staff_volunteer_can_view == false && staff_Volunteer.canView == true){
                    staff_volunteer_can_view = true;
                  }
                  if(staff_volunteer_can_delete == false && staff_Volunteer.canDelete == true){
                    staff_volunteer_can_delete = true;
                  }
            }else{
                console.log('Role Status => StaffRoleJson_profile_management.Volunteer not found.');
            }
              

              var temp_staff_volunteer_can = {};

              temp_staff_volunteer_can.volunteer_can_create = staff_volunteer_can_create;
              temp_staff_volunteer_can.volunteer_can_update = staff_volunteer_can_update;
              temp_staff_volunteer_can.volunteer_can_view = staff_volunteer_can_view;
              temp_staff_volunteer_can.volunteer_can_delete = staff_volunteer_can_delete;
              
              TempRoleArray.staff_volunteer_can = temp_staff_volunteer_can;
            /* --------------------- Staff Volunteer --------------------- */

            /* --------------------- Staff Employment_History --------------------- */
                if(StaffRoleJson_profile_management.Employment_History !== undefined){
                  var staff_Employment_History=StaffRoleJson_profile_management.Employment_History;

                  if(staff_employment_history_can_create == false && staff_Employment_History.canCreate == true){
                    staff_employment_history_can_create = true;
                  }
                  if(staff_employment_history_can_update == false && staff_Employment_History.canUpdate == true){
                    staff_employment_history_can_update = true;
                  }
                  if(staff_employment_history_can_view == false && staff_Employment_History.canView == true){
                    staff_employment_history_can_view = true;
                  }
                  if(staff_employment_history_can_delete == false && staff_Employment_History.canDelete == true){
                    staff_employment_history_can_delete = true;
                  }
                }else{
                    console.log('Role Status => StaffRoleJson_profile_management.Employment_History not found.');
                }
              
              
              var temp_staff_employment_history_can = {};

              temp_staff_employment_history_can.employment_history_can_create = staff_employment_history_can_create;
              temp_staff_employment_history_can.employment_history_can_update = staff_employment_history_can_update;
              temp_staff_employment_history_can.employment_history_can_view = staff_employment_history_can_view;
              temp_staff_employment_history_can.employment_history_can_delete = staff_employment_history_can_delete;
              
              TempRoleArray.staff_employment_history_can = temp_staff_employment_history_can;
            /* --------------------- Staff Employment_History --------------------- */

            /* --------------------- Staff Skills --------------------- */
                if(StaffRoleJson_profile_management.Skills !== undefined){
                  var staff_Skills=StaffRoleJson_profile_management.Skills;

                  if(staff_skills_can_create == false && staff_Skills.canCreate == true){
                    staff_skills_can_create = true;
                  }
                  if(staff_skills_can_update == false && staff_Skills.canUpdate == true){
                    staff_skills_can_update = true;
                  }
                  if(staff_skills_can_view == false && staff_Skills.canView == true){
                    staff_skills_can_view = true;
                  }
                  if(staff_skills_can_delete == false && staff_Skills.canDelete == true){
                    staff_skills_can_delete = true;
                  }
                }else{
                    console.log('Role Status => StaffRoleJson_profile_management.Skills not found.');
                }

              
              
              var temp_staff_skills_can = {};

              temp_staff_skills_can.skills_can_create = staff_skills_can_create;
              temp_staff_skills_can.skills_can_update = staff_skills_can_update;
              temp_staff_skills_can.skills_can_view = staff_skills_can_view;
              temp_staff_skills_can.skills_can_delete = staff_skills_can_delete;

              TempRoleArray.staff_skills_can = temp_staff_skills_can;
            /* --------------------- Staff Skills --------------------- */

            /* --------------------- Staff Course_Training --------------------- */
                if(StaffRoleJson_profile_management.Course_Training !== undefined){
                    var staff_Course_Training=StaffRoleJson_profile_management.Course_Training;

                  if(staff_course_training_can_create == false && staff_Course_Training.canCreate == true){
                    staff_course_training_can_create = true;
                  }
                  if(staff_course_training_can_update == false && staff_Course_Training.canUpdate == true){
                    staff_course_training_can_update = true;
                  }
                  if(staff_course_training_can_view == false && staff_Course_Training.canView == true){
                    staff_course_training_can_view = true;
                  }
                  if(staff_course_training_can_delete == false && staff_Course_Training.canDelete == true){
                    staff_course_training_can_delete = true;
                  }
                }else{
                    console.log('Role Status => StaffRoleJson_profile_management.Course_Training not found.');
                }

              
              
              var temp_staff_course_training_can = {};

              temp_staff_course_training_can.course_training_can_create = staff_course_training_can_create;
              temp_staff_course_training_can.course_training_can_update = staff_course_training_can_update;
              temp_staff_course_training_can.course_training_can_view = staff_course_training_can_view;
              temp_staff_course_training_can.course_training_can_delete = staff_course_training_can_delete;
              
              TempRoleArray.staff_course_training_can = temp_staff_course_training_can;
            /* --------------------- Staff Course_Training --------------------- */

            /* --------------------- Staff Awards_Recognitions --------------------- */
                if(StaffRoleJson_profile_management.Awards_Recognitions !== undefined){
                    var staff_Awards_Recognitions=StaffRoleJson_profile_management.Awards_Recognitions;
              
                  if(staff_awards_recognitions_can_create == false && staff_Awards_Recognitions.canCreate == true){
                    staff_awards_recognitions_can_create = true;
                  }
                  if(staff_awards_recognitions_can_update == false && staff_Awards_Recognitions.canUpdate == true){
                    staff_awards_recognitions_can_update = true;
                  }
                  if(staff_awards_recognitions_can_view == false && staff_Awards_Recognitions.canView == true){
                    staff_awards_recognitions_can_view = true;
                  }
                  if(staff_awards_recognitions_can_delete == false && staff_Awards_Recognitions.canDelete == true){
                    staff_awards_recognitions_can_delete = true;
                  }
                }else{
                    console.log('Role Status => StaffRoleJson_profile_management.Awards_Recognitions not found.');
                }
              

              var temp_staff_awards_recognitions_can = {};

              temp_staff_awards_recognitions_can.awards_recognitions_can_create = staff_awards_recognitions_can_create;
              temp_staff_awards_recognitions_can.awards_recognitions_can_update = staff_awards_recognitions_can_update;
              temp_staff_awards_recognitions_can.awards_recognitions_can_view = staff_awards_recognitions_can_view;
              temp_staff_awards_recognitions_can.awards_recognitions_can_delete = staff_awards_recognitions_can_delete;
              
              TempRoleArray.staff_awards_recognitions_can = temp_staff_awards_recognitions_can;
            /* --------------------- Staff Awards_Recognitions --------------------- */

            /* --------------------- Staff Consents_Waivers --------------------- */
                if(StaffRoleJson_profile_management.Consents_Waivers !== undefined){
                    var staff_Consents_Waivers=StaffRoleJson_profile_management.Consents_Waivers;
              
                  if(staff_consents_waivers_can_create == false && staff_Consents_Waivers.canCreate == true){
                    staff_consents_waivers_can_create = true;
                  }
                  if(staff_consents_waivers_can_update == false && staff_Consents_Waivers.canUpdate == true){
                    staff_consents_waivers_can_update = true;
                  }
                  if(staff_consents_waivers_can_view == false && staff_Consents_Waivers.canView == true){
                    staff_consents_waivers_can_view = true;
                  }
                  if(staff_consents_waivers_can_delete == false && staff_Consents_Waivers.canDelete == true){
                    staff_consents_waivers_can_delete = true;
                  }
                }else{
                    console.log('Role Status => StaffRoleJson_profile_management.Consents_Waivers not found.');
                }
              

              var temp_staff_consents_waivers_can = {};
              
              temp_staff_consents_waivers_can.consents_waivers_can_create = staff_consents_waivers_can_create;
              temp_staff_consents_waivers_can.consents_waivers_can_update = staff_consents_waivers_can_update;
              temp_staff_consents_waivers_can.consents_waivers_can_view = staff_consents_waivers_can_view;
              temp_staff_consents_waivers_can.consents_waivers_can_delete = staff_consents_waivers_can_delete;
              
              TempRoleArray.staff_consents_waivers_can = temp_staff_consents_waivers_can;
            /* --------------------- Staff Consents_Waivers --------------------- */

            /* ------------ Staff Location Department profile tab ------------ */
                if(StaffRoleJson_profile_management.Locations_Departments !== undefined){
                    var staff_Locations_Departments=StaffRoleJson_profile_management.Locations_Departments;
              
                  if(staff_locations_departments_can_create == false && staff_Locations_Departments.canCreate == true){
                    staff_locations_departments_can_create = true;
                  }
                  if(staff_locations_departments_can_update == false && staff_Locations_Departments.canUpdate == true){
                    staff_locations_departments_can_update = true;
                  }
                  if(staff_locations_departments_can_view == false && staff_Locations_Departments.canView == true){
                    staff_locations_departments_can_view = true;
                  }
                  if(staff_locations_departments_can_delete == false && staff_Locations_Departments.canDelete == true){
                    staff_locations_departments_can_delete = true;
                  }
                }else{
                    console.log('Role Status => StaffRoleJson_profile_management.Locations_Departments not found.');
                }
              

              var temp_staff_locations_departments_can = {};

              temp_staff_locations_departments_can.locations_departments_can_create = staff_locations_departments_can_create;
              temp_staff_locations_departments_can.locations_departments_can_update = staff_locations_departments_can_update;
              temp_staff_locations_departments_can.locations_departments_can_view = staff_locations_departments_can_view;
              temp_staff_locations_departments_can.locations_departments_can_delete = staff_locations_departments_can_delete;
              
              TempRoleArray.staff_locations_departments_can = temp_staff_locations_departments_can;
            /* ------------ Staff Location Department profile tab ------------ */

            /* --------------------- Staff HumanResource --------------------- */
                if(StaffRoleJson_profile_management.HumanResource !== undefined){
                    var staff_HumanResource=StaffRoleJson_profile_management.HumanResource;
              
                  if(staff_human_resource_can_create == false && staff_HumanResource.canCreate == true){
                    staff_human_resource_can_create = true;
                  }
                  if(staff_human_resource_can_update == false && staff_HumanResource.canUpdate == true){
                    staff_human_resource_can_update = true;
                  }
                  if(staff_human_resource_can_view == false && staff_HumanResource.canView == true){
                    staff_human_resource_can_view = true;
                  }
                  if(staff_human_resource_can_delete == false && staff_HumanResource.canDelete == true){
                    staff_human_resource_can_delete = true;
                  }
                }else{
                    console.log('Role Status => StaffRoleJson_profile_management.HumanResource not found.');
                }
              

              var temp_staff_human_resource_can = {};
                    
              temp_staff_human_resource_can.human_resource_can_create = staff_human_resource_can_create;
              temp_staff_human_resource_can.human_resource_can_update = staff_human_resource_can_update;
              temp_staff_human_resource_can.human_resource_can_view = staff_human_resource_can_view;
              temp_staff_human_resource_can.human_resource_can_delete = staff_human_resource_can_delete;
              
              TempRoleArray.staff_human_resource_can = temp_staff_human_resource_can;
            /* --------------------- Staff HumanResource --------------------- */

            /* --------------------- Staff PersonalInfo --------------------- */
                if(StaffRoleJson_profile_management.PersonalInfo !== undefined){
                    var staff_PersonalInfo=StaffRoleJson_profile_management.PersonalInfo;

                     if(staff_personal_info_can_update == false && staff_PersonalInfo.canUpdate == true){
                        staff_personal_info_can_update = true;
                      }
                      if(staff_personal_info_can_view == false && staff_PersonalInfo.canView == true){
                        staff_personal_info_can_view = true;
                      }
                }else{
                    console.log('Role Status => StaffRoleJson_profile_management.PersonalInfo not found.');
                }
              
              
              var temp_staff_personal_info_can = {};

              temp_staff_personal_info_can.personal_info_can_update = staff_personal_info_can_update;
              temp_staff_personal_info_can.personal_info_can_view = staff_personal_info_can_view;
              
              TempRoleArray.staff_personal_info_can = temp_staff_personal_info_can;
            /* --------------------- Staff PersonalInfo --------------------- */

            /* ------------------------- Staff ProfileInfo ------------------------- */
                if(StaffRoleJson_profile_management.ProfileInfo !== undefined){
                    var staff_ProfileInfo=StaffRoleJson_profile_management.ProfileInfo;

                  if(staff_profile_info_can_update == false && staff_ProfileInfo.canUpdate == true){
                    staff_profile_info_can_update = true;
                  }
                  if(staff_profile_info_can_view == false && staff_ProfileInfo.canView == true){
                    staff_profile_info_can_view = true;
                  }
                }else{
                    console.log('Role Status => StaffRoleJson_profile_management.ProfileInfo not found.');
                }
              
              
              var temp_staff_profile_info_can = {};

              temp_staff_profile_info_can.profile_info_can_update = staff_profile_info_can_update;
              temp_staff_profile_info_can.profile_info_can_view = staff_profile_info_can_view;
              
              TempRoleArray.staff_profile_info_can = temp_staff_profile_info_can;

            /* ------------------------- Staff ProfileInfo ------------------------- */

            /* --------------------- Staff Documents --------------------- */
                if(StaffRoleJson_profile_management.Documents !== undefined){
                    var staff_Documents=StaffRoleJson_profile_management.Documents;
              
                  if(staff_documents_can_create == false && staff_Documents.canCreate == true){
                    staff_documents_can_create = true;
                  }
                  if(staff_documents_can_update == false && staff_Documents.canUpdate == true){
                    staff_documents_can_update = true;
                  }
                  if(staff_documents_can_view == false && staff_Documents.canView == true){
                    staff_documents_can_view = true;
                  }
                  if(staff_documents_can_delete == false && staff_Documents.canDelete == true){
                    staff_documents_can_delete = true;
                  }
                }else{
                    console.log('Role Status => StaffRoleJson_profile_management.Documents not found.');
                }
              

              var temp_staff_documents_can = {};

              temp_staff_documents_can.documents_can_create = staff_documents_can_create;
              temp_staff_documents_can.documents_can_update = staff_documents_can_update;
              temp_staff_documents_can.documents_can_view = staff_documents_can_view;
              temp_staff_documents_can.documents_can_delete = staff_documents_can_delete;
              
              TempRoleArray.staff_documents_can = temp_staff_documents_can;
            /* --------------------- Staff Documents --------------------- */

            /* --------------------- Staff Phss_EmpInfo --------------------- */
                if(StaffRoleJson_profile_management.Phss_EmpInfo !== undefined){
                    var staff_Phss_EmpInfo=StaffRoleJson_profile_management.Phss_EmpInfo;

                      if(staff_phss_empinfo_can_update == false && staff_Phss_EmpInfo.canUpdate == true){
                        staff_phss_empinfo_can_update = true;
                      }
                      if(staff_phss_empinfo_can_view == false && staff_Phss_EmpInfo.canView == true){
                        staff_phss_empinfo_can_view = true;
                      }
                }else{
                    console.log('Role Status => StaffRoleJson_profile_management.Phss_EmpInfo not found.');
                }
              
              
              var temp_staff_phss_empinfo_can = {};

              temp_staff_phss_empinfo_can.phss_empinfo_can_update = staff_phss_empinfo_can_update;
              temp_staff_phss_empinfo_can.phss_empinfo_can_view = staff_phss_empinfo_can_view;
              
              TempRoleArray.staff_phss_empinfo_can = temp_staff_phss_empinfo_can;
            /* --------------------- Staff Phss_EmpInfo --------------------- */

            /* --------------------- Staff Entitlement --------------------- */
              if(StaffRoleJson_profile_management.Entitlement !== undefined){
                var staff_Entitlement=StaffRoleJson_profile_management.Entitlement;

                if(staff_entitlement_can_view == false && staff_Entitlement.canView == true){
                    staff_entitlement_can_view = true;
                }

                if(staff_entitlement_can_create == false && staff_Entitlement.canCreate == true){
                    staff_entitlement_can_create = true;
                }
              }
              
              
              var temp_staff_entitlement_can = {};

              temp_staff_entitlement_can.entitlement_can_view = staff_entitlement_can_view;
              temp_staff_entitlement_can.entitlement_can_create = staff_entitlement_can_create;
              
              TempRoleArray.staff_entitlement_can = temp_staff_entitlement_can;
            /* --------------------- Staff Entitlement --------------------- */
          /* >>>>>>>>>>>>>>>>>>>>>>>> Staff Role permission <<<<<<<<<<<<<<<<<<<<<<<<< */  

          // Profile Role permission
          var JsonArray = row[zz].userRoleString
          var JsonCreate = JSON.parse(JsonArray);
          console.log('FetchRole All Helper');
          console.log(JsonCreate);
          
          console.log(JsonCreate.profile_management);

          var RoleJson_profile_management = JsonCreate.profile_management;
          // Profile Role permission

          /* ********************* profile management ********************* */
            /* ------------------------- ProfileInfo ------------------------- */
                if(RoleJson_profile_management.ProfileInfo !== undefined){
                    var ProfileInfo=RoleJson_profile_management.ProfileInfo;
                  if(profile_info_can_update == false && ProfileInfo.canUpdate == true){
                    profile_info_can_update = true;
                  }
                  if(profile_info_can_view == false && ProfileInfo.canView == true){
                    profile_info_can_view = true;
                  }
                }else{
                    console.log('Role Status => RoleJson_profile_management.ProfileInfo not found.');
                }
              
              var temp_profile_info_can = {};
              temp_profile_info_can.profile_info_can_update = profile_info_can_update;
              temp_profile_info_can.profile_info_can_view = profile_info_can_view;
              
              TempRoleArray.profile_info_can = temp_profile_info_can;
            /* ------------------------- ProfileInfo ------------------------- */

            /* --------------------- Contact_information --------------------- */
                if(RoleJson_profile_management.Contact_information !== undefined){
                    var Contact_information=RoleJson_profile_management.Contact_information;
                      if(contact_information_can_update == false && Contact_information.canUpdate == true){
                        contact_information_can_update = true;
                      }
                      if(contact_information_can_view == false && Contact_information.canView == true){
                        contact_information_can_view = true;
                      }
                }else{
                    console.log('Role Status => RoleJson_profile_management.Contact_information not found.');
                }
              
              
              var temp_contact_information_can = {};
              temp_contact_information_can.contact_information_can_update = contact_information_can_update;
              temp_contact_information_can.contact_information_can_view =contact_information_can_view;
              
              TempRoleArray.contact_information_can = temp_contact_information_can;
            /* --------------------- Contact_information --------------------- */

            /* --------------------- PersonalInfo --------------------- */
                if(RoleJson_profile_management.PersonalInfo !== undefined){
                    var PersonalInfo=RoleJson_profile_management.PersonalInfo;

                  if(personal_info_can_update == false && PersonalInfo.canUpdate == true){
                    personal_info_can_update = true;
                  }
                  if(personal_info_can_view == false && PersonalInfo.canView == true){
                    personal_info_can_view = true;
                  }
                }else{
                    console.log('Role Status => RoleJson_profile_management.PersonalInfo not found.');
                }
              
              

              var temp_personal_info_can = {};

              temp_personal_info_can.personal_info_can_update = personal_info_can_update;
              temp_personal_info_can.personal_info_can_view = personal_info_can_view;
              
              TempRoleArray.personal_info_can = temp_personal_info_can;
            /* --------------------- PersonalInfo --------------------- */

            /* --------------------- Phss_EmpInfo --------------------- */
                if(RoleJson_profile_management.Phss_EmpInfo !== undefined){
                    var Phss_EmpInfo=RoleJson_profile_management.Phss_EmpInfo;

                      if(phss_empinfo_can_update == false && Phss_EmpInfo.canUpdate == true){
                        phss_empinfo_can_update = true;
                      }
                      if(phss_empinfo_can_view == false && Phss_EmpInfo.canView == true){
                        phss_empinfo_can_view = true;
                      }
                }else{
                    console.log('Role Status => RoleJson_profile_management.Phss_EmpInfo not found.');
                }
              
              
              var temp_phss_empinfo_can = {};

              temp_phss_empinfo_can.phss_empinfo_can_update = phss_empinfo_can_update;
              temp_phss_empinfo_can.phss_empinfo_can_view = phss_empinfo_can_view;
              
              TempRoleArray.phss_empinfo_can = temp_phss_empinfo_can;
            /* --------------------- Phss_EmpInfo --------------------- */                

            /* --------------------- Address --------------------- */
                if(RoleJson_profile_management.Address !== undefined){
                    var Address=RoleJson_profile_management.Address;

                  if(address_can_create == false && Address.canCreate == true){
                    address_can_create = true;
                  }
                  if(address_can_update == false && Address.canUpdate == true){
                    address_can_update = true;
                  }
                  if(address_can_view == false && Address.canView == true){
                    address_can_view = true;
                  }
                  if(address_can_delete == false && Address.canDelete == true){
                    address_can_delete = true;
                  }
                }else{
                    console.log('Role Status => RoleJson_profile_management.Address not found.');
                }
              
              
              var temp_address_can = {};

              temp_address_can.address_can_create = address_can_create;
              temp_address_can.address_can_update = address_can_update;
              temp_address_can.address_can_view = address_can_view;
              temp_address_can.address_can_delete = address_can_delete;

              TempRoleArray.address_can = temp_address_can;
            /* --------------------- Address --------------------- */

            /* --------------------- Availability --------------------- */
            if(RoleJson_profile_management.Availability !== undefined){
              var availability=RoleJson_profile_management.Availability;

              if(availability_can_create == false && availability.canCreate == true){
                availability_can_create = true;
              }
              if(availability_can_update == false && availability.canUpdate == true){
                availability_can_update = true;
              }
              if(availability_can_view == false && availability.canView == true){
                availability_can_view = true;
              }
              if(availability_can_delete == false && availability.canDelete == true){
                availability_can_delete = true;
              }
            }  
              var temp_availability_can = {};

              temp_availability_can.availability_can_create = availability_can_create;
              temp_availability_can.availability_can_update = availability_can_update;
              temp_availability_can.availability_can_view = availability_can_view;
              temp_availability_can.availability_can_delete = availability_can_delete;

              TempRoleArray.availability_can = temp_availability_can;
            /* --------------------- Availability --------------------- */

            /* --------------------- Emergency_Contact --------------------- */
                if(RoleJson_profile_management.Emergency_Contact !== undefined){
                    var Emergency_Contact=RoleJson_profile_management.Emergency_Contact;

                  if(emergency_contact_can_create == false && Emergency_Contact.canCreate == true){
                    emergency_contact_can_create = true;
                  }
                  if(emergency_contact_can_update == false && Emergency_Contact.canUpdate == true){
                    emergency_contact_can_update = true;
                  }
                  if(emergency_contact_can_view == false && Emergency_Contact.canView == true){
                    emergency_contact_can_view = true;
                  }
                  if(emergency_contact_can_delete == false && Emergency_Contact.canDelete == true){
                    emergency_contact_can_delete = true;
                  }
                }else{
                    console.log('Role Status => RoleJson_profile_management.Emergency_Contact not found.');
                }
              
                    
              var temp_emergency_contact_can = {};

              temp_emergency_contact_can.emergency_contact_can_create = emergency_contact_can_create;
              temp_emergency_contact_can.emergency_contact_can_update = emergency_contact_can_update;
              temp_emergency_contact_can.emergency_contact_can_view = emergency_contact_can_view;
              temp_emergency_contact_can.emergency_contact_can_delete = emergency_contact_can_delete;

              TempRoleArray.emergency_contact_can = temp_emergency_contact_can;
            /* --------------------- Emergency_Contact --------------------- */

            /* --------------------- Employment_History --------------------- */
                if(RoleJson_profile_management.Employment_History !== undefined){
                    var Employment_History=RoleJson_profile_management.Employment_History;

                  if(employment_history_can_create == false && Employment_History.canCreate == true){
                    employment_history_can_create = true;
                  }
                  if(employment_history_can_update == false && Employment_History.canUpdate == true){
                    employment_history_can_update = true;
                  }
                  if(employment_history_can_view == false && Employment_History.canView == true){
                    employment_history_can_view = true;
                  }
                  if(employment_history_can_delete == false && Employment_History.canDelete == true){
                    employment_history_can_delete = true;
                  }
                }else{
                    console.log('Role Status => RoleJson_profile_management.Employment_History not found.');
                }

              
              
              var temp_employment_history_can = {};

              temp_employment_history_can.employment_history_can_create = employment_history_can_create;
              temp_employment_history_can.employment_history_can_update = employment_history_can_update;
              temp_employment_history_can.employment_history_can_view = employment_history_can_view;
              temp_employment_history_can.employment_history_can_delete = employment_history_can_delete;
              
              TempRoleArray.employment_history_can = temp_employment_history_can;
            /* --------------------- Employment_History --------------------- */

            /* --------------------- Course_Training --------------------- */
                if(RoleJson_profile_management.Course_Training !== undefined){
                    var Course_Training=RoleJson_profile_management.Course_Training;

                  if(course_training_can_create == false && Course_Training.canCreate == true){
                    course_training_can_create = true;
                  }
                  if(course_training_can_update == false && Course_Training.canUpdate == true){
                    course_training_can_update = true;
                  }
                  if(course_training_can_view == false && Course_Training.canView == true){
                    course_training_can_view = true;
                  }
                  if(course_training_can_delete == false && Course_Training.canDelete == true){
                    course_training_can_delete = true;
                  }
                }else{
                    console.log('Role Status => RoleJson_profile_management.Course_Training not found.');
                }
              
              
              var temp_course_training_can = {};

              temp_course_training_can.course_training_can_create = course_training_can_create;
              temp_course_training_can.course_training_can_update = course_training_can_update;
              temp_course_training_can.course_training_can_view = course_training_can_view;
              temp_course_training_can.course_training_can_delete = course_training_can_delete;
              
              TempRoleArray.course_training_can = temp_course_training_can;
            /* --------------------- Course_Training --------------------- */

            /* --------------------- Skills --------------------- */
                if(RoleJson_profile_management.Skills !== undefined){
                    var Skills=RoleJson_profile_management.Skills;

                  if(skills_can_create == false && Skills.canCreate == true){
                    skills_can_create = true;
                  }
                  if(skills_can_update == false && Skills.canUpdate == true){
                    skills_can_update = true;
                  }
                  if(skills_can_view == false && Skills.canView == true){
                    skills_can_view = true;
                  }
                  if(skills_can_delete == false && Skills.canDelete == true){
                    skills_can_delete = true;
                  }
                }else{
                    console.log('Role Status => RoleJson_profile_management.Skills not found.');
                }
              
              
              var temp_skills_can = {};

              temp_skills_can.skills_can_create = skills_can_create;
              temp_skills_can.skills_can_update = skills_can_update;
              temp_skills_can.skills_can_view = skills_can_view;
              temp_skills_can.skills_can_delete = skills_can_delete;

              TempRoleArray.skills_can = temp_skills_can;
            /* --------------------- Skills --------------------- */
              
            /* --------------------- Consents_Waivers --------------------- */
                if(RoleJson_profile_management.Consents_Waivers !== undefined){
                    var Consents_Waivers=RoleJson_profile_management.Consents_Waivers;
              
                  if(consents_waivers_can_create == false && Consents_Waivers.canCreate == true){
                    consents_waivers_can_create = true;
                  }
                  if(consents_waivers_can_update == false && Consents_Waivers.canUpdate == true){
                    consents_waivers_can_update = true;
                  }
                  if(consents_waivers_can_view == false && Consents_Waivers.canView == true){
                    consents_waivers_can_view = true;
                  }
                  if(consents_waivers_can_delete == false && Consents_Waivers.canDelete == true){
                    consents_waivers_can_delete = true;
                  }
                }else{
                    console.log('Role Status => RoleJson_profile_management.Consents_Waivers not found.');
                }
              

              var temp_consents_waivers_can = {};
              
              temp_consents_waivers_can.consents_waivers_can_create = consents_waivers_can_create;
              temp_consents_waivers_can.consents_waivers_can_update = consents_waivers_can_update;
              temp_consents_waivers_can.consents_waivers_can_view = consents_waivers_can_view;
              temp_consents_waivers_can.consents_waivers_can_delete = consents_waivers_can_delete;
              
              TempRoleArray.consents_waivers_can = temp_consents_waivers_can;
            /* --------------------- Consents_Waivers --------------------- */

            /* --------------------- Awards_Recognitions --------------------- */
                if(RoleJson_profile_management.Awards_Recognitions !== undefined){
                    var Awards_Recognitions=RoleJson_profile_management.Awards_Recognitions;
              
                  if(awards_recognitions_can_create == false && Awards_Recognitions.canCreate == true){
                    awards_recognitions_can_create = true;
                  }
                  if(awards_recognitions_can_update == false && Awards_Recognitions.canUpdate == true){
                    awards_recognitions_can_update = true;
                  }
                  if(awards_recognitions_can_view == false && Awards_Recognitions.canView == true){
                    awards_recognitions_can_view = true;
                  }
                  if(awards_recognitions_can_delete == false && Awards_Recognitions.canDelete == true){
                    awards_recognitions_can_delete = true;
                  }
                }else{
                    console.log('Role Status => RoleJson_profile_management.Awards_Recognitions not found.');
                }
              

              var temp_awards_recognitions_can = {};

              temp_awards_recognitions_can.awards_recognitions_can_create = awards_recognitions_can_create;
              temp_awards_recognitions_can.awards_recognitions_can_update = awards_recognitions_can_update;
              temp_awards_recognitions_can.awards_recognitions_can_view = awards_recognitions_can_view;
              temp_awards_recognitions_can.awards_recognitions_can_delete = awards_recognitions_can_delete;
              
              TempRoleArray.awards_recognitions_can = temp_awards_recognitions_can;
            /* --------------------- Awards_Recognitions --------------------- */

            /* --------------------- HumanResource --------------------- */
                if(RoleJson_profile_management.HumanResource !== undefined){
                    var HumanResource=RoleJson_profile_management.HumanResource;
              
                  if(human_resource_can_create == false && HumanResource.canCreate == true){
                    human_resource_can_create = true;
                  }
                  if(human_resource_can_update == false && HumanResource.canUpdate == true){
                    human_resource_can_update = true;
                  }
                  if(human_resource_can_view == false && HumanResource.canView == true){
                    human_resource_can_view = true;
                  }
                  if(human_resource_can_delete == false && HumanResource.canDelete == true){
                    human_resource_can_delete = true;
                  }
                }else{
                    console.log('Role Status => RoleJson_profile_management.HumanResource not found.');
                }
              

              var temp_human_resource_can = {};
                    
              temp_human_resource_can.human_resource_can_create = human_resource_can_create;
              temp_human_resource_can.human_resource_can_update = human_resource_can_update;
              temp_human_resource_can.human_resource_can_view = human_resource_can_view;
              temp_human_resource_can.human_resource_can_delete = human_resource_can_delete;
              
              TempRoleArray.human_resource_can = temp_human_resource_can;
            /* --------------------- HumanResource --------------------- */

            /* --------------------- Documents --------------------- */
                if(RoleJson_profile_management.Documents !== undefined){
                    var Documents=RoleJson_profile_management.Documents;
              
                  if(documents_can_create == false && Documents.canCreate == true){
                    documents_can_create = true;
                  }
                  if(documents_can_update == false && Documents.canUpdate == true){
                    documents_can_update = true;
                  }
                  if(documents_can_view == false && Documents.canView == true){
                    documents_can_view = true;
                  }
                  if(documents_can_delete == false && Documents.canDelete == true){
                    documents_can_delete = true;
                  }
                }else{
                    console.log('Role Status => RoleJson_profile_management.Documents not found.');
                }
              

              var temp_documents_can = {};

              temp_documents_can.documents_can_create = documents_can_create;
              temp_documents_can.documents_can_update = documents_can_update;
              temp_documents_can.documents_can_view = documents_can_view;
              temp_documents_can.documents_can_delete = documents_can_delete;
              
              TempRoleArray.documents_can = temp_documents_can;
            /* --------------------- Documents --------------------- */

            /* ------------ Location Department profile tab ------------ */
                if(RoleJson_profile_management.Locations_Departments !== undefined){
                    var Locations_Departments=RoleJson_profile_management.Locations_Departments;
              
                      if(locations_departments_can_create == false && Locations_Departments.canCreate == true){
                        locations_departments_can_create = true;
                      }
                      if(locations_departments_can_update == false && Locations_Departments.canUpdate == true){
                        locations_departments_can_update = true;
                      }
                      if(locations_departments_can_view == false && Locations_Departments.canView == true){
                        locations_departments_can_view = true;
                      }
                      if(locations_departments_can_delete == false && Locations_Departments.canDelete == true){
                        locations_departments_can_delete = true;
                      }
                }else{
                    console.log('Role Status => RoleJson_profile_management.Locations_Departments not found.');
                }
              

              var temp_locations_departments_can = {};

              temp_locations_departments_can.locations_departments_can_create = locations_departments_can_create;
              temp_locations_departments_can.locations_departments_can_update = locations_departments_can_update;
              temp_locations_departments_can.locations_departments_can_view = locations_departments_can_view;
              temp_locations_departments_can.locations_departments_can_delete = locations_departments_can_delete;
              
              TempRoleArray.locations_departments_can = temp_locations_departments_can;
            /* ------------ Location Department profile tab ------------ */

            /* --------------------- Volunteer --------------------- */
                if(RoleJson_profile_management.Volunteer !== undefined){
                    var Volunteer=RoleJson_profile_management.Volunteer;
              
                  if(volunteer_can_create == false && Volunteer.canCreate == true){
                    volunteer_can_create = true;
                  }
                  if(volunteer_can_update == false && Volunteer.canUpdate == true){
                    volunteer_can_update = true;
                  }
                  if(volunteer_can_view == false && Volunteer.canView == true){
                    volunteer_can_view = true;
                  }
                  if(volunteer_can_delete == false && Volunteer.canDelete == true){
                    volunteer_can_delete = true;
                  }
                }else{
                    console.log('Role Status => RoleJson_profile_management.Volunteer not found.');
                }
              

              var temp_volunteer_can = {};

              temp_volunteer_can.volunteer_can_create = volunteer_can_create;
              temp_volunteer_can.volunteer_can_update = volunteer_can_update;
              temp_volunteer_can.volunteer_can_view = volunteer_can_view;
              temp_volunteer_can.volunteer_can_delete = volunteer_can_delete;
              
              TempRoleArray.volunteer_can = temp_volunteer_can;
            /* --------------------- Volunteer --------------------- */ 

            /* --------------------- Entitlement --------------------- */

            if(RoleJson_profile_management.Entitlement !== undefined){
              var Entitlement=RoleJson_profile_management.Entitlement;
              
              if(entitlement_can_view == false && Entitlement.canView == true){
                entitlement_can_view = true;
              }

              if(entitlement_can_create == false && Entitlement.canCreate == true){
                entitlement_can_create = true;
              }
            }  
              var temp_entitlement_can = {};

              temp_entitlement_can.entitlement_can_view = entitlement_can_view;
              temp_entitlement_can.entitlement_can_create = entitlement_can_create;
              
              TempRoleArray.entitlement_can = temp_entitlement_can;
            /* --------------------- Entitlement --------------------- */
          /* ********************* profile management ********************* */

          /* ********************* timeSheet management ********************* */
            console.log(i+'timeSheetJson');
            var JsontimeSheetJson = row[zz].timeSheetJson
            var JsontimeSheet = JSON.parse(JsontimeSheetJson);
            console.log(JsontimeSheet);
            var RoleJson_timeSheetJson = JsontimeSheet.Timesheet;

            var Audit=RoleJson_timeSheetJson.Audit;

            var temp_audit_can = {};
            temp_audit_can.audit_can_view = Audit.canView;
            TempRoleArray.audit_can = temp_audit_can;

            if(RoleJson_timeSheetJson.Timesheet !== undefined){
                var Timesheet=RoleJson_timeSheetJson.Timesheet;

                if(timesheet_can_create == false && Timesheet.canCreate == true){
                  timesheet_can_create = true;
                }
                if(timesheet_can_update == false && Timesheet.canUpdate == true){
                  timesheet_can_update = true;
                }
                if(timesheet_can_view == false && Timesheet.canView == true){
                  timesheet_can_view = true;
                }
                if(timesheet_can_delete == false && Timesheet.canDelete == true){
                  timesheet_can_delete = true;
                }
                if(timesheet_can_export == false && Timesheet.Export == true){
                  timesheet_can_export = true;
                }
                if(timesheet_can_summary == false && Timesheet.Summary == true){
                  timesheet_can_summary = true;
                }
                if(timesheet_can_approve == false && Timesheet.canApprove == true){
                  timesheet_can_approve = true;
                }
            }else{
                console.log('Role Status => RoleJson_timeSheetJson.Timesheet not found.');
            }

            
                  
            var temp_timesheet_can = {};

            temp_timesheet_can.timesheet_can_create = timesheet_can_create;
            temp_timesheet_can.timesheet_can_update = timesheet_can_update;
            temp_timesheet_can.timesheet_can_view = timesheet_can_view;
            temp_timesheet_can.timesheet_can_delete = timesheet_can_delete;
            temp_timesheet_can.timesheet_can_export = timesheet_can_export;
            temp_timesheet_can.timesheet_can_summary = timesheet_can_summary;
            temp_timesheet_can.timesheet_can_approve = timesheet_can_approve;
                  
            TempRoleArray.timesheet_can = temp_timesheet_can;

          /* ********************* timeSheet management ********************* */

          /* ********************* User ProfileJson management ********************* */
            console.log(i+'userProfileJson');
            var userProfileJson = row[zz].userProfileJson
            var JsonuserProfile = JSON.parse(userProfileJson);
            console.log(JsonuserProfile);
            var RoleJson_UserManagement = JsonuserProfile.UserManagement;

            /* --------------------- User --------------------- */
            if(RoleJson_UserManagement.User !== undefined){
                var User=RoleJson_UserManagement.User;

                // if(user_can_create == false && User.canCreate == true){
                //   user_can_create = true;
                // }
                if(user_can_update == false && User.canUpdate == true){
                  user_can_update = true;
                }
                if(user_can_view == false && User.canView == true){
                  user_can_view = true;
                }
                // if(user_can_delete == false && User.canDelete == true){
                //   user_can_delete = true;
                // }
            }else{
                console.log('Role Status => RoleJson_UserManagement.User not found.');
            }

            
                  
            var temp_user_can = {};

            //temp_user_can.user_can_create = user_can_create;
            temp_user_can.user_can_update = user_can_update;
            temp_user_can.user_can_view = user_can_view;
            //temp_user_can.user_can_delete = user_can_delete;
            
            TempRoleArray.user_can = temp_user_can;
            /* --------------------- User --------------------- */

            /* --------------------- ContactList --------------------- */
            if(RoleJson_UserManagement.ContactList !== undefined){
                var ContactList=RoleJson_UserManagement.ContactList;
            
                if(contactlist_can_create == false && ContactList.canCreate == true){
                  contactlist_can_create = true;
                }
                if(contactlist_can_update == false && ContactList.canUpdate == true){
                  contactlist_can_update = true;
                }
                if(contactlist_can_view == false && ContactList.canView == true){
                  contactlist_can_view = true;
                }
                if(contactlist_can_delete == false && ContactList.canDelete == true){
                  contactlist_can_delete = true;
                }

                if(contactlist_can_viewall == false && ContactList.canViewAll == true){
                  contactlist_can_viewall = true;
                }
            }else{
                console.log('Role Status => RoleJson_UserManagement.ContactList not found.');
            }
            

            var temp_contactlist_can = {};

            temp_contactlist_can.contactlist_can_create = contactlist_can_create;
            temp_contactlist_can.contactlist_can_update = contactlist_can_update;
            temp_contactlist_can.contactlist_can_view = contactlist_can_view;
            temp_contactlist_can.contactlist_can_delete = contactlist_can_delete;
            temp_contactlist_can.contactlist_can_viewall = contactlist_can_viewall;
            
            TempRoleArray.contactlist_can = temp_contactlist_can;
            /* --------------------- ContactList --------------------- */

          /* ********************* User ProfileJson management ********************* */

          /* ********************* masterJson management ********************* */
            console.log(i+'masterJson');
            var masterJson = row[zz].masterJson
            var Jsonmaster = JSON.parse(masterJson);
            console.log(Jsonmaster);
            var RoleJson_Jsonmaster = Jsonmaster.Masters;

            /* --------------------- Locations --------------------- */
            if(RoleJson_Jsonmaster.Locations !== undefined){
                var Locations=RoleJson_Jsonmaster.Locations;

                if(locations_can_create == false && Locations.canCreate == true){
                  locations_can_create = true;
                }
                if(locations_can_update == false && Locations.canUpdate == true){
                  locations_can_update = true;
                }
                if(locations_can_view == false && Locations.canView == true){
                  locations_can_view = true;
                }
                if(locations_can_delete == false && Locations.canDelete == true){
                  locations_can_delete = true;
                }

                if(locations_can_viewall == false && Locations.canViewAll == true){
                  locations_can_viewall = true;
                }
            }else{
                console.log('Role Status => RoleJson_Jsonmaster.Locations not found.');
            }

            
            
            var temp_locations_can = {};
            
            temp_locations_can.locations_can_create = locations_can_create;
            temp_locations_can.locations_can_update = locations_can_update;
            temp_locations_can.locations_can_view = locations_can_view;
            temp_locations_can.locations_can_delete = locations_can_delete;
            temp_locations_can.locations_can_viewall = locations_can_viewall;
            
            TempRoleArray.locations_can = temp_locations_can;
            /* --------------------- Locations --------------------- */

            /* --------------------- Employees --------------------- */
            if(RoleJson_Jsonmaster.Employees !== undefined){
                var Employees=RoleJson_Jsonmaster.Employees;
            
                if(employees_can_create == false && Employees.canCreate == true){
                  employees_can_create = true;
                }
                if(employees_can_update == false && Employees.canUpdate == true){
                  employees_can_update = true;
                }
                if(employees_can_view == false && Employees.canView == true){
                  employees_can_view = true;
                }
                if(employees_can_delete == false && Employees.canDelete == true){
                  employees_can_delete = true;
                }

                if(employees_can_viewall == false && Employees.canViewAll == true){
                  employees_can_viewall = true;
                }
            }else{
                console.log('Role Status => RoleJson_Jsonmaster.Employees not found.');
            }

            

            var temp_employees_can = {};
            
            temp_employees_can.employees_can_create = employees_can_create;
            temp_employees_can.employees_can_update = employees_can_update;
            temp_employees_can.employees_can_view = employees_can_view;
            temp_employees_can.employees_can_delete = employees_can_delete;
            temp_employees_can.employees_can_viewall = employees_can_viewall;
            
            TempRoleArray.employees_can = temp_employees_can;
            /* --------------------- Employees --------------------- */

            /* --------------------- holidays --------------------- */
            if(RoleJson_Jsonmaster.Holidays !== undefined){
                var holidays=RoleJson_Jsonmaster.Holidays;
            
                if(holidays_can_create == false && holidays.canCreate == true){
                  holidays_can_create = true;
                }
                if(holidays_can_update == false && holidays.canUpdate == true){
                  holidays_can_update = true;
                }
                if(holidays_can_view == false && holidays.canView == true){
                  holidays_can_view = true;
                }
                if(holidays_can_delete == false && holidays.canDelete == true){
                  holidays_can_delete = true;
                }

                if(holidays_can_viewall == false && holidays.canViewAll == true){
                  holidays_can_viewall = true;
                }
            }else{
                console.log('Role Status => RoleJson_Jsonmaster.Holidays not found.');
            }

            

            var temp_holidays_can = {};
            
            temp_holidays_can.holidays_can_create = holidays_can_create;
            temp_holidays_can.holidays_can_update = holidays_can_update;
            temp_holidays_can.holidays_can_view = holidays_can_view;
            temp_holidays_can.holidays_can_delete = holidays_can_delete;
            temp_holidays_can.holidays_can_viewall = holidays_can_viewall;
            
            TempRoleArray.holidays_can = temp_holidays_can;
            /* --------------------- holidays --------------------- */

          /* ********************* masterJson management ********************* */

          /* ********************* reportJson management ********************* */
            console.log(i+'reportJson');
            var reportJson = row[zz].reportJson;

            console.log('repor json');
            console.log(row[zz].reportJson);

            /* --------------------- Report --------------------- */
            
            var Jsonreport = JSON.parse(reportJson);
            console.log(Jsonreport);
            var RoleJson_Jsonreport = Jsonreport.Report;

            if(RoleJson_Jsonreport.YTD_Hours !== undefined){
                var Report=RoleJson_Jsonreport.YTD_Hours;
                /*console.log('repor if');
                console.log(Report);
                console.log('11111 =' + zz);
                console.log(report_can_view);
                console.log(Report.canView);*/
                
                if(report_can_view == false && Report.canView == true){
                  report_can_view = true;
                }
            }else{
                console.log('Role Status => RoleJson_Jsonreport.YTD_Hours not found.');
            }
            
            
            var temp_report_can = {};

            temp_report_can.report_can_view = report_can_view;
            
            TempRoleArray.report_can = temp_report_can;
              
            /* --------------------- Report --------------------- */

            /* --------------------- AuditReport --------------------- */
            if(RoleJson_Jsonreport.AuditReport !== undefined){
              var ReportAudit=RoleJson_Jsonreport.AuditReport;
              
              if(audit_report_menu_can_view == false && ReportAudit.canView == true){
                audit_report_menu_can_view = true;
              }
            }else{
                console.log('Role Status => RoleJson_Jsonreport.AuditReport not found.');
            }

            var temp_audit_report_menu_can_view = {};

            temp_audit_report_menu_can_view.audit_report_menu_can_view = audit_report_menu_can_view;
            
            TempRoleArray.audit_report_menu_can = temp_audit_report_menu_can_view;
            /* --------------------- AuditReport --------------------- */

            /* --------------------- Batch --------------------- */
            if(RoleJson_Jsonreport.Batch !== undefined){
                var ReportBatch=RoleJson_Jsonreport.Batch;
            
                if(batch_report_menu_can_view == false && ReportBatch.canView == true){
                  batch_report_menu_can_view = true;
                }
            }else{
                console.log('Role Status => RoleJson_Jsonreport.Batch not found.');
            }
            
            
            var temp_batch_report_menu_can_view = {};

            temp_batch_report_menu_can_view.batch_report_menu_can_view = batch_report_menu_can_view;
            
            TempRoleArray.batch_report_menu_can = temp_batch_report_menu_can_view;
            /* --------------------- Batch --------------------- */

          /* ********************* reportJson management ********************* */

          

          /* >>>>>>>>>>>>>>>>>>>>>>>> Scheduler Role permission <<<<<<<<<<<<<<<<<<<<<<<<< */  

            // Scheduler Role permission
            var JsonArray2 = row[zz].scheduleJson
            var JsonSchedule = JSON.parse(JsonArray2);
            console.log('FetchRole All Helper Scheduler');
            console.log(JsonSchedule);
            
            console.log(JsonSchedule.Scheduler);

            var RoleJson_scheduler_management = JsonSchedule.Scheduler;
            // Scheduler Role permission

            /* --------------------- Scheduler --------------------- */
            if(RoleJson_scheduler_management.Scheduler !== undefined){
                var scheduler=RoleJson_scheduler_management.Scheduler;

                if(scheduler_can_create == false && scheduler.canCreate == true){
                  scheduler_can_create = true;
                }
                if(scheduler_can_update == false && scheduler.canUpdate == true){
                  scheduler_can_update = true;
                }
                if(scheduler_can_view == false && scheduler.canView == true){
                  scheduler_can_view = true;
                }
                if(scheduler_can_delete == false && scheduler.canDelete == true){
                  scheduler_can_delete = true;
                }

                if(scheduler_can_assign == false  && scheduler.Assign == true ){
                  scheduler_can_assign = true;
                }

                if(scheduler_can_duplicate == false  && scheduler.Duplicate == true){
                  scheduler_can_duplicate = true;
                }
            }else{
                console.log('Role Status => RoleJson_scheduler_management.Scheduler not found.');
            }
            
            
            var temp_scheduler_can = {};

            temp_scheduler_can.scheduler_can_create = scheduler_can_create;
            temp_scheduler_can.scheduler_can_update = scheduler_can_update;
            temp_scheduler_can.scheduler_can_view = scheduler_can_view;
            temp_scheduler_can.scheduler_can_delete = scheduler_can_delete;
            temp_scheduler_can.scheduler_can_assign = scheduler_can_assign;
            temp_scheduler_can.scheduler_can_duplicate = scheduler_can_duplicate;

            TempRoleArray.scheduler_can = temp_scheduler_can;
            /* --------------------- Scheduler --------------------- */
          /* >>>>>>>>>>>>>>>>>>>>>>>> Scheduler Role permission <<<<<<<<<<<<<<<<<<<<<<<<< */  

          /* ********************* HierarchyJson management ********************* */  
            /* --------------------- Hierarchy --------------------- */
            
            console.log(i+'hierarchyJson');
            var hierarchyJson = row[zz].hierarchy;

            console.log('hierarchy json');
            console.log(hierarchyJson);

            var temp_hierarchy_can = {};
            if(hierarchyJson != null && hierarchyJson != "")
            {
              if(hierarchy_can_id < hierarchyJson || hierarchy_can_id == ""){
                hierarchy_can_id = hierarchyJson;
              }

              temp_hierarchy_can.hierarchy_can_id = hierarchy_can_id;
              
              if(hierarchy_can_id == 1){
                temp_hierarchy_can.hierarchy_can_type = "H0 - Myself";
              }else if(hierarchy_can_id == 2){
                temp_hierarchy_can.hierarchy_can_type = "H1 - People in my location/Primary Location";
              }else if(hierarchy_can_id == 3){
                temp_hierarchy_can.hierarchy_can_type = "H2 - People in the location i was assigned to";
              }else if(hierarchy_can_id == 3){
                temp_hierarchy_can.hierarchy_can_type = "H3 - People in all location";
              }
              
              
              TempRoleArray.hierarchy_can = temp_hierarchy_can;
            }
            /* --------------------- Hierarchy --------------------- */
          /* ********************* HierarchyJson management ********************* */

          /* ********************* Entitlement Side Menu management ********************* */
            console.log(i+'entitlementJson');
            var entitlementJson = row[zz].entitlementJson;

            console.log('entitlement Side json');
            console.log(row[zz].entitlementJson);

            /* --------------------- Entitlement Side --------------------- */
            
            var Jsonentitlement = JSON.parse(entitlementJson);
            console.log(Jsonentitlement);

            if(Jsonentitlement.Entitlement !== undefined){
                var RoleJson_Jsonreport = Jsonentitlement.Entitlement;

            
                var Entitlement_menu=RoleJson_Jsonreport.Entitlement;
                
                if(entitlement_menu_can_view == false && Entitlement_menu.canView == true){
                  entitlement_menu_can_view = true;
                }

                if(entitlement_menu_can_create == false && Entitlement_menu.canCreate == true){
                  entitlement_menu_can_create = true;
                }
            }else{
                console.log('Role Status => Jsonentitlement.Entitlement not found.');
            }

            
            
            var temp_entitlement_menu_can = {};

            temp_entitlement_menu_can.entitlement_menu_can_view = entitlement_menu_can_view;
            temp_entitlement_menu_can.entitlement_menu_can_create = entitlement_menu_can_create;
            
            TempRoleArray.entitlement_menu_can = temp_entitlement_menu_can;
              
            
            /* --------------------- Entitlement Side --------------------- */
          /* ********************* Entitlement Side Menu management ********************* */

          /* ********************* Payroll management ********************* */
            console.log(i+'Payroll adjustmentJson');
            var adjustmentJson = row[zz].adjustmentJson;

            console.log('Payroll adjustment json');
            console.log(row[zz].adjustmentJson);

            /* --------------------- Adjustment --------------------- */
            
            var Jsonadjustment = JSON.parse(adjustmentJson);
            console.log(Jsonadjustment);

            if(Jsonadjustment.Adjustment !== undefined){
                var RoleJson_Jsonadjustment = Jsonadjustment.Adjustment;

            
                var PayrollAdjustment=RoleJson_Jsonadjustment.Payroll_Adjustment;
                
                if(payroll_adjustment_menu_can_view == false && PayrollAdjustment.canView == true){
                  payroll_adjustment_menu_can_view = true;
                }
            }else{
                console.log('Role Status => Jsonadjustment.Adjustment not found.');
            }
            
            
            var temp_payroll_adjustment_menu_can = {};

            temp_payroll_adjustment_menu_can.payroll_adjustment_menu_can_view = payroll_adjustment_menu_can_view;
            
            TempRoleArray.payroll_adjustment_menu_can = temp_payroll_adjustment_menu_can;
              
            
            /* --------------------- Adjustment --------------------- */
          /* ********************* Payroll management ********************* */


          

          if(i == row_count)
          {
            //this.props.setPropState('role_func_call', true);
          }
          i++;
          console.log('=======+++++++++');
          console.log(TempRoleArray);
        
      }

        console.log('=======+++++++++');
        console.log(TempRoleArray);
        console.log('=======String+++++++++');
        console.log(TempRoleArray);
        
        var pwd = contactId+"Phss@123";
        var Role_str = CryptoAES.encrypt(JSON.stringify(TempRoleArray), pwd);
        localStorage.setItem('sessiontoken', Role_str);
              
    },

    GetUserWiseTimeSheetData: function(contactId,token){
      var url=process.env.API_API_URL+'GetUserWiseTimeSheetData?contactIdFillFor='+contactId;
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+token
      },
      //body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
      console.log("responseJson GetUserWiseTimeSheetData");
      console.log(data);
      
      // debugger;
      if (data.responseType === "1") {
        // Profile & Contact

        if(data != null){
          if(data.data.timeSheetPeriodViews != null){
            this.Default_TimeSheet(data.data.timeSheetPeriodViews);
            var pwd = contactId+"Phss@123";
            var Period_str = CryptoAES.encrypt(JSON.stringify(data.data.timeSheetPeriodViews), pwd);
            localStorage.setItem('timeSheetPeriodViews', Period_str);
            //this.setState({ timeSheetPeriodViews: data.data.timeSheetPeriodViews });
            
          }
        }
      }
    })
    .catch(error => {
      //this.props.history.push("/error-500");
    });
    },
    GetDecodeStr: function(sessionName){
    //alert($sessionName);
    var pwd = localStorage.getItem("contactId")+"Phss@123";
    var Role_session = localStorage.getItem(sessionName);

    var _ciphertext = CryptoAES.decrypt(Role_session, pwd);
    var JsonCreate = JSON.parse(_ciphertext.toString(CryptoENC));
    return JsonCreate;
    },
    Default_TimeSheet: function(timeSheetPeriod){
      
      var length = timeSheetPeriod.length;
      //alert(length);
      if (length > 0) {
        var i = 1;
        for (var zz = 0; zz < length; zz++) {
          //alert(timeSheetPeriod[zz].isCurrentTimeSheet);
          if(timeSheetPeriod[zz].isCurrentTimeSheet == true){
            localStorage.setItem('CurrenttimeSheetPeriodId', timeSheetPeriod[zz].timeSheetPeriodId);
          }
        }
      }
    },

    TimeZone_DateTime: function(date)
    {
      //console.log('TimeZone_DateTime =========');
      //console.log(date);
      var timedifference =  MomentTimezone.tz.guess();
      
      var system_timezone = process.env.TIMEZONE;
      //console.log(system_timezone);
      var m = moment.tz(moment(date).format("YYYY-MM-DD HH:mm"), system_timezone);
      //console.log(m);
      //alert(m);
      //m.format();                     // 2013-11-18T11:55:00-05:00
      //m.startOf("day").format();      // 2013-11-18T00:00:00-05:00

      // console.log('timedifference');
      // console.log(timedifference);
      // console.log(system_timezone);



      if(timedifference == system_timezone){
        var user_date = date;

        //console.log('timedifference IF');
        //console.log(date);
      }else{
        var user_date= m.tz(timedifference).format(); // 2013-11-18T06:00:00+01:00
        //console.log('timedifference ELSE');
      }
      
      //m.startOf("day").format();
      //alert(user_date);
      //console.log('TimeZone_DateTime');
      //console.log(user_date);
      return moment(user_date).format(process.env.DATETIME_FORMAT);
      //return user_date;
    }
}

export default Systemhelpers;