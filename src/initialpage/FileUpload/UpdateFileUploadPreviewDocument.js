import React, { Component, Fragment } from 'react';
import ReactDOM from 'react-dom';
import { Upload } from '@progress/kendo-react-upload';
import '@progress/kendo-theme-default/dist/all.css';
import DocxImg from '../../assets/img/doc/docx.png'
import ExcelImg from '../../assets/img/doc/excel.png'
import PdfImg from '../../assets/img/doc/pdf.png'
import $ from 'jquery';
const fileStatuses = [
    'UploadFailed',
    'Initial',
    'Selected',
    'Uploading',
    'Uploaded',
    'RemoveFailed',
    'Removing'
];

class UpdateFileUploadPreview extends Component {
   
    constructor(props) {
        super(props);

        this.state = {
            
             files: [],
             events: [],
             UpdatefilePreviews: {},
             UpdatefilePreviewsFinal: []
        };
        this.setPropState = this.setPropState.bind(this);
    }

    componentDidMount() {
          console.log('componentDidMount');
          $('.EditDocumentremoveFunc').hide();
          //console.log(this.props.devevents);
          //console.log(this.props.devfiles);
          //this.setState({ files: this.props.devfiles });
          //this.setState({ events: this.props.devevents });
          //this.setState({ UpdatefilePreviews: this.props.UpdatefilePreviews });
          //this.setState({ UpdatefilePreviewsFinal: this.props.UpdatefilePreviewsFinal });
    }

    setPropState(key, value) {
        this.setState({ [key]: value });
    }

    base64MimeType(encoded) {
      var result = null;

      if (typeof encoded !== 'string') {
        return result;
      }

      var mime = encoded.match(/data:([a-zA-Z0-9]+\/[a-zA-Z0-9-.+]+).*,.*/);

      if (mime && mime.length) {
        result = mime[1];
      }

      return result;
    }

    
    onAdd = (event) => {
        console.log("pk");
        console.log(event.affectedFiles);


        /* pk Validation Add*/
        const file_pk = event.affectedFiles[0];
        const reader_pk = new FileReader();
        let image_width = 0;
        let image_height = 0;

        reader_pk.readAsDataURL(file_pk.getRawFile());

        reader_pk.onload = function (e) {
            var img = new Image;
            img.onload = function() {
                console.log("The width of the image is " + img.width + "px." + img.height + "px.");
                image_width = img.width;
                image_height = img.height;
                
                
                // if(img.width != 800 || img.height != 533){
                //     alert("please upload an image with 800x533 pixels dimension");
                //     return false;
                // }
                
            };
            img.src = reader_pk.result;
        };


        


        /* pk Validation Add*/
            var current_upload=this.state.UpdatefilePreviewsFinal.length;
            //var new_current_upload=event.affectedFiles.length;
            //var total_upload=current_upload+new_current_upload;
            var current_upload1=$("#filePreviewsFinalDocumentEditActive").val();
            var new_current_upload=event.affectedFiles.length;
            var total_upload=parseInt(current_upload1)+parseInt(current_upload)+parseInt(new_current_upload);
            

            if(total_upload > 6){
                alert("You can upload maximum 6 Files.");
                return false;
            }

            const files = new Map();
            
            const afterStateChange = () => {
                event.affectedFiles
                    .filter(file => !file.validationErrors)
                    .forEach(file => {
                        const reader = new FileReader();
                        files.set(reader, file);
                        
                        reader.onloadend = (ev) => {

                            let ArrayJson = {
                                FileData: ev.target.result,
                                FileName: file.name,
                            };
                            
                            this.setState({
                                UpdatefilePreviews: {
                                    ...this.state.UpdatefilePreviews,
                                    [file.uid]: ArrayJson
                                }
                            });
                        };


                        reader.readAsDataURL(file.getRawFile());

                        // reader.onload = function (e) {
                        //     var img = new Image;
                        //     img.onload = function() {
                        //       console.log("The width of the image is " + img.width + "px.");
                        //     };
                        //     img.src = reader.result;
                        // };
                    });
                   
            };

            this.setState({
                files: event.newState,
                events: [
                    ...this.state.events,
                    `File selected: ${event.affectedFiles[0].name}`
                ]
            }, afterStateChange);

        

    }

    onRemove = (event) => {
        const UpdatefilePreviews = {
            ...this.state.UpdatefilePreviews
        };

        event.affectedFiles.forEach(file => {
            delete UpdatefilePreviews[file.uid];
        });

        this.setState({
            files: event.newState,
            events: [
                ...this.state.events,
                `File removed: ${event.affectedFiles[0].name}`
            ],
            UpdatefilePreviews: UpdatefilePreviews
        });

        /* ***  pk code **** */
        let temp = [];
        Object.keys(this.state.UpdatefilePreviews).map((fileKey) => (
            temp.push({'FileData':this.state.UpdatefilePreviews[fileKey].FileData,'FileName':this.state.UpdatefilePreviews[fileKey].FileName}) 
        ))
        this.setState({ UpdatefilePreviewsFinal: temp })
        this.props.setPropState('UpdatefilePreviewsFinal', temp);
        /* ***  pk code **** */
    }

    onProgress = (event) => {
        this.setState({
            files: event.newState,
            events: [
                ...this.state.events,
                `On Progress: ${event.affectedFiles[0].progress} %`
            ]
        });
    }

    onStatusChange = (event) => {
        
        const file = event.affectedFiles[0];

        /* ***  pk code **** */
        let temp = [];
        Object.keys(this.state.UpdatefilePreviews).map((fileKey) => (
            temp.push({'FileData':this.state.UpdatefilePreviews[fileKey].FileData,'FileName':this.state.UpdatefilePreviews[fileKey].FileName})   
        ))
        this.setState({ UpdatefilePreviewsFinal: temp })
        this.props.setPropState('UpdatefilePreviewsFinal', temp);
        /* ***  pk code **** */
        this.setState({
            files: event.newState,
            events: [
                ...this.state.events,
                `File '${file.name}' status changed to: ${fileStatuses[file.status]}`
            ]
        });
    }

    randomFun()
    {
        var min = 100;
        var max = 999;
        return Math.floor(Math.random()*(max-min+1)+min);
    }
    GetFileName(baseurl){
        var imgbase=baseurl;
        var gettype=this.base64MimeType(baseurl);

        var ext='';
        
        var today = new Date();
        var y = today.getFullYear();
        var m = today.getMonth() + 1;
        var d = today.getDate();
        var h = today.getHours();
        var mi = today.getMinutes();
        var s = today.getSeconds();
        var ms = today.getMilliseconds();
        var time = "PHSS000"+y  + m  + d  + h  + mi  + s + ms+this.randomFun();
        if(gettype == 'image/png'){
            ext='.png';
        }else if(gettype == 'image/jpeg'){
            ext='.jpg';
        }else if(gettype == 'image/jpg'){
            ext='.jpg';
        }else if(gettype == 'application/vnd.ms-excel'){
            ext='.csv';
        }else if(gettype == 'application/pdf'){
            ext='.pdf';
        }else if(gettype == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'){
            ext='.xlsx';
        }else if(gettype == 'application/vnd.ms-excel'){
            ext='.xls';
        }else if(gettype == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'){
            ext='.docx';
        }else if(gettype == 'application/msword'){
            ext='.doc';
        }

        var file_name=time+ext

        
        return file_name;
    }

    GetImageAll(baseurl){
        var imgbase=baseurl;
        var gettype=this.base64MimeType(baseurl);

        let Image_return = [];

        if(gettype == 'image/png' || gettype == 'image/jpeg' || gettype == 'image/jpg' ){
            Image_return.push(<img src={baseurl} alt={'image preview'} style={{ width: 100, height:100, margin: 10 }} />);
        } else if(gettype == 'application/pdf'){
            Image_return.push(<img src={PdfImg} alt={'image preview'} style={{ width: 100, height:100, margin: 10 }} />);
        } else if(gettype == 'application/vnd.ms-excel'){
            Image_return.push(<img src={ExcelImg} alt={'image preview'} style={{ width: 100, height:100, margin: 10 }} />);
        } else if(gettype == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'){
            Image_return.push(<img src={ExcelImg} alt={'image preview'} style={{ width: 100, height:100, margin: 10 }} />);
        } else if(gettype == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' || gettype == 'application/msword'){
            Image_return.push(<img src={DocxImg} alt={'image preview'} style={{ width: 100, height:100, margin: 10 }} />);
        }

        return Image_return;
    }

    resetFile(){
        if(this.props.EditAwardResetFileMethod == true ){
            $( ".EditDocumentremoveFunc" ).trigger( "click" );
        }
    }

    removeFunc = () => e => {
        e.preventDefault();
        
        this.setState({ files: [] });
        this.setState({ events: [] });
        this.setState({ UpdatefilePreviews: {} });
        this.setState({ UpdatefilePreviewsFinal: [] });
        console.log('resetFile');
    
        return false;
    }



    render() {
    
    return (

       <div>
                <Upload
                    batch={false}
                    //autoUpload={false}
                    multiple={true}
                    restrictions={{
                        maxFileSize: 10000000,
                        allowedExtensions: [ '.jpeg','.jpg', '.png','.pdf','.xls','.xlsx','.csv','.docx','.doc']
                    }}
                    files={this.state.files}
                    onAdd={this.onAdd}
                    onRemove={this.onRemove}
                    onProgress={this.onProgress}
                    onStatusChange={this.onStatusChange}
                    withCredentials={false}
                    saveUrl={'https://demos.telerik.com/kendo-ui/service-v4/upload/save'}
                    removeUrl={'https://demos.telerik.com/kendo-ui/service-v4/upload/remove'}
                />
                 <div className={'example-config'} style={{ marginTop: 20, display: 'none' }}>
                    <ul className={'event-log'}>
                        {
                            this.state.events.map(event => <li>{event}</li>)
                        }
                    </ul>
                </div>
                {
                    this.state.files.length ?
                    <div className={'img-preview example-config'}>
                        <h3>Preview Files</h3>
                        {   

                            Object.keys(this.state.UpdatefilePreviews)
                                .map((fileKey) => (
                                    this.GetImageAll(this.state.UpdatefilePreviews[fileKey])
                                    ))
                        }
                    </div> : undefined
                }

                {this.props.ResetFileMethod == true ?
                    this.resetFile()
                    : null
                }
                <button type="hidden" class="EditDocumentremoveFunc" onClick={this.removeFunc()} >Submit</button>
            </div>
    );
  }
}
export default UpdateFileUploadPreview;
