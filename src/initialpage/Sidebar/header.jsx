/**
 * App Header
 */
import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import {ProfileImage,headerlogo,lnEnglish,lnFrench,lnSpanish,lnGerman, Avatar_02,Avatar_03,Avatar_05,
  Avatar_06,Avatar_08,Avatar_09,Avatar_13,Avatar_17,Avatar_21,ProfileImageFemale} from '../../Entryfile/imagepath'

import Loader from '../Loader';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import SystemHelpers from '../Helpers/SystemHelper';

import SessionTimeout from "../SessionTimeout";

import moment from 'moment';

class Header extends Component {
  constructor(props) {
      super(props);

      this.state = {
          loading: false,
          preferredName:'',

          ListGrid: [],
          ListGridNotificationsCount : 0,
          headerisProfileImageShow : false
      };
      
  }


  // Loader hide show method
  hideLoader = () => {
    this.setState({ loading: false });
  }

  showLoader = () => {
    this.setState({ loading: true });
  }
  // Loader hide show method

  
  componentDidMount() {
    if (localStorage.getItem("token") == null || localStorage.token == 'undefined') {
      this.props.history.push("/");
    }
    this.GetProfile();

    this.GetUserWiseNotificationListForAlert();
    //console.log("token");
    //console.log(localStorage.getItem("token"));

    
    $( "#mobile_btn" ).click(function() {
        var $wrapper = $('.main-wrapper');
        $wrapper.toggleClass('slide-nav');
        $('.sidebar-overlay').toggleClass('opened');
        $('html').addClass('menu-opened');
        $('#task_window').removeClass('opened');
        return false;
    });

    $(".sidebar-overlay").on("click", function () {
        var $wrapper = $('.main-wrapper');
        $('html').removeClass('menu-opened');
        $(this).removeClass('opened');
        $wrapper.removeClass('slide-nav');
        $('.sidebar-overlay').removeClass('opened');
        $('#task_window').removeClass('opened');
    });

  }

  push_func(){
    this.props.history.push("/");
    this.hideLoader();
  }

  LogoutAuth = () => e => {
    this.showLoader();
    SystemHelpers.SessionOut();
    this.props.history.push("/login");
    SystemHelpers.ToastSuccess('logout successfully');
    this.hideLoader();
    return null;
  }

  GetProfile(){
    //console.log(localStorage.getItem("token"));
    this.showLoader();
    var url=process.env.API_API_URL+'GetUserBasicInfoById?contactId='+localStorage.getItem("contactId");
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
      //body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
        console.log("responseJson GetUserBasicInfoById header");
        console.log(data);
        //console.log(data.data.userRole);
        // debugger;
        
         
        if (data.responseType === "1") {
            //this.setState({ preferredName: data.data.preferredName});
            localStorage.setItem('preferredName', data.data.preferredName);
            this.setState({ userGender: data.data.gender});
            

            if(data.data.photoAuthorization == null )
            {
              if(data.data.gender == "Female")
              {
                localStorage.setItem('Headerprofileimage', ProfileImageFemale);
              }
              else
              {
                localStorage.setItem('Headerprofileimage', ProfileImage);
              }
              
            }else{
              localStorage.setItem('Headerprofileimage', data.data.photoAuthorization);
            }
            localStorage.setItem('headerisProfileImageShow', data.data.photoRequired);

        }else{
              if(data.message == 'Authorization has been denied for this request.'){
                SystemHelpers.SessionOut();
              }else{
                SystemHelpers.ToastError(data.message);
              }
              
        }
        this.hideLoader();
        
    })
    .catch(error => {
      this.props.history.push("/error-500");
    });
  }

  GetUserWiseNotificationListForAlert(){

      this.setState({ ListGrid: [] }) 
      //this.showLoader();
      var url=process.env.API_API_URL+'GetUserWiseNotificationListForAlert'+'?contactId='+localStorage.getItem("contactId");
      fetch(url, {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'bearer '+localStorage.getItem("token")
        },
      }).then((response) => response.json())
      .then(data => {
          console.log("responseJson GetUserWiseNotificationListForAlert header");
          console.log(data);
          if (data.responseType === "1") {
              this.setState({ ListGrid: this.rowData(data.data) });
              
              if(data.dataCount != null)
              {
                //this.setState({ ListGridNotificationsCount: data.dataCount });
                localStorage.setItem('ListGridNotificationsCount', data.dataCount);
              }
              
              //this.hideLoader();
          }else{
            if(data.message == 'Authorization has been denied for this request.'){
              SystemHelpers.SessionOut();
              this.props.history.push("/login");
            }else{
              SystemHelpers.ToastError(data.message);
            }
            //this.hideLoader();    
          }
          
          
      })
      .catch(error => {
        console.log("responseJson GetUserWiseNotificationListForAlert catch error");
        console.log(error);
        this.props.history.push("/error-500");
      });
  }

  rowData(ListGrid) {
      console.log(ListGrid)
      
      var ListGrid_length = ListGrid.length;
      let dataArray = [];
      var i=1;
      for (var z = 0; z < ListGrid_length; z++) {
        var tempdataArray = [];
        //tempdataArray.rownum = i;

        //if(ListGrid[z].isRead == false){
          tempdataArray.createdOn = ListGrid[z].createdOn;
          tempdataArray.isRead = ListGrid[z].isRead;
          tempdataArray.notificationText = ListGrid[z].notificationText;
          tempdataArray.notificationType = ListGrid[z].notificationType;
          tempdataArray.notificationTitle = ListGrid[z].notificationTitle;
          
          
          dataArray.push(tempdataArray);
          i++;
        //}
      }
      //console.log('notification row fun');
      //console.log(tempdataArray);
      return dataArray;
  }

  // notification read status
  PostUserNotificationReadStatus = e => {
    //alert();
    e.preventDefault();
      //this.showLoader();

      var notification_Id = '';
      var url=process.env.API_API_URL+'PostUserNotificationReadStatus'+'?contactId='+localStorage.getItem("contactId")+'&notificationId='+notification_Id;
      fetch(url, {
        method: 'PUT',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'bearer '+localStorage.getItem("token")
        },
      }).then((response) => response.json())
      .then(data => {
          console.log("responseJson PostUserNotificationReadStatus");
          console.log(data);
          if (data.responseType === "1") {
              /*setTimeout(function() {
                location.reload();
              }, 5000);*/
              //this.hideLoader();
              //this.setState({ ListGridNotificationsCount: 0 });
              //alert();
              localStorage.setItem('ListGridNotificationsCount', 0);
              //this.GetUserWiseNotificationListForAlert();
              
          }else{
            if(data.message == 'Authorization has been denied for this request.'){
              SystemHelpers.SessionOut();
              this.props.history.push("/login");
            }else{
              SystemHelpers.ToastError(data.message);
            }
            //this.hideLoader();    
          }
          
          
      })
      .catch(error => {
        console.log("responseJson PostUserNotificationReadStatus catch error");
        console.log(error);
        this.props.history.push("/error-500");
      });
  }
  // notification read status

   render() {
    const {  location } = this.props
    let pathname = location.pathname
    
      return (
      
          

         <div className="header" style={{right:"0px"}}>
         <SessionTimeout />
         <ToastContainer position="top-right"
              autoClose={process.env.API_TOAST_TIMEOUT}
              hideProgressBar={false}
              newestOnTop={false}
              closeOnClick={false}
              rtl={false}
              pauseOnVisibilityChange={false}
              draggable={false}
              pauseOnHover={false} />
          {(this.state.loading) ? <Loader /> : null} {/* Loder method use */}
           
         
        {/* Logo */}
        <div className="header-left">
          <a href="/dashboard" className="logo">
            <img src={headerlogo} width={40} height={40} alt="" />
          </a>
        </div>
        {/* /Logo */}
        <a id="toggle_btn" href="" style={{display: pathname.includes('tasks') ?"none" :pathname.includes('compose') ? "none" :""}}>
          <span className="bar-icon"><span />
            <span />
            <span />
          </span>
        </a>
        {/* Header Title */}
        <div className="page-title-box">
          <h3></h3>
        </div>
        {/* /Header Title */}
        <a id="mobile_btn" className="mobile_btn" href="#sidebar"><i className="fa fa-bars" /></a>
        {/* Header Menu */}
        <ul className="nav user-menu">
          {/* Search */}
          {/*
            <li className="nav-item">
              <div className="top-nav-search">
                <a href="" className="responsive-search">
                  <i className="fa fa-search" />
                </a>
                <form action="/app/pages/search">
                  <input className="form-control" type="text" placeholder="Search here" />
                  <button className="btn" type="submit"><i className="fa fa-search" /></button>
                </form>
              </div>
            </li>
          */}
          {/* /Search */}
          {/* Flag */}
          {/*
            <li className="nav-item dropdown has-arrow flag-nav">
              <a className="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button">
                <img src={lnEnglish} alt="" height={20} /> <span>English</span>
              </a>
              <div className="dropdown-menu dropdown-menu-right">
              <a href="" className="dropdown-item">
                      <img src={lnEnglish} alt="" height={16} /> English
                    </a>
                    <a href="" className="dropdown-item">
                      <img src={lnFrench} alt="" height={16} /> French
                    </a>
                    <a href="" className="dropdown-item">
                      <img src={lnSpanish} alt="" height={16} /> Spanish
                    </a>
                    <a href="" className="dropdown-item">
                      <img src={lnGerman} alt="" height={16} /> German
                    </a>
              </div>
            </li>
          */}
          {/* /Flag */}
          {/* Notifications */}
          <li className="nav-item dropdown">
            <a href="#" className="dropdown-toggle nav-link" data-toggle="dropdown"   onClick={this.PostUserNotificationReadStatus}>
              <i className="fa fa-comment-o" /> <span className="badge badge-pill">{localStorage.getItem('ListGridNotificationsCount')}</span>
            </a>
            <div className="dropdown-menu notifications">
              <div className="topnav-dropdown-header">
                <span className="notification-title">Notifications</span>
                {/*<a href="" className="clear-noti"> Clear All </a>*/}
              </div>
              <div className="noti-content">
                <ul className="notification-list">

                  {this.state.ListGrid.map(( listValue, index ) => {
                    return (<li className="notification-message">
                        <a href="#">
                          <div className="media">
                            <span className="avatar">
                              {this.state.userGender == "Female" ?
                                <img alt="" src={ProfileImageFemale} /> :
                                <img alt="" src={ProfileImage} />
                              }
                            </span>
                            <div className="media-body">
                              <p className="noti-details  notification-color"><span className="noti-title notification-color"></span> {listValue.notificationTitle}</p>
                              <p className="noti-time"><span className="notification-time  notification-color">{moment(listValue.createdOn).format("YYYY-MM-DD HH:mm A")}</span></p>
                            </div>
                          </div>
                        </a>
                      </li>);
                    })
                  }
                  
                </ul>
              </div>
              <div className="topnav-dropdown-footer">
                <a href="/notifications">View all Notifications</a>
              </div>
            </div>
          </li>
          {/* /Notifications */}
          {/* Message Notifications */}
          <li className="nav-item dropdown">
            <a href="#" className="dropdown-toggle nav-link" data-toggle="dropdown">
              <i className="fa fa-bell-o" /> <span className="badge badge-pill">8</span>
            </a>
            <div className="dropdown-menu notifications">
              <div className="topnav-dropdown-header">
                <span className="notification-title">Messages</span>
                <a href="" className="clear-noti"> Clear All </a>
              </div>
              <div className="noti-content">
                <ul className="notification-list">
                  <li className="notification-message">
                    <a href="#">
                      <div className="list-item">
                        <div className="list-left">
                          <span className="avatar">
                            <img alt="" src={Avatar_09} />
                          </span>
                        </div>
                        <div className="list-body">
                          <span className="message-author">Richard Miles </span>
                          <span className="message-time">12:28 AM</span>
                          <div className="clearfix" />
                          <span className="message-content">Lorem ipsum dolor sit amet, consectetur adipiscing</span>
                        </div>
                      </div>
                    </a>
                  </li>
                  <li className="notification-message">
                    <a href="#">
                      <div className="list-item">
                        <div className="list-left">
                          <span className="avatar">
                            <img alt="" src={Avatar_02} />
                          </span>
                        </div>
                        <div className="list-body">
                          <span className="message-author">John Doe</span>
                          <span className="message-time">6 Mar</span>
                          <div className="clearfix" />
                          <span className="message-content">Lorem ipsum dolor sit amet, consectetur adipiscing</span>
                        </div>
                      </div>
                    </a>
                  </li>
                  <li className="notification-message">
                    <a href="#">
                      <div className="list-item">
                        <div className="list-left">
                          <span className="avatar">
                            <img alt="" src={Avatar_03} />
                          </span>
                        </div>
                        <div className="list-body">
                          <span className="message-author"> Tarah Shropshire </span>
                          <span className="message-time">5 Mar</span>
                          <div className="clearfix" />
                          <span className="message-content">Lorem ipsum dolor sit amet, consectetur adipiscing</span>
                        </div>
                      </div>
                    </a>
                  </li>
                  <li className="notification-message">
                    <a href="#">
                      <div className="list-item">
                        <div className="list-left">
                          <span className="avatar">
                            <img alt="" src={Avatar_05} />
                          </span>
                        </div>
                        <div className="list-body">
                          <span className="message-author">Mike Litorus</span>
                          <span className="message-time">3 Mar</span>
                          <div className="clearfix" />
                          <span className="message-content">Lorem ipsum dolor sit amet, consectetur adipiscing</span>
                        </div>
                      </div>
                    </a>
                  </li>
                  <li className="notification-message">
                    <a href="#">
                      <div className="list-item">
                        <div className="list-left">
                          <span className="avatar">
                            <img alt="" src={Avatar_08} />
                          </span>
                        </div>
                        <div className="list-body">
                          <span className="message-author"> Catherine Manseau </span>
                          <span className="message-time">27 Feb</span>
                          <div className="clearfix" />
                          <span className="message-content">Lorem ipsum dolor sit amet, consectetur adipiscing</span>
                        </div>
                      </div>
                    </a>
                  </li>
                </ul>
              </div>
              <div className="topnav-dropdown-footer">
                <a href="#">View all Messages</a>
              </div>
            </div>
          </li>
          {/* /Message Notifications */}
          <li className="nav-item dropdown has-arrow main-drop">
            <a href="#" className="dropdown-toggle nav-link" data-toggle="dropdown">
              <span className="user-img">
                {localStorage.getItem('headerisProfileImageShow') == true ?
                  <img src={localStorage.getItem('Headerprofileimage')} alt="" />:
                  <img src={localStorage.getItem('Headerprofileimage')} alt="" />
                }
                <span className="status online" /></span>
              <span> Welcome, {localStorage.getItem("preferredName")}</span>
            </a>
            <div className="dropdown-menu">
              <a className="dropdown-item" href="/staff-profile">My Profile</a>
              <a className="dropdown-item" href="/settings">Settings</a>
              <a className="dropdown-item" href="/change-password">Change Password</a>
              <a className="dropdown-item" href="#" onClick={this.LogoutAuth()} >Logout</a>
            </div>
          </li>
        </ul>
        {/* /Header Menu */}
        {/* Mobile Menu */}
        <div className="dropdown mobile-user-menu">
          <a href="#" className="nav-link dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i className="fa fa-ellipsis-v" /></a>
          <div className="dropdown-menu dropdown-menu-right">
            <a className="dropdown-item" href="/staff-profile">My Profile</a>
            <a className="dropdown-item" href="/settings">Settings</a>
            <a className="dropdown-item" href="/change-password">Change Password</a>
            <a className="dropdown-item" href="#" onClick={this.LogoutAuth()}>Logout</a>
          </div>
        </div>
        {/* /Mobile Menu */}
      </div>
    
      );
   }
}

export default withRouter(Header);