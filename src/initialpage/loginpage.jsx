/**
 * Signin Firebase
 */

import React, { Component } from 'react';
import { Helmet } from "react-helmet";
import {headerlogo,Applogo,ProfileImageFemale,ProfileImage} from '../Entryfile/imagepath.jsx'
import PasswordMask from 'react-password-mask';
import InputMask from 'react-input-mask';


import SystemHelpers from './Helpers/SystemHelper';

import Loader from './Loader';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import CryptoAES from 'crypto-js/aes';
import CryptoENC from 'crypto-js/enc-utf8';

import uniqueString from 'unique-string';



class Loginpage extends Component {
    constructor(props) {
      super(props);

      this.state = {
          questions: [],
          password : '',
          errormsg :  '',
          step : 1,
          setuserName : '',
          setMobileNumber : '',
          setPassword : '',
          setConfirmPassword : '',
          setQue1  : 0,
          setQue2 : 0,
          setQue3 : 0,
          setAns1 : '',
          setAns2 : '',
          setAns3 : '',
          machineId : ''

      };
      this.handleChange = this.handleChange.bind(this)
    }
   

  // Input box Type method
  handleChange = input => e => {
    this.setState({ [input]: e.target.value });

    if (this.state[input] != '') {
        delete this.state.errormsg[input];
    }
    
  }

  componentDidMount(){

      // if (window.location.protocol == 'http:') {
      //     console.log("you are accessing us via "+  "an insecure protocol (HTTP). "+ "Redirecting you to HTTPS.");
      //     window.location.href = window.location.href.replace('http:', 'https:');
      // }

      if (location.pathname.includes("login") || location.pathname.includes("register") || location.pathname.includes("forgotpassword")
      || location.pathname.includes("resetpassword") || location.pathname.includes("mobilesubmit") || location.pathname.includes("otp")|| location.pathname.includes("lockscreen") ) {
          $('body').addClass('account-page');
      }else if (location.pathname.includes("error-404") || location.pathname.includes("error-500") ) {
          $('body').addClass('error-page');
      }

      if (localStorage.getItem("token") != null) {
        this.props.history.push("/dashboard");
      }

      if (localStorage.getItem("temp_registration_default_userName") != null) {
        //alert(localStorage.getItem("temp_registration_default_userName"));
        this.setState({ userName: localStorage.getItem("temp_registration_default_userName") });
      }

      
      

      if (localStorage.getItem('machineId') != null)
      {
        var local_machine_id = localStorage.getItem('machineId');
        var pwd = "Phss@123";
        var decrypt_machineId = CryptoAES.decrypt(local_machine_id, pwd);
        this.setState({ machineId: decrypt_machineId.toString(CryptoENC) });
      }
      else
      {
        var unique_string = uniqueString();

        this.setState({ machineId: unique_string });
      }
      
      localStorage.setItem('Headerprofileimage', ProfileImage);
  }

  onEnterPress = (e) => {
    if(e.keyCode == 13 && e.shiftKey == false) {
      e.preventDefault();
      $( "#login_btn" ).trigger( "click" );
    }
  }
      
  // Loader hide show method
  hideLoader = () => {
    this.setState({ loading: false });
  }

  showLoader = () => {
    this.setState({ loading: true });
  }
  // Loader hide show method

  
  
  Setuser = () => e => {    
      //debugger; 
      e.preventDefault();
      
      let step1Errors = {};
      if (this.state["setuserName"] === '') {
        step1Errors["setuserName"] = "Please Enter User Name";
      }else{
        var regex_user = /^(?=[a-zA-Z0-9._]{8,20}$)(?!.*[_.]{2})[^_.].*[^_.]$/
        if (!(regex_user.test(this.state["setuserName"]))) {
          step1Errors["setuserName"] = "Username should be 8-20 characters long , no _ or . at the end, no __ or _. or ._ or .. inside, no _ or . at the beginning";
        }
      }

      if (this.state["setQue1"] === 0) {
        step1Errors["setQue1"] = "Please Select Security Question#1";
      }

      var setAns1_strlen = this.state["setAns1"].length;
      if (this.state["setAns1"] === '') {
        step1Errors["setAns1"] = "Please Enter Answer #1";
      }else if (setAns1_strlen< 6 || setAns1_strlen > 32){
        step1Errors["setAns1"] = "Your Answer #1 must atleast be 6 characters.";
      }

      if (this.state["setQue2"] === 0) {
        step1Errors["setQue2"] = "Please Select Security Question#2";
      }

      var setAns2_strlen = this.state["setAns2"].length;
      if (this.state["setAns2"] === '') {
        step1Errors["setAns2"] = "Please Enter Answer #2";
      }else if (setAns2_strlen< 6 || setAns2_strlen > 32){
        step1Errors["setAns2"] = "Your Answer #1 must atleast be 6 characters.";
      }

      if (this.state["setQue3"] === 0) {
        step1Errors["setQue3"] = "Please Select Security Question#3";
      }

      var setAns3_strlen = this.state["setAns3"].length;
      if (this.state["setAns3"] === '') {
        step1Errors["setAns3"] = "Please Enter Answer #3";
      }else if (setAns3_strlen< 6 || setAns3_strlen > 32){
        step1Errors["setAns3"] = "Your Answer #1 must atleast be 6 characters.";
      }


      if (this.state["setConfirmPassword"] === '') {
        step1Errors["setConfirmPassword"] = "Please Enter Password";
      }else if (this.state["setPassword"] !== this.state["setConfirmPassword"]) {
          step1Errors["setConfirmPassword"] = "Confirm Password doesn't match"
      }

      if (this.state["setPassword"] == '') {
          step1Errors["setPassword"] = "Password is mandatory"
      }else {
          var regex = /^(?=.*\d)(?=.*[A-Z])(?=.*[a-z])(?=.*[a-zA-Z!#$%&?@ "])[a-zA-Z0-9!#$%&?@]{8,20}$/
          if (!(regex.test(this.state["setPassword"]))) {
            step1Errors["setPassword"] = "Password should contain one upper, lower, special char and min 8 char";
          }
      }

      var MobileNumber =this.state["setMobileNumber"];
      var splitmobile = MobileNumber.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/ ]/gi, '');
      if(MobileNumber.length > 0 && splitmobile.length != 10) {
        step1Errors["setMobileNumber"] = "Please Enter a Valid Phone Number.";
      }

      if(this.state["setQue1"] == this.state["setQue2"])
      {
        step1Errors["setQue2"] = "You must select different a Security Question.";
      }else if(this.state["setQue2"] == this.state["setQue3"]){
        step1Errors["setQue3"] = "You must select different a Security Question.";
      }else if(this.state["setQue1"] == this.state["setQue3"]){
        step1Errors["setQue3"] = "You must select different a Security Question.";
      }
      //console.log(splitmobile.length);
      //console.log(MobileNumber.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/ ]/gi, ''));
      this.setState({ errormsg: step1Errors });
        
      if (Object.keys(step1Errors).length > 0) {
          return false;
      }
      

      

      let Que_Ans_Json = [
            {
                securityQuestionId: this.state["setQue1"],
                securityQuestion: this.state["setAns1"],
                securityQuestionAns: this.state["setAns"]
            },
            {
                securityQuestionId: this.state["setQue2"],
                securityQuestion: this.state["setAns2"],
                securityQuestionAns: this.state["setAns2"]
            },
            {
                securityQuestionId: this.state["setQue3"],
                securityQuestion: this.state["setAns3"],
                securityQuestionAns: this.state["setAns3"]
            }
        ];
     
      let bodyarray = {};
      bodyarray["userName"] = this.state["setuserName"];
      bodyarray["password"] = this.state["setPassword"];
      bodyarray["contactId"] = localStorage.getItem("temp_login_id");
      bodyarray["mobileNo"] = this.state["setMobileNumber"];
      bodyarray["securityQuestionView"] = Que_Ans_Json;
      
      //console.log(bodyarray);
      //return false;
      this.showLoader();
      var url=process.env.API_API_URL+'SetUsernamePasswordAndSecurityQuestion';
      fetch(url, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'bearer '+localStorage.getItem("temp_login_token")
        },
        body: JSON.stringify(bodyarray)
      }).then((response) => response.json())
      .then(data => {
          console.log("responseJson SetUsernamePasswordAndSecurityQuestion");
          console.log(data);
          return false;
          if (data.responseType === "1") {
              if(data.redirectUrl == '/login'){
                  SystemHelpers.ToastSuccess(data.responseMessge);
                  localStorage.removeItem('temp_login_token');
                  localStorage.removeItem('temp_login_id');
                  localStorage.removeItem('temp_login_UserName');
                  localStorage.removeItem('temp_login_Password');
                  setTimeout(function() {
                      location.reload();
                  }, 5000);
              }
          }else{
            SystemHelpers.ToastError(data.responseMessge);
          }
          this.hideLoader();
          
      })
      .catch(error => {
        this.props.history.push("/error-500");
      });
  }

  loginclick = (password) => e => {  
      e.preventDefault();  
      //debugger;
      //console.log('login machineId');
      //console.log(this.state.machineId)
      //console.log('=======');
      let step1Errors = {};
      if (this.state["userName"] === '') {
        step1Errors["userName"] = "Please Enter User Name"
      }
      if (this.state["password"] === '') {
        step1Errors["password"] = "Please Enter Password"
      }

      this.setState({ errormsg: step1Errors });
        
      if (Object.keys(step1Errors).length > 0) {
          return false;
      }

      this.showLoader();
     
      let bodyarray = {};
      bodyarray["UserName"] = this.state["userName"];
      bodyarray["Password"] = this.state["password"];

      bodyarray["ipAddress"] = this.state["machineId"];
      bodyarray["deviceToken"] = this.state["machineId"];

      var url=process.env.API_API_URL+'DoLogin';
      fetch(url, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(bodyarray)
      }).then((response) => response.json())
      .then(data => {
          console.log("responseJson DoLogin");
          console.log(data);
          //console.log(responseJson);
          // debugger;
          if (data.responseType === "1") {
              localStorage.setItem("temp_login_token", data.token);
              localStorage.setItem("temp_login_id", data.data.id);
              localStorage.setItem("temp_login_UserName", data.data.emailId);
              localStorage.setItem("temp_login_Password", this.state["password"]);

              localStorage.removeItem('temp_registration_default_userName');

              var pwd = "Phss@123";
              var machineId_str = CryptoAES.encrypt(this.state.machineId, pwd);
              localStorage.setItem('machineId', machineId_str);

              //SystemHelpers.FetchRole(data.data.id,data.token)
              //this.FetchRole(data.data.id,data.token,data.redirectUrl);
              SystemHelpers.FetchRoleJson(data.data.userRole,data.data.id);
              SystemHelpers.GetUserWiseTimeSheetData(data.data.id,data.token);
              //this.props.history.push('/dashboard');
              if(data.redirectUrl == '/setusername')
              {
                this.setState({step: 2});
                this.setState({ questions: data.data.securityQuestionView })
                //SystemHelpers.ToastSuccess(data.responseMessge);
              }else if(data.redirectUrl == '/dashboard'){
                


                localStorage.removeItem('temp_login_token');
                localStorage.removeItem('temp_login_id');
                localStorage.removeItem('temp_login_UserName');
                localStorage.removeItem('temp_login_Password');

                localStorage.setItem("contactId", data.data.id);
                localStorage.setItem("eMailAddress", data.data.emailId);
                localStorage.setItem("fullName", data.data.fullName);
                //localStorage.setItem("userRole", data.data.userRole);
                localStorage.setItem("token", data.token);

                localStorage.setItem("primaryLocationId", data.data.userBasicInfo.primaryLocationId);
                localStorage.setItem("isPayrollAdmin", data.data.userBasicInfo.isPayrollAdmin);

                var pwd = data.data.id+"Phss@123";
                var Period_str = CryptoAES.encrypt(JSON.stringify(data.data.timeSheetPeriodViews), pwd);
                localStorage.setItem('timeSheetPeriodViews', Period_str);

                localStorage.setItem("userGender", data.data.userBasicInfo.gender);

                localStorage.setItem("headerisProfileImageShow", data.data.userBasicInfo.photoRequired);
                localStorage.setItem("Headerprofileimage", data.data.userBasicInfo.photoAuthorization);

                // User Type store session
                let UserTypeSession = {};
                UserTypeSession.isCordinator=data.data.userBasicInfo.isCordinator;
                UserTypeSession.isPayrollAdmin=data.data.userBasicInfo.isPayrollAdmin;
                UserTypeSession.isSeniorCordinator=data.data.userBasicInfo.isSeniorCordinator;
                UserTypeSession.isServiceCoordinatorLead=data.data.userBasicInfo.isServiceCoordinatorLead;

                console.log('UserTypeSession');
                console.log(UserTypeSession);

                var pwd = data.data.id+"Phss@123";
                var UserType_str = CryptoAES.encrypt(JSON.stringify(UserTypeSession), pwd);
                localStorage.setItem('usertypesession', UserType_str);
                // User Type store session
                
                localStorage.setItem("primaryLocationGuid", data.data.userBasicInfo.locationIdGuid);

                
                localStorage.setItem("employmentHoursName", data.data.userBasicInfo.employmentHours);
                localStorage.setItem("employmentStatusName", data.data.userBasicInfo.employmentStatus);
                
                localStorage.setItem("primaryLocationName", data.data.userBasicInfo.primaryLocation);

                this.props.history.push(data.redirectUrl);
                

              }
              else
              {

                if(data.redirectUrl == '/OTPVerification'){
                  

                  localStorage.setItem("primaryLocationId", data.data.userBasicInfo.primaryLocationId);
                  localStorage.setItem("isPayrollAdmin", data.data.userBasicInfo.isPayrollAdmin);

                  var pwd = data.data.id+"Phss@123";
                  var Period_str = CryptoAES.encrypt(JSON.stringify(data.data.timeSheetPeriodViews), pwd);
                  localStorage.setItem('timeSheetPeriodViews', Period_str);


                  // User Type store session
                  let UserTypeSession = {};
                  UserTypeSession.isCordinator=data.data.userBasicInfo.isCordinator;
                  UserTypeSession.isPayrollAdmin=data.data.userBasicInfo.isPayrollAdmin;
                  UserTypeSession.isSeniorCordinator=data.data.userBasicInfo.isSeniorCordinator;
                  UserTypeSession.isServiceCoordinatorLead=data.data.userBasicInfo.isServiceCoordinatorLead;

                  console.log('UserTypeSession');
                  console.log(UserTypeSession);

                  var pwd = data.data.id+"Phss@123";
                  var UserType_str = CryptoAES.encrypt(JSON.stringify(UserTypeSession), pwd);
                  localStorage.setItem('usertypesession', UserType_str);
                  // User Type store session
                }
                
                this.props.history.push(data.redirectUrl);
              }
              
          }
          else if (data.responseType === "2") {
              SystemHelpers.ToastError(data.responseMessge);
          }
          this.hideLoader();
          
      })
      .catch(error => {
        console.log('DoLogin');
        console.log(error);
        this.props.history.push("/error-500");
      });
  }

  step_function(){
    let div_return = [];
    const { step } = this.state;
    const { userName, password, setuserName, setMobileNumber, setPassword, setConfirmPassword, setQue1 , setQue2, setQue3, setAns1, setAns2, setAns3 } = this.props;
    if(step == 1){
      div_return.push(
        <div className="account-content">
          
          <div className="container">
            {/* Account Logo */}
            <div className="account-logo">
              <a href="/login"><img src={Applogo} className="pk-logo" alt="PARTICIPATION HOUSE SUPPORT SERVICES" /></a>
            </div>
            {/* /Account Logo */}
            <div className="account-box">
              <div className="account-wrapper">
                <h3 className="account-title">Login</h3>
                <p className="account-subtitle"></p>
                {/* Account Form */}
                
                  <div className="form-group">
                    <label>Email</label>
                    <input className="form-control" type="text" id="userName" value={this.state.userName} onChange={this.handleChange('userName')}  />
                    <span className="form-text error-font-color">{this.state.errormsg["userName"]}</span>
                  </div>
                  <div className="form-group">
                    <div className="row">
                      <div className="col">
                        <label>Password</label>
                      </div>
                      <div className="col-auto">
                        <a className="text-muted" href="/forgot-password">
                          Forgot password?
                        </a>
                      </div>
                    </div>
                    <PasswordMask
                      id="password"
                      name="password"
                      //maxLength='14'
                      buttonClassName="hideshowbtn"
                      inputClassName="form-control"
                      //placeholder="Enter password"
                      value={this.state.password}
                      onKeyDown={this.onEnterPress}
                      onChange={this.handleChange('password')}
                    />
                    <span className="form-text error-font-color">{this.state.errormsg["password"]}</span>
                  </div>
                  <div className="form-group text-center">
                    <button className="btn btn-primary account-btn" id="login_btn" onClick={this.loginclick({password})}>Login</button>
                  </div>
                  <div className="account-footer">
                    
                  </div>
               
                {/* /Account Form */}
              </div>
            </div>
            
          </div>
        </div>
        
      );
    }

    if(step == 2){
      div_return.push(
        <div className="account-content account-content-pk">
          <div className="container">
            {/* Account Logo */}
            <div className="account-logo">
              <a href="/login"><img src={Applogo} className="pk-logo" alt="PARTICIPATION HOUSE SUPPORT SERVICES" /></a>
            </div>
            {/* /Account Logo */}
            <div className="account-box account-box-pk">
              <div className="account-wrapper">
                <h3 className="account-title">Configuration</h3>
                <p className="account-subtitle"></p>
                {/* Account Form */}
                <form action="/app/main/dashboard">
                  <div className="row">
                    <div className="col-sm-6">
                      <div className="form-group">
                        <label>Username <span className="text-danger">*</span></label>
                        <input className="form-control" type="text" value={setuserName} onChange={this.handleChange('setuserName')} />
                        <span className="form-text error-font-color">{this.state.errormsg["setuserName"]}</span>
                      </div>
                    </div>
                    <div className="col-sm-6">
                      <div className="form-group">
                        <label>Phone</label>
                        <InputMask className="form-control" mask="(999) 999-9999" value={setMobileNumber} onChange={this.handleChange('setMobileNumber')} ></InputMask>
                        <span className="form-text error-font-color">{this.state.errormsg["setMobileNumber"]}</span>
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-sm-6">
                      <div className="form-group">
                        <label>Password <span className="text-danger">*</span></label>
                        <input className="form-control" type="password" value={setPassword} onChange={this.handleChange('setPassword')}  />
                        <span className="form-text error-font-color">{this.state.errormsg["setPassword"]}</span>
                      </div>
                    </div>
                    <div className="col-sm-6">
                      <div className="form-group">
                        <label>Confirm Password <span className="text-danger">*</span></label>
                        <input className="form-control" type="password" value={setConfirmPassword} onChange={this.handleChange('setConfirmPassword')}  />
                        <span className="form-text error-font-color">{this.state.errormsg["setConfirmPassword"]}</span>
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-sm-6">
                      <div className="form-group">
                        <label>Question #1 <span className="text-danger">*</span></label>
                        <select className="select" value={setQue1} onChange={this.handleChange('setQue1')}>
                          <option className="select_otp_pk">Select</option>
                          {this.state.questions.map((question) => <option className="select_otp_pk" value={question.securityQuestionId}  >{question.securityQuestion}</option>)}
                        </select>
                        <span className="form-text error-font-color">{this.state.errormsg["setQue1"]}</span>
                      </div>
                    </div>
                    <div className="col-sm-6">
                      <div className="form-group">
                        <label>Answer #1 <span className="text-danger">*</span></label>
                        <input type="text" className="form-control" value={setAns1} onChange={this.handleChange('setAns1')} />
                        <span className="form-text error-font-color">{this.state.errormsg["setAns1"]}</span>
                      </div>
                    </div>
                  </div>
                  

                  <div className="row">
                    
                    <div className="col-sm-6">
                        <div className="form-group">
                          <label>Question #2 <span className="text-danger">*</span></label>
                          <select className="select" value={setQue2} onChange={this.handleChange('setQue2')}>
                            <option>Select</option>
                           {this.state.questions.map((question) => <option value={question.securityQuestionId}  >{question.securityQuestion}</option>)}
                          </select>
                          <span className="form-text error-font-color">{this.state.errormsg["setQue2"]}</span>
                        </div>
                    </div>

                    <div className="col-sm-6">
                      <div className="form-group">
                        <label>Answer #2 <span className="text-danger">*</span></label>
                        <input type="text" className="form-control" value={setAns2} onChange={this.handleChange('setAns2')} />
                        <span className="form-text error-font-color">{this.state.errormsg["setAns2"]}</span>
                      </div>
                    </div>
                    
                    
                  </div>

                  <div className="row">
                    <div className="col-sm-6">
                      <div className="form-group">
                        <label>Question #3 <span className="text-danger">*</span></label>
                        <select className="select" value={setQue3} onChange={this.handleChange('setQue3')}>
                          <option>Select</option>
                          {this.state.questions.map((question) => <option value={question.securityQuestionId}  >{question.securityQuestion}</option>)}
                        </select>
                        <span className="form-text error-font-color">{this.state.errormsg["setQue3"]}</span>
                      </div>
                    </div>

                    <div className="col-sm-6">
                       <div className="form-group">
                          <label>Answer #3 <span className="text-danger">*</span></label>
                          <input type="text" className="form-control" value={setAns3} onChange={this.handleChange('setAns3')} />
                          <span className="form-text error-font-color">{this.state.errormsg["setAns3"]}</span>
                        </div>
                    </div>
                  </div>
                  
                  <div className="row">
                    
                    <div className="col-sm-12">
                      <div className="text-right">
                        <button type="submit" className="btn btn-primary" onClick={this.Setuser()}>Submit</button>
                      </div>
                    </div>
                    
                    
                  </div>
                  
                  <div className="account-footer">
                    
                  </div>
                </form>
                {/* /Account Form */}
              </div>
            </div>
          </div>
        </div>
      );
    }

    return div_return;
  }

  

   render() {
      
      return (
        <div>
         <ToastContainer position="top-right"
              autoClose={process.env.API_TOAST_TIMEOUT}
              hideProgressBar={false}
              newestOnTop={false}
              closeOnClick={false}
              rtl={false}
              pauseOnVisibilityChange={false}
              draggable={false}
              pauseOnHover={false} />
         {(this.state.loading) ? <Loader /> : null} {/* Loder method use */}
         
         <div className="main-wrapper">
           <Helmet>
               <title>Login - PARTICIPATION HOUSE SUPPORT SERVICES</title>
               <meta name="description" content="Login page"/>					
         </Helmet>
        {this.step_function()}
      </div>
      </div>
      );
   }
}

export default Loginpage;
